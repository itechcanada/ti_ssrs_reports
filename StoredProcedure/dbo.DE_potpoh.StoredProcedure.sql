USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_potpoh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Sumit       
-- Create date: Oct 29, 2020      
-- Description: <Purchase Order>      
-- =============================================      
Create PROCEDURE [dbo].[DE_potpoh]       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;     
    -- Insert statements for procedure here      
          
    IF OBJECT_ID('dbo.DE_potpoh_rec', 'U') IS NOT NULL      
  drop table dbo.DE_potpoh_rec;      
          
SELECT *      
into  dbo.DE_potpoh_rec      
FROM [LIVEDESTX].[livedestxdb].[informix].[potpoh_rec];      
      
END      
--  exec  DE_potpoh      
-- select * from DE_potpoh_rec  
GO
