USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ortxre]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,mukesh>    
-- Create date: <Create Date,Oct 05, 2015,>    
-- Description: <Description,Due Date,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[US_ortxre]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.US_ortxre_rec', 'U') IS NOT NULL    
  drop table dbo.US_ortxre_rec;     
    
     
    -- Insert statements for procedure here    
SELECT xre_cmpy_id,xre_ord_pfx,xre_ord_no,xre_sld_cus_id,xre_shp_to,xre_ord_typ,'-' as xre_city,xre_cry,xre_crtd_dtts,
xre_crtd_dtms,xre_expy_dt,xre_multp_po,xre_multp_part,xre_multp_ddt,xre_sts_actn

into  dbo.US_ortxre_rec    
  from [LIVEUSSTX].[liveusstxdb].[informix].[ortxre_rec];     
      
END    
-- select * from US_ortxre_rec
GO
