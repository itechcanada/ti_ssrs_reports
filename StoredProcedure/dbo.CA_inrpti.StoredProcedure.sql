USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_inrpti]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Sep 15, 2016,>    
-- Description: <Description,Excess inventory,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CA_inrpti]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.CA_inrpti_rec', 'U') IS NOT NULL    
  drop table dbo.CA_inrpti_rec ;     
    
     
    -- Insert statements for procedure here    
SELECT *   
into  dbo.CA_inrpti_rec    
  from [LIVECASTX].[livecastxdb].[informix].[inrpti_rec] ;    
      
END


-- Select * from CA_inrpti_rec
GO
