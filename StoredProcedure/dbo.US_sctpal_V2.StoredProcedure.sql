USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sctpal_V2]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Jan 7, 2021  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[US_sctpal_V2]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  
   
-- Insert statements for procedure here  
Declare @palAudCtlNo nvarchar(25)
select @palAudCtlNo = max(pal_aud_ctl_no) from dbo.US_sctpal_rec
Print @palAudCtlNo;

 insert into dbo.US_sctpal_rec
select * FROM [LIVEUSSTX].[liveusstxdb].[informix].[sctpal_rec] where pal_aud_ctl_no > @palAudCtlNo;   
  
END  
/*
 --  exec  US_sctpal  
-- select * from US_sctpal_rec  
20210107	Sumit
use audit control number to insert day wise data to minimise data sync activity time
*/
GO
