USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_perppb]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH    
-- Create date: <11/25/2020>  
-- Description: <Description,Germany Product Price Base,>  
-- =============================================  
create PROCEDURE [dbo].[DE_perppb]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

truncate table dbo.DE_perppb_rec;     
-- Insert statements for procedure here  

insert into  dbo.DE_perppb_rec  
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[perppb_rec];  
  
END  

GO
