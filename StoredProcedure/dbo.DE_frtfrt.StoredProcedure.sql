USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_frtfrt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author, Sumit>    
-- Create date: <Create Date, Jan 27 2021>    
--   
    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_frtfrt]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.DE_frtfrt_rec', 'U') IS NOT NULL    
  drop table dbo.DE_frtfrt_rec;     
    
     
    -- Insert statements for procedure here    
SELECT frt_cmpy_id,
frt_ref_pfx,
frt_ref_no,
frt_ref_itm,
frt_trm_trd,
frt_dlvy_mthd,
frt_frt_ven_id,
frt_crr_nm,
frt_port,
-- frt_dest,
frt_cry,
frt_exrt,
frt_ex_rt_typ,
frt_ven_ref,
frt_trrte,
frt_cst,
frt_cst_um,
frt_comp_cst,
frt_fl_cst,
frt_fl_cst_um,
frt_chrg,
frt_chrg_um,
frt_comp_chrg,
frt_fl_chrg,
frt_fl_chrg_um,
frt_frt_na
    
into  dbo.DE_frtfrt_rec    
  FROM [LIVEDESTX].[livedestxdb].[informix].[frtfrt_rec]    
      
END    
-- select * from DE_frtfrt_rec
GO
