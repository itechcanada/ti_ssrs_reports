USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_mxtarh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: April 4, 2013  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_mxtarh]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
 IF OBJECT_ID('dbo.CA_mxtarh_rec', 'U') IS NOT NULL  
  drop table dbo.CA_mxtarh_rec;      

        
SELECT arh_cmpy_id,arh_arch_ver_No,arh_bus_Doc_Typ,arh_gen_dtts,arh_prim_ref
into  dbo.CA_mxtarh_rec 
FROM [LIVECASTX].[livecastxdb].[informix].[mxtarh_rec];  
  
END  
  
--- exec CA_mxtarh  
-- select * from CA_mxtarh_rec
/*
Date:20170412
Modified by mukesh
Modified SP due to it is taking time
*/
GO
