USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_iptjso]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,11/3/2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[CA_iptjso]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 IF OBJECT_ID('dbo.CA_iptjso_rec', 'U') IS NOT NULL
		drop table dbo.CA_iptjso_rec;
    
        
SELECT *
into  dbo.CA_iptjso_rec
FROM [LIVECASTX].[livecastxdb].[informix].[iptjso_rec];
	
	

END

-- Select * from CA_iptjso_rec













GO
