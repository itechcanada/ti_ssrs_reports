USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Emaily_Survey]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mukesh>
-- Create date: <10 Jan 2014>
-- Description:	<LCS_SHIPMENTS Report >
-- =============================================
CREATE  PROCEDURE [dbo].[sp_itech_Emaily_Survey]  @DBNAME varchar(50), @FromDate datetime, @ToDate datetime, @version char = '0'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @DB varchar(100);
declare @sqltxt varchar(6000);
declare @execSQLtxt varchar(7000);
DECLARE @company VARCHAR(15);   
DECLARE @prefix VARCHAR(15);   
DECLARE @DatabaseName VARCHAR(35);    
declare @FD varchar(10)
declare @TD varchar(10)

SET @FD = CONVERT( Varchar(10), @FromDate, 120);
SET @TD	= CONVERT( Varchar(10), @ToDate, 120);

CREATE TABLE #temp ( Dbname			VARCHAR(10),
					  Cus_id			VARCHAR(10), 
					  Cus_long_nm		VARCHAR(70),
					  Admin_brh			VARCHAR(3), 
					  Cntc_typ			VARCHAR(2),
					  Frst_nm			VARCHAR(30), 
					  Lst_nm			VARCHAR(30), 
					  Email				VARCHAR(70),
					  Lgn_id			VARCHAR(30),
					  Frst_sls_dt		VARCHAR(10)
					);
IF @DBNAME = 'ALL'
	BEGIN
		IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
		FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @query NVARCHAR(MAX);
			SET @DB = UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
				SET @query = 'INSERT INTO #temp (Dbname, Cus_id, Cus_long_nm, Admin_brh, Cntc_typ, Frst_nm, Lst_nm, Email, Lgn_id, Frst_sls_dt)
						SELECT DISTINCT '''+ @Prefix +''' , a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, f.cvt_cntc_typ,f.cvt_frst_nm, f.cvt_lst_nm, f.cvt_email,e.slp_lgn_id,b.coc_frst_sls_dt
						FROM  ' + @Prefix + '_arrcus_rec a
						INNER JOIN ' + @Prefix + '_arbcoc_rec b ON a.cus_cus_id=b.coc_cus_id
						INNER JOIN ' + @Prefix + '_arrshp_rec d ON b.coc_cus_id=d.shp_cus_id
						INNER JOIN ' + @Prefix + '_scrslp_rec e ON d.shp_is_slp=e.slp_slp
						INNER JOIN ' + @Prefix + '_scrcvt_rec f ON a.cus_cus_id=f.cvt_cus_ven_id
						WHERE a.cus_actv=1
						--AND a.cus_admin_brh=''WDL''
						AND f.cvt_cus_ven_typ = ''C''
						AND CAST(b.coc_frst_sls_dt AS datetime) BETWEEN  ''' + @FD + ''' AND ''' + @TD + '''
						AND f.cvt_addr_typ = ' + '''L'''
						 
			print @query;
			EXECUTE sp_executesql @query;
			FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;
		END			
		CLOSE ScopeCursor;
		DEALLOCATE ScopeCursor;
	END
ELSE
BEGIN
				
		SET @sqltxt ='INSERT INTO #temp (Dbname, Cus_id, Cus_long_nm, Admin_brh, Cntc_typ, Frst_nm, Lst_nm, Email, Lgn_id, Frst_sls_dt)
						SELECT DISTINCT '''+ @DBNAME +''' , a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, f.cvt_cntc_typ,f.cvt_frst_nm, f.cvt_lst_nm, f.cvt_email,e.slp_lgn_id,b.coc_frst_sls_dt
						FROM  ' + @DBNAME + '_arrcus_rec a
						INNER JOIN ' + @DBNAME + '_arbcoc_rec b ON a.cus_cus_id=b.coc_cus_id
						INNER JOIN ' + @DBNAME + '_arrshp_rec d ON b.coc_cus_id=d.shp_cus_id
						INNER JOIN ' + @DBNAME + '_scrslp_rec e ON d.shp_is_slp=e.slp_slp
						INNER JOIN ' + @DBNAME + '_scrcvt_rec f ON a.cus_cus_id=f.cvt_cus_ven_id
						WHERE a.cus_actv=1
						--AND a.cus_admin_brh=''WDL''
						AND f.cvt_cus_ven_typ = ''C''
						AND CAST(b.coc_frst_sls_dt AS datetime) BETWEEN  ''' + @FD + ''' AND ''' + @TD + '''
						AND f.cvt_addr_typ = ' + '''L'''
	print(@sqltxt)
	set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
END
	SELECT * FROM #temp
	DROP TABLE  #TEMP;
END

--EXEC [sp_itech_Emaily_Survey] 'ALL', '2013-01-01', '2013-11-30'
--EXEC [sp_itech_Emaily_Survey] 'CN', '2013-01-01', '2013-11-30'
--CA
--TW
--NO
--UK
--CN
GO
