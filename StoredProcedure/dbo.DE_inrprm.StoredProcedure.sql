USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_inrprm]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <10/27/2020>    
-- Description: <Germany Product defination>
-- =============================================    
CREATE PROCEDURE [dbo].[DE_inrprm]
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.DE_inrprm_rec', 'U') IS NOT NULL              
  drop table dbo.DE_inrprm_rec;      
     
    
-- Insert statements for procedure here    
SELECT *    
into dbo.DE_inrprm_rec     
FROM [LIVEDESTX].[livedestxdb].[informix].[inrprm_rec]    
    
    
END    

GO
