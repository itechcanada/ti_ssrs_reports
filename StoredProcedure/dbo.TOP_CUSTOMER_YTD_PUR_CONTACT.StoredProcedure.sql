USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TOP_CUSTOMER_YTD_PUR_CONTACT]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/20/2012>
-- Description:	<Description,get current year top 40 customer and list prio year sale value for comparison>
-- =============================================
CREATE PROCEDURE [dbo].[TOP_CUSTOMER_YTD_PUR_CONTACT]
	-- Add the parameters for the stored procedure here
	@PriorYear int,
	@CurrentYear int,
	@USD_UK float,
	@USD_TW float,
	@USD_NO float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
   
CREATE TABLE #ALL_CUSTOMER
(
review_year varchar(3),
cust_id varchar(10),
cust_name varchar(50),
branch varchar(3),
current_total float,
current_conversion float,
prior_total float,
prior_conversion float,
prior_gp float,
current_gp float,
prior_mtl float,
prior_tot_mtl float,
current_mtl float,
current_tot_mtl float
)
/*YTD*/
INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
 'YTD' AS 'YTD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END) AS 'CurrentConversion' 
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [US_arrcus_rec] a 
	INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC') 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
 'YTD' AS 'YTD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*(@USD_UK) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*(@USD_UK) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [UK_arrcus_rec] a 
	INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
AND c.cuc_desc30<>'Interco'
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*(@USD_UK) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*(@USD_UK) END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
 'YTD' AS 'YTD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [CA_arrcus_rec] a 
	INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
	INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat	
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
'YTD' AS 'YTD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val/(@USD_TW) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val/(@USD_TW) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [TW_arrcus_rec] a 
	INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val/(@USD_TW) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val/(@USD_TW) END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
'YTD' AS 'YTD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val* (@USD_NO) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val* (@USD_NO) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [NO_arrcus_rec] a 
	INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val* (@USD_NO) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val* (@USD_NO) END) DESC

/*LYD*/
INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
 'LYD' AS 'LYD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END) AS 'CurrentConversion' 
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [US_arrcus_rec] a 
	INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC') 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
'LYD' AS 'LYD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*(@USD_UK) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*(@USD_UK) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [UK_arrcus_rec] a 
	INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
	INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat	
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*(@USD_UK) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*(@USD_UK) END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
 'LYD' AS 'LYD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val*1 END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [CA_arrcus_rec] a 
	INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
	INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat	
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND c.cuc_desc30<>'Interco'
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val*1 END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
'LYD' AS 'LYD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val/(@USD_TW) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val/(@USD_TW) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [TW_arrcus_rec] a 
	INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
AND c.cuc_desc30<>'Interco'
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val/(@USD_TW) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val/(@USD_TW) END) DESC

INSERT INTO #ALL_CUSTOMER
SELECT TOP 40 WITH TIES 
'LYD' AS 'LYD', a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END) AS 'CurrentTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val* (@USD_NO) END) AS 'CurrentConversion'
, SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END) AS 'PriorTotal', 
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val* (@USD_NO) END) AS 'PriorConversion',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END)*100 END as 'PriorGP%',
CASE WHEN SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)>0 THEN
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END)/SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)*100 END as 'CurrentGP%',
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val-b.sat_tot_avg_val END),
SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@CurrentYear THEN b.sat_tot_val END)
FROM [NO_arrcus_rec] a 
	INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND DATEPART(YYYY,b.sat_inv_dt) BETWEEN @PriorYear AND @CurrentYear
AND c.cuc_desc30<>'Interco'
GROUP BY a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh
HAVING SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val* (@USD_NO) END)>0
ORDER BY  SUM(CASE WHEN DATEPART(YYYY,b.sat_inv_dt)=@PriorYear THEN b.sat_tot_val* (@USD_NO) END) DESC


/*Select TOP 30 customer internationally*/

CREATE TABLE #TOP_CUSTOMER
(
review_year varchar(3),
cust_id varchar(10),
cust_name varchar(50),
branch varchar(3),
current_total float,
current_conversion float,
prior_total float,
prior_conversion float,
prior_gp float,
current_gp float,
prior_mtl float,
prior_tot_mtl float,
current_mtl float,
current_tot_mtl float
)

INSERT INTO #TOP_CUSTOMER
SELECT TOP 40 WITH TIES *
FROM #ALL_CUSTOMER
WHERE
review_year='YTD'
ORDER BY current_conversion DESC

INSERT INTO #TOP_CUSTOMER
SELECT TOP 40 WITH TIES *
FROM #ALL_CUSTOMER
WHERE
review_year='LYD'
ORDER BY prior_conversion DESC

SELECT * FROM #TOP_CUSTOMER

DROP TABLE #ALL_CUSTOMER
DROP TABLE #TOP_CUSTOMER



END


GO
