USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_cctctk]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CA_cctctk] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_cctctk_rec', 'U') IS NOT NULL
		drop table dbo.CA_cctctk_rec;
    
        
SELECT *
into  dbo.CA_cctctk_rec
FROM [LIVECASTX].[livecastxdb].[informix].[cctctk_rec];

END
 --  exec  CA_cctctk
-- select * from CA_cctctk_rec
GO
