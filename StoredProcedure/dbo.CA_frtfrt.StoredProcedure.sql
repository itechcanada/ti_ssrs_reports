USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_frtfrt]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_frtfrt]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.CA_frtfrt_rec', 'U') IS NOT NULL        
  drop table dbo.CA_frtfrt_rec;        
            
                
SELECT *        
into  dbo.CA_frtfrt_rec  
FROM [LIVECASTX].[livecastxdb].[informix].[frtfrt_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
