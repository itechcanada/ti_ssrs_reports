USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_scrdtr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Jan 13, 2020    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[TW_scrdtr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.TW_scrdtr_rec', 'U') IS NOT NULL    
  drop table dbo.TW_scrdtr_rec;    
        
            
SELECT *    
into  dbo.TW_scrdtr_rec    
FROM [LIVETWSTX].[livetwstxdb].[informix].[scrdtr_rec];    
    
END    
--- exec TW_scrpyt    
-- select * from TW_scrdtr_rec
GO
