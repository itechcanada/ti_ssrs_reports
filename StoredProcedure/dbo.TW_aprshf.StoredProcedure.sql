USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_aprshf]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Apr 28, 2016,>  
-- Description: <Description,Open Job history,>  
  
-- =============================================  
create PROCEDURE [dbo].[TW_aprshf]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_aprshf_rec', 'U') IS NOT NULL  
  drop table dbo.TW_aprshf_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_aprshf_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[aprshf_rec] ;   
    
END  
-- select * from TW_aprshf_rec
GO
