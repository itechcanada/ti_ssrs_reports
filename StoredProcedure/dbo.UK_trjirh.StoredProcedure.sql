USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_trjirh]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
-- =============================================    
-- Author:  <Sumit>    
-- Create date: <Create Date,29/6/2020>    
-- Description: <To Select trjirh_rec records from UK Linked Server>    
-- Requirement: <To get customer vendor id and receipt type for journal receiving report>
-- =============================================    
CREATE PROCEDURE [dbo].[UK_trjirh]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.UK_trjirh_rec', 'U') IS NOT NULL          
  drop table dbo.UK_trjirh_rec;          
              
                  
SELECT irh_cmpy_id, irh_ref_pfx, irh_ref_no, irh_actvy_dt, irh_cus_ven_typ, irh_cus_ven_id, irh_rcvg_whs, irh_rcpt_typ
into  dbo.UK_trjirh_rec   
FROM [LIVEUKSTX].[liveukstxdb].[informix].[trjirh_rec]

END

GO
