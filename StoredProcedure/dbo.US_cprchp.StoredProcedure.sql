USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_cprchp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  mukesh    
-- Create date:May 29, 2017    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE [dbo].[US_cprchp]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.US_cprchp_rec', 'U') IS NOT NULL    
  drop table dbo.US_cprchp_rec;    
        
            
SELECT *    
into  dbo.US_cprchp_rec    
FROM [LIVEUSSTX].[liveusstxdb].[informix].[cprchp_rec];    
    
END    
    
-- select * from US_cprchp_rec
GO
