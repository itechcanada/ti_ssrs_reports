USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OOPS_Shipment]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukesh >              
-- Create date: <09 JAN 2018>              
-- Description: <Getting top 50 customers for SSRS reports>              
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_OOPS_Shipment] @DBNAME varchar(50),@Branch varchar(15)      
              
AS              
BEGIN              
 SET NOCOUNT ON;         
         
           
declare @sqltxt varchar(6000)              
declare @execSQLtxt varchar(7000)              
declare @DB varchar(100)              
declare @FD varchar(10)              
declare @TD varchar(10)              
declare @NOOfCust varchar(15)              
DECLARE @ExchangeRate varchar(15)             
DECLARE @IsExcInterco char(1)             
             
set @DB=  @DBNAME              
            
  --set @FD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0),120)   -- First date of Previou Month                       
  --set @TD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1),120) -- Last date of previous Month          
        
  set @FD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0),120)   -- First date of current Month                       
  set @TD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1),120) -- Last date of current Month          

-- 20200428
IF @Branch = 'ROC'          
BEGIN          
SET @Branch = 'ROS'          
END             
--

IF @Branch = 'ALL'          
BEGIN          
SET @Branch = ''          
END             
              
CREATE TABLE #tmp (         
Databases   VARCHAR(15)       
, Branch   VARCHAR(65)      
, TotalValue    DECIMAL(20, 0)              
, NetGP               DECIMAL(20, 2)       
,WorkDaysIncluded int      
  --CustID   VARCHAR(10)              
  --      , CustName     VARCHAR(100)              
  --      ,SalesPersonInside VARCHAR(10)              
  --      ,SalesPersonOutside VARCHAR(10)            
  --      ,SalesPersonTaken VARCHAR(10)            
  --      , Market   VARCHAR(65)               
        --, Form           Varchar(65)              
        --, Grade           Varchar(65)              
        --, TotWeight  DECIMAL(20, 2)              
        --, WeightUM           Varchar(10)              
        --, TotalMatValue    DECIMAL(20, 2)              
        --, MatGP    DECIMAL(20, 2)              
        --, MarketID   integer,              
        --Finish varchar(35),              
        --Size varchar(35),stn_cry   varchar(5)            
        --,InvDate Varchar(10)            
        --,InvNo Varchar(25)           
        --,SalesCategory Varchar(2)          
        --,SizeDesc Varchar(65)           
        --,millName Varchar(40)            
        --,MillDlyDate Varchar(10)       
        --,ShpgWhs Varchar(3)          
                 );               
              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(35);              
DECLARE @Name VARCHAR(15);              
DECLARE @CurrenyRate varchar(15);              
              
              
IF @DBNAME = 'ALL'              
 BEGIN              
        
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(max);                 
      SET @DB= @Prefix        
        
       IF (UPPER(@Prefix) = 'TW')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
        End              
        Else if (UPPER(@Prefix) = 'NO')              
        begin             
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
        End              
        Else if (UPPER(@Prefix) = 'CA')              
        begin              
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
        End              
        Else if (UPPER(@Prefix) = 'CN')              
        begin              
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
        End              
        Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
begin              
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
        End              
        Else if(UPPER(@Prefix) = 'UK')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
        End              
        Else if(UPPER(@Prefix) = 'DE')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
        End              
                    
                    
      if  ( UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')   --UPPER(@Prefix) = 'TW' OR              
                BEGIN              
         SET @query ='INSERT INTO #tmp (Databases, Branch, TotalValue, NetGP,WorkDaysIncluded)                
             
          select  '''+ @Name +''',stn_shpt_brh as Branch, SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as TotalValue,SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP ,      
          (select (      
Case when MONTH(getdate()) = 1 then [janWorkDays]       
 when MONTH(getdate()) = 2 then [febWorkDays]      
 when MONTH(getdate()) = 3 then [marWorkDays]      
 when MONTH(getdate()) = 4 then [aprWorkDays]      
 when MONTH(getdate()) = 5 then [mayWorkDays]      
 when MONTH(getdate()) = 6 then [junWorkDays]      
 when MONTH(getdate()) = 7 then [julWorkDays]      
 when MONTH(getdate()) = 8 then [augWorkDays]      
 when MONTH(getdate()) = 9 then [sepWorkDays]      
 when MONTH(getdate()) = 10 then [octWorkDays]      
 when MONTH(getdate()) = 11 then [novWorkDays]       
else [decWorkDays] end )      
from [tbl_itech_Networkdays] where branchName = stn_shpt_brh)          
         from ' + @Prefix + '_sahstn_rec        
         join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                                           
         left join ' + @Prefix + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) and stn_frm <> ''XXXX''  group by   stn_shpt_brh '              
    END              
       Else              
      BEGIN              
        SET @query ='INSERT INTO #tmp (Databases, Branch, TotalValue, NetGP,WorkDaysIncluded)                
             
          select  '''+ @Name +''',stn_shpt_brh as Branch, SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as TotalValue,SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,      
          (select (      
Case when MONTH(getdate()) = 1 then [janWorkDays]       
 when MONTH(getdate()) = 2 then [febWorkDays]      
 when MONTH(getdate()) = 3 then [marWorkDays]      
 when MONTH(getdate()) = 4 then [aprWorkDays]      
 when MONTH(getdate()) = 5 then [mayWorkDays]      
 when MONTH(getdate()) = 6 then [junWorkDays]      
 when MONTH(getdate()) = 7 then [julWorkDays]      
 when MONTH(getdate()) = 8 then [augWorkDays]      
 when MONTH(getdate()) = 9 then [sepWorkDays]      
 when MONTH(getdate()) = 10 then [octWorkDays]      
 when MONTH(getdate()) = 11 then [novWorkDays]       
else [decWorkDays] end )      
from [tbl_itech_Networkdays] where branchName = stn_shpt_brh)           
         from ' + @Prefix + '_sahstn_rec       
        join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                                           
        left join ' + @Prefix + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
       and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) and stn_frm <> ''XXXX''           
          group by   stn_shpt_brh  '              
      End              
        EXECUTE sp_executesql @query;              
                      
                 
        print(@query)              
      FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE              
     BEGIN             
    Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
                 
    IF (UPPER(@DBNAME) = 'TW')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
   End              
   Else if (UPPER(@DBNAME) = 'NO')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
   End              
   Else if (UPPER(@DBNAME) = 'CA')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
   End              
   Else if (UPPER(@DBNAME) = 'CN')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
   End              
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
   begin              
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
   End              
   Else if(UPPER(@DBNAME) = 'UK')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
   End              
   Else if(UPPER(@DBNAME) = 'DE')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
   End              
                   
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')        --UPPER(@DBNAME) = 'TW' OR         
                BEGIN              
         SET @sqltxt ='INSERT INTO #tmp (Databases, Branch, TotalValue, NetGP,WorkDaysIncluded)                
             
          select  '''+ @Name +''',stn_shpt_brh as Branch, SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as TotalValue,SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP ,      
          (select (      
Case when MONTH(getdate()) = 1 then [janWorkDays]       
 when MONTH(getdate()) = 2 then [febWorkDays]      
 when MONTH(getdate()) = 3 then [marWorkDays]      
 when MONTH(getdate()) = 4 then [aprWorkDays]      
 when MONTH(getdate()) = 5 then [mayWorkDays]      
 when MONTH(getdate()) = 6 then [junWorkDays]      
 when MONTH(getdate()) = 7 then [julWorkDays]      
 when MONTH(getdate()) = 8 then [augWorkDays]      
 when MONTH(getdate()) = 9 then [sepWorkDays]      
 when MONTH(getdate()) = 10 then [octWorkDays]      
 when MONTH(getdate()) = 11 then [novWorkDays]       
else [decWorkDays] end )      
from [tbl_itech_Networkdays] where branchName = stn_shpt_brh)         
         from ' + @DBNAME + '_sahstn_rec        
         join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                                           
         left join ' + @DBNAME + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat       
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''       
        and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) and stn_frm <> ''XXXX''               
          group by  stn_shpt_brh'              
    END              
       Else              
      BEGIN              
        SET @sqltxt ='INSERT INTO #tmp (Databases, Branch, TotalValue, NetGP,WorkDaysIncluded)                  
     select  '''+ @Name +''',stn_shpt_brh as Branch, SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as TotalValue,SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,      
     (select (      
Case when MONTH(getdate()) = 1 then [janWorkDays]       
 when MONTH(getdate()) = 2 then [febWorkDays]      
 when MONTH(getdate()) = 3 then [marWorkDays]      
 when MONTH(getdate()) = 4 then [aprWorkDays]      
 when MONTH(getdate()) = 5 then [mayWorkDays]      
 when MONTH(getdate()) = 6 then [junWorkDays]      
 when MONTH(getdate()) = 7 then [julWorkDays]      
 when MONTH(getdate()) = 8 then [augWorkDays]      
 when MONTH(getdate()) = 9 then [sepWorkDays]      
 when MONTH(getdate()) = 10 then [octWorkDays]      
 when MONTH(getdate()) = 11 then [novWorkDays]       
else [decWorkDays] end )      
from [tbl_itech_Networkdays] where branchName = stn_shpt_brh)           
         from ' + @DBNAME + '_sahstn_rec          
         join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id        
         left join ' + @DBNAME + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat       
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
      and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) and stn_frm <> ''XXXX''        
          group by   stn_shpt_brh'              
      End              
      print(@sqltxt)              
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
     END              
                 
           
   --SELECT TotalValue as ACTUAL,(TotalValue/WorkDaysIncluded)*23 as PACE      
   --  FROM #tmp  WHERE Branch not in ('SFS') and (Branch = @Branch OR @Branch = '')                  
                  
   select 'ACTUAL' as Categ , TotalValue as Value ,     
   (SELECT (CASE WHEN MONTH(getdate()) = 1 THEN [janTarget] WHEN MONTH(getdate()) = 2 THEN [febTarget] WHEN MONTH(getdate())     
                      = 3 THEN [marTarget] WHEN MONTH(getdate()) = 4 THEN [aprTarget] WHEN MONTH(getdate()) = 5 THEN [mayTarget] WHEN MONTH(getdate())     
                      = 6 THEN [junTarget] WHEN MONTH(getdate()) = 7 THEN [julTarget] WHEN MONTH(getdate()) = 8 THEN [augTarget] WHEN MONTH(getdate())     
                      = 9 THEN [sepTarget] WHEN MONTH(getdate()) = 10 THEN [octTarget] WHEN MONTH(getdate()) = 11 THEN [novTarget] ELSE [decTarget] END) AS TargetValue    
FROM         tbl_itech_MonthlyTarget    
WHERE     (branchName = Branch) AND (valueOfType = 'Sales') ) AS TARGET from #tmp WHERE Branch not in ('SFS') and (Branch = @Branch OR @Branch = '')       
   Union       
   select 'PACE' as Categ , case when [dbo].[DATEDIFF_WorkingDaysOnly](@FD,GETDATE()-1) = 0 then 0 else  
   (TotalValue/[dbo].[DATEDIFF_WorkingDaysOnly](@FD,GETDATE()-1))*WorkDaysIncluded  end as Value ,     
   (SELECT (CASE WHEN MONTH(getdate()) = 1 THEN [janTarget] WHEN MONTH(getdate()) = 2 THEN [febTarget] WHEN MONTH(getdate())     
                      = 3 THEN [marTarget] WHEN MONTH(getdate()) = 4 THEN [aprTarget] WHEN MONTH(getdate()) = 5 THEN [mayTarget] WHEN MONTH(getdate())     
                      = 6 THEN [junTarget] WHEN MONTH(getdate()) = 7 THEN [julTarget] WHEN MONTH(getdate()) = 8 THEN [augTarget] WHEN MONTH(getdate())     
                      = 9 THEN [sepTarget] WHEN MONTH(getdate()) = 10 THEN [octTarget] WHEN MONTH(getdate()) = 11 THEN [novTarget] ELSE [decTarget] END) AS TargetValue    
FROM         tbl_itech_MonthlyTarget    
WHERE     (branchName = Branch) AND (valueOfType = 'Sales') )    
    AS TARGET from #tmp WHERE Branch not in ('SFS') and (Branch = @Branch OR @Branch = '') ;      
         
         
   drop table #tmp;      
                 
END              
-- RTRIM(LTrim(CustID))+'-'+Databases as              
           
-- exec [sp_itech_OOPS_Shipment]  'ALL','BHM'         
-- exec [sp_itech_OOPS_Shipment]  'UK','BHM'             
-- exec [sp_itech_OOPS_Shipment]  'US','ROC'             
-- exec [sp_itech_OOPS_Shipment]  'TW','ALL'                       
   /*          
    [DATEDIFF_WorkingDaysOnly]     
        
    SELECT     branchName, valueOfType, (CASE WHEN MONTH(getdate()) = 1 THEN [janTarget] WHEN MONTH(getdate()) = 2 THEN [febTarget] WHEN MONTH(getdate())     
                      = 3 THEN [marTarget] WHEN MONTH(getdate()) = 4 THEN [aprTarget] WHEN MONTH(getdate()) = 5 THEN [mayTarget] WHEN MONTH(getdate())     
                      = 6 THEN [junTarget] WHEN MONTH(getdate()) = 7 THEN [julTarget] WHEN MONTH(getdate()) = 8 THEN [augTarget] WHEN MONTH(getdate())     
                      = 9 THEN [sepTarget] WHEN MONTH(getdate()) = 10 THEN [octTarget] WHEN MONTH(getdate()) = 11 THEN [novTarget] ELSE [decTarget] END) AS TargetValue    
FROM         tbl_itech_MonthlyTarget    
WHERE     (branchName = 'BHM') AND (valueOfType = 'Sales')    

-- 20200428
Mail sub:Home > STRATIXReports > Development > Warehouse Slides
*/   
GO
