USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ReceiptsForVoucher]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <18 Dec 2013>
-- Description:	<Getting Receipt Details for a Voucher SSRS reports>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_ReceiptsForVoucher] @DBNAME varchar(50),  @VoucherNo Varchar(10), @version char = '0'
As
Begin


declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)

/*set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)
*/
IF @VoucherNo = 'ALL' OR @VoucherNo = ''
BEGIN
SET @VoucherNo = '0'
END

CREATE TABLE #tmp (   [Database]		 VARCHAR(10)
   					, VoucherPfx		 VARCHAR(2)
   					, VoucherNo			 int
   					, VchDate			 Date
   					, VenID				 int
   					, POPfx				 Varchar(2)
   					, PONo 				 int
   					, RCPfx 			 Varchar(2)
   					, RCNo				 int
   					)
   					
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
	IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB=  @Prefix
			    SET @query ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, VchDate, VenID, POPfx, PONo, RCPfx, RCNo)
			    SELECT ''' +  @Prefix + ''' as [Database], jci_vchr_pfx as Voucher, jci_vchr_no as VoucherNo, jvc_ent_dt ,jvc_ven_id , jvc_po_pfx , jvc_po_no, 
							jci_trs_pfx , jci_trs_no from ' + @DB + '_apjjci_rec 
							left join ' + @DB + '_apjjvc_rec on jvc_vchr_no = jci_vchr_no and jvc_vchr_pfx = ''VR''							
							where jci_vchr_no = ''' + @VoucherNo +''';' 
				print @query;
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
     IF @DBNAME = 'PS'
     BEGIN
     Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DBNAME +'')
     END
     ELSE
     BEGIN
     Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')
     END
     
			     SET @sqltxt ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, VchDate, VenID, POPfx, PONo, RCPfx, RCNo)
			     SELECT ''' +  @DBNAME + ''' as [Database], jci_vchr_pfx , jci_vchr_no , jvc_ent_dt ,jvc_ven_id , jvc_po_pfx , jvc_po_no, 
							jci_trs_pfx , jci_trs_no from ' + @DBNAME + '_apjjci_rec 
							left join ' + @DBNAME + '_apjjvc_rec on jvc_vchr_no = jci_vchr_no and jvc_vchr_pfx = ''VR''							
							where jci_vchr_no = ''' + @VoucherNo +''';' 
					print(@sqltxt)	
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
   END
   Select * from #tmp order by [Database],VchDate,VoucherNo
   Drop table #tmp
End 

-- Exec [sp_itech_ReceiptsForVoucher] 'ALL', '53250'
-- Exec [sp_itech_ReceiptsForVoucher] 'CA', '53250'
GO
