USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ListingOfLTA_Customer]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Mukesh >      
-- Create date: <10 Feb 2016>      
-- Description: <Getting LTA>      
-- =============================================      
CREATE  PROCEDURE [dbo].[sp_itech_ListingOfLTA_Customer] @DBNAME varchar(50) , @Branch varchar(3), @DayDiff Varchar(10), @version char = '0'
      
AS      
BEGIN      
SET NOCOUNT ON; 

     
      
declare @sqltxt varchar(6000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(100)      
      
CREATE TABLE #tmp (   CompanyName   VARCHAR(10) 
		,ComboID Varchar(100)
        , CustID     VARCHAR(10) 
         , OrderCreateDate    Varchar(10)       
        ,NoOfDays    integer  
        ,CusLongNm  VARCHAR(40)  
     ,Branch   VARCHAR(3)
     ,Market Varchar(35)
     ,LTACustomer Varchar(1)
     ,OrderType Varchar(100)
     ,ForcastPurchasing Varchar(150)
     ,ConcernedStock Varchar(200)  
                 );      
                       
DECLARE @company VARCHAR(35);      
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
   
if @Branch ='ALL'    
 BEGIN    
 set @Branch = ''    
 END               
             
 IF @DBNAME = 'ALL'      
 BEGIN      
      
    IF @version = '0'      
  BEGIN      
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,company,Prefix from tbl_itech_DatabaseName      
    OPEN ScopeCursor;      
  END      
  ELSE      
  BEGIN
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,company,Prefix from tbl_itech_DatabaseName_PS   
			OPEN ScopeCursor; 
  END
    
         
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
        DECLARE @query NVARCHAR(4000);         
       SET @query = 'INSERT INTO #tmp (CompanyName, ComboID, CustID, OrderCreateDate, NoOfDays,CusLongNm,Branch,Market,LTACustomer,OrderType,ForcastPurchasing,ConcernedStock)
       select  ''' + @prefix + ''' ,LTrim(Rtrim(stn_frm)) + '''+ ' ' + ''' + RTrim(LTrim(stn_grd)), xre_sld_cus_id,  Convert(Varchar(10),xre_crtd_dtts,120),    DATEDIFF(day,xre_crtd_dtts, max(orl_rdy_by_dt)) as NoDays , 
     b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30, '' '' as LTACustomer, '' '' as OrderType , '' '' as ForcastPurchasing, '' '' as ConcernedStock  
     from  '+ @prefix +'_ortorl_rec pr  
     join '+ @prefix +'_sahstn_rec on stn_ord_no = orl_ord_no  and stn_ord_pfx = stn_ord_pfx 
     join  '+ @prefix +'_ortxre_rec on xre_ord_pfx = orl_ord_pfx and xre_ord_no = orl_ord_no 
     INNER JOIN '+ @prefix +'_arrcus_rec b ON xre_sld_cus_id=b.cus_cus_id 
       left join '+ @prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
		where orl_ord_pfx = ''SO'' and xre_sts_actn <> ''C'' and (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
		group by xre_sld_cus_id,xre_crtd_dtts, b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30,LTrim(Rtrim(stn_frm)) + '''+ ' ' + ''' + RTrim(LTrim(stn_grd))
		having DATEDIFF(day, xre_crtd_dtts, max(orl_rdy_by_dt)) > '''+ @DayDiff +'''
		order by NoDays desc '      
          print @query;
       EXECUTE sp_executesql @query;      
              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN       
           
     Set @prefix= @DBNAME       
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ComboID, CustID, OrderCreateDate, NoOfDays,CusLongNm,Branch,Market,LTACustomer,OrderType,ForcastPurchasing,ConcernedStock)
       select  ''' + @prefix + ''' ,LTrim(Rtrim(stn_frm)) + '''+ ' ' + ''' + RTrim(LTrim(stn_grd)), xre_sld_cus_id,  Convert(Varchar(10),xre_crtd_dtts,120),    DATEDIFF(day,xre_crtd_dtts, max(orl_rdy_by_dt)) as NoDays , 
     b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30, '' '' as LTACustomer, '' '' as OrderType , '' '' as ForcastPurchasing, '' '' as ConcernedStock  
     from  '+ @prefix +'_ortorl_rec pr 
     join '+ @prefix +'_sahstn_rec on stn_ord_no = orl_ord_no  and stn_ord_pfx = stn_ord_pfx 
     join  '+ @prefix +'_ortxre_rec on xre_ord_pfx = orl_ord_pfx and xre_ord_no = orl_ord_no 
     INNER JOIN '+ @prefix +'_arrcus_rec b ON xre_sld_cus_id=b.cus_cus_id 
       left join '+ @prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
		where orl_ord_pfx = ''SO'' and xre_sts_actn <> ''C'' and (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
		group by xre_sld_cus_id,xre_crtd_dtts, b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30,LTrim(Rtrim(stn_frm)) + '''+ ' ' + ''' + RTrim(LTrim(stn_grd))
		having DATEDIFF(day, xre_crtd_dtts, max(orl_rdy_by_dt)) > '''+ @DayDiff +'''
		order by NoDays desc  '  
            
    print(@sqltxt)      
   set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
       
   END      
select distinct CompanyName,ComboID,CustID,CusLongNm,Branch,Market,OrderType,ForcastPurchasing,ConcernedStock from #tmp  ;
drop table #tmp      
      
      
END      
      
-- exec [sp_itech_ListingOfLTA_Customer]  'US','ALL',90      
      
                      	 	 	 	 
GO
