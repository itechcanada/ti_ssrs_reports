USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_TCRSTC]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sumit>
-- Create date: <29 Sept 2020>
-- Description:	<to get voucher status>

-- =============================================
create PROCEDURE [dbo].[CN_TCRSTC]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CN_tcrstc_rec', 'U') IS NOT NULL
		drop table dbo.CN_tcrstc_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CN_tcrstc_rec
from [LIVECNSTX].[livecnstxdb].[informix].tcrstc_rec; 

END
GO
