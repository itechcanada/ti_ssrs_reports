USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_scrrsn]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[CN_scrrsn] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_scrrsn_rec', 'U') IS NOT NULL
		drop table dbo.CN_scrrsn_rec;
    
        
SELECT *
into  dbo.CN_scrrsn_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[scrrsn_rec];

END
-- select * from CN_scrrsn_rec
-- exec CN_scrrsn
GO
