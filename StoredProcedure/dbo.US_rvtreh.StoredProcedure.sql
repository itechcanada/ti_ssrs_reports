USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_rvtreh]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,OCT 06, 2021,>    
-- Description: <Description,Reservation Update>    
-- =============================================    
CREATE PROCEDURE [dbo].[US_rvtreh]   
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
	SET NOCOUNT ON;    
    IF OBJECT_ID('dbo.US_rvtreh_rec', 'U') IS NOT NULL    
		drop table dbo.US_intpcr_rec ;
 
-- Insert statements for procedure here    
	SELECT *  into  dbo.US_rvtreh_rec  from [LIVEUSSTX].[liveusstxdb].[informix].[rvtreh_rec] ;     
  
END    
GO
