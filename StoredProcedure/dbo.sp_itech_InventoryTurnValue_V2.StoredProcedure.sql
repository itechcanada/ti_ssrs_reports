USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_InventoryTurnValue_V2]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <1 Nov 2016 >    
-- Description: <Get Inventory Turn Value>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_InventoryTurnValue_V2] @Whs Varchar(3),@DBNAME varchar(50), @FromDate datetime, @ToDate datetime    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
   

 declare @sqltxt varchar(6000);    
 declare @execSQLtxt varchar(7000)  ;    
 declare @Whsstore varchar(10)  ;    
 declare @DB varchar(100)   ;    
 DECLARE @CurrenyRate varchar(15)   ;    
 declare @FD varchar(10)          
declare @TD varchar(10)           
    
     
 DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(35);            
DECLARE @Name VARCHAR(15);      
 SET @DB=@DBNAME;      
     
 If(@Whs = 'ALL')    
 begin    
 set @Whs = '';    
 end    
     
 set @FD = CONVERT(VARCHAR(10), @FromDate,120)      
 set @TD = CONVERT(VARCHAR(10), @ToDate,120)      
     
                   CREATE TABLE #tmp (    warehouse   VARCHAR(3)      
        , prd_frm     VARCHAR(6)      
        , prd_Grd  VARCHAR(10)      
        , prd_size   VARCHAR(18)       
        , prd_fnsh   VARCHAR(10)       
                    , PCS    numeric      
        , MSR               DECIMAL(20, 2)      
        , WGT    DECIMAL(20, 2)      
        , InvtVal    DECIMAL(20, 2)      
        , Databases   VARCHAR(15)      
        ,InvtTrn          DECIMAL(20, 1)     
         , InvtWGT    DECIMAL(20, 2)      
         , InvtSale    DECIMAL(20, 2)      
         , InvtNPPct    DECIMAL(20, 2)      
          , InvtAvgWgt    DECIMAL(20, 2)     
          , InvtCount    numeric    
                   );  --     
    
IF @DBNAME = 'ALL'            
  BEGIN          
  DECLARE ScopeCursor CURSOR FOR              
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
  OPEN ScopeCursor;          
            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
        WHILE @@FETCH_STATUS = 0            
       BEGIN            
   DECLARE @query NVARCHAR(max);              
   SET @DB= @Prefix        
     IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))           
    End                     
    Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))           
    End                     
   SET @query =  'insert into #tmp ( warehouse, prd_frm , prd_Grd, prd_size, prd_fnsh, PCS, MSR,  WGT,InvtWGT, InvtVal, InvtTrn,InvtSale,InvtNPPct,InvtAvgWgt,InvtCount,Databases)          
select prd_whs as warehouse, prd_frm, prd_Grd, prd_size, prd_fnsh, sum(prd_ohd_pcs) as PCS,  sum(prd_ohd_msr) as MSR, '                  
       if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                  
       BEGIN                  
       SET @query = @query + ' sum(prd_ohd_wgt) * 2.20462 as Weight,     
       (select SUM(stn_blg_wgt) * 2.20462 from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )   ) as blgweight, '                  
       END                  
       ELSE                  
       BEGIN                  
       SET @query = @query + ' sum(prd_ohd_wgt) as Weight,     
       (select SUM(stn_blg_wgt)  from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )   ) as blgweight, '                  
       END                  
        SET @query = @query + '     
sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / isnull(acp_tot_qty,1)))* '+ @CurrenyRate +'  as InvtVal ,     
(select max(rpd_invt_trn) from    
   ' +  @Prefix + '_plrrpd_rec where rpd_frm = prd_frm and rpd_grd = prd_grd and rpd_size = prd_size and rpd_fnsh = prd_fnsh) as InvtTrn,    
   (select SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +' from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' ) ) as totalsale,    
   (select (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and   
   stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ) as npPct,    
   (select AVG( stn_blg_wgt) from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ) as avgblgWeight,    
   (select COUNT(*)  from  ' +  @Prefix + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ),    
    ''' + @Prefix + '''    
      from ' +  @Prefix + '_intprd_rec left join    
      ' +  @Prefix + '_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool             
       where  (acp_tot_qty <> 0 or acp_tot_qty is null) and prd_invt_sts = ''S'' and (prd_whs = '''+ @Whs +''' or '''+ @Whs +'''= '''')      
        group by prd_whs,prd_frm, prd_Grd, prd_size, prd_fnsh         
     '    
     EXECUTE sp_executesql @query;            
   print(@query)              
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                 
                 
   END            
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;          
                 
                 
 END   -- All Database block end here          
           
           
 ELSE  -- Single database query start here          
  BEGIN       
  SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')                
                        
     IF (UPPER(@DB) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DB) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End                    
    Else if(UPPER(@DB) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End                    
          
   SET @sqltxt=  ' insert into #tmp ( warehouse, prd_frm , prd_Grd, prd_size, prd_fnsh, PCS, MSR,  WGT, InvtWGT, InvtVal, InvtTrn,InvtSale,InvtNPPct,InvtAvgWgt,InvtCount,Databases)          
select prd_whs as warehouse, prd_frm, prd_Grd, prd_size, prd_fnsh, sum(prd_ohd_pcs) as PCS,  sum(prd_ohd_msr) as MSR, '                  
       if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                  
       BEGIN                  
       SET @sqltxt = @sqltxt + ' sum(prd_ohd_wgt) * 2.20462 as Weight,    
       (select SUM(stn_blg_wgt) * 2.20462 from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )   ) as blgweight, '                  
       END                  
       ELSE                  
       BEGIN                  
       SET @sqltxt = @sqltxt + ' sum(prd_ohd_wgt) as Weight,     
           (select SUM(stn_blg_wgt)  from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''  AND stn_shpt_brh NOT IN(''SFS'' )  ) as blgweight, '                  
       END                  
        SET @sqltxt = @sqltxt + '     
sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / isnull(acp_tot_qty,1)))* '+ @CurrenyRate +' as InvtVal ,     
(select max(rpd_invt_trn) from    
   ' +  @DBNAME + '_plrrpd_rec where rpd_frm = prd_frm and rpd_grd = prd_grd and rpd_size = prd_size and rpd_fnsh = prd_fnsh) as InvtTrn,    
       
   (select SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +' from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' ) ) as totalsale,    
   (select (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and   
   stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ) as npPct,    
   (select AVG( stn_blg_wgt) from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ) as avgblgWeight,    
   (select COUNT(*)  from  ' +  @DBNAME + '_sahstn_rec Where stn_frm = prd_frm and stn_grd= prd_grd and stn_size = prd_size and stn_fnsh = prd_fnsh    
   and stn_shpg_whs = prd_whs and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' AND stn_shpt_brh NOT IN(''SFS'' )  ),    
       
    ''' + @DBNAME + '''    
      from ' +  @DBNAME + '_intprd_rec left join    
      ' +  @DBNAME + '_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool             
       where  (acp_tot_qty <> 0 or acp_tot_qty is null) and prd_invt_sts = ''S'' and (prd_whs = '''+ @Whs +''' or '''+ @Whs +'''= '''')      
        group by prd_whs,prd_frm, prd_Grd, prd_size, prd_fnsh '    
         print(@sqltxt)      
  set @execSQLtxt = @sqltxt;       
  EXEC (@execSQLtxt);      
  End    
    
  SElect * from #tmp;    
  drop table #tmp;    
    
        
END    
    
    
-- Exec [sp_itech_InventoryTurnValue_v2] 'ALL','ALL', '2016-10-01','2016-11-01'    
-- SP_help US_intprd_rec    
    
/*    
date: 2016-11-01    
Mail Sub: Inventory Turns Report    
2.  Please add a date range in the header and new fields in the report body such that the following is reported for each product code, between the entered date range    
     
a.  Total Invoice Dollars    
b.  Total lbs. shipped    
c.   Average Net Profit %    
d.  Average lot size = average lbs. per line item (or BOL) whichever you can access.    
e.  Count of Invoices for that product code    
     
    
    
*/  
GO
