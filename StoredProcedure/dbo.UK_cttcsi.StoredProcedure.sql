USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_cttcsi]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: Dec 12, 2015  
-- Description: <Description,,>  
-- =============================================  
Create PROCEDURE [dbo].[UK_cttcsi]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.UK_cttcsi_rec', 'U') IS NOT NULL  
  drop table dbo.UK_cttcsi_rec;  
      
          
SELECT *  
into  dbo.UK_cttcsi_rec  
FROM [LIVEUKSTX].[liveukstxdb].[informix].[cttcsi_rec];  
  
END  
--  exec  UK_cttcsi  
-- select * from UK_cttcsi_rec
GO
