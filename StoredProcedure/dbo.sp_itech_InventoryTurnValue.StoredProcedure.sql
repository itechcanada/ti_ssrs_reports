USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_InventoryTurnValue]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh>
-- Create date: <23Nov 2015 >
-- Description:	<Get Inventory Turn Value>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_InventoryTurnValue] @Whs Varchar(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	If(@Whs = 'ALL')
	begin
	set @Whs = '';
	end
	
select prd_whs as warehouse, prd_frm, prd_Grd, prd_size, prd_fnsh, sum(prd_ohd_pcs) as PCS,  sum(prd_ohd_msr) as MSR, sum(prd_ohd_wgt) as WGT,
sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / isnull(acp_tot_qty,1))) as InvtVal , 
(select max(rpd_invt_trn) from
    US_plrrpd_rec where rpd_frm = prd_frm and rpd_grd = prd_grd and rpd_size = prd_size and rpd_fnsh = prd_fnsh) as InvtTrn
      from US_intprd_rec left join
      US_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool         
       where  (acp_tot_qty <> 0 or acp_tot_qty is null) and prd_invt_sts = 'S' and (prd_whs = @Whs OR '' = @Whs)
        group by prd_whs,prd_frm, prd_Grd, prd_size, prd_fnsh

    
END
GO
