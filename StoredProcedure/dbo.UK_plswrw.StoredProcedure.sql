USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_plswrw]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,JUN 28, 2015,>  
-- Description: <Description,  Warehouse Rmnt Weekly Data>  
  
-- =============================================  
Create PROCEDURE [dbo].[UK_plswrw]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_plswrw_rec', 'U') IS NOT NULL  
  drop table dbo.UK_plswrw_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_plswrw_rec  
  from [LIVEUK_IW].[liveukstxdb_iw].[informix].[plswrw_rec] ;   
    
END  
-- select * from UK_plswrw_rec where wrw_run_dt is desc
GO
