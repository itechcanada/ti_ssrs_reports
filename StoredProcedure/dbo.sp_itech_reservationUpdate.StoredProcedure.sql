USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_reservationUpdate]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
--<Author: Sumit>  
--<Date: 06 oct 2021>
--<Desc: For Reservation Update report>    
--------------------------------------------    
CREATE proc [dbo].[sp_itech_reservationUpdate]     
(     
 @DBName varchar(45),     
 @Branch varchar(45),    
 @FromDate datetime,    
 @ToDate datetime    
)    
as    
BEGIN    
 SET NOCOUNT ON;    
 declare @sqltext varchar(8000)    
 DECLARE @DB varchar(100)                                                     
 DECLARE @DatabaseName VARCHAR(35);                                                        
 DECLARE @Prefix VARCHAR(35);                          
 DECLARE @Name VARCHAR(15);                                                  
 DECLARE @CurrenyRate varchar(15);  
 DECLARE @FD varchar(10);  
 DECLARE @TD varchar(10);  
  
 set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                
 set @TD = CONVERT(VARCHAR(10), @ToDate , 120)    
  
 print @FD +' '+ @TD;  
    
 set @DB=  @DBNAME    
 if(@Branch = 'ALL')  
 set @Branch = '';  
    
 create table #temp    
 (    
  [Databases] nvarchar(10),    
  Branch nvarchar(45),    
  ItmCtlNo nvarchar(45),  
  HRefPfx nvarchar(5),    
  HRefNo nvarchar(45),    
  HPcs int,    
  HMsr decimal(20,2),    
  HWgt decimal(20,2),    
  Whs nvarchar(10),    
  Loc nvarchar(45),    
  TagNo nvarchar(45),    
  Mill nvarchar(45),    
  Heat nvarchar(45),    
  MaterialValue decimal(20,2),    
  Form nvarchar(45),    
  Grade nvarchar(45),    
  Size nvarchar(45),    
  Finish nvarchar(45),    
  ReservationDate nvarchar(10),   
  ReservationAction nvarchar(45),  
  ReservedBy nvarchar(45),    
  CurrentReservationPfx nvarchar(5),    
  CurrentReservationNo nvarchar(45)    
 );    
  
 print '1';  
    
 IF @DBNAME = 'ALL'                                                        
 BEGIN                                                        
  DECLARE ScopeCursor CURSOR FOR                                            
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                             
  OPEN ScopeCursor;                                            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                  
  WHILE @@FETCH_STATUS = 0                                                        
  BEGIN                                                        
   SET @DB= @Prefix    
    
   IF (UPPER(@DB) = 'TW')                                              
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                                          
   End                                              
   Else if (UPPER(@DB) = 'NO')                                                          
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                                          
   End                                                
   Else if (UPPER(@DB) = 'CA')                                                          
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                                  
   End                                                          
   Else if (UPPER(@DB) = 'CN')                                                          
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                                          
   End                                                          
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                                          
   begin                                                    
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                                          
   End                                                          
   Else if(UPPER(@DB) = 'UK')                                               
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                                          
   End                                               
   Else if(UPPER(@DB) = 'DE')                                                          
   begin                                                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                                          
   End                                                           
     
   print '2';  
     
   set @sqltext = 'insert into #temp([Databases], Branch, ItmCtlNo, HRefPfx, HRefNo, HPcs, HMsr, HWgt, Whs, Loc, TagNo, Mill, Heat, MaterialValue, Form, Grade, Size, Finish,     
  ReservationDate, ReservationAction, ReservedBy, CurrentReservationPfx, CurrentReservationNo)      
  select distinct '''+ @DB +''',rlg_brh, reh_itm_ctl_no, reh_ref_pfx,reh_ref_no, reh_res_pcs, reh_res_msr, reh_res_wgt, rlg_whs, rlg_loc, rlg_tag_no, rlg_mill, rlg_heat, ('+ @CurrenyRate +' * rlg_mat_val) as rlg_mat_val,     
  rlg_frm, rlg_grd, rlg_size, rlg_fnsh, convert(varchar(10),rlg_ssn_dtts,103) as rlg_ssn_dtts, (case when RLG_UPD_ACTN =''A'' then ''Add'' when rlg_upd_actn = ''D'' then ''Delete''   
  when rlg_upd_actn =''C'' then ''Change'' when rlg_upd_actn = ''R'' then ''Relieve Inventory'' when rlg_upd_actn = ''P'' then ''Physically delete relieved''  
  when rlg_upd_actn = ''V'' then ''Compute Material Value'' end) as rlg_upd_actn, usr_nm, ISNULL(res_ref_pfx,'''') as res_ref_pfx, ISNULL(CAST(res_ref_no as nvarchar(45)),'''') as res_ref_no  
  FROM '+ @DB +'_rvtreh_rec     
  inner join '+ @DB +'_rvtrlg_rec on reh_Cmpy_id= rlg_cmpy_id and reh_itm_ctl_no = rlg_itm_Ctl_no     
  inner join '+ @DB +'_mxrusr_rec on usr_lgn_id = rlg_lgn_id    
  left join '+ @DB +'_rvtres_rec on res_Cmpy_id= rlg_cmpy_id    
  and res_itm_ctl_no = rlg_itm_Ctl_no     
  where convert(varchar(10),rlg_ssn_dtts,120) >= convert(varchar(10),'''+ @FD +''',120) and convert(varchar(10),rlg_ssn_dtts,120) <= convert(varchar(10),'''+ @TD +''',120)    
  and (rlg_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')';    
    
   print @sqltext;    
   --EXECUTE sp_executesql @sqltext;                                                        
   EXEC (@sqltext);    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
  END    
  CLOSE ScopeCursor;                                                        
  DEALLOCATE ScopeCursor;     
 END    
 ELSE    
 BEGIN    
  IF (UPPER(@DB) = 'TW')                                              
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                                          
  End                                              
  Else if (UPPER(@DB) = 'NO')                                                          
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                                          
  End                                                
  Else if (UPPER(@DB) = 'CA')                                                          
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                                  
  End                                                          
  Else if (UPPER(@DB) = 'CN')                                                          
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                                          
  End                                                          
  Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
  begin                                                    
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                                          
  End                                                          
  Else if(UPPER(@DB) = 'UK')                                                          
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                                          
  End                                               
  Else if(UPPER(@DB) = 'DE')                                                          
  begin                                                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                                          
  End    
  print '3';  
  
  set @sqltext = 'insert into #temp([Databases], Branch, ItmCtlNo, HRefPfx, HRefNo, HPcs, HMsr, HWgt, Whs, Loc, TagNo, Mill, Heat, MaterialValue, Form, Grade, Size, Finish,     
  ReservationDate, ReservationAction, ReservedBy, CurrentReservationPfx, CurrentReservationNo)      
  select distinct '''+ @DB +''',rlg_brh, reh_itm_ctl_no, reh_ref_pfx,reh_ref_no, reh_res_pcs, reh_res_msr, reh_res_wgt, rlg_whs, rlg_loc, rlg_tag_no, rlg_mill, rlg_heat, ('+ @CurrenyRate +' * rlg_mat_val) as rlg_mat_val,     
  rlg_frm, rlg_grd, rlg_size, rlg_fnsh, convert(varchar(10),rlg_ssn_dtts,103) as rlg_ssn_dtts, (case when RLG_UPD_ACTN =''A'' then ''Add'' when rlg_upd_actn = ''D'' then ''Delete''   
  when rlg_upd_actn =''C'' then ''Change'' when rlg_upd_actn = ''R'' then ''Relieve Inventory'' when rlg_upd_actn = ''P'' then ''Physically delete relieved''  
  when rlg_upd_actn = ''V'' then ''Compute Material Value'' end) as rlg_upd_actn, usr_nm, ISNULL(res_ref_pfx,'''') as res_ref_pfx, ISNULL(CAST(res_ref_no as nvarchar(45)),'''') as res_ref_no    
  FROM '+ @DB +'_rvtreh_rec     
  inner join '+ @DB +'_rvtrlg_rec on reh_Cmpy_id= rlg_cmpy_id and reh_itm_ctl_no = rlg_itm_Ctl_no     
  inner join '+ @DB +'_mxrusr_rec on usr_lgn_id = rlg_lgn_id    
  left join '+ @DB +'_rvtres_rec on res_Cmpy_id= rlg_cmpy_id    
  and res_itm_ctl_no = rlg_itm_Ctl_no     
  where convert(varchar(10),rlg_ssn_dtts,120) >= convert(varchar(10),'''+ @FD +''',120) and convert(varchar(10),rlg_ssn_dtts,120) <= convert(varchar(10),'''+ @TD +''',120)    
  and (rlg_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')';    
    
  print @sqltext;    
  EXEC (@sqltext);    
 END    
    
 Select * from #temp order by ItmCtlNo desc;    
 Drop table #temp;    
    
END  
/*  
-- exec sp_itech_reservationUpdate 'CA','MTL', '2021-09-25','2021-10-06'  
  
*/  
  
GO
