USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sahsax]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Nov 26, 2019    
-- Description: <Description,,>    
-- =============================================    
create PROCEDURE [dbo].[US_sahsax]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.US_sahsax_rec', 'U') IS NOT NULL    
  drop table dbo.US_sahsax_rec;    
        
            
SELECT *    
into  dbo.US_sahsax_rec    
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsax_rec];    
    
END    
-- select * from US_sahsax_rec
GO
