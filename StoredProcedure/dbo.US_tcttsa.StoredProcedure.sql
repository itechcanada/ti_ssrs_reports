USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_tcttsa]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <Create Date,August,30 2015,>


-- =============================================
CREATE PROCEDURE [dbo].[US_tcttsa]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

Insert  into  [Stratix_US].[dbo].[US_tcttsa_rec]
  SELECT * from [LIVEUSSTX].[liveusstxdb].[informix].[tcttsa_rec] 
  
END
-- select * from us_tcttsa_rec 

GO
