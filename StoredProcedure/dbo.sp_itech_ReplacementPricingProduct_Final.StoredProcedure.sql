USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ReplacementPricingProduct_Final]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Mukesh >                            
-- Create date: <08 Aug 2018>                            
-- Description: <Get inventory results with size and finish>                           
                        
                          
-- =============================================                            
create PROCEDURE [dbo].[sp_itech_ReplacementPricingProduct_Final] @DBNAME varchar(50)                              
                            
AS                            
BEGIN                            
                             
 SET NOCOUNT ON;                  
          if(@DBNAME = 'ALL')
          Begin
          select * from tbl_itech_ReplacementPricing;
          End        
          else if(@DBNAME = 'US')
          begin
          select * from tbl_itech_ReplacementPricing where [Database] = 'US'
          END
          else if(@DBNAME = 'UK')
          begin
          select * from tbl_itech_ReplacementPricing where [Database] = 'UK'
          END
          else if(@DBNAME = 'TW')
          begin
          select * from tbl_itech_ReplacementPricing where [Database] = 'TW'
          END
          else if(@DBNAME = 'CA')
          begin
          select * from tbl_itech_ReplacementPricing where [Database] = 'CA'
          END
          else if(@DBNAME = 'CN')
          begin
          select * from tbl_itech_ReplacementPricing where [Database] = 'CN'
          END
          
END                            
GO
