USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ProductQtySales]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================                  
-- Author:  <Mukesh >                  
-- Create date: <26 NOV 2019>                  
-- Description: <Getting top 50 customers for SSRS reports>                  
-- =============================================                  
CREATE PROCEDURE [dbo].[sp_itech_ProductQtySales] @DBNAME varchar(50), @Form varchar(6), @Grade varchar(8), @Size varchar(15), @Finish varchar(8),      
  @FromDate datetime, @ToDate datetime                 
                  
AS                  
BEGIN                  
                   
                   
 SET NOCOUNT ON;                  
declare @sqltxt1 varchar(8000)                  
declare @sqltxt2 varchar(8000)                  
declare @execSQLtxt varchar(7000)                  
declare @DB varchar(100)                  
declare @NOOfCust varchar(15)                  
DECLARE @ExchangeRate varchar(15)                         
DECLARE @CurrenyRate varchar(15)                
declare @FD varchar(10)                  
declare @TD varchar(10)                  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                  
                  
set @DB=  @DBNAME                  
                 
                  
                  
CREATE TABLE #tmp ( [Database]   VARCHAR(3)                 
  ,branch Varchar(3)                 
        , invoiceDate   datetime                  
        , branchLongName    Varchar(15)                  
        , form   Varchar(6)                  
        , grade Varchar(8)                
        , size Varchar(15)                
        , finish   Varchar(8)                  
        , efEvar   Varchar(30)       
        , dimSeg   Varchar(3)       
        , dimDSGN   Varchar(1)        
        , lgthDisplayFormat   Varchar(1)      
        , entMst   Varchar(1)                   
        , invtCtl   Varchar(1)                   
        , width Decimal(20,2)                
        , satLength  Decimal(20,2)                  
        , gaSize Decimal(20,2)                
        , idia Decimal (20,2)                
        , odia Decimal (20,2)        
        ,gaType varchar(1)      
        ,WeightUM varchar(3)      
        ,slsQlf varchar(1)      
        ,blgQty decimal(20,2)      
        ,matVal decimal(20,2)              
       , avgVal Decimal (20,2)      
       , customerNM varchar(35)   
       ,customerPCD varchar(10)             
                 );                   
                              
                  
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @Prefix VARCHAR(35);                  
DECLARE @Name VARCHAR(15);                  
                  
IF @DBNAME = 'ALL'                  
 BEGIN                  
                    
  DECLARE ScopeCursor CURSOR FOR                  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                    
    OPEN ScopeCursor;                  
                  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
     WHILE @@FETCH_STATUS = 0                  
       BEGIN                  
        DECLARE @query1 nVARCHAR(max);                     
        DECLARE @query2 nVARCHAR(max);                     
      SET @DB= @Prefix                   
                      
      IF (UPPER(@DB) = 'TW')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                
   End                                
   Else if (UPPER(@DB) = 'NO')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                
   End                                
   Else if (UPPER(@DB) = 'CA')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                       
   End                                
   Else if (UPPER(@DB) = 'CN')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                
 End                                
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                
   begin                          
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                
   End                                
   Else if(UPPER(@DB) = 'UK')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                
 End                    
   Else if(UPPER(@DB) = 'DE')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                
 End                    
                   
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
        Begin                  
           SET @query1 ='INSERT INTO #tmp ([Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN, lgthDisplayFormat , entMst , invtCtl, width      
   , satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf  ,blgQty,matVal , avgVal,customerNM,customerPCD )                  
             SELECT ''' +  @DB + ''', sat_shpt_brh, sat_inv_dt,brh_brh_nm,sat_frm,sat_grd,sat_size,sat_fnsh,sat_ef_evar,prm_dim_seg,sat_dim_dsgn,prm_lgth_disp_fmt,      
    sat_ent_msr,frm_invt_ctl,sat_wdth,sat_lgth,sat_ga_size,sat_idia,sat_odia,sat_ga_typ,sat_ord_wgt_um,sat_sls_qlf,      
    CASE      
WHEN sat_sls_qlf = ''T'' AND      
    (select COUNT(*) from ' + @DB + '_sahsax_rec, ' + @DB + '_orrprs_rec      
where sax_cmpy_id = sat_cmpy_id AND      
sax_sls_qlf = sat_sls_qlf AND      
sax_upd_ref_no = sat_upd_ref_no AND      
 sax_upd_ref_itm = sat_upd_ref_itm AND      
DATEPART(YEAR,sax_upd_dt) = sat_upd_cy AND      
DATEPART(MONTH,sax_upd_dt) = sat_upd_mth  AND      
DATEPART(DAY,sax_upd_dt) = sat_upd_dy AND      
prs_cmpy_id = sax_cmpy_id AND      
prs_prs = sax_prs AND      
prs_prs_cl in (''AC'', ''IN'', ''PG'', ''PK'', ''UP'')      
 )> 0      
 THEN 0      
ELSE sat_blg_qty      
END      
 ,      
     sat_tot_mtl_val* '+ @CurrenyRate +' ,      
    sat_mpft_avg_val* '+ @CurrenyRate +' ,    
    cus_cus_long_nm ,cva_pcd         
FROM      
      ' + @DB + '_sahsat_rec,      
    ' + @DB + '_arrshp_rec,      
    ' + @DB + '_scrbrh_rec,      
    ' + @DB + '_inrprm_rec,      
    ' + @DB + '_inrfrm_rec  ,    
     ' + @DB + '_arrcus_rec ,  
     ' + @DB + '_scrcva_rec      
WHERE            
      
       sat_cmpy_id      =   shp_cmpy_id      
AND        sat_sld_cus_id   =   shp_cus_id      
AND        sat_shp_to       =   shp_shp_to      
      
AND        sat_cmpy_id      =   brh_cmpy_id      
AND        sat_shpt_brh     =   brh_brh      
      
AND        prm_frm          =   sat_frm      
AND        prm_grd          =   sat_grd      
AND        prm_size         =   sat_size      
AND        prm_fnsh         =   sat_fnsh      
      
AND        sat_frm          =   frm_frm      
    
and  cus_cmpy_id = sat_cmpy_id    
and cus_cus_id = sat_sld_cus_id    
and cva_cmpy_id = sat_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = sat_sld_cus_id  
--and  cva_addr_no = 0   
and cva_addr_typ = ''L''   
   and ( sat_inv_dt >= '''+ @FD +''' AND sat_inv_dt <= '''+ @TD +''' );'                                     
        End                  
        Else                  
        Begin                  
           SET @query1 ='INSERT INTO #tmp ([Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN, lgthDisplayFormat , entMst , invtCtl, width      
   , satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf  ,blgQty,matVal , avgVal ,customerNM,customerPCD )                  
              SELECT ''' +  @DB + ''', sat_shpt_brh, sat_inv_dt,brh_brh_nm,sat_frm,sat_grd,sat_size,sat_fnsh,sat_ef_evar,prm_dim_seg,sat_dim_dsgn,prm_lgth_disp_fmt,      
    sat_ent_msr,frm_invt_ctl,sat_wdth,sat_lgth,sat_ga_size,sat_idia,sat_odia,sat_ga_typ,sat_ord_wgt_um,sat_sls_qlf,      
    CASE      
WHEN sat_sls_qlf = ''T'' AND      
    (select COUNT(*) from ' + @DB + '_sahsax_rec, ' + @DB + '_orrprs_rec      
where sax_cmpy_id = sat_cmpy_id AND      
sax_sls_qlf = sat_sls_qlf AND      
sax_upd_ref_no = sat_upd_ref_no AND      
 sax_upd_ref_itm = sat_upd_ref_itm AND      
DATEPART(YEAR,sax_upd_dt) = sat_upd_cy AND      
DATEPART(MONTH,sax_upd_dt) = sat_upd_mth  AND      
DATEPART(DAY,sax_upd_dt) = sat_upd_dy AND      
prs_cmpy_id = sax_cmpy_id AND      
prs_prs = sax_prs AND      
prs_prs_cl in (''AC'', ''IN'', ''PG'', ''PK'', ''UP'')      
 )> 0      
 THEN 0      
ELSE sat_blg_qty      
END      
 ,      
     sat_tot_mtl_val* '+ @CurrenyRate +' ,      
    sat_mpft_avg_val* '+ @CurrenyRate +' ,    
    cus_cus_long_nm ,cva_pcd         
FROM      
      ' + @DB + '_sahsat_rec,      
    ' + @DB + '_arrshp_rec,      
    ' + @DB + '_scrbrh_rec,      
    ' + @DB + '_inrprm_rec,      
    ' + @DB + '_inrfrm_rec   ,    
     ' + @DB + '_arrcus_rec ,  
     ' + @DB + '_scrcva_rec     
WHERE            
      
       sat_cmpy_id      =   shp_cmpy_id      
AND        sat_sld_cus_id   =   shp_cus_id      
AND        sat_shp_to       =   shp_shp_to      
      
AND        sat_cmpy_id      =   brh_cmpy_id      
AND        sat_shpt_brh     =   brh_brh      
      
AND        prm_frm          =   sat_frm      
AND        prm_grd          =   sat_grd      
AND        prm_size         =   sat_size      
AND        prm_fnsh         =   sat_fnsh      
      
AND        sat_frm          =   frm_frm      
    
and  cus_cmpy_id = sat_cmpy_id    
and cus_cus_id = sat_sld_cus_id    
and cva_cmpy_id = sat_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = sat_sld_cus_id  
--and  cva_addr_no = 0   
and cva_addr_typ = ''L''   
   and ( sat_inv_dt >= '''+ @FD +''' AND sat_inv_dt <= '''+ @TD +''' );'                    
        End                  
                  
       print @query1;                  
        EXECUTE sp_executesql @query1;  --@query2                  
                       
                          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
       END                   
    CLOSE ScopeCursor;                  
    DEALLOCATE ScopeCursor;                  
  END               
  ELSE                  
     BEGIN                  
                     
     IF (UPPER(@DB) = 'TW')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                
    End                                
    Else if (UPPER(@DB) = 'NO')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                
    End                                
    Else if (UPPER(@DB) = 'CA')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                
    End                                
    Else if (UPPER(@DB) = 'CN')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                
    End                                
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                
    begin                                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                
    End                                
    Else if(UPPER(@DB) = 'UK')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                
End                 
    Else if(UPPER(@DB) = 'DE')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                
End                 
                    
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
   Begin                  
       SET @sqltxt1 ='INSERT INTO #tmp ([Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN, lgthDisplayFormat , entMst , invtCtl, width      
   , satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf  ,blgQty,matVal , avgVal,customerNM ,customerPCD )                  
             SELECT ''' +  @DB + ''', sat_shpt_brh, sat_inv_dt,brh_brh_nm,sat_frm,sat_grd,sat_size,sat_fnsh,sat_ef_evar,prm_dim_seg,sat_dim_dsgn,prm_lgth_disp_fmt,      
    sat_ent_msr,frm_invt_ctl,sat_wdth,sat_lgth,sat_ga_size,sat_idia,sat_odia,sat_ga_typ,sat_ord_wgt_um,sat_sls_qlf,      
    CASE      
WHEN sat_sls_qlf = ''T'' AND      
    (select COUNT(*) from ' + @DB + '_sahsax_rec, ' + @DB + '_orrprs_rec      
where sax_cmpy_id = sat_cmpy_id AND      
sax_sls_qlf = sat_sls_qlf AND      
sax_upd_ref_no = sat_upd_ref_no AND      
 sax_upd_ref_itm = sat_upd_ref_itm AND      
DATEPART(YEAR,sax_upd_dt) = sat_upd_cy AND      
DATEPART(MONTH,sax_upd_dt) = sat_upd_mth  AND      
DATEPART(DAY,sax_upd_dt) = sat_upd_dy AND      
prs_cmpy_id = sax_cmpy_id AND      
prs_prs = sax_prs AND      
prs_prs_cl in (''AC'', ''IN'', ''PG'', ''PK'', ''UP'')      
 )> 0      
 THEN 0      
ELSE sat_blg_qty      
END      
 ,      
     sat_tot_mtl_val* '+ @CurrenyRate +' ,      
    sat_mpft_avg_val* '+ @CurrenyRate +' ,    
    cus_cus_long_nm ,cva_pcd       
FROM      
      ' + @DB + '_sahsat_rec,      
    ' + @DB + '_arrshp_rec,      
    ' + @DB + '_scrbrh_rec,      
    ' + @DB + '_inrprm_rec,      
    ' + @DB + '_inrfrm_rec  ,    
     ' + @DB + '_arrcus_rec  ,  
     ' + @DB + '_scrcva_rec    
WHERE            
      
       sat_cmpy_id      =   shp_cmpy_id      
AND        sat_sld_cus_id   =   shp_cus_id      
AND        sat_shp_to       =   shp_shp_to      
      
AND        sat_cmpy_id      =   brh_cmpy_id      
AND        sat_shpt_brh     =   brh_brh      
      
AND        prm_frm          = sat_frm      
AND        prm_grd          =   sat_grd      
AND        prm_size         =   sat_size      
AND        prm_fnsh         =   sat_fnsh      
      
AND        sat_frm          =   frm_frm      
    
and  cus_cmpy_id = sat_cmpy_id    
and cus_cus_id = sat_sld_cus_id   
and cva_cmpy_id = sat_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = sat_sld_cus_id  
--and  cva_addr_no = 0   
and cva_addr_typ = ''L''   
   and ( sat_inv_dt >= '''+ @FD +''' AND sat_inv_dt <= '''+ @TD +''' );'                  
   End                  
   Else                  
   Begin                  
                  
        SET @sqltxt1 ='INSERT INTO #tmp ([Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN,       
        lgthDisplayFormat , entMst , invtCtl, width, satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf  ,blgQty,matVal , avgVal,customerNM,customerPCD )                  
            SELECT ''' +  @DB + ''', sat_shpt_brh, sat_inv_dt,brh_brh_nm,sat_frm,sat_grd,sat_size,sat_fnsh,sat_ef_evar,prm_dim_seg,sat_dim_dsgn,prm_lgth_disp_fmt,      
    sat_ent_msr,frm_invt_ctl,sat_wdth,sat_lgth,sat_ga_size,sat_idia,sat_odia,sat_ga_typ,sat_ord_wgt_um,sat_sls_qlf,      
    CASE      
WHEN sat_sls_qlf = ''T'' AND      
    (select COUNT(*) from ' + @DB + '_sahsax_rec, ' + @DB + '_orrprs_rec      
where sax_cmpy_id = sat_cmpy_id AND      
sax_sls_qlf = sat_sls_qlf AND      
sax_upd_ref_no = sat_upd_ref_no AND      
 sax_upd_ref_itm = sat_upd_ref_itm AND      
DATEPART(YEAR,sax_upd_dt) = sat_upd_cy AND      
DATEPART(MONTH,sax_upd_dt) = sat_upd_mth  AND      
DATEPART(DAY,sax_upd_dt) = sat_upd_dy AND      
prs_cmpy_id = sax_cmpy_id AND      
prs_prs = sax_prs AND      
prs_prs_cl in (''AC'', ''IN'', ''PG'', ''PK'', ''UP'')      
 )> 0      
 THEN 0      
ELSE sat_blg_qty      
END      
 ,      
     sat_tot_mtl_val* '+ @CurrenyRate +' ,      
    sat_mpft_avg_val* '+ @CurrenyRate +' ,    
    cus_cus_long_nm ,cva_pcd    
FROM      
      ' + @DB + '_sahsat_rec,      
    ' + @DB + '_arrshp_rec,      
    ' + @DB + '_scrbrh_rec,      
    ' + @DB + '_inrprm_rec,      
    ' + @DB + '_inrfrm_rec ,    
     ' + @DB + '_arrcus_rec ,  
     ' + @DB + '_scrcva_rec    
WHERE            
      
       sat_cmpy_id      =   shp_cmpy_id      
AND        sat_sld_cus_id   =   shp_cus_id      
AND        sat_shp_to       =   shp_shp_to      
      
AND        sat_cmpy_id      =   brh_cmpy_id      
AND        sat_shpt_brh     =   brh_brh      
      
AND        prm_frm          =   sat_frm      
AND        prm_grd          =   sat_grd      
AND        prm_size         =   sat_size      
AND        prm_fnsh         =   sat_fnsh      
      
AND        sat_frm          =   frm_frm      
    
and  cus_cmpy_id = sat_cmpy_id    
and cus_cus_id = sat_sld_cus_id    
and cva_cmpy_id = sat_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = sat_sld_cus_id  
--and  cva_addr_no = 0   
and cva_addr_typ = ''L''  
   and ( sat_inv_dt >= '''+ @FD +''' AND sat_inv_dt <= '''+ @TD +''' );'                   
   End                  
                            
     print(@sqltxt1)                   
    --set @execSQLtxt = @sqltxt;                   
   EXEC (@sqltxt1 );                  
   END                  
                     
--select [Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN,       
--        lgthDisplayFormat , entMst , invtCtl, width, satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf  ,sum(blgQty) as blgQty,sum(matVal) as matVal,      
--         sum(avgVal) as avgVal from #tmp                    
-- where (form = '' + @Form + '' OR '' + @Form + '' = '')                 
--and (grade = '' + @Grade + '' OR '' + @Grade + '' = '')                
--and (size = '' + @Size + '' OR '' + @Size + '' = '')      
--and (finish = '' + @Finish + '' OR '' + @Finish + '' = '')         
--group by [Database],  branch , invoiceDate , branchLongName , form , grade , size , finish , efEvar, dimSeg , dimDSGN,       
--        lgthDisplayFormat , entMst , invtCtl, width, satLength , gaSize , idia , odia,gaType  ,WeightUM ,slsQlf;     
            
                    
select branch , branchLongName ,customerNM,customerPCD, WeightUM ,sum(blgQty) as blgQty,sum(matVal) as matVal,      
         sum(avgVal) as avgVal from #tmp                    
 where (form = '' + @Form + '' OR '' + @Form + '' = '')                 
and (grade = '' + @Grade + '' OR '' + @Grade + '' = '')                
and (size = '' + @Size + '' OR '' + @Size + '' = '')      
and (finish = '' + @Finish + '' OR '' + @Finish + '' = '')         
group by  branch ,  branchLongName ,customerNM,customerPCD,WeightUM     
            
   drop table #tmp;                  
                     
END                  
-- @DBNAME varchar(50), @Form varchar(6), @Grade varchar(8), @Size varchar(15), @Finish varchar(8),      
  -- @FromDate datetime, @ToDate datetime                    
-- exec [sp_itech_ProductQtySales] 'US','TIPL','64E', '','','2019-11-01' ,'2019-11-30'                
                  
/*                
Date:20191126               
Mail Sub: New Report      
*/
GO
