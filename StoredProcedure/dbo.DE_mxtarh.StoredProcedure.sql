USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_mxtarh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


      
      
-- =============================================      
-- Author:  <Author,Sumit>      
-- Create date: <Create Date,01/25/2021,>      
-- Description: <Description,Document Archive,>      
      
-- =============================================      
CREATE PROCEDURE [dbo].[DE_mxtarh]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
Truncate table dbo.DE_mxtarh_rec ;       
      
-- Insert statements for procedure here      
insert into dbo.DE_mxtarh_rec (arh_cmpy_id,arh_arch_ver_No,arh_bus_Doc_Typ,arh_gen_dtts,arh_prim_ref)    
SELECT arh_cmpy_id,arh_arch_ver_No,arh_bus_Doc_Typ,arh_gen_dtts,arh_prim_ref       
  from [LIVEDESTX].[livedestxdb].[informix].[mxtarh_rec] ;       
        
END 
/*
20210413		Sumit
out of memory error
select and insert usable columns only
*/
GO
