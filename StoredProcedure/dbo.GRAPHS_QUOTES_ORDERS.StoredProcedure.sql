USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[GRAPHS_QUOTES_ORDERS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/6/2012,>
-- Description:	<Description,Total quotes and order last 13 months, added Norway>
-- =============================================
CREATE PROCEDURE [dbo].[GRAPHS_QUOTES_ORDERS]
	@EndDate datetime,
	@Branch varchar(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    
 CREATE TABLE #OPEN
(
category varchar(20),
total int,
acct_date varchar(15),
branch varchar(3)
)

/*BOOKING US*/
SELECT a.mbk_ord_no 
INTO #QUOTE1
FROM .[US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN .[US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_brh =@Branch
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,a.mbk_brh
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND c.cuc_desc30<>'Interco'
AND a.mbk_wgt > 0 
AND a.mbk_brh =@Branch
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE1)
GROUP BY a.mbk_brh, CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC

/*BOOKING UK*/
SELECT a.mbk_ord_no
INTO #QUOTE2
FROM [UK_ortmbk_rec] a
INNER JOIN [UK_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_brh =@Branch
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,a.mbk_brh
FROM [UK_ortmbk_rec] a
INNER JOIN [UK_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND c.cuc_desc30<>'Interco'
AND a.mbk_brh =@Branch
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE2)
GROUP BY a.mbk_brh, CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC

/*BOOKING CANADA*/
SELECT a.mbk_ord_no 
INTO #QUOTE3
FROM [CA_ortmbk_rec] a
INNER JOIN [CA_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_brh =@Branch
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,a.mbk_brh
FROM [CA_ortmbk_rec] a
INNER JOIN [CA_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND a.mbk_brh =@Branch
AND c.cuc_desc30<>'Interco'
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE3)
GROUP BY a.mbk_brh, CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC

/*BOOKING TAIWAN*/
SELECT a.mbk_ord_no
INTO #QUOTE4
FROM [TW_ortmbk_rec] a
INNER JOIN [TW_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_brh =@Branch
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,a.mbk_brh
FROM [TW_ortmbk_rec] a
INNER JOIN [TW_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND a.mbk_brh =@Branch
AND c.cuc_desc30<>'Interco'
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE4)
GROUP BY a.mbk_brh, CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC

/*BOOKING NORWAY*/
SELECT a.mbk_ord_no
INTO #QUOTE7
FROM [NO_ortmbk_rec] a
INNER JOIN [NO_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_brh =@Branch
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,a.mbk_brh
FROM [NO_ortmbk_rec] a
INNER JOIN [NO_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND a.mbk_brh =@Branch
AND c.cuc_desc30<>'Interco'
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE7)
GROUP BY a.mbk_brh, CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC


/*HPM*/
SELECT a.mbk_ord_no 
INTO #QUOTE5
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_grd IN ('13-8','15-5','1537','316L','316LVM', '625', '625 PLUS','6B','718','S240')
                                AND a.mbk_wgt > 0
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,'HPM' 
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND a.mbk_grd IN ('13-8','15-5','1537','316L','316LVM', '625', '625 PLUS','6B','718','S240')
AND c.cuc_desc30<>'Interco'
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE5)
                          
GROUP BY CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC


/*Industrial*/
SELECT a.mbk_ord_no 
INTO #QUOTE6
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
                          WHERE a.mbk_ord_pfx='SO'
                                AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
                                AND a.mbk_trs_md='A'
                                AND a.mbk_ord_itm<>999 
                                AND c.cuc_desc30<>'Interco'
                                AND a.mbk_wgt > 0 
                                AND a.mbk_frm IN ('TIFT','TISP','TIWP')
INSERT INTO #OPEN
SELECT DISTINCT 'Quote' AS item,COUNT(a.mbk_ord_no), CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,'IDS'
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat

WHERE
a.mbk_trs_md='A'
AND a.mbk_ord_itm<>999 
AND a.mbk_wgt > 0 
AND a.mbk_frm IN ('TIFT','TISP','TIWP')
AND c.cuc_desc30<>'Interco'
AND a.mbk_ord_pfx='QT'
AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.mbk_ord_no NOT IN (SELECT mbk_ord_no FROM #QUOTE6)
                                
GROUP BY CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE)
ORDER BY TMP ASC


/*Order*/
INSERT INTO #OPEN
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,a.cus_admin_brh
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,a.cus_admin_brh
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,a.cus_admin_brh
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,a.cus_admin_brh
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,a.cus_admin_brh
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,'HPM'
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND b.sat_grd IN ('13-8','15-5','1537','316L','316LVM', '625', '625 PLUS','6B','718','S240')
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh
UNION
SELECT DISTINCT 'Order', COUNT(b.sat_upd_ref),CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP,'IDS'
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND b.sat_frm IN ('TIFT','TISP','TIWP')
AND a.cus_admin_brh=@Branch
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),a.cus_admin_brh


/*New and Lost accounts*/
/*US*/
INSERT INTO #OPEN
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [US_arrcus_rec] a
INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
AND a.cus_admin_brh <>'TAI'
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

INSERT INTO #OPEN /*Prior 18 month to get 12 months lost sales*/
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [US_arrcus_rec] a
	INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

/*UK*/	
INSERT INTO #OPEN
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [UK_arrcus_rec] a
INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

INSERT INTO #OPEN
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [UK_arrcus_rec] a
	INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

/*CANADA*/	
INSERT INTO #OPEN
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [CA_arrcus_rec] a
INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

INSERT INTO #OPEN
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [CA_arrcus_rec] a
	INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC
/*TAIWAN*/
INSERT INTO #OPEN
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [TW_arrcus_rec] a
INNER JOIN [TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

INSERT INTO #OPEN
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [TW_arrcus_rec] a
INNER JOIN	[TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

/*NORWAY*/
INSERT INTO #OPEN
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [NO_arrcus_rec] a
INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND a.cus_admin_brh=@Branch
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC

INSERT INTO #OPEN
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
a.cus_admin_brh
FROM [NO_arrcus_rec] a
INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND a.cus_admin_brh=@Branch
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,a.cus_admin_brh
ORDER BY TMP ASC
    
/*Domestic and International New and lost accounts*/
    
CREATE TABLE #ACCT
(
category varchar(20),
total int,
acct_date varchar(15),
branch varchar(3)
)
INSERT INTO #ACCT
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
'DOM'
FROM [US_arrcus_rec] a
INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
ORDER BY TMP ASC

INSERT INTO #ACCT /*Prior 18 month to get 12 months lost sales*/
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
'DOM'
FROM [US_arrcus_rec] a
	INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
ORDER BY TMP ASC

/*UK*/	
INSERT INTO #ACCT
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [UK_arrcus_rec] a
INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
ORDER BY TMP ASC

INSERT INTO #ACCT
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [UK_arrcus_rec] a
	INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
ORDER BY TMP ASC

/*CANADA*/	
INSERT INTO #ACCT
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
'DOM'
FROM [CA_arrcus_rec] a
INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
ORDER BY TMP ASC

INSERT INTO #ACCT
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
'DOM'
FROM [CA_arrcus_rec] a
	INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
ORDER BY TMP ASC
/*TAIWAN*/
INSERT INTO #ACCT
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [TW_arrcus_rec] a
INNER JOIN [TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
ORDER BY TMP ASC

INSERT INTO #ACCT
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [TW_arrcus_rec] a
INNER JOIN [TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
ORDER BY TMP ASC

/*NORWAY*/
INSERT INTO #ACCT
SELECT 'New Accounts', COUNT(b.coc_frst_sls_dt),
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [NO_arrcus_rec] a
INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
ORDER BY TMP ASC

INSERT INTO #ACCT
SELECT  'Lost Accounts',COUNT(b.coc_lst_sls_dt),
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
'INT'
FROM [NO_arrcus_rec] a
INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
ORDER BY TMP ASC

SELECT category, SUM(total) as total, acct_date, branch
FROM #ACCT 
WHERE branch=@Branch
GROUP BY branch, category, acct_date
UNION
SELECT * 
FROM #OPEN WHERE branch=@Branch
ORDER BY category, acct_date ASC

DROP TABLE #OPEN
DROP TABLE #ACCT
DROP TABLE #QUOTE1
DROP TABLE #QUOTE2
DROP TABLE #QUOTE3
DROP TABLE #QUOTE4
DROP TABLE #QUOTE5
DROP TABLE #QUOTE6
DROP TABLE #QUOTE7

END




GO
