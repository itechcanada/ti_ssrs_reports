USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysis_with_YTD]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <MRinal >  
-- Create date: <07 sep 2015>  
-- Last Updated By: mrinal      
-- Last updated Description:   
-- Last Updated Date:   
-- last updated by :  
-- =============================================  
-- Description: <Getting top 50 customers for SSRS reports>  
  
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysis_with_YTD] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65), @Year as varchar(5), @Branch varchar(10)   
  
AS  
BEGIN  


   
 SET NOCOUNT ON;  
   
declare @sqltxt varchar(7000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
DECLARE @FromYear varchar(10)  
DECLARE @ToYear varchar(10)  
  
set @DB=  @DBNAME  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
set @FromYear = CONVERT(VARCHAR(10), '01-01-' + @Year , 120)  
set @ToYear = CONVERT(VARCHAR(10), '12-31-' +@Year , 120)  
print @Year  + ' ' +@FromYear  +'  '+ @ToYear  
  
CREATE TABLE #tmp (   CustID   VARCHAR(10)  
        , CustName     VARCHAR(65)  
        , Market   VARCHAR(65)   
        , ShpBrh  Varchar(3)  
        , TotWeight  DECIMAL(20, 0)  
        , WeightUM           Varchar(10)  
                    , TotalValue    DECIMAL(20, 2)  
        , NetGP               DECIMAL(20, 0)  
        , TotalMatValue    DECIMAL(20, 0)  
        , MatGP    DECIMAL(20, 0)  
        , Databases   VARCHAR(15)   
          
        ,CustAdd VARCHAR(170)   
        ,City  VARCHAR(40)  
        ,StateProv Varchar(5)  
        ,Country Varchar(5)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)  
                 );   
  
CREATE TABLE #tmpDynamicYear (   CustID   VARCHAR(10)         
        , Market   VARCHAR(65)     
        , ShpBrh  Varchar(3)   
        , TotalValue    DECIMAL(20, 2)      
        , NetGP               DECIMAL(20, 0)  
        , TotalMatValue    DECIMAL(20, 0)  
        , MatGP    DECIMAL(20, 0)  
        , Databases   VARCHAR(15)   
        , WeightUM           Varchar(10)  
                 );   
                   
                  
                   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
DECLARE @CurrenyRate varchar(15);  
  
if @Market ='ALL'  
 BEGIN  
 set @Market = ''  
 END  
  
if @Branch = 'ALL'  
BEGIN  
set @Branch = ''  
END  
  
IF @DBNAME = 'ALL'  
 BEGIN  
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);    
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
       IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End   
    Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End   
      
       
      SET @query =  'INSERT INTO #tmp (CustID, CustName,Market,ShpBrh,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,Databases, CustAdd, City, StateProv, Country,SalesPresonIs,SalesPresonOs)       
       select stn_sld_cus_id  as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh,  
      SUM(stn_blg_wgt) as TotWeight, '' '',   
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,   
       '''+ @Name +''' as databases  
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))               
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),shp_is_slp, shp_os_slp   
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec          
       on cus_cus_id = stn_sld_cus_id left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0   
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat     
       JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id  
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
        group by  stn_shpt_brh,stn_sld_cus_id, cus_cus_nm, cuc_desc30,   
       cuc_cus_cat, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))         
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),shp_is_slp, shp_os_slp  
       '  
         
         
        EXECUTE sp_executesql @query;  
        print(@query)   
          
          
          
        -- For dynamic year  
     
   SET @query ='INSERT INTO #tmpDynamicYear (CustID,Market,ShpBrh,TotalValue,NetGP,TotalMatValue,MatGP,Databases,WeightUM)       
        select stn_sld_cus_id,cuc_desc30 as Market, stn_shpt_brh,  
        
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,   
       '''+ @Name +''' as databases,'' ''  
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec          
       on cus_cus_id = stn_sld_cus_id  
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
       where stn_inv_Dt >='''+ @FromYear +''' and stn_inv_dt <='''+ @ToYear +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
        group by  stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30 --,stn_ord_wgt_um  
       order by stn_sld_cus_id ;'  
    EXECUTE sp_executesql @query;  
        print(@query)   
      
          
           
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
         
       /*  
       EXECUTE sp_executesql @query;  
          
          
        print(@query)  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
         
       */  
         
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
         
    Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')    
    print('2:' + @Name);  
    print('3:' + @DBNAME);  
     
    IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End  
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End  
       
     SET @sqltxt ='INSERT INTO #tmp (CustID, CustName,Market,ShpBrh,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,Databases, CustAdd, City, StateProv, Country,SalesPresonIs,SalesPresonOs)       
        select stn_sld_cus_id  as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh,  
      SUM(stn_blg_wgt) as TotWeight, '' '',   
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,   
       '''+ @Name +''' as databases  
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))               
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),shp_is_slp, shp_os_slp   
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec          
       on cus_cus_id = stn_sld_cus_id left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0   
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat     
       JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id  
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
        group by  stn_shpt_brh,stn_sld_cus_id, cus_cus_nm, cuc_desc30,   
       cuc_cus_cat, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))         
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),shp_is_slp, shp_os_slp  
       '  
    print(@sqltxt)  ;  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
   -- For dynamic year  
     
   SET @sqltxt ='INSERT INTO #tmpDynamicYear (CustID,Market,ShpBrh,TotalValue,NetGP,TotalMatValue,MatGP,Databases,WeightUM)       
        select stn_sld_cus_id,cuc_desc30 as Market, stn_shpt_brh,  
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,   
       '''+ @Name +''' as databases,'' ''  
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec          
       on cus_cus_id = stn_sld_cus_id  
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
       where stn_inv_Dt >='''+ @FromYear +''' and stn_inv_dt <='''+ @ToYear +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
        group by  stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30 --,stn_ord_wgt_um  
       order by stn_sld_cus_id ;'  
    print(@sqltxt)  ;  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
     
     
     
     
     END  
      
      
        SELECT RTRIM(LTrim(a.CustID))+'-'+a.Databases as CustID, a.CustName,a.Market,  
      case a.Databases when 'UK' then (a.TotWeight * 2.20462) when 'Norway' then (a.TotWeight * 2.20462) else a.TotWeight end as TotWeight,  
        'LBS' as WeightUM,  
        a.TotalValue,a.NetGP,a.TotalMatValue,a.MatGP,a.Databases,a.CustAdd,a.City,a.StateProv,a.Country, b.TotalValue as PrevTotalValue, b.NetGP as PrevNetGP,  
        b.TotalMatValue as PrevTotalMatValue, b.MatGP AS PrevMatGP, a.SalesPresonIs, a.SalesPresonOs, a.ShpBrh, (a.TotalValue - b.TotalValue) AS Change  
         FROM #tmp  a  
       LEFT JOIN #tmpDynamicYear b ON a.CustID = b.CustID AND a.Market = b.Market AND a.ShpBrh = b.ShpBrh AND a.Databases = b.Databases --and a.WeightUM = b.WeightUM  
       WHERE a.ShpBrh not in ('SFS')
        order by a.TotalValue desc   
       
     --select * FROM #tmpDynamicYear  
       
     drop table #tmp  
     drop table #tmpDynamicYear  
END  
  -- exec #sp_itech_SalesAnalysis_with_YTD '3/25/2016', '05/25/2016' , 'US','ALL','2015','ALL'; 
-- exec sp_itech_SalesAnalysis_with_YTD '07/01/2015', '07/31/2015' , 'ALL','ALL','2014';  
-- exec sp_itech_SalesAnalysis_with_YTD '4/19/2015', '08/31/2015' , 'US','ALL','2013','SEA';  
  /*
CHANGES:-
20160525 :-
 option out PSM and especially SFS when all databases is selected
 
 SOLUTION:- 
 NitBranch not in ('SFS') and NitWhs not in ('SFS') 
*/ 
GO
