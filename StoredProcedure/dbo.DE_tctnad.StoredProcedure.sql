USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_tctnad]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Oct 29, 2020>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_tctnad]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.DE_tctnad_rec', 'U') IS NOT NULL  
  drop table dbo.DE_tctnad_rec ;   
  
    -- Insert statements for procedure here  
SELECT nad_cmpy_id,nad_Ref_pfx,nad_Ref_no,nad_Ref_itm, nad_addr_typ,nad_pcd, nad_nm1   
into  dbo.DE_tctnad_rec  
  from [LIVEDESTX].[livedestxdb].[informix].[tctnad_rec] ;   
    
END  
-- select * from DE_tctnad_rec  
GO
