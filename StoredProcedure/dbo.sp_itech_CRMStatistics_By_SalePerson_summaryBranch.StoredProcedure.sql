USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CRMStatistics_By_SalePerson_summaryBranch]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                        
-- Author:  <Mukesh >                        
-- Create date: <27 Oct 2016>                        
-- Modified by: <Mukesh>                       
                      
-- =============================================                        
CREATE PROCEDURE [dbo].[sp_itech_CRMStatistics_By_SalePerson_summaryBranch] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(50),@SalePerson as Varchar(65)                       
                        
AS                        
BEGIN                        
                         
 SET NOCOUNT ON;               
             
  
               
declare @sqltxtAct varchar(6000)                        
declare @execSQLtxtAct varchar(7000)                        
declare @sqltxtTsk varchar(6000)                        
declare @execSQLtxtTsk varchar(7000)                        
declare @DB varchar(100)                        
declare @FD varchar(10)                        
declare @TD varchar(10)                        
                        
set @DB=  @DBNAME                        
              
IF @Branch = 'ALL'                        
 BEGIN                        
  set @Branch = ''                        
 END                          
                         
 IF @SalePerson = 'ALL'                        
 BEGIN                        
  set @SalePerson = ''                        
 END                        
                         
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                        
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                        
                        
CREATE TABLE #tmpAct ([Database]   VARCHAR(10)              
  ,Branch Varchar(10)                      
     , LoginID   VARCHAR(50)                        
                      
    -- , MonthYear   VARCHAR(10)                        
        , ActDormant  integer                        
        , ActExisting  integer                        
        , ActProspect  integer                        
        , TskDormant  integer                        
        , TskExisting  integer                        
        , TskProspect  integer                        
        , ActCallDormant  integer                        
        , ActCallExisting  integer                        
        , ActCallProspect  integer                        
        , TskCallDormant  integer                        
        , TskCallExisting  integer                        
        , TskCallProspect  integer                        
                 );                         
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)                
     ,Branch Varchar(10)                             
     , LoginID   VARCHAR(50)                   
      , SalesPersonInitial   VARCHAR(50)                        
    -- , MonthYear   VARCHAR(10)                        
        , ActDormant  integer                        
        , ActExisting  integer                        
        , ActProspect  integer                        
        , TskDormant  integer                        
        , TskExisting  integer                        
        , TskProspect  integer                       
        , ActCallDormant  integer                        
        , ActCallExisting  integer                        
        , ActCallProspect  integer                        
        , TskCallDormant  integer                        
        , TskCallExisting  integer                        
        , TskCallProspect  integer                        
                 );                         
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @Prefix VARCHAR(35);                        
DECLARE @Name VARCHAR(15);                        
                        
IF @DBNAME = 'ALL'                        
 BEGIN                        
                            
    DECLARE ScopeCursor CURSOR FOR                          
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                  
   OPEN ScopeCursor;                          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
     WHILE @@FETCH_STATUS = 0                        
       BEGIN                        
        DECLARE @queryAct NVARCHAR(1500);                           
        DECLARE @queryTsk NVARCHAR(1500);                           
      SET @DB= @Prefix                          
                                
        SET @queryTsk =                        
              'INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting, TskCallProspect, TskDormant, TskExisting, TskProspect)                      
      Select * from (                       
      select ''' +  @Prefix + ''' as [Database], usr_usr_brh as branch , cta_tsk_asgn_to ,  rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                       
       from ' + @DB + '_cctcta_rec join                       
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1         
        join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to         
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                  
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR              
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                     
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')                
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                        
       group by  cta_tsk_asgn_to, usr_usr_brh ,  atp_desc30              
       ) AS Query1                        
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'                        
        EXECUTE sp_executesql @queryTsk;                        
        print(@queryTsk);                        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
       END                         
    CLOSE ScopeCursor;                        
    DEALLOCATE ScopeCursor;                        
  END                        
  ELSE                        
     BEGIN                            
 -- task                          
                            
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting, TskCallProspect, TskDormant, TskExisting, TskProspect)                      
      Select * from (                       
      select ''' +  @DB + ''' as [Database], usr_usr_brh as branch,  cta_tsk_asgn_to, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                       
       from ' + @DB + '_cctcta_rec join                       
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1        
         join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to         
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR              
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                        
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')                   
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                   
       group by  cta_tsk_asgn_to,usr_usr_brh, atp_desc30                    
       ) AS Query1        
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'                            
 
                            
    print(@sqltxtTsk)                        
    set @execSQLtxtTsk = @sqltxtTsk;                         
   EXEC (@execSQLtxtTsk);                        
     END                        
   -- SELECT * FROM #tmpAct Order by MonthYear desc, LoginID                    
   -- SELECT * FROM #tmpTsk Order by MonthYear desc, LoginID                        
   SELECT   [Database],  Branch,                   
    tsk.LoginID  AS LoginID,                    
    SUM(ISNULL(tsk.TskCallDormant,0)) AS TskCallDormant,                         
    Sum(ISNULL(tsk.TskCallExisting,0)) AS TskCallExisting,                        
    Sum(ISNULL(tsk.TskCallProspect,0)) AS TskCallProspect,                       
    Sum(ISNULL(tsk.TskDormant,0)) AS TskDormant,                         
    Sum(ISNULL(tsk.TskExisting,0)) AS TskExisting,                        
    Sum(ISNULL(tsk.TskProspect,0)) AS TskProspect                         
    FROM #tmpTsk as tsk group by [Database],  Branch,  LoginID                      
    order by  LoginID                        
                            
    Drop table #tmpAct Drop table #tmpTsk                        
END                        
                        
-- exec sp_itech_CRMStatistics_By_SalePerson_summaryBranch '08/30/2019', '09/05/2019' , 'ALL','ROS' ,'ALL'             
-- exec sp_itech_CRMStatistics_By_SalePerson_summary '07/05/2015', '08/04/2015' , 'ALL', 'bvalerie'                
/*              
20171221              
Mail Sub: CRM Data Issue        
      
20190904      
Mail Sub:DMS Report - LAX Branch Update Request       
join                      
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id    
20200710	Sumit
Mail : RE: CRM By SalePerson Summary.xlsx  
Modify Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account 
*/ 
GO
