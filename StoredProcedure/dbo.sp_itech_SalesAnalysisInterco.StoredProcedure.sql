USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisInterco]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <27 Jun 2016>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisInterco] @FromDate datetime, @ToDate datetime, @Warehouse as varchar(5),@DateRange Varchar(3) = 'NON'

AS
BEGIN

	
	SET NOCOUNT ON;
	
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)
DECLARE @IsExcInterco char(1)

IF (@DateRange = 'NON')
BEGIN
		set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
		set @TD = CONVERT(VARCHAR(10), @ToDate , 120)
END
ELSE IF (@DateRange = 'EOM')
BEGIN
		set @FD = CONVERT(VARCHAR(10),DATEADD(Month,DateDiff(Month,0,GetDATE()),0), 120) -- first date of current month
		set @TD = CONVERT(VARCHAR(10),DATEADD(Day, -1, DATEADD(Month,DateDiff(Month,0,GetDATE())+1,0)), 120) -- Last Date of Current Month
END
ELSE
BEGIN
		set @FD = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear    
		set @TD = CONVERT(VARCHAR(10),GETDATE(), 120)  --TodayOFCurrentYear
END

CREATE TABLE #tmp (   CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					,SalesPerson	VARCHAR(4)
   					, Market	  VARCHAR(65) 
   					, Warehouse		 VARCHAR(65) 
   					, Form           Varchar(65)
   					, Grade           Varchar(65)
   					, TotWeight		DECIMAL(20, 2)
   					, WeightUM           Varchar(10)
                    , TotalValue  	 DECIMAL(20, 2)
   					, NetGP               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, MatGP			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15) 
   					, MarketID	  integer
   					,stn_cry   varchar(5)
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);
DECLARE @CurrenyRate varchar(15);
if @Warehouse ='ALL' 
 BEGIN
  set @Warehouse = ''
 END

				 
								SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPerson,CustName,Market,Warehouse,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID,stn_is_slp, cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(stn_shpg_whs,''SFS'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,

								SUM(stn_tot_val ) as TotalValue, 
								SUM(stn_npft_avg_val ) as NetGP,
								SUM(stn_tot_mtl_val ) as TotalMatValue , 
								SUM(stn_mpft_avg_val ) as MatGP,
								  cuc_cus_cat as MarketID, '' US '' as databases,stn_cry
								 from US_sahstn_rec 
								 join US_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join US_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (stn_shpg_whs = '''+ @Warehouse +''' or '''+ @Warehouse +'''= '''')
								  
								  group by  stn_sld_cus_id, stn_is_slp,cus_cus_nm, cuc_desc30, stn_shpg_whs, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry
								 order by stn_sld_cus_id '
						print(@sqltxt)
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     


   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPerson,CustName,Market,Warehouse,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry,
    (Case SUM(ISNULL(TotalValue,0)) When 0 then 0 else (SUM(ISNULL(NetGP,0))/SUM(ISNULL(TotalValue,0))*100) end ) as 'NPPct'
     FROM #tmp where CustID in ('10993','11980','12896','2046','3330','5833')
   group by CustID,SalesPerson, CustName,Market,Warehouse,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry
   order by CustID 
END
-- exec [sp_itech_SalesAnalysisInterco] '2016-06-01', '2016-06-27' , 'ALL'
-- exec [sp_itech_SalesAnalysisInterco_NP] '2016-06-01', '2016-06-27' , 'ALL'




GO
