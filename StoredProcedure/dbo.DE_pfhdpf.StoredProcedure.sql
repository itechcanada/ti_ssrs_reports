USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_pfhdpf]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Author,Sumit>          
-- Create date: <Create Date,Jan 27 2021,>          
-- Description: <Description,Open Orders,>          
          
-- =============================================          
CREATE PROCEDURE [dbo].[DE_pfhdpf]          
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 -- prob in dpf_part column        
 SET NOCOUNT ON;   
IF OBJECT_ID('dbo.DE_pfhdpf_rec', 'U') IS NOT NULL            
  drop table dbo.DE_pfhdpf_rec;            
                
                    
SELECT *            
into  dbo.DE_pfhdpf_rec   
FROM [LIVEDESTX].[livedestxdb].[informix].[pfhdpf_rec]  ;       
        
          
END 
GO
