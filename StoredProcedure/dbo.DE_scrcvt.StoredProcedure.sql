USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrcvt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_scrcvt]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
truncate table dbo.DE_scrcvt_rec ;   
   
Insert into dbo.DE_scrcvt_rec   
  
    -- Insert statements for procedure here  
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[scrcvt_rec]  
  
  
END  
  
  
  
  
GO
