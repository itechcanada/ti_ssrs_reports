USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_TaskTypeGraph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mayank >    
-- Create date: <11 Feb 2013>    
-- Modified date : <14-05-15> 
-- Modified By : Mrinal Jha
-- Description: <Changed query as per new document, since we lost some table>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_TaskTypeGraph] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch as varchar(65), @version char = '0'    
    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
declare @sqltxt varchar(6000)    
declare @execSQLtxt varchar(7000)    
declare @DB varchar(100)    
declare @FD varchar(10)    
declare @TD varchar(10)    
declare @NOOfCust varchar(15)    
DECLARE @ExchangeRate varchar(15)    
    
set @DB=  @DBNAME    
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)    
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)    
    
CREATE TABLE #tmp (   Branch   VARCHAR(10)    
        , Types     VARCHAR(200)    
        , Counts   integer    
                 );     
    
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(35);    
DECLARE @Name VARCHAR(15);    
    
if @Branch ='ALL'    
 BEGIN    
 set @Branch = ''    
 END    
    
IF @DBNAME = 'ALL'    
 BEGIN    
   
 IF @version = '0'      
    BEGIN      
    DECLARE ScopeCursor CURSOR FOR      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName      
   OPEN ScopeCursor;      
    END      
    ELSE      
    BEGIN      
    DECLARE ScopeCursor CURSOR FOR      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS       
   OPEN ScopeCursor;      
    END    
    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(1500);       
      SET @DB= @Prefix     
      SET @query ='INSERT INTO #tmp (Branch, Types,Counts)     
         select cus_admin_brh, atp_desc30, COUNT(*) as counts from ' + @DB + '_cctcta_rec join       
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1  join  
       ' + @DB + '_arrcus_rec on cus_cmpy_id = cta_cmpy_id and cta_crmacct_id = cus_cus_id   
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))     
       And (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, atp_desc30 '  
       print @query;   
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
     SET @sqltxt ='INSERT INTO #tmp (Branch, Types,Counts)     
         select cus_admin_brh, atp_desc30, COUNT(*) as counts from ' + @DB + '_cctcta_rec join       
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1  join  
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id   
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))    
       And (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, atp_desc30 '   
       print(@sqltxt)    
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
     END    
   SELECT * FROM #tmp     
   DROP TABLE #tmp  
END    
    
-- exec sp_itech_TaskTypeGraph '01/01/2012', '1/31/2013' , 'ALL','Industrial'    
-- exec sp_itech_TaskTypeGraph '04/01/2015', '04/30/2015' , 'ALL','ALL','1'    
    
    
    
GO
