USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itechDeleteTransLogfile]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itechDeleteTransLogfile]
	
AS
BEGIN


-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE Stratix_US SET RECOVERY SIMPLE;

-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (Stratix_US_Log, 1);

-- Reset the database recovery model.
ALTER DATABASE Stratix_US SET RECOVERY FULL;
END

-- exec sp_itechDeleteTransLogfile;
GO
