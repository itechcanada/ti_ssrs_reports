USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_aprven]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[UK_aprven]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.UK_aprven_rec', 'U') IS NOT NULL          
  drop table dbo.UK_aprven_rec;          
              
                  
SELECT *          
into  dbo.UK_aprven_rec   
FROM [LIVEUKSTX].[liveukstxdb].[informix].[aprven_rec]  where ven_ven_id <> '528'   
    
    
END    
    
GO
