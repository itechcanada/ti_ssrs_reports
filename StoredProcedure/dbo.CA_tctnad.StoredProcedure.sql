USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_tctnad]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 6, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[CA_tctnad]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_tctnad_rec', 'U') IS NOT NULL
		drop table dbo.CA_tctnad_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CA_tctnad_rec
  from [LIVECASTX].[livecastxdb].[informix].[tctnad_rec] ; 
  
END
-- select * from CA_arrcuc_rec 
GO
