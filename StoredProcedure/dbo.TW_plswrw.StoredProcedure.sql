USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_plswrw]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 23, 2014,>  
-- Description: <Description,  Warehouse Rmnt Weekly Data>  
  
-- =============================================  
Create PROCEDURE [dbo].[TW_plswrw]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_plswrw_rec', 'U') IS NOT NULL  
  drop table dbo.TW_plswrw_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_plswrw_rec  
  from [LIVETW_IW].[livetwstxdb_iw].[informix].[plswrw_rec] ;   
    
END  
-- select * from TW_plswrw_rec order by wrw_run_dt desc
GO
