USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTop40CustomerModOnDec20]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <26 Feb 2013>
-- Description:	<Getting top 40 customers for SSRS reports>
--Last Changed: Change the from date it pick data from last 13 month  
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- Last updated by : Mukesh 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetTop40CustomerModOnDec20] @DBNAME varchar(50),@Location int, @ReportType int,@Market varchar(65),@Branch varchar(10)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @DB=  @DBNAME 

IF (@ReportType=1)
	 BEGIN
	   set @FD = CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0), 120)  --FirstDateOfPreviousYear
	   set @TD = CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)), 120)  --LastdateOFPreviousYear
	 End
	 Else
	 BEGIN
	   set @FD = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear
	   set @TD = CONVERT(VARCHAR(10),GETDATE(), 120)  --TodayOFCurrentYear
	 End

print(@FD)
print(@TD)
CREATE TABLE #tmp1 (    CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
                    , TotalValue  	 DECIMAL(20, 2)
   					, NProfitPercentage               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, GProfitPercentage			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15)
   					,Cry           Varchar(5)
   					, TotalNetProfitValue  	 DECIMAL(20, 2)
   					, TotalMatProfitValue  	 DECIMAL(20, 2)   					
   	               );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(5);
DECLARE @Name VARCHAR(15);

if @Market ='ALL'
 BEGIN
 set @Market = ''
 END

if @Branch ='ALL'
 BEGIN
 set @Branch = ''
 END

DECLARE @CurrenyRate varchar(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  				SET @DB= @Prefix
  			--	print(@DB)
  			   DECLARE @query NVARCHAR(4000);
  			    IF (UPPER(@Prefix) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@Prefix) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if(UPPER(@Prefix) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@Prefix) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
  			   
  			   SET @query = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry, TotalNETProfitValue, TotalMatProfitValue)
							select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh as Branch,'
  				if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')		
					 BEGIN
					 SET @query= @query +   '   Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, AVG(stn_npft_avg_pct) as NProfitPercentage, 
												Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
												AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,
												Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as TotalNETProfitValue,
												Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalMatProfitValue
												from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
												left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
												Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
												where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' 
												and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
												and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'
												--if @Market =''
												--BEGIN
												-- SET @query= @query + ' and cuc_desc30 <> ''Interco'''
												--END
												
					 END
					 Else
					 BEGIN
					 SET @query= @query +   ' Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as TotalValue, 
												AVG(stn_npft_avg_pct) as NProfitPercentage, 
												Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
												AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,
												case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else  SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalNETProfitValue, 												
												Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalMatProfitValue 
												from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
												left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
												Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
												where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' 
												and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
												and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'
												--if @Market =''
												--BEGIN
												-- SET @query= @query + ' and cuc_desc30 <> ''Interco'''
												--END
					  END	
					IF (@Location=1)
					 BEGIN
					 SET @query= @query +   ' group by stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_cry,crx_xexrt
							                    order by TotalValue desc '
					 END
					 Else
					 BEGIN
					 SET @query= @query +   ' group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt
							                    order by TotalValue desc '
					 
					 END
							
					print(@query)		
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			   IF (UPPER(@DBNAME) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@DBNAME) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if(UPPER(@DBNAME) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@DBNAME) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
				Else if(UPPER(@DBNAME) = 'TWCN')
				begin
				   SET @DB ='TW'
				   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
			  
			  if (UPPER(@DBNAME) = 'TWCN')
                   Set @Name='Taiwan - China'
              else
                   Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')
			  
			  --and cva_cty <> ''CHN''
			  --if  (UPPER(@DBNAME) = 'TW')
					--							BEGIN
					--	 SET @sqltxt= @sqltxt +   ' inner join ' + @DB + '_scrcva_rec on cva_cmpy_id = stn_cmpy_id 
					--							and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = stn_sld_cus_id  and cva_addr_no = 0 
					--							 and cva_addr_typ = ''L'' and CVA_REF_PFX=''CU'''
					--							END
					--							Else 
			  SET @sqltxt = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry, TotalNETProfitValue, TotalMatProfitValue)
							select  stn_sld_cus_id  as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,case when ('''+@DBNAME +''' = ''TWCN'') then ''TAI - SHA'' else stn_shpt_brh end as Branch,'
					if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' or UPPER(@DBNAME) = 'TWCN')			
					 BEGIN
					 SET @sqltxt= @sqltxt +   ' Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, AVG(stn_npft_avg_pct) as NProfitPercentage, 
												Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
												AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,
												Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as TotalNETProfitValue, 
												Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalMatProfitValue  
												from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
												left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
												Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' '
												
												if (UPPER(@DBNAME) = 'TWCN')
												BEGIN
						 SET @sqltxt= @sqltxt +   ' inner join ' + @DB + '_scrcva_rec on cva_cmpy_id = stn_cmpy_id 
												and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = stn_sld_cus_id  and cva_addr_no = 0 
												and cva_cty = ''CHN'' and cva_addr_typ = ''L'' and CVA_REF_PFX=''CU'''
												END
												
						 SET @sqltxt= @sqltxt +   ' where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''
												and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
												and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'
												--if @Market =''
												--BEGIN
												-- SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
												--END
					 END
					 Else
					 BEGIN
					 SET @sqltxt= @sqltxt +   ' Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as TotalValue, 
												AVG(stn_npft_avg_pct) as NProfitPercentage, 
												Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
												AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,
												Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else  SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalNETProfitValue, 
												Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalMatProfitValue
												from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
												left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
												Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
												where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' 
												and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
												and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'
												--if @Market =''
												--BEGIN
												-- SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
												--END
					  END	
				
				IF (@Location=1)
					 BEGIN
					 SET @sqltxt= @sqltxt +   ' group by stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_cry,crx_xexrt
							                    order by TotalValue desc '
					 END
					 Else
					 BEGIN
					 SET @sqltxt= @sqltxt +   ' group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt
							                    order by TotalValue desc '
					 
					 END
					
		print(@sqltxt)
		set @execSQLtxt = @sqltxt; 
		EXEC (@execSQLtxt);
     END
   
   
   if @Market =''
	BEGIN
	    SELECT  CustID, CustName,Market,Branch,SUM(TotalValue) as TotalValue, SUM(TotalNETProfitValue)/nullif(SUM(TotalValue), 0) * 100 as NProfitPercentage,
		sUM(TotalMatValue) as TotalMatValue,SUM(TotalMatProfitValue)/nullif(SUM(TotalMatValue), 0) * 100 as GProfitPercentage,Databases,'' as 'Cry' FROM #tmp1 
		where (Market <> 'Interco' OR Market is Null)
		group by CustID, CustName,Market,Branch,Databases
		order by TotalValue desc;
	END
	Else
	Begin
		SELECT  CustID, CustName,Market,Branch,SUM(TotalValue) as TotalValue, SUM(TotalNETProfitValue)/nullif(SUM(TotalValue), 0) * 100 as NProfitPercentage,
		sUM(TotalMatValue) as TotalMatValue,SUM(TotalMatProfitValue)/nullif(SUM(TotalMatValue), 0) * 100 as GProfitPercentage,Databases,'' as 'Cry' FROM #tmp1 
		group by CustID, CustName,Market,Branch,Databases
		order by TotalValue desc;
	End
   drop table #tmp1
END

-- exec sp_itech_GetTop40Customer 'UK',1,1,'ALL','ALL' ---Privious 
-- exec sp_itech_GetTop40Customer 'UK',1,0,'ALL','ALL'       --- current 
-- SELECT  CustID, CustName,Market,Branch,SUM(TotalValue) as TotalValue,avg(NProfitPercentage) as NProfitPercentage,
--sUM(TotalMatValue) as TotalMatValue,AVG(GProfitPercentage) as GProfitPercentage,Databases,'' as 'Cry' FROM #tmp1 
--group by CustID, CustName,Market,Branch,Databases
--order by CustID,TotalValue desc;
--SELECT    CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry
		 --FROM (SELECT
			-- ROW_NUMBER() OVER ( PARTITION BY Branch ORDER BY TotalValue DESC ) AS 'RowNumber',
			--CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry
		 -- FROM #tmp
		 -- ) dt
		 -- WHERE RowNumber <= 40


GO
