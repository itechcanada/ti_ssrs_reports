USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_potpoh]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: Feb 05, 2016    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[US_potpoh]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.US_potpoh_rec', 'U') IS NOT NULL    
  drop table dbo.US_potpoh_rec;    
        
            
SELECT *    
into  dbo.US_potpoh_rec    
FROM [LIVEUSSTX].[liveusstxdb].[informix].[potpoh_rec];    
    
END    
--  exec  US_potpoh    
-- select * from US_potpoh_rec
GO
