USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OPA_OpenOrder]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukesh>              
-- Create date: <14-06-2019>              
-- Description: <Booking to Invoice Ratio>             
          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_OPA_OpenOrder]  @DBNAME varchar(50), @Branch varchar(3)            
AS              
BEGIN              
            
 -- SET NOCOUNT ON added to prevent extra result sets from              
 SET NOCOUNT ON;              
declare @DB varchar(100);              
declare @sqltxt varchar(6000);              
declare @execSQLtxt varchar(7000);              
DECLARE @CountryName VARCHAR(25);                 
DECLARE @prefix VARCHAR(15);                 
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @CurrenyRate varchar(15);            
declare @FD varchar(10)                
declare @TD varchar(10)         
declare @TDInv varchar(10)                     
declare @FDOPA varchar(10)                
declare @TDOPA varchar(10) 

   set @FD = CONVERT(VARCHAR(10),  DATEADD(year, -1, GETDATE()),120)    -- One year back     
 set @TD = CONVERT(VARCHAR(10), DATEADD(year, 1, GETDATE()) ,120)      -- One year after   

 set @FDOPA = CONVERT(VARCHAR(10),  DATEADD(MONTH, -1, GETDATE()),120)    -- One year back     
 set @TDOPA = CONVERT(VARCHAR(10), DATEADD(MONTH, 1, GETDATE()) ,120)      -- One year after              
             
IF @Branch = 'ALL'              
 BEGIN              
  set @Branch = ''              
 END    
            
              
CREATE TABLE #temp ( CompanyId varchar(5)               
        ,OrderPrefix Varchar(2)              
        ,OrderNo varchar(10)              
        ,OrderItem   varchar(3)              
        ,CustID   VARCHAR(10)   
        , CustName     VARCHAR(65)    
        ,Branch varchar(10)  
        , DueDate    varchar(20)    
  ,BalWgt      DECIMAL(20, 2)    
        , ChargeValue    DECIMAL(20, 2)    
        , Salesperson     varchar(65)  
        ,Form  varchar(35)                
        ,Grade  varchar(35)                
        ,Size  varchar(35)                
        ,Finish  varchar(35)             
        , OrderType     varchar(65)    
     );             
  
create table #tempOPA (CompanyId varchar(5)  
 ,OrderPrefix Varchar(2)              
        ,OrderNo varchar(10)              
        ,OrderItem   varchar(3)    
		,JobSetNo  varchar(8)

);


              
IF @DBNAME = 'ALL'              
 BEGIN              
               
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
                
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  WHILE @@FETCH_STATUS = 0              
  BEGIN              
   DECLARE @query NVARCHAR(MAX);              
   IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End              
                  
    SET @query = 'INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, CustName,Branch, DueDate, BalWgt, ChargeValue, Salesperson,Form,Grade,Size,Finish, OrderType)    
  select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_sld_cus_id as CustID,  
        cus_cus_long_nm as CustName,ord_ord_brh as Branch,orl_due_to_dt as DueDate, orl_bal_wgt * 2.20462 as BalWgt, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,     
    usr_nm as Salesperson,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)),cds_desc  
       from ' + @Prefix + '_ortord_rec,' + @Prefix + '_arrcus_rec ,        
       ' + @Prefix + '_ortorh_rec ,' + @Prefix + '_ortorl_rec,            
        ' + @Prefix + '_tctipd_rec,   
       ' + @Prefix + '_ortchl_rec            
       , ' + @Prefix + '_scrslp_rec ou  
       ,' + @Prefix + '_mxrusr_rec            
       ,' + @Prefix + '_arrcuc_rec, ' + @Prefix + '_rprcds_rec ,   
        ' + @Prefix + '_ortcht_rec where            
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id            
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and ou.slp_slp=orh_tkn_slp   
       and usr_lgn_id = ou.slp_lgn_id   
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ          
       AND         
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm       
        and cht_tot_typ = ''T''            
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and  (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10), ''' + @TD + ''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
      AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
       and  ord_ord_brh not in (''SFS'')       
       and cds_desc in (''Normal Order'',''Job Detail Order'')        
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt  
         '             
   print @query;              
   EXECUTE sp_executesql @query;              
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  END                 
  CLOSE ScopeCursor;              
  DEALLOCATE ScopeCursor;              
 END              
ELSE              
BEGIN              
             
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)              
 IF (UPPER(@DBNAME) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DBNAME) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DBNAME) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DBNAME) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DBNAME) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End              
    Else if(UPPER(@DBNAME) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End              
    Else if(UPPER(@DBNAME) = 'TWCN')              
    begin              
       SET @DB ='TW'              
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
                  
                
  SET @sqltxt ='INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, CustName,Branch, DueDate, BalWgt, ChargeValue, Salesperson,Form,Grade,Size,Finish, OrderType)    
  select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_sld_cus_id as CustID,  
        cus_cus_long_nm as CustName,ord_ord_brh as Branch,orl_due_to_dt as DueDate, orl_bal_wgt * 2.20462 as BalWgt, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,     
    usr_nm as Salesperson,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)),cds_desc  
       from ' + @DBNAME + '_ortord_rec,' + @DBNAME + '_arrcus_rec ,        
       ' + @DBNAME + '_ortorh_rec ,' + @DBNAME + '_ortorl_rec,            
        ' + @DBNAME + '_tctipd_rec,   
       ' + @DBNAME + '_ortchl_rec            
       , ' + @DBNAME + '_scrslp_rec ou  
       ,' + @DBNAME + '_mxrusr_rec            
       ,' + @DBNAME + '_arrcuc_rec, ' + @DBNAME + '_rprcds_rec ,   
        ' + @DBNAME + '_ortcht_rec where            
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id            
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and ou.slp_slp=orh_tkn_slp   
       and usr_lgn_id = ou.slp_lgn_id   
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ          
       AND         
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm       
        and cht_tot_typ = ''T''            
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and  (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10), ''' + @TD + ''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
      AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
       and  ord_ord_brh not in (''SFS'')       
       and cds_desc in (''Normal Order'',''Job Detail Order'')        
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt  
       '            
 print(@sqltxt)              
 set @execSQLtxt = @sqltxt;               
 EXEC (@execSQLtxt); 
 
 set @sqltxt = 'insert into #tempOPA (CompanyId,OrderPrefix,OrderNo,OrderItem,JobSetNo)  
select job_cmpy_id, job_eord_pfx, job_eord_no, job_eord_itm, job_jbs_no  from  '+ @DBNAME +'_iptjob_rec 
join '+ @DBNAME +'_iptjbs_rec on jbs_jbs_no = job_jbs_no
join '+ @DBNAME +'_iprpwc_rec on pwc_cmpy_id = job_cmpy_id and pwc_whs = job_actvy_whs and pwc_pwc = job_pwc  where 
-- jbs_plng_whs = ''ROC'' and 
CONVERT(VARCHAR(10), job_sch_strt_ltts,120)  >= CONVERT(VARCHAR(10), ''' + @FDOPA + ''', 120)  
and CONVERT(VARCHAR(10), job_sch_strt_ltts,120)  <= CONVERT(VARCHAR(10), ''' + @TDOPA + ''', 120)  
--and job_jbs_no = ''434687''
and pwc_osprs = 1;'
print(@sqltxt)              
 set @execSQLtxt = @sqltxt;               
 EXEC (@execSQLtxt); 

END       
  
 
 
select #temp.CompanyId,#temp.OrderPrefix ,#temp.OrderNo ,#temp.OrderItem,#temp.CustID, #temp.CustName,#temp.Branch, #temp.DueDate, #temp.BalWgt, #temp.ChargeValue, 
#temp.Salesperson,#temp.Form,#temp.Grade,#temp.Size,#temp.Finish, #temp.OrderType,#tempOPA.JobSetNo from #temp 
left join #tempOPA on #tempOPA.CompanyId = #temp.CompanyId and #tempOPA.OrderPrefix = #temp.OrderPrefix and #tempOPA.OrderNo = #temp.OrderNo 
and #tempOPA.OrderItem = #temp.OrderItem
;  
  
   
 DROP TABLE  #temp;             
 DROP TABLE  #tempOPA;
END              
              
              
-- @DBNAME varchar(50), @Branch varchar(3)              
-- EXEC [sp_itech_OPA_OpenOrder] 'UK','ALL'  -- 125.99   ,469      
/*      
   
*/  
GO
