USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_TCRSTC]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sumit>
-- Create date: <29 Sept 2020>
-- Description:	<to get voucher status>

-- =============================================
create PROCEDURE [dbo].[US_TCRSTC]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_tcrstc_rec', 'U') IS NOT NULL
		drop table dbo.US_tcrstc_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_tcrstc_rec
from [LIVEUSSTX].[liveusstxdb].[informix].tcrstc_rec; 

END
GO
