USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OTPTransferAndRegular_Summary_RsCode]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_itech_OTPTransferAndRegular_Summary_RsCode] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID  varchar(10)

AS
BEGIN
SET NOCOUNT ON;



declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)


CREATE TABLE #tmp (   CompanyName		 VARCHAR(10)
   					, ShpgWhs 			 VARCHAR(65)
   					, Pcs			 DECIMAL(20, 2)
   					, Wgt			 DECIMAL(20, 2)
   					,Late			 integer
   					,TranspNO			 integer 	
   					,OTPType			 VARCHAR(35) 
   					,OrderNo   varchar(15)
   					,ShipmentReceiptNo		integer	
   					,RsnType          VARCHAR(35)
   					,RsnCode          VARCHAR(35)	
   	             );
   	             
   	             
   	             CREATE TABLE #tmpTransfer (   CompanyName		 VARCHAR(10)
   					, ShpgWhs 			 VARCHAR(65)
   					, Pcs			 DECIMAL(20, 2)
   					, Wgt			 DECIMAL(20, 2)
   					,Late			 integer
   					,TranspNO			 integer 	
   					,OTPType			 VARCHAR(35) 
   					,ShipmentReceiptNo		integer
   					,RsnType          VARCHAR(35)
   					,RsnCode          VARCHAR(35)
   	             );
   	                	         
DECLARE @company VARCHAR(35);
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CusID varchar(10);
Declare @Value as varchar(500);
DECLARE @CustIDLength int;

if @CustomerID ='ALL'
 BEGIN
 set @CusID = ''
 END
 
 SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));
   	   
 IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName, company,prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000);  	
  				-- set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
  				print(@DB)
							
							SET @query = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShipmentReceiptNo,RsnType,RsnCode)
					      	select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'',
							sum(dpf_shp_pcs) as ''Pcs'',SUM(dpf_shp_wgt) as ''Wgt'',Max(dpf_lte_shpt) as ''Late'',
							count(distinct dpf_transp_no) as ''TranspNO'',
							''Regular'' as OTPType, dpf_ord_no as ''orderNO'',count(dpf_sprc_no) as ''ShipmentReceiptNo'',Max(dpf_rsn_typ) as ''RsnType'' ,max(dpf_rsn) as ''RsnCode''
							from '+ @prefix +'_pfhdpf_rec
							JOIN '+ @prefix +'_ortorl_rec 
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no
							Join '+ @prefix +'_ortord_rec
							on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
							join '+ @prefix +'_arrcus_rec
							on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
							where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)
							and dpf_sprc_pfx = ''SH'''
					IF @CustomerID ='ALL'
					 BEGIN
						Set @query += ' and  (ord_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')' 
					 END
					ELSE
					 BEGIN
					    if @CustIDLength > 7
						 BEGIN
						  Set @Value= (select dbo.fun_itech_funSub_ContactorID(@Prefix,@CustomerID))
						  
						  Set @query += ' and  ord_sld_cus_id IN ('+ @Value +')' 
						 END
						 Else
						 BEGIN
						   Set @query += ' and  ord_sld_cus_id IN ('''+RTRIM(LTRIM(@CustomerID))+''')'
						 END
					 END
						Set @query += ' group by dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,dpf_transp_no ,
							            dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) '
							
							
  	  			EXECUTE sp_executesql @query;
  	  			Print(@query);
  	  		   SET @query = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode)
				SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',sum(b.tud_comp_pcs) as ''Pcs'',sum(b.tud_comp_wgt) as ''Wgt'',
				case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType,count(b.tud_sprc_no) as ''ShipmentReceiptNo'','''' as RsnType,'''' as RsnCode
				FROM ['+ @prefix +'_trjtph_rec] a
				left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
				left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
				left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
				left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
				WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''
				 AND(
						(b.tud_prnt_no<>0 
						and TPH_NBR_STP =1 
						and b.tud_sprc_pfx<>''SH'' 
						and b.tud_sprc_pfx <>''IT'')  
						OR 
						(b.tud_sprc_pfx=''IT'')
						)'
						
					IF @CustomerID ='ALL'
					 BEGIN
						Set @query += ' and  (ord_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')' 
					 END
					ELSE
					 BEGIN
					    if @CustIDLength > 7
						 BEGIN
						  
						  Set @Value= (select dbo.fun_itech_funSub_ContactorID(@Prefix,@CustomerID))
						  
						  Set @query += ' and  ord_sld_cus_id IN ('+ @Value +')' 
						 END
						 Else
						 BEGIN
						   Set @query += ' and  ord_sld_cus_id IN ('''+RTRIM(LTRIM(@CustomerID))+''')'
						 END
					 END
						Set @query += 'group by tph_CMPY_ID ,b.tud_trpln_whs ,
				                       a.tph_sch_dtts , a.tph_transp_no'
				
						
  	  		EXECUTE sp_executesql @query;
  	  			
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			--print('232')
			--  Set @DatabaseName=(select DatabaseName from tbl_itech_DatabaseName where company=''+ @DBNAME + '')
			 -- set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
			  print(@DB)  
			  
			  Set @prefix= @DBNAME 
					SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShipmentReceiptNo,RsnType,RsnCode)
					      	select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'',
							sum(dpf_shp_pcs) as ''Pcs'',SUM(dpf_shp_wgt) as ''Wgt'',Max(dpf_lte_shpt) as ''Late'',
							count(distinct dpf_transp_no) as ''TranspNO'',
							''Regular'' as OTPType, dpf_ord_no as ''orderNO'',count(dpf_sprc_no) as ''ShipmentReceiptNo'',Max(dpf_rsn_typ) as ''RsnType'' ,max(dpf_rsn) as ''RsnCode''
							from '+ @prefix +'_pfhdpf_rec
							JOIN '+ @prefix +'_ortorl_rec 
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no
							Join '+ @prefix +'_ortord_rec
							on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
							join '+ @prefix +'_arrcus_rec
							on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
							where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)
							and dpf_sprc_pfx = ''SH'' '
					IF @CustomerID ='ALL'
					 BEGIN
						Set @sqltxt += ' and  (ord_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')' 
					 END
					ELSE
					 BEGIN
					    if @CustIDLength > 7
						 BEGIN
						  Set @Value= (select dbo.fun_itech_funSub_ContactorID(@Prefix,@CustomerID))
						  
						  Set @sqltxt += ' and  ord_sld_cus_id IN ('+ @Value +')' 
						 END
						 Else
						 BEGIN
						   Set @sqltxt += ' and  ord_sld_cus_id IN ('''+RTRIM(LTRIM(@CustomerID))+''')'
						 END
					 END
						Set @sqltxt += ' group by dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,dpf_transp_no,
							dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx  ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)'
							
						
				print(@sqltxt)
			set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
		
		SET @sqltxt =	'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode)
				SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',sum(b.tud_comp_pcs) as ''Pcs'',sum(b.tud_comp_wgt) as ''Wgt'',
				case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType,count(b.tud_sprc_no) as ''ShipmentReceiptNo'','''' as RsnType,'''' as RsnCode
				FROM ['+ @prefix +'_trjtph_rec] a
				left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
				left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
				left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
				left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
				WHERE a.tph_sch_dtts BETWEEN '''+ @FD +''' AND '''+ @TD +'''
				 AND(
						(b.tud_prnt_no<>0 
						and TPH_NBR_STP =1 
						and b.tud_sprc_pfx<>''SH'' 
						and b.tud_sprc_pfx <>''IT'')  
						OR 
						(b.tud_sprc_pfx=''IT'')
						)'
						
					IF @CustomerID ='ALL'
					 BEGIN
						Set @sqltxt += ' and  (ord_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')' 
					 END
					ELSE
					 BEGIN
					    if @CustIDLength > 7
						 BEGIN
						  
						  Set @Value= (select dbo.fun_itech_funSub_ContactorID(@Prefix,@CustomerID))
						  
						  Set @sqltxt += ' and  ord_sld_cus_id IN ('+ @Value +')' 
						 END
						 Else
						 BEGIN
						   Set @sqltxt += ' and  ord_sld_cus_id IN ('''+RTRIM(LTRIM(@CustomerID))+''')'
						 END
					 END
						Set @sqltxt += 'group by tph_CMPY_ID ,b.tud_trpln_whs ,
				                       a.tph_sch_dtts , a.tph_transp_no'
			print(@sqltxt)
		set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
   END
     
       
SELECT COUNT(TranspNO) as 'TranspNO', CompanyName, ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType,sum(ShipmentReceiptNo) as 'ShipmentReceiptNo' ,'ZZ Late with NO Rsn Code' as RsnCode
FROM #tmpTransfer 
group by CompanyName, ShpgWhs,OTPType
union 
select sum(TranspNO) as 'TranspNO', CompanyName, ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType,sum(ShipmentReceiptNo) as 'ShipmentReceiptNo' ,Case RsnCode when '' then 'ZZ Late with NO Rsn Code' when null then 'ZZ Late with NO Rsn Code' else RsnCode end as RsnCode
from #tmp
group by CompanyName, ShpgWhs,OTPType,RsnCode
order by ShpgWhs

END

-- exec sp_itech_OTPTransferAndRegular_Summary_rsCode '07/01/2013', '07/31/2013' , 'NO','ALL'



	--select  dpf_cmpy_id as 'CompanyName',dpf_shpg_whs as 'ShpgWhs',
	--						sum(dpf_shp_pcs) as 'Pcs',SUM(dpf_shp_wgt) as 'Wgt',Max(dpf_lte_shpt) as 'Late',
	--						dpf_transp_no as 'TranspNO',
	--						'Regular' as OTPType, dpf_ord_no as 'orderNO',count(dpf_sprc_no) as 'ShipmentReceiptNo',Max(dpf_rsn_typ) as 'RsnType' ,max(dpf_rsn) as 'RsnCode'
	--						from US_pfhdpf_rec
	--						JOIN US_ortorl_rec 
 --                            ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no
	--						Join US_ortord_rec
	--						on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
	--						join US_arrcus_rec
	--						on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
	--						where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '2013-01-01', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '2013-04-30', 120)
	--						and dpf_sprc_pfx = 'SH' and  (ord_sld_cus_id = '' or ''= '') group by dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,
	--						dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)
							
							
							
	--							SELECT tph_CMPY_ID as 'CompanyName',b.tud_trpln_whs as 'ShpgWhs',sum(b.tud_comp_pcs) as 'Pcs',sum(b.tud_comp_wgt) as 'Wgt',
	--			case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as 'Late',a.tph_transp_no as 'TranspNO','Transfer' as OTPType,count(b.tud_sprc_no) as 'ShipmentReceiptNo','' as RsnType,'' as RsnCode
	--			FROM [US_trjtph_rec] a
	--			left JOIN [US_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
	--			left JOIN [US_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
	--			left Join [US_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no 
	--			left join [US_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
	--			WHERE a.tph_sch_dtts BETWEEN '2013-01-01' AND '2013-04-30'
	--			 AND(
	--					(b.tud_prnt_no<>0 
	--					and TPH_NBR_STP =1 
	--					and b.tud_sprc_pfx<>'SH' 
	--					and b.tud_sprc_pfx <>'IT')  
	--					OR 
	--					(b.tud_sprc_pfx='IT')
	--					) and  (ord_sld_cus_id = '' or ''= '')group by tph_CMPY_ID ,b.tud_trpln_whs ,
	--			                       a.tph_sch_dtts , a.tph_transp_no
GO
