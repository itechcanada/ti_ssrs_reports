USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Booking_Daily_By_Category]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <10 Jan 2014>    
-- Description: <Booking Daily Reports>  
-- Last updated by Mukesh 
-- Last updated Date 16 Jun 2015
-- Last updated desc: add IS and OS sales person  
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_Booking_Daily_By_Category]  @DBNAME varchar(50), @Market  varchar(65), @version char = '0'    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);     
DECLARE @CurrenyRate varchar(15);       
    
Set @DB = UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'    
    
print @DB;    
    
if @Market ='ALL'    
 BEGIN    
 set @Market = ''    
 END    
    
CREATE TABLE #temp ( Dbname   VARCHAR(10)     
     ,DBCountryName VARCHAR(25) 
     ,ISlp		VARCHAR(4)
     ,OSlp		VARCHAR(4)    
     ,CusID   VARCHAR(10)    
     ,CusLongNm  VARCHAR(40)    
     ,Branch   VARCHAR(3)    
     ,ActvyDT  VARCHAR(20)    
     ,OrderNo  NUMERIC    
     ,OrderItm  NUMERIC    
     ,Product  VARCHAR(500)    
     ,Market   VARCHAR(50)    
     ,Wgt   decimal(20,2)    
     ,TotalMtlVal decimal(20,2)    
     ,ReplCost  decimal(20,2)    
     ,Profit   decimal(20,2)    
     , TotalSlsVal decimal(20,2)    
     );    
    
IF @DBNAME = 'ALL'    
 BEGIN    
  IF @version = '0'    
  BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
    OPEN ScopeCursor;    
  END    
  ELSE    
  BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
    OPEN ScopeCursor;    
  END    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   DECLARE @query NVARCHAR(MAX);    
   IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
        
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR     
    BEGIN    
    SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Market, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)    
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,    
      c.cuc_desc30, a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'    
      FROM ' + @Prefix + '_ortmbk_rec a    
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id    
      left JOIN ' + @Prefix + '_arrcuc_rec c ON b.cus_cus_cat=c.cuc_cus_cat    
      WHERE a.mbk_ord_pfx=''SO''     
         AND a.mbk_ord_itm<>999     
        -- AND a.mbk_wgt > 0  
        and a.mbk_trs_md = ''A''     
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)    
         AND (c.cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'    
    END    
    ELSE    
    BEGIN    
     SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Market, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)    
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,    
      c.cuc_desc30, a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''=('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'    
      FROM ' + @Prefix + '_ortmbk_rec a    
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id    
      left JOIN ' + @Prefix + '_arrcuc_rec c ON b.cus_cus_cat=c.cuc_cus_cat    
      WHERE a.mbk_ord_pfx=''SO''     
         AND a.mbk_ord_itm<>999     
        -- AND a.mbk_wgt > 0     
        and a.mbk_trs_md = ''A''  
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)    
         AND (c.cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') '    
    END    
   print @query;    
   EXECUTE sp_executesql @query;    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  END       
  CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
ELSE    
BEGIN    
  IF @version = '0'    
  BEGIN    
  SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)    
  END    
  ELSE    
  BEGIN    
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)    
  END    
  print @CountryName;    
  IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR     
   BEGIN    
   SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Market, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)    
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,    
      c.cuc_desc30, a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''=('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'    
      FROM ' + @DBNAME + '_ortmbk_rec a    
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id    
      left JOIN ' + @DBNAME + '_arrcuc_rec c ON b.cus_cus_cat=c.cuc_cus_cat    
      WHERE a.mbk_ord_pfx=''SO''     
         AND a.mbk_ord_itm<>999     
        -- AND a.mbk_wgt > 0   
        and a.mbk_trs_md = ''A''    
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)     
         AND (c.cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') '    
  END    
  ELSE    
  BEGIN    
  SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Market, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)    
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,    
      c.cuc_desc30, a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''=('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'    
      FROM ' + @DBNAME + '_ortmbk_rec a    
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id    
      left JOIN ' + @DBNAME + '_arrcuc_rec c ON b.cus_cus_cat=c.cuc_cus_cat    
      WHERE a.mbk_ord_pfx=''SO''     
         AND a.mbk_ord_itm<>999     
        -- AND a.mbk_wgt > 0 
        and a.mbk_trs_md = ''A''      
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)      
         AND (c.cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') '    
  END    
 print(@sqltxt)    
 set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
END    
 SELECT * FROM #temp order by Branch ;    
 DROP TABLE  #temp;    
END    
    
--EXEC [sp_itech_Booking_Daily_By_Category] 'ALL','ALL','1'    
--EXEC [sp_itech_Booking_Daily_By_Category] 'US', 'Aerospace','1'    
    
--select * from UK_ortmbk_rec where mbk_actvy_dt = '2014-01-16' and mbk_ord_no = 11407
GO
