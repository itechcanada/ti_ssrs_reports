USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_xctava]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Jan 5, 2017,>
-- Description:	<Description,Customer conversation,>

-- =============================================
create PROCEDURE [dbo].[US_xctava]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_xctava_rec', 'U') IS NOT NULL
		drop table dbo.US_xctava_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_xctava_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[xctava_rec] ; 
  
END
-- select * from US_xctava_rec
GO
