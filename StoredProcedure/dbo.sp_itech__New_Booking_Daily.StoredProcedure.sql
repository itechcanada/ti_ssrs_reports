USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech__New_Booking_Daily]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mrinal>  
-- Create date: <14 Oct 2015>  
-- Description: <Booking Daily Reports> 
-- Last updated by Mukesh 
-- Last updated Date 16 Jun 2015
-- Last updated desc: add IS and OS sales person  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech__New_Booking_Daily]  @DBNAME varchar(50)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
declare @DB varchar(100);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);  
DECLARE @CountryName VARCHAR(25);     
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15);    
  
Set @DB = UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'  
  
CREATE TABLE #temp ( Dbname   VARCHAR(10)  
     ,DBCountryName VARCHAR(25) 
     ,ISlp		VARCHAR(4)
     ,OSlp		VARCHAR(4)   
     ,CusID   VARCHAR(10)  
     ,CusLongNm  VARCHAR(40)  
     ,Branch   VARCHAR(3)  
     ,ActvyDT  VARCHAR(10)  
     ,OrderNo  NUMERIC  
     ,OrderItm  NUMERIC  
     ,Product  VARCHAR(500)  
     ,Wgt   decimal(20,0)  
     ,TotalMtlVal decimal(20,0)  
     ,ReplCost  decimal(20,0)  
     ,Profit   decimal(20,1)  
     , TotalSlsVal decimal(20,0)  
     );  
  
IF @DBNAME = 'ALL'  
 BEGIN  
   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
  
    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR   
    BEGIN  
    SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)  
           
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''' , bki_is_slp, bki_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.bki_brh,a.bki_actvy_dt, a.bki_ord_no, a.bki_ord_itm, a.bki_frm+a.bki_grd+a.bki_size+a.bki_fnsh as Product,  
a.bki_wgt, a.bki_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END),  
''Profit''=CASE WHEN a.bki_tot_mtl_val=0 THEN 0 ELSE(a.bki_tot_mtl_val-(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END))/a.bki_tot_mtl_val*100 END, a.bki_tot_val* 1  
FROM ' + @Prefix + '_ortbki_rec a  
INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.bki_sld_cus_id=b.cus_cus_id  
INNER JOIN ' + @Prefix + '_ortxre_rec c ON c.xre_cmpy_id = a.bki_cmpy_id AND c.xre_ord_pfx = a.bki_ord_pfx AND c.xre_ord_no = a.bki_ord_no
WHERE a.bki_ord_pfx=''SO''   
AND a.bki_ord_itm<>999   
AND CAST(c.xre_crtd_dtts AS date)= CONVERT (date, GETDATE()-1) '  
  
    END  
    ELSE  
    BEGIN  
     SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)  
           
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''' , bki_is_slp, bki_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.bki_brh,a.bki_actvy_dt, a.bki_ord_no, a.bki_ord_itm, a.bki_frm+a.bki_grd+a.bki_size+a.bki_fnsh as Product,  
a.bki_wgt, a.bki_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END),  
''Profit''=CASE WHEN a.bki_tot_mtl_val=0 THEN 0 ELSE(a.bki_tot_mtl_val-(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END))/a.bki_tot_mtl_val*100 END, a.bki_tot_val* 1  
FROM ' + @Prefix + '_ortbki_rec a  
INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.bki_sld_cus_id=b.cus_cus_id  
INNER JOIN ' + @Prefix + '_ortxre_rec c ON c.xre_cmpy_id = a.bki_cmpy_id AND c.xre_ord_pfx = a.bki_ord_pfx AND c.xre_ord_no = a.bki_ord_no
WHERE a.bki_ord_pfx=''SO''   
AND a.bki_ord_itm<>999   
AND CAST(c.xre_crtd_dtts AS date)= CONVERT (date, GETDATE()-1) '  

    END  
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
 
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)  
  
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)  
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
      
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR   
   BEGIN  
   SET @sqltxt ='INSERT INTO #temp (Dbname,DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)  
     SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''' , bki_is_slp, bki_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.bki_brh,a.bki_actvy_dt, a.bki_ord_no, a.bki_ord_itm, a.bki_frm+a.bki_grd+a.bki_size+a.bki_fnsh as Product,  
a.bki_wgt, a.bki_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END),  
''Profit''=CASE WHEN a.bki_tot_mtl_val=0 THEN 0 ELSE(a.bki_tot_mtl_val-(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END))/a.bki_tot_mtl_val*100 END, a.bki_tot_val* 1  
FROM ' + @DBNAME + '_ortbki_rec a  
INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.bki_sld_cus_id=b.cus_cus_id  
INNER JOIN ' + @DBNAME + '_ortxre_rec c ON c.xre_cmpy_id = a.bki_cmpy_id AND c.xre_ord_pfx = a.bki_ord_pfx AND c.xre_ord_no = a.bki_ord_no
WHERE a.bki_ord_pfx=''SO''   
AND a.bki_ord_itm<>999   
AND CAST(c.xre_crtd_dtts AS date)= CONVERT (date, GETDATE()-1)'
  END  
  ELSE  
  BEGIN  
  SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal)  
      
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''' , bki_is_slp, bki_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.bki_brh,a.bki_actvy_dt, a.bki_ord_no, a.bki_ord_itm, a.bki_frm+a.bki_grd+a.bki_size+a.bki_fnsh as Product,  
a.bki_wgt, a.bki_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END),  
''Profit''=CASE WHEN a.bki_tot_mtl_val=0 THEN 0 ELSE(a.bki_tot_mtl_val-(CASE WHEN a.bki_mtl_repl_val=0 THEN a.bki_mtl_avg_val ELSE a.bki_mtl_repl_val END))/a.bki_tot_mtl_val*100 END, a.bki_tot_val* 1  
FROM ' + @DBNAME + '_ortbki_rec a  
INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.bki_sld_cus_id=b.cus_cus_id  
INNER JOIN ' + @DBNAME + '_ortxre_rec c ON c.xre_cmpy_id = a.bki_cmpy_id AND c.xre_ord_pfx = a.bki_ord_pfx AND c.xre_ord_no = a.bki_ord_no
WHERE a.bki_ord_pfx=''SO''   
AND a.bki_ord_itm<>999   
AND CAST(c.xre_crtd_dtts AS date)= CONVERT (date, GETDATE()-1)'
               
         
  END  
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ;  
 DROP TABLE  #temp;  
END  
  
--EXEC [sp_itech__New_Booking_Daily] 'ALL'  
--EXEC [sp_itech__New_Booking_Daily] 'PS','1'  
--EXEC [sp_itech__New_Booking_Daily] 'PS'  
--select * from tbl_itech_DatabaseName
GO
