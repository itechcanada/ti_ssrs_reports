USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_LTA]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Mukesh >      
-- Create date: <19 Nov 2015>      
-- Description: <Getting LTA>      
-- =============================================      
CREATE  PROCEDURE [dbo].[sp_itech_LTA] @DBNAME varchar(50) , @DayDiff Varchar(10)
      
AS      
BEGIN      
SET NOCOUNT ON;      
      
declare @sqltxt varchar(6000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(100)      
      
CREATE TABLE #tmp (   CompanyName   VARCHAR(10)      
        , CustID     VARCHAR(10)      
        , OrdNo    Varchar(10)      
        , OrderCreateDate    Varchar(10)      
        ,NoOfDays    integer  
        , BalWgt Decimal(20,2)    
        ,ReleaseCount    integer  
        ,InvoiceCount    integer 
        ,CusLongNm  VARCHAR(40)  
     ,Branch   VARCHAR(3)
     ,Market Varchar(35)  
                 );      
                       
DECLARE @company VARCHAR(35);      
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
             
             
 IF @DBNAME = 'ALL'      
 BEGIN      
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS      
    OPEN ScopeCursor;      
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
        DECLARE @query NVARCHAR(4000);         
       SET @query = 'INSERT INTO #tmp (CompanyName, CustID, OrdNo, OrderCreateDate, NoOfDays,BalWgt,ReleaseCount,InvoiceCount,CusLongNm,Branch,Market)
       select ''' + @prefix + ''' ,xre_sld_cus_id, orl_ord_no,   Convert(Varchar(10),xre_crtd_dtts,120), 
     DATEDIFF(day,xre_crtd_dtts, max(orl_rdy_by_dt)) as NoDays , 
     (select Sum(ord_bal_wgt) from '+ @prefix +'_ortord_rec where ord_ord_no = orl_ord_no ) as balanceWgt ,
     ( Select COUNT(*) from '+ @prefix +'_ortorl_rec rls where rls.orl_ord_no   = pr.orl_ord_no) as releaseCount,
     (select COUNT(*) from '+ @prefix +'_sahstn_rec Where  stn_ord_no = pr.orl_ord_no and stn_ord_pfx = ''SO'' ) as invoiceCount,
     b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30 
     from  '+ @prefix +'_ortorl_rec pr  
     join  '+ @prefix +'_ortxre_rec on xre_ord_pfx = orl_ord_pfx and xre_ord_no = orl_ord_no 
     INNER JOIN '+ @prefix +'_arrcus_rec b ON xre_sld_cus_id=b.cus_cus_id 
       left join '+ @prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
		where orl_ord_pfx = ''SO'' and xre_sts_actn <> ''C'' 
		group by xre_sld_cus_id, orl_ord_no, xre_crtd_dtts,b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30
		having DATEDIFF(day, xre_crtd_dtts, max(orl_rdy_by_dt)) > '''+ @DayDiff +'''
		order by NoDays desc '      
          print @query;
       EXECUTE sp_executesql @query;      
              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN       
           
     Set @prefix= @DBNAME       
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName, CustID, OrdNo, OrderCreateDate, NoOfDays,BalWgt,ReleaseCount,InvoiceCount,CusLongNm,Branch,Market)
     select ''' + @prefix + ''' ,xre_sld_cus_id, orl_ord_no,   Convert(Varchar(10),xre_crtd_dtts,120), 
     DATEDIFF(day,xre_crtd_dtts, max(orl_rdy_by_dt)) as NoDays , 
     (select Sum(ord_bal_wgt) from '+ @prefix +'_ortord_rec where ord_ord_no = orl_ord_no ) as balanceWgt ,
     ( Select COUNT(*) from '+ @prefix +'_ortorl_rec rls where rls.orl_ord_no   = pr.orl_ord_no) as releaseCount,
     (select COUNT(*) from '+ @prefix +'_sahstn_rec Where  stn_ord_no = pr.orl_ord_no and stn_ord_pfx = ''SO'' ) as invoiceCount,
     b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30 
     from  '+ @prefix +'_ortorl_rec pr
     join  '+ @prefix +'_ortxre_rec on xre_ord_pfx = orl_ord_pfx and xre_ord_no = orl_ord_no 
     INNER JOIN '+ @prefix +'_arrcus_rec b ON xre_sld_cus_id=b.cus_cus_id 
       left join '+ @prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
		where orl_ord_pfx = ''SO'' and xre_sts_actn <> ''C'' 
		group by xre_sld_cus_id, orl_ord_no, xre_crtd_dtts,b.cus_cus_long_nm, b.cus_admin_brh, cuc_desc30
		having DATEDIFF(day, xre_crtd_dtts, max(orl_rdy_by_dt)) > '''+ @DayDiff +'''
		order by NoDays desc '  
            
    print(@sqltxt)      
   set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
       
   END      
select * from #tmp  
drop table #tmp      
      
      
END      
      
-- exec [sp_itech_LTA]  'US',90      
      
      
GO
