USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ExportPrdRmks]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <17 Dec 2013>
-- Description:	<Getting Picking List SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_ExportPrdRmks] @DBNAME varchar(50)
As
Begin
declare @DB varchar(100)
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @intFlag integer;

DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(35);            
DECLARE @Name VARCHAR(15);  


IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB=  UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				SET @intFlag = 1
				
				
					SET @query =' select pti_frm,	pti_grd,	pti_size,	pti_fnsh,	pti_apln,	pti_rmk_cl,	pti_rmk_typ,	pti_lng,
 pti_lib_id, case when pti_lib_id is not null then lib_txt else	pti_txt end as pti_txt,	pti_rmk_expy_dt
 from ' + @DB + '.[inrpti_rec]
 left join ' + @DB + '.[scrlib_rec] on lib_lib_id= pti_lib_id and lib_lng=pti_lng;' ;
					
					print @query;
  	  				EXECUTE sp_executesql @query;
  	  				SET @intFlag = @intFlag + 1
				
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
    
				set @DB=@DBNAME;
				
					SET @sqltxt =' select pti_frm,	pti_grd,	pti_size,	pti_fnsh,	pti_apln,	pti_rmk_cl,	pti_rmk_typ,	pti_lng,
 pti_lib_id, case when pti_lib_id is not null then lib_txt else	pti_txt end as pti_txt,	pti_rmk_expy_dt
 from ' + @DB + '_inrpti_rec
 left join ' + @DB + '_scrlib_rec on lib_lib_id= pti_lib_id and lib_lng=pti_lng;'
					print(@sqltxt)	
					set @execSQLtxt = @sqltxt; 
					EXEC (@execSQLtxt);
				
				
			     
   END
  
End 
-- exec sp_itech_ExportPrdRmks 'US'
GO
