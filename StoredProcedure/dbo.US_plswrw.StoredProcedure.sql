USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_plswrw]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 23, 2014,>  
-- Description: <Description,  Warehouse Rmnt Weekly Data>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_plswrw]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_plswrw_rec', 'U') IS NOT NULL  
  drop table dbo.US_plswrw_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_plswrw_rec  
  from [LIVEUS_IW].[liveusstxdb_iw].[informix].[plswrw_rec] ;   
    
END  
-- select * from US_plswrw_rec where wrw_run_dt is desc
GO
