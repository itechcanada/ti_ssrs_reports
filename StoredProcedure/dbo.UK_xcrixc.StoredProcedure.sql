USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_xcrixc]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Author,Mukesh>          
-- Create date: <Create Date,2020-02-04,>          
-- Description: <Description,Open Orders,>          
          
-- =============================================          
CREATE PROCEDURE [dbo].[UK_xcrixc]          
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
IF OBJECT_ID('dbo.UK_xcrixc_rec', 'U') IS NOT NULL                    
  drop table dbo.UK_xcrixc_rec;              
          
    -- Insert statements for procedure here     
    select *  
    
into dbo.UK_xcrixc_rec      
FROM [LIVEUKSTX].[liveukstxdb].[informix].[xcrixc_rec]          
          
END   
-- select * from UK_xcrixc_rec;
GO
