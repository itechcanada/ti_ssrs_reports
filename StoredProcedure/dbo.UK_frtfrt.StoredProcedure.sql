USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_frtfrt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
-- Last Updated By: Mukesh  
-- Last Updated Date: 14 Apr 2015  
-- Last Updated Desc: avoid the error generating rows  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_frtfrt]  
   
AS  
BEGIN  
 -- SET NOOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.UK_frtfrt_rec', 'U') IS NOT NULL        
  drop table dbo.UK_frtfrt_rec;        
            
                
SELECT *        
into  dbo.UK_frtfrt_rec 
FROM [LIVEUKSTX].[liveukstxdb].[informix].[frtfrt_rec]  
-- Added by mukesh 20150413 to avoid this row  
WHERE frt_cmpy_id = 'UKS' and   (frt_ref_no !=6250  OR not (frt_ref_pfx = 'SI' AND frt_ref_itm in(1,2)))  
AND (frt_ref_no !=12968  OR not (frt_ref_pfx = 'SO' AND frt_ref_itm = 0))   
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
