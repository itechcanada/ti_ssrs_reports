USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_tcttsa]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
 
-- =============================================  
-- Author:  <Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_tcttsa]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
  IF OBJECT_ID('dbo.DE_tcttsa_rec', 'U') IS NOT NULL
		drop table dbo.DE_tcttsa_rec;
    
        
SELECT *
into  dbo.DE_tcttsa_rec
FROM [LIVEDESTX].[livedestxdb].[informix].[tcttsa_rec];
    
END  
-- select * from DE_tcttsa_rec   
  
GO
