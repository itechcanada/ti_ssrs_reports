USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Shipping_Charges]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================        
-- Author:  <Mukesh>        
-- Create date: <28 Mar 2018>        
-- Description: <sp_itech_Shipping_Charges>       
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_Shipping_Charges]  @DBNAME varchar(50), @fromDate date, @toDate date    
AS        
BEGIN        
      
      
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
declare @DB varchar(100);        
declare @sqltxt varchar(6000);        
declare @execSQLtxt varchar(7000);        
DECLARE @CountryName VARCHAR(25);           
DECLARE @prefix VARCHAR(15);           
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @CurrenyRate varchar(15);          
declare @FD varchar(10)              
declare @TD varchar(10)     
        
 set @FD = CONVERT(VARCHAR(10), @fromDate,120)          
 set @TD = CONVERT(VARCHAR(10), @toDate,120)       
         
CREATE TABLE #temp ( Dbname   VARCHAR(10)        
     ,CusID   VARCHAR(10)       
     ,InvoiceNo   NUMERIC   
     ,OrderNo  NUMERIC        
     ,ShippingCharge  decimal(20,2)        
     ,ShippingWarehouse  VARCHAR(3)          
     ,OrderWarehouse VARCHAR(3)
     );        
     
       
IF @DBNAME = 'ALL'        
 BEGIN        
        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
          
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   DECLARE @query NVARCHAR(MAX);        
   IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
            
         
    SET @query = 'INSERT INTO #temp (Dbname,CusID,InvoiceNo,OrderNo,ShippingCharge,ShippingWarehouse ,OrderWarehouse)    
   select '''+ @Prefix +''' as Country,sat_sld_cus_id, sat_upd_ref,sat_ord_no, SUM(csi_bas_cry_val * '+ @CurrenyRate +') , sat_shpg_whs, orh_shpg_whs
    from ' + @Prefix + '_sahsat_rec 
      join ' + @Prefix + '_ivjjvh_rec on jvh_cmpy_id = sat_cmpy_id and jvh_upd_ref_no = sat_upd_ref_no and jvh_inv_pfx=''SE'' and sat_shpt_no <>0   
	INNER JOIN ' + @Prefix + '_arrcus_rec  ON cus_cmpy_id = sat_cmpy_id and cus_cus_id = sat_sld_cus_id    
	join ' + @Prefix + '_cttcsi_rec on jvh_cmpy_id = csi_cmpy_id and jvh_inv_pfx= csi_ref_pfx and jvh_inv_no=csi_ref_no and csi_cst_no=400 and csi_cst_cl=''I''
	left join ' + @Prefix + '_ortorh_rec on orh_cmpy_id = sat_cmpy_id and orh_ord_pfx = sat_ord_pfx and orh_ord_no= sat_ord_no 
	left join ' + @Prefix + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
	where sat_inv_dt >= ''' + @FD + '''  and sat_inv_dt <= ''' + @TD + '''     
	and cuc_desc30 = ''Interco''  group by sat_sld_cus_id, sat_shpg_whs, orh_shpg_whs,sat_upd_ref,sat_ord_no  '        
       
   print @query;        
   EXECUTE sp_executesql @query;        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  END           
 CLOSE ScopeCursor;        
  DEALLOCATE ScopeCursor;        
 END        
ELSE        
BEGIN        
      
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)        
 IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DB ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
            
         
   SET @sqltxt ='INSERT INTO #temp (Dbname,CusID,InvoiceNo,OrderNo,ShippingCharge,ShippingWarehouse ,OrderWarehouse)        
      select '''+ @DBNAME +''' as Country,sat_sld_cus_id, sat_upd_ref,sat_ord_no, SUM(csi_bas_cry_val * '+ @CurrenyRate +') , sat_shpg_whs, orh_shpg_whs
       from ' + @DBNAME + '_sahsat_rec 
      join ' + @DBNAME + '_ivjjvh_rec on jvh_cmpy_id = sat_cmpy_id and jvh_upd_ref_no = sat_upd_ref_no and jvh_inv_pfx=''SE'' and sat_shpt_no <>0   
	INNER JOIN ' + @DBNAME + '_arrcus_rec  ON cus_cmpy_id = sat_cmpy_id and cus_cus_id = sat_sld_cus_id    
	join ' + @DBNAME + '_cttcsi_rec on jvh_cmpy_id = csi_cmpy_id and jvh_inv_pfx= csi_ref_pfx and jvh_inv_no=csi_ref_no and csi_cst_no=400 and csi_cst_cl=''I''
	left join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = sat_cmpy_id and orh_ord_pfx = sat_ord_pfx and orh_ord_no= sat_ord_no 
	left join ' + @DBNAME + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
	where sat_inv_dt >= ''' + @FD + '''  and sat_inv_dt <= ''' + @TD + '''     
	and cuc_desc30 = ''Interco''  group by sat_sld_cus_id, sat_shpg_whs, orh_shpg_whs,sat_upd_ref,sat_ord_no '     
     
 print(@sqltxt)        
 set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
END        
 SELECT *   FROM #temp ;        
 DROP TABLE  #temp ;        
END        
        
--EXEC [sp_itech_Shipping_Charges] 'US' ,'2018-03-01', '2018-04-04'      
--EXEC [sp_itech_Shipping_Charges] 'ALL' 
GO
