USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_perppb]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH  
-- Create date: April 24, 2015
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[US_perppb] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_perppb_rec', 'U') IS NOT NULL
		drop table dbo.US_perppb_rec;
    
        
SELECT *
into  dbo.US_perppb_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[perppb_rec];

END
-- select * from US_perppb_rec
GO
