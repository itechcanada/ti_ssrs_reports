USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_ivtivh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CN_ivtivh] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_ivtivh_rec', 'U') IS NOT NULL
		drop table dbo.CN_ivtivh_rec;
    
        
SELECT *
into  dbo.CN_ivtivh_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[ivtivh_rec];

END

--- exec CN_ivtivh
-- select * from CN_ivtivh_rec
GO
