USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_FlaggedExcessInventory]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <15 Sep 2016>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_FlaggedExcessInventory] @DBNAME varchar(50),@Warehouse  varchar(10)

AS
BEGIN

	
	SET NOCOUNT ON;
declare @sqltxt varchar(max)
declare @execSQLtxt varchar(max)
declare @DB varchar(100)
DECLARE @ExchangeRate varchar(15)
DECLARE @CurrenyRate varchar(15)  

SET @DB=@DBNAME;

CREATE TABLE #tmp ( Databases varchar(3)  
					,Warehouse varchar(3) 
   					,Form  varchar(35)
   					,Grade  varchar(35)
   					,Size  varchar(35)
   					,Finish  varchar(35)
   					,OnHndWgt	decimal(20,0)
   					,OnHndPcs	decimal(20,0)
   					,MaterialVal	 Decimal(20,2)
   					,ExpiryDate	 Varchar(10)
   	             );	
   	             
IF(@Warehouse = 'ALL')
Begin
Set @Warehouse = '';
End

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(5);
DECLARE @Name VARCHAR(15);

		
IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000); 
  	  			IF (UPPER(@Prefix) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@Prefix) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@Prefix) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@Prefix) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@Prefix) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End  	
    Else if(UPPER(@Prefix) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End  	
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')          
       BEGIN          
       SET @query = 'INSERT INTO #tmp (Databases,Warehouse,Form,Grade ,Size,Finish ,OnHndWgt,OnHndPcs,MaterialVal,ExpiryDate)
							SElect '''+ @Prefix + ''' as Databases, prd_whs,prd_frm, prd_grd, prd_size, prd_fnsh, Sum(prd_ohd_wgt * 2.20462) as prd_ohd_wgt, 
							Sum(prd_ohd_pcs) as prd_ohd_pcs,
 isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +' )) as Matval,
  pti_rmk_expy_dt from '+ @Prefix + '_inrpti_rec
join '+ @Prefix + '_intprd_rec on prd_frm = pti_frm and prd_grd = pti_grd and prd_size = pti_size and prd_fnsh = pti_fnsh 
left join '+ @Prefix + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool 
Where pti_apln = ''OR'' and pti_rmk_cl = 1 and pti_rmk_typ = 90 and pti_lng = ''en'' and prd_invt_sts = ''S'' 
and pti_rmk_expy_dt >= getdate() and cast(pti_txt as varchar(max)) like ''EXCESS%'' and (prd_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''')
Group by  prd_whs, prd_frm, prd_grd, prd_size, prd_fnsh,pti_rmk_expy_dt '
			  
			  --  * 2.20462         
       END          
       ELSE          
       BEGIN          
        SET @query = 'INSERT INTO #tmp (Databases,Warehouse,Form,Grade ,Size,Finish ,OnHndWgt,OnHndPcs,MaterialVal,ExpiryDate)
							SElect '''+ @Prefix + ''' as Databases, prd_whs,prd_frm, prd_grd, prd_size, prd_fnsh, Sum(prd_ohd_wgt ) as prd_ohd_wgt, 
							Sum(prd_ohd_pcs) as prd_ohd_pcs,
 isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +' )) as Matval,
  pti_rmk_expy_dt from '+ @Prefix + '_inrpti_rec
join '+ @Prefix + '_intprd_rec on prd_frm = pti_frm and prd_grd = pti_grd and prd_size = pti_size and prd_fnsh = pti_fnsh 
left join '+ @Prefix + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool 
Where pti_apln = ''OR'' and pti_rmk_cl = 1 and pti_rmk_typ = 90 and pti_lng = ''en'' and prd_invt_sts = ''S'' 
and pti_rmk_expy_dt >= getdate() and cast(pti_txt as varchar(max)) like ''EXCESS%'' and (prd_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''')
Group by  prd_whs, prd_frm, prd_grd, prd_size, prd_fnsh,pti_rmk_expy_dt'        
       END          
       
                  print(@query);
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
      IF (UPPER(@DB) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@DB) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@DB) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@DB) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@DB) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End  
    Else if(UPPER(@DB) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End  
    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
       BEGIN      
       -- * 2.20462 as Weight    
       SET @sqltxt =  'INSERT INTO #tmp (Databases,Warehouse,Form,Grade ,Size,Finish ,OnHndWgt,OnHndPcs,MaterialVal,ExpiryDate)
							SElect '''+ @DB + ''' as Databases, prd_whs,prd_frm, prd_grd, prd_size, prd_fnsh, Sum(prd_ohd_wgt * 2.20462) as prd_ohd_wgt, 
							Sum(prd_ohd_pcs) as prd_ohd_pcs,
 isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +' )) as Matval,
  pti_rmk_expy_dt from '+ @DB + '_inrpti_rec
join '+ @DB + '_intprd_rec on prd_frm = pti_frm and prd_grd = pti_grd and prd_size = pti_size and prd_fnsh = pti_fnsh 
left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool 
Where pti_apln = ''OR'' and pti_rmk_cl = 1 and pti_rmk_typ = 90 and pti_lng = ''en'' and prd_invt_sts = ''S'' 
and pti_rmk_expy_dt >= getdate() and cast(pti_txt as varchar(max)) like ''EXCESS%'' and (prd_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''')
Group by  prd_whs, prd_frm, prd_grd, prd_size, prd_fnsh,pti_rmk_expy_dt  '         
       END          
       ELSE          
       BEGIN    
          
       SET @sqltxt =  'INSERT INTO #tmp (Databases,Warehouse,Form,Grade ,Size,Finish ,OnHndWgt,OnHndPcs,MaterialVal,ExpiryDate)
							SElect '''+ @DB + ''' as Databases, prd_whs,prd_frm, prd_grd, prd_size, prd_fnsh, Sum(prd_ohd_wgt) as prd_ohd_wgt, 
							Sum(prd_ohd_pcs) as prd_ohd_pcs,
 isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +' )) as Matval,
  pti_rmk_expy_dt from '+ @DB + '_inrpti_rec
join '+ @DB + '_intprd_rec on prd_frm = pti_frm and prd_grd = pti_grd and prd_size = pti_size and prd_fnsh = pti_fnsh 
left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool 
Where pti_apln = ''OR'' and pti_rmk_cl = 1 and pti_rmk_typ = 90 and pti_lng = ''en'' and prd_invt_sts = ''S'' 
and pti_rmk_expy_dt >= getdate() and cast(pti_txt as varchar(max)) like ''EXCESS%'' and (prd_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''')
Group by  prd_whs, prd_frm, prd_grd, prd_size, prd_fnsh,pti_rmk_expy_dt '          
       END          
  
					print(@sqltxt);	
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
   SELECT  * FROM #tmp ;
  DROP TABLE #tmp
END

-- exec [sp_itech_FlaggedExcessInventory] 'Us','EMT'

GO
