USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_IRM_ALL_Database]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh>        
-- Create date: <01 Nov 2014>        
-- Description: <IRM_Report>  
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database       
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_IRM_ALL_Database]  @FromDate datetime,@CustType Varchar(2)  
AS        
BEGIN        

 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
declare @DB varchar(100);        
declare @sqltxt varchar(8000);        
declare @execSQLtxt varchar(8000);        
declare @FD varchar(10)        
        
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)      
        
CREATE TABLE #tempALL ( Dbname    VARCHAR(2)
	,OrderNo   VARCHAR(20)     
     ,PONumber  VARCHAR(75) 
     ,Part   Varchar(100)               
     ,Branch    VARCHAR(3)        
     ,ActvyDT   VARCHAR(10)        
     ,Product  VARCHAR(100)        
     
      ,StkSDFQty1   DECIMAL(20, 2)        
      ,StkSDFQty2   DECIMAL(20, 2)        
      ,StkSDFQty3   DECIMAL(20, 2)        
      ,StkSDFQty4   DECIMAL(20, 2)        
      ,StkSDFQty5   DECIMAL(20, 2)        
      ,StkSDFQty6   DECIMAL(20, 2)        
      ,StkSDFQty7   DECIMAL(20, 2)        
      ,StkSDFQty8   DECIMAL(20, 2)        
      ,StkSDFQty9   DECIMAL(20, 2)        
      ,StkSDFQty10   DECIMAL(20, 2)        
      ,StkSDFQty11   DECIMAL(20, 2)        
      ,StkSDFQty12   DECIMAL(20, 2)        
      ,StkSDFQty13   DECIMAL(20, 2)        
      ,StkSDFQty14   DECIMAL(20, 2)        
      ,StkSDFQty15   DECIMAL(20, 2)
      
      ,MimStkQty1   DECIMAL(20, 2)        
      ,MimStkQty2   DECIMAL(20, 2)        
      ,MimStkQty3   DECIMAL(20, 2)        
      ,MimStkQty4   DECIMAL(20, 2)        
      ,MimStkQty5   DECIMAL(20, 2)        
      ,MimStkQty6   DECIMAL(20, 2)        
      ,MimStkQty7   DECIMAL(20, 2)        
      ,MimStkQty8   DECIMAL(20, 2)        
      ,MimStkQty9   DECIMAL(20, 2)        
      ,MimStkQty10   DECIMAL(20, 2)        
      ,MimStkQty11   DECIMAL(20, 2)        
      ,MimStkQty12   DECIMAL(20, 2)        
      ,MimStkQty13   DECIMAL(20, 2)        
      ,MimStkQty14   DECIMAL(20, 2)        
      ,MimStkQty15   DECIMAL(20, 2)   
      
      ,NPosQty1   DECIMAL(20, 0)        
      ,NPosQty2   DECIMAL(20, 0)        
      ,NPosQty3   DECIMAL(20, 0)        
      ,NPosQty4   DECIMAL(20, 0)        
      ,NPosQty5   DECIMAL(20, 0)        
      ,NPosQty6   DECIMAL(20, 0)        
      ,NPosQty7   DECIMAL(20, 0)        
      ,NPosQty8   DECIMAL(20, 0)        
      ,NPosQty9   DECIMAL(20, 0)        
      ,NPosQty10   DECIMAL(20, 0)        
      ,NPosQty11   DECIMAL(20, 0)        
      ,NPosQty12   DECIMAL(20, 0)        
      ,NPosQty13   DECIMAL(20, 0)        
      ,NPosQty14   DECIMAL(20, 0)        
      ,NPosQty15   DECIMAL(20, 0)   
      
      ,OOBalQty1   DECIMAL(20, 0)        
      ,OOBalQty2   DECIMAL(20, 0)        
      ,OOBalQty3   DECIMAL(20, 0)        
      ,OOBalQty4   DECIMAL(20, 0)        
      ,OOBalQty5   DECIMAL(20, 0)        
      ,OOBalQty6   DECIMAL(20, 0)        
      ,OOBalQty7   DECIMAL(20, 0)        
      ,OOBalQty8   DECIMAL(20, 0)        
      ,OOBalQty9   DECIMAL(20, 0)        
      ,OOBalQty10   DECIMAL(20, 0)        
      ,OOBalQty11   DECIMAL(20, 0)        
      ,OOBalQty12   DECIMAL(20, 0)        
      ,OOBalQty13   DECIMAL(20, 0)        
      ,OOBalQty14   DECIMAL(20, 0)        
      ,OOBalQty15   DECIMAL(20, 0) 
      
      ,StkSDFQty16   DECIMAL(20, 0)        
      ,StkSDFQty17   DECIMAL(20, 0)        
      ,StkSDFQty18   DECIMAL(20, 0)        
      ,StkSDFQty19   DECIMAL(20, 0)        
      ,StkSDFQty20   DECIMAL(20, 0)        
      ,StkSDFQty21   DECIMAL(20, 0)        
      ,StkSDFQty22   DECIMAL(20, 0)        
      ,StkSDFQty23   DECIMAL(20, 0)        
      ,StkSDFQty24   DECIMAL(20, 0)        
      ,StkSDFQty25   DECIMAL(20, 0)        
      ,StkSDFQty26   DECIMAL(20, 0)        
      ,StkSDFQty27   DECIMAL(20, 0)        
      ,StkSDFQty28   DECIMAL(20, 0)        
      ,StkSDFQty29   DECIMAL(20, 0)        
      ,StkSDFQty30   DECIMAL(20, 0)
      
      ,MimStkQty16   DECIMAL(20, 0)        
      ,MimStkQty17   DECIMAL(20, 0)        
      ,MimStkQty18   DECIMAL(20, 0)        
      ,MimStkQty19   DECIMAL(20, 0)        
      ,MimStkQty20   DECIMAL(20, 0)        
      ,MimStkQty21   DECIMAL(20, 0)        
      ,MimStkQty22   DECIMAL(20, 0)        
      ,MimStkQty23   DECIMAL(20, 0)        
      ,MimStkQty24   DECIMAL(20, 0)        
      ,MimStkQty25   DECIMAL(20, 0)        
      ,MimStkQty26   DECIMAL(20, 0)        
      ,MimStkQty27   DECIMAL(20, 0)        
      ,MimStkQty28   DECIMAL(20, 0)        
      ,MimStkQty29   DECIMAL(20, 0)        
      ,MimStkQty30   DECIMAL(20, 0)
      
      ,NPosQty16   DECIMAL(20, 0)        
      ,NPosQty17   DECIMAL(20, 0)        
      ,NPosQty18   DECIMAL(20, 0)        
      ,NPosQty19   DECIMAL(20, 0)        
      ,NPosQty20   DECIMAL(20, 0)        
      ,NPosQty21   DECIMAL(20, 0)        
      ,NPosQty22   DECIMAL(20, 0)        
      ,NPosQty23   DECIMAL(20, 0)        
      ,NPosQty24   DECIMAL(20, 0)        
      ,NPosQty25   DECIMAL(20, 0)        
      ,NPosQty26   DECIMAL(20, 0)        
      ,NPosQty27   DECIMAL(20, 0)        
      ,NPosQty28   DECIMAL(20, 0)        
      ,NPosQty29   DECIMAL(20, 0)        
      ,NPosQty30   DECIMAL(20, 0)
      
      ,OOBalQty16   DECIMAL(20, 0)        
      ,OOBalQty17   DECIMAL(20, 0)        
      ,OOBalQty18   DECIMAL(20, 0)        
      ,OOBalQty19   DECIMAL(20, 0)        
      ,OOBalQty20   DECIMAL(20, 0)        
      ,OOBalQty21   DECIMAL(20, 0)        
      ,OOBalQty22   DECIMAL(20, 0)        
      ,OOBalQty23   DECIMAL(20, 0)        
      ,OOBalQty24   DECIMAL(20, 0)        
      ,OOBalQty25   DECIMAL(20, 0)        
      ,OOBalQty26   DECIMAL(20, 0)        
      ,OOBalQty27   DECIMAL(20, 0)        
      ,OOBalQty28   DECIMAL(20, 0)        
      ,OOBalQty29   DECIMAL(20, 0)        
      ,OOBalQty30   DECIMAL(20, 0)
      
      ,StkSDFQty31   DECIMAL(20, 0)        
      ,StkSDFQty32   DECIMAL(20, 0)        
      ,StkSDFQty33   DECIMAL(20, 0)        
      ,StkSDFQty34   DECIMAL(20, 0)        
      ,StkSDFQty35   DECIMAL(20, 0)        
      ,StkSDFQty36   DECIMAL(20, 0)        
      ,StkSDFQty37   DECIMAL(20, 0)                      
      ,MimStkQty31   DECIMAL(20, 0)        
      ,MimStkQty32   DECIMAL(20, 0)        
      ,MimStkQty33   DECIMAL(20, 0)        
      ,MimStkQty34   DECIMAL(20, 0)        
      ,MimStkQty35   DECIMAL(20, 0)        
      ,MimStkQty36   DECIMAL(20, 0)        
      ,MimStkQty37   DECIMAL(20, 0) 
      ,NPosQty31   DECIMAL(20, 0)        
      ,NPosQty32   DECIMAL(20, 0)        
      ,NPosQty33   DECIMAL(20, 0)        
      ,NPosQty34   DECIMAL(20, 0)        
      ,NPosQty35   DECIMAL(20, 0)        
      ,NPosQty36   DECIMAL(20, 0)        
      ,NPosQty37   DECIMAL(20, 0) 
      ,OOBalQty31   DECIMAL(20, 0)        
      ,OOBalQty32   DECIMAL(20, 0)        
      ,OOBalQty33   DECIMAL(20, 0)        
      ,OOBalQty34   DECIMAL(20, 0)        
      ,OOBalQty35   DECIMAL(20, 0)        
      ,OOBalQty36   DECIMAL(20, 0)        
      ,OOBalQty37   DECIMAL(20, 0)
     );        
      
Insert into #tempALL 
	EXEC [sp_itech_IRM_ALL_Prd_Report] 'TW',@FD,'', @CustType;
Insert into #tempALL 
	EXEC [sp_itech_IRM_ALL_Prd_Report] 'US',@FD,'', @CustType;	
	--Select part from #tempALL group by part;
select 
   'ALL' as Dbname, '' as OrderNo, '' as PONumber, Part,'' as Branch, '' as ActvyDT, 
   --'' as Product,
    Product,
   sum(StkSDFQty1) as StkSDFQty1,sum(StkSDFQty2) as StkSDFQty2,sum(StkSDFQty3) as StkSDFQty3
   ,sum(StkSDFQty4) as StkSDFQty4,sum(StkSDFQty5) as StkSDFQty5,sum(StkSDFQty6) as StkSDFQty6,sum(StkSDFQty7) as StkSDFQty7,sum(StkSDFQty8) as StkSDFQty8
   ,sum(StkSDFQty9) as StkSDFQty9,sum(StkSDFQty10) as StkSDFQty10,sum(StkSDFQty11) as StkSDFQty11,sum(StkSDFQty12) as StkSDFQty12,sum(StkSDFQty13) as StkSDFQty13,
   sum(StkSDFQty14) as StkSDFQty14,sum(StkSDFQty15) as StkSDFQty15,sum(MimStkQty1) as MimStkQty1,sum(MimStkQty2) as MimStkQty2,sum(MimStkQty3) as MimStkQty3
   ,sum(MimStkQty4) as MimStkQty4,sum(MimStkQty5) as MimStkQty5,sum(MimStkQty6) as MimStkQty6,sum(MimStkQty7) as MimStkQty7,sum(MimStkQty8) as MimStkQty8
   ,sum(MimStkQty9) as MimStkQty9,sum(MimStkQty10) as MimStkQty10,sum(MimStkQty11) as MimStkQty11,sum(MimStkQty12) as MimStkQty12,sum(MimStkQty13) as MimStkQty13
   ,sum(MimStkQty14) as MimStkQty14,sum(MimStkQty15) as MimStkQty15,sum(NPosQty1) as NPosQty1,sum(NPosQty2) as NPosQty2,sum(NPosQty3) as NPosQty3
   ,sum(NPosQty4) as NPosQty4,sum(NPosQty5) as NPosQty5,sum(NPosQty6) as NPosQty6,sum(NPosQty7) as NPosQty7,sum(NPosQty8) as NPosQty8,sum(NPosQty9) as NPosQty9,
   sum(NPosQty10) as NPosQty10,sum(NPosQty11) as NPosQty11,sum(NPosQty12) as NPosQty12,sum(NPosQty13) as NPosQty13,sum(NPosQty14) as NPosQty14
   ,sum(NPosQty15) as NPosQty15,sum(OOBalQty1) as OOBalQty1,sum(OOBalQty2) as OOBalQty2,sum(OOBalQty3) as OOBalQty3,sum(OOBalQty4) as OOBalQty4,sum(OOBalQty5) as OOBalQty5
   ,sum(OOBalQty6) as OOBalQty6,sum(OOBalQty7) as OOBalQty7,sum(OOBalQty8) as OOBalQty8,sum(OOBalQty9) as OOBalQty9,sum(OOBalQty10) as OOBalQty10
   ,sum(OOBalQty11) as OOBalQty11,sum(OOBalQty12) as OOBalQty12,sum(OOBalQty13) as OOBalQty13,sum(OOBalQty14) as OOBalQty14,sum(OOBalQty15) as OOBalQty15
   ,sum(StkSDFQty16) as StkSDFQty16,sum(StkSDFQty17) as StkSDFQty17,sum(StkSDFQty18) as StkSDFQty18,sum(StkSDFQty19) as StkSDFQty19,sum(StkSDFQty20) as StkSDFQty20
   ,sum(StkSDFQty21) as StkSDFQty21,sum(StkSDFQty22) as StkSDFQty22,sum(StkSDFQty23) as StkSDFQty23,sum(StkSDFQty24) as StkSDFQty24,sum(StkSDFQty25) as StkSDFQty25
   ,sum(StkSDFQty26) as StkSDFQty26,sum(StkSDFQty27) as StkSDFQty27,sum(StkSDFQty28) as StkSDFQty28,sum(StkSDFQty29) as StkSDFQty29,sum(StkSDFQty30) as StkSDFQty30
   ,sum(MimStkQty16) as MimStkQty16,sum(MimStkQty17) as MimStkQty17,sum(MimStkQty18) as MimStkQty18,sum(MimStkQty19) as MimStkQty19,sum(MimStkQty20) as MimStkQty20
   ,sum(MimStkQty21) as MimStkQty21,sum(MimStkQty22) as MimStkQty22,sum(MimStkQty23) as MimStkQty23,sum(MimStkQty24) as MimStkQty24,sum(MimStkQty25) as MimStkQty25
   ,sum(MimStkQty26) as MimStkQty26,sum(MimStkQty27) as MimStkQty27,sum(MimStkQty28) as MimStkQty28,sum(MimStkQty29) as MimStkQty29,sum(MimStkQty30) as MimStkQty30
   ,sum(NPosQty16) as NPosQty16,sum(NPosQty17) as NPosQty17,sum(NPosQty18) as NPosQty18,sum(NPosQty19) as NPosQty19,sum(NPosQty20) as NPosQty20,sum(NPosQty21) as NPosQty21
   ,sum(NPosQty22) as NPosQty22,sum(NPosQty23) as NPosQty23,sum(NPosQty24) as NPosQty24,sum(NPosQty25) as NPosQty25,sum(NPosQty26) as NPosQty26,sum(NPosQty27) as NPosQty27
   ,sum(NPosQty28) as NPosQty28,sum(NPosQty29) as NPosQty29,sum(NPosQty30) as NPosQty30,sum(OOBalQty16) as OOBalQty16,sum(OOBalQty17) as OOBalQty17,sum(OOBalQty18) as OOBalQty18
   ,sum(OOBalQty19) as OOBalQty19,sum(OOBalQty20) as OOBalQty20,sum(OOBalQty21) as OOBalQty21,sum(OOBalQty22) as OOBalQty22,sum(OOBalQty23) as OOBalQty23
   ,sum(OOBalQty24) as OOBalQty24,sum(OOBalQty25) as OOBalQty25,sum(OOBalQty26) as OOBalQty26,sum(OOBalQty27) as OOBalQty27,sum(OOBalQty28) as OOBalQty28
   ,sum(OOBalQty29) as OOBalQty29,sum(OOBalQty30) as OOBalQty30,sum(StkSDFQty31) as StkSDFQty31,sum(StkSDFQty32) as StkSDFQty32,sum(StkSDFQty33) as StkSDFQty33
   ,sum(StkSDFQty34) as StkSDFQty34,sum(StkSDFQty35) as StkSDFQty35,sum(StkSDFQty36) as StkSDFQty36,sum(StkSDFQty37) as StkSDFQty37,sum(MimStkQty31) as MimStkQty31
   ,sum(MimStkQty32) as MimStkQty32,sum(MimStkQty33) as MimStkQty33,sum(MimStkQty34) as MimStkQty34,sum(MimStkQty35) as MimStkQty35,sum(MimStkQty36) as MimStkQty36
   ,sum(MimStkQty37) as MimStkQty37,sum(NPosQty31) as NPosQty31,sum(NPosQty32) as NPosQty32,sum(NPosQty33) as NPosQty33,sum(NPosQty34) as NPosQty34
   ,sum(NPosQty35) as NPosQty35,sum(NPosQty36) as NPosQty36,sum(NPosQty37) as NPosQty37,sum(OOBalQty31) as OOBalQty31,sum(OOBalQty32) as OOBalQty32
   ,sum(OOBalQty33) as OOBalQty33,sum(OOBalQty34) as OOBalQty34,sum(OOBalQty35) as OOBalQty35,sum(OOBalQty36) as OOBalQty36,sum(OOBalQty37) as OOBalQty37
  from #tempALL group by Part, Product order by Part,Branch  ;
     
 --SELECT * FROM #tempALL ;        
 DROP TABLE  #tempALL;     
 
 
END        
        
     
--EXEC [sp_itech_IRM_ALL_Database] '2014-10-30','M'  

GO
