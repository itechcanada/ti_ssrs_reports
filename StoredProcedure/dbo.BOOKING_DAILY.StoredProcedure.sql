USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[BOOKING_DAILY]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <12/9/2011>
-- Description:	<Global Booking>
-- =============================================
CREATE PROCEDURE [dbo].[BOOKING_DAILY] 
	-- Add the parameters for the stored procedure here
	
	
AS

SELECT 'USA' as Country, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,
	a.mbk_wgt, a.mbk_tot_mtl_val, 'ReplCost'=CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END,
	'Profit'=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE())
	  	  	    
UNION
SELECT 'UK' as Country,  b.cus_cus_id,b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,
	a.mbk_wgt,a.mbk_tot_mtl_val, 'ReplCost'=CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END,
	'Profit'=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END
FROM [UK_ortmbk_rec] a
INNER JOIN [UK_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)
	    	  
	
UNION
SELECT 'CANADA' as Country, b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,
	a.mbk_wgt,a.mbk_tot_mtl_val, 'ReplCost'=CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END,
	'Profit'=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END
FROM [CA_ortmbk_rec] a
INNER JOIN [CA_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)
	  	  
UNION
SELECT 'TAIWAN' as Country, b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,
	a.mbk_wgt,a.mbk_tot_mtl_val, 'ReplCost'=CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END,
	'Profit'=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END
FROM [TW_ortmbk_rec] a
INNER JOIN [TW_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)
	 
UNION
SELECT 'NORWAY' as Country, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,
	a.mbk_wgt, a.mbk_tot_mtl_val, 'ReplCost'=CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END,
	'Profit'=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END
FROM [NO_ortmbk_rec] a
INNER JOIN [NO_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)  	  


GO
