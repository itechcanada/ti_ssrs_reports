USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetCustomerSnapShotDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <13 Jan 2020>          
-- Description: <Getting top 50 customers for SSRS reports>          
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_GetCustomerSnapShotDetails]  @DBNAME varchar(50), @CustID varchar(30)         
          
AS          
BEGIN          
           
 SET NOCOUNT ON;          
declare @sqltxt varchar(6000)          
declare @execSQLtxt varchar(7000)          
declare @DB varchar(100)          
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(5);          
          
CREATE TABLE #tmp (  databases varchar(2)           
        ,custID Varchar(30)          
        ,paymentTerm varchar(30)   
        ,discountTerm varchar(30)   
        ,avgPayment3M int  
        ,avgPayment6M int  
        ,appliedCreditLimit decimal(20,2)    
        ,arBalance decimal(20,2)  
        ,releaseOrder decimal(20,2)  
        ,availableBalance decimal(20,2)        
        )          
          
IF @DBNAME = 'ALL'          
 BEGIN          
           
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
        DECLARE @query NVARCHAR(4000);             
               
      SET @query ='INSERT INTO #tmp (databases,custID,paymentTerm,discountTerm,avgPayment3M,avgPayment6M,appliedCreditLimit,arBalance,releaseOrder,availableBalance)          
        select ''' + @Prefix + ''', ''' + @CustID + ''', LTRIM(RTRIM(pyt_desc30)) ,LTRIM(RTRIM(dtr_desc30)),'''','''',crd_co_lim,coc_ar_bal,0,0   
        from ' + @Prefix + '_arrcrd_rec   
join ' + @Prefix + '_scrpyt_rec on pyt_pttrm = crd_pmt_trm   
join ' + @Prefix + '_scrdtr_rec on dtr_disc_trm = crd_disc_trm   
join ' + @Prefix + '_arbcoc_rec on coc_cmpy_id = crd_cmpy_id and coc_cus_id = crd_cus_id  
where crd_cus_id = ''' + @CustID + ''' '          
        print(@query);          
        EXECUTE sp_executesql @query;          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN              
     SET @sqltxt ='INSERT INTO #tmp (databases,custID,paymentTerm,discountTerm,avgPayment3M,avgPayment6M,appliedCreditLimit,arBalance,releaseOrder,availableBalance)          
        select ''' + @DBNAME + ''', ''' + @CustID + ''', LTRIM(RTRIM(pyt_desc30)) ,LTRIM(RTRIM(dtr_desc30)),'''','''',crd_co_lim,coc_ar_bal,0,0   
        from ' + @DBNAME + '_arrcrd_rec   
join ' + @DBNAME + '_scrpyt_rec on pyt_pttrm = crd_pmt_trm   
join ' + @DBNAME + '_scrdtr_rec on dtr_disc_trm = crd_disc_trm   
join ' + @DBNAME + '_arbcoc_rec on coc_cmpy_id = crd_cmpy_id and coc_cus_id = crd_cus_id  
where crd_cus_id = ''' + @CustID + ''';                    
       '          
     print(@sqltxt);           
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);          
      End          
                
      select * from #tmp;      
      drop table  #tmp          
END          
          
-- exec sp_itech_GetCustomerSnapShotDetails  'ALL' ,'1049'        
/*  
crd_cmpy_id = 'USS' and    
*/          
-- 
GO
