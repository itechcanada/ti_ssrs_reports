USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_tctitc]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,mukesh>    
-- Create date: <Create Date,Dec 20, 2016,>    
-- Description: <Description,Due Date,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CN_tctitc]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.CN_tctitc_rec', 'U') IS NOT NULL    
  drop table dbo.CN_tctitc_rec;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.CN_tctitc_rec    
  from [LIVECNSTX].[livecnstxdb].[informix].[tctitc_rec];     
      
END 
GO
