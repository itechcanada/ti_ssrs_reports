USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[INVENTORY_PIPE_FITTINGS_REORDER]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/8/2012>
-- Description:	<Description,For Stock, Open OPA, Open PO to decide the reorder>
-- =============================================
CREATE PROCEDURE [dbo].[INVENTORY_PIPE_FITTINGS_REORDER]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
CREATE TABLE #PRODUCT
(
country varchar(10),
[type] varchar(10),
product varchar(255),
frmgrd varchar(255),
size varchar(255),
po float,
stock_so float,
cust_so float,
stock float
)

INSERT INTO #PRODUCT
SELECT 'USA','FITTINGS' , prm_frm+prm_grd+prm_size as product, prm_frm+prm_grd,prm_frm+prm_grd+prm_size,NULL, NULL, NULL, NULL
FROM [US_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
UNION
SELECT 'USA','PIPE' , prm_frm+prm_grd+prm_size as product, prm_frm+prm_grd,prm_frm+prm_grd+prm_size,NULL, NULL, NULL, NULL
FROM [US_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TISP  2','TIWP  2')
UNION
SELECT 'CA','FITTINGS' , prm_frm+prm_grd+prm_size as product, prm_frm+prm_grd,prm_frm+prm_grd+prm_size,NULL, NULL, NULL, NULL
FROM [CA_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
UNION
SELECT 'CA','PIPE' , prm_frm+prm_grd+prm_size as product, prm_frm+prm_grd,prm_frm+prm_grd+prm_size,NULL, NULL, NULL, NULL
FROM [CA_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TISP  2','TIWP  2')


/*Open PO*/
CREATE TABLE #OPEN_PO
(
country varchar(10),
[type] varchar(10),
product varchar(50),
po float
)
INSERT INTO #OPEN_PO
SELECT 'USA','FITTINGS', b.ipd_frm+b.ipd_grd+b.ipd_size as product, SUM(a.pod_bal_pcs)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size

INSERT INTO #OPEN_PO
SELECT 'USA','PIPE', b.ipd_frm+b.ipd_grd+b.ipd_size as product, ROUND(SUM(a.pod_bal_msr)/12,0)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size

INSERT INTO #OPEN_PO
SELECT 'CA','FITTINGS', b.ipd_frm+b.ipd_grd+b.ipd_size as product, SUM(a.pod_bal_pcs)
FROM [CA_potpod_rec] a
INNER JOIN [CA_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size

INSERT INTO #OPEN_PO
SELECT 'CA','PIPE', b.ipd_frm+b.ipd_grd+b.ipd_size as product, ROUND(SUM(a.pod_bal_msr)/12,0)
FROM [CA_potpod_rec] a
INNER JOIN [CA_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size


/*OPEN SO*/
CREATE TABLE #OPEN_SO
(
country varchar(10),
[type] varchar(10),
product varchar(50),
so float,
order_type varchar(1)
)


SELECT DISTINCT ipd_ref_pfx, ipd_ref_no, ipd_ref_itm, ipd_frm,ipd_grd, ipd_size
INTO #TCTIPD1
FROM [US_tctipd_rec] 
WHERE ipd_frm+ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')


SELECT DISTINCT ord_ord_pfx, ord_ord_no, ord_ord_itm, ord_bal_pcs 
INTO #ORTORD1
FROM [US_ortord_rec] 
WHERE ord_ord_pfx= 'SO' AND
ord_sts_actn='A' 


INSERT INTO #OPEN_SO
SELECT 'USA','FITTINGS', c.ipd_frm+c.ipd_grd+c.ipd_size as product,SUM(b.ord_bal_pcs) ,d.orh_ord_typ
FROM #ORTORD1 b
INNER JOIN [US_ortorh_rec] d ON b.ord_ord_pfx=d.orh_ord_pfx AND b.ord_ord_no=d.orh_ord_no 
INNER JOIN #TCTIPD1 c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size,d.orh_ord_typ

/*PIPE*/
SELECT DISTINCT ipd_ref_pfx, ipd_ref_no, ipd_ref_itm, ipd_frm,ipd_grd, ipd_size
INTO #TCTIPD2
FROM [US_tctipd_rec] 
WHERE ipd_frm+ipd_grd IN ('TISP  2','TIWP  2')


SELECT DISTINCT ord_ord_pfx, ord_ord_no, ord_ord_itm, ord_bal_msr
INTO #ORTORD2
FROM [US_ortord_rec] 
WHERE ord_ord_pfx= 'SO' AND
ord_sts_actn='A' 


INSERT INTO #OPEN_SO
SELECT 'USA','PIPE', c.ipd_frm+c.ipd_grd+c.ipd_size as product,ROUND(SUM(b.ord_bal_msr)/12,0),d.orh_ord_typ
FROM #ORTORD2 b
INNER JOIN [US_ortorh_rec] d ON b.ord_ord_pfx=d.orh_ord_pfx AND b.ord_ord_no=d.orh_ord_no 
INNER JOIN #TCTIPD2 c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size,d.orh_ord_typ


/*CA*/
/*FITTINGS*/
SELECT DISTINCT ipd_ref_pfx, ipd_ref_no, ipd_ref_itm, ipd_frm,ipd_grd, ipd_size
INTO #TCTIPDUK1
FROM [CA_tctipd_rec] 
WHERE ipd_frm+ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')


SELECT DISTINCT ord_ord_pfx, ord_ord_no, ord_ord_itm, ord_bal_pcs 
INTO #ORTORDUK1
FROM [CA_ortord_rec] 
WHERE ord_ord_pfx= 'SO' AND
ord_sts_actn='A' 


INSERT INTO #OPEN_SO
SELECT 'CA','FITTINGS', c.ipd_frm+c.ipd_grd+c.ipd_size as product,SUM(b.ord_bal_pcs) ,d.orh_ord_typ
FROM #ORTORDUK1 b
INNER JOIN [CA_ortorh_rec] d ON b.ord_ord_pfx=d.orh_ord_pfx AND b.ord_ord_no=d.orh_ord_no 
INNER JOIN #TCTIPDUK1 c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size,d.orh_ord_typ

/*PIPE*/
SELECT DISTINCT ipd_ref_pfx, ipd_ref_no, ipd_ref_itm, ipd_frm,ipd_grd, ipd_size
INTO #TCTIPDUK2
FROM [CA_tctipd_rec] 
WHERE ipd_frm+ipd_grd IN ('TISP  2','TIWP  2')


SELECT DISTINCT ord_ord_pfx, ord_ord_no, ord_ord_itm, ord_bal_msr
INTO #ORTORDUK2
FROM [CA_ortord_rec] 
WHERE ord_ord_pfx= 'SO' AND
ord_sts_actn='A' 


INSERT INTO #OPEN_SO
SELECT 'CA','PIPE', c.ipd_frm+c.ipd_grd+c.ipd_size as product,ROUND(SUM(b.ord_bal_msr)/12,0),d.orh_ord_typ
FROM #ORTORDUK2 b
INNER JOIN [CA_ortorh_rec] d ON b.ord_ord_pfx=d.orh_ord_pfx AND b.ord_ord_no=d.orh_ord_no 
INNER JOIN #TCTIPDUK2 c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size,d.orh_ord_typ


/*OPEN SO - Not closed yet
CREATE TABLE #OPEN_ACTIVE_SO
(
[type] varchar(10),
product varchar(50),
open_so float
)
INSERT INTO #OPEN_ACTIVE_SO
SELECT 'FITTINGS',orb_frm+orb_grd+orb_size+orb_fnsh,SUM(orb_bal_pcs) FROM [US_plsorb_rec]
WHERE
orb_ord_no NOT IN (SELECT DISTINCT sat_ord_no FROM [US_sahsat_rec])
AND orb_frm+orb_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY orb_frm+orb_grd+orb_size+orb_fnsh

INSERT INTO #OPEN_ACTIVE_SO
SELECT 'PIPE',orb_frm+orb_grd+orb_size+orb_fnsh,ROUND(SUM(orb_bal_msr)/12,0)
FROM [US_plsorb_rec]
WHERE
orb_ord_no NOT IN (SELECT DISTINCT sat_ord_no FROM [US_sahsat_rec])
AND orb_frm+orb_grd IN ('TISP  2','TIWP  2')
GROUP BY orb_frm+orb_grd+orb_size+orb_fnsh
*/

/*STOCK STATUS*/
CREATE TABLE #STOCK_STATUS1
(
country varchar(10),
[type] varchar(10),
product varchar(50),
stock float
)
INSERT INTO #STOCK_STATUS1
SELECT 'USA','FITTINGS' , b.prd_frm+b.prd_grd+b.prd_size, b.prd_ohd_pcs
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')

INSERT INTO #STOCK_STATUS1
SELECT 'USA','PIPE' , b.prd_frm+b.prd_grd+b.prd_size, b.prd_ohd_msr
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TISP  2','TIWP  2')


INSERT INTO #STOCK_STATUS1
SELECT 'CA','FITTINGS' , b.prd_frm+b.prd_grd+b.prd_size, b.prd_ohd_pcs
FROM [CA_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
UNION
SELECT 'CA','PIPE' , b.prd_frm+b.prd_grd+b.prd_size, b.prd_ohd_msr
FROM [CA_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TISP  2','TIWP  2')




CREATE TABLE #STOCK_STATUS2
(
country varchar(10),
[type] varchar(10),
product varchar(50),
stock float
)
INSERT INTO #STOCK_STATUS2
SELECT country,[type], product, SUM(stock)
FROM #STOCK_STATUS1
WHERE [type]='FITTINGS'
GROUP BY country,[type], product
UNION
SELECT country,[type], product, ROUND(SUM(stock)/12,0)
FROM #STOCK_STATUS1
WHERE [type]='PIPE'
GROUP BY country,[type], product




UPDATE #PRODUCT
SET po=b.po
FROM #PRODUCT a ,#OPEN_PO b
WHERE a.country=b.country AND a.[type]=b.[type] AND a.product=b.product
AND b.po IS NOT NULL

UPDATE #PRODUCT
SET stock_so=b.so
FROM #PRODUCT a , #OPEN_SO b
WHERE a.country=b.country AND a.[type]=b.[type] AND a.product=b.product AND b.order_type='S'
AND b.so IS NOT NULL

UPDATE #PRODUCT
SET cust_so=b.so
FROM #PRODUCT a , #OPEN_SO b
WHERE a.country=b.country AND a.[type]=b.[type] AND a.product=b.product AND b.order_type <>'S'
AND b.so IS NOT NULL



UPDATE #PRODUCT
SET stock=b.stock   
FROM #PRODUCT a,#STOCK_STATUS2 b
WHERE a.country=b.country AND a.[type]=b.[type] AND a.product=b.product
AND (b.stock IS NOT NULL)

/*
CREATE TABLE PRE_INV_REORDER 
(
title varchar(50),
product varchar(50),
rating varchar(20),
needed varchar (1),
stock_qty int,
reorder_point int,
input varchar(30),
vendor varchar(50),
comment nvarchar(255)
)
DELETE FROM PRE_INV_REORDER
SELECT * FROM PRE_INV_REORDER

INSERT INTO PRE_INV_REORDER
SELECT DISTINCT CASE WHEN prm_frm='TIFL' AND prm_size LIKE '%150R%' THEN 'FLANGES-RAISED FACE SLIP-ON'           
            WHEN prm_frm='TIFL' AND prm_size LIKE '%3\300R%' THEN 'FLANGES-RAISED FACE SLIP-ON'
            WHEN prm_frm='TIFL' AND prm_size LIKE '%150B%' THEN 'BLIND FLANGE'
            WHEN prm_frm+prm_grd='TICR  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%') THEN 'CONCENTRIC REDUCERS'
            WHEN prm_frm+prm_grd='TIFT  2' AND prm_size LIKE '%WC%' THEN 'WELD CAP'
            WHEN prm_frm+prm_grd='TITE  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%') AND prm_size NOT LIKE '%X%' THEN 'FULL OUTLET TEES'
            WHEN prm_frm+prm_grd='TITE  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%') AND prm_size LIKE '%X%' THEN 'SHORT OUTLET REDUCING TEES'
            WHEN prm_frm+prm_grd='TIEL  2' AND (prm_size LIKE '%S1045%' OR prm_size LIKE '%S4045%') THEN 'ELBOWS 45'
            WHEN prm_frm+prm_grd='TIEL  2' AND (prm_size LIKE '%S1090%' OR prm_size LIKE '%S4090%') THEN 'ELBOWS 90'
            WHEN prm_frm+prm_grd='TIER  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%')  THEN 'ECCENTRIC REDUCERS'
            WHEN prm_frm+prm_grd='TISE  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%' OR prm_size LIKE '%S80%') THEN 'STUB ENDS'
            WHEN prm_frm+prm_grd='TIFT  2' AND prm_size LIKE '%FCPL%' THEN 'FULL COUPLING' END
--prm_frm,prm_grd,prm_size, prm_fnsh
,prm_frm+prm_grd+prm_size
,CASE WHEN prm_frm='TIFL' AND prm_size LIKE '%150R%' THEN '150#'
      WHEN prm_frm='TIFL' AND prm_size LIKE '%3\300R%' THEN '300#' END
,CASE WHEN prm_frm='TIFL' AND prm_size LIKE '%150R%' THEN 'R' 
      WHEN prm_frm='TIFL' AND prm_size LIKE '%3\300R%' THEN 'R' 
       WHEN prm_frm='TIFL' AND prm_size LIKE '%150B%' THEN 'B' END
,0
,0
,CASE WHEN prm_frm+prm_grd='TISE  2' AND (prm_size LIKE '%S10%' OR prm_size LIKE '%S40%' OR prm_size LIKE '%S80%') THEN 'PLATE BAR PIPE EASTMAN' END
,''
,''
FROM [LIVEUSSTX].[liveusstxdb].[informix].[inrprm_rec] 
WHERE prm_frm+prm_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')

SELECT * FROM [LIVEUSSTX].[liveusstxdb].[informix].[inrprm_rec] 
WHERE prm_frm='TIFL' AND prm_size LIKE '%150B%' 
*/
/*
CREATE TABLE FITTINGS_DETAIL
(
[type] varchar(10),
product varchar(50),
frmgrd varchar(255),
size varchar(255),
po float,
stock_so float,
cust_so float,
stock float,   
)
*/
DELETE FROM FITTINGS_DETAIL


INSERT INTO FITTINGS_DETAIL
SELECT 
[type],product,frmgrd,size,po,stock_so,cust_so,stock
FROM #PRODUCT a
WHERE 
a.country='USA' AND (
a.po  IS NOT NULL OR
a.stock_so  IS NOT NULL OR
a.cust_so IS NOT NULL OR
a.stock  IS NOT NULL )



SELECT a.country,a.[type],b.product,a.frmgrd,a.size,a.po,a.stock_so,a.cust_so,a.stock, b.title, b.rating, b.needed,b.stock_qty,b.reorder_point,b.input,b.vendor, b.comment
FROM #PRODUCT a
RIGHT OUTER JOIN INV_REORDER b ON a.product=b.product
WHERE 
a.country='USA' AND (
a.po  IS NOT NULL OR
a.stock_so  IS NOT NULL OR
a.cust_so IS NOT NULL OR
a.stock  IS NOT NULL )
UNION
SELECT a.country,a.[type],a.product,a.frmgrd,a.size,a.po,a.stock_so,a.cust_so,a.stock,'','','','','','','',''
FROM #PRODUCT a
WHERE 
a.[type]='FITTINGS' AND
a.country='CA' AND (
a.po  IS NOT NULL OR
a.stock_so  IS NOT NULL OR
a.cust_so IS NOT NULL OR
a.stock  IS NOT NULL )




DROP TABLE #ORTORD1
DROP TABLE #ORTORD2
DROP TABLE #ORTORDUK1
DROP TABLE #ORTORDUK2

DROP TABLE #TCTIPD1
DROP TABLE #TCTIPD2
DROP TABLE #TCTIPDUK1
DROP TABLE #TCTIPDUK2

DROP TABLE #PRODUCT
DROP TABLE #OPEN_PO
DROP TABLE #OPEN_SO

DROP TABLE #STOCK_STATUS1
DROP TABLE #STOCK_STATUS2

END

GO
