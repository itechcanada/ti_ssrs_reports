USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_salesRepAccountDetails_SRR]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- sp_helptext [sp_itech_salesRepAccountDetails]                                
-- =============================================                                  
-- Author:  <Mukesh >                                  
-- Create date: <15 Oct 2015>                                  
                               
-- =============================================                                  
CREATE PROCEDURE [dbo].[sp_itech_salesRepAccountDetails_SRR]  @DBNAME varchar(50),@FromMonth Datetime,@ToMonth Datetime                             
AS                                
BEGIN                                
                  
                  
SET NOCOUNT ON;                  
                                
                      
declare @sqltxt varchar(MAX)                                        
declare @sqltxt1 varchar(MAX)                                        
declare @execSQLtxt varchar(MAX)                                
declare @DB varchar(100)                                
declare @NFD varchar(10)                                
declare @NTD varchar(10)                                       
declare @LFD varchar(10)                                
declare @LTD varchar(10)                      
declare @AFD varchar(10)                            
declare @ATD varchar(10)                   
declare @FD varchar(10)                            
declare @TD varchar(10)                      
DECLARE @CurrenyRate varchar(15)                      
                               
SET @DB = @DBNAME                            
                     
SET @FD = CONVERT(VARCHAR(10), @FromMonth , 120)                    
SET @TD = CONVERT(VARCHAR(10), @ToMonth , 120)                    
                             
SET @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromMonth)-1,0)) , 120)   -- New Account                              
SET @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToMonth),0)), 120)  -- New Account                         
                        
SET @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@ToMonth)-19,0)) , 120)   -- Lost Account                              
SET @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToMonth)-18,0)), 120)  -- Lost Account                       
              
    IF(DATEPART(DAY,@ToMonth) > 0)                
    BEGIN        
  set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@ToMonth)-17,0)) , 120)    -- Active Accounts                             
    END        
    ELSE        
    BEGIN        
  set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@ToMonth)-18,0)) , 120)    -- Active Accounts                             
    END        
--set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToMonth),0)), 120)                        
    SET @ATD = dbo.gnc_get_complete_date_of_month(@ToMonth)        
        
Print 'mrinal test :' + @AFD  + ' ' + @ATD        
                        
CREATE TABLE #tmp(                      
                         
     salePerson  varchar(50),                       
     newAccount  int,                       
     lostAccount  int,                      
     activeAccount int,                    
     reactiveAccount int,                    
     GPAmt DECIMAL(20, 2),                          
     NPAmt  DECIMAL(20, 2),                     
     lbsSold DECIMAL(20, 2),                     
     phoneCall int,                    
     visit int,                    
     form int,                    
     sapl int                      
     );                     
                            
 CREATE TABLE #tactive (  CustomerID  VARCHAR(15)                      
      ,FirstInvOfMonth Date                      
      ,shp_is_slp varchar(75)                      
        );         
 CREATE TABLE #tactive1 (  CustomerID  VARCHAR(15)                      
      ,FirstInvOfMonth Date                      
      ,shp_os_slp varchar(75)                      
        );                 
                      
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @Prefix VARCHAR(35);                        
DECLARE @Name VARCHAR(15);                        
                      
IF @DBNAME = 'ALL'                        
 BEGIN                       
 DECLARE ScopeCursor CURSOR FOR                        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                        
    OPEN ScopeCursor;                        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
     WHILE @@FETCH_STATUS = 0                        
       BEGIN                        
        DECLARE @query NVARCHAR(max);                     
        IF (UPPER(@Prefix) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@Prefix) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CN')                            
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                          
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                   
    End                            
    Else if(UPPER(@Prefix) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                      
    Else if(UPPER(@Prefix) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                      
      SET @DB= @Prefix      
                          
      -- insert total active customers records                    
 SET @query ='INSERT INTO #tactive ( CustomerID, FirstInvOfMonth, shp_is_slp ) 
 select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,MIN(slp_lgn_id)                      
              from '+ @DB +'_sahstn_rec                      
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id      
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP  
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +''' and cuc_desc30 <> ''Interco''                      
               group by stn_sld_cus_id '                    
  print(@query)                    
   EXECUTE sp_executesql @query;         
           
           
   SET @query ='INSERT INTO #tactive1 ( CustomerID, FirstInvOfMonth, shp_os_slp )                     
    select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,MIN(slp_lgn_id)                      
              from '+ @DB +'_sahstn_rec                      
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                  
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP  
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +''' and cuc_desc30 <> ''Interco''                      
               group by stn_sld_cus_id  '                    
  print(@query)                    
   EXECUTE sp_executesql @query;         
                      
       SET @query ='INSERT INTO #tmp (salePerson, newAccount, lostAccount, activeAccount, reactiveAccount,GPAmt,NPAmt,lbsSold,phoneCall,visit, form, sapl )                      
       Select  oq.SalePersonName, SUM(oq.Newaccount), SUM(oq.Lostaccount), SUM(oq.activeAccount), SUM(oq.reactiveAccount),                    
       SUM(oq.GPAmt), SUM(oq.NPAmt), SUM(oq.Weight), Sum(oq.phoneCall), Sum(oq.visit), Sum(oq.form), SUM(oq.sapl)   from (                      
    SELECT SalePersonName, 0 AS Newaccount, 0 as Lostaccount, 0 as activeAccount, 0 as reactiveAccount,sum(t.GPAmt) as GPAmt,                    
Sum(t.NPAmt) as NPAmt, Sum(t.Weight) as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl FROM (                            
        SELECT                         
        slp_lgn_id as SalePersonName, Sum(ISNULL(stn_mpft_avg_val,0)  * '+ @CurrenyRate +') as GPAmt,SUM(ISNULL(stn_tot_val,0)  * '+ @CurrenyRate +') as NPAmt, '                    
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                          
       BEGIN                          
       SET @query = @query + ' SUM(ISNULL(stn_blg_wgt,0) * 2.20462) as Weight '                          
       END                          
       ELSE                          
       BEGIN                          
       SET @query = @query + ' SUM(ISNULL(stn_blg_wgt,0)) as Weight '                          
       END                          
        SET @query = @query + '                             
        FROM ' + @DB + '_sahstn_rec                 
        JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                         
        join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP          join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
        left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
        WHERE stn_inv_dt Between '''+  @FD + ''' And ''' + @TD +''' and cuc_desc30 <> ''Interco''                               
        GROUP BY slp_lgn_id       
              
        UNION ALL      
            
        SELECT                         
        slp_lgn_id as SalePersonName, Sum(ISNULL(stn_mpft_avg_val,0)  * '+ @CurrenyRate +') as GPAmt,SUM(ISNULL(stn_tot_val,0)  * '+ @CurrenyRate +') as NPAmt, '                    
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                          
       BEGIN                          
       SET @query = @query + ' SUM(ISNULL(stn_blg_wgt,0) * 2.20462) as Weight '                          
       END                          
       ELSE                          
       BEGIN                          
       SET @query = @query + ' SUM(ISNULL(stn_blg_wgt,0)) as Weight '                          
       END                          
        SET @query = @query + '                             
        FROM ' + @DB + '_sahstn_rec                 
        JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                          
        join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP  
        join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
        left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
        WHERE stn_inv_dt Between '''+  @FD + ''' And ''' + @TD +''' and cuc_desc30 <> ''Interco''                               
        GROUP BY slp_lgn_id      
              
        ) AS t                            
        GROUP BY SalePersonName  
              
    Union                  
             
SELECT SalePersonName,COUNT(t.STN_SLD_CUS_ID) AS Newaccount, 0 as Lostaccount, 0 as activeAccount, 0 as reactiveAccount ,0 as GPAmt, 0 as NPAmt,0 as Weight          
,0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  FROM          
(   
SELECT                
MIN(slp_lgn_id) as SalePersonName,STN_SLD_CUS_ID            
FROM ' + @DB + '_sahstn_rec INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                          
WHERE          
coc_frst_sls_dt Between  '''+  @FD + ''' And ''' + @TD +'''  and cuc_desc30 <> ''Interco''             
Group by  STN_SLD_CUS_ID          
        
UNION ALL        
        
SELECT                
MIN(slp_lgn_id) as SalePersonName,STN_SLD_CUS_ID            
FROM ' + @DB + '_sahstn_rec INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                           
WHERE          
coc_frst_sls_dt Between  '''+  @FD + ''' And ''' + @TD +'''  and cuc_desc30 <> ''Interco''             
Group by  STN_SLD_CUS_ID         
        
) as t GROUP BY SalePersonName          
             
              
   Union                                 
                                     
   SELECT salesPerson,0 as Newaccount,COUNT(t.STN_SLD_CUS_ID) AS Lostaccount , 0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt          
   , 0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  FROM          
(   
SELECT   Min(slp_lgn_id) as salesPerson , STN_SLD_CUS_ID            
    FROM ' + @DB + '_sahstn_rec                
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id     
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                           
where           
STN_INV_DT <= ''' + @NTD  +''' and cuc_desc30 <> ''Interco''          
group by STN_SLD_CUS_ID              
Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD + '''          
        
UNION ALL        
        
SELECT   Min(slp_lgn_id) as salesPerson , STN_SLD_CUS_ID            
    FROM ' + @DB + '_sahstn_rec                
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                             
where           
STN_INV_DT <= ''' + @NTD  +''' and cuc_desc30 <> ''Interco''          
group by STN_SLD_CUS_ID              
Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD + '''         
        
) as t GROUP BY salesPerson  
              
       Union                      
                         
          Select t.salesPerson, 0 as Newaccount, 0 as Lostaccount ,COUNT(t.stn_sld_cus_id) as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
     0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  from  (                      
                select distinct  stn_sld_cus_id, min(slp_lgn_id) as salesPerson                           
              from ' + @DB + '_sahstn_rec                 
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id      
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                          
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                         
              where STN_INV_DT > ''' + @FD + ''' and STN_INV_DT <= ''' + @TD + ''' and cuc_desc30 <> ''Interco''                           
               group by stn_sld_cus_id        
                       
               UNION ALL        
                       
               select distinct stn_sld_cus_id, min(slp_lgn_id) as salesPerson                           
              from ' + @DB + '_sahstn_rec                 
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                             
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                         
              where STN_INV_DT > ''' + @FD + ''' and STN_INV_DT <= ''' + @TD + ''' and cuc_desc30 <> ''Interco''                           
               group by stn_sld_cus_id        
                       
             ) as t group by t.salesPerson 
                                   
       UNION                    
                           
       SELECT ra.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, COUNT(ra.CustomerID) as reactiveAccount, 0 as GPAmt,                    
     0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl FROM                    
       ( 
       SELECT                             
              t.CustomerID,Min(t.shp_is_slp) as SalePersonName                     
  FROM #tactive t, ' + @DB + '_arrcrd_rec join ' + @DB + '_arrcus_rec on crd_cus_id = cus_cus_id                       
              left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
              left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                        
                              
              where                       
              t.CustomerID Not IN                       
              ( select distinct t1.CustomerID  from #tactive t1,                      
              ' + @DB + '_sahstn_rec u                   
              where t1.CustomerID = u.STN_SLD_CUS_ID                       
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + '''  )                      
              and t.CustomerID not in  (select coc_cus_id from ' + @DB + '_arbcoc_rec                   
                                
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''                      
              )                      
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''   and cuc_desc30 <> ''Interco''                 
              group by  t.CustomerID          
                      
              UNION ALL        
                      
              SELECT                             
              t.CustomerID,Min(t.shp_os_slp) as SalePersonName                     
              FROM #tactive1 t, ' + @DB + '_arrcrd_rec join ' + @DB + '_arrcus_rec on crd_cus_id = cus_cus_id                       
              left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
              left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                        
                              
              where                       
              t.CustomerID Not IN            
              ( select distinct t1.CustomerID  from #tactive1 t1,                      
              ' + @DB + '_sahstn_rec u                   
              where t1.CustomerID = u.STN_SLD_CUS_ID                       
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + '''  )                      
              and t.CustomerID not in  (select coc_cus_id from ' + @DB + '_arbcoc_rec                   
                                
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''                      
              )                      
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''   and cuc_desc30 <> ''Interco''                 
              group by  t.CustomerID        
                      
              ) as ra group by ra.SalePersonName 
              
              Union                    
                                  
                                  
              Select  t.SalePersonName,  0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, Sum(t.TotalPhoneCall) as phoneCall, 0 as visit, 0 as form, 0 as sapl from (                
                
  select cta_tsk_asgn_to as SalePersonName,  COUNT(*) as TotalPhoneCall                   
       from ' + @DB + '_cctcta_rec join                   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1     
         join '+ @DB +'_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to                                      
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                    
                
       and rtrim(atp_desc30) = ''Phone Call - Prospect Account''          
       group by  cta_tsk_asgn_to, atp_desc30  
       ) as t group by  t.SalePersonName                  
                          
   Union                    
                          
   Select  t.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, 0 as phoneCall, Sum(t.totalVisit) as visit, 0 as form, 0 as sapl from (               
               
   select cta_tsk_asgn_to as SalePersonName,  COUNT(*) as totalVisit                   
       from ' + @DB + '_cctcta_rec join                   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to           
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                      
                
       and rtrim(atp_desc30) = ''Visit - Prospect Account''          
       group by  cta_tsk_asgn_to, atp_desc30  
         
       ) as t group by  t.SalePersonName                    
                                    
   Union                     
                                    
     Select t.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
     0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, count(*) as form, 0 as sapl  from (                     
    select slp_lgn_id as SalePersonName,stn_frm as form                    
  from ' + @DB + '_sahstn_rec                   
  JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id   
     join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_is_slp                               
  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
  left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
  where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''   and cuc_desc30 <> ''Interco''                  
    group by slp_lgn_id, stn_frm       
          
    UNION ALL      
        
  select slp_lgn_id as SalePersonName,stn_frm as form                    
  from ' + @DB + '_sahstn_rec                   
  JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id     
     join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_os_slp                              
  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
  left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
  where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''   and cuc_desc30 <> ''Interco''                  
    group by slp_lgn_id, stn_frm         
    ) as t group by t.SalePersonName                 
                         
     union                                       
   Select oqq.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, COUNT(*) as sapl                     
   from (                    
   Select t.stn_sld_cus_id, t.SalePersonName, count(*) as form                     
   from (                     
    select STN_SLD_CUS_ID,slp_lgn_id as SalePersonName,stn_frm as form                    
    from ' + @DB + '_sahstn_rec                     
    JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_is_slp                             
       join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                    
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
   where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''    and cuc_desc30 <> ''Interco''                
   group by slp_lgn_id,STN_SLD_CUS_ID, stn_frm        
   ) as t  group by t.SalePersonName,t.stn_sld_cus_id    
       
   Union All    
       
   Select t.stn_sld_cus_id, t.SalePersonName, count(*) as form                     
   from (                     
    select STN_SLD_CUS_ID,slp_lgn_id as SalePersonName,stn_frm as form                    
    from ' + @DB + '_sahstn_rec                     
    JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_os_slp                              
       join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                    
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
   where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''    and cuc_desc30 <> ''Interco''                
   group by slp_lgn_id,STN_SLD_CUS_ID, stn_frm        
   ) as t  group by t.SalePersonName,t.stn_sld_cus_id    
       
    ) as oqq where oqq.form >= 4                    
   group by oqq.SalePersonName                                                      
       ) as oq group by oq.SalePersonName   '                      
                             
 print @query;                        
        EXECUTE sp_executesql @query;                     
        truncate table #tactive;                         
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
       END                         
    CLOSE ScopeCursor;                        
    DEALLOCATE ScopeCursor;                        
 END                      
 Else                      
  Begin                     
                      
  IF (UPPER(@DB) = 'TW')                            
    begin                   
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@DB) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@DB) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@DB) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                   
    Else if(UPPER(@DB) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                      
    Else if(UPPER(@DB) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                      
                        
    SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')                        
                       
 -- insert total active customers records                    
 SET @sqltxt ='INSERT INTO #tactive ( CustomerID, FirstInvOfMonth, shp_is_slp )                     
    select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,MIN(slp_lgn_id)                      
              from '+ @DB +'_sahstn_rec                      
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id      
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP  
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +''' and cuc_desc30 <> ''Interco''                      
               group by stn_sld_cus_id        
                '                    
  print(@sqltxt)                         
    set @execSQLtxt = @sqltxt;                         
   EXEC (@execSQLtxt);             
           
   SET @sqltxt ='INSERT INTO #tactive1 ( CustomerID, FirstInvOfMonth, shp_os_slp )                     
    select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,MIN(slp_lgn_id)                      
              from '+ @DB +'_sahstn_rec                      
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                  
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP  
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +''' and cuc_desc30 <> ''Interco''                      
               group by stn_sld_cus_id        
                '                    
  print(@sqltxt)                         
    set @execSQLtxt = @sqltxt;                         
   EXEC (@execSQLtxt);         
                       
  SET @sqltxt ='INSERT INTO #tmp (salePerson, newAccount, lostAccount, activeAccount, reactiveAccount,GPAmt,NPAmt,lbsSold,phoneCall,visit, form, sapl )                      
       Select oq.SalePersonName, SUM(oq.Newaccount), SUM(oq.Lostaccount), SUM(oq.activeAccount), SUM(oq.reactiveAccount),                    
       SUM(oq.GPAmt), SUM(oq.NPAmt), SUM(oq.Weight), Sum(oq.phoneCall), Sum(oq.visit), Sum(oq.form), SUM(oq.sapl)   from (                      
    SELECT SalePersonName, 0 AS Newaccount, 0 as Lostaccount, 0 as activeAccount, 0 as reactiveAccount,sum(t.GPAmt) as GPAmt,                    
     Sum(t.NPAmt) as NPAmt, Sum(t.Weight) as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl FROM (                            
        SELECT                         
        slp_lgn_id as SalePersonName, Sum(ISNULL(stn_mpft_avg_val,0)  * '+ @CurrenyRate +') as GPAmt,SUM(ISNULL(stn_tot_val,0)  * '+ @CurrenyRate +') as NPAmt, '                    
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' SUM(ISNULL(stn_blg_wgt,0) * 2.20462) as Weight '                          
       END                          
       ELSE                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' SUM(ISNULL(stn_blg_wgt,0)) as Weight '                          
       END                          
        SET @sqltxt = @sqltxt + '                             
        FROM ' + @DB + '_sahstn_rec                 
        JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                         
        join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP          join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
        left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
        WHERE stn_inv_dt Between '''+  @FD + ''' And ''' + @TD +''' and cuc_desc30 <> ''Interco''                               
        GROUP BY slp_lgn_id       
              
        UNION ALL      
            
        SELECT                         
        slp_lgn_id as SalePersonName, Sum(ISNULL(stn_mpft_avg_val,0)  * '+ @CurrenyRate +') as GPAmt,SUM(ISNULL(stn_tot_val,0)  * '+ @CurrenyRate +') as NPAmt, '                    
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' SUM(ISNULL(stn_blg_wgt,0) * 2.20462) as Weight '                          
       END                          
       ELSE                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' SUM(ISNULL(stn_blg_wgt,0)) as Weight '                          
       END                          
        SET @sqltxt = @sqltxt + '                             
        FROM ' + @DB + '_sahstn_rec                 
        JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                          
        join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP  
        join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
        left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
        WHERE stn_inv_dt Between '''+  @FD + ''' And ''' + @TD +''' and cuc_desc30 <> ''Interco''                               
        GROUP BY slp_lgn_id      
              
        ) AS t                            
        GROUP BY SalePersonName    
   Union                  
             
SELECT SalePersonName,COUNT(t.STN_SLD_CUS_ID) AS Newaccount, 0 as Lostaccount, 0 as activeAccount, 0 as reactiveAccount ,0 as GPAmt, 0 as NPAmt,0 as Weight          
,0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  FROM          
(          
SELECT                
MIN(slp_lgn_id) as SalePersonName,STN_SLD_CUS_ID            
FROM ' + @DB + '_sahstn_rec INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                          
WHERE          
coc_frst_sls_dt Between  '''+  @FD + ''' And ''' + @TD +'''  and cuc_desc30 <> ''Interco''             
Group by  STN_SLD_CUS_ID          
        
UNION ALL        
        
SELECT                
MIN(slp_lgn_id) as SalePersonName,STN_SLD_CUS_ID            
FROM ' + @DB + '_sahstn_rec INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                           
WHERE          
coc_frst_sls_dt Between  '''+  @FD + ''' And ''' + @TD +'''  and cuc_desc30 <> ''Interco''             
Group by  STN_SLD_CUS_ID         
        
) as t GROUP BY SalePersonName          
            
   UNION              
             
   SELECT salesPerson,0 as Newaccount,COUNT(t.STN_SLD_CUS_ID) AS Lostaccount , 0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt          
   , 0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  FROM          
(          
SELECT   Min(slp_lgn_id) as salesPerson , STN_SLD_CUS_ID            
    FROM ' + @DB + '_sahstn_rec                
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id     
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                           
where           
STN_INV_DT <= ''' + @NTD  +''' and cuc_desc30 <> ''Interco''          
group by STN_SLD_CUS_ID              
Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD + '''          
        
UNION ALL        
        
SELECT   Min(slp_lgn_id) as salesPerson , STN_SLD_CUS_ID            
    FROM ' + @DB + '_sahstn_rec                
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                             
where           
STN_INV_DT <= ''' + @NTD  +''' and cuc_desc30 <> ''Interco''          
group by STN_SLD_CUS_ID              
Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD + '''         
        
) as t GROUP BY salesPerson           
          
Union                      
                             
      Select t.salesPerson, 0 as Newaccount, 0 as Lostaccount ,COUNT(t.stn_sld_cus_id) as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
     0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl  from  (                      
               select distinct  stn_sld_cus_id, min(slp_lgn_id) as salesPerson                           
              from ' + @DB + '_sahstn_rec                 
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id      
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_IS_SLP                          
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                         
              where STN_INV_DT > ''' + @FD + ''' and STN_INV_DT <= ''' + @TD + ''' and cuc_desc30 <> ''Interco''                           
               group by stn_sld_cus_id        
                       
               UNION ALL        
                       
               select distinct stn_sld_cus_id, min(slp_lgn_id) as salesPerson                           
              from ' + @DB + '_sahstn_rec                 
              JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
              join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = SHP_OS_SLP                             
              join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
              left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                         
              where STN_INV_DT > ''' + @FD + ''' and STN_INV_DT <= ''' + @TD + ''' and cuc_desc30 <> ''Interco''                           
               group by stn_sld_cus_id        
                       
             ) as t group by t.salesPerson                     
                                   
       UNION                   
                           
 SELECT ra.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, COUNT(ra.CustomerID) as reactiveAccount, 0 as GPAmt,                    
     0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, 0 as sapl FROM                    
       ( SELECT                             
              t.CustomerID,Min(t.shp_is_slp) as SalePersonName                     
  FROM #tactive t, ' + @DB + '_arrcrd_rec join ' + @DB + '_arrcus_rec on crd_cus_id = cus_cus_id                       
              left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
              left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                        
                              
              where                       
              t.CustomerID Not IN                       
              ( select distinct t1.CustomerID  from #tactive t1,                      
              ' + @DB + '_sahstn_rec u                   
              where t1.CustomerID = u.STN_SLD_CUS_ID                       
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + '''  )                      
              and t.CustomerID not in  (select coc_cus_id from ' + @DB + '_arbcoc_rec                   
                                
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''                      
              )                      
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''   and cuc_desc30 <> ''Interco''                 
              group by  t.CustomerID          
                      
              UNION ALL        
                      
              SELECT                             
              t.CustomerID,Min(t.shp_os_slp) as SalePersonName                     
              FROM #tactive1 t, ' + @DB + '_arrcrd_rec join ' + @DB + '_arrcus_rec on crd_cus_id = cus_cus_id                       
              left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
              left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                        
                              
              where                       
              t.CustomerID Not IN            
              ( select distinct t1.CustomerID  from #tactive1 t1,                      
              ' + @DB + '_sahstn_rec u                   
              where t1.CustomerID = u.STN_SLD_CUS_ID                       
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + '''  )                      
              and t.CustomerID not in  (select coc_cus_id from ' + @DB + '_arbcoc_rec                   
                                
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''                      
              )                      
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''   and cuc_desc30 <> ''Interco''                 
              group by  t.CustomerID        
                      
              ) as ra group by ra.SalePersonName             
                     
                 Union       
                                  
                                  
              Select  t.SalePersonName,  0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, Sum(t.TotalPhoneCall) as phoneCall, 0 as visit, 0 as form, 0 as sapl from (                
                
   select cta_tsk_asgn_to as SalePersonName,  COUNT(*) as TotalPhoneCall                   
       from ' + @DB + '_cctcta_rec join                   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1   
         join '+ @DB +'_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to                                     
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                   
                
       and rtrim(atp_desc30) = ''Phone Call - Prospect Account''          
       group by  cta_tsk_asgn_to, atp_desc30  
         
       ) as t group by  t.SalePersonName              
               
                           
                        
    Union '   
      
     SET @sqltxt1 ='                    
                        
    Select  t.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, 0 as phoneCall, Sum(t.totalVisit) as visit, 0 as form, 0 as sapl from (               
               
select cta_tsk_asgn_to as SalePersonName,  COUNT(*) as totalVisit                   
       from ' + @DB + '_cctcta_rec join                   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to            
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                    
                
       and rtrim(atp_desc30) = ''Visit - Prospect Account''          
       group by  cta_tsk_asgn_to, atp_desc30  
          
       ) as t group by  t.SalePersonName          
                                  
    Union                     
                                  
   Select t.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, count(*) as form, 0 as sapl  from (                     
  select slp_lgn_id as SalePersonName,stn_frm as form                    
  from ' + @DB + '_sahstn_rec                   
  JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id   
     join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_is_slp                               
  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
  left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
  where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''   and cuc_desc30 <> ''Interco''                  
    group by slp_lgn_id, stn_frm       
          
    UNION ALL      
        
  select slp_lgn_id as SalePersonName,stn_frm as form                    
  from ' + @DB + '_sahstn_rec                   
  JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id     
     join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_os_slp                              
  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                      
  left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
  where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''   and cuc_desc30 <> ''Interco''                  
    group by slp_lgn_id, stn_frm         
    ) as t group by t.SalePersonName         
          
                               
   union                        
    Select oqq.SalePersonName, 0 as Newaccount, 0 as Lostaccount ,0 as activeAccount, 0 as reactiveAccount, 0 as GPAmt,                    
   0 as NPAmt, 0 as Weight, 0 as phoneCall, 0 as visit, 0 as form, COUNT(*) as sapl                     
   from (                    
   Select t.stn_sld_cus_id, t.SalePersonName, count(*) as form                     
   from (                     
    select STN_SLD_CUS_ID,slp_lgn_id as SalePersonName,stn_frm as form                    
    from ' + @DB + '_sahstn_rec                     
    JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_is_slp                             
       join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                    
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
   where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''    and cuc_desc30 <> ''Interco''                
   group by slp_lgn_id,STN_SLD_CUS_ID, stn_frm        
   ) as t  group by t.SalePersonName,t.stn_sld_cus_id    
       
   Union All    
       
   Select t.stn_sld_cus_id, t.SalePersonName, count(*) as form                     
   from (                     
    select STN_SLD_CUS_ID,slp_lgn_id as SalePersonName,stn_frm as form                    
    from ' + @DB + '_sahstn_rec                     
    JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id    
       join '+ @DB +'_scrslp_rec on slp_cmpy_id = shp_cmpy_id and slp_slp = shp_os_slp                              
       join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                    
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                   
   where STN_INV_DT > ''' + @FD + ''' AND STN_INV_DT <= ''' + @TD + '''    and cuc_desc30 <> ''Interco''                
   group by slp_lgn_id,STN_SLD_CUS_ID, stn_frm        
   ) as t  group by t.SalePersonName,t.stn_sld_cus_id    
       
    ) as oqq where oqq.form >= 4                    
   group by oqq.SalePersonName                                                      
       ) as oq group by oq.SalePersonName                       
       '                      
                             
        --print ('sql');                
        print(@sqltxt);                         
    set @execSQLtxt = @sqltxt + @sqltxt1;            
    print(@sqltxt1);                                      
   EXEC (@execSQLtxt);                       
    drop table #tactive;                        
  End                       
                      
                                         
                                     
             
     SELECT   salePerson, SUM(newAccount) as newAccount, SUM(lostAccount)as lostAccount ,SUM(activeAccount) as activeAccount                
     ,SUM(reactiveAccount) as reactiveAccount,SUM(GPAmt) as GPAmt, SUM(NPAmt) as NPAmt, SUM(lbsSold) as lbsSold,SUM(phoneCall) as phoneCall                
     ,SUM(visit) as visit,SUM(form) as form,SUM(sapl    ) as sapl                
     FROM #tmp -- where salePerson = 'jpudlock'     
     GROUP BY salePerson   order by salePerson                        
     DROP TABLE   #tmp ;                     
                          
END                                
                            
                           
                
--exec [sp_itech_salesRepAccountDetails_SRR] 'US','2015-04-01','2015-06-30'                         
--exec sp_itech_salesRepAccountDetails_SRR 'ALL','2015-09-01','2015-09-30' 
GO
