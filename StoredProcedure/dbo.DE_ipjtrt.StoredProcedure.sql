USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ipjtrt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  cdaigle   
-- Create date: Jan 27 2021  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_ipjtrt]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.DE_ipjtrt_rec', 'U') IS NOT NULL  
  drop table dbo.DE_ipjtrt_rec;  
      
          
SELECT   [trt_cmpy_id]
      ,[trt_ref_pfx]
      ,[trt_ref_no]
      ,[trt_ref_itm]
      ,[trt_ref_sbitm]
      ,[trt_tprod_wgt]
      ,[trt_tcons_wgt]
      ,[trt_tdrp_wgt]
      ,[trt_tmst_wgt]
      ,[trt_trjct_wgt]
      ,[trt_tscr_wgt]
      ,[trt_tetrm_wgt]
      ,[trt_tecut_wgt]
      ,[trt_tkrfls_wgt]
      ,[trt_tuscr_wgt]  
into  dbo.DE_ipjtrt_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[ipjtrt_rec];  
  
END  
  
-- exce DE_ipjtrt  
-- select * from DE_ipjtrt_rec 

  
GO
