USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_injitd_temp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Author,ITECH>                      
-- Create date: <Create Date, Jan 6 2021,>                      
-- Description: <Description,Open Orders,>                
-- =============================================                      
CREATE PROCEDURE [dbo].[US_injitd_temp]                      
AS                      
BEGIN                      
 -- SET NOCOUNT ON added to prevent extra result sets from                      
 -- interfering with SELECT statements.                      
 SET NOCOUNT ON;                            

Declare @itdItmCtlNo nvarchar(25)
select @itdItmCtlNo = max(itd_itm_ctl_no) from US_injitd_temp_rec
Print @itdItmCtlNo;

-- Insert statements for procedure here
insert into dbo.US_injitd_temp_rec 
select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,  
itd_itm_ctl_no,itd_brh,itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_whs,itd_mill,itd_heat,itd_ownr,itd_bgt_for, itd_invt_cat, itd_upd_dtts, itd_cst_qty_indc 
FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec] where itd_itm_ctl_no > @itdItmCtlNo;
                      
END                      
      
/*      

 */
GO
