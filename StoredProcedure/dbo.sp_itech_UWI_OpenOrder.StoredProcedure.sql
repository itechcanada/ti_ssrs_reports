USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_UWI_OpenOrder]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                    
-- Author:  <Mukesh>                    
-- Create date: <03-05-2019>                    
-- Description: <Booking to Invoice Ratio>                   
                
-- =============================================                    
CREATE PROCEDURE [dbo].[sp_itech_UWI_OpenOrder]  @DBNAME varchar(50), @Branch varchar(3)                  
AS                    
BEGIN                    
             
 -- SET NOCOUNT ON added to prevent extra result sets from                    
 SET NOCOUNT ON;                    
declare @DB varchar(100);                    
declare @sqltxt varchar(6000);                    
declare @execSQLtxt varchar(7000);                    
DECLARE @CountryName VARCHAR(25);                       
DECLARE @prefix VARCHAR(15);                       
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @CurrenyRate varchar(15);                  
declare @FD varchar(10)                      
declare @TD varchar(10)               
declare @TDInv varchar(10)      
declare @UPDate varchar(10)                           
                    
   set @FD = CONVERT(VARCHAR(10),  DATEADD(year, -1, GETDATE()),120) ;   -- One year back           
 set @TD = CONVERT(VARCHAR(10), DATEADD(year, 1, GETDATE()) ,120);      -- One year after      
 set @UPDate = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), GETDATE() + 15 ,120) ,120);      
          
IF @Branch = 'ALL'                    
 BEGIN                    
  set @Branch = ''                    
 END          
                  
                    
CREATE TABLE #temp ( CompanyId varchar(5)                     
        ,OrderPrefix Varchar(2)                    
        ,OrderNo varchar(10)                    
        ,OrderItem   varchar(3)                    
        ,CustID   VARCHAR(10)         
        , CustName     VARCHAR(65)          
        ,Branch varchar(10)        
        , DueDate    varchar(20)          
  ,BalWgt      DECIMAL(20, 2)          
        , ChargeValue    DECIMAL(20, 2)          
        , Salesperson     varchar(65)        
        ,Form  varchar(35)                      
        ,Grade  varchar(35)                      
        ,Size  varchar(35)                      
        ,Finish  varchar(35)                   
        , OrderType     varchar(65)          
     );                   
        
create table #tempUP (      
CmpyID varchar(5)      
,SOPfx varchar(2)      
,SONumber varchar(10)      
,SOItem Varchar(3)      
,UPrs1 Varchar(3)      
,UPrs2 Varchar(3)      
,UPrs3 Varchar(3)      
,UPrs4 Varchar(3)      
,UPrs5 Varchar(3)      
,UPrs6 Varchar(3)      
,UPCK int      
,USAW int      
,UPSW int      
,UBSW int      
,UWJT int      
)      
      
create table #tempWIP (      
WCmpyID varchar(5)      
,WJbsNo varchar(10)      
,WSOPfx varchar(2)      
,WSONumber varchar(10)      
,WSOItem Varchar(3)      
,WPrs1 Varchar(3)      
,WPrs2 Varchar(3)      
,WPrs3 Varchar(3)      
,WPrs4 Varchar(3)      
,WPrs5 Varchar(3)      
,WPrs6 Varchar(3)      
,WPrs7 Varchar(3)      
,WPrs8 Varchar(3)      
,WPrs9 Varchar(3)      
,WPCK int      
,WSAW int      
,WPSW int      
,WBSW int      
,WWJT int      
)      
      
      
      
                    
IF @DBNAME = 'ALL'                    
 BEGIN                    
                     
  DECLARE ScopeCursor CURSOR FOR                    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                     
    OPEN ScopeCursor;                    
                      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                    
  WHILE @@FETCH_STATUS = 0                    
  BEGIN                    
   DECLARE @query NVARCHAR(MAX);                    
   IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End                    
    Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End                    
                        
    SET @query = 'INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, CustName,Branch, DueDate, BalWgt, ChargeValue, Salesperson,Form,Grade,Size,Finish, OrderType)          
  select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_sld_cus_id as CustID,        
        cus_cus_long_nm as CustName,ord_ord_brh as Branch,orl_due_to_dt as DueDate, orl_bal_wgt * 2.20462 as BalWgt, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,           
    usr_nm as Salesperson,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)),cds_desc        
       from ' + @Prefix + '_ortord_rec,' + @Prefix + '_arrcus_rec ,              
       ' + @Prefix + '_ortorh_rec ,' + @Prefix + '_ortorl_rec,                  
        ' + @Prefix + '_tctipd_rec,         
       ' + @Prefix + '_ortchl_rec                  
       , ' + @Prefix + '_scrslp_rec ou        
       ,' + @Prefix + '_mxrusr_rec                  
       ,' + @Prefix + '_arrcuc_rec, ' + @Prefix + '_rprcds_rec ,         
        ' + @Prefix + '_ortcht_rec where                  
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id                  
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id                  
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                  
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                  
       and ou.slp_slp=orh_tkn_slp         
       and usr_lgn_id = ou.slp_lgn_id         
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ                
       AND               
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm             
        and cht_tot_typ = ''T''                  
       and orl_bal_qty >= 0                   
       and chl_chrg_cl= ''E''                  
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''                  
       and orh_sts_actn <> ''C''                  
       and ord_ord_pfx <>''QT''                   
       and  (               
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10), ''' + @TD + ''', 120)   )              
       or              
       (orl_due_to_dt is null and orh_ord_typ =''J''))              
                     
       AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')                
       and  ord_ord_brh not in (''SFS'')             
       and cds_desc in (''Normal Order'',''Job Detail Order'')                     order by ord_ord_no,ord_ord_itm,orl_due_to_dt        
         '                   
   print @query;                    
   EXECUTE sp_executesql @query;        
         
    SET @query = 'insert into #tempUP(CmpyID,SOPfx,SONumber,SOItem,UPrs1,UPrs2,UPrs3,UPrs4,UPrs5,UPrs6,UPCK,USAW,UPSW,UBSW,UWJT)       
 select ppl_cmpy_id, ppl_ord_pfx,ppl_ord_no,ppl_ord_itm,ppl_prs_1,ppl_prs_2,ppl_prs_3,ppl_prs_4,ppl_prs_5,ppl_prs_6,Case when ppl_prs_2 = '''' then 1 else 0 end as UPCK,      
 Case when (ppl_prs_2 = ''SAW'' OR ppl_prs_3 = ''SAW'' OR ppl_prs_4 = ''SAW'' OR ppl_prs_5 = ''SAW'' OR ppl_prs_6 = ''SAW'' ) then 1 else 0 end as USAW,      
 Case when (ppl_prs_2 = ''PSW'' OR ppl_prs_3 = ''PSW'' OR ppl_prs_4 = ''PSW'' OR ppl_prs_5 = ''PSW'' OR ppl_prs_6 = ''PSW'' ) then 1 else 0 end as UPSW,      
 Case when (ppl_prs_2 = ''BSW'' OR ppl_prs_3 = ''BSW'' OR ppl_prs_4 = ''BSW'' OR ppl_prs_5 = ''BSW'' OR ppl_prs_6 = ''BSW'' ) then 1 else 0 end as UBSW,      
 Case when (ppl_prs_2 = ''WJT'' OR ppl_prs_3 = ''WJT'' OR ppl_prs_4 = ''WJT'' OR ppl_prs_5 = ''WJT'' OR ppl_prs_6 = ''WJT'' ) then 1 else 0 end as UWJT      
FROM ' + @Prefix + '_ortppl_rec      
join ' + @Prefix + '_ortorl_rec   on  ppl_cmpy_id = orl_cmpy_id and ppl_ord_pfx = orl_ord_pfx and ppl_ord_no= orl_ord_no and ppl_ord_itm = orl_ord_itm      
and ppl_ord_rls_no=orl_ord_rls_no      
left join ' + @Prefix + '_ortipr_rec on  ppl_cmpy_id = ipr_cmpy_id and ppl_ord_pfx = ipr_ref_pfx and ppl_ord_no= ipr_ref_no      
and ppl_ord_itm = ipr_ref_itm and ppl_ref_ln_no_1=ipr_ref_ln_no and ipr_prs_seq_no = 1 and ipr_ref_sitm = 1      
where ISNULL(ipr_sch_strt_ltts,ppl_rdy_by_ltts) < ''' + @UPDate + ''' and ppl_plng_whs = '''+ @Branch +'''  '      
 print @query;                    
   EXECUTE sp_executesql @query;       
      
set @query = ' insert into #tempWIP(WCmpyID,WJbsNo,WSOPfx,WSONumber,WSOItem,WPrs1,WPrs2,WPrs3,WPrs4,WPrs5,WPrs6,WPrs7,WPrs8,WPrs9,WPCK,WSAW,WPSW,WBSW,WWJT)      
select rps_cmpy_id,rps_jbs_no, rps_ord_pfx, case when count(rps_jbs_no) > 1 then 0 else MAx(rps_ord_no) end as ordNO,      
case when count(rps_jbs_no) > 1 then 0 else MAx(rps_ord_itm) end as ordItem,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9,      
case when rps_prs_2 = '''' then 1 else 0 end as WPCK,      
Case When (rps_prs_2 = ''SAW'' OR rps_prs_3 = ''SAW'' OR rps_prs_4 = ''SAW''  OR rps_prs_5 = ''SAW'' OR rps_prs_6 = ''SAW'' OR rps_prs_7 = ''SAW'' OR rps_prs_8 = ''SAW''       
OR rps_prs_9 = ''SAW'' ) then 1 else 0 end as WSAW,      
Case When (rps_prs_2 = ''PSW'' OR rps_prs_3 = ''PSW'' OR rps_prs_4 = ''PSW''  OR rps_prs_5 = ''PSW'' OR rps_prs_6 = ''PSW'' OR rps_prs_7 = ''PSW'' OR rps_prs_8 = ''PSW''       
OR rps_prs_9 = ''PSW'' ) then 1 else 0 end as WPSW,      
Case When (rps_prs_2 = ''BSW'' OR rps_prs_3 = ''BSW'' OR rps_prs_4 = ''BSW''  OR rps_prs_5 = ''BSW'' OR rps_prs_6 = ''BSW'' OR rps_prs_7 = ''BSW'' OR rps_prs_8 = ''BSW''       
OR rps_prs_9 = ''BSW'' ) then 1 else 0 end as WBSW,      
Case When (rps_prs_2 = ''WJT'' OR rps_prs_3 = ''WJT'' OR rps_prs_4 = ''WJT''  OR rps_prs_5 = ''WJT'' OR rps_prs_6 = ''WJT'' OR rps_prs_7 = ''WJT'' OR rps_prs_8 = ''WJT''       
OR rps_prs_9 = ''WJT'' ) then 1 else 0 end as WWJT      
from (      
select distinct rps_cmpy_id,rps_jbs_no,rps_ord_pfx,rps_ord_no,rps_ord_itm,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9      
from ' + @Prefix + '_iptrps_rec       
join ' + @Prefix + '_iptjob_rec  on rps_cmpy_id = job_cmpy_id and rps_jbs_pfx = job_jbs_pfx and rps_jbs_no = job_jbs_no      
where job_sch_strt_dtts < ''' + @UPDate + ''' and job_actvy_whs = '''+ @Branch +'''      
) as ou      
group by rps_cmpy_id,rps_jbs_no,rps_ord_pfx,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9'      
      
print @query;                    
   EXECUTE sp_executesql @query;       
      
                     
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                  
  END                       
  CLOSE ScopeCursor;                    
  DEALLOCATE ScopeCursor;                    
 END                    
ELSE                    
BEGIN                    
                   
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)                    
 IF (UPPER(@DBNAME) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DBNAME) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End                    
    Else if(UPPER(@DBNAME) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End                    
    Else if(UPPER(@DBNAME) = 'TWCN')                    
    begin                    
       SET @DB ='TW'                    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
                        
                      
  SET @sqltxt ='INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, CustName,Branch, DueDate, BalWgt, ChargeValue, Salesperson,Form,Grade,Size,Finish, OrderType)          
  select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_sld_cus_id as CustID,        
        cus_cus_long_nm as CustName,ord_ord_brh as Branch,orl_due_to_dt as DueDate, orl_bal_wgt * 2.20462 as BalWgt, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,           
    usr_nm as Salesperson,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)),cds_desc        
       from ' + @DBNAME + '_ortord_rec,' + @DBNAME + '_arrcus_rec ,              
       ' + @DBNAME + '_ortorh_rec ,' + @DBNAME + '_ortorl_rec,                  
        ' + @DBNAME + '_tctipd_rec,         
       ' + @DBNAME + '_ortchl_rec                  
       , ' + @DBNAME + '_scrslp_rec ou        
       ,' + @DBNAME + '_mxrusr_rec                  
       ,' + @DBNAME + '_arrcuc_rec, ' + @DBNAME + '_rprcds_rec ,         
        ' + @DBNAME + '_ortcht_rec where                  
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id                  
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id                  
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                  
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                  
       and ou.slp_slp=orh_tkn_slp         
       and usr_lgn_id = ou.slp_lgn_id         
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ                
       AND               
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND     ord_ord_itm = cht_ref_itm             
        and cht_tot_typ = ''T''                  
       and orl_bal_qty >= 0                   
       and chl_chrg_cl= ''E''                  
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''                  
       and orh_sts_actn <> ''C''                  
       and ord_ord_pfx <>''QT''                   
       and  (               
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10), ''' + @TD + ''', 120)   )              
       or              
       (orl_due_to_dt is null and orh_ord_typ =''J''))              
                     
       AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')                
       and  ord_ord_brh not in (''SFS'')             
       and cds_desc in (''Normal Order'',''Job Detail Order'')              
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt        
       '                  
 print(@sqltxt)                    
 set @execSQLtxt = @sqltxt;                     
 EXEC (@execSQLtxt);         
       
 Set @sqltxt = 'insert into #tempUP(CmpyID,SOPfx,SONumber,SOItem,UPrs1,UPrs2,UPrs3,UPrs4,UPrs5,UPrs6,UPCK,USAW,UPSW,UBSW,UWJT)       
 select ppl_cmpy_id, ppl_ord_pfx,ppl_ord_no,ppl_ord_itm,ppl_prs_1,ppl_prs_2,ppl_prs_3,ppl_prs_4,ppl_prs_5,ppl_prs_6,Case when ppl_prs_2 = '''' then 1 else 0 end as UPCK,      
 Case when (ppl_prs_2 = ''SAW'' OR ppl_prs_3 = ''SAW'' OR ppl_prs_4 = ''SAW'' OR ppl_prs_5 = ''SAW'' OR ppl_prs_6 = ''SAW'' ) then 1 else 0 end as USAW,      
 Case when (ppl_prs_2 = ''PSW'' OR ppl_prs_3 = ''PSW'' OR ppl_prs_4 = ''PSW'' OR ppl_prs_5 = ''PSW'' OR ppl_prs_6 = ''PSW'' ) then 1 else 0 end as UPSW,      
 Case when (ppl_prs_2 = ''BSW'' OR ppl_prs_3 = ''BSW'' OR ppl_prs_4 = ''BSW'' OR ppl_prs_5 = ''BSW'' OR ppl_prs_6 = ''BSW'' ) then 1 else 0 end as UBSW,      
 Case when (ppl_prs_2 = ''WJT'' OR ppl_prs_3 = ''WJT'' OR ppl_prs_4 = ''WJT'' OR ppl_prs_5 = ''WJT'' OR ppl_prs_6 = ''WJT'' ) then 1 else 0 end as UWJT      
FROM ' + @DBNAME + '_ortppl_rec      
join ' + @DBNAME + '_ortorl_rec   on  ppl_cmpy_id = orl_cmpy_id and ppl_ord_pfx = orl_ord_pfx and ppl_ord_no= orl_ord_no and ppl_ord_itm = orl_ord_itm      
and ppl_ord_rls_no=orl_ord_rls_no      
left join ' + @DBNAME + '_ortipr_rec on  ppl_cmpy_id = ipr_cmpy_id and ppl_ord_pfx = ipr_ref_pfx and ppl_ord_no= ipr_ref_no      
and ppl_ord_itm = ipr_ref_itm and ppl_ref_ln_no_1=ipr_ref_ln_no and ipr_prs_seq_no = 1 and ipr_ref_sitm = 1      
where ISNULL(ipr_sch_strt_ltts,ppl_rdy_by_ltts) < ''' + @UPDate + ''' and ppl_plng_whs = '''+ @Branch +'''   ;       
 '      
print(@sqltxt)                    
 set @execSQLtxt = @sqltxt;                     
 EXEC (@execSQLtxt);        
      
set @sqltxt = ' insert into #tempWIP(WCmpyID,WJbsNo,WSOPfx,WSONumber,WSOItem,WPrs1,WPrs2,WPrs3,WPrs4,WPrs5,WPrs6,WPrs7,WPrs8,WPrs9,WPCK,WSAW,WPSW,WBSW,WWJT)      
select rps_cmpy_id,rps_jbs_no, rps_ord_pfx, case when count(rps_jbs_no) > 1 then 0 else MAx(rps_ord_no) end as ordNO,      
case when count(rps_jbs_no) > 1 then 0 else MAx(rps_ord_itm) end as ordItem,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9,      
case when rps_prs_2 = '''' then 1 else 0 end as WPCK,      
Case When (rps_prs_2 = ''SAW'' OR rps_prs_3 = ''SAW'' OR rps_prs_4 = ''SAW''  OR rps_prs_5 = ''SAW'' OR rps_prs_6 = ''SAW'' OR rps_prs_7 = ''SAW'' OR rps_prs_8 = ''SAW''       
OR rps_prs_9 = ''SAW'' ) then 1 else 0 end as WSAW,      
Case When (rps_prs_2 = ''PSW'' OR rps_prs_3 = ''PSW'' OR rps_prs_4 = ''PSW''  OR rps_prs_5 = ''PSW'' OR rps_prs_6 = ''PSW'' OR rps_prs_7 = ''PSW'' OR rps_prs_8 = ''PSW''       
OR rps_prs_9 = ''PSW'' ) then 1 else 0 end as WPSW,      
Case When (rps_prs_2 = ''BSW'' OR rps_prs_3 = ''BSW'' OR rps_prs_4 = ''BSW''  OR rps_prs_5 = ''BSW'' OR rps_prs_6 = ''BSW'' OR rps_prs_7 = ''BSW'' OR rps_prs_8 = ''BSW''       
OR rps_prs_9 = ''BSW'' ) then 1 else 0 end as WBSW,      
Case When (rps_prs_2 = ''WJT'' OR rps_prs_3 = ''WJT'' OR rps_prs_4 = ''WJT''  OR rps_prs_5 = ''WJT'' OR rps_prs_6 = ''WJT'' OR rps_prs_7 = ''WJT'' OR rps_prs_8 = ''WJT''       
OR rps_prs_9 = ''WJT'' ) then 1 else 0 end as WWJT      
from (      
select distinct rps_cmpy_id,rps_jbs_no,rps_ord_pfx,rps_ord_no,rps_ord_itm,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9      
from ' + @DBNAME + '_iptrps_rec       
join ' + @DBNAME + '_iptjob_rec  on rps_cmpy_id = job_cmpy_id and rps_jbs_pfx = job_jbs_pfx and rps_jbs_no = job_jbs_no      
where job_sch_strt_dtts < ''' + @UPDate + ''' and job_actvy_whs = '''+ @Branch +'''      
) as ou      
group by rps_cmpy_id,rps_jbs_no,rps_ord_pfx,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,rps_prs_9      
'      
print(@sqltxt)                    
 set @execSQLtxt = @sqltxt;                     
 EXEC (@execSQLtxt);       
       
END             
        
        
select *, (select COUNT(*) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as UNPlanned       
 ,(select COUNT(*) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WIP      
 ,(select ISNULL(sum(UPCK),0) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as UPCK      
 ,(select ISNULL(sum(USAW),0) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as USAW      
 ,(select ISNULL(sum(UPSW),0) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as UPSW      
 ,(select ISNULL(sum(UBSW),0) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as UBSW      
 ,(select ISNULL(sum(UWJT),0) from #tempUP where CmpyID=CompanyId and SOPfx = OrderPrefix and SONumber = OrderNo and SOItem = OrderItem ) as UWJT      
 ,(select ISNULL(sum(WPCK),0) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WPCK      
 ,(select ISNULL(sum(WSAW),0) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WSAW      
 ,(select ISNULL(sum(WPSW),0) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WPSW      
 ,(select ISNULL(sum(WBSW),0) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WBSW      
 ,(select ISNULL(sum(WWJT),0) from #tempWIP where WCmpyID=CompanyId and WSOPfx = OrderPrefix and WSONumber = OrderNo and WSOItem = OrderItem ) as WWJT      
       
from #temp;        
        
         
 DROP TABLE  #temp;        
 drop table #tempUP;          
 drop table #tempWIP;      
END                    
                    
                    
-- @DBNAME varchar(50), @Branch varchar(3)                    
-- EXEC [sp_itech_UWI_OpenOrder] 'ALL','MTL'  -- 125.99   ,558576            
/*            
   select ppl_cmpy_id, ppl_ord_pfx,ppl_ord_no,ppl_ord_itm      
FROM CA_ortppl_rec      
join CA_ortorl_rec   on  ppl_cmpy_id = orl_cmpy_id and ppl_ord_pfx = orl_ord_pfx and ppl_ord_no= orl_ord_no and ppl_ord_itm = orl_ord_itm      
and ppl_ord_rls_no=orl_ord_rls_no      
left join CA_ortipr_rec on  ppl_cmpy_id = ipr_cmpy_id and ppl_ord_pfx = ipr_ref_pfx and ppl_ord_no= ipr_ref_no      
and ppl_ord_itm = ipr_ref_itm and ppl_ref_ln_no_1=ipr_ref_ln_no and ipr_prs_seq_no = 1 and ipr_ref_sitm = 1      
where ISNULL(ipr_sch_strt_ltts,ppl_rdy_by_ltts) < '2019-06-27'        
-- and ppl_ord_no= '14357'      
order by ppl_ord_no;      
      
      
select rps_cmpy_id,rps_jbs_no,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8,      
case when count(rps_jbs_no) > 1 then 0 else MAx(rps_ord_no) end as ordNO      
from (      
select distinct rps_cmpy_id,rps_jbs_no,rps_ord_pfx,      
rps_ord_no,      
rps_ord_itm,      
rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8      
from [LIVECASTX].[livecastxdb].[informix].iptrps_rec       
join [LIVECASTX].[livecastxdb].[informix].iptjob_rec  on rps_cmpy_id = job_cmpy_id and rps_jbs_pfx = job_jbs_pfx and rps_jbs_no = job_jbs_no      
where job_sch_strt_dtts < '2019-06-27'       
) as ou      
--Where ou.rps_jbs_no = '27412'      
group by rps_cmpy_id,rps_jbs_no,rps_prs_1, rps_prs_2,rps_prs_3,rps_prs_4,rps_prs_5,rps_prs_6,rps_prs_7,rps_prs_8      
      
      
*/ 
GO
