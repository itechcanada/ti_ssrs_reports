USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_scrdtr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Jan 13, 2020    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[UK_scrdtr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.UK_scrdtr_rec', 'U') IS NOT NULL    
  drop table dbo.UK_scrdtr_rec;    
        
            
SELECT *    
into  dbo.UK_scrdtr_rec    
FROM [LIVEUKSTX].[liveukstxdb].[informix].[scrdtr_rec];    
    
END    
--- exec UK_scrpyt    
-- select * from UK_scrdtr_rec
GO
