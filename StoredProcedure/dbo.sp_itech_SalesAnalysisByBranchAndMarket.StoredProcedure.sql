USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisByBranchAndMarket]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
--Last Changes Date: <24 Dec 2014>
--Last Change Desc: < add new filter in market "ALL Market excluding Interco"
--Last Changes By: mukesh
--Last Changes Date: <13 Apr 2014>
--Last Change Desc: < change its column name crx_xex_rt to crx_xexrt
--Last Changes By: mukesh
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisByBranchAndMarket] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65),@version char = '0',@DateRange Varchar(3) = 'NON',@IncludePierce char = '0', @IncludeInterco char = '0' 

AS
BEGIN
	
	
	SET NOCOUNT ON;
	
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)
DECLARE @IsExcInterco char(1)

set @DB=  @DBNAME
IF (@DateRange = 'NON')
BEGIN
		set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
		set @TD = CONVERT(VARCHAR(10), @ToDate , 120)
END
ELSE IF (@DateRange = 'EOM')
BEGIN
		set @FD = CONVERT(VARCHAR(10),DATEADD(Month,DateDiff(Month,0,GetDATE()),0), 120) -- first date of current month
		set @TD = CONVERT(VARCHAR(10),DATEADD(Day, -1, DATEADD(Month,DateDiff(Month,0,GetDATE())+1,0)), 120) -- Last Date of Current Month
END
ELSE
BEGIN
		set @FD = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear    
		set @TD = CONVERT(VARCHAR(10),GETDATE(), 120)  --TodayOFCurrentYear
END

CREATE TABLE #tmp (   CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					,SalesPerson	VARCHAR(4)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
   					, Form           Varchar(65)
   					, Grade           Varchar(65)
   					, TotWeight		DECIMAL(20, 2)
   					, WeightUM           Varchar(10)
                    , TotalValue  	 DECIMAL(20, 2)
   					, NetGP               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, MatGP			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15) 
   					, MarketID	  integer
   					,stn_cry   varchar(5)
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);
DECLARE @CurrenyRate varchar(15);

if (@Market ='ExcInterco' OR @IncludeInterco = '0')
BEGIN
set @IsExcInterco ='T'
END

if @Market ='ALL' OR @Market ='ExcInterco'
 BEGIN
  set @Market = ''
 END

IF @DBNAME = 'ALL'
	BEGIN
		IF (@version = '0' OR @IncludePierce = '0')
		  BEGIN  
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
			OPEN ScopeCursor;  
		  END  
		  ELSE  
		  BEGIN  
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
			OPEN ScopeCursor;  
		  END  
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				
  				 IF (UPPER(@Prefix) = 'TW')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
								End
								Else if (UPPER(@Prefix) = 'NO')
								begin
									SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
								End
								Else if (UPPER(@Prefix) = 'CA')
								begin
									SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
								End
								Else if (UPPER(@Prefix) = 'CN')
								begin
									SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
								End
								Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')
								begin
									SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
								End
								Else if(UPPER(@Prefix) = 'UK')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
								End
								Else if(UPPER(@Prefix) = 'DE')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
								End
  				
  				
  				if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
                BEGIN
							  SET @query ='INSERT INTO #tmp (CustID,SalesPerson, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, stn_is_slp,cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''SFS'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,'
								if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
							      SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'
							      ELSE
							        SET @query = @query + '	SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,'
								
								SET @query = @query + '

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id,stn_is_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
				END
						 Else if  (UPPER(@Prefix) = 'PS')
						BEGIN
								SET @query ='INSERT INTO #tmp (CustID, SalesPerson,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select ''L'' + stn_sld_cus_id as CustID, stn_is_slp,cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''PSM'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, 
								stn_ord_wgt_um as WeightUM,

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  
								  group by  stn_sld_cus_id,stn_is_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
						
						Else
						BEGIN
								SET @query ='INSERT INTO #tmp (CustID, SalesPerson,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, stn_is_slp,cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''SFS'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, 
								stn_ord_wgt_um as WeightUM,

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  
								  group by  stn_sld_cus_id,stn_is_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
						
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			
  	  			print(@query)
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
				IF @version = '0'  
				 BEGIN    
				 Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')  
				 END
				 ELSE
				 BEGIN
				 Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')  
				 END
				 
			 IF (UPPER(@DBNAME) = 'TW')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
			End
			Else if (UPPER(@DBNAME) = 'NO')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
			End
			Else if (UPPER(@DBNAME) = 'CA')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
			End
			Else if (UPPER(@DBNAME) = 'CN')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
			End
			Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')
			begin
				SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
			End
			Else if(UPPER(@DBNAME) = 'UK')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
			End
			Else if(UPPER(@DBNAME) = 'DE')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
			End
			  
			if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
                BEGIN
							  SET @sqltxt ='INSERT INTO #tmp (CustID,SalesPerson, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID,stn_is_slp, cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''SFS'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,'
								if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
							      SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'
							      ELSE
							        SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt) as TotWeight,stn_ord_wgt_um as WeightUM,' 
								
							SET @sqltxt = @sqltxt + '	

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id,stn_is_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
				END
						 Else if (UPPER(@DBNAME) = 'PS' )
						BEGIN
								SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPerson,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select ''L'' + stn_sld_cus_id as CustID,stn_is_slp, cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''PSM'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  
								  group by  stn_sld_cus_id, stn_is_slp,cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
						
						Else
						BEGIN
								SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPerson,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID,stn_is_slp, cus_cus_nm as CustName, cuc_desc30 as Market,
								Replace(STN_SHPT_BRH,''SFS'',''LAX'') as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,

								SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue, 
								SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,
								SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue , 
								SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  
								  group by  stn_sld_cus_id, stn_is_slp,cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
						print(@sqltxt)
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END

if @IsExcInterco ='T'
 BEGIN
  SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPerson,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry,
     (Case SUM(ISNULL(TotalValue,0)) When 0 then 0 else (SUM(ISNULL(NetGP,0))/SUM(ISNULL(TotalValue,0))*100) end ) as 'NPPct' FROM #tmp Where Market <> 'Interco'
   group by CustID,SalesPerson, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry
   order by CustID 
END
ELSE
BEGIN
   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPerson,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry,
     (Case SUM(ISNULL(TotalValue,0)) When 0 then 0 else (SUM(ISNULL(NetGP,0))/SUM(ISNULL(TotalValue,0))*100) end ) as 'NPPct' FROM #tmp 
   group by CustID,SalesPerson, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry
   order by CustID 
   END
END
-- RTRIM(LTrim(CustID))+'-'+Databases as
-- exec sp_itech_SalesAnalysisByBranchAndMarket '09/23/2015', '11/23/2015' , 'PS','ALL',1

-- exec sp_itech_SalesAnalysisByBranchAndMarket_NP '09/23/2015', '11/23/2015' , 'PS','ALL',1


-- exec sp_itech_SalesAnalysisByBranchAndMarket '10/24/2014', '12/24/2014' , 'ALL','ExcInterco',1,'YTD'




GO
