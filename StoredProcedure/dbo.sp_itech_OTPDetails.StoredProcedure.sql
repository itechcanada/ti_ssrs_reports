USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OTPDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <18 Feb 2013>  
-- Description: <Getting OTP>  
--Last Changed Date: <11 Nov 2014>
--Use: This report use in Technical Report Name(OTP_Transfer_and_Regular.rdl)
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_OTPDetails] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID varchar(Max),@CustmomerNoTxt varchar(Max),@DateRange int, @version char = '0'  
  
AS  
BEGIN  



SET NOCOUNT ON;  
  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
  
CREATE TABLE #tmp (    CompanyName   VARCHAR(10)  
        , ShpgWhs     VARCHAR(65)  
        ,SPRCPFX          VARCHAR(35)  
        ,SprcNo          VARCHAR(35)  
        ,Orders          VARCHAR(35)  
        ,OrderNo          VARCHAR(35)  
        ,OrderItem          VARCHAR(35)  
        ,RlsNo          VARCHAR(35)  
        ,Branch          VARCHAR(35)  
        ,DueFromDt          VARCHAR(15)  
        ,Transport          VARCHAR(35)  
        ,TranspNO          VARCHAR(35)  
        ,shpgDates          VARCHAR(15)  
        ,LateShptWithReason          VARCHAR(2)  
        , Pcs    DECIMAL(20, 2)  
        , Wgt    DECIMAL(20, 2)  
        ,Late    integer  
        ,RsnType          VARCHAR(35)  
        ,RsnCode          VARCHAR(35)  
        ,Remarks          Text  
        ,OTPType    VARCHAR(35)   
        ,CustID      varchar(15)   
        ,CustName      Varchar(100)   
        --,TPRoute      Varchar(10)   
        --,TPAddress      Varchar(100) 
        ,Heat      Varchar(20)    
                 );  
               
DECLARE @company VARCHAR(35);  
DECLARE @prefix VARCHAR(15);   
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @CusID varchar(max);  
Declare @Value as varchar(500);  
DECLARE @CustIDLength int;  
  
if @CustmomerNoTxt <> ''  
 BEGIN  
  set @CustomerID = @CustmomerNoTxt  
 END  
  
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));  
  
  
if @CustomerID = ''  
 BEGIN  
  set @CustomerID = '0'  
 END  
  
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month  
 End  
else if @DateRange=2 -- Last 7 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=3 -- Last 14 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=4 --Last 12 months excluding all the days in the current month  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month  
 End  
else  
 Begin  
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
 End  
  
        --->> Input customer data  
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)  
  SELECT @pos=0  
  SELECT @input =@CustomerID   
  SELECT @input = @input + ','  
  CREATE TABLE #tempTable (temp varchar(max) )  
  WHILE CHARINDEX(',',@input) > 0  
  BEGIN  
   SELECT @pos=CHARINDEX(',',@input)  
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))  
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))  
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)  
  END  
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable  
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)  
  DROP TABLE #tempTable  
        
 IF @DBNAME = 'ALL'  
 BEGIN  
 IF @version = '0'  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
  END  
  ELSE  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
  END  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       --set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'    
        
      --Regular  
      SET @query =  
          'INSERT INTO #tmp (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
          Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat)  
         select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'', dpf_sprc_pfx as ''SPRCPFX'',Max(dpf_sprc_no) as ''SprcNo'',   
       dpf_ord_pfx as ''Orders'', dpf_ord_no as ''OrderNo'',count(dpf_ord_itm) as ''OrderItem'',  
       dpf_ord_rls_no as ''RlsNo'', dpf_ord_brh as ''Branch'',CONVERT(VARCHAR(10), Max(dpf_rls_due_fm_dt), 120)  as ''DueFromDt'',  
       dpf_transp_pfx as ''Transport'',dpf_transp_no as ''TranspNO'',CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)  as ''shpgDates'' ,  
       sum(dpf_shp_pcs) as ''Pcs'', sum(dpf_shp_wgt) as ''Wgt'',  
       max(dpf_lte_shpt) as ''Late'', '''' as LateShptWithReason,  
       Max(dpf_rsn_typ) as ''RsnType'' ,max(dpf_rsn) as ''RsnCode'',Max(dpf_rsn_rmk) as ''Remarks'' ,''Regular'' as OTPType  
       ,ord_sld_cus_id as CustID, cus_cus_long_nm as CustName,itd_heat  
       from '+ @prefix +'_pfhdpf_rec  
       JOIN '+ @prefix +'_ortorl_rec   
       ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no  
       Join '+ @prefix +'_ortord_rec  
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
       join '+ @prefix +'_arrcus_rec  
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id  
       join '+ @prefix +'_injitd_rec
                 on ord_ord_no = itd_prnt_no  and ord_ord_pfx = itd_prnt_pfx and ord_ord_itm = itd_prnt_itm and ord_cmpy_id = itd_cmpy_id         
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)  
       and dpf_sprc_pfx = ''SH'''  
         
         
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'   
        END  
       Else  
        BEGIN  
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'  
        END  
      END  
      Set @query += ' group by  dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,    
          dpf_ord_pfx, dpf_ord_no,   
          dpf_ord_rls_no , dpf_ord_brh ,  
          dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),ord_sld_cus_id ,cus_cus_long_nm,itd_heat'  
          
          
        EXECUTE sp_executesql @query;  
        -- Transfer  
          SET @query ='INSERT INTO #tmp (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat)  
                   SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',b.tud_sprc_pfx as ''SPRCPFX'',MAx(b.tud_sprc_no) as ''SprcNo'','''' as Orders,   
                   '''' as OrderNo,'''' as OrderItem,'''' as RlsNo,b.tud_trpln_whs as Branch,  
                     CONVERT(VARCHAR(10), MAX(c.orl_due_to_dt), 120) as ''DueFromDt'',    
                    a.tph_transp_pfx as ''Transfer'', a.tph_transp_no as ''Transfer No'', CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) as ''shpgDates'' ,    
        Sum(b.tud_comp_pcs) as ''Pcs'', Sum(b.tud_comp_wgt) as ''Wgt'',   
      case when (a.tph_sch_dtts > MAX(c.orl_due_to_dt)) then 1 else 0 end as ''Late'','''' as LateShptWithReason,'''' as RsnType,'''' as RsnCode,'''' as Remarks,  
      ''Transfer'' as OTPType  
      ,Max(ord_sld_cus_id) as CustID, max(cus_cus_long_nm) as CustName,itd_heat
      FROM ['+ @prefix +'_trjtph_rec] a  
      left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no  
      left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  
      left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
      left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
      left join '+ @prefix +'_injitd_rec
                 on ord_ord_no = itd_prnt_no  and ord_ord_pfx = itd_prnt_pfx and ord_ord_itm = itd_prnt_itm and ord_cmpy_id = itd_cmpy_id   
      WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''  
      AND(  
      (b.tud_prnt_no<>0   
      and TPH_NBR_STP =1   
      and b.tud_sprc_pfx<>''SH''   
      and b.tud_sprc_pfx <>''IT'')    
      OR   
      (b.tud_sprc_pfx=''IT'')  
      )'  
        
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'   
        END  
       Else  
        BEGIN  
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'  
        END  
      END  
      Set @query += ' Group by tph_CMPY_ID ,b.tud_sprc_pfx ,  
          b.tud_trpln_whs ,    
          a.tph_transp_pfx, a.tph_transp_no , CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) ,   
          a.tph_sch_dtts,itd_heat '  
       
        EXECUTE sp_executesql @query;  
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
     Set @prefix= @DBNAME   
      SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
       Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat)  
      select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'', dpf_sprc_pfx as ''SPRCPFX'',Max(dpf_sprc_no) as ''SprcNo'',   
    dpf_ord_pfx as ''Orders'', dpf_ord_no as ''OrderNo'',count(dpf_ord_itm) as ''OrderItem'',  
    dpf_ord_rls_no as ''RlsNo'', dpf_ord_brh as ''Branch'',CONVERT(VARCHAR(10), Max(dpf_rls_due_fm_dt), 120)  as ''DueFromDt'',  
    dpf_transp_pfx as ''Transport'',dpf_transp_no as ''TranspNO'',CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)  as ''shpgDates'' ,  
    sum(dpf_shp_pcs) as ''Pcs'', sum(dpf_shp_wgt) as ''Wgt'',  
    max(dpf_lte_shpt) as ''Late'', '''' as LateShptWithReason,  
    Max(dpf_rsn_typ) as ''RsnType'' ,max(dpf_rsn) as ''RsnCode'',Max(dpf_rsn_rmk) as ''Remarks'' ,''Regular'' as OTPType  
    ,ord_sld_cus_id as CustID, cus_cus_long_nm as CustName,itd_heat
    from '+ @prefix +'_pfhdpf_rec  
    JOIN '+ @prefix +'_ortorl_rec   
                ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no  
                Join '+ @prefix +'_ortord_rec  
                on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
				join '+ @prefix +'_arrcus_rec  
                on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id 
                join '+ @prefix +'_injitd_rec
                 on ord_ord_no = itd_prnt_no  and ord_ord_pfx = itd_prnt_pfx and ord_ord_itm = itd_prnt_itm and ord_cmpy_id = itd_cmpy_id  
    where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)  
    and dpf_sprc_pfx = ''SH'''  
    IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'   
        END  
       Else  
        BEGIN  
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'  
        END  
      END  
      Set @sqltxt += ' group by  dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,    
          dpf_ord_pfx, dpf_ord_no,   
          dpf_ord_rls_no , dpf_ord_brh ,  
          dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),ord_sld_cus_id ,cus_cus_long_nm,itd_heat'  
    print(@sqltxt)  
     
  set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
 -- Transfer  
  SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat)  
                   SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',b.tud_sprc_pfx as ''SPRCPFX'',MAx(b.tud_sprc_no) as ''SprcNo'','''' as Orders,   
                   '''' as OrderNo,'''' as OrderItem,'''' as RlsNo,b.tud_trpln_whs as Branch,  
                     CONVERT(VARCHAR(10),MAX(c.orl_due_to_dt), 120) as ''DueFromDt'',    
                    a.tph_transp_pfx as ''Transfer'', a.tph_transp_no as ''Transfer No'', CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) as ''shpgDates'' ,    
        Sum(b.tud_comp_pcs) as ''Pcs'', Sum(b.tud_comp_wgt) as ''Wgt'',   
      case when (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'','''' as LateShptWithReason,'''' as RsnType,'''' as RsnCode,'''' as Remarks,  
      ''Transfer'' as OTPType  
      ,Max(ord_sld_cus_id) as CustID, max(cus_cus_long_nm) as CustName, itd_heat
      FROM ['+ @prefix +'_trjtph_rec] a  
      left  JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no  
      left  JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  
      left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
      left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id 
      left join '+ @prefix +'_injitd_rec
                 on ord_ord_no = itd_prnt_no  and ord_ord_pfx = itd_prnt_pfx and ord_ord_itm = itd_prnt_itm and ord_cmpy_id = itd_cmpy_id  
      WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''  
      AND(  
      (b.tud_prnt_no<>0   
      and TPH_NBR_STP =1   
      and b.tud_sprc_pfx<>''SH''   
      and b.tud_sprc_pfx <>''IT'')    
      OR   
      (b.tud_sprc_pfx=''IT'')  
      )'  
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
            
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'   
        END  
       Else  
        BEGIN  
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'  
        END  
      END  
      Set @sqltxt += ' Group by tph_CMPY_ID ,b.tud_sprc_pfx ,  
          b.tud_trpln_whs ,    
          a.tph_transp_pfx, a.tph_transp_no , CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) ,   
          a.tph_sch_dtts,itd_heat '  
     
   print(@sqltxt)  
   set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
     END                 
--select CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
--               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType  
--               FROM (SELECT  
--         ROW_NUMBER() OVER ( PARTITION BY TranspNO,OTPType ORDER BY DueFromDt DESC ) AS 'RowNumber',  
--         CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
--               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType  
--      FROM #tmp  
--      ) dt  
--WHERE RowNumber <= 1  
  select * from #tmp  
drop table #tmp  
  
  
END  
  
-- exec sp_itech_OTPdetails '01/01/2013', '04/30/2013' , 'US','1111111111','',5   
--exec sp_itech_OTPdetails '10/01/2014', '10/30/2014' , 'US','','',5   
  
  
  
  
  
--select  dpf_cmpy_id as 'CompanyName',dpf_shpg_whs as 'ShpgWhs', dpf_sprc_pfx as 'SPRCPFX',Max(dpf_sprc_no) as 'SprcNo',   
--    dpf_ord_pfx as 'Orders', dpf_ord_no as 'OrderNo',count(dpf_ord_itm) as 'OrderItem',  
--    dpf_ord_rls_no as 'RlsNo', dpf_ord_brh as 'Branch',CONVERT(VARCHAR(10), Max(dpf_rls_due_fm_dt), 120)  as 'DueFromDt',  
--    dpf_transp_pfx as 'Transport',dpf_transp_no as 'TranspNO',CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)  as 'shpgDates' ,  
--    sum(dpf_shp_pcs) as 'Pcs', sum(dpf_shp_wgt) as 'Wgt',  
--    max(dpf_lte_shpt) as 'Late', '' as LateShptWithReason,  
--    Max(dpf_rsn_typ) as 'RsnType' ,max(dpf_rsn) as 'RsnCode',Max(dpf_rsn_rmk) as 'Remarks' ,'Regular' as OTPType  
--    from US_pfhdpf_rec  
--    JOIN US_ortorl_rec   
--                ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no  
--    where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '2013-01-01', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '2013-04-10', 120)  
--    and dpf_sprc_pfx = 'SH'  
--    group by  dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,    
--    dpf_ord_pfx, dpf_ord_no,   
--    dpf_ord_rls_no , dpf_ord_brh ,  
--    dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)
GO
