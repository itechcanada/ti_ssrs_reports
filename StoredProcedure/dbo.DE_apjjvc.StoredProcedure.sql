USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_apjjvc]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_apjjvc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.DE_apjjvc_rec', 'U') IS NOT NULL        
  drop table dbo.DE_apjjvc_rec;        
            
                
SELECT *        
into  dbo.DE_apjjvc_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[apjjvc_rec]  
  
  
END  
  
GO
