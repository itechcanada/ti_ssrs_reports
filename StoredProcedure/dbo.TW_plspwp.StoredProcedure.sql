USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_plspwp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 26, 2014,>  
-- Description: <Description,  >  
  
-- =============================================  
Create PROCEDURE [dbo].[TW_plspwp]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_plspwp_rec', 'U') IS NOT NULL  
  drop table dbo.TW_plspwp_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_plspwp_rec  
  from [LIVETW_IW].[livetwstxdb_iw].[informix].[plspwp_rec] ;   
    
END  
-- select * from TW_plspwp_rec
GO
