USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_inrfrm]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Oct 30, 2020>  
-- Description: <Description,Germany Form>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_inrfrm]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
-- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.DE_inrfrm_rec', 'U') IS NOT NULL  
  drop table dbo.DE_inrfrm_rec;  
      
SELECT *  
into  dbo.DE_inrfrm_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[inrfrm_rec];  
  
END  
-- select * from DE_inrfrm_rec  
GO
