USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_orrprs]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author, Sumit>
-- Create date: <Create Date,Jan 27 2021,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[DE_orrprs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.DE_orrprs_rec', 'U') IS NOT NULL
		drop table dbo.DE_orrprs_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.DE_orrprs_rec
  from [LIVEDESTX].[livedestxdb].[informix].[orrprs_rec] ; 
  
END
-- select * from DE_orrprs_rec
GO
