USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_cprchp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Sumit    
-- Create date:Jan 27 2021    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_cprchp]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.DE_cprchp_rec', 'U') IS NOT NULL    
  drop table dbo.DE_cprchp_rec;    
        
            
SELECT *    
into  dbo.DE_cprchp_rec    
FROM [LIVEDESTX].[livedestxdb].[informix].[cprchp_rec];    
    
END    
    
-- select * from DE_cprchp_rec
GO
