USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[EA_SALES_HISTORY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/7/2012>
-- Description:	<Description,EARD/EAHL Report >
-- =============================================
CREATE PROCEDURE [dbo].[EA_SALES_HISTORY]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.itd_mill, c.itd_heat, a.cus_cus_id,a.cus_cus_long_nm, b.sat_upd_ref, b.sat_upd_ref_itm,b.sat_inv_dt,b.sat_blg_msr,b.sat_blg_wgt,
	 b.sat_tot_mtl_val, b.sat_frm, b.sat_grd, b.sat_size, b.sat_fnsh
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcus_rec] a 
	INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
	INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec] c ON b.sat_sprc_pfx=c.itd_ref_pfx AND b.sat_sprc_no=c.itd_ref_no
WHERE
b.sat_frm LIKE 'EA%' AND
CAST(b.sat_inv_dt AS datetime) BETWEEN @FromDate AND @ToDate
END

GO
