USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_potpoh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: Feb 05, 2016    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[CN_potpoh]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.CN_potpoh_rec', 'U') IS NOT NULL    
  drop table dbo.CN_potpoh_rec;    
        
            
SELECT *    
into  dbo.CN_potpoh_rec    
FROM [LIVECNSTX].[livecnstxdb].[informix].[potpoh_rec];    
    
END    
--  exec  CN_potpoh    
-- select * from CN_potpoh_rec
GO
