USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_IRM_Report]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh>        
-- Create date: <12 Sep 2014>        
-- Description: <IRM_Report> 
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database       
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_IRM_Report]  @DBNAME varchar(50),@FromDate datetime,@Whs Varchar(10),@CustType Varchar(2)  --,@Frm Varchar(6), @Grd Varchar(8), @Size Varchar(15), @Fnsh Varchar(8)      
AS        
BEGIN        

Return;

 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
declare @DB varchar(100);        
declare @sqltxt varchar(8000);        
declare @execSQLtxt varchar(8000);        
declare @sqltxt1 varchar(6000);        
declare @execSQLtxt1 varchar(6000);        
DECLARE @prefix VARCHAR(15);          
DECLARE @CountryName VARCHAR(25);          
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @CurrenyRate varchar(15);          
declare @FD varchar(10)        
        
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)      
        
CREATE TABLE #temp ( Dbname    VARCHAR(10)        
     --,CusID    VARCHAR(10)        
     ,Branch    VARCHAR(3)        
     ,ActvyDT   VARCHAR(10)        
     ,Product  VARCHAR(100)        
     --,OrderPrefix  VARCHAR(100)        
     ,OrderNo   VARCHAR(20)     
     ,PONumber  VARCHAR(75)      
     ,Part   Varchar(100)  
      ,StkSDFQty1   DECIMAL(20, 2)        
      ,StkSDFQty2   DECIMAL(20, 2)        
      ,StkSDFQty3   DECIMAL(20, 2)        
      ,StkSDFQty4   DECIMAL(20, 2)        
      ,StkSDFQty5   DECIMAL(20, 2)        
      ,StkSDFQty6   DECIMAL(20, 2)        
      ,StkSDFQty7   DECIMAL(20, 2)        
      ,StkSDFQty8   DECIMAL(20, 2)        
      ,StkSDFQty9   DECIMAL(20, 2)        
      ,StkSDFQty10   DECIMAL(20, 2)        
      ,StkSDFQty11   DECIMAL(20, 2)        
      ,StkSDFQty12   DECIMAL(20, 2)        
      ,StkSDFQty13   DECIMAL(20, 2)        
      ,StkSDFQty14   DECIMAL(20, 2)        
      ,StkSDFQty15   DECIMAL(20, 2)
      ,MimStkQty1   DECIMAL(20, 2)        
      ,MimStkQty2   DECIMAL(20, 2)        
      ,MimStkQty3   DECIMAL(20, 2)        
      ,MimStkQty4   DECIMAL(20, 2)        
      ,MimStkQty5   DECIMAL(20, 2)        
      ,MimStkQty6   DECIMAL(20, 2)        
      ,MimStkQty7   DECIMAL(20, 2)        
      ,MimStkQty8   DECIMAL(20, 2)        
      ,MimStkQty9   DECIMAL(20, 2)        
      ,MimStkQty10   DECIMAL(20, 2)        
      ,MimStkQty11   DECIMAL(20, 2)        
      ,MimStkQty12   DECIMAL(20, 2)        
      ,MimStkQty13   DECIMAL(20, 2)        
      ,MimStkQty14   DECIMAL(20, 2)        
      ,MimStkQty15   DECIMAL(20, 2)   
      ,NPosQty1   DECIMAL(20, 0)        
      ,NPosQty2   DECIMAL(20, 0)        
      ,NPosQty3   DECIMAL(20, 0)        
      ,NPosQty4   DECIMAL(20, 0)        
      ,NPosQty5   DECIMAL(20, 0)        
      ,NPosQty6   DECIMAL(20, 0)        
      ,NPosQty7   DECIMAL(20, 0)        
      ,NPosQty8   DECIMAL(20, 0)        
      ,NPosQty9   DECIMAL(20, 0)        
      ,NPosQty10   DECIMAL(20, 0)        
      ,NPosQty11   DECIMAL(20, 0)        
      ,NPosQty12   DECIMAL(20, 0)        
      ,NPosQty13   DECIMAL(20, 0)        
      ,NPosQty14   DECIMAL(20, 0)        
      ,NPosQty15   DECIMAL(20, 0)   
      ,OOBalQty1   DECIMAL(20, 0)        
      ,OOBalQty2   DECIMAL(20, 0)        
      ,OOBalQty3   DECIMAL(20, 0)        
      ,OOBalQty4   DECIMAL(20, 0)        
      ,OOBalQty5   DECIMAL(20, 0)        
      ,OOBalQty6   DECIMAL(20, 0)        
      ,OOBalQty7   DECIMAL(20, 0)        
      ,OOBalQty8   DECIMAL(20, 0)        
      ,OOBalQty9   DECIMAL(20, 0)        
      ,OOBalQty10   DECIMAL(20, 0)        
      ,OOBalQty11   DECIMAL(20, 0)        
      ,OOBalQty12   DECIMAL(20, 0)        
      ,OOBalQty13   DECIMAL(20, 0)        
      ,OOBalQty14   DECIMAL(20, 0)        
      ,OOBalQty15   DECIMAL(20, 0) 
     );        
CREATE TABLE #temp1 ( Dbname    VARCHAR(10)        
     ,Branch    VARCHAR(3)        
     ,ActvyDT   VARCHAR(10)        
     ,Product  VARCHAR(100)        
     ,OrderNo   VARCHAR(20)     
     ,PONumber  VARCHAR(75)      
     ,Part   Varchar(100)  
      ,StkSDFQty16   DECIMAL(20, 0)        
      ,StkSDFQty17   DECIMAL(20, 0)        
      ,StkSDFQty18   DECIMAL(20, 0)        
      ,StkSDFQty19   DECIMAL(20, 0)        
      ,StkSDFQty20   DECIMAL(20, 0)        
      ,StkSDFQty21   DECIMAL(20, 0)        
      ,StkSDFQty22   DECIMAL(20, 0)        
      ,StkSDFQty23   DECIMAL(20, 0)        
      ,StkSDFQty24   DECIMAL(20, 0)        
      ,StkSDFQty25   DECIMAL(20, 0)        
      ,StkSDFQty26   DECIMAL(20, 0)        
      ,StkSDFQty27   DECIMAL(20, 0)        
      ,StkSDFQty28   DECIMAL(20, 0)        
      ,StkSDFQty29   DECIMAL(20, 0)        
      ,StkSDFQty30   DECIMAL(20, 0)
      ,MimStkQty16   DECIMAL(20, 0)        
      ,MimStkQty17   DECIMAL(20, 0)        
      ,MimStkQty18   DECIMAL(20, 0)        
      ,MimStkQty19   DECIMAL(20, 0)        
      ,MimStkQty20   DECIMAL(20, 0)        
      ,MimStkQty21   DECIMAL(20, 0)        
      ,MimStkQty22   DECIMAL(20, 0)        
      ,MimStkQty23   DECIMAL(20, 0)        
      ,MimStkQty24   DECIMAL(20, 0)        
      ,MimStkQty25   DECIMAL(20, 0)        
      ,MimStkQty26   DECIMAL(20, 0)        
      ,MimStkQty27   DECIMAL(20, 0)        
      ,MimStkQty28   DECIMAL(20, 0)        
      ,MimStkQty29   DECIMAL(20, 0)        
      ,MimStkQty30   DECIMAL(20, 0)
      ,NPosQty16   DECIMAL(20, 0)        
      ,NPosQty17   DECIMAL(20, 0)        
      ,NPosQty18   DECIMAL(20, 0)        
      ,NPosQty19   DECIMAL(20, 0)        
      ,NPosQty20   DECIMAL(20, 0)        
      ,NPosQty21   DECIMAL(20, 0)        
      ,NPosQty22   DECIMAL(20, 0)        
      ,NPosQty23   DECIMAL(20, 0)        
      ,NPosQty24   DECIMAL(20, 0)        
      ,NPosQty25   DECIMAL(20, 0)        
      ,NPosQty26   DECIMAL(20, 0)        
      ,NPosQty27   DECIMAL(20, 0)        
      ,NPosQty28   DECIMAL(20, 0)        
      ,NPosQty29   DECIMAL(20, 0)        
      ,NPosQty30   DECIMAL(20, 0)
      ,OOBalQty16   DECIMAL(20, 0)        
      ,OOBalQty17   DECIMAL(20, 0)        
      ,OOBalQty18   DECIMAL(20, 0)        
      ,OOBalQty19   DECIMAL(20, 0)        
      ,OOBalQty20   DECIMAL(20, 0)        
      ,OOBalQty21   DECIMAL(20, 0)        
      ,OOBalQty22   DECIMAL(20, 0)        
      ,OOBalQty23   DECIMAL(20, 0)        
      ,OOBalQty24   DECIMAL(20, 0)        
      ,OOBalQty25   DECIMAL(20, 0)        
      ,OOBalQty26   DECIMAL(20, 0)        
      ,OOBalQty27   DECIMAL(20, 0)        
      ,OOBalQty28   DECIMAL(20, 0)        
      ,OOBalQty29   DECIMAL(20, 0)        
      ,OOBalQty30   DECIMAL(20, 0)
        );   
        
      CREATE TABLE #temp2 ( Dbname    VARCHAR(10)        
     ,Branch    VARCHAR(3)        
     ,ActvyDT   VARCHAR(10)        
     ,Product  VARCHAR(100)        
     ,OrderNo   VARCHAR(20)     
     ,PONumber  VARCHAR(75)      
     ,Part   Varchar(100)  
      ,StkSDFQty31   DECIMAL(20, 0)        
      ,StkSDFQty32   DECIMAL(20, 0)        
      ,StkSDFQty33   DECIMAL(20, 0)        
      ,StkSDFQty34   DECIMAL(20, 0)        
      ,StkSDFQty35   DECIMAL(20, 0)        
      ,StkSDFQty36   DECIMAL(20, 0)        
      ,StkSDFQty37   DECIMAL(20, 0)        
      ,MimStkQty31   DECIMAL(20, 0)        
      ,MimStkQty32   DECIMAL(20, 0)        
      ,MimStkQty33   DECIMAL(20, 0)        
      ,MimStkQty34   DECIMAL(20, 0)        
      ,MimStkQty35   DECIMAL(20, 0)        
      ,MimStkQty36   DECIMAL(20, 0)        
      ,MimStkQty37   DECIMAL(20, 0)
      ,NPosQty31   DECIMAL(20, 0)        
      ,NPosQty32   DECIMAL(20, 0)        
      ,NPosQty33   DECIMAL(20, 0)        
      ,NPosQty34   DECIMAL(20, 0)        
      ,NPosQty35   DECIMAL(20, 0)        
      ,NPosQty36   DECIMAL(20, 0)        
      ,NPosQty37   DECIMAL(20, 0)        
      ,OOBalQty31   DECIMAL(20, 0)        
      ,OOBalQty32   DECIMAL(20, 0)        
      ,OOBalQty33   DECIMAL(20, 0)        
      ,OOBalQty34   DECIMAL(20, 0)        
      ,OOBalQty35   DECIMAL(20, 0)        
      ,OOBalQty36   DECIMAL(20, 0)        
      ,OOBalQty37   DECIMAL(20, 0)        
        );        
     

        
IF @DBNAME = 'ALL'        
 BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   DECLARE @query NVARCHAR(MAX);        
   IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
     
          
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')        
    BEGIN        
    SET @query = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @Prefix +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,  wrw_
  
wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1      
  FROM ' + @Prefix + '_ortmbk_rec a        
      join ' + @Prefix + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @Prefix + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @Prefix + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  --where orl_bal_wgt > 0 '    
                 
    END        
    ELSE        
    BEGIN        
      SET @query = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @Prefix +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,  wrw_
  
wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1      
  FROM ' + @Prefix + '_ortmbk_rec a        
      join ' + @Prefix + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @Prefix + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @Prefix + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''')  '  
  --where orl_bal_wgt > 0 '       
    END        
   print @query;        
   EXECUTE sp_executesql @query;        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  END           
  CLOSE ScopeCursor;        
  DEALLOCATE ScopeCursor;        
 END        
ELSE        
BEGIN      
 IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DBNAME ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix = + ''+ @DBNAME + '')     
     print @DatabaseName;     
     
     --Select * from tbl_itech_DatabaseName_PS;    
     --select top 10 * from [LIVECASTX].[livecastxdb].[informix].plswrw_rec    
     --[LIVEUS_IW].[liveusstxdb_iw]    
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR         
   BEGIN        
   SET @sqltxt = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @DBNAME +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,
        wrw_wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1     
  FROM ' + @DBNAME + '_ortmbk_rec a        
      join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''')  '  
  --where orl_bal_wgt > 0 '       
  END        
  ELSE        
  BEGIN        
      
--   SET @sqltxt = 'INSERT INTO #temp (Dbname, OrderNo,PONumber, Part,Branch, ActvyDT, Product, StkSDFQty1,StkSDFQty2,StkSDFQty3,StkSDFQty4,StkSDFQty5,StkSDFQty6,StkSDFQty7,StkSDFQty8,StkSDFQty9,StkSDFQty10,StkSDFQty11,StkSDFQty12,StkSDFQty13,StkSDFQty14,
--   StkSDFQty15,MimStkQty1,MimStkQty2,MimStkQty3,MimStkQty4,MimStkQty5,MimStkQty6,MimStkQty7,MimStkQty8,MimStkQty9,MimStkQty10,MimStkQty11,MimStkQty12,MimStkQty13,MimStkQty14,
--   MimStkQty15)        
--      SELECT  distinct '''+ @DBNAME +''',mbk_ord_no,ord_cus_po,clg_part, wrw_whs,wrw_run_dt,RTRIM(mbk_frm) + ''-'' + RTRIM(mbk_grd) + ''-'' + RTRIM(mbk_size) + ''-'' + RTRIM(mbk_fnsh),wrw_stk_sdf_qty_1,wrw_stk_sdf_qty_2,wrw_stk_sdf_qty_3,wrw_stk_sdf_qty_4
--,wrw_stk_sdf_qty_5,wrw_stk_sdf_qty_6,wrw_stk_sdf_qty_7,wrw_stk_sdf_qty_8,wrw_stk_sdf_qty_9,wrw_stk_sdf_qty_10,wrw_stk_sdf_qty_11,wrw_stk_sdf_qty_12,wrw_stk_sdf_qty_13,wrw_stk_sdf_qty_14,wrw_stk_sdf_qty_15,
--wrw_mstk_qty_1,wrw_mstk_qty_2,wrw_mstk_qty_3,wrw_mstk_qty_4
--,wrw_mstk_qty_5,wrw_mstk_qty_6,wrw_mstk_qty_7,wrw_mstk_qty_8,wrw_mstk_qty_9,wrw_mstk_qty_10,wrw_mstk_qty_11,wrw_mstk_qty_12,wrw_mstk_qty_13,wrw_mstk_qty_14,wrw_mstk_qty_15      
--  FROM ' + @DBNAME + '_ortmbk_rec a        
--      left join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
--      join ' + @DBNAME + '_ortord_rec on ord_cmpy_id = mbk_cmpy_id and ord_ord_pfx = mbk_ord_pfx and ord_ord_no = mbk_ord_no and ord_ord_itm = mbk_ord_itm    
--      join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = mbk_cmpy_id and orh_ord_pfx = mbk_ord_pfx and orh_ord_no = mbk_ord_no and orh_sts_actn <> ''C''      
--  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
--  left join ' + @DBNAME + '_cprcpc_rec on cpc_cmpy_id = mbk_cmpy_id and cpc_frm = mbk_frm and cpc_grd = mbk_grd and cpc_size = mbk_size and cpc_fnsh =mbk_fnsh  
--  left join ' + @DBNAME + '_cprclg_rec on clg_part_ctl_no = cpc_part_ctl_no  
--  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
--  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  
  SET @sqltxt = 'INSERT INTO #temp (Dbname, OrderNo,PONumber, Part,Branch, ActvyDT, Product, StkSDFQty1,StkSDFQty2,StkSDFQty3,StkSDFQty4,StkSDFQty5,StkSDFQty6,StkSDFQty7,StkSDFQty8,StkSDFQty9,StkSDFQty10,StkSDFQty11,StkSDFQty12,StkSDFQty13,StkSDFQty14,
   StkSDFQty15,MimStkQty1,MimStkQty2,MimStkQty3,MimStkQty4,MimStkQty5,MimStkQty6,MimStkQty7,MimStkQty8,MimStkQty9,MimStkQty10,MimStkQty11,MimStkQty12,MimStkQty13,MimStkQty14,
   MimStkQty15,NPosQty1,NPosQty2,NPosQty3,NPosQty4,NPosQty5,NPosQty6,NPosQty7,NPosQty8,NPosQty9,NPosQty10,NPosQty11,NPosQty12,NPosQty13,NPosQty14,NPosQty15
,OOBalQty1,OOBalQty2,OOBalQty3,OOBalQty4,OOBalQty5,OOBalQty6,OOBalQty7,OOBalQty8,OOBalQty9,OOBalQty10,OOBalQty11,OOBalQty12,OOBalQty13,OOBalQty14,OOBalQty15)        
      SELECT  distinct '''+ @DBNAME +''',mbk_ord_no,ord_cus_po,ipd_part, wrw_whs,wrw_run_dt,RTRIM(mbk_frm) + ''-'' + RTRIM(mbk_grd) + ''-'' + RTRIM(mbk_size) + ''-'' + RTRIM(mbk_fnsh),
      wrw_stk_sdf_qty_1,wrw_stk_sdf_qty_2,wrw_stk_sdf_qty_3,wrw_stk_sdf_qty_4
,wrw_stk_sdf_qty_5,wrw_stk_sdf_qty_6,wrw_stk_sdf_qty_7,wrw_stk_sdf_qty_8,wrw_stk_sdf_qty_9,wrw_stk_sdf_qty_10,wrw_stk_sdf_qty_11,wrw_stk_sdf_qty_12,wrw_stk_sdf_qty_13,wrw_stk_sdf_qty_14,wrw_stk_sdf_qty_15,
wrw_mstk_qty_1,wrw_mstk_qty_2,wrw_mstk_qty_3,wrw_mstk_qty_4
,wrw_mstk_qty_5,wrw_mstk_qty_6,wrw_mstk_qty_7,wrw_mstk_qty_8,wrw_mstk_qty_9,wrw_mstk_qty_10,wrw_mstk_qty_11,wrw_mstk_qty_12,wrw_mstk_qty_13,wrw_mstk_qty_14,wrw_mstk_qty_15,
wrw_net_pos_qty_1,wrw_net_pos_qty_2,wrw_net_pos_qty_3,wrw_net_pos_qty_4
,wrw_net_pos_qty_5,wrw_net_pos_qty_6,wrw_net_pos_qty_7,wrw_net_pos_qty_8,wrw_net_pos_qty_9,wrw_net_pos_qty_10,wrw_net_pos_qty_11,wrw_net_pos_qty_12,wrw_net_pos_qty_13,wrw_net_pos_qty_14,wrw_net_pos_qty_15,
wrw_ohd_stk_qty_1,wrw_ohd_stk_qty_2,wrw_ohd_stk_qty_3,wrw_ohd_stk_qty_4
,wrw_ohd_stk_qty_5,wrw_ohd_stk_qty_6,wrw_ohd_stk_qty_7,wrw_ohd_stk_qty_8,wrw_ohd_stk_qty_9,wrw_ohd_stk_qty_10,wrw_ohd_stk_qty_11,wrw_ohd_stk_qty_12,wrw_ohd_stk_qty_13,wrw_ohd_stk_qty_14,wrw_ohd_stk_qty_15      
  FROM ' + @DBNAME + '_ortmbk_rec a   
  -- join ' + @DBNAME + '_IRM_Input ir on ir.product = RTRIM(mbk_frm) + ''\'' + RTRIM(mbk_grd) + ''\'' + RTRIM(mbk_size) + ''\'' + RTRIM(mbk_fnsh)    
      left join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
      join ' + @DBNAME + '_ortord_rec on ord_cmpy_id = mbk_cmpy_id and ord_ord_pfx = mbk_ord_pfx and ord_ord_no = mbk_ord_no and ord_ord_itm = mbk_ord_itm    
      join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = mbk_cmpy_id and orh_ord_pfx = mbk_ord_pfx and orh_ord_no = mbk_ord_no and orh_sts_actn <> ''C''      
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  left join ' + @DBNAME + '_tctipd_rec on ipd_cmpy_id = mbk_cmpy_id and ipd_ref_pfx = mbk_ord_pfx and ipd_ref_no = mbk_ord_no and ipd_ref_itm = mbk_ord_itm  
  join ' + @DBNAME + '_IRM_Part_Input on partNo = ipd_part
  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  
  if  (UPPER(@DBNAME)= 'US' )  
   BEGIN    
	   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''11782'' or mbk_sld_cus_id = ''1767'' or mbk_sld_cus_id = ''993'')  '  
		END
  END  
  ELSE  
  BEGIN  
   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''302'')  '  
		END
  END  
  SET @sqltxt = @sqltxt + ' where  wrw_rec_nbr =  1 and a.mbk_actvy_dt >= ''2013-11-01'' order by mbk_ord_no'  
  
  --To show data from week 16 to 30
  SET @sqltxt = @sqltxt + ' INSERT INTO #temp1 (Dbname, OrderNo,PONumber, Part,Branch, ActvyDT, Product,StkSDFQty16,StkSDFQty17,StkSDFQty18,StkSDFQty19,StkSDFQty20,StkSDFQty21,StkSDFQty22,StkSDFQty23,StkSDFQty24,StkSDFQty25,StkSDFQty26,StkSDFQty27,StkSDFQty28,StkSDFQty29,StkSDFQty30
,MimStkQty16,MimStkQty17,MimStkQty18,MimStkQty19,MimStkQty20,MimStkQty21,MimStkQty22,MimStkQty23,MimStkQty24,MimStkQty25,MimStkQty26,MimStkQty27,MimStkQty28,MimStkQty29,MimStkQty30
,NPosQty16,NPosQty17,NPosQty18,NPosQty19,NPosQty20,NPosQty21,NPosQty22,NPosQty23,NPosQty24,NPosQty25,NPosQty26,NPosQty27,NPosQty28,NPosQty29,NPosQty30
,OOBalQty16,OOBalQty17,OOBalQty18,OOBalQty19,OOBalQty20,OOBalQty21,OOBalQty22,OOBalQty23,OOBalQty24,OOBalQty25,OOBalQty26,OOBalQty27,OOBalQty28,OOBalQty29,OOBalQty30)        
      SELECT  distinct '''+ @DBNAME +''',mbk_ord_no,ord_cus_po,ipd_part, wrw_whs,wrw_run_dt,RTRIM(mbk_frm) + ''-'' + RTRIM(mbk_grd) + ''-'' + RTRIM(mbk_size) + ''-'' + RTRIM(mbk_fnsh),
      wrw_stk_sdf_qty_1,wrw_stk_sdf_qty_2,wrw_stk_sdf_qty_3,wrw_stk_sdf_qty_4
,wrw_stk_sdf_qty_5,wrw_stk_sdf_qty_6,wrw_stk_sdf_qty_7,wrw_stk_sdf_qty_8,wrw_stk_sdf_qty_9,wrw_stk_sdf_qty_10,wrw_stk_sdf_qty_11,wrw_stk_sdf_qty_12,wrw_stk_sdf_qty_13,wrw_stk_sdf_qty_14,wrw_stk_sdf_qty_15,
wrw_mstk_qty_1,wrw_mstk_qty_2,wrw_mstk_qty_3,wrw_mstk_qty_4
,wrw_mstk_qty_5,wrw_mstk_qty_6,wrw_mstk_qty_7,wrw_mstk_qty_8,wrw_mstk_qty_9,wrw_mstk_qty_10,wrw_mstk_qty_11,wrw_mstk_qty_12,wrw_mstk_qty_13,wrw_mstk_qty_14,wrw_mstk_qty_15,
wrw_net_pos_qty_1,wrw_net_pos_qty_2,wrw_net_pos_qty_3,wrw_net_pos_qty_4
,wrw_net_pos_qty_5,wrw_net_pos_qty_6,wrw_net_pos_qty_7,wrw_net_pos_qty_8,wrw_net_pos_qty_9,wrw_net_pos_qty_10,wrw_net_pos_qty_11,wrw_net_pos_qty_12,wrw_net_pos_qty_13,wrw_net_pos_qty_14,wrw_net_pos_qty_15,
wrw_ohd_stk_qty_1,wrw_ohd_stk_qty_2,wrw_ohd_stk_qty_3,wrw_ohd_stk_qty_4
,wrw_ohd_stk_qty_5,wrw_ohd_stk_qty_6,wrw_ohd_stk_qty_7,wrw_ohd_stk_qty_8,wrw_ohd_stk_qty_9,wrw_ohd_stk_qty_10,wrw_ohd_stk_qty_11,wrw_ohd_stk_qty_12,wrw_ohd_stk_qty_13,wrw_ohd_stk_qty_14,wrw_ohd_stk_qty_15 
  FROM ' + @DBNAME + '_ortmbk_rec a   
  -- join ' + @DBNAME + '_IRM_Input ir on ir.product = RTRIM(mbk_frm) + ''\'' + RTRIM(mbk_grd) + ''\'' + RTRIM(mbk_size) + ''\'' + RTRIM(mbk_fnsh)    
      left join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
      join ' + @DBNAME + '_ortord_rec on ord_cmpy_id = mbk_cmpy_id and ord_ord_pfx = mbk_ord_pfx and ord_ord_no = mbk_ord_no and ord_ord_itm = mbk_ord_itm    
      join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = mbk_cmpy_id and orh_ord_pfx = mbk_ord_pfx and orh_ord_no = mbk_ord_no and orh_sts_actn <> ''C''      
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  left join ' + @DBNAME + '_tctipd_rec on ipd_cmpy_id = mbk_cmpy_id and ipd_ref_pfx = mbk_ord_pfx and ipd_ref_no = mbk_ord_no and ipd_ref_itm = mbk_ord_itm  
  join ' + @DBNAME + '_IRM_Part_Input on partNo = ipd_part
  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  
  if  (UPPER(@DBNAME)= 'US' )  
   BEGIN    
	   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''11782'' or mbk_sld_cus_id = ''1767'' or mbk_sld_cus_id = ''993'')  '  
		END
  END  
  ELSE  
  BEGIN  
   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''302'')  '  
		END
  END  
  SET @sqltxt = @sqltxt + ' where  wrw_rec_nbr =  2 and a.mbk_actvy_dt >= ''2013-11-01'' order by mbk_ord_no'          
  
  --To show data from week 31 to 37
  SET @sqltxt1 = ' INSERT INTO #temp2 (Dbname, OrderNo,PONumber, Part,Branch, ActvyDT, Product,StkSDFQty31,StkSDFQty32,StkSDFQty33,StkSDFQty34,StkSDFQty35,StkSDFQty36,StkSDFQty37,
  MimStkQty31,MimStkQty32,MimStkQty33,MimStkQty34,MimStkQty35,MimStkQty36,MimStkQty37,NPosQty31,NPosQty32,NPosQty33,NPosQty34,NPosQty35,NPosQty36,NPosQty37,
  OOBalQty31,OOBalQty32,OOBalQty33,OOBalQty34,OOBalQty35,OOBalQty36,OOBalQty37)        
      SELECT  distinct '''+ @DBNAME +''',mbk_ord_no,ord_cus_po,ipd_part, wrw_whs,wrw_run_dt,RTRIM(mbk_frm) + ''-'' + RTRIM(mbk_grd) + ''-'' + RTRIM(mbk_size) + ''-'' + RTRIM(mbk_fnsh),
      wrw_stk_sdf_qty_1,wrw_stk_sdf_qty_2,wrw_stk_sdf_qty_3,wrw_stk_sdf_qty_4,wrw_stk_sdf_qty_5,wrw_stk_sdf_qty_6,wrw_stk_sdf_qty_7,
wrw_mstk_qty_1,wrw_mstk_qty_2,wrw_mstk_qty_3,wrw_mstk_qty_4,wrw_mstk_qty_5,wrw_mstk_qty_6,wrw_mstk_qty_7,
wrw_net_pos_qty_1,wrw_net_pos_qty_2,wrw_net_pos_qty_3,wrw_net_pos_qty_4,wrw_net_pos_qty_5,wrw_net_pos_qty_6,wrw_net_pos_qty_7,
wrw_ohd_stk_qty_1,wrw_ohd_stk_qty_2,wrw_ohd_stk_qty_3,wrw_ohd_stk_qty_4,wrw_ohd_stk_qty_5,wrw_ohd_stk_qty_6,wrw_ohd_stk_qty_7
  FROM ' + @DBNAME + '_ortmbk_rec a   
  -- join ' + @DBNAME + '_IRM_Input ir on ir.product = RTRIM(mbk_frm) + ''\'' + RTRIM(mbk_grd) + ''\'' + RTRIM(mbk_size) + ''\'' + RTRIM(mbk_fnsh)    
      left join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
      join ' + @DBNAME + '_ortord_rec on ord_cmpy_id = mbk_cmpy_id and ord_ord_pfx = mbk_ord_pfx and ord_ord_no = mbk_ord_no and ord_ord_itm = mbk_ord_itm    
      join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = mbk_cmpy_id and orh_ord_pfx = mbk_ord_pfx and orh_ord_no = mbk_ord_no and orh_sts_actn <> ''C''      
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  left join ' + @DBNAME + '_tctipd_rec on ipd_cmpy_id = mbk_cmpy_id and ipd_ref_pfx = mbk_ord_pfx and ipd_ref_no = mbk_ord_no and ipd_ref_itm = mbk_ord_itm  
  join ' + @DBNAME + '_IRM_Part_Input on partNo = ipd_part
  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  
  if  (UPPER(@DBNAME)= 'US' )  
   BEGIN    
	   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt1 = @sqltxt1 + ' and (mbk_sld_cus_id = ''11782'' or mbk_sld_cus_id = ''1767'' or mbk_sld_cus_id = ''993'')  '  
		END
  END  
  ELSE  
  BEGIN  
   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt1 = @sqltxt1 + ' and (mbk_sld_cus_id = ''302'')  '  
		END
  END  
  SET @sqltxt1 = @sqltxt1 + ' where  wrw_rec_nbr =  3 and a.mbk_actvy_dt >= ''2013-11-01'' order by mbk_ord_no'          
     
  print 'Mukesh';      
  END        
 print(@sqltxt)        
 set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);  
 print(@sqltxt1)        
 set @execSQLtxt1 = @sqltxt1;         
 EXEC (@execSQLtxt1);      
END        
 SELECT distinct t1.Dbname, t1.OrderNo,t1.PONumber, t1.Part,t1.Branch, t1.ActvyDT, t1.Product, t1.StkSDFQty1,t1.StkSDFQty2,t1.StkSDFQty3,t1.StkSDFQty4,t1.StkSDFQty5,t1.StkSDFQty6,t1.StkSDFQty7,t1.StkSDFQty8,t1.StkSDFQty9,t1.StkSDFQty10,t1.StkSDFQty11,t1.StkSDFQty12,t1.StkSDFQty13,t1.StkSDFQty14,
   t1.StkSDFQty15,t1.MimStkQty1,t1.MimStkQty2,t1.MimStkQty3,t1.MimStkQty4,t1.MimStkQty5,t1.MimStkQty6,t1.MimStkQty7,t1.MimStkQty8,t1.MimStkQty9,t1.MimStkQty10,t1.MimStkQty11,t1.MimStkQty12,t1.MimStkQty13,t1.MimStkQty14,
   t1.MimStkQty15,t1.NPosQty1,t1.NPosQty2,t1.NPosQty3,t1.NPosQty4,t1.NPosQty5,t1.NPosQty6,t1.NPosQty7,t1.NPosQty8,t1.NPosQty9,t1.NPosQty10,t1.NPosQty11,t1.NPosQty12,t1.NPosQty13,t1.NPosQty14,t1.NPosQty15
,t1.OOBalQty1,t1.OOBalQty2,t1.OOBalQty3,t1.OOBalQty4,t1.OOBalQty5,t1.OOBalQty6,t1.OOBalQty7,t1.OOBalQty8,t1.OOBalQty9,t1.OOBalQty10,t1.OOBalQty11,t1.OOBalQty12,t1.OOBalQty13,t1.OOBalQty14,t1.OOBalQty15
,t2.StkSDFQty16,t2.StkSDFQty17,t2.StkSDFQty18,t2.StkSDFQty19,t2.StkSDFQty20,t2.StkSDFQty21,t2.StkSDFQty22,t2.StkSDFQty23,t2.StkSDFQty24,t2.StkSDFQty25,t2.StkSDFQty26,t2.StkSDFQty27,t2.StkSDFQty28,t2.StkSDFQty29,t2.StkSDFQty30
,t2.MimStkQty16,t2.MimStkQty17,t2.MimStkQty18,t2.MimStkQty19,t2.MimStkQty20,t2.MimStkQty21,t2.MimStkQty22,t2.MimStkQty23,t2.MimStkQty24,t2.MimStkQty25,t2.MimStkQty26,t2.MimStkQty27,t2.MimStkQty28,t2.MimStkQty29,t2.MimStkQty30
,t2.NPosQty16,t2.NPosQty17,t2.NPosQty18,t2.NPosQty19,t2.NPosQty20,t2.NPosQty21,t2.NPosQty22,t2.NPosQty23,t2.NPosQty24,t2.NPosQty25,t2.NPosQty26,t2.NPosQty27,t2.NPosQty28,t2.NPosQty29,t2.NPosQty30
,t2.OOBalQty16,t2.OOBalQty17,t2.OOBalQty18,t2.OOBalQty19,t2.OOBalQty20,t2.OOBalQty21,t2.OOBalQty22,t2.OOBalQty23,t2.OOBalQty24,t2.OOBalQty25,t2.OOBalQty26,t2.OOBalQty27,t2.OOBalQty28,t2.OOBalQty29,t2.OOBalQty30
,t3.StkSDFQty31,t3.StkSDFQty32,t3.StkSDFQty33,t3.StkSDFQty34,t3.StkSDFQty35,t3.StkSDFQty36,t3.StkSDFQty37,t3.MimStkQty31,t3.MimStkQty32,t3.MimStkQty33,t3.MimStkQty34,t3.MimStkQty35,t3.MimStkQty36,t3.MimStkQty37
,t3.NPosQty31,t3.NPosQty32,t3.NPosQty33,t3.NPosQty34,t3.NPosQty35,t3.NPosQty36,t3.NPosQty37,t3.OOBalQty31,t3.OOBalQty32,t3.OOBalQty33,t3.OOBalQty34,t3.OOBalQty35,t3.OOBalQty36,t3.OOBalQty37
 FROM #temp t1 
left	join  #temp1  t2 on t1.Dbname = t2.Dbname and t1.OrderNo = t2.OrderNo and t1.PONumber = t2.PONumber and t1.Part = t2.Part
    and t1.Branch = t2.Branch and t1.ActvyDT = t2.ActvyDT and t1.Product = t2.Product
 left   join #temp2 t3 on t1.Dbname = t3.Dbname and t1.OrderNo = t3.OrderNo and t1.PONumber = t3.PONumber and t1.Part = t3.Part
    and t1.Branch = t3.Branch and t1.ActvyDT = t3.ActvyDT and t1.Product = t3.Product;  
         
 --SELECT * FROM #temp ;        
 DROP TABLE  #temp;     
 --SELECT * FROM #temp1 ;        
 --SELECT * FROM #temp2 ;        
 DROP TABLE  #temp1; 
 DROP TABLE  #temp2;        
END        
        
     
--EXEC [sp_itech_IRM_Report] 'US','2014-09-25','','M'  
GO
