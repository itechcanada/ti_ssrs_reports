USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_apjpjt]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Mukesh       
-- Create date: Jul 15, 2019     
-- Description: <Description,,>      
-- =============================================      
create PROCEDURE [dbo].[CA_apjpjt]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
    -- Insert statements for procedure here      
          
    IF OBJECT_ID('dbo.CA_apjpjt_rec', 'U') IS NOT NULL      
  drop table dbo.CA_apjpjt_rec;      
              
SELECT *      
into  dbo.CA_apjpjt_rec      
FROM [LIVECASTX].[livecastxdb].[informix].[apjpjt_rec];      
      
END      
-- select * from CA_apjpjt_rec
GO
