USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ccratt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: April 4, 2013  
-- Description: <Description,,>  
-- Updated on 01-05-2015
-- Updated by : Mrinal Jha
-- =============================================  
CREATE PROCEDURE [dbo].[TW_ccratt]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.TW_ccratt_rec', 'U') IS NOT NULL  
  drop table dbo.TW_ccratt_rec;  
      
  -- previous code        
--SELECT atp_actvy_tsk_purp, atp_ref_ctl_no, atp_desc30, atp_actv
--INTO  dbo.TW_ccratt_rec  
--FROM [LIVETWSTX].[livetwstxdb].[informix].[ccratt_rec];  

SELECT atp_actvy_tsk_purp, atp_ref_ctl_no, atp_desc30, atp_actv
INTO  dbo.TW_ccratt_rec  
FROM [LIVETWSTX].[livetwstxdb].[informix].[ccratp_rec];
  
END  
--  exec  TW_ccratt  
-- select * from TW_ccratt_rec


GO
