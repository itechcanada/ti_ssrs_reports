USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[itemnum_itemnum]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,chrisriggi>
-- Create date: <6/12/2014>
-- Description:	<concatenate Form grade size and finish into item num>
-- =============================================
CREATE PROCEDURE [dbo].[itemnum_itemnum]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	prm_frm,
	prm_grd,
	prm_size,
	prm_fnsh
	 FROM [Stratix_US].[dbo].[US_item_num_E]
	 
END
GO
