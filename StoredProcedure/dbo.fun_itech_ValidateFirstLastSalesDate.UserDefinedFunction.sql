USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_ValidateFirstLastSalesDate]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukes>
-- Create date: <09 MAr 2016>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fun_itech_ValidateFirstLastSalesDate](@customerID Varchar(10), @salesType Varchar(10), @compareDate Varchar(10))

RETURNS int
AS
BEGIN
declare @firstSalesDate Varchar(10);
declare @lastSalesDate Varchar(10);
declare @returnStatus int  ;
set @returnStatus = 0;

	if(Upper(@salesType) = 'FIRST' )
	begin
	select  @firstSalesDate = convert(Varchar(10),coc_frst_sls_dt,120)  from PS_arbcoc_rec where coc_cus_id = SUBSTRING(@customerID,2,len(@customerID));
	
	if (@compareDate = '' and @firstSalesDate is not null and @firstSalesDate != '')
	begin
	set @returnStatus =  1;
	end
	else if (@compareDate != '' and (@firstSalesDate is null OR @firstSalesDate = ''))
	begin
	set @returnStatus =  1;
	end 
	else if (@firstSalesDate is not null and @firstSalesDate != '' and @compareDate != '' and @compareDate != @firstSalesDate)
	begin 
	set @returnStatus =  0;
	end
	end
	else
	begin
	select  @lastSalesDate = convert(Varchar(10),coc_lst_sls_dt,120)  from PS_arbcoc_rec where coc_cus_id = SUBSTRING(@customerID,2,len(@customerID));
	
	if (@compareDate = '' and @lastSalesDate is not null and @lastSalesDate != '')
	begin
	set @returnStatus =  1;
	end
	else if (@compareDate != '' and (@lastSalesDate is null OR @lastSalesDate = ''))
	begin
	set @returnStatus =  1;
	end 
	else if (@lastSalesDate is not null and @lastSalesDate != '' and @compareDate != '' and @compareDate != @lastSalesDate)
	begin 
	set @returnStatus =  0;
	end
	end
	
return  @returnStatus ;
END
GO
