USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[SALES_BY_STATE]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date 2/13/2012>
-- Description:	<Description,Sales by State>
-- =============================================
CREATE PROCEDURE [dbo].[SALES_BY_STATE]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime,
	@State varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT c.sat_sld_cus_id, c.sat_nm1, a.cva_city, SUM(c.sat_tot_val) as total,SUM(c.sat_blg_wgt) AS wgt,c.sat_inv_dt,c.sat_frm+c.sat_grd+c.sat_size+c.sat_fnsh as product
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[scrcva_rec] a
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrshp_rec] b ON a.cva_cus_ven_id=b.shp_cus_id AND a.cva_addr_no=b.shp_shp_to
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] c ON b.shp_cus_id=c.sat_sld_cus_id
WHERE
b.shp_ter=@State 
AND c.sat_inv_pfx='SE'
AND CAST(c.sat_inv_dt AS datetime) BETWEEN @FromDate AND @ToDate
AND a.cva_addr_no=0
AND a.cva_addr_typ = 'L'
AND a.cva_ref_pfx = 'CU'
AND a.cva_cus_ven_typ = 'C'
GROUP BY c.sat_sld_cus_id, c.sat_nm1, a.cva_city,c.sat_inv_dt,c.sat_frm+c.sat_grd+c.sat_size+c.sat_fnsh
HAVING SUM(c.sat_tot_val)>0
END
GO
