USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_glbacb]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,20/11/2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[TW_glbacb]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_glbacb_rec', 'U') IS NOT NULL
		drop table dbo.TW_glbacb_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.TW_glbacb_rec
  from [LIVETWGL].[livetwgldb].[informix].[glbacb_rec] ;
  
END

-- Select * from TW_glbacb_rec
GO
