USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sasmsu]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 17, 2016,>  
-- Description: <Description,Open Job history,>  
  
-- =============================================  
create PROCEDURE [dbo].[US_sasmsu]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_sasmsu_rec', 'U') IS NOT NULL  
  drop table dbo.US_sasmsu_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_sasmsu_rec  
  from [LIVEUS_IW].[liveusstxdb_iw].[informix].[sasmsu_rec]  ;   
    
END  
-- select * from US_sasmsu_rec
GO
