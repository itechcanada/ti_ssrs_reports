USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_VendorPaymentView]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <05 Mar 2015>
-- Description:	<Getting Receipt Details for a Voucher SSRS reports>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_VendorPaymentView] @DBNAME varchar(50),  @VendorNo Varchar(20),@FromDate datetime, @ToDate datetime
As
Begin



declare @DB varchar(100)
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @FD varchar(10)
declare @TD varchar(10)
DECLARE @CurrenyRate varchar(15) 

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)

IF @VendorNo = 'ALL' OR @VendorNo = ''
BEGIN
SET @VendorNo = ''
END
set @DB = @DBNAME

Set @VendorNo = RTRIM(LTRIM(@VendorNo));

CREATE TABLE #tmp (   [Database]		 VARCHAR(10)
   					, VendorID			 VARCHAR(8)
   					, VendorLongNM		 VARCHAR(35)
   					, VendorTaxID		 VARCHAR(15)
   					, RefYear			 VARCHAR(4)
   					, OrigAmt 			 decimal(20,2)
   					, Wgt				decimal(20,2)
   					, Cost				decimal(20,2)
   					)
   					
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max); 
  	  			 IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End  
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End  
			    SET @query ='INSERT INTO #tmp ([Database], VendorID, VendorLongNM, VendorTaxID, RefYear, OrigAmt, Wgt, Cost)
			    SELECT ''' +  @Prefix + ''' as [Database],  pra_cus_ven_ID,	ven_ven_long_nm, ven_1099_tx_id, Year(pra_rcpt_upd_dt),
			     	Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + '), '
			     	 
			     	if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')           
             SET @query = @query + ' SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) , Case when SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) = 0 then 0 else Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + ')/SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) end'        
           ELSE        
            SET @query = @query + ' SUM(ISNULL(pra_rcvd_wgt,0)) , Case when SUM(ISNULL(pra_rcvd_wgt,0)) = 0 then 0 else Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + ')/SUM(ISNULL(pra_rcvd_wgt,0)) end' 
            
			SET @query = @query + '	from ' + @Prefix + '_pohpra_rec 
			     	join ' + @Prefix + '_aprven_rec on ven_cmpy_id = pra_cmpy_id and ven_ven_id = pra_cus_ven_ID
			     	where pra_rcpt_upd_dt >= ''' + @FD + ''' and pra_rcpt_upd_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(pra_cus_ven_ID)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''' )
			     	 group by pra_cus_ven_ID,ven_ven_long_nm,ven_1099_tx_id,Year(pra_rcpt_upd_dt) ;'	
			     	 						
				print @query;
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')    
        IF (UPPER(@DB) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DB) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DB) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DB) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DB) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End  
    Else if(UPPER(@DB) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End  
			     SET @sqltxt ='INSERT INTO #tmp ([Database], VendorID, VendorLongNM, VendorTaxID, RefYear, OrigAmt, Wgt, Cost)
			     
			     SELECT ''' +  @DBNAME + ''' as [Database],  pra_cus_ven_ID,	ven_ven_long_nm, ven_1099_tx_id, Year(pra_rcpt_upd_dt),
			     	Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + '),  '
			     	 
			     	if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')           
             SET @sqltxt = @sqltxt + ' SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) , Case when SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) = 0 then 0 else Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + ')/SUM(ISNULL(pra_rcvd_wgt,0) * 2.20462) end'        
           ELSE        
            SET @sqltxt = @sqltxt + ' SUM(ISNULL(pra_rcvd_wgt,0)) , Case when SUM(ISNULL(pra_rcvd_wgt,0)) = 0 then 0 else Sum(ISNULL(pra_bas_mtl_val,0) * '+ @CurrenyRate + ')/SUM(ISNULL(pra_rcvd_wgt,0)) end' 
            
			SET @sqltxt = @sqltxt + ' from ' + @DBNAME + '_pohpra_rec 
			     	join ' + @DBNAME + '_aprven_rec on ven_cmpy_id = pra_cmpy_id and ven_ven_id = pra_cus_ven_ID
			     	where pra_rcpt_upd_dt >= ''' + @FD + ''' and pra_rcpt_upd_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(pra_cus_ven_ID)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''' )
			     	 group by pra_cus_ven_ID,ven_ven_long_nm,ven_1099_tx_id,Year(pra_rcpt_upd_dt) ;'
			     
			     	
			     	
					print(@sqltxt)	
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
   END
   Select * from #tmp order by [Database],VendorID
   Drop table #tmp
End 

							 
-- Exec [sp_itech_VendorPaymentView] 'US', '', '2016-01-01', '2017-07-31'
GO
