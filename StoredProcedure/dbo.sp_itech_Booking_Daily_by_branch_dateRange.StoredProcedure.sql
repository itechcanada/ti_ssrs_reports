USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Booking_Daily_by_branch_dateRange]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Mukes>                            
-- Create date: <14-02-2017>                            
-- Description: <Booking Daily Reports>                           
                        
-- =============================================                            
CREATE PROCEDURE [dbo].[sp_itech_Booking_Daily_by_branch_dateRange]  @DBNAME varchar(50), @version char = '0',@Branch varchar(50),@fromDate as Datetime ,              
@toDate as Datetime                           
AS                            
BEGIN            
          
          
 -- SET NOCOUNT ON added to prevent extra result sets from                            
 SET NOCOUNT ON;        
 

declare @DB varchar(100);                            
declare @sqltxt varchar(6000);                            
declare @execSQLtxt varchar(7000);                            
DECLARE @CountryName VARCHAR(25);                               
DECLARE @prefix VARCHAR(15);                               
DECLARE @DatabaseName VARCHAR(35);                                
DECLARE @CurrenyRate varchar(15);                          
declare @FD varchar(10)                              
declare @TD varchar(10)                       
declare @FD12 varchar(10)                                   
                            
   set @FD = CONVERT(VARCHAR(10), @fromDate,120)                          
 set @TD = CONVERT(VARCHAR(10), @toDate,120)                       
 set @FD12 = CONVERT(VARCHAR(10), DATEADD(month, -12 ,@toDate) ,120)                        
                          
                           
IF @Branch = 'ALL'                            
 BEGIN                            
  set @Branch = ''                            
                            
 END                           
 print '@Branch =' + @Branch;                          
 declare @start varchar = ''                          
-- SET @start = @Month;                           
-- print  @start;                          
                            
CREATE TABLE #temp ( Dbname   VARCHAR(10)                            
     ,DBCountryName VARCHAR(25)                           
     ,ISlp  VARCHAR(4)                          
     ,OSlp  VARCHAR(4)                             
     ,CusID   VARCHAR(10)                            
     ,CusLongNm  VARCHAR(40)                            
     ,Branch   VARCHAR(3)                            
     ,ActvyDT  VARCHAR(10)                            
     ,OrderNo  NUMERIC                            
     ,OrderItm  NUMERIC                            
     ,Product  VARCHAR(500)                            
     ,Wgt   decimal(20,0)                            
     ,TotalMtlVal decimal(20,0)                            
     ,ReplCost  decimal(20,0)                            
     ,Profit   decimal(20,1)                            
     , TotalSlsVal decimal(20,0)                           
     ,Market Varchar(35)                        
     ,ReplProfit decimal(20,0)                      
     ,ObsolateInvBooking decimal(20,1)                
     ,CusState Varchar(3)                          
        ,cusPCD varchar(10)                         
        ,CusCTY Varchar(3)            
        ,DueDate varchar(10)       
        ,BookingProcessMethod varchar(1)                      
     );                            
                            
IF @DBNAME = 'ALL'                            
 BEGIN                            
   IF @version = '0'                            
  BEGIN                            
  DECLARE ScopeCursor CURSOR FOR                            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                            
    OPEN ScopeCursor;                            
  END                            
  ELSE                            
  BEGIN                            
  DECLARE ScopeCursor CURSOR FOR                            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                     
    OPEN ScopeCursor;                            
  END                            
                              
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                            
  WHILE @@FETCH_STATUS = 0                            
  BEGIN                            
   DECLARE @query NVARCHAR(MAX);                            
   IF (UPPER(@Prefix) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                       
    End                            
    Else if (UPPER(@Prefix) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                            
    Else if(UPPER(@Prefix) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                            
    Else if(UPPER(@Prefix) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                            
                                
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR                             
    BEGIN                            
    SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,               
    ReplCost, Profit, TotalSlsVal,Market, ReplProfit, ObsolateInvBooking,CusState ,cusPCD,CusCTY,DueDate,BookingProcessMethod)                            
   SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no,                  
    a.mbk_ord_itm, (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))                   
    from ' + @Prefix + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size and prm_fnsh = a.mbk_fnsh) as Product,                                     
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                            
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'  ,'' ''                        
  
   
        , ( (a.mbk_tot_val* '+ @CurrenyRate +') - ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END) )                        
      ,                       
      (Select SUM(SAT_BLG_WGT) from ' + @Prefix + '_sahsat_rec where a.mbk_frm = SAT_FRM and a.mbk_grd = SAT_GRD  and a.mbk_size = sat_size and a.mbk_fnsh = sat_fnsh                       
 and sat_shpg_whs in (select  im.prd_whs from  ' + @Prefix + '_intprd_rec im  where  im.prd_frm = a.mbk_frm and im.prd_Grd = a.mbk_grd and im.prd_size = a.mbk_size and im.prd_fnsh = a.mbk_fnsh )                                           
       and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''')                      
       ,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty)) ,          
       (select max(orl_due_to_dt) from ' + @Prefix + '_ortorl_rec where orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no           
     and orl_ord_itm = mbk_ord_itm ) ,a.mbk_trs_md           
      FROM ' + @Prefix + '_ortmbk_rec a                            
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON  a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id            
      left join ' + @Prefix + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''                                   
           and cva_addr_no = 0 and cva_addr_typ = ''L''                               
      WHERE a.mbk_ord_pfx=''SO''                             
          AND a.mbk_ord_itm<>999                              
        -- and a.mbk_trs_md = ''A''                            
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''                            
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                           
                                    
    END                            
    ELSE                            
    BEGIN                            
     SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,               
     ReplCost, Profit, TotalSlsVal,Market, ReplProfit, ObsolateInvBooking,CusState ,cusPCD,CusCTY,DueDate,BookingProcessMethod)                            
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt,                   
      a.mbk_ord_no, a.mbk_ord_itm, (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))                   
      from ' + @Prefix + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size and prm_fnsh = a.mbk_fnsh) as Product,                              
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                            
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,'' ''                         
   
        , ( (a.mbk_tot_val* '+ @CurrenyRate +') - ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END) )                        
      ,                       
      (Select SUM(SAT_BLG_WGT) from ' + @Prefix + '_sahsat_rec where a.mbk_frm = SAT_FRM and a.mbk_grd = SAT_GRD  and a.mbk_size = sat_size and a.mbk_fnsh = sat_fnsh                       
 and sat_shpg_whs in (select  im.prd_whs from  ' + @Prefix + '_intprd_rec im  where  im.prd_frm = a.mbk_frm and im.prd_Grd = a.mbk_grd and im.prd_size = a.mbk_size and im.prd_fnsh = a.mbk_fnsh )                                                          
       and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''')                      
        ,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty)) ,          
        (select max(orl_due_to_dt) from ' + @Prefix + '_ortorl_rec where orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no           
     and orl_ord_itm = mbk_ord_itm ) ,a.mbk_trs_md           
      FROM ' + @Prefix + '_ortmbk_rec a                            
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id               
      left join ' + @Prefix + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''                                   
           and cva_addr_no = 0 and cva_addr_typ = ''L''                          
      WHERE a.mbk_ord_pfx=''SO''                             
         AND a.mbk_ord_itm<>999                              
        -- and a.mbk_trs_md = ''A''                             
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''                             
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                           
                                   
    END                            
   print @query;                            
   EXECUTE sp_executesql @query;                            
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                            
  END                               
  CLOSE ScopeCursor;                            
  DEALLOCATE ScopeCursor;                            
 END                            
ELSE                   
BEGIN                            
IF @version = '0'                            
  BEGIN                            
  SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)                            
  END                            
  ELSE                            
  BEGIN                            
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)                            
  END                            
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)                            
 IF (UPPER(@DBNAME) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                     
    End                            
    Else if(UPPER(@DBNAME) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                            
    Else if(UPPER(@DBNAME) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                            
    Else if(UPPER(@DBNAME) = 'TWCN')                            
    begin                            
       SET @DB ='TW'                            
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
                                
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR                             
   BEGIN                            
   SET @sqltxt ='INSERT INTO #temp (Dbname,DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,               
   ReplCost, Profit, TotalSlsVal, Market , ReplProfit, ObsolateInvBooking,CusState ,cusPCD,CusCTY,DueDate,BookingProcessMethod)                            
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt,                   
      a.mbk_ord_no, a.mbk_ord_itm, (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))                   
      from ' + @DBNAME + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size and prm_fnsh = a.mbk_fnsh) as Product,                              
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                            
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' , '' ''                        
  
   
        , ( (a.mbk_tot_val* '+ @CurrenyRate +') - ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END) )                        
      ,                       
      (Select SUM(SAT_BLG_WGT) from ' + @DBNAME + '_sahsat_rec where a.mbk_frm = SAT_FRM and a.mbk_grd = SAT_GRD  and a.mbk_size = sat_size and a.mbk_fnsh = sat_fnsh                       
 and sat_shpg_whs in (select  im.prd_whs from  ' + @DBNAME + '_intprd_rec im  where  im.prd_frm = a.mbk_frm and im.prd_Grd = a.mbk_grd and im.prd_size = a.mbk_size and im.prd_fnsh = a.mbk_fnsh )                                                          
       and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''')                      
      ,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty)),          
      (select max(orl_due_to_dt) from ' + @DBNAME + '_ortorl_rec where orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no           
     and orl_ord_itm = mbk_ord_itm ) ,a.mbk_trs_md           
      FROM ' + @DBNAME + '_ortmbk_rec a                            
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id           
      left join ' + @DBNAME + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''                                   
           and cva_addr_no = 0 and cva_addr_typ = ''L''                            
      WHERE a.mbk_ord_pfx=''SO''                             
         AND a.mbk_ord_itm<>999                               
      --  and a.mbk_trs_md = ''A''                            
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''                              
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                           
                                   
  END                            
  ELSE                            
  BEGIN                            
  SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,               
  ReplCost, Profit, TotalSlsVal, Market , ReplProfit, ObsolateInvBooking,CusState ,cusPCD,CusCTY,DueDate,BookingProcessMethod)                            
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt,                   
      a.mbk_ord_no, a.mbk_ord_itm, (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))                   
 from ' + @DBNAME + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size and prm_fnsh = a.mbk_fnsh) as Product,                              
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                            
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,'' ''                          
 
       , ( (a.mbk_tot_val* '+ @CurrenyRate +') - ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END) )                        
      ,                       
      (Select SUM(SAT_BLG_WGT) from ' + @DBNAME + '_sahsat_rec where a.mbk_frm = SAT_FRM and a.mbk_grd = SAT_GRD  and a.mbk_size = sat_size and a.mbk_fnsh = sat_fnsh                       
 and sat_shpg_whs in (select  im.prd_whs from  ' + @DBNAME + '_intprd_rec im  where  im.prd_frm = a.mbk_frm and im.prd_Grd = a.mbk_grd and im.prd_size = a.mbk_size and im.prd_fnsh = a.mbk_fnsh )                                       
       and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''')                      
       ,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty)),          
       (select max(orl_due_to_dt) from ' + @DBNAME + '_ortorl_rec where orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no           
     and orl_ord_itm = mbk_ord_itm ) ,a.mbk_trs_md           
      FROM ' + @DBNAME + '_ortmbk_rec a                            
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id           
       left join ' + @DBNAME + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''                                   
           and cva_addr_no = 0 and cva_addr_typ = ''L''                           
      WHERE a.mbk_ord_pfx=''SO''                             
        AND a.mbk_ord_itm<>999                             
         -- and a.mbk_trs_md = ''A''                           
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''                             
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                           
                                    
  END                            
 print(@sqltxt)                            
 set @execSQLtxt = @sqltxt;                             
 EXEC (@execSQLtxt);                            
END                            
 SELECT Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal, Market , ReplProfit                      
 ,CASE WHEN ObsolateInvBooking > 0  then 0 ELSE Wgt END AS ObsolateInvBooking, (CASE WHEN TotalSlsVal = 0 THEN 0 ELSE (ReplProfit /  TotalSlsVal)  * 100 end ) as ReplProfitPercent               
  ,CusState ,cusPCD,CusCTY,DueDate,BookingProcessMethod                    
 FROM #temp ;                            
 DROP TABLE  #temp;                            
END                            
                            
                            
                            
-- EXEC [sp_itech_Booking_Daily_by_branch_dateRange] 'UK', '0'  , 'ALL' ,'2019-09-01' ,'2019-09-30' -- UK 479 CA 383                             
--EXEC [sp_itech_Booking_Daily_by_branch_dateRange] 'ALL', '0'  , 'ALL' ,'2017-01-21' ,'2017-01-31'                     
/*                    
Date : 2017-03-20                    
Sub: why when i run this report for WDL dose JAC show up on it?                    
                    
Date: 20170606                    
Sub:Report Adjustment                    
                    
Date:20170807                    
Mail:Report Request Modification               
              
Date:20191022              
Mail sub:STRATIXReports > Branch - BHM  & Branch - IND  Update Request                   
        
Date:20191209        
Mail sub:Booking Daily Report By Branch Update Request        
      
         
Date:20191211          
Mail sub:Booking Daily Report - Additional Request         
*/ 
GO
