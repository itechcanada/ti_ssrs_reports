USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_EmailContactsMarketingReport]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Mukesh >                            
-- Create date: <30 JAN 2019>                            
-- Description: <Getting top 40 Propects customers for SSRS reports>                            
-- =============================================                            
-- select * from US_mxrusr_rec Where usr_lgn_id = 'ccutro'                          
                          
CREATE PROCEDURE [dbo].[sp_itech_EmailContactsMarketingReport] @DBNAME varchar(50), @Branch varchar(15), @version char = '0' , @IncludeInterco char = '0'                            
                            
AS                            
BEGIN                            
                             
 SET NOCOUNT ON;                     
                      
declare @sqltxt varchar(max)                            
declare @execSQLtxt varchar(max)                            
declare @DB varchar(100)                           
declare @LTD varchar(10)                           
declare @D3MFD varchar(10)                            
declare @D3MTD varchar(10)                           
                            
set @DB=  @DBNAME                             
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-18,0)), 120)  -- Lost Account                          
-- set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-4,0)) , 120)   -- Dormant 3 Month                            
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)   -- Last date of previous month                         
                         
                            
CREATE TABLE #tmp1 (    CustID   VARCHAR(100)                            
        , CustName     VARCHAR(100)                            
        , Market   VARCHAR(65)                             
        , Branch   VARCHAR(3)                     
        , ShpBranch   VARCHAR(3)                             
        , Databases   VARCHAR(15)                            
        ,ContactFirstNM  Varchar(100)                            
        ,ContactLastNM  Varchar(100)                            
        ,CustEmail Varchar(100)                           
        ,SalesPresonIs varchar(10)                          
        ,SalesPresonOs varchar(10)                           
        ,LastSaleDT varchar(10)                           
        ,SlsPrsLngOS varchar(35)                         
        ,CusType Varchar(1)                                 
        ,LostCustomer Varchar(1)                       
        ,cusAddress Varchar(150)                        
        ,cusPCD varchar(10)                        
        ,cusCity Varchar(35)                        
        ,CusState Varchar(3)                        
        ,CusPhone Varchar(30)                    
        ,NPAmt DECIMAL(20,2)                 
        ,CusCTY Varchar(3)                
        ,TotalSalesAmt DECIMAL(20,2)                
        ,cusActv  Varchar(1)                
        ,AccountDate varchar(10)               
        ,ContactDate varchar(10)                 
        ,CustomerTitle varchar(30)                
        ,AddressType varchar(1)                        
                   );                             
  CREATE TABLE #tmp2 (    CustID   VARCHAR(100)                            
        , CustName     VARCHAR(100)                            
        , Market   VARCHAR(65)                             
        , Branch   VARCHAR(3)                     
        , ShpBranch   VARCHAR(3)                             
        , Databases   VARCHAR(15)                            
        ,ContactFirstNM  Varchar(100)                            
        ,ContactLastNM  Varchar(100)                            
        ,CustEmail Varchar(100)                           
        ,SalesPresonIs varchar(10)                          
        ,SalesPresonOs varchar(10)                           
        ,LastSaleDT varchar(10)                           
        ,SlsPrsLngOS varchar(35)                          
        ,CusType Varchar(1)               
        ,LostCustomer Varchar(1)                      
        ,cusAddress Varchar(150)                        
        ,cusPCD varchar(10)                        
        ,cusCity Varchar(35)                        
        ,CusState Varchar(3)                        
        ,CusPhone Varchar(30)                                       
        ,NPAmt DECIMAL(20,2)                
        ,CusCTY Varchar(3)                 
        ,TotalSalesAmt DECIMAL(20,2)           
        ,cusActv  Varchar(1)                
        ,AccountDate varchar(10)               
        ,ContactDate varchar(10)                 
        ,CustomerTitle varchar(30)                
,AddressType varchar(1)                           
                   );                             
CREATE TABLE #tmp3 (    CustID   VARCHAR(100)                            
        , CustName     VARCHAR(100)                            
        , Market   VARCHAR(65)                             
        , Branch   VARCHAR(3)                    
        , ShpBranch   VARCHAR(3)                              
        , Databases   VARCHAR(15)                            
        ,ContactFirstNM  Varchar(100)                            
        ,ContactLastNM  Varchar(100)                            
        ,CustEmail Varchar(100)                           
        ,SalesPresonIs varchar(10)                          
        ,SalesPresonOs varchar(10)                           
        ,LastSaleDT varchar(10)                           
        ,SlsPrsLngOS varchar(35)                         
        ,CusType Varchar(1)                          
        ,LostCustomer Varchar(1)                       
        ,cusAddress Varchar(150)                        
        ,cusPCD varchar(10)                        
        ,cusCity Varchar(35)                        
        ,CusState Varchar(3)                        
        ,CusPhone Varchar(30)                           
        ,NPAmt DECIMAL(20,2)                 
        ,CusCTY Varchar(3)                    
        ,TotalSalesAmt DECIMAL(20,2)              
        ,cusActv  Varchar(1)               
        ,AccountDate varchar(10)               
        ,ContactDate varchar(10)                 
        ,CustomerTitle varchar(30)               
        ,AddressType varchar(1)                         
                   );                             
CREATE TABLE #tmp4 (    CustID   VARCHAR(100)                            
        , CustName     VARCHAR(100)                            
        , Market   VARCHAR(65)                             
        , Branch   VARCHAR(3)                    
        , ShpBranch   VARCHAR(3)                              
        , Databases   VARCHAR(15)                            
        ,ContactFirstNM  Varchar(100)                            
        ,ContactLastNM  Varchar(100)                            
        ,CustEmail Varchar(100)                           
        ,SalesPresonIs varchar(10)                          
        ,SalesPresonOs varchar(10)                           
        ,LastSaleDT varchar(10)                           
        ,SlsPrsLngOS varchar(35)                         
        ,CusType Varchar(1)                          
        ,LostCustomer Varchar(1)                       
        ,cusAddress Varchar(150)                        
        ,cusPCD varchar(10)                        
        ,cusCity Varchar(35)                        
        ,CusState Varchar(3)                        
        ,CusPhone Varchar(30)                           
        ,NPAmt DECIMAL(20,2)                  
        ,CusCTY Varchar(3)                  
        ,TotalSalesAmt DECIMAL(20,2)                
        ,cusActv  Varchar(1)              
        ,AccountDate varchar(10)               
        ,ContactDate varchar(10)                 
        ,CustomerTitle varchar(30)               
 ,AddressType varchar(1)                                       
                   );                   
--CREATE TABLE #tmp5 (    CustID   VARCHAR(100)                          
--        , CustName     VARCHAR(100)                            
--        , Market   VARCHAR(65)                       
--        , Branch   VARCHAR(3)                    
--        , ShpBranch   VARCHAR(3)                              
--        , Databases   VARCHAR(2)                            
--        ,ContactFirstNM  Varchar(100)                            
--        ,ContactLastNM  Varchar(100)                       
--        ,CustEmail Varchar(100)                           
--        ,SalesPresonIs varchar(10)                          
--        ,SalesPresonOs varchar(10)                           
--        ,LastSaleDT varchar(10)                           
--        ,SlsPrsLngOS varchar(35)                   --        ,CusType Varchar(1)                          
--        ,LostCustomer Varchar(1)                       
--        ,cusAddress Varchar(150)                        
--        ,cusPCD varchar(10)                        
--        ,cusCity Varchar(35)                        
--        ,CusState Varchar(3)                        
--        ,CusPhone Varchar(30)                           
--        ,NPAmt DECIMAL(20,2)                  
--        ,CusCTY Varchar(3)                                       
--                   );                                           
DECLARE @DatabaseName VARCHAR(35);                            
DECLARE @Prefix VARCHAR(5);                            
DECLARE @Name VARCHAR(15);                            
                          
--if @Market ='ALL'                            
-- BEGIN                            
-- set @Market = ''                            
-- END                            
                            
if @Branch ='ALL'                            
 BEGIN                            
 set @Branch = ''                            
 END                            
                            
DECLARE @CurrenyRate varchar(15);                            
                            
                       
                            
IF @DBNAME = 'ALL'                            
 BEGIN                            
  IF @version = '0'                              
   BEGIN                              
   DECLARE ScopeCursor CURSOR FOR                            
    select DatabaseName, Name,prefix from tbl_itech_DatabaseName                             
     OPEN ScopeCursor;                             
   END                              
   ELSE                              
   BEGIN                             
  DECLARE ScopeCursor CURSOR FOR                            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_ALL                             
    OPEN ScopeCursor;                            
  End                            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                            
     WHILE @@FETCH_STATUS = 0                            
       BEGIN                            
      SET @DB= @Prefix                            
      print(@DB)                            
                                 
     IF (UPPER(@DB) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@DB) = 'NO')                            
    begin                   
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@DB) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@DB) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                   
    Else if(UPPER(@DB) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                            
    Else if(UPPER(@DB) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                            
    Else if(UPPER(@DB) = 'TWCN')                            
    begin                            
       SET @DB ='TW'                            
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
                                 
                                 
        DECLARE @query NVARCHAR(max);                            
  DECLARE @query1 NVARCHAR(max);                
                                       
        SET @query = 'INSERT INTO #tmp4 (Databases,CustID, CustName,Market,Branch,ShpBranch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,                    
        LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt ,CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)              
select  ''' + @Name + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,'''',              
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, IsNull(cvt_email,'''') as CustEmail              
, isslp.slp_lgn_id, osslp.slp_lgn_id  ,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )                          
,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                    
, cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                        
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                    
,(select Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ' from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''NPAmt'',LTRIM(RTRIM(cva_cty))                    
,(select SUM(stn_tot_val * '+ @CurrenyRate +') from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''TotalSalesAmt'',cus_actv ,              
crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
from ' + @DB + '_arrcus_rec                 
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id                      
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''            
left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''            
and cva_addr_no = 0 and cva_addr_typ = ''L''             
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id            
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp            
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp            
where cus_cus_acct_typ = ''P''   '                  
-- and cus_actv = 1              
print(@query)                              
        EXECUTE sp_executesql @query;                   
                
SET @query1 = 'Union                
SELECT  ''' + @Name + ''', CUS_CUS_ID as CustID,  CUS_CUS_LONG_NM as CustName,cuc_desc30 as Market,MAX(CUS_ADMIN_BRH) as Branch,MAX(stn_shpt_brh),                
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail,isslp.slp_lgn_id, osslp.slp_lgn_id ,coc_lst_sls_dt,                
(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                
,cus_cus_acct_typ, '''' as lostcustomer , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                
,Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ' ,LTRIM(RTRIM(cva_cty))                
,SUM(stn_tot_val * '+ @CurrenyRate +'),cus_actv ,crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
FROM ' + @DB + '_sahstn_rec                
join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no and orh_sld_cus_id = STN_SLD_CUS_ID            
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id            
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''            
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''            
and cva_addr_no = 0 and cva_addr_typ = ''L''            
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id            
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id            
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp            
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp            
where  coc_lst_sls_dt > '''+@LTD+''' and cus_cus_acct_typ != ''P''            
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1))            
+ '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),            
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '',''''),cus_actv,            
ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ  '                
-- and cus_actv = 1               
                
                                  
SET @query = 'INSERT INTO #tmp2 (Databases,CustID, CustName,Market,Branch,ShpBranch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,            
        LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt ,CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)            
SELECT  ''' + @Name + ''', CUS_CUS_ID as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, max(CUS_ADMIN_BRH) as Branch,Max(stn_shpt_brh),            
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail, isslp.slp_lgn_id, osslp.slp_lgn_id  , coc_lst_sls_dt,            
(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )            
,cus_cus_acct_typ, ''L'' as lostcustomer , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),            
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')            
,Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ',LTRIM(RTRIM(cva_cty))            
,SUM(stn_tot_val * '+ @CurrenyRate +') ,cus_actv ,crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ            
FROM ' + @DB + '_arbcoc_rec             
INNER JOIN ' + @DB + '_arrcus_rec ON coc_cus_id = CUS_CUS_ID            
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id             
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and  cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''            
left join ' + @DB + '_sahstn_rec on stn_sld_cus_id = CUS_CUS_ID            
join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no and orh_sld_cus_id = STN_SLD_CUS_ID            
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''            
and cva_addr_no = 0 and cva_addr_typ = ''L''            
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = coc_cmpy_id and shp_shp_to = 0 and shp_cus_id = coc_cus_id            
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp             
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp             
where coc_lst_sls_dt <= '''+@LTD+''' and cus_cus_acct_typ != ''P''            
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, isslp.slp_lgn_id, osslp.slp_lgn_id ,coc_lst_sls_dt ,cus_cus_acct_typ , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,            
Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)) ,REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')            
,cus_actv,ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ   '                
              
-- and cus_actv = 1                
print(@query + @query1)                    
set @query = @query + @query1;                          
        EXECUTE sp_executesql @query ;                           
                          
SET @query = 'INSERT INTO #tmp3 (Databases,CustID, CustName,Market,Branch, ShpBranch,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt     
  
    
       
       
          
,CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)                    
select ''' + @Name + ''',  cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,''''              
,ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail,  isslp.slp_lgn_id, osslp.slp_lgn_id  ,                          
 ( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )                          
 ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                       
 ,cus_cus_acct_typ  , '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                        
 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                         
 ,(select Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ' from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''NPAmt'',LTRIM(RTRIM(cva_cty))                    
 ,(select SUM(stn_tot_val * '+ @CurrenyRate +') from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''TotalSalesAmt'',cus_actv,              
 crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
from ' + @DB + '_arrcus_rec                 
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id                  
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''                       
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''                         
           and cva_addr_no = 0 and cva_addr_typ = ''L''                             
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id                    
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp                     
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                       
where cus_cus_id not in (select CustID from #tmp2) and cus_cus_acct_typ != ''P''                        
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH, isslp.slp_lgn_id, osslp.slp_lgn_id ,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' +                   
LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),                      
LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),                
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')              
,cus_actv,ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ  '                            
-- cus_actv= 1 and               
                                   
     print(@query)                              
        EXECUTE sp_executesql @query;                       
                                  
insert into #tmp1                                   
select * from #tmp2                  
--Union                           
--select * from #tmp5                        
Union                           
select * from #tmp3                  
Union                           
select * from #tmp4;                  
                          
Truncate table #tmp2;                          
Truncate table #tmp3;                            
Truncate table #tmp4;                 
--Truncate table #tmp5;                  
                                 
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                            
       END                             
    CLOSE ScopeCursor;                            
    DEALLOCATE ScopeCursor;                            
  END                            
  ELSE                            
     BEGIN                             
       print 'starting' ;                     
       Set @Name=(select Name from tbl_itech_DatabaseName_ALL where Prefix=''+ @DBNAME + '')                  
                              
      IF (UPPER(@DBNAME) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@DBNAME) = 'CN')                            
    begin                 
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                            
   Else if(UPPER(@DBNAME) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                            
   Else if(UPPER(@DBNAME) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                            
    Else if(UPPER(@DBNAME) = 'TWCN')                            
    begin                            
       SET @DB ='TW'                            
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
     print 'Ending else';                            
     print @CurrenyRate ;                            
                                 
     SET @sqltxt = 'INSERT INTO #tmp4 (Databases,CustID, CustName,Market,Branch, ShpBranch,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone      
  
    
      
        
          
     ,NPAmt,CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)                    
select  ''' + @Name + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,'''',              
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, IsNull(cvt_email,'''') as CustEmail              
, isslp.slp_lgn_id, osslp.slp_lgn_id  ,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )                          
,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                    
, cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                        
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                    
,(select Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ' from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''NPAmt'',LTRIM(RTRIM(cva_cty))                    
,(select SUM(stn_tot_val * '+ @CurrenyRate +') from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''TotalSalesAmt'',cus_actv,crd_acct_opn_dt,              
cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
from ' + @DB + '_arrcus_rec               
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id               
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''              
left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''                         
and cva_addr_no = 0 and cva_addr_typ = ''L''                              
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id           left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp                     
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                                  
where cus_cus_acct_typ = ''P'' '                  
              
--  and cus_actv = 1              
print(@sqltxt)                            
  set @execSQLtxt = @sqltxt;                             
  EXEC (@execSQLtxt);                   
                    
SET @sqltxt = 'INSERT INTO #tmp2 (Databases,CustID, CustName,Market,Branch, ShpBranch,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt ,   
           
CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)                
SELECT  ''' + @Name + ''', CUS_CUS_ID as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, Max(CUS_ADMIN_BRH) as Branch,Max(stn_shpt_brh),                
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail, isslp.slp_lgn_id, osslp.slp_lgn_id               
 , coc_lst_sls_dt ,              
(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                
,cus_cus_acct_typ, ''L'' as lostcustomer , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                
,Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ',LTRIM(RTRIM(cva_cty))                
,SUM(stn_tot_val * '+ @CurrenyRate +') ,cus_actv ,crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
FROM ' + @DB + '_arbcoc_rec                
INNER JOIN ' + @DB + '_arrcus_rec ON coc_cus_id = CUS_CUS_ID                
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id               
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''                    
left join ' + @DB + '_sahstn_rec on stn_sld_cus_id = CUS_CUS_ID                   
join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no and orh_sld_cus_id = STN_SLD_CUS_ID                   
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''                    
and cva_addr_no = 0 and cva_addr_typ = ''L''                     
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                    
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = coc_cmpy_id and shp_shp_to = 0 and shp_cus_id = coc_cus_id                     
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp                     
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                     
where coc_lst_sls_dt <= '''+@LTD+''' and cus_cus_acct_typ != ''P''                    
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, isslp.slp_lgn_id, osslp.slp_lgn_id ,coc_lst_sls_dt ,cus_cus_acct_typ , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM               
  
   
(cva_city)),''-'',''''),LTRIM(RTRIM(cva_cty)),LTRIM(RTRIM(cva_st_prov)) ,REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '',''''),cus_actv              
,ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
Union                     
SELECT  ''' + @Name + ''', CUS_CUS_ID as CustID,  CUS_CUS_LONG_NM as CustName,cuc_desc30 as Market,Max(CUS_ADMIN_BRH) as Branch,MAX(stn_shpt_brh),                    
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail,               
isslp.slp_lgn_id, osslp.slp_lgn_id ,coc_lst_sls_dt,               
(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                    
,cus_cus_acct_typ, '''' as lostcustomer , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),                     
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                     
,Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ',LTRIM(RTRIM(cva_cty))                    
,SUM(stn_tot_val * '+ @CurrenyRate +') ,cus_actv,crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
FROM ' + @DB + '_sahstn_rec                    
join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no and orh_sld_cus_id = STN_SLD_CUS_ID                  
INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID              
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id                 
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and  cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''                    
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''                    
and cva_addr_no = 0 and cva_addr_typ = ''L''                    
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                    
left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                     
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id                      
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp                     
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                     
where  coc_lst_sls_dt > '''+@LTD+''' and cus_cus_acct_typ != ''P''                     
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1))                    
+ '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),                        
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''') ,cus_actv              
,ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ                                     
   '                    
                 
-- and cus_actv = 1  and cus_actv = 1                        
print(@sqltxt)                            
  set @execSQLtxt = @sqltxt;                             
  EXEC (@execSQLtxt);                          
SET @sqltxt = 'INSERT INTO #tmp3 (Databases,CustID, CustName,Market,Branch,ShpBranch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt     
  
    
      
       
           
,CusCTY,TotalSalesAmt,cusActv,AccountDate ,ContactDate ,CustomerTitle,AddressType)                    
                      
select ''' + @Name + ''',  cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,'''',              
ISNULL(rtrim(cvt_frst_nm),'''') as firstNM, ISNULL(Rtrim(cvt_lst_nm),'''') as LastNM, ISNULL(cvt_email,'''') as CustEmail, isslp.slp_lgn_id, osslp.slp_lgn_id  ,                          
 ( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )                          
 ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )                       
 ,cus_cus_acct_typ  , '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),              
 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')                         
 ,(select Sum(CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val END)*' + @CurrenyRate + ' from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''NPAmt'',LTRIM(RTRIM(cva_cty))                
 ,(select SUM(stn_tot_val * '+ @CurrenyRate +') from ' + @DB + '_sahstn_rec where stn_sld_cus_id = cus_cus_id ) as ''TotalSalesAmt'',cus_actv,              
 crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ                
from ' + @DB + '_arrcus_rec                 
left join ' + @DB + '_arrcrd_rec on crd_cmpy_id = cus_cmpy_id and crd_cus_id = cus_cus_id               
left join ' + @DB + '_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id   and cvt_cus_ven_typ = ''C''                        
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id   and cva_cus_ven_typ = ''C''                         
           and cva_addr_no = 0 and cva_addr_typ = ''L''                         
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id                    
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp                     
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                       
where cus_cus_id not in (select CustID from #tmp2) and cus_cus_acct_typ != ''P''                        
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH, isslp.slp_lgn_id, osslp.slp_lgn_id ,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' +                   
LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),                      
LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty)),                        
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''') ,cus_actv               
,ISNULL(rtrim(cvt_frst_nm),''''),ISNULL(Rtrim(cvt_lst_nm),''''),ISNULL(cvt_email,''''),crd_acct_opn_dt,cvt_upd_dtts,cvt_ttl,cvt_addr_typ               
                   
 '                     
--cus_actv= 1 and                                    
  print(@sqltxt)                            
  set @execSQLtxt = @sqltxt;                             
  EXEC (@execSQLtxt);                            
                            
  insert into #tmp1                                   
select * from #tmp2  Union                           
select * from #tmp3  Union                           
select * from #tmp4;                          
                          
Truncate table #tmp2;                          
Truncate table #tmp3;                           
Truncate table #tmp4;                  
     END                            
                             
                             
                         
 if @IncludeInterco = '1'                          
 begin                          
  Select distinct Databases,RTRIM(LTrim(CustID))+'-'+Databases as CustID, CustName,Market,Branch,ShpBranch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,                        
  ( case when DATEDIFF(D,LastSaleDT,GETDATE()) > 182.5 And DATEDIFF(D,LastSaleDT,GETDATE()) < 547.5 then '6MD'      
  when DATEDIFF(D,LastSaleDT,GETDATE()) > 93.2 And DATEDIFF(D,LastSaleDT,GETDATE()) < 182.5 then '3MD'      
  else       
  (CAse When cusActv = '0' then 'I' when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) end) as CusType,                        
  LostCustomer , cusAddress ,cusPCD,cusCity ,CusState, CusPhone ,NPAmt ,CusCTY,TotalSalesAmt,AccountDate,ContactDate,CustomerTitle,              
  Case When AddressType = 'B' then 'Bought From'      
          When AddressType = 'F' then 'Ship From'                 
          When AddressType = 'I' then 'Invoice To'                 
          When AddressType = 'L' then 'Legal Address'                 
          When AddressType = 'O' then 'Sold To'                 
          When AddressType = 'P' then 'Payment To'                 
          When AddressType = 'S' then 'Shipping To'                 
        Else '' End AS AddressType              
   from #tmp1 where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ') --and Market  = 'Interco' --include interco true              
   order by CusType;                        
  End                          
  Else                          
  Begin                          
 Select distinct Databases,RTRIM(LTrim(CustID))+'-'+Databases as CustID, CustName,Market,Branch,ShpBranch, ContactFirstNM, ContactLastNM,CustEmail,      
 SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,                       
 ( case when DATEDIFF(D,LastSaleDT,GETDATE()) > 182.5 And DATEDIFF(D,LastSaleDT,GETDATE()) < 547.5 then '6MD'      
  when DATEDIFF(D,LastSaleDT,GETDATE()) > 93.2 And DATEDIFF(D,LastSaleDT,GETDATE()) < 182.5 then '3MD'      
  else       
  (CAse When cusActv = '0' then 'I' when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) end) as CusType,                        
  LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone,NPAmt ,CusCTY,TotalSalesAmt,AccountDate,ContactDate,CustomerTitle,              
  Case When AddressType = 'B' then 'Bought From'                 
          When AddressType = 'F' then 'Ship From'                 
          When AddressType = 'I' then 'Invoice To'                 
          When AddressType = 'L' then 'Legal Address'                 
          When AddressType = 'O' then 'Sold To'                 
          When AddressType = 'P' then 'Payment To'                 
          When AddressType = 'S' then 'Shipping To'                 
        Else '' End AS AddressType              
   from #tmp1 where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ')                          
  and (Market  != 'Interco' or Market = '' or Market is null)                    
  order by CusType;                        
  End                          
                          
                            
   drop table #tmp1 ;                     
   drop table #tmp2 ;                          
   drop table #tmp3 ;                  
   drop table #tmp4 ;                 
   -- drop table #tmp5 ;                           
END                
                            
                        
-- exec [sp_itech_EmailContactsMarketingReport] 'US', 'ALL','0','0' -- 42809 --35647                  
-- exec [sp_itech_EmailContactsMarketingReport] 'CA', 'ALL','0','0' -- 3767 , 3753,3667            
-- exec [sp_itech_CustomerList_ALDP_V1] 'NO', 'ALL','1'   -- 10566  -- old SP                     
                        
           
/*                        
20190130                        
Mail sub: Report Change Request                        
          
20190731          
mail sub:Report Error        
      
 20191108      
 Mail sub:Home > STRATIXReports > Sales Performance > Email Marketing Report V3 12-2018 Update Request      
   
 Date 20200501  
 Mail sub:Email Contacts Marketing  Report Ship to Address Request  

 20200513
 Mail Sub:Email Marketing and Email Contacts Report Selling Branch Discrepancy
 Modification: use CUS_ADMIN_BRH instead of orh_ord_brh
*/
GO
