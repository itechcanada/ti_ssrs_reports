USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerByProduct_Eaton]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Mukesh >                      
-- Create date: <05 May 2019>                      
-- Description: <Getting top 50 customers for SSRS reports>                
                  
-- =============================================                      
CREATE PROCEDURE [dbo].[sp_itech_CustomerByProduct_Eaton] @FromDate datetime, @ToDate datetime,               
@DateRange int, @IncludeInterco char = '0'                    
AS                      
BEGIN               
         
                       
 SET NOCOUNT ON;       
 

declare @sqltxt varchar(max)                      
declare @execSQLtxt varchar(max)                      
declare @DB varchar(100)                    
declare @FD varchar(10)                      
declare @TD varchar(10)                      
declare @NOOfCust varchar(15)                      
DECLARE @CurrenyRate varchar(15)                      
DECLARE @IsExcInterco char(1)            
                      
CREATE TABLE #tmp (  CustID varchar(15)                       
        ,CustName Varchar(65)                      
        ,InvDt varchar(15)                      
        ,Product   varchar(300)                      
        ,NoOfPcs Varchar(10)                      
        , Inches    DECIMAL(20, 2)                      
        , Weight  DECIMAL(20, 2)                      
        , Part   VARCHAR(50)                       
        ,InvNo Varchar(50)                      
        , Date     varchar(65)                      
        ,Form  varchar(35)                      
        ,Grade  varchar(35)                      
        ,Size  varchar(35)                      
        ,Finish  varchar(35)                      
        ,Measure  DECIMAL(20, 2)                      
        ,TotalValue DECIMAL(20, 2)            
  ,MatGP DECIMAL(20, 2)                     
        ,GPPct DECIMAL(20, 2)                      
  ,NPAmt DECIMAL(20, 2)          
        ,NPPct DECIMAL(20, 2)                     
        ,PONumber Varchar(75)                  
        ,TransportNo Varchar(30)     ,                
        MktCatg varchar(30),                
        ShpWhs varchar(3),            
        Branch varchar(10)             
        ,SalesPersonIs Varchar(10)            
        ,SalesPersonOs Varchar(10)            
        ,sizeDesc Varchar(50)           
        ,millName Varchar(40)               
        ,MillSurCost Decimal(20,2)       
        ,MillDlyDate Varchar(10)      
                 );                       
                     
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @Prefix VARCHAR(5);                      
DECLARE @Name VARCHAR(15);                      
DECLARE @CusID varchar(max);                      
Declare @Value as varchar(500);                      
DECLARE @CustIDLength int;   ---SELECT DATALENGTH(yourtextfield)                      
             
if (@IncludeInterco = '0')            
BEGIN            
set @IsExcInterco ='T'            
END            
                      
                
                      
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month                      
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month                      
 End                   
else if @DateRange=2 -- Last 7 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End                      
else if @DateRange=3 -- Last 14 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End      
else if @DateRange=4 --Last 12 months excluding all the days in the current month                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                      
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month                      
 End                      
else                      
 Begin                      
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                      
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                      
 End                   
                      
 insert into #tmp       
 Exec  sp_itech_CustomerByProduct_NP @FD, @TD ,'US','0',@DateRange,'8960,ROMAA,293,612,1684,2603,2826,3770,5307,5487,7627,12651,ADTUA,AEMAA,CLMEA,CLTUA,DISCA,EAJAA,EDMAA,INAEA,KAMAA,LC11900,LCO8000,MACOA,PEMAA,SCMAA,TUMAA,855, 10120, 11930, 12088, 12217,
  
     
12645, 6302, 9309, 12681, 13211, 13219, 141, 2373, 654, 9890, 3451, 5567, 5724, 6010, 614, 7566, 522,1089,1242,1749,1806,2372,2382,2631,3532,4242,4457,12535,4894 ,9886,367,12731,3041,11588,6854,8897,5701,2143,2897,2862,5706,VACOA,286,170,166,26,470,VACOB,
  
    
56,171,10489,ALMAA,tbd,4787,10337','0','I',@IncludeInterco      
       
 insert into #tmp       
 Exec  sp_itech_CustomerByProduct_NP @FD, @TD ,'UK','0',@DateRange,'3445','0','I',@IncludeInterco      
       
 insert into #tmp       
 Exec  sp_itech_CustomerByProduct_NP @FD, @TD ,'TW','0',@DateRange,'593,470','0','I',@IncludeInterco      
       
 insert into #tmp       
 Exec  sp_itech_CustomerByProduct_NP @FD, @TD ,'CN','0',@DateRange,'171,56','0','I',@IncludeInterco      
       
       
 select * from #tmp;      
 drop table #tmp;                   
                      
END                      
                      
 -- exec sp_itech_CustomerByProduct_Eaton '01/23/2020', '03/23/2020' ,'0','0'      
 -- Remove Eaton User 795      
 /*          
*/ 
GO
