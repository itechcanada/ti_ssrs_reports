USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_GetLastSalesDate]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukes>    
-- Create date: <11 MAr 2016>    
-- Description: <Description, ,>    
-- =============================================    
CREATE FUNCTION [dbo].[fun_itech_GetLastSalesDate](@customerID Varchar(10), @compareDate Varchar(10)) 
RETURNS Varchar(10)    
AS    
BEGIN    
declare @lastSalesDate Varchar(10);    
    
declare @returnStatus Varchar(10)  ;    
set @returnStatus = '';    
    
 if(SUBSTRING(@customerID,1,1) ! = 'L')    
 return @compareDate ;    
     
 select  @lastSalesDate = convert(Varchar(10),coc_lst_sls_dt,120)  from PS_arbcoc_rec where coc_cus_id = SUBSTRING(@customerID,2,len(@customerID));    
     
 if (@compareDate is null OR @compareDate = '')    
 begin    
 set @returnStatus =  @lastSalesDate;    
 end     
 else if (@lastSalesDate is not null and @lastSalesDate != '')    
 begin    
   set @returnStatus =  case When (@compareDate > @lastSalesDate) then @compareDate else @lastSalesDate end;    
 end    
     
  
return  @returnStatus ;    
END    
GO
