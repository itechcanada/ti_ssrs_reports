USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_sahsat]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                              
-- =============================================                                                                
-- Author:  <Author,Sumit>                                                                
-- Create date: <Create Date,11/25/2020,>                                                                
-- Description: <Description,Sales Analysis Transactions,>                                                              
-- =============================================                                                                
CREATE PROCEDURE [dbo].[DE_sahsat]                                                                                                       
AS                                                                
BEGIN                                                                
 -- SET NOCOUNT ON added to prevent extra result sets from                                                                
 -- interfering with SELECT statements.                                                                
SET NOCOUNT ON;                                                                

truncate table dbo.DE_sahsat_rec; 

-- Insert statements for procedure here   
-- There is a problem in column sat_part  

Insert into dbo.DE_sahsat_rec                                                                 
SELECT *                                                                
FROM [LIVEDESTX_IW].[livedestxdb_iw].[informix].[sahsat_rec]                                                                
                 
END         
       
/*

*/
GO
