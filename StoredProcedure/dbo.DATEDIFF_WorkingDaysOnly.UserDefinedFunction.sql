USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[DATEDIFF_WorkingDaysOnly]    Script Date: 03-11-2021 16:23:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[DATEDIFF_WorkingDaysOnly](@StartDate datetime, @EndDate datetime)
RETURNS INT
AS
BEGIN
 RETURN (DATEDIFF(dd, @StartDate, @EndDate) + 1) --Number of days between Start and End
 -
 (
  (DATEDIFF(wk, @StartDate, @EndDate) * 2) --Weekend Days
  +(CASE WHEN DATENAME(dw, @StartDate) = 'Sunday' THEN 1 ELSE 0 END) --Taking into consideration the startdate being a Sunday
  +(CASE WHEN DATENAME(dw, @EndDate) = 'Saturday' THEN 1 ELSE 0 END) --Taking into consideration the enddate being a Saturday
  )
END
GO
