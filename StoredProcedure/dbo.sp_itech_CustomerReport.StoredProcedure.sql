USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerReport]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
  
CREATE proc [dbo].[sp_itech_CustomerReport] @DBNAME varchar(50)    
as     
Begin    
 DECLARE @sqltxt varchar(8000)    
 DECLARE @12FD varchar(10)    
 DECLARE @DB varchar(50)    
 DECLARE @CurrenyRate varchar(15)    
    
 DECLARE @DatabaseName VARCHAR(45);            
 DECLARE @Prefix VARCHAR(45);            
 DECLARE @Name VARCHAR(15);     
  
 CREATE TABLE #tmp (    
  [Database] VARCHAR(10),     
  Branch varchar(45),     
  CustID varchar(45),     
  CustName varchar(45),     
  cub_apd_6 Decimal(20,0) ,     
  cub_apd_7 Decimal(20,0) ,     
  cub_apd_8 Decimal(20,0) ,     
  cub_apd_9 Decimal(20,0) ,     
  cub_apd_10 Decimal(20,0) ,     
  cub_apd_11 Decimal(20,0) ,     
  cub_apd_12 Decimal(20,0) ,     
  TotSales Decimal(20,2),     
  NetAvgProfit Decimal (20,2),  
  UpdRefNo varchar(45)  
  );  
  
 CREATE TABLE #tmpSummary (    
  [Database] VARCHAR(10),     
  Branch varchar(45),     
  CustID varchar(45),     
  CustName varchar(45),     
  Avg_Pmt_Days_6_Months Decimal(20,0) ,     
  TotSales Decimal(20,2),     
  NetAvgProfit Decimal (20,2), 
  NetProfit Decimal(20,2)
  );    
    
 set @12FD = convert(varchar(10), DATEADD(year,-1,GETDATE()),120)    
 Print 'Last Year Date '+ @12FD    
    
 set @DB = @DBNAME    
    
 IF @DBNAME = 'ALL'            
 BEGIN            
  DECLARE ScopeCursor CURSOR FOR            
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_V2             
  OPEN ScopeCursor;            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
  WHILE @@FETCH_STATUS = 0            
  BEGIN            
   SET @DB= @Prefix             
            
   IF (UPPER(@DB) = 'TW')            
   begin            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))            
   End            
   Else if (UPPER(@DB) = 'NO')            
   begin            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))            
   End            
   Else if (UPPER(@DB) = 'CA')            
   begin            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))            
   End            
   Else if (UPPER(@DB) = 'CN')            
   begin            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))            
   End            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )            
   begin            
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))             
   End            
   Else if(UPPER(@DB) = 'UK')            
   begin            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))            
   End             
   Else if(UPPER(@DB) = 'DE')            
   begin            
    SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))            
   End    
    
  --set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, Avg_Pmt_Days_6_Months, TotSales, NetAvgProfit)    
   --select '''+ @DB +''', sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm,    
   --(case when (    
   --(case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
   --(case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) =0 then 0 else (cub_apd_6 +  cub_apd_7 + cub_apd_8 + cub_apd_9+ cub_apd_10 + cub_apd_11 + cub_apd_12)/(    
   --(case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
   --(case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) end )    
   --as Avg_Pmt_Days_6_Months,    
   --sum(sat_tot_val) * '+ @CurrenyRate +' as TotSales,    
   --sum(sat_npft_avg_val) * '+ @CurrenyRate +' as NetAvgProfit    
   --from ' + @DB + '_sahsat_Rec    
   --join ' + @DB + '_arrcus_rec on cus_cmpy_id= Sat_cmpy_id and cus_cus_id=sat_sld_cus_id    
   --join ' + @DB + '_arrcrd_rec on crd_cmpy_id= Sat_cmpy_id and crd_cus_id=sat_sld_cus_id    
   --join ' + @DB + '_arbcub_rec on cub_cmpy_id= Sat_cmpy_id and cub_cus_id=sat_sld_cus_id    
   --where sat_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8    
   --group by sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm, cub_apd_6 ,  cub_apd_7 , cub_apd_8 ,cub_apd_9, cub_apd_10 , cub_apd_11 , cub_apd_12'  
  
   set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, cub_apd_6, cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12, TotSales, NetAvgProfit, UpdRefNo)  
   select '''+ @DB +''', sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm, cub_apd_6 , cub_apd_7 , cub_apd_8 , cub_apd_9 , cub_apd_10 , cub_apd_11 ,cub_apd_12 ,    
  (sat_tot_val * '+ @CurrenyRate +') as sat_tot_val , (sat_npft_avg_val * '+ @CurrenyRate +') as sat_npft_avg_val , sat_upd_ref_no  
  from ' + @DB + '_sahsat_Rec    
  join ' + @DB + '_arrcus_rec on cus_cmpy_id= Sat_cmpy_id and cus_cus_id=sat_sld_cus_id    
  join ' + @DB + '_arrcrd_rec on crd_cmpy_id= Sat_cmpy_id and crd_cus_id=sat_sld_cus_id    
  join ' + @DB + '_arbcub_rec on cub_cmpy_id= Sat_cmpy_id and cub_cus_id=sat_sld_cus_id    
  where sat_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8  order by sat_shpt_brh, sat_sld_cus_id'  
    
   print @sqltxt;    
   exec (@sqltxt);   
     
   set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, cub_apd_6, cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12, TotSales, NetAvgProfit, UpdRefNo)  
   select '''+ @DB +''', jvh_inv_brh, jvh_sld_cus_id, cus_cus_long_nm, cub_apd_6 , cub_apd_7 , cub_apd_8 , cub_apd_9 , cub_apd_10 , cub_apd_11 ,cub_apd_12 ,   
   (jvh_ar_val * '+ @CurrenyRate +') as jvh_ar_val , 100 as netAvgProfit , jvh_upd_ref_no  
   from ' + @DB + '_ivjjvh_rec    
  join ' + @DB + '_arrcus_rec on cus_cmpy_id= jvh_cmpy_id and cus_cus_id=jvh_sld_cus_id    
  join ' + @DB + '_arrcrd_rec on crd_cmpy_id= jvh_cmpy_id and crd_cus_id=jvh_sld_cus_id    
  join ' + @DB + '_arbcub_rec on cub_cmpy_id= jvh_cmpy_id and cub_cus_id=jvh_sld_cus_id    
  where jvh_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8    
  and jvh_upd_ref_no not in (select distinct UpdRefNo from #tmp where [Database] = '''+ @DB +''')  
  order by jvh_inv_brh, jvh_sld_cus_id'  
    
 print @sqltxt;    
   exec (@sqltxt);   
  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
  End    
 End    
 Else    
 Begin    
  IF (UPPER(@DB) = 'TW')            
  begin            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))            
  End            
  Else if (UPPER(@DB) = 'NO')            
  begin            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))            
  End            
  Else if (UPPER(@DB) = 'CA')            
  begin            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))            
  End            
  Else if (UPPER(@DB) = 'CN')            
  begin            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))            
  End            
  Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )            
  begin            
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))            
  End            
  Else if(UPPER(@DB) = 'UK')            
  begin            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))            
  End            
  Else if(UPPER(@DB) = 'DE')            
  begin            
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))                   
  End    
      
    
  --set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, Avg_Pmt_Days_6_Months, TotSales, NetAvgProfit)    
  --select '''+ @DB +''', sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm,   
  --(case when (    
  --(case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
  --(case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) =0 then 0 else (cub_apd_6 +  cub_apd_7 + cub_apd_8 + cub_apd_9+ cub_apd_10 + cub_apd_11 + cub_apd_12)/(    
  --(case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
  --(case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) end )    
  --as Avg_Pmt_Days_6_Months,    
  --sum(sat_tot_val) * '+ @CurrenyRate +' as TotSales,    
  --sum(sat_npft_avg_val) * '+ @CurrenyRate +' as NetAvgProfit    
  --from ' + @DB + '_sahsat_Rec    
  --join ' + @DB + '_arrcus_rec on cus_cmpy_id= Sat_cmpy_id and cus_cus_id=sat_sld_cus_id    
  --join ' + @DB + '_arrcrd_rec on crd_cmpy_id= Sat_cmpy_id and crd_cus_id=sat_sld_cus_id    
  --join ' + @DB + '_arbcub_rec on cub_cmpy_id= Sat_cmpy_id and cub_cus_id=sat_sld_cus_id    
  --where sat_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8    
  --group by sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm, cub_apd_6 ,  cub_apd_7 , cub_apd_8 ,cub_apd_9, cub_apd_10 , cub_apd_11 , cub_apd_12'    
    
  set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, cub_apd_6, cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12, TotSales, NetAvgProfit, UpdRefNo)  
   select '''+ @DB +''', sat_shpt_brh, sat_sld_cus_id, cus_cus_long_nm, cub_apd_6 , cub_apd_7 , cub_apd_8 , cub_apd_9 , cub_apd_10 , cub_apd_11 ,cub_apd_12 ,    
  (sat_tot_val * '+ @CurrenyRate +') as sat_tot_val , (sat_npft_avg_val * '+ @CurrenyRate +') as sat_npft_avg_val , sat_upd_ref_no  
  from ' + @DB + '_sahsat_Rec    
  join ' + @DB + '_arrcus_rec on cus_cmpy_id= Sat_cmpy_id and cus_cus_id=sat_sld_cus_id    
  join ' + @DB + '_arrcrd_rec on crd_cmpy_id= Sat_cmpy_id and crd_cus_id=sat_sld_cus_id    
  join ' + @DB + '_arbcub_rec on cub_cmpy_id= Sat_cmpy_id and cub_cus_id=sat_sld_cus_id    
  where sat_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8  order by sat_shpt_brh, sat_sld_cus_id'  
  
  print @sqltxt;    
  exec (@sqltxt);  
    
  set @sqltxt = 'insert into #tmp ([Database], Branch, CustID, CustName, cub_apd_6, cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12, TotSales, NetAvgProfit, UpdRefNo)  
   select '''+ @DB +''', jvh_inv_brh, jvh_sld_cus_id, cus_cus_long_nm, cub_apd_6 , cub_apd_7 , cub_apd_8 , cub_apd_9 , cub_apd_10 , cub_apd_11 ,cub_apd_12 ,   
   (jvh_ar_val * '+ @CurrenyRate +') as jvh_ar_val , 100 as netAvgProfit , jvh_upd_ref_no  
   from ' + @DB + '_ivjjvh_rec    
  join ' + @DB + '_arrcus_rec on cus_cmpy_id= jvh_cmpy_id and cus_cus_id=jvh_sld_cus_id    
  join ' + @DB + '_arrcrd_rec on crd_cmpy_id= jvh_cmpy_id and crd_cus_id=jvh_sld_cus_id    
  join ' + @DB + '_arbcub_rec on cub_cmpy_id= jvh_cmpy_id and cub_cus_id=jvh_sld_cus_id    
  where jvh_inv_dt > '''+ @12FD +''' and crd_pmt_trm = 8    
  and jvh_upd_ref_no not in (select distinct UpdRefNo from #tmp where [Database] = '''+ @DB +''')  
  order by jvh_inv_brh, jvh_sld_cus_id'  
    
 print @sqltxt;    
 exec (@sqltxt);  
  
    
 End    
   
 set @sqltxt = 'insert into #tmpSummary ([Database], Branch, CustID, CustName, Avg_Pmt_Days_6_Months, TotSales, NetAvgProfit,NetProfit)  
 select [Database], Branch, CustID, CustName,    
 (case when (    
 (case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
 (case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) =0 then 0 else (cub_apd_6 +  cub_apd_7 + cub_apd_8 + cub_apd_9+ cub_apd_10 + cub_apd_11 + cub_apd_12)/(    
 (case when cub_apd_6 > 0 then 1 else 0 end) + (case when cub_apd_7 > 0 then 1 else 0 end) + (case when cub_apd_8 > 0 then 1 else 0 end) + (case when cub_apd_9 > 0 then 1 else 0 end) +    
 (case when cub_apd_10 > 0 then 1 else 0 end) + (case when cub_apd_11 > 0 then 1 else 0 end) + (case when cub_apd_12 > 0 then 1 else 0 end)) end )    
 as Avg_Pmt_Days_6_Months,    
 sum(TotSales) as TotSales,    
 sum(NetAvgProfit) as NetAvgProfit, (sum(TotSales) / sum(NetAvgProfit)) as NetProfit 
 from #tmp    
 group by [Database],Branch, CustID, CustName, cub_apd_6 ,  cub_apd_7 , cub_apd_8 ,cub_apd_9, cub_apd_10 , cub_apd_11 , cub_apd_12'  
 print @sqltxt;    
 exec (@sqltxt);  
  
   
 select * from #tmpSummary;    
 drop table #tmp;    
 drop table #tmpSummary;  
End  
  
/*  
20210806  
Add direct billing records  
Mail: FW: Customer Report_V1_20210618.xlsx, RE: eCIM Call #368557  
2021/08/20
Add NetProfit column
Mail: FWD: Customer Report_V1_20210618.xlsx
*/  
  
GO
