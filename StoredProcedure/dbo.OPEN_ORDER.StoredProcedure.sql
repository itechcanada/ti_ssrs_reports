USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[OPEN_ORDER]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <10/5/2012>
-- Description:	<Open Sales Order>
-- =============================================
CREATE PROCEDURE [dbo].[OPEN_ORDER] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

CREATE TABLE #OPEN_ORDER
(
country varchar(3),
ord_ord_no varchar(10),
ord_ord_pfx varchar(10),
cus_cus_id varchar(10),
ord_ord_itm varchar(10),
ord_ord_brh varchar(3),
cus_cus_long_nm varchar(50),
orl_bal_pcs int,
orl_bal_msr float,
ord_bal_wgt float,
chl_chrg_val float,
orl_atnbl_rdy_dt date,
orl_due_dt_ent int,
product varchar(50),
sales varchar(35)
)
/*US*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #US1
FROM [US_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #US2 
 FROM [US_ortord_rec] b 
INNER JOIN [US_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #US1 a
INNER JOIN #US2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [US_scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [US_mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [US_ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [US_tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm

/*UK*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #UK1
FROM [UK_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #UK2 
 FROM [UK_ortord_rec] b 
INNER JOIN [UK_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm,f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #UK1 a
INNER JOIN #UK2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [UK_ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [UK_scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [UK_mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [UK_ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [UK_tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm

/*CANADA*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #CA1
FROM [CA_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #CA2 
 FROM [CA_ortord_rec] b 
INNER JOIN [CA_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #CA1 a
INNER JOIN #CA2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [CA_ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [CA_scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [CA_mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [CA_ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [CA_tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm

/*TAIWAN*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #TW1
FROM [TW_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #TW2 
 FROM [TW_ortord_rec] b 
INNER JOIN [TW_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #TW1 a
INNER JOIN #TW2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [TW_ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [TW_scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [TW_mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [TW_ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [TW_tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm

/*NORWAY*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #NO1
FROM [NO_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #NO2 
 FROM [NO_ortord_rec] b 
INNER JOIN [NO_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #NO1 a
INNER JOIN #NO2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [NO_ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [NO_scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [NO_mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [NO_ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [NO_tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm

SELECT * FROM #OPEN_ORDER

DROP TABLE #US1
DROP TABLE #US2
DROP TABLE #UK1
DROP TABLE #UK2
DROP TABLE #CA1
DROP TABLE #CA2
DROP TABLE #TW1
DROP TABLE #TW2
DROP TABLE #NO1
DROP TABLE #NO2
DROP TABLE #OPEN_ORDER


/*
SELECT 'USA' as country, d.ord_ord_no, d.ord_ord_pfx, e.cus_cus_id, d.ord_ord_itm, d.ord_ord_brh, e.cus_cus_long_nm, d.ord_ord_pcs,d.ord_bal_msr,d.ord_bal_wgt,a.chl_chrg_val,
 b.orl_atnbl_rdy_dt, b.orl_due_dt_ent,c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as Product, z.usr_nm as Sales--, g.prs_desc15

FROM [US_ortorh_rec] x
INNER JOIN [US_ortchl_rec] a ON x.orh_ord_pfx=a.chl_ref_pfx AND x.orh_ord_no=a.chl_ref_no 
INNER JOIN [US_scrslp_rec] w ON x.orh_tkn_slp=w.slp_slp
INNER JOIN [US_mxrusr_rec] z ON w.slp_lgn_id=z.usr_lgn_id
INNER JOIN [US_ortorl_rec] b ON a.chl_ref_pfx=b.orl_ord_pfx AND a.chl_ref_no=b.orl_ord_no AND a.chl_ref_itm=b.orl_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.orl_ord_pfx=c.ipd_ref_pfx AND b.orl_ord_no=c.ipd_ref_no AND b.orl_ord_itm=c.ipd_ref_itm
INNER JOIN [US_ortord_rec] d ON c.ipd_ref_pfx=d.ord_ord_pfx AND c.ipd_ref_no=d.ord_ord_no AND c.ipd_ref_itm=d.ord_ord_itm
INNER JOIN [US_arrcus_rec] e ON d.ord_sld_cus_id=e.cus_cus_id 

WHERE 
a.chl_ref_pfx='SO' AND
b.orl_ord_pfx='SO' AND
c.ipd_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
d.ord_sts_actn= 'A' AND
a.chl_chrg_val<>0

UNION
SELECT 'UK' as country, d.ord_ord_no,  d.ord_ord_pfx, e.cus_cus_id, d.ord_ord_itm, d.ord_ord_brh, e.cus_cus_long_nm, d.ord_ord_pcs,d.ord_bal_msr,d.ord_bal_wgt,a.chl_chrg_val,
 b.orl_atnbl_rdy_dt, b.orl_due_dt_ent,c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as Product, z.usr_nm as Sales

FROM [UK_ortorh_rec] x
INNER JOIN [UK_ortchl_rec] a ON x.orh_ord_pfx=a.chl_ref_pfx AND x.orh_ord_no=a.chl_ref_no 
INNER JOIN [UK_scrslp_rec] w ON x.orh_tkn_slp=w.slp_slp
INNER JOIN [UK_mxrusr_rec] z ON w.slp_lgn_id=z.usr_lgn_id
INNER JOIN [UK_ortorl_rec] b ON a.chl_ref_pfx=b.orl_ord_pfx AND a.chl_ref_no=b.orl_ord_no AND a.chl_ref_itm=b.orl_ord_itm
INNER JOIN [UK_tctipd_rec] c ON b.orl_ord_pfx=c.ipd_ref_pfx AND b.orl_ord_no=c.ipd_ref_no AND b.orl_ord_itm=c.ipd_ref_itm
INNER JOIN [UK_ortord_rec] d ON c.ipd_ref_pfx=d.ord_ord_pfx AND c.ipd_ref_no=d.ord_ord_no AND c.ipd_ref_itm=d.ord_ord_itm
INNER JOIN [UK_arrcus_rec] e ON d.ord_sld_cus_id=e.cus_cus_id 
WHERE 
a.chl_ref_pfx='SO' AND
b.orl_ord_pfx='SO' AND
c.ipd_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
d.ord_sts_actn= 'A' AND
a.chl_chrg_val<>0

UNION
SELECT 'CANADA' as country, d.ord_ord_no, d.ord_ord_pfx, e.cus_cus_id,  d.ord_ord_itm, d.ord_ord_brh, e.cus_cus_long_nm, d.ord_ord_pcs,d.ord_bal_msr,d.ord_bal_wgt,a.chl_chrg_val,
 b.orl_atnbl_rdy_dt, b.orl_due_dt_ent,c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as Product, z.usr_nm as Sales

FROM [CA_ortorh_rec] x
INNER JOIN [CA_ortchl_rec] a ON x.orh_ord_pfx=a.chl_ref_pfx AND x.orh_ord_no=a.chl_ref_no 
INNER JOIN [CA_scrslp_rec] w ON x.orh_tkn_slp=w.slp_slp
INNER JOIN [CA_mxrusr_rec] z ON w.slp_lgn_id=z.usr_lgn_id
INNER JOIN [CA_ortorl_rec] b ON a.chl_ref_pfx=b.orl_ord_pfx AND a.chl_ref_no=b.orl_ord_no AND a.chl_ref_itm=b.orl_ord_itm
INNER JOIN [CA_tctipd_rec] c ON b.orl_ord_pfx=c.ipd_ref_pfx AND b.orl_ord_no=c.ipd_ref_no AND b.orl_ord_itm=c.ipd_ref_itm
INNER JOIN [CA_ortord_rec] d ON c.ipd_ref_pfx=d.ord_ord_pfx AND c.ipd_ref_no=d.ord_ord_no AND c.ipd_ref_itm=d.ord_ord_itm
INNER JOIN [CA_arrcus_rec] e ON d.ord_sld_cus_id=e.cus_cus_id 
WHERE 
a.chl_ref_pfx='SO' AND
b.orl_ord_pfx='SO' AND
c.ipd_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
d.ord_sts_actn= 'A' AND
a.chl_chrg_val<>0

UNION
SELECT 'TAIWAN' as country, d.ord_ord_no, d.ord_ord_pfx, e.cus_cus_id,  d.ord_ord_itm, d.ord_ord_brh, e.cus_cus_long_nm, d.ord_ord_pcs,d.ord_bal_msr,d.ord_bal_wgt,a.chl_chrg_val,
 b.orl_atnbl_rdy_dt, b.orl_due_dt_ent,c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as Product, z.usr_nm as Sales

FROM [TW_ortorh_rec] x
INNER JOIN [TW_ortchl_rec] a ON x.orh_ord_pfx=a.chl_ref_pfx AND x.orh_ord_no=a.chl_ref_no 
INNER JOIN [TW_scrslp_rec] w ON x.orh_tkn_slp=w.slp_slp
INNER JOIN [TW_mxrusr_rec] z ON w.slp_lgn_id=z.usr_lgn_id
INNER JOIN [TW_ortorl_rec] b ON a.chl_ref_pfx=b.orl_ord_pfx AND a.chl_ref_no=b.orl_ord_no AND a.chl_ref_itm=b.orl_ord_itm
INNER JOIN [TW_tctipd_rec] c ON b.orl_ord_pfx=c.ipd_ref_pfx AND b.orl_ord_no=c.ipd_ref_no AND b.orl_ord_itm=c.ipd_ref_itm
INNER JOIN [TW_ortord_rec] d ON c.ipd_ref_pfx=d.ord_ord_pfx AND c.ipd_ref_no=d.ord_ord_no AND c.ipd_ref_itm=d.ord_ord_itm
INNER JOIN [TW_arrcus_rec] e ON d.ord_sld_cus_id=e.cus_cus_id 
WHERE 
a.chl_ref_pfx='SO' AND
b.orl_ord_pfx='SO' AND
c.ipd_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
d.ord_sts_actn= 'A' AND
a.chl_chrg_val<>0

UNION
SELECT 'NORWAY' as country, d.ord_ord_no, d.ord_ord_pfx, e.cus_cus_id,  d.ord_ord_itm, d.ord_ord_brh, e.cus_cus_long_nm, d.ord_ord_pcs,d.ord_bal_msr,d.ord_bal_wgt,a.chl_chrg_val,
 b.orl_atnbl_rdy_dt, b.orl_due_dt_ent,c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as Product, z.usr_nm as Sales

FROM [NO_ortorh_rec] x
INNER JOIN [NO_ortchl_rec] a ON x.orh_ord_pfx=a.chl_ref_pfx AND x.orh_ord_no=a.chl_ref_no 
INNER JOIN [NO_scrslp_rec] w ON x.orh_tkn_slp=w.slp_slp
INNER JOIN [NO_mxrusr_rec] z ON w.slp_lgn_id=z.usr_lgn_id
INNER JOIN [NO_ortorl_rec] b ON a.chl_ref_pfx=b.orl_ord_pfx AND a.chl_ref_no=b.orl_ord_no AND a.chl_ref_itm=b.orl_ord_itm
INNER JOIN [NO_tctipd_rec] c ON b.orl_ord_pfx=c.ipd_ref_pfx AND b.orl_ord_no=c.ipd_ref_no AND b.orl_ord_itm=c.ipd_ref_itm
INNER JOIN [NO_ortord_rec] d ON c.ipd_ref_pfx=d.ord_ord_pfx AND c.ipd_ref_no=d.ord_ord_no AND c.ipd_ref_itm=d.ord_ord_itm
INNER JOIN [NO_arrcus_rec] e ON d.ord_sld_cus_id=e.cus_cus_id 
WHERE 
a.chl_ref_pfx='SO' AND
b.orl_ord_pfx='SO' AND
c.ipd_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
d.ord_sts_actn= 'A' AND
a.chl_chrg_val<>0


*/
END
GO
