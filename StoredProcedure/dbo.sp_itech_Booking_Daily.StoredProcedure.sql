USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Booking_Daily]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
              
-- =============================================              
-- Author:  <Mukesh>              
-- Create date: <10 Jan 2014>              
-- Description: <Booking Daily Reports>             
-- Last updated by Mukesh             
-- Last updated Date 16 Jun 2015            
-- Last updated desc: add IS and OS sales person              
-- =============================================              
CREATE  PROCEDURE [dbo].[sp_itech_Booking_Daily]  @DBNAME varchar(50), @version char = '0', @Branch Varchar(3) = 'ALL'              
AS              
BEGIN              
            
        
 -- SET NOCOUNT ON added to prevent extra result sets from              
 -- interfering with SELECT statements.              
 SET NOCOUNT ON;              
declare @DB varchar(100);              
declare @sqltxt varchar(6000);              
declare @execSQLtxt varchar(7000);              
DECLARE @CountryName VARCHAR(25);                 
DECLARE @prefix VARCHAR(15);                 
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @CurrenyRate varchar(15);                
              
-- Set @DB = UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'              
              
CREATE TABLE #temp ( Dbname   VARCHAR(10)              
     ,DBCountryName VARCHAR(25)             
     ,ISlp  VARCHAR(4)            
     ,OSlp  VARCHAR(4)               
     ,CusID   VARCHAR(10)              
     ,CusLongNm  VARCHAR(40)              
     ,Branch   VARCHAR(3)              
     ,ActvyDT  VARCHAR(10)              
     ,OrderNo  NUMERIC              
     ,OrderItm  NUMERIC              
     ,Product  VARCHAR(500)              
     ,Wgt   decimal(20,0)              
     ,TotalMtlVal decimal(20,0)              
     ,ReplCost  decimal(20,0)              
     ,Profit   decimal(20,2)              
     , TotalSlsVal decimal(20,0)             
     , NetPct decimal(20,0)             
     , NetAvgAmt decimal(20,0)             
     ,Market Varchar(35)             
     ,TotalReplCost Decimal(20,0)            
     ,NPReplCost decimal(20,0)            
     ,MtlAvgVal decimal(20,0)            
     ,LineItemCount int            
     );              
                 
IF @Branch = 'ALL'                
 BEGIN                
  set @Branch = ''                
                
 END              
            
              
IF @DBNAME = 'ALL'              
 BEGIN              
   IF @version = '0'              
  BEGIN              
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName              
    OPEN ScopeCursor;              
  END              
  ELSE              
  BEGIN              
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
  END              
                
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  WHILE @@FETCH_STATUS = 0              
  BEGIN              
   DECLARE @query NVARCHAR(MAX);              
   IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))           
    End
	Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))           
    End
                  
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR               
    BEGIN              
    SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal,NetPct,NetAvgAmt,Market,TotalReplCost,NPReplCost,MtlAvgVal,           
    LineItemCount)              
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm,             
      (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @Prefix + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size         
 
   
      and prm_fnsh = a.mbk_fnsh) as Product,                 
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),              
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val- a.mbk_mtl_avg_val)/a.mbk_tot_mtl_val*100 END,            
       a.mbk_tot_val* '+ @CurrenyRate +'  ,            
      (Case ISNULL(a.mbk_tot_val,0) When 0 then 0 else (ISNULL(a.mbk_tot_avg_val,0)/a.mbk_tot_val)*100 end ) as netpct,            
      a.mbk_tot_avg_val* '+ @CurrenyRate +',            
      cuc_desc30, a.mbk_tot_repl_val * '+ @CurrenyRate +', (select oit_npft_repl_val * '+ @CurrenyRate +' from ' + @Prefix + '_ortoit_rec where oit_cmpy_id = a.mbk_cmpy_id and oit_ref_pfx = a.mbk_ord_pfx            
       and oit_ref_no = a.mbk_ord_no and oit_ref_itm = a.mbk_ord_itm),a.mbk_mtl_avg_val* '+ @CurrenyRate +'            
       ,             
       (select COUNT(*) from ' + @Prefix + '_ortord_rec where  ord_ord_no = a.mbk_ord_no)            
      FROM ' + @Prefix + '_ortmbk_rec a              
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id             
       left join '+ @Prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                        
                        
      WHERE a.mbk_ord_pfx=''SO''               
          AND a.mbk_ord_itm<>999               
         -- AND a.mbk_wgt > 0               
         and a.mbk_trs_md = ''A''              
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)             
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )             
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '              
    END              
    ELSE              
    BEGIN              
     SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal,NetPct,NetAvgAmt,Market,TotalReplCost,NPReplCost,MtlAvgVal,          
      LineItemCount)              
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm,             
      (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @Prefix + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size         
 
       and prm_fnsh = a.mbk_fnsh) as Product,                
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),              
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val- a.mbk_mtl_avg_val)/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,            
      (Case ISNULL(a.mbk_tot_val,0) When 0 then 0 else (ISNULL(a.mbk_tot_avg_val,0)/a.mbk_tot_val)*100 end ) as netpct,            
      a.mbk_tot_avg_val* '+ @CurrenyRate +',            
      cuc_desc30 , a.mbk_tot_repl_val * '+ @CurrenyRate +', (select oit_npft_repl_val * '+ @CurrenyRate +' from ' + @Prefix + '_ortoit_rec where oit_cmpy_id = a.mbk_cmpy_id and oit_ref_pfx = a.mbk_ord_pfx            
       and oit_ref_no = a.mbk_ord_no and oit_ref_itm = a.mbk_ord_itm),a.mbk_mtl_avg_val* '+ @CurrenyRate +'            
       ,             
       (select COUNT(*) from ' + @Prefix + '_ortord_rec where  ord_ord_no = a.mbk_ord_no)            
      FROM ' + @Prefix + '_ortmbk_rec a              
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id             
      left join '+ @Prefix +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
      WHERE a.mbk_ord_pfx=''SO''               
         AND a.mbk_ord_itm<>999               
        --  AND a.mbk_wgt > 0              
        and a.mbk_trs_md = ''A''               
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)             
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )             
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '              
    END              
   print @query;              
   EXECUTE sp_executesql @query;              
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  END                 
 CLOSE ScopeCursor;              
  DEALLOCATE ScopeCursor;              
 END              
ELSE              
BEGIN              
IF @version = '0'              
  BEGIN              
  SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)              
  END              
  ELSE              
  BEGIN              
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)              
  END              
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)              
 IF (UPPER(@DBNAME) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DBNAME) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DBNAME) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DBNAME) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DBNAME) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@DBNAME) = 'TWCN')              
    begin              
       SET @DB ='TW'              
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End
	Else if(UPPER(@DBNAME) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End
                  
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR               
   BEGIN              
   SET @sqltxt ='INSERT INTO #temp (Dbname,DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal,NetPct, NetAvgAmt,Market,TotalReplCost,NPReplCost,MtlAvgVal, LineItemCount
	)              
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm,             
      (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DBNAME + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size         
 
       and prm_fnsh = a.mbk_fnsh) as Product,              
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),              
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val- a.mbk_mtl_avg_val)/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,            
      (Case ISNULL(a.mbk_tot_val,0) When 0 then 0 else (ISNULL(a.mbk_tot_avg_val,0)/a.mbk_tot_val)*100 end ) as netpct,            
      a.mbk_tot_avg_val* '+ @CurrenyRate +',            
       cuc_desc30 , a.mbk_tot_repl_val * '+ @CurrenyRate +', (select oit_npft_repl_val * '+ @CurrenyRate +' from ' + @DBNAME + '_ortoit_rec where oit_cmpy_id = a.mbk_cmpy_id and oit_ref_pfx = a.mbk_ord_pfx            
       and oit_ref_no = a.mbk_ord_no and oit_ref_itm = a.mbk_ord_itm),a.mbk_mtl_avg_val* '+ @CurrenyRate +'            
       ,            
       (select COUNT(*) from ' + @DBNAME + '_ortord_rec where  ord_ord_no = a.mbk_ord_no)            
      FROM ' + @DBNAME + '_ortmbk_rec a              
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id              
      left join '+ @DBNAME +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
      WHERE a.mbk_ord_pfx=''SO''                AND a.mbk_ord_itm<>999               
        -- AND a.mbk_wgt > 0               
        and a.mbk_trs_md = ''A''              
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)              
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )            
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '              
  END              
  ELSE              
  BEGIN              
  SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost, Profit, TotalSlsVal, NetPct,NetAvgAmt,Market,TotalReplCost,NPReplCost,MtlAvgVal, LineItemCount
	)              
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm,             
      (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DBNAME + '_inrprm_rec where prm_frm = a.mbk_frm and prm_grd = a.mbk_grd and prm_size = a.mbk_size and     
	prm_fnsh = a.mbk_fnsh) as Product,              
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),              
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val- a.mbk_mtl_avg_val)/a.mbk_tot_mtl_val*100 END,            
       a.mbk_tot_val* '+ @CurrenyRate +' ,            
       (Case ISNULL(a.mbk_tot_val,0) When 0 then 0 else (ISNULL(a.mbk_tot_avg_val,0)/a.mbk_tot_val)*100 end ) as netpct,            
       a.mbk_tot_avg_val* '+ @CurrenyRate +',            
       cuc_desc30 , a.mbk_tot_repl_val * '+ @CurrenyRate +', (select oit_npft_repl_val * '+ @CurrenyRate +' from ' + @DBNAME + '_ortoit_rec where oit_cmpy_id = a.mbk_cmpy_id and oit_ref_pfx = a.mbk_ord_pfx            
       and oit_ref_no = a.mbk_ord_no and oit_ref_itm = a.mbk_ord_itm),a.mbk_mtl_avg_val* '+ @CurrenyRate +'            
       ,            
(select COUNT(*) from ' + @DBNAME + '_ortord_rec where ord_ord_no = a.mbk_ord_no)            
            
      FROM ' + @DBNAME + '_ortmbk_rec a              
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id              
      left join '+ @DBNAME +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
      WHERE a.mbk_ord_pfx=''SO''               
        AND a.mbk_ord_itm<>999               
         -- AND a.mbk_wgt > 0             
         and a.mbk_trs_md = ''A''                
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)             
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )            
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') '              
  END              
 print(@sqltxt)              
 set @execSQLtxt = @sqltxt;               
 EXEC (@execSQLtxt);              
END              
 SELECT Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,MtlAvgVal, ReplCost, Profit, TotalSlsVal,            
  NetPct,NetAvgAmt,Market,TotalReplCost,NPReplCost, (Case When TotalReplCost >0 then  (NPReplCost/TotalReplCost)*100 else 0 end )   as RepProfiPct,             
  (select COUNT(*) from #temp t where t.OrderNo = ou.OrderNo)as  LineItemCount            
  FROM #temp ou order by Profit ;--  where OrderNo = '150993' ;              
 DROP TABLE  #temp ;              
END              
              
--EXEC [sp_itech_Booking_Daily] 'US'              
--EXEC [sp_itech_Booking_Daily] 'ALL'              
--select * from tbl_itech_DatabaseName            
            
            
/*            
20161220            
Mail sub: one these two adjusted reports            
Regarding Repl Profit % i have only chenge in Details report not in summary report.            
But some one directly moved summary report in production            
            
20161229            
Mail sub: Correction to the detailed booking report. 
20210127	Sumit
Add Germany Database
*/
GO
