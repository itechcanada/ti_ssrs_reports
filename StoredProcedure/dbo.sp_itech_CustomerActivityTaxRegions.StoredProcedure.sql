USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerActivityTaxRegions]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================      
-- Author:  <Mukesh>      
-- Create date: <7 Jul 2017>      
-- Description: <CustomerActivityTaxRegions>     
-- =============================================      
CREATE PROCEDURE [dbo].[sp_itech_CustomerActivityTaxRegions]  @DBNAME varchar(50) --, @FromDate Date, @ToDate Date    
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
declare @DB varchar(100);      
declare @sqltxt varchar(6000);      
declare @execSQLtxt varchar(7000);      
DECLARE @CountryName VARCHAR(25);         
DECLARE @prefix VARCHAR(15);         
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @CurrenyRate varchar(15);        
declare @FD varchar(10)  ;            
declare @TD varchar(10)   ;    
    
 --set @FD = CONVERT(VARCHAR(10), @fromDate,120)          
 --set @TD = CONVERT(VARCHAR(10), @toDate,120)     
      
CREATE TABLE #temp ( Dbname   VARCHAR(10)      
     ,CusID   VARCHAR(10)      
     ,CusLongNm  VARCHAR(40)      
     ,AdminBranch   VARCHAR(3)      
     ,ShipTo  VARCHAR(10)     
     ,ShipToAddress VARCHAR(200)     
     ,CustTaxType  VARCHAR(3)      
     ,CustTaxRgn  VARCHAR(10)  
     ,TaxExmptRgn  VARCHAR(3)      
     ,TaxExmptType  VARCHAR(3)      
     ,CustTaxID1  VARCHAR(20)     
     ,CustTaxID2  VARCHAR(20)   
  ,ZipCode   VARCHAR(15)     
     );      
     
IF @DBNAME = 'ALL'      
 BEGIN      
       
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS       
    OPEN ScopeCursor;      
      
        
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;      
  WHILE @@FETCH_STATUS = 0      
  BEGIN      
   DECLARE @query NVARCHAR(MAX);      
   IF (UPPER(@Prefix) = 'TW')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
    End      
    Else if (UPPER(@Prefix) = 'NO')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
    End      
    Else if (UPPER(@Prefix) = 'CA')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
    End      
    Else if (UPPER(@Prefix) = 'CN')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
    End      
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')      
    begin      
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
    End      
    Else if(UPPER(@Prefix) = 'UK')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
    End      
    Else if(UPPER(@Prefix) = 'DE')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
    End      
          
        
    SET @query = 'INSERT INTO #temp ( Dbname,CusID ,CusLongNm,AdminBranch,ShipTo,ShipToAddress,CustTaxType ,CustTaxRgn,TaxExmptRgn,TaxExmptType,CustTaxID1,CustTaxID2,ZipCode)      
        select distinct '''+ @Prefix +''', cus_cus_id, cus_cus_long_nm,cus_admin_brh,shp_shp_to,   
        LTRIM(RTRIM(ISNULL(cva_addr1,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_addr2,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_addr3,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_city,''''))) + '' '' +      
  ISNULL(cva_pcd,'''') + '' '' + LTRIM(RTRIM(ISNULL(cva_cty,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_st_prov,''''))) as shipToAdd ,stx_tx_typ, stx_tx_rgn,
  stx_exmpt_rsn,stx_tx_Sts,stx_tx_id1,stx_tx_id2 ,ISNULL(cva_pcd,'''')     
        from ' + @Prefix + '_arrcus_rec    
        left  join ' + @Prefix + '_arrshp_rec on shp_cmpy_id = cus_cmpy_id and shp_cus_id = cus_cus_id   
        left join ' + @Prefix + '_scrcva_rec on cva_cmpy_id = shp_cmpy_id and cva_ref_pfx = ''CS'' and cva_cus_ven_typ = ''c'' and   
 cva_cus_ven_id = shp_cus_id and cva_addr_no = shp_shp_to  and cva_addr_typ = ''S''   
  left join ' + @Prefix + '_arrstx_rec on stx_cmpy_id = shp_cmpy_id and stx_cus_id = shp_cus_id   and stx_shp_to = shp_shp_to   
       
  Where  cus_actv = 1    
   '      
      
   print @query;      
   EXECUTE sp_executesql @query;      
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;      
  END         
  CLOSE ScopeCursor;      
  DEALLOCATE ScopeCursor;      
 END      
ELSE      
BEGIN      
     
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)      
 IF (UPPER(@DBNAME) = 'TW')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
    End      
    Else if (UPPER(@DBNAME) = 'NO')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
    End      
    Else if (UPPER(@DBNAME) = 'CA')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
    End      
    Else if (UPPER(@DBNAME) = 'CN')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
    End      
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')      
    begin      
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
    End      
    Else if(UPPER(@DBNAME) = 'UK')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
    End      
    Else if(UPPER(@DBNAME) = 'DE')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
    End      
    Else if(UPPER(@DBNAME) = 'TWCN')      
    begin      
       SET @DB ='TW'      
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
    End      
          
       
   SET @sqltxt ='INSERT INTO #temp ( Dbname,CusID ,CusLongNm,AdminBranch,ShipTo,ShipToAddress,CustTaxType ,CustTaxRgn,TaxExmptRgn,TaxExmptType,CustTaxID1,CustTaxID2,ZipCode)      
        select distinct '''+ @DBNAME +''', cus_cus_id, cus_cus_long_nm,cus_admin_brh,shp_shp_to,     
        LTRIM(RTRIM(ISNULL(cva_addr1,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_addr2,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_addr3,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_city,''''))) + '' '' +      
 ISNULL(cva_pcd,'''') + '' '' + LTRIM(RTRIM(ISNULL(cva_cty,''''))) + '' '' + LTRIM(RTRIM(ISNULL(cva_st_prov,''''))) as shipToAdd ,stx_tx_typ, stx_tx_rgn,
 stx_exmpt_rsn,stx_tx_sts,stx_tx_id1,stx_tx_id2 ,ISNULL(cva_pcd,'''')     
        from ' + @DBNAME + '_arrcus_rec    
        left  join ' + @DBNAME + '_arrshp_rec on shp_cmpy_id = cus_cmpy_id and shp_cus_id = cus_cus_id   
        left join ' + @DBNAME + '_scrcva_rec on cva_cmpy_id = shp_cmpy_id and cva_ref_pfx = ''CS'' and cva_cus_ven_typ = ''c'' and   
 cva_cus_ven_id = shp_cus_id and cva_addr_no = shp_shp_to  and cva_addr_typ = ''S''  
  left join ' + @DBNAME + '_arrstx_rec on stx_cmpy_id = shp_cmpy_id and stx_cus_id = shp_cus_id   and stx_shp_to = shp_shp_to  
       
  Where  cus_actv = 1'    
      
 print(@sqltxt)      
 set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
END      
 SELECT Dbname,CusID ,CusLongNm,AdminBranch,ShipTo,ShipToAddress,CustTaxType ,CustTaxRgn,TaxExmptRgn,
 case When TaxExmptType IS null then '' else TaxExmptType end as TaxExmptType,
 -- TaxExmptType,
 CustTaxID1,CustTaxID2,ZipCode FROM #temp order by cusID,ShipTo ;      
 DROP TABLE  #temp;      
END      
      
--EXEC [sp_itech_CustomerActivityTaxRegions] 'ALL'  
--EXEC [sp_itech_CustomerActivityTaxRegions] 'US'      
-- and cva_addr_no = shp_shp_to --sat_inv_dt >= ''' + @FD + '''  and sat_inv_dt <=  ''' + @TD + ''' and
/*
Date:20180508
Mail Sub:Customer Activity Tax Regions.xls

*/
GO
