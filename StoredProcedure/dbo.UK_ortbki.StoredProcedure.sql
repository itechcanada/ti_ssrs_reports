USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ortbki]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mukesh>  
-- Create date: <Create Date,Oct 15, 2015,>  
-- Description: <Description,Due Date,>  
  
-- =============================================  
Create PROCEDURE [dbo].[UK_ortbki]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_ortbki_rec', 'U') IS NOT NULL  
  drop table dbo.UK_ortbki_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_ortbki_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[ortbki_rec];   
    
END  
-- select * from UK_ortbki_rec
GO
