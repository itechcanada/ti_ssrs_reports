USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_injitv]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,4/25/2013,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[TW_injitv]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.TW_injitv_rec ;	
	
Insert into dbo.TW_injitv_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVETWSTX].[livetwstxdb].[informix].[injitv_rec]


END

GO
