USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_EatonSummary]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                    
-- Author:  <Mukesh >                    
-- Create date: <16 APR 2019>                    
-- Description: <Getting top 50 customers for SSRS reports>              
-- =============================================                    
CREATE PROCEDURE [dbo].[sp_itech_EatonSummary]                 
AS                    
BEGIN             
          
                     
 SET NOCOUNT ON;                    
declare @sqltxt varchar(max)                    
declare @execSQLtxt varchar(max)                    
declare @DB varchar(100)                  
declare @FD12M varchar(10)                    
declare @TD12M varchar(10) 
declare @FD3M varchar(10)                    
declare @TD3M varchar(10)                    
declare @FD6M varchar(10)                    
declare @TD6M varchar(10)                    
declare @NOOfCust varchar(15)                    
DECLARE @CurrenyRate varchar(15)                    
DECLARE @IsExcInterco char(1)         
                    
SET @DB='US';     

set @FD12M = CONVERT(varchar(10),DATEADD(MONTH, -12, GETDATE()),120); -- From date of last 12 month    
set @TD12M = CONVERT(varchar(10),GETDATE(),120);    
    
set @FD3M = CONVERT(varchar(10),DATEADD(MONTH, -3, GETDATE()),120); -- From date of last three month    
set @TD3M = CONVERT(varchar(10),GETDATE(),120);    
    
set @FD6M = CONVERT(varchar(10),DATEADD(MONTH, -6, GETDATE()),120); -- From date of six month    
set @TD6M = CONVERT(varchar(10),GETDATE(),120);           
     
CREATE TABLE #tmp6M (  CustID varchar(15)                     
        ,CustName Varchar(65)                    
        ,InvDt varchar(15)                    
        ,Product   varchar(300)                    
        ,NoOfPcs Varchar(10)                    
        , Inches    DECIMAL(20, 2)                    
        , Weight  DECIMAL(20, 2)                    
        , Part   VARCHAR(50)                     
        ,InvNo Varchar(50)                    
        , Date     varchar(65)                    
        ,Form  varchar(35)                    
        ,Grade  varchar(35)                    
        ,Size  varchar(35)                    
        ,Finish  varchar(35)                    
        ,Measure  DECIMAL(20, 2)                    
        ,TotalValue DECIMAL(20, 2)          
  ,MatGP DECIMAL(20, 2)                   
        ,GPPct DECIMAL(20, 2)                    
  ,NPAmt DECIMAL(20, 2)        
        ,NPPct DECIMAL(20, 2)                   
        ,PONumber Varchar(75)                
        ,TransportNo Varchar(30)     ,              
        MktCatg varchar(30),              
        ShpWhs varchar(3),          
        Branch varchar(10)           
        ,SalesPersonIs Varchar(10)          
        ,SalesPersonOs Varchar(10)          
        ,sizeDesc Varchar(50)         
        ,millName Varchar(40)             
        ,MillSurCost Decimal(20,2)     
        ,MillDlyDate Varchar(10)    
      );                                  
      
                   
insert into #tmp6M    
exec sp_itech_CustomerByProduct_NP @FD12M, @TD12M, 'US','0','5','8960,ROMAA,293,612,1684,2603,2826,3770,5307,5487,7627,12651,ADTUA,AEMAA,CLMEA,CLTUA,DISCA,EAJAA,EDMAA,INAEA,KAMAA,LC11900,LCO8000,MACOA,PEMAA,SCMAA,TUMAA,855, 10120, 11930, 12088, 12217,   
12645, 6302, 9309, 12681, 13211, 13219, 141, 2373, 654, 9890, 3451, 5567, 5724, 6010, 614, 7566, 522,1089,1242,1749,1806,2372,2382,2631,3532,4242,4457,12535,4894 ,9886,367,12731,3041,11588,6854,8897,5701,2143,2897,795,2862,5706,VACOA,286,170,166,26,  
470,VACOB,56,171,10489,ALMAA,tbd,4787,10337'    
    
    
select ouq.Form, ouq.Grade, ouq.sizeDesc, ouq.Finish, ouq.size, sum(ouq.Weight) as Month12Wgt ,    
(select ISNULL(SUM(inq.Weight),0) from #tmp6M as inq where inq.Form = ouq.Form and inq.Grade = ouq.Grade and inq.sizeDesc = ouq.sizeDesc and inq.Finish = ouq.finish    
and inq.Size = ouq.Size and  inq.InvDt >= @FD6M and inq.InvDt <= @TD6M ) as Month6Wgt, 
(select ISNULL(SUM(inq.Weight),0) from #tmp6M as inq where inq.Form = ouq.Form and inq.Grade = ouq.Grade and inq.sizeDesc = ouq.sizeDesc and inq.Finish = ouq.finish    
and inq.Size = ouq.Size and  inq.InvDt >= @FD3M and inq.InvDt <= @TD3M ) as Month3Wgt,    
(select ISNULL(DETQTY,0) from tbl_itech_ReplacementPricing where [Database] = 'US' and Form = ouq.Form and Grade = ouq.Grade and Size = ouq.size and Finish = ouq.Finish) as DETQTY ,    
(select ISNULL(AvailableWeight,0) from tbl_itech_ReplacementPricing where [Database] = 'US' and Form = ouq.Form and Grade = ouq.Grade and Size = ouq.size and Finish = ouq.Finish) as AvailableWgt    ,
(select Max(ipd_ref_no) from US_tctipd_rec where ipd_cmpy_id = 'USS' and ipd_ref_pfx = 'PO' and ipd_frm = ouq.Form and ipd_grd = ouq.Grade and 
ipd_size = ouq.size and ipd_fnsh = ouq.finish) as PONumber,
(select Ltrim(Rtrim(poh_ven_id)) from US_potpoh_rec where poh_po_no = (select Max(ipd_ref_no) from US_tctipd_rec where ipd_cmpy_id = 'USS' and ipd_ref_pfx = 'PO' and ipd_frm = ouq.Form and ipd_grd = ouq.Grade and 
ipd_size = ouq.size and ipd_fnsh = ouq.finish)) as mill ,
(select max(pod_arr_to_dt) from US_potpod_rec where pod_po_no = (select Max(ipd_ref_no) from US_tctipd_rec where ipd_cmpy_id = 'USS' and ipd_ref_pfx = 'PO' and ipd_frm = ouq.Form and ipd_grd = ouq.Grade and 
ipd_size = ouq.size and ipd_fnsh = ouq.finish)) as nextDate,
(select ISNULL(OpenPOWgt,0) from tbl_itech_ReplacementPricing where [Database] = 'US' and Form = ouq.Form and Grade = ouq.Grade and Size = ouq.size and Finish = ouq.Finish) as IncomingWgt


--,Max(ouq.PONumber) as PONumber  
-- ,(select iq.millName from #tmp6M iq where iq.PONumber = Max(ouq.PONumber)) as millName  
from  #tmp6M ouq    
group by ouq.Form, ouq.Grade, ouq.sizeDesc, ouq.Finish, ouq.size;                 
                   

       
Drop table #tmp6M;    
    
END                    

 -- exec sp_itech_EatonSummary    
           
 -- exec sp_itech_CustomerByProduct '01/18/2016', '03/18/2016' ,'US','0','5','','0','I'                    
--11930,12681,141,614,654,855,5567,6010,9309,12217,12535,12645,13219                    
--'11930','12681','141','614','654','855','5567','6010','9309','12217','12535','12645','13219'                
--select * US_ortcht_rec              
              

    
GO
