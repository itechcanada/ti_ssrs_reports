USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ExcessInventoryProductBranch]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <22 Apr 2016>    
-- Description: <Excess Inventory By Product By Branch>    
   
-- =============================================    
CREATE  PROCEDURE [dbo].[sp_itech_ExcessInventoryProductBranch]  @DBNAME varchar(50), @Branch Varchar(3),@FromDate datetime, @ToDate datetime, @IncludeLTA char(1) = '1'  
AS    
BEGIN    
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
declare @FD varchar(10)      
declare @TD varchar(10)  
declare @FD1 varchar(10)      
declare @TD1 varchar(10)  
declare @FD2 varchar(10)      
declare @TD2 varchar(10)  
declare @FD3 varchar(10)      
declare @TD3 varchar(10)  
declare @FD4 varchar(10)      
declare @TD4 varchar(10)  
declare @FD5 varchar(10)      
declare @TD5 varchar(10)  
DECLARE @company VARCHAR(15);     
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);     
  
set @FD = CONVERT(VARCHAR(10),@FromDate,120)   
set @TD = CONVERT(VARCHAR(10),@ToDate,120)    
  
  
set @FD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)    -- current month from date  
set @TD1 = @FD  --current month To date  
  
set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date  
set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date  
  
  
print @FD;  
print @TD;   
set @DB= @DBNAME  
  
IF @Branch = 'ALL'  
BEGIN  
SET @Branch = ''  
END  
        
  
        
--  IF @DBNAME = 'ALL'  
           
CREATE TABLE #temp (  
Dbname   VARCHAR(10)   
,form VARCHAR(10)  
,grade VARCHAR(10)    
,size VARCHAR(15)    
,finish VARCHAR(10)  
  ,   Branch   VARCHAR(3)   
     , Blgweight decimal(20,0)    
     );    
  
IF @DBNAME = 'ALL'  
 BEGIN  
 DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,company,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;   
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @Prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
     if(@IncludeLTA = '0')  
    Begin  
   if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                 
        Begin   
         SET @query ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)  
         SELECT '''+ @Prefix +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @Prefix + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt* 2.20462,0)),0) FROM ' + @Prefix + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.SAT_FRM and oquery.prd_grd = currentPrdData.SAT_GRD   
and oquery.prd_size = currentPrdData.sat_size and oquery.prd_fnsh = currentPrdData.sat_fnsh and oquery.prd_brh = currentPrdData.sat_shpt_brh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @Prefix + '_intprd_rec_history inq  
  join ' + @Prefix + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.sat_shpt_brh  
 where inq.prd_invt_sts = ''S''   
  and inq.prd_brh = currentPrdData.sat_shpt_brh  
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg  
   
   
FROM ' + @Prefix + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
    And currentPrdData.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery    
'  
   
        End  
        Else  
        Begin  
          
   SET @query ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)   
   SELECT '''+ @Prefix +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT,0)),0)  from ' + @Prefix + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @Prefix + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.SAT_FRM and oquery.prd_grd = currentPrdData.SAT_GRD   
and oquery.prd_size = currentPrdData.sat_size and oquery.prd_fnsh = currentPrdData.sat_fnsh and oquery.prd_brh = currentPrdData.sat_shpt_brh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @Prefix + '_intprd_rec_history inq  
  join ' + @Prefix + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.sat_shpt_brh  
 where inq.prd_invt_sts = ''S''   
  and inq.prd_brh = currentPrdData.sat_shpt_brh  
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg  
   
   
FROM ' + @Prefix + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
    And currentPrdData.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery    
     
 '  
End                 
 End  
 Else  
 begin  
  if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                 
        Begin   
         SET @query ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)  
         SELECT '''+ @Prefix +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @Prefix + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @Prefix + '_intprd_rec_history where prd_frm = currentPrdData.SAT_FRM and prd_grd = currentPrdData.SAT_GRD   
and prd_size = currentPrdData.sat_size and prd_fnsh = currentPrdData.sat_fnsh and prd_brh = currentPrdData.sat_shpt_brh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg  
   
   
FROM ' + @Prefix + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery    
'  
   
        End  
        Else  
        Begin  
          
   SET @query ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)   
   SELECT '''+ @Prefix +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT,0)),0)  from ' + @Prefix + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt,0)),0) FROM ' + @Prefix + '_intprd_rec_history where prd_frm = currentPrdData.SAT_FRM and prd_grd = currentPrdData.SAT_GRD   
and prd_size = currentPrdData.sat_size and prd_fnsh = currentPrdData.sat_fnsh and prd_brh = currentPrdData.sat_shpt_brh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg  
   
   
FROM ' + @Prefix + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery    
     
 '  
End    
End  
     
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
    if(@IncludeLTA = '0')  
    Begin  
        
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR     
   BEGIN    
   SET @sqltxt ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)   
   SELECT '''+ @DBNAME +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DBNAME + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DBNAME + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.SAT_FRM and oquery.prd_grd = currentPrdData.SAT_GRD   
and oquery.prd_size = currentPrdData.sat_size and oquery.prd_fnsh = currentPrdData.sat_fnsh and oquery.prd_brh = currentPrdData.sat_shpt_brh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DBNAME + '_intprd_rec_history inq  
  join ' + @DBNAME + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.sat_shpt_brh  
 where inq.prd_invt_sts = ''S''   
  and inq.prd_brh = currentPrdData.sat_shpt_brh  
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg  
   
   
FROM ' + @DBNAME + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
    And currentPrdData.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery   
 '  
  END    
  ELSE    
  BEGIN    
  SET @sqltxt ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)    
  SELECT '''+ @DBNAME +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT,0)),0)  from ' + @DBNAME + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @DBNAME + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.SAT_FRM and oquery.prd_grd = currentPrdData.SAT_GRD   
and oquery.prd_size = currentPrdData.sat_size and oquery.prd_fnsh = currentPrdData.sat_fnsh and oquery.prd_brh = currentPrdData.sat_shpt_brh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DBNAME + '_intprd_rec_history inq  
  join ' + @DBNAME + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.sat_shpt_brh  
 where inq.prd_invt_sts = ''S''   
  and inq.prd_brh = currentPrdData.sat_shpt_brh  
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg  
   
   
FROM ' + @DBNAME + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
    And currentPrdData.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.sat_shpt_brh)  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery   
'  
End    
END   
Else   
Begin  
if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR     
   BEGIN    
   SET @sqltxt ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)   
   SELECT '''+ @DBNAME +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DBNAME + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DBNAME + '_intprd_rec_history where prd_frm = currentPrdData.SAT_FRM and prd_grd = currentPrdData.SAT_GRD   
and prd_size = currentPrdData.sat_size and prd_fnsh = currentPrdData.sat_fnsh and prd_brh = currentPrdData.sat_shpt_brh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg  
   
   
FROM ' + @DBNAME + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery   
 '  
  END    
  ELSE    
  BEGIN    
  SET @sqltxt ='INSERT INTO #temp (Dbname,form,grade,size,finish,Branch, Blgweight)    
  SELECT '''+ @DBNAME +''', SAT_FRM,SAT_GRD,sat_size, sat_fnsh, sat_shpt_brh ,(case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) as excess  
  FROM   
  (   
SELECT currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh, currentPrdData.sat_shpt_brh ,ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange,  
  
(select ISNULL(SUM(ISNULL(SAT_BLG_WGT,0)),0)  from ' + @DBNAME + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.sat_frm and sixMonthWgt.sat_grd = currentPrdData.sat_grd and sixMonthWgt.sat_size = currentPrdData.sat_size  
and sixMonthWgt.sat_fnsh = currentPrdData.sat_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.sat_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt,0)),0) FROM ' + @DBNAME + '_intprd_rec_history where prd_frm = currentPrdData.SAT_FRM and prd_grd = currentPrdData.SAT_GRD   
and prd_size = currentPrdData.sat_size and prd_fnsh = currentPrdData.sat_fnsh and prd_brh = currentPrdData.sat_shpt_brh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg  
   
   
FROM ' + @DBNAME + '_sahsat_rec currentPrdData  
WHERE currentPrdData.SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  
    AND (currentPrdData.sat_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
group by currentPrdData.sat_shpt_brh,currentPrdData.SAT_FRM,currentPrdData.SAT_GRD,currentPrdData.sat_size, currentPrdData.sat_fnsh  
     
) as oquery   
'  
End  
  
End  
print(@sqltxt)    
 set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
 End  
   
 SELECT  CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'OTHER' END as MatGroup,*   
 FROM #temp where (Dbname!='US' or BRANCH!='TAI') and Blgweight > 0 and Dbname not in ('NO');    
 DROP TABLE  #temp;    
END    
    
--EXEC [sp_itech_ExcessInventoryProductBranch] 'US' , 'ALL','2016-01-01','2016-04-01','0'   
GO
