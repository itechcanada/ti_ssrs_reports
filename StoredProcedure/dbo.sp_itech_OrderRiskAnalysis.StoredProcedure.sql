USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OrderRiskAnalysis]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <19 Dec 2017>  
-- Description: <Getting Order Risk Analysis Details>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_OrderRiskAnalysis] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50)
  
AS  
BEGIN  
SET NOCOUNT ON;  


  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10) 
  
CREATE TABLE #tmp (    
					CompanyName   VARCHAR(3) 
					,Branch          VARCHAR(3) 
					,OrderPfx          VARCHAR(3) 
					,OrderNo          VARCHAR(35)  
					,OrderRisk          VARCHAR(35)  
					,OrderCreatedDate Varchar(10)
                 );  
              
DECLARE @company VARCHAR(35);  
DECLARE @prefix VARCHAR(15);   
DECLARE @DatabaseName VARCHAR(35);    
 
 set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) 
        
 IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
               
      --Regular  
      SET @query =  
          'INSERT INTO #tmp (CompanyName, Branch,OrderPfx,OrderNo,OrderRisk,OrderCreatedDate)  
        select ''' + @prefix + ''',orh_ord_brh, orh_ord_pfx,orh_ord_no,rTRIM(LTRIM(ava_attr_val_var)) as risk,CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
		from ' + @prefix + '_ortorh_rec 
		left join ' + @prefix + '_ortxre_rec on xre_cmpy_id = orh_cmpy_id AND xre_ord_pfx = orh_ord_pfx AND xre_ord_no = orh_ord_no
		left join ' + @prefix + '_xctava_rec on ava_key_fld01_var = orh_cmpy_id and  ava_key_fld02_var = orh_ord_pfx
		and ava_key_fld03_var = orh_ord_no and ava_attr = ''ORD-RS''
       where  CONVERT(VARCHAR(10), xre_crtd_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), xre_crtd_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120) '  
          
          
        EXECUTE sp_executesql @query;  
        
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     
     BEGIN   
     Set @prefix= @DBNAME   
     
      SET @sqltxt = 'INSERT INTO #tmp (CompanyName, Branch,OrderPfx,OrderNo,OrderRisk,OrderCreatedDate)  
        select ''' + @prefix + ''',orh_ord_brh, orh_ord_pfx,orh_ord_no,rTRIM(LTRIM(ava_attr_val_var)) as risk,CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
		from ' + @prefix + '_ortorh_rec 
		left join ' + @prefix + '_ortxre_rec on xre_cmpy_id = orh_cmpy_id AND xre_ord_pfx = orh_ord_pfx AND xre_ord_no = orh_ord_no
		left join ' + @prefix + '_xctava_rec on ava_key_fld01_var = orh_cmpy_id and  ava_key_fld02_var = orh_ord_pfx
		and ava_key_fld03_var = orh_ord_no and ava_attr = ''ORD-RS''
       where  CONVERT(VARCHAR(10), xre_crtd_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), xre_crtd_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120) '  
           
    print(@sqltxt)  
     
  set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
 
     
     END                 
 
  select * from #tmp  
drop table #tmp  
  
  
END  
  
-- exec sp_itech_OrderRiskAnalysis '11/01/2017', '11/31/2017' , 'US'
GO
