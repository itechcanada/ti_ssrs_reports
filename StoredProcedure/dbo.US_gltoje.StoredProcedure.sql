USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_gltoje]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,08/03/2016,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[US_gltoje]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_gltoje_rec', 'U') IS NOT NULL
		drop table dbo.US_gltoje_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_gltoje_rec
  from [LIVEUSGL].[liveusgldb].[informix].[gltoje_rec] ;
  
END

-- Select * from US_gltoje_rec
GO
