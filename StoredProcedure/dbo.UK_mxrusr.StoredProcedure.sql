USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_mxrusr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,10/5/2012,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[UK_mxrusr]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.UK_mxrusr_rec ;	
	
Insert into dbo.UK_mxrusr_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVEUKSTX].[liveukstxdb].[informix].[mxrusr_rec]


END















GO
