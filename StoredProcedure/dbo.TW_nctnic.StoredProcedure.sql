USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_nctnic]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mrinal>  
-- Create date: <Create Date,Aug 6, 2015,>  
-- Description: <Description,gl account,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[TW_nctnic]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_nctnic_rec', 'U') IS NOT NULL  
  drop table dbo.TW_nctnic_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_nctnic_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[nctnic_rec];   
    
END  
-- select * from TW_nctnic_rec
GO
