USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[AR_AGED_TRIAL_BALANCE]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Wendy Wang>  
-- Create date: <Create Date,3/2/2012,>  
-- Description: <Description,Stratix report but doesn't have total by customer. It's for Canada office use.>  
-- =============================================  
create PROCEDURE [dbo].[AR_AGED_TRIAL_BALANCE]  
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
SELECT b.rvh_sld_cus_id,a.cus_cus_long_nm, b.rvh_inv_dt, b.rvh_due_dt,b.rvh_ar_pfx, b.rvh_upd_ref,b.rvh_ar_brh,b.rvh_orig_amt,b.rvh_balamt,  
CASE WHEN b.rvh_balamt<>0 THEN b.rvh_balamt ELSE b.rvh_orig_amt END balance, b.rvh_cry, b.rvh_arch_dt  
FROM [LIVECA_IW].[livecastxdb_iw].[informix].[arrcus_rec] a  
INNER JOIN [LIVECASTX].[livecastxdb].[informix].[artrvh_rec] b ON a.cus_cus_id=b.rvh_sld_cus_id  
WHERE  
b.rvh_cry='USD'  
AND b.rvh_balamt<>0  
AND b.rvh_arch_dt > GETDATE()  
END  
GO
