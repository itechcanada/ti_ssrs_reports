USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OTPDetails_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <09 Oct 2017>  
-- Description: <Getting OTP Details>  
--Use: This report use in Technical Report Name(OTP_Transfer_and_Regular_V1.rdl)
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_OTPDetails_V1] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID varchar(Max),@CustmomerNoTxt varchar(Max),@DateRange int, @version char = '0', @InBranch Varchar(3) = 'ALL'  
  
AS  
BEGIN  
SET NOCOUNT ON;  

  
  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10) 
DECLARE @CurrenyRate varchar(15)    
  
  
if(@InBranch = 'ALL')
begin
set @InBranch = ''
end

create table #tmpSalesNP(
CompanyID Varchar(3),
OrderPfx varchar(2),
OrderNo Varchar(17),
OrderItem Varchar(12)
, SalesNP   DECIMAL(20, 0) 
)

  
CREATE TABLE #tmpTransfer (    CompanyName   VARCHAR(10)  
        , ShpgWhs     VARCHAR(65)  
        ,SPRCPFX          VARCHAR(35)  
        ,SprcNo          VARCHAR(35)  
        ,Orders          VARCHAR(35)  
        ,OrderNo          VARCHAR(35)  
        ,OrderItem          VARCHAR(35)  
        ,RlsNo          VARCHAR(35)  
        ,Branch          VARCHAR(35)  
        ,DueFromDt          VARCHAR(15)  
        ,Transport          VARCHAR(35)  
        ,TranspNO          VARCHAR(35)  
        ,shpgDates          VARCHAR(15)  
        ,LateShptWithReason          VARCHAR(2)  
        , Pcs    DECIMAL(20, 2)  
        , Wgt    DECIMAL(20, 2)  
        ,Late    integer  
        ,RsnType          VARCHAR(35)  
        ,RsnCode          VARCHAR(35)  
        ,Remarks          Text  
        ,OTPType    VARCHAR(35)   
        ,CustID      varchar(15)   
        ,CustName      Varchar(100)   
        ,Heat      Varchar(20)    
        ,OriginalDueDt  VARCHAR(15) 
        ,OrderEntryDt  VARCHAR(10) 
        ,Product	Varchar(100)
        ,RmkUser	Varchar(10)
         ,SalesPresonIs varchar(10)      
        ,SalesPresonOs varchar(10) 
        , SalesNP   DECIMAL(20, 0) 
                 );  
                 
CREATE TABLE #tmpRegular (    CompanyName   VARCHAR(10)  
        , ShpgWhs     VARCHAR(65)  
        ,SPRCPFX          VARCHAR(35)  
        ,SprcNo          VARCHAR(35)  
        ,Orders          VARCHAR(35)  
        ,OrderNo          VARCHAR(35)  
        ,OrderItem          VARCHAR(35)  
        ,RlsNo          VARCHAR(35)  
        ,Branch          VARCHAR(35)  
        ,DueFromDt          VARCHAR(15)  
        ,Transport          VARCHAR(35)  
        ,TranspNO          VARCHAR(35)  
        ,shpgDates          VARCHAR(15)  
        ,LateShptWithReason          VARCHAR(2)  
        , Pcs    DECIMAL(20, 2)  
        , Wgt    DECIMAL(20, 2)  
        ,Late    integer  
        ,RsnType          VARCHAR(35)  
        ,RsnCode          VARCHAR(35)  
        ,Remarks          Text  
        ,OTPType    VARCHAR(35)   
        ,CustID      varchar(15)   
        ,CustName      Varchar(100)   
        ,Heat      Varchar(20)    
        ,OriginalDueDt  VARCHAR(15) 
        ,OrderEntryDt  VARCHAR(10) 
        ,Product	Varchar(100)
        ,RmkUser	Varchar(10)
         ,SalesPresonIs varchar(10)      
        ,SalesPresonOs varchar(10) 
        , SalesNP   DECIMAL(20, 0) 
                 );                  

CREATE TABLE #tmp (    CompanyName   VARCHAR(10)  
        , ShpgWhs     VARCHAR(65)  
        ,SPRCPFX          VARCHAR(35)  
        ,SprcNo          VARCHAR(35)  
        ,Orders          VARCHAR(35)  
        ,OrderNo          VARCHAR(35)  
        ,OrderItem          VARCHAR(35)  
        ,RlsNo          VARCHAR(35)  
        ,Branch          VARCHAR(35)  
        ,DueFromDt          VARCHAR(15)  
        ,Transport          VARCHAR(35)  
        ,TranspNO          VARCHAR(35)  
        ,shpgDates          VARCHAR(15)  
        ,LateShptWithReason          VARCHAR(2)  
        , Pcs    DECIMAL(20, 2)  
        , Wgt    DECIMAL(20, 2)  
        ,Late    integer  
        ,RsnType          VARCHAR(35)  
        ,RsnCode          VARCHAR(35)  
        ,Remarks          Text  
        ,OTPType    VARCHAR(35)   
        ,CustID      varchar(15)   
        ,CustName      Varchar(100)   
        ,Heat      Varchar(20)    
        ,OriginalDueDt  VARCHAR(15) 
        ,OrderEntryDt  VARCHAR(10) 
        ,Product	Varchar(100)
        ,RmkUser	Varchar(10)
         ,SalesPresonIs varchar(10)      
        ,SalesPresonOs varchar(10) 
        , SalesNP   DECIMAL(20, 0) 
                 );  
              
DECLARE @company VARCHAR(35);  
DECLARE @prefix VARCHAR(15);   
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @CusID varchar(max);  
Declare @Value as varchar(500);  
DECLARE @CustIDLength int;  
  
if @CustmomerNoTxt <> ''  
 BEGIN  
  set @CustomerID = @CustmomerNoTxt  
 END  
  
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));  
  
  
if @CustomerID = ''  
 BEGIN  
  set @CustomerID = '0'  
 END  
  
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month  
 End  
else if @DateRange=2 -- Last 7 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=3 -- Last 14 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=4 --Last 12 months excluding all the days in the current month  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month  
 End  
else  
 Begin  
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
 End  
  
        --->> Input customer data  
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)  
  SELECT @pos=0  
  SELECT @input =@CustomerID   
  SELECT @input = @input + ','  
  CREATE TABLE #tempTable (temp varchar(max) )  
  WHILE CHARINDEX(',',@input) > 0  
  BEGIN  
   SELECT @pos=CHARINDEX(',',@input)  
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))  
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))  
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)  
  END  
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable  
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)  
  DROP TABLE #tempTable  
        
 IF @DBNAME = 'ALL'  
 BEGIN  
 IF @version = '0'  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
  END  
  ELSE  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
  END  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
             SET @DB= @Prefix                     
      IF (UPPER(@DB) = 'TW')                  
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                              
   End                  
   Else if (UPPER(@DB) = 'NO')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                              
   End                    
   Else if (UPPER(@DB) = 'CA')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
   End                              
   Else if (UPPER(@DB) = 'CN')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                              
 End                              
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                              
   begin                        
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                              
   End                              
   Else if(UPPER(@DB) = 'UK')                              
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                              
   End   
   Else if(UPPER(@DB) = 'DE')                              
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                              
   End   
      --Regular  
      SET @query =  
          'INSERT INTO #tmpRegular (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
          Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat,OriginalDueDt,OrderEntryDt,Product ,RmkUser,SalesPresonIs,SalesPresonOs,SalesNP)  
         select  dpf_cmpy_id as ''CompanyName'','
      if (UPPER(@prefix)= 'US')
      BEGIN
      SET @query = @query + ' Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '
      END
      ELSE
      BEGIN
      SET @query = @query + 'ISNULL((case when orh_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po = us.stn_end_usr_po and 
 cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(dpf_ord_rls_no as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '
      END 
      
      SET @query = @query + ' dpf_sprc_pfx as ''SPRCPFX'',(dpf_sprc_no) as ''SprcNo'',   
       dpf_ord_pfx as ''Orders'', dpf_ord_no as ''OrderNo'',(dpf_ord_itm) as ''OrderItem'',  
       dpf_ord_rls_no as ''RlsNo'', dpf_ord_brh as ''Branch'',CONVERT(VARCHAR(10), (dpf_rls_due_fm_dt), 120)  as ''DueFromDt'',  
       dpf_transp_pfx as ''Transport'',dpf_transp_no as ''TranspNO'',CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)  as ''shpgDates'' ,  
       (dpf_shp_pcs) as ''Pcs'', '
         
        if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')          
     BEGIN          
     SET @query = @query + ' (dpf_shp_wgt) * 2.20462 as ''Wgt'', '          
     END          
       ELSE          
     BEGIN    
             
     SET @query = @query + ' (dpf_shp_wgt) as ''Wgt'', '          
     END  
        
       
      SET @query = @query + ' case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'', '''' as LateShptWithReason,  
       (dpf_rsn_typ) as ''RsnType'' ,(dpf_rsn) as ''RsnCode'',(dpf_rsn_rmk) as ''Remarks'' ,''Regular'' as OTPType  
       ,ord_sld_cus_id as CustID, cus_cus_long_nm as CustName,0, (orl_rqst_fm_dt), CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
       ,RTRIM(LTRIM((prm_frm))) + ''/'' + RTRIM(LTRIM((prm_grd))) + ''/'' + RTRIM(LTRIM((prm_size_desc))) + ''/'' + RTRIM(LTRIM((prm_fnsh))) as product
     ,(dpf_lgn_id),isslp.slp_lgn_id, osslp.slp_lgn_id,
     (select SUM(sat_tot_val)* '+ @CurrenyRate +'  from '+ @prefix +'_sahsat_rec where sat_cmpy_id = dpf_cmpy_id and sat_ord_pfx = dpf_ord_pfx and sat_ord_no = dpf_ord_no 
     and sat_ord_itm = dpf_ord_itm and sat_ord_rls_no = dpf_ord_rls_no)
       from '+ @prefix +'_pfhdpf_rec  
       join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no
       JOIN '+ @prefix +'_ortorl_rec   
       ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no  
       Join '+ @prefix +'_ortord_rec  
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
       join '+ @prefix +'_arrcus_rec  
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id  
       left join ' + @prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = orh_cmpy_id and isslp.slp_slp = orh_is_slp         
       left join ' + @prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = orh_cmpy_id and osslp.slp_slp = orh_os_slp  
      LEFT JOIN ' + @prefix + '_ortxre_rec on xre_cmpy_id = orl_cmpy_id AND xre_ord_pfx = orl_ord_pfx AND xre_ord_no = orl_ord_no
      left join ' + @prefix + '_inrprm_rec on prm_frm = dpf_frm and prm_grd = dpf_grd and prm_size = dpf_size and prm_fnsh = dpf_fnsh
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)  
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '  
         
         
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'   
        END  
       Else  
        BEGIN  
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'  
        END  
      END  
      Set @query += ' '  
          
          print @query;
        EXECUTE sp_executesql @query;  

		 -- Sales NP for Transfer
   SET @query = ' insert into #tmpSalesNP (CompanyID,OrderPfx,OrderNo,OrderItem,SalesNP)  
   select sat_cmpy_id,sat_ord_pfx,sat_ord_no,sat_ord_itm,SUM(sat_tot_val)* '+ @CurrenyRate +' as total
from US_sahsat_rec 
group by sat_cmpy_id,sat_ord_pfx,sat_ord_no,sat_ord_itm;
   '
   
   print(@query)  
     
  EXECUTE sp_executesql @query;  

        -- Transfer  
          SET @query ='INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat,OriginalDueDt,OrderEntryDt,Product ,RmkUser,SalesPresonIs,SalesPresonOs,SalesNP)  
                   SELECT tph_CMPY_ID as ''CompanyName'',Replace(b.tud_trpln_whs,''SFS'',''LAX'') as ''ShpgWhs'',b.tud_sprc_pfx as ''SPRCPFX'',(b.tud_sprc_no) as ''SprcNo'','''' as Orders,   
                   '''' as OrderNo,'''' as OrderItem,'''' as RlsNo,Replace(b.tud_trpln_whs,''SFS'',''LAX'') as Branch,  
                     CONVERT(VARCHAR(10), (c.orl_due_to_dt), 120) as ''DueFromDt'',    
                    a.tph_transp_pfx as ''Transfer'', a.tph_transp_no as ''Transfer No'', CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) as ''shpgDates'' ,    
        (b.tud_comp_pcs) as ''Pcs'', '
        if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')          
     BEGIN          
     SET @query = @query + ' (b.tud_comp_wgt) * 2.20462 as ''Wgt'', '          
     END          
       ELSE          
     BEGIN    
             
     SET @query = @query + ' (b.tud_comp_wgt) as ''Wgt'', '          
     END  
        
         
      SET @query = @query + ' case when (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'','''' as LateShptWithReason,'''' as RsnType,'''' as RsnCode,'''' as Remarks,  
      ''Transfer'' as OTPType  
      ,(ord_sld_cus_id) as CustID, (cus_cus_long_nm) as CustName,0,(c.orl_rqst_fm_dt), CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
       ,RTRIM(LTRIM((prm_frm))) + ''/'' + RTRIM(LTRIM((prm_grd))) + ''/'' + RTRIM(LTRIM((prm_size_desc))) + ''/'' + RTRIM(LTRIM((prm_fnsh))) as product
      , '''' as rmkuser,isslp.slp_lgn_id, osslp.slp_lgn_id,
      SalesNP
      FROM ['+ @prefix +'_trjtph_rec] a  
      left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no 
	  left join #tmpSalesNP on CompanyID = tph_CMPY_ID and OrderPfx = b.tud_prnt_pfx and OrderNo = b.tud_prnt_no and OrderItem = b.tud_prnt_itm
      left join ['+ @prefix +'_ortorh_rec]  ON b.tud_cmpy_id = orh_cmpy_id and b.tud_prnt_pfx=orh_ord_pfx AND b.tud_prnt_no=orh_ord_no   
      left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  and b.tud_prnt_sitm = c.orl_ord_rls_no  
      left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
      left join ' + @prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = orh_cmpy_id and isslp.slp_slp = orh_is_slp         
       left join ' + @prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = orh_cmpy_id and osslp.slp_slp = orh_os_slp 
      left join '+ @prefix +'_tctipd_rec on ipd_cmpy_id = ord_cmpy_id and ipd_ref_pfx = ord_ord_pfx and ipd_ref_no = ord_ord_no and ipd_ref_itm = ord_ord_itm 
      left join '+ @prefix +'_inrprm_rec on prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size = ipd_size and prm_fnsh = ipd_fnsh 
      left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id
      LEFT JOIN ' + @prefix + '_ortxre_rec on xre_cmpy_id = orl_cmpy_id AND xre_ord_pfx = orl_ord_pfx AND xre_ord_no = orl_ord_no
      WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''     and ord_ord_brh not in  (''SFS'') 
      AND(  
      (b.tud_prnt_no<>0   
      and TPH_NBR_STP =1   
      and b.tud_sprc_pfx<>''SH''   
      and b.tud_sprc_pfx <>''IT'')    
      OR   
       (b.tud_sprc_pfx in (''IT'',''SH'')) 
      )'  
        
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'   
        END  
       Else  
        BEGIN  
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'  
        END  
      END  
      Set @query += '  '  
       
       print @query;
        EXECUTE sp_executesql @query;  
        
        insert into #tmp select * from #tmpRegular;
  insert into #tmp select * from #tmpTransfer tr where tr.SprcNo not in (select rg.SprcNo from #tmpRegular rg);
  truncate table #tmpRegular;
  truncate table #tmpTransfer;
  truncate table #tmpSalesNP;
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     
     BEGIN  
     Set @prefix= @DBNAME  
     SET @DB= @Prefix                     
      IF (UPPER(@DB) = 'TW')                  
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                              
   End                  
   Else if (UPPER(@DB) = 'NO')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                              
   End                    
   Else if (UPPER(@DB) = 'CA')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
   End                              
   Else if (UPPER(@DB) = 'CN')                              
   begin                              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                              
 End                              
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                              
   begin                        
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                              
   End                              
   Else if(UPPER(@DB) = 'UK')                              
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                              
   End   
   Else if(UPPER(@DB) = 'DE')                              
   begin                              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                              
   End   
       
     
      SET @sqltxt = 'INSERT INTO #tmpRegular (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
       Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat,OriginalDueDt,OrderEntryDt,Product ,RmkUser,SalesPresonIs,SalesPresonOs,SalesNP)  
      select  dpf_cmpy_id as ''CompanyName'', '
      if (UPPER(@prefix)= 'US')
      BEGIN
      SET @sqltxt = @sqltxt + ' Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '
      END
      ELSE
      BEGIN
      SET @sqltxt = @sqltxt + ' ISNULL((case when orh_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po = us.stn_end_usr_po and 
 cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(dpf_ord_rls_no as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '
      END 
      
      SET @sqltxt = @sqltxt + ' dpf_sprc_pfx as ''SPRCPFX'',(dpf_sprc_no) as ''SprcNo'',   
    dpf_ord_pfx as ''Orders'', dpf_ord_no as ''OrderNo'',(dpf_ord_itm) as ''OrderItem'',  
    dpf_ord_rls_no as ''RlsNo'', dpf_ord_brh as ''Branch'',CONVERT(VARCHAR(10), (dpf_rls_due_fm_dt), 120)  as ''DueFromDt'',  
    dpf_transp_pfx as ''Transport'',dpf_transp_no as ''TranspNO'',CONVERT(VARCHAR(10), dpf_shpg_dtts, 120)  as ''shpgDates'' ,  
    (dpf_shp_pcs) as ''Pcs'', '
    
        if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')          
     BEGIN          
     SET @sqltxt = @sqltxt + ' (dpf_shp_wgt) * 2.20462 as ''Wgt'', '          
     END          
       ELSE          
     BEGIN    
             
     SET @sqltxt = @sqltxt + ' (dpf_shp_wgt) as ''Wgt'', '          
     END  
    
    
    SET @sqltxt = @sqltxt + ' case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'', '''' as LateShptWithReason,  
    (dpf_rsn_typ) as ''RsnType'' ,(dpf_rsn) as ''RsnCode'',(dpf_rsn_rmk) as ''Remarks'' ,''Regular'' as OTPType  
    ,ord_sld_cus_id as CustID, cus_cus_long_nm as CustName,0, (orl_rqst_fm_dt), CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
    ,RTRIM(LTRIM((prm_frm))) + ''/'' + RTRIM(LTRIM((prm_grd))) + ''/'' + RTRIM(LTRIM((prm_size_desc))) + ''/'' + RTRIM(LTRIM((prm_fnsh))) as product
     ,(dpf_lgn_id),isslp.slp_lgn_id, osslp.slp_lgn_id,
     (select SUM(sat_tot_val)* '+ @CurrenyRate +'  from '+ @prefix +'_sahsat_rec where sat_cmpy_id = dpf_cmpy_id and sat_ord_pfx = dpf_ord_pfx and sat_ord_no = dpf_ord_no 
     and sat_ord_itm = dpf_ord_itm and sat_ord_rls_no = dpf_ord_rls_no)
    from '+ @prefix +'_pfhdpf_rec 
    join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no    
    JOIN '+ @prefix +'_ortorl_rec   
                ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no  
                Join '+ @prefix +'_ortord_rec  
                on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
				join '+ @prefix +'_arrcus_rec  
                on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id 
		left join ' + @prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = orh_cmpy_id and isslp.slp_slp = orh_is_slp         
       left join ' + @prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = orh_cmpy_id and osslp.slp_slp = orh_os_slp                 
     left join ' + @prefix + '_inrprm_rec on prm_frm = dpf_frm and prm_grd = dpf_grd and prm_size = dpf_size and prm_fnsh = dpf_fnsh           
     LEFT JOIN ' + @prefix + '_ortxre_rec on xre_cmpy_id = orl_cmpy_id AND xre_ord_pfx = orl_ord_pfx AND xre_ord_no = orl_ord_no
    where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)  
    and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '  
    IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'   
        END  
       Else  
        BEGIN  
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'  
        END  
      END  
      Set @sqltxt += ' '  
    print(@sqltxt)  
     
  set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
   
   -- Sales NP for Transfer
   SET @sqltxt = ' insert into #tmpSalesNP (CompanyID,OrderPfx,OrderNo,OrderItem,SalesNP)  
   select sat_cmpy_id,sat_ord_pfx,sat_ord_no,sat_ord_itm,SUM(sat_tot_val)* '+ @CurrenyRate +' as total
from US_sahsat_rec 
group by sat_cmpy_id,sat_ord_pfx,sat_ord_no,sat_ord_itm;
   '
   
   print(@sqltxt)  
     
  set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt); 
   
 -- Transfer  
  SET @sqltxt = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,SPRCPFX,SprcNo,Orders,OrderNo,OrderItem,RlsNo,Branch,DueFromDt,Transport,TranspNO,shpgDates,  
               Pcs, Wgt,Late,LateShptWithReason,RsnType,RsnCode,Remarks,OTPType,CustID,CustName,Heat,OriginalDueDt,OrderEntryDt,Product ,RmkUser,SalesPresonIs,SalesPresonOs,SalesNP)  
                   SELECT tph_CMPY_ID as ''CompanyName'',Replace(b.tud_trpln_whs,''SFS'',''LAX'') as ''ShpgWhs'',b.tud_sprc_pfx as ''SPRCPFX'',(b.tud_sprc_no) as ''SprcNo'',
				   '''' as Orders, '''' as OrderNo,'''' as OrderItem,'''' as RlsNo,Replace(b.tud_trpln_whs,''SFS'',''LAX'') as Branch,  
                     CONVERT(VARCHAR(10),(c.orl_due_to_dt), 120) as ''DueFromDt'',    
                    a.tph_transp_pfx as ''Transfer'', a.tph_transp_no as ''Transfer No'', CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) as ''shpgDates'' ,    
        (b.tud_comp_pcs) as ''Pcs'', '
        if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')          
     BEGIN          
     SET @sqltxt = @sqltxt + ' (b.tud_comp_wgt) * 2.20462 as ''Wgt'', '          
     END          
       ELSE          
     BEGIN    
             
     SET @sqltxt = @sqltxt + ' (b.tud_comp_wgt) as ''Wgt'','          
     END 
       
        
         
      SET @sqltxt = @sqltxt + 'case when (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'','''' as LateShptWithReason,'''' as RsnType,'''' as RsnCode,'''' as Remarks,  
      ''Transfer'' as OTPType  
      ,(ord_sld_cus_id) as CustID, (cus_cus_long_nm) as CustName, 0, (c.orl_rqst_fm_dt), CONVERT(VARCHAR(10),(xre_crtd_dtts), 120)
      ,RTRIM(LTRIM((prm_frm))) + ''/'' + RTRIM(LTRIM((prm_grd))) + ''/'' + RTRIM(LTRIM((prm_size_desc))) + ''/'' + RTRIM(LTRIM((prm_fnsh))) as product
      , '''' as rmkuser,isslp.slp_lgn_id, osslp.slp_lgn_id,
      SalesNP 
      FROM ['+ @prefix +'_trjtph_rec] a  
      left  JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no  
	  left join #tmpSalesNP on CompanyID = tph_CMPY_ID and OrderPfx = b.tud_prnt_pfx and OrderNo = b.tud_prnt_no and OrderItem = b.tud_prnt_itm
      left join ['+ @prefix +'_ortorh_rec]  ON b.tud_cmpy_id = orh_cmpy_id and b.tud_prnt_pfx=orh_ord_pfx AND b.tud_prnt_no=orh_ord_no  
      left  JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  and b.tud_prnt_sitm = c.orl_ord_rls_no  
      left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no   
      left join ' + @prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = orh_cmpy_id and isslp.slp_slp = orh_is_slp         
       left join ' + @prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = orh_cmpy_id and osslp.slp_slp = orh_os_slp 
      left join '+ @prefix +'_tctipd_rec on ipd_cmpy_id = ord_cmpy_id and ipd_ref_pfx = ord_ord_pfx and ipd_ref_no = ord_ord_no and ipd_ref_itm = ord_ord_itm 
      left join '+ @prefix +'_inrprm_rec on prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size = ipd_size and prm_fnsh = ipd_fnsh 
      left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id 
      LEFT JOIN ' + @prefix + '_ortxre_rec on xre_cmpy_id = orl_cmpy_id AND xre_ord_pfx = orl_ord_pfx AND xre_ord_no = orl_ord_no
      WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''    and ord_ord_brh not in  (''SFS'')  
      AND(  
      (b.tud_prnt_no<>0   
      and TPH_NBR_STP =1   
      and b.tud_sprc_pfx<>''SH''   
      and b.tud_sprc_pfx <>''IT'')    
      OR   
      (b.tud_sprc_pfx in (''IT'',''SH''))  
      )'  
     IF  @CusID Like '%''0''%'  
      BEGIN  
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'   
      END  
     ELSE  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
            
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'   
        END  
       Else  
        BEGIN  
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'  
        END  
      END  
      Set @sqltxt += '  '  
     
   print(@sqltxt)  
   set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt); 
    
      insert into #tmp select * from #tmpRegular;
  insert into #tmp select * from #tmpTransfer tr where tr.SprcNo not in (select rg.SprcNo from #tmpRegular rg);
     
     END                 
 
 
  select * from #tmp where (ShpgWhs = @InBranch OR Branch = @InBranch OR @InBranch = '') 

drop table #tmpSalesNP;  
  drop table #tmpTransfer ;
  drop table #tmpRegular ;
drop table #tmp ;
 
  
  
END  

-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID varchar(Max),@CustmomerNoTxt varchar(Max),@DateRange int, @version char = '0', @InBranch Varchar(3) = 'ALL'    
-- exec sp_itech_OTPDetails_V1 '03/01/2020', '03/31/2020' , 'US','','',5  -- 110
/*
date 20180420/20180507
sub: Please check this report

Mail sub:OTP report not loading

*/
GO
