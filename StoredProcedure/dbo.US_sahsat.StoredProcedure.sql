USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sahsat]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                            
-- =============================================                                                              
-- Author:  <Author,Clayton Daigle>                                                              
-- Create date: <Create Date,10/5/2012,>                                                              
-- Description: <Description,Open Orders,>                                                              
 -- Last updated by mukesh Date 28 Jul,2015                                                         
 -- Last change Desc:ignore the record sat_upd_ref_no= 113717                                                              
-- =============================================                                                              
CREATE PROCEDURE [dbo].[US_sahsat]                                                              
                                                               
AS                                                              
BEGIN                                                              
 -- SET NOCOUNT ON added to prevent extra result sets from                                                              
 -- interfering with SELECT statements.                                                              
 SET NOCOUNT ON;                                                              
Delete from dbo.US_sahsat_rec where sat_upd_cy >= YEAR(GETDATE() - 4);                                                              
                                                               
Insert into dbo.US_sahsat_rec                                                               
                                                              
    -- Insert statements for procedure here                                                              
    -- There is a problem in column sat_part                                                            
                                                                
SELECT *                                                              
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec]                                                              
where (sat_upd_ref_no != 111853 OR sat_upd_ref_itm != 1)                                                          
AND (sat_upd_ref_no != 113160 OR sat_upd_ref_itm != 1 )                                                               
AND (sat_upd_ref_no != 113717 OR sat_upd_ref_itm != 1 )                                                        
AND (sat_upd_ref_no != 116328 OR sat_upd_ref_itm != 1 )                                                      
AND (sat_upd_ref_no != 117952 OR sat_upd_ref_itm != 1 )                                                  
AND (sat_upd_ref_no != 118773 OR sat_upd_ref_itm != 1 )                                                 
AND(sat_upd_ref_no != 119365 OR  sat_upd_ref_itm != 1 )                                               
AND(sat_upd_ref_no != 119744 OR  sat_upd_ref_itm != 1 )                                             
AND(sat_upd_ref_no != 144566 OR  sat_upd_ref_itm != 1 )                                           
AND(sat_upd_ref_no != 145170 OR  sat_upd_ref_itm != 1 )                                                    
AND(sat_upd_ref_no != 145633 OR  sat_upd_ref_itm != 1 )                                    
AND(sat_upd_ref_no != 145789 OR  sat_upd_ref_itm != 1 )                                    
AND(sat_upd_ref_no != 145789 OR  sat_upd_ref_itm != 2 )                                    
AND(sat_upd_ref_no != 188766 )                                 
AND(sat_upd_ref_no != 192641 )                                
AND(sat_upd_ref_no != 198912 )                              
AND(sat_upd_ref_no != 207721 )                            
AND(sat_upd_ref_no != 207857 )                          
AND(sat_upd_ref_no != 208839 ) -- 20170823                            
AND(sat_upd_ref_no != 208977 ) -- 20170824                      
AND(sat_upd_ref_no != 216507 OR  sat_upd_ref_itm != 1 ) -- 20171101                    
AND(sat_upd_ref_no != 224166 ) --20180119                   
AND(sat_upd_ref_no != 225797 ) -- 20180201                
AND (sat_upd_ref_no != 250091 ) -- 20180829             
--AND (sat_upd_ref_no != 265578 )     -- 20190118              
--and  (sat_upd_ref_no != 269876 ) -- 20190222        
        
    --and sat_upd_cy = 2020 and sat_upd_mth = 3 and sat_upd_dy = 19    
and  sat_upd_cy >= YEAR(GETDATE() - 4)  
--order by sat_upd_ref_no desc            
 ;                                      
                                      
-- Inserting PS record in US database                                      
                                      
insert into US_sahsat_rec                       
select                                       
'USS',                                      
sat_sls_qlf,                                      
sat_upd_ref_no,                                      
sat_upd_ref_itm,                      
sat_upd_cy,                                      
sat_upd_mth,                                      
sat_upd_dy,                                      
sat_shpt_pfx,                                      
sat_shpt_no,                                      
sat_shpt_itm,                                      
Replace(sat_shpt_brh,'PSM','LAX'),                                      
sat_inv_pfx,                                      
sat_inv_typ,                                      
sat_upd_ref,                                      
sat_inv_dt,                                      
'L' + sat_sld_cus_id,                                      
sat_shp_to,                                      
sat_nm1,                                
sat_cus_po,                                      
sat_cus_po_dt,                                      
sat_cus_ctrct,                                      
sat_end_usr_po,                                      
sat_cus_rls,                                      
sat_job_id,                                      
sat_cry,                                      
sat_exrt,                                      
sat_ex_rt_typ,                                      
sat_is_slp,                                      
sat_os_slp,                                      
sat_tkn_slp,                                      
sat_sls_cat,                                      
sat_chrg_qty_typ,                                      
sat_wfl_sls,                                      
sat_use_wgt_mult,                                      
sat_wgt_mult_pct,                                      
sat_aly_schg_cl,                                      
sat_src,                                      
sat_cntr_sls,                                      
sat_fab_itm_cd,                                      
sat_ord_pfx,                                      
sat_ord_no,                                      
sat_ord_itm,                                      
sat_ord_rls_no,                                      
sat_transp_pfx,                                      
sat_transp_no,                                      
sat_pk_list_no,                                      
replace(sat_shpg_whs,'PSM','LAX'),                                      
sat_shp_dt,                                      
sat_sprc_pfx,                                      
sat_sprc_no,                                      
sat_trm_trd,                                      
sat_frm,                                      
sat_grd,                                      
sat_num_size1,                                      
sat_num_size2,                                      
sat_num_size3,                                      
sat_num_size4,                                      
sat_num_size5,                                      
sat_size,                                      
sat_fnsh,                                      
sat_ef_evar,                                      
sat_wdth,                                      
sat_lgth,                                      
sat_dim_dsgn,                                      
sat_octg_ent_md,                                      
sat_idia,                                      
sat_odia,                                      
sat_ga_size,                                      
sat_ga_typ,                                      
sat_alt_size,            
sat_rdm_dim_1,                                      
sat_rdm_dim_2,                                      
sat_rdm_dim_3,                                      
sat_rdm_dim_4,                                      
sat_rdm_dim_5,                                      
sat_rdm_dim_6,                                      
sat_rdm_dim_7,                         
sat_rdm_dim_8,                                      
sat_rdm_area,                                      
sat_ent_msr,                                      
sat_dia_frc_fmt,                                      
sat_ord_lgth_typ,                                      
sat_part_cus_id,                                 
sat_part,                                      
sat_part_rev_no,                                      
sat_part_acs,                                      
sat_wdth_intgr,                                      
sat_wdth_nmr,                                      
sat_wdth_dnm,                                      
sat_lgth_intgr,            
sat_lgth_nmr,                                      
sat_lgth_dnm,                                      
sat_idia_intgr,                                      
sat_idia_nmr,                                      
sat_idia_dnm,                                      
sat_odia_intgr,                                      
sat_odia_nmr,                               
sat_odia_dnm,                                      
sat_blg_pcs,                                      
sat_blg_msr,                                      
sat_blg_wgt,                                      
sat_blg_qty,                                      
sat_ord_wgt_um,                                      
sat_rlvd_pcs,                                      
sat_rlvd_msr,                                      
sat_rlvd_wgt,                                      
sat_rlvd_qty,                                      
sat_ord_chrg,                                      
sat_mtl_chrg,                                      
sat_mtl_chrg_um,                                      
sat_tot_chrg,                                      
sat_tot_val,                                      
sat_tot_mtl_val,                                      
sat_mtl_avg_val,                                      
sat_mtl_repl_val,                                      
sat_tot_avg_val,                                     
sat_tot_repl_val,                                      
sat_mpft_avg_val,                                      
sat_mpft_repl_val,                                      
sat_npft_avg_val,                                      
sat_npft_repl_val,                                      
sat_mpft_avg_pct,                                      
sat_mpft_repl_pct,                                      
sat_npft_avg_pct,                                      
sat_npft_repl_pct                                      
from PS_sahsat_rec;                                      
                                        
END       
      
      
      
/*20190225      
Change the lenght of sat_part_rev_no from 2 to 8      
  
20200322  
Mail sub:RE: Fwd: eCIM Call #352036 - Regarding Sales History Data  
*/
GO
