USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_injitd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

        
-- =============================================        
-- Author:  <Author,Clayton Daigle>        
-- Create date: <Create Date,12/2/2013,>        
-- Description: <>        
-- Last changes by mukesh         
-- Last updated date: 30 Jun 2015           
-- Last Desc: Change the select Query       
        
-- =============================================        
CREATE PROCEDURE [dbo].[TW_injitd]
AS        
BEGIN        
 -- SET TWCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;                
IF OBJECT_ID('dbo.TW_injitd_rec', 'U') IS NOT NULL              
  drop table dbo.TW_injitd_rec;         
        
    -- Insert statements for procedure here        
--(itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,itd_itm_ctl_no,itd_brh,        
--itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_idia,itd_odia,itd_ga_size,itd_ga_typ,itd_wdth,itd_lgth,itd_dim_dsgn,itd_rdm_dim_1,itd_rdm_dim_2,itd_rdm_dim_3,        
--itd_rdm_dim_4,itd_rdm_dim_5,itd_rdm_dim_6,itd_rdm_dim_7,itd_rdm_dim_8,itd_tag_no,itd_lbl_idfr,itd_whs,itd_loc,itd_mill,itd_heat,itd_qds_ctl_no,itd_ownr,itd_ownr_ref_id,itd_ownr_tag_no,        
--itd_bgt_for,itd_bgt_for_id,itd_invt_typ,itd_invt_sts,itd_invt_qlty,itd_invt_cat,itd_orig_zn,itd_ord_ffm,itd_upd_dtts,itd_upd_dtms,itd_prod_for,itd_part_cus_id,        
--itd_part,itd_part_rev_no,itd_part_acs,itd_cst_qty_indc,itd_prod_indc,itd_prs_parm_indc,itd_instr_indc,itd_hld_indc,itd_cond_indc,itd_atchmt_indc,itd_src_tag_indc)        

--Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,itd_itm_ctl_no,itd_brh,        
--itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_idia,itd_odia,itd_ga_size,itd_ga_typ,itd_wdth,itd_lgth,itd_dim_dsgn,itd_rdm_dim_1,itd_rdm_dim_2,itd_rdm_dim_3,        
--itd_rdm_dim_4,itd_rdm_dim_5,itd_rdm_dim_6,itd_rdm_dim_7,itd_rdm_dim_8,itd_tag_no,itd_lbl_idfr,itd_whs,itd_loc,itd_mill,itd_heat,itd_qds_ctl_no,itd_ownr,itd_ownr_ref_id,itd_ownr_tag_no,        
--itd_bgt_for,itd_bgt_for_id,itd_invt_typ,itd_invt_sts,itd_invt_qlty,itd_invt_cat,itd_orig_zn,itd_ord_ffm,itd_upd_dtts,itd_upd_dtms,itd_prod_for,itd_part_cus_id,        
--itd_part_revno,itd_part_acs,itd_cst_qty_indc,itd_prod_indc,itd_prs_parm_indc,itd_instr_indc,itd_hld_indc,itd_cond_indc,itd_atchmt_indc,itd_src_tag_indc        
--into dbo.TW_injitd_rec       
--FROM [LIVETWSTX].[livetwstxdb].[informix].[injitd_rec]        
Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,itd_itm_ctl_no,itd_brh,        
itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_idia,itd_odia,itd_ga_size,itd_ga_typ,itd_wdth,itd_lgth,itd_dim_dsgn,itd_rdm_dim_1,itd_rdm_dim_2,itd_rdm_dim_3,        
itd_rdm_dim_4,itd_rdm_dim_5,itd_rdm_dim_6,itd_rdm_dim_7,itd_rdm_dim_8,itd_tag_no,itd_lbl_idfr,itd_whs,itd_loc,itd_mill,itd_heat,itd_qds_ctl_no,itd_ownr,itd_ownr_ref_id,itd_ownr_tag_no,        
itd_bgt_for,itd_bgt_for_id,itd_invt_typ,itd_invt_sts,itd_invt_qlty,itd_invt_cat,itd_orig_zn,itd_ord_ffm,itd_upd_dtts,itd_upd_dtms,itd_prod_for,itd_part_cus_id,        
itd_part_revno,itd_part_acs,itd_cst_qty_indc,itd_prod_indc,itd_instr_indc,itd_hld_indc,itd_cond_indc,itd_atchmt_indc,itd_src_tag_indc        
into dbo.TW_injitd_rec       
FROM [LIVETWSTX].[livetwstxdb].[informix].[injitd_rec] 

        
END        
/*
Date: 20200706	Sumit
Stratix Upgrade 10.4 column mismatch issue
remove itd_prs_parm_indc from selection
*/ 
GO
