USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesOffice]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh>        
-- Create date: <16 Nov 2017>        
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_SalesOffice]  @DBNAME varchar(50), @Branch varchar(50)      
AS        
BEGIN        
      
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
declare @DB varchar(100);        
declare @sqltxt varchar(6000);        
declare @execSQLtxt varchar(7000);        
DECLARE @CountryName VARCHAR(25);           
DECLARE @prefix VARCHAR(15);           
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @CurrenyRate varchar(15);          
        
      
IF @Branch = 'ALL'        
 BEGIN        
  set @Branch = ''        
        
 END       
 print '@Branch =' + @Branch;      
 declare @start varchar = ''      
        
CREATE TABLE #temp (   
Dbname   VARCHAR(10)  
,Branch   VARCHAR(3)  
,ISlp  VARCHAR(4)   
,OrderNo  NUMERIC  
, TotalSlsVal decimal(20,0)  
,Profit   decimal(20,1)        
  --   ,DBCountryName VARCHAR(25)       
     --,OSlp  VARCHAR(4)         
    -- ,CusID   VARCHAR(10)        
     --,CusLongNm  VARCHAR(40)        
     --,ActvyDT  VARCHAR(10)        
     --,OrderItm  NUMERIC        
     --,Product  VARCHAR(500)        
     --,Wgt   decimal(20,0)        
     --,TotalMtlVal decimal(20,0)        
     --,ReplCost  decimal(20,0)        
     --,Market Varchar(35)       
     );        
        
IF @DBNAME = 'ALL'        
 BEGIN        
        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
          
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   DECLARE @query NVARCHAR(MAX);        
   IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
            
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR         
    BEGIN        
    SET @query = 'INSERT INTO #temp (Dbname, Branch, ISlp, OrderNo, TotalSlsVal,Profit)        
      SELECT '''+ @Prefix +''' as Country, a.mbk_brh, mbk_is_slp, a.mbk_ord_no, a.mbk_tot_val* '+ @CurrenyRate +'  ,     
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END  
      FROM ' + @Prefix + '_ortmbk_rec a        
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id           
      WHERE a.mbk_ord_pfx=''SO''         
          AND a.mbk_ord_itm<>999          
         and a.mbk_trs_md = ''A''        
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)       
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '       
                
    END        
    ELSE        
    BEGIN        
     SET @query = 'INSERT INTO #temp (Dbname, Branch, ISlp, OrderNo, TotalSlsVal,Profit)        
 SELECT '''+ @Prefix +''' as Country, a.mbk_brh, mbk_is_slp, a.mbk_ord_no, a.mbk_tot_val* '+ @CurrenyRate +'  ,     
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END  
      FROM ' + @Prefix + '_ortmbk_rec a        
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id           
      WHERE a.mbk_ord_pfx=''SO''         
          AND a.mbk_ord_itm<>999          
         and a.mbk_trs_md = ''A''        
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)       
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')'       
               
    END        
   print @query;        
   EXECUTE sp_executesql @query;        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  END           
  CLOSE ScopeCursor;        
  DEALLOCATE ScopeCursor;        
 END        
ELSE        
BEGIN        
  
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)        
 IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DB ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
            
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR         
   BEGIN        
   SET @sqltxt ='INSERT INTO #temp (Dbname, Branch, ISlp, OrderNo, TotalSlsVal,Profit)        
      SELECT '''+ @DBNAME +''' as Country, a.mbk_brh, mbk_is_slp, a.mbk_ord_no, a.mbk_tot_val* '+ @CurrenyRate +'  ,     
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END  
      FROM ' + @DBNAME + '_ortmbk_rec a        
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id           
      WHERE a.mbk_ord_pfx=''SO''         
          AND a.mbk_ord_itm<>999          
         and a.mbk_trs_md = ''A''        
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)       
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '       
               
  END        
  ELSE        
  BEGIN        
  SET @sqltxt ='INSERT INTO #temp (Dbname, Branch, ISlp, OrderNo, TotalSlsVal,Profit)        
      SELECT '''+ @DBNAME +''' as Country, a.mbk_brh, mbk_is_slp, a.mbk_ord_no, a.mbk_tot_val* '+ @CurrenyRate +'  ,     
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END  
      FROM ' + @DBNAME + '_ortmbk_rec a        
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id           
      WHERE a.mbk_ord_pfx=''SO''         
          AND a.mbk_ord_itm<>999          
         and a.mbk_trs_md = ''A''      
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, GETDATE()-1)       
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '       
                
  END        
 print(@sqltxt)        
 set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
END    
      
 SELECT Dbname, Branch, t3.ISlp, COUNT(distinct OrderNo) as OrderNo, SUM(TotalSlsVal) as TotalSlsVal, 
 case when SUM(TotalSlsVal) = 0 then 0 else (Sum((Profit*TotalSlsVal)/100)/SUM(TotalSlsVal))*100 end as Profit,   
 count(t3.ISlp) as lines,  
   
 (select Sum(t4.BelowMin) from     
(select Case When SUM(t2.TotalSlsVal) < 315 then 1 else 0 end as BelowMin, t2.ISlp from #temp t2  group by t2.OrderNo,t2.ISlp)   
as t4 Where t4.ISlp = t3.ISlp) as BelowMin,  
   
 (select (Sum((t1.Profit*t1.TotalSlsVal)/100)/SUM(t1.TotalSlsVal))*100 from #temp t1) as AVGProfit  
   FROM #temp t3  
 group by Dbname, Branch, t3.ISlp;   
  
        
 DROP TABLE  #temp;        
END        
        
--EXEC [sp_itech_SalesOffice] 'ALL'        
--EXEC [sp_itech_SalesOffice] 'UK', 'ALL'       
--EXEC [sp_itech_SalesOffice] 'US', 'CRP'   
--select * from tbl_itech_DatabaseName  
GO
