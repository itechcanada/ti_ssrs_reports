USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_tctcnt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================        
-- Author:  <Author,Mukesh>        
-- Create date: <Create Date,2019-03-26,>        
-- Description: <Description,Open Orders,>        
-- Problem in column cus_cus_nm        
-- =============================================        
CREATE PROCEDURE [dbo].[UK_tctcnt]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
Delete from dbo.UK_tctcnt_rec ;         
         
Insert into dbo.UK_tctcnt_rec         
        
    -- Insert statements for procedure here        
SELECT *     
FROM [LIVEUKSTX].[liveukstxdb].[informix].[tctcnt_rec]      
     
END 


GO
