USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysis_BusinessPlan]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Mukesh >      
-- Create date: <11 sep 2015>      
-- Last Updated By: Mukesh          
-- Last updated Description:       
-- Last Updated Date:       
-- last updated by :      
-- =============================================      
      
      
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysis_BusinessPlan] @ToDate datetime, @DBNAME varchar(50),@Branch varchar(3), @Market as varchar(65),@IncludePierce char = '0', @IncludeInterco char = '0'       
      
AS      
BEGIN     

 
      
 SET NOCOUNT ON;      
declare @sqltxt varchar(7000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(50)      
declare @FD varchar(10)      
declare @TD varchar(10)      
      
set @DB=  @DBNAME      
-- set @FD = CONVERT(VARCHAR(10), '01-01-' + CONVERT(varchar(4), YEAR(@ToDate)) , 120)      
set @FD = CONVERT(VARCHAR(10), '01-01-' + CONVERT(varchar(4), YEAR(CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @ToDate)-1, -1) , 120) )) , 120) 
 set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @ToDate)-1, -1) , 120)      
    
CREATE TABLE #tmp (  Databases   VARCHAR(15)       
     , Branch  Varchar(3)        
     ,CustID   VARCHAR(10)      
        , CustName     VARCHAR(45)      
        , Market   VARCHAR(65)       
        , TotWeight  DECIMAL(20, 0)      
        , TotalValue    DECIMAL(20, 0)      
        , TotalMtlVal DECIMAL(20, 0)     
        , TotalAvgVal DECIMAL(20, 0)     
        , MatGP       DECIMAL(20, 1)      
        ,BaseMetal Varchar(2)      
        ,Grd  Varchar(10)      
        ,Product Varchar(2)      
        ,Finish  Varchar(10)   
        ,SalesPresonIs varchar(10)    
        ,SalesPresonOs varchar(10) 
        , NetGP       DECIMAL(20, 1)       
        , NetAVGVal       DECIMAL(20, 0) 
        , NetTotalVal       DECIMAL(20, 0) 
        
                 );       
      
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(35);      
DECLARE @Name VARCHAR(15);      
DECLARE @CurrenyRate varchar(15);   
DECLARE @IsExcInterco char(1);     
      
if @Market ='ALL'      
 BEGIN      
 set @Market = ''      
 END      
   
 if (@Market ='ExcInterco' OR @IncludeInterco = '0')  
BEGIN  
set @IsExcInterco ='T'  
END  
     
if @Branch = 'ALL'      
BEGIN      
set @Branch = ''      
END      
      
IF @DBNAME = 'ALL'      
 BEGIN      
   IF ( @IncludePierce = '0')  
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
   OPEN ScopeCursor;    
    END    
    ELSE    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    END      
         
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
        DECLARE @query NVARCHAR(max);        
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'      
       IF (UPPER(@Prefix) = 'TW')      
        begin      
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
        End      
    Else if (UPPER(@Prefix) = 'NO')      
        begin      
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
        End      
    Else if (UPPER(@Prefix) = 'CA')      
        begin      
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
        End      
    Else if (UPPER(@Prefix) = 'CN')      
        begin      
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
        End      
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')      
        begin      
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
        End      
    Else if(UPPER(@Prefix) = 'UK')      
        begin      
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
        End       
    Else if(UPPER(@Prefix) = 'DE')      
        begin      
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
        End       
          
           
      SET @query =  'INSERT INTO #tmp (Databases, Branch, CustID, CustName, Market, TotWeight, TotalValue,TotalMtlVal,TotalAvgVal, MatGP, BaseMetal, Grd, Product, Finish,SalesPresonIs,SalesPresonOs,NetGP,NetAVGVal,NetTotalVal)           
       select '''+ @Name +''' as databases, stn_shpt_brh, stn_sld_cus_id  as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,      
      SUM(stn_blg_wgt) as TotWeight, SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,    
       SUM(ISNULL(stn_tot_mtl_val,0))* '+ @CurrenyRate +',SUM(ISNULL(stn_mpft_avg_val,0))* '+ @CurrenyRate +', CASE WHEN SUM(ISNULL(stn_tot_mtl_val,0))<> 0 THEN (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_mtl_val,0)))*100 END AS MatGP,            
      SUBSTRING(stn_frm,1,2), stn_grd, SUBSTRING(stn_frm,3,2), stn_fnsh ,isslp.slp_lgn_id, osslp.slp_lgn_id,
      CASE WHEN SUM(ISNULL(stn_tot_val,0))<> 0 THEN (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0)))*100 END AS NetGP,
       SUM(ISNULL(stn_npft_avg_val,0))* '+ @CurrenyRate +',SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'      
       from ' + @DB + '_sahstn_rec       
       join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
       JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id  
       left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp       
       left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp           
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''      
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')      
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')      
        group by  stn_shpt_brh,stn_sld_cus_id, cus_cus_long_nm,isslp.slp_lgn_id, osslp.slp_lgn_id, cuc_desc30,stn_frm,stn_grd,stn_fnsh'      
    print(@query)          
        EXECUTE sp_executesql @query;      
               
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
       END       
             
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN       
             
    Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')        
         
    IF (UPPER(@DBNAME) = 'TW')      
   begin      
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
   End      
   Else if (UPPER(@DBNAME) = 'NO')      
   begin      
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
   End      
   Else if (UPPER(@DBNAME) = 'CA')      
   begin      
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
   End      
   Else if (UPPER(@DBNAME) = 'CN')      
   begin      
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
   End      
       
     Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')      
        begin      
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
        End      
    Else if(UPPER(@DBNAME) = 'UK')      
        begin      
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
        End       
    Else if(UPPER(@DBNAME) = 'DE')      
        begin      
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
        End       
          
           
     SET @sqltxt ='INSERT INTO #tmp (Databases, Branch, CustID, CustName, Market, TotWeight, TotalValue,TotalMtlVal,TotalAvgVal, MatGP, BaseMetal, Grd, Product, Finish,SalesPresonIs,SalesPresonOs,NetGP,NetAVGVal,NetTotalVal)           
       select  '''+ @Name +''' as databases, stn_shpt_brh, stn_sld_cus_id  as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,      
      SUM(stn_blg_wgt) as TotWeight, SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,    
      SUM(ISNULL(stn_tot_mtl_val,0))* '+ @CurrenyRate +',SUM(ISNULL(stn_mpft_avg_val ,0))* '+ @CurrenyRate +', CASE WHEN SUM(ISNULL(stn_tot_mtl_val,0))<> 0 THEN (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_mtl_val,0)))*100 END AS MatGP,            
      SUBSTRING(stn_frm,1,2), stn_grd, SUBSTRING(stn_frm,3,2), stn_fnsh ,isslp.slp_lgn_id, osslp.slp_lgn_id,
      CASE WHEN SUM(ISNULL(stn_tot_val,0))<> 0 THEN (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0)))*100 END AS NetGP,
       SUM(ISNULL(stn_npft_avg_val,0))* '+ @CurrenyRate +',SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'    
       from ' + @DB + '_sahstn_rec       
       join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
       JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id  
       left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp       
       left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp        
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''      
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')      
        and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')      
        group by  stn_shpt_brh,stn_sld_cus_id, cus_cus_long_nm,isslp.slp_lgn_id, osslp.slp_lgn_id, cuc_desc30,stn_frm,stn_grd ,stn_fnsh'      
    print(@sqltxt)  ;      
    set @execSQLtxt = @sqltxt;       
   EXEC (@execSQLtxt);      
     END      
          
      if @IsExcInterco ='T'  
      Begin    
        SELECT Databases, Branch, CustID, CustName, Market,       
      case Databases when 'UK' then (TotWeight * 2.20462) when 'Norway' then (TotWeight * 2.20462) else TotWeight end as TotWeight,      
        TotalValue, MatGP, BaseMetal, Grd, Product, Finish ,TotalMtlVal, TotalAvgVal  ,SalesPresonIs,SalesPresonOs, NetGP,NetAVGVal,NetTotalVal  
         FROM #tmp  where Market <> 'Interco' and Branch not in ('SFS');   
         End  
         Else  
         Begin  
          SELECT Databases, Branch, CustID, CustName, Market,       
      case Databases when 'UK' then (TotWeight * 2.20462) when 'Norway' then (TotWeight * 2.20462) else TotWeight end as TotWeight,      
        TotalValue, MatGP, BaseMetal, Grd, Product, Finish ,TotalMtlVal, TotalAvgVal  ,SalesPresonIs,SalesPresonOs, NetGP,NetAVGVal,NetTotalVal 
         FROM #tmp where Branch not in ('SFS');  
         End      
           
     drop table #tmp      
END      
      
-- exec sp_itech_SalesAnalysis_BusinessPlan '2018-02-17' , 'ALL','',''      
      
      
  --CASE WHEN SUM(stn_tot_mtl_val)<> 0 THEN SUM(stn_mpft_avg_val)/SUM(stn_tot_mtl_val)*100 END AS 'GP%',''
  /*
  20160906
  Sub: Business Plan Report Modification.
  Happy Monday!,  Please have this report changed from Gross GP to Net Profit % thanks. (see last column below).
  
CHANGES:-
20160525 :-
 option out PSM and especially SFS when all databases is selected
 
 SOLUTION:- 
 NitBranch not in ('SFS') and NitWhs not in ('SFS') 
 
 2017-01-03
 Change the from date 
 
 20171013
 sub:error in Business Planning Report
 
*/ 
GO
