USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_pohphr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Clayton Daigle>    
-- Create date: <Create Date,12/2/2013,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[TW_pohphr]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
Delete from dbo.TW_pohphr_rec ;     
     
Insert into dbo.TW_pohphr_rec (    
  phr_cmpy_id,phr_po_pfx,phr_po_no,phr_po_itm,phr_po_crtd_dt,phr_brh,phr_po_typ,phr_po_pl_dt,phr_prc_in_eff,phr_ord_pcs,phr_ord_msr,phr_ord_wgt,phr_ord_qty,phr_ord_wgt_um,    
phr_ven_id,phr_shp_fm,phr_ven_long_nm,phr_frm,phr_grd,phr_num_size1,phr_num_size2,phr_num_size3,phr_num_size4,phr_num_size5,phr_size,phr_fnsh,phr_ef_evar,phr_wdth,    
phr_lgth,phr_dim_dsgn,phr_idia,phr_odia,phr_ga_size,phr_ga_typ,phr_rdm_dim_1,phr_rdm_dim_2,phr_rdm_dim_3,phr_rdm_dim_4,phr_rdm_dim_5,phr_rdm_dim_6,phr_rdm_dim_7,phr_rdm_dim_8,    
phr_rdm_area,phr_ent_msr,phr_frc_fmt,phr_ord_lgth_typ,phr_prd_desc50a,phr_prd_desc50b,phr_prd_desc50c,phr_mtl_cst,phr_mtl_cst_um,phr_tot_cst,phr_qty_cst_um,phr_cry,phr_ex_rt,    
phr_ex_rt_typ,phr_pur_cat,phr_dflt_dist_typ,phr_src,phr_aly_schg_apln,phr_consg,phr_invt_qlty,phr_trm_trd,phr_byr,phr_crtd_lgn_id,phr_po_ref_key    
  )     
    
    -- Insert statements for procedure here    
SELECT     
phr_cmpy_id,phr_po_pfx,phr_po_no,phr_po_itm,phr_po_crtd_dt,phr_brh,phr_po_typ,phr_po_pl_dt,phr_prc_in_eff,phr_ord_pcs,phr_ord_msr,phr_ord_wgt,phr_ord_qty,phr_ord_wgt_um,    
phr_ven_id,phr_shp_fm,phr_ven_long_nm,phr_frm,phr_grd,phr_num_size1,phr_num_size2,phr_num_size3,phr_num_size4,phr_num_size5,phr_size,phr_fnsh,phr_ef_evar,phr_wdth,    
phr_lgth,phr_dim_dsgn,phr_idia,phr_odia,phr_ga_size,phr_ga_typ,phr_rdm_dim_1,phr_rdm_dim_2,phr_rdm_dim_3,phr_rdm_dim_4,phr_rdm_dim_5,phr_rdm_dim_6,phr_rdm_dim_7,phr_rdm_dim_8,    
phr_rdm_area,phr_ent_msr,phr_frc_fmt,phr_ord_lgth_typ,'-','-','-',phr_mtl_cst,phr_mtl_cst_um,phr_tot_cst,phr_qty_cst_um,phr_cry,phr_exrt,    
phr_ex_rt_typ,phr_pur_cat,phr_dflt_dist_typ,phr_src,phr_aly_schg_apln,phr_consg,phr_invt_qlty,phr_trm_trd,phr_byr,phr_crtd_lgn_id,phr_po_ref_key   
FROM [LIVETWSTX].[livetwstxdb].[informix].[pohphr_rec]    
    
    
END    
GO
