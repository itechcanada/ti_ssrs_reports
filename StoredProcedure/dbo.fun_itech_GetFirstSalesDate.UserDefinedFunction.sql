USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_GetFirstSalesDate]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukes>
-- Create date: <09 MAr 2016>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fun_itech_GetFirstSalesDate](@customerID Varchar(10), @compareDate Varchar(10))

RETURNS Varchar(10)
AS
BEGIN
declare @firstSalesDate Varchar(10);

declare @returnStatus Varchar(10)  ;
set @returnStatus = '';

	if(SUBSTRING(@customerID,1,1) ! = 'L')
	return @compareDate ;
	
	select  @firstSalesDate = convert(Varchar(10),coc_frst_sls_dt,120)  from PS_arbcoc_rec where coc_cus_id = SUBSTRING(@customerID,2,len(@customerID));
	
	if (@firstSalesDate is not null and @firstSalesDate != '')
	begin
			set @returnStatus =  case When (@compareDate < @firstSalesDate) then @compareDate else @firstSalesDate end;
	end
	
	else if (@firstSalesDate is null OR @firstSalesDate = '')
	begin
	set @returnStatus =  @compareDate;
	end 
	
	
return  @returnStatus ;
END

GO
