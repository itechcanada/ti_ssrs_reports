USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_tcttsh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: Jan 27 2021
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DE_tcttsh] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.DE_tcttsh_rec', 'U') IS NOT NULL
		drop table dbo.DE_tcttsh_rec;
    
        
SELECT *
into  dbo.DE_tcttsh_rec
FROM [LIVEDESTX].[livedestxdb].[informix].[tcttsh_rec];

END

--- exec DE_tcttsh
-- select top 1 * from DE_tcttsh_rec
GO
