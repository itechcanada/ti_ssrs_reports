USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_scrcrx]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[CA_scrcrx] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_scrcrx_rec', 'U') IS NOT NULL
		drop table dbo.CA_scrcrx_rec;
    
        
SELECT *
into  dbo.CA_scrcrx_rec
FROM [LIVECASTX].[livecastxdb].[informix].[scrcrx_rec];

END
-- select * from CA_scrcrx_rec
GO
