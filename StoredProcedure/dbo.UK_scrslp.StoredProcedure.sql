USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_scrslp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,10/5/2012,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[UK_scrslp]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.UK_scrslp_rec ;	
	
Insert into dbo.UK_scrslp_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVEUKSTX].[liveukstxdb].[informix].[scrslp_rec]


END















GO
