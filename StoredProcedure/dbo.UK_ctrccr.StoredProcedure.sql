USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ctrccr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                                                                
-- Author:  <Author,Sumit>                                                                
-- Create date: <Create Date,11/23/2020,>                                                                
-- Description: <Description,UK Process Cost Number,>    
-- =============================================                                                                
CREATE PROCEDURE [dbo].[UK_ctrccr]                                  
AS                                                                
BEGIN                                                                
 -- SET NOCOUNT ON added to prevent extra result sets from                                                                
 -- interfering with SELECT statements.                                                                
SET NOCOUNT ON;                                                                
  
truncate table dbo.UK_ctrccr_rec ;                                                                 
-- Insert statements for procedure here                                                                
Insert into dbo.UK_ctrccr_rec                                       
SELECT *                                                                
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ctrccr_rec] ;                                       
                                      
                                            
END           
          
          
/*  
  
*/
GO
