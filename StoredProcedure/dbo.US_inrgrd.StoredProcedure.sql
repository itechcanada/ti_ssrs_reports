USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_inrgrd]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		itech
-- Create date:April 4, 2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[US_inrgrd]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_inrgrd_rec', 'U') IS NOT NULL
		drop table dbo.US_inrgrd_rec;
    
        
SELECT *
into  dbo.US_inrgrd_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[inrgrd_rec];

END

-- select * from US_inrgrd_rec
GO
