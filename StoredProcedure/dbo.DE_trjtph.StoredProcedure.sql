USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_trjtph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,01/25/2021,>    
-- Description: <Description,Transport Journal,>    
    
-- =============================================    
create PROCEDURE [dbo].[DE_trjtph]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
Truncate table dbo.DE_trjtph_rec ;     
    
-- Insert statements for procedure here    
insert into dbo.DE_trjtph_rec  
SELECT *    
  from [LIVEDESTX].[livedestxdb].[informix].[trjtph_rec] ;     
      
END    
GO
