USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBranches]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <04 Jun 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetBranches]  @DBNAME varchar(50), @version char = '0'  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  Value varchar(15)   
        ,text Varchar(100)  
        ,temp varchar(3)  
        )  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END 
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( Value,text,temp)  
                  SELECT     brh_brh AS ''Value'', brh_brh AS ''text'',''B'' AS temp  
                            FROM         '+ @Prefix +'_scrbrh_rec '  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)  
                    SELECT     brh_brh AS ''Value'', brh_brh AS ''text'',''B'' AS temp  
                            FROM         '+ @DBNAME +'_scrbrh_rec  
       Union   
       Select ''ALL'' as ''Value'',''All Branch'' as ''text'',''A'' as temp  
       Order by temp,text  
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
        
      IF @DBNAME = 'ALL'  
 BEGIN  
      select * from #tmp  where Value not in ('SFS')
      Union   
   Select 'ALL' as 'Value','All Branch' as 'text','A' as temp  
   Order by temp,text  
      End  
      ELSE  
      BEGIN  
                 IF @DBNAME = 'US' OR @DBNAME = 'PS'  
              select * from #tmp where Value not in ('BHM','MTL','TAI','SFS')  
               Else  
                   select * from #tmp where Value not in ('SFS'); 
      END  
      drop table  #tmp  
END  
  
-- exec sp_itech_GetBranches  'ALL'  
  
  /*
  2016-05-19
  Changed by mukesh
  */
  
  
  
GO
