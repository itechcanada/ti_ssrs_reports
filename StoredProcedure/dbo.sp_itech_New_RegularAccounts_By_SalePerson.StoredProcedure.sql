USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_New_RegularAccounts_By_SalePerson]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <21 Nov 2013>
-- Description:	<Getting Sale Person Name and its AccountCount for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_New_RegularAccounts_By_SalePerson] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch as varchar(65), @SalePerson as Varchar(65) 

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @DB=  @DBNAME

IF @Branch = 'ALL'
 BEGIN
	 set @Branch = ''
 END
 
 IF @SalePerson = 'ALL'
 BEGIN
	 set @SalePerson = ''
 END
 
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)

CREATE TABLE #tmp	 ([Database]		 VARCHAR(10) 
					, Branch			 VARCHAR(65)  
					, AcctOpenDate		 date 
					, SalPerName		 VARCHAR(65)
   					, AcctCount		     integer
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				SET @DB= @Prefix 
  				SET @query ='INSERT INTO #tmp ([Database], Branch, AcctOpenDate, SalPerName, AcctCount) 
  				        SELECT ''' +   @DB + ''' as [Database], shp.shp_admin_brh, CONVERT(CHAR(4), crd_acct_opn_dt, 100) + CONVERT(CHAR(4), crd_acct_opn_dt, 120), slp.slp_lgn_id, COUNT(*)as acctcount
  				        from ' + @DB + '_arrcrd_rec as crd
						join ' + @DB + '_arrshp_rec as shp on shp.shp_cmpy_id = crd.crd_cmpy_id and shp.shp_cus_id = crd.crd_cus_id
						join ' + @DB + '_scrslp_rec as slp on slp.slp_cmpy_id= shp.shp_cmpy_id and slp.slp_slp = shp.shp_is_slp
						where CONVERT(VARCHAR(10), crd_acct_opn_dt, 120) >= '''+ @FD + ''' AND CONVERT(VARCHAR(10), crd_acct_opn_dt, 120) <= ''' + @TD + '''
						And shp.shp_shp_to = 0  And (shp.shp_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
						And (slp.slp_lgn_id = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')
						GROUP BY shp.shp_admin_brh, slp.slp_lgn_id, CONVERT(CHAR(4), crd_acct_opn_dt, 100) + CONVERT(CHAR(4), crd_acct_opn_dt, 120)'
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			  SET @sqltxt ='INSERT INTO #tmp ([Database], Branch, AcctOpenDate, SalPerName, AcctCount) 
  				        SELECT ''' +   @DB + ''' as [Database], shp.shp_admin_brh, CONVERT(CHAR(4), crd_acct_opn_dt, 100) + CONVERT(CHAR(4), crd_acct_opn_dt, 120), slp.slp_lgn_id, COUNT(*)as acctcount
  				        from ' + @DB + '_arrcrd_rec as crd
						join ' + @DB + '_arrshp_rec as shp on shp.shp_cmpy_id = crd.crd_cmpy_id and shp.shp_cus_id = crd.crd_cus_id
						join ' + @DB + '_scrslp_rec as slp on slp.slp_cmpy_id= shp.shp_cmpy_id and slp.slp_slp = shp.shp_is_slp
						where CONVERT(VARCHAR(10), crd_acct_opn_dt, 120) >= '''+ @FD + ''' AND CONVERT(VARCHAR(10), crd_acct_opn_dt, 120) <= ''' + @TD + '''
						And shp.shp_shp_to = 0  And (shp.shp_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
						And (slp.slp_lgn_id = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')
						GROUP BY shp.shp_admin_brh, slp.slp_lgn_id, CONVERT(CHAR(4), crd_acct_opn_dt, 100) + CONVERT(CHAR(4), crd_acct_opn_dt, 120)'
				print(@sqltxt)
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
   SELECT * FROM #tmp ORDER BY AcctOpenDate, Branch, SalPerName
   DROP table #tmp 
END

-- exec sp_itech_New_RegularAccounts_By_SalePerson '09/01/2013', '11/30/2013' , 'ALL','ALL','ALL'
-- exec sp_itech_New_RegularAccounts_By_SalePerson '09/01/2013', '11/30/2013' , 'US','EXP','ALL'
GO
