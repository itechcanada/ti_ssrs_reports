USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_InventoryExcess]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                          
-- Author:  <Mukesh >                          
-- Create date: <07 Apr 2020>      
-- =============================================                          
CREATE PROCEDURE [dbo].[sp_itech_InventoryExcess] @DBNAME varchar(50), @InventoryStatus varchar(10),@IncludePierce char = '0', @IncludeInterco char = '0',  @IncludeSizeFinish Char = '1',  @CurrencyInUS Char = '1'                         
                          
AS                          
BEGIN                          
                   
                           
 SET NOCOUNT ON;                          
declare @sqltxt varchar(8000)                          
declare @execSQLtxt varchar(8000)                          
declare @DB varchar(100)                          
declare @12FD varchar(10)                          
declare @3FD varchar(10)                          
declare @6FD varchar(10)                          
declare @TD varchar(10)                          
declare @NOOfCust varchar(15)                          
DECLARE @ExchangeRate varchar(15)                     
DECLARE @CurrenyRate varchar(15)                       
                          
set @DB=  @DBNAME                          
--set @FD = select CONVERT(VARCHAR(10), DateAdd(mm, -12, GetDate()) , 120)                          
                          
                          
set @3FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 3, 0) , 120)   --First day of previous 3 month                          
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)   --First day of previous 6 month                          
set @12FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                          
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)    ---Last day of previous month                          
                 
--if(@IncludeSizeFinish = '0')            
--Begin            
--exec [sp_itech_MonthlyInventoryReportSUMMARY_NO_SizeFnsh] @DBNAME,  @InventoryStatus, @IncludePierce, @IncludeInterco, @CurrencyInUS            
--return;            
--End            
CREATE TABLE #tmpSummaryOrder (   [Database]   VARCHAR(10)                          
        , Form     Varchar(65)                          
        , Grade     Varchar(65)                          
        , Size     Varchar(65)              
        , Finish    Varchar(65)                
        , Whs Varchar(10)                        
        , SummaryOrder   DECIMAL(20, 2)        
        )       
CREATE TABLE #tmpBacklogOrder (   [Database]   VARCHAR(10)                          
        , Form     Varchar(65)                          
        , Grade     Varchar(65)                          
        , Size     Varchar(65)              
        , Finish    Varchar(65)                
        , Whs Varchar(10)                        
        , BacklogOrder   DECIMAL(20, 2)        
        )      
                                          
CREATE TABLE #tmp (   [Database]   VARCHAR(10)                          
        , Form     Varchar(65)                          
        , Grade     Varchar(65)                          
        , Size     Varchar(65)              
        , SizeDesc     Varchar(65)                        
        , Finish    Varchar(65)                
        , Whs Varchar(10)                        
        , Months3InvoicedWeight   DECIMAL(20, 2)                          
        , Months6InvoicedWeight   DECIMAL(20, 2)                          
        , Months12InvoicedWeight   DECIMAL(20, 2)                          
        , OpenSOWgt DECIMAL(20, 2)                          
        , OpenPOWgt DECIMAL(20, 2)                          
                     , Avg3Month     DECIMAL(20, 2)                          
          ,OhdStock DECIMAL(20, 2)                          
                     ,OhdStockCost DECIMAL(20, 2)                          
              ,ReplCost DECIMAL(20, 2)                            
                     ,CustID Varchar(max)                         
                     ,MktSeg Varchar(max)             
                     , Months3Sales   DECIMAL(20, 2)                
                     , COGS   DECIMAL(20, 2)             
                     , ReservedWeight Decimal (20,2)             
                     ,AvgAge int           
                     , SummaryOrders Decimal (20,2)           
                     , BacklogOrders Decimal (20,2)                       
                 );                          
                          
DECLARE @DatabaseName VARCHAR(35);                          
DECLARE @Prefix VARCHAR(35);                          
DECLARE @Name VARCHAR(15);                    
               
            
                        
                          
IF @DBNAME = 'ALL'                          
 BEGIN                          
 IF (@IncludePierce = '0')            
    BEGIN              
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName              
   OPEN ScopeCursor;              
 END              
    ELSE              
    BEGIN              
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
   OPEN ScopeCursor;              
    END                           
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                          
     WHILE @@FETCH_STATUS = 0                          
       BEGIN                          
        DECLARE @query NVARCHAR(max);                             
      SET @DB= @Prefix                   
                 
     IF (@CurrencyInUS = '0')            
     Begin            
     set @CurrenyRate = 1;            
     End            
     Else            
     Begin                
                         
      IF (UPPER(@DB) = 'TW')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
   End                            
   Else if (UPPER(@DB) = 'NO')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
   End                            
   Else if (UPPER(@DB) = 'CA')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
   End                            
   Else if (UPPER(@DB) = 'CN')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
 End                            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                            
   begin                      
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
   End                            
   Else if(UPPER(@DB) = 'UK')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
   End                 
   Else if(UPPER(@DB) = 'DE')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
   End                 
               
   End                
   SET @query ='INSERT INTO #tmpSummaryOrder ([Database],Form,Grade,Size, Finish,Whs,SummaryOrder)      
         select ''' +  @DB + ''', ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh,orh_shpg_whs,Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec         
       join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm          
       and ord_ord_pfx = ipd_ref_pfx         
       join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and             
       orh_ord_pfx = ipd_ref_pfx         
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id         
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,         
       ' + @DB + '_ortorl_rec,         
       ' + @DB + '_ortchl_rec          
       where ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
       and orl_bal_qty > 0          
       and chl_chrg_cl= ''E''         
       and ord_sts_actn=''A''         
       and ord_ord_pfx <>''QT''         
       group by orh_shpg_whs,ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh ;'      
     print @query;                          
        EXECUTE sp_executesql @query;         
              
         SET @query ='INSERT INTO #tmpBacklogOrder ([Database],Form,Grade,Size, Finish,Whs,BacklogOrder)      
         select ''' +  @DB + ''', ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh,orh_shpg_whs,Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec         
       join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm          
       and ord_ord_pfx = ipd_ref_pfx         
       join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and             
       orh_ord_pfx = ipd_ref_pfx         
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id         
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,         
       ' + @DB + '_ortorl_rec,         
       ' + @DB + '_ortchl_rec          
       where ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
       and orl_bal_qty > 0          
       and chl_chrg_cl= ''E''         
       and ord_sts_actn=''A''         
       and ord_ord_pfx <>''QT'' and orh_ord_typ <> ''J''        
       group by orh_shpg_whs,ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh ;'      
     print @query;                          
        EXECUTE sp_executesql @query;           
                          
            SET @query =' INSERT INTO #tmp ([Database],Form,Grade,Size, SizeDesc,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,         
        ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg,Months3Sales,COGS,ReservedWeight,AvgAge, SummaryOrders , BacklogOrders )         
                   select  ''' +  @DB + ''' as [Database], Form, Grade,Size, SizeDesc,Finish,Whs, Months3InvoicedWeight,      
   Months6InvoicedWeight, Months12InvoicedWeight,ReplCost* '+ @CurrenyRate +', OpenPOWgt, OpenSOWgt,  Avg3Month, OhdStock,OhdStockCost*'+ @CurrenyRate +',      
   CustID,MktSeg,Months3Sales*'+ @CurrenyRate +',COGS*'+ @CurrenyRate +',ReservedWeight,       
   (select Avg(DATEDIFF(Day,pcr_crtd_dtts, getdate())) from ' + @DB + '_intpcr_rec       
    join ' + @DB + '_intprd_rec on pcr_cmpy_id = prd_cmpy_id and pcr_itm_ctl_no = prd_itm_ctl_no       
    where prd_frm = Form and prd_Grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_whs = Whs and prd_invt_sts = ''S'' ) as AgeAvg,      
    (select t.SummaryOrder from #tmpSummaryOrder t where t.Form = t1.Form and t.Grade = t1.Grade and t.Size = t1.Size and t.Finish = t1.Finish      
    and t.Whs = t1.Whs) as SummaryOrders ,      
       (select t.BacklogOrder from #tmpBacklogOrder t where t.Form = t1.Form and t.Grade = t1.Grade and t.Size = t1.Size and t.Finish = t1.Finish      
    and t.Whs = t1.Whs) as BacklogOrders       
    from tbl_itech_MonthlyInventoryReportSUMMARYS t1 where [Database] = ''' +  @DB + ''';'                          
                                 
      print @query;                          
        EXECUTE sp_executesql @query;                    
              
        truncate table #tmpSummaryOrder;      
        truncate table #tmpBacklogOrder;      
                    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                          
       END                           
    CLOSE ScopeCursor;                          
    DEALLOCATE ScopeCursor;                          
  END                          
  ELSE                          
     BEGIN                      
                 
     IF (@CurrencyInUS = '0')            
     Begin            
     set @CurrenyRate = 1;            
     End            
     Else            
     Begin                    
    -- This is for setting currency rate by mrinal                  
   IF (UPPER(@DB) = 'TW')                            
    begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@DB) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@DB) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@DB) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                            
    Else if(UPPER(@DB) = 'UK')                            
    begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End                  
    Else if(UPPER(@DB) = 'DE')                            
    begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End                  
   -- Currency block end                  
    End                   
                         
         -- insert SummaryOrders       
         SET @sqltxt ='INSERT INTO #tmpSummaryOrder ([Database],Form,Grade,Size, Finish,Whs,SummaryOrder)      
         select ''' +  @DB + ''', ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh,orh_shpg_whs,Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec         
       join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm          
       and ord_ord_pfx = ipd_ref_pfx         
       join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and             
       orh_ord_pfx = ipd_ref_pfx         
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id         
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,         
       ' + @DB + '_ortorl_rec,         
       ' + @DB + '_ortchl_rec          
       where ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
       and orl_bal_qty > 0          
       and chl_chrg_cl= ''E''         
       and ord_sts_actn=''A''         
       and ord_ord_pfx <>''QT''         
       group by orh_shpg_whs,ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh      
         '      
         print( @sqltxt)                           
    set @execSQLtxt = @sqltxt;                           
   EXEC (@execSQLtxt);         
         
   -- insert BacklogOrders       
         SET @sqltxt ='INSERT INTO #tmpBacklogOrder ([Database],Form,Grade,Size, Finish,Whs,BacklogOrder)      
         select ''' +  @DB + ''', ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh,orh_shpg_whs,Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec         
       join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm          
       and ord_ord_pfx = ipd_ref_pfx         
       join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and             
       orh_ord_pfx = ipd_ref_pfx         
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id         
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,         
       ' + @DB + '_ortorl_rec,         
    ' + @DB + '_ortchl_rec          
       where ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
       and orl_bal_qty > 0          
       and chl_chrg_cl= ''E''         
       and ord_sts_actn=''A''         
       and ord_ord_pfx <>''QT'' and orh_ord_typ <> ''J''        
       group by orh_shpg_whs,ipd_FRM, ipd_GRD,ipd_size,ipd_fnsh      
         '      
         print( @sqltxt)                           
    set @execSQLtxt = @sqltxt;                           
   EXEC (@execSQLtxt);        
                             
        SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size, SizeDesc,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,         
        ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg,Months3Sales,COGS,ReservedWeight,AvgAge, SummaryOrders , BacklogOrders )         
                   select  ''' +  @DB + ''' as [Database], Form, Grade,Size, SizeDesc,Finish,Whs, Months3InvoicedWeight,      
   Months6InvoicedWeight, Months12InvoicedWeight,ReplCost* '+ @CurrenyRate +', OpenPOWgt, OpenSOWgt,  Avg3Month, OhdStock,OhdStockCost*'+ @CurrenyRate +',      
   CustID,MktSeg,Months3Sales*'+ @CurrenyRate +',COGS*'+ @CurrenyRate +',ReservedWeight,       
   (select Avg(DATEDIFF(Day,pcr_crtd_dtts, getdate())) from ' + @DB + '_intpcr_rec       
    join ' + @DB + '_intprd_rec on pcr_cmpy_id = prd_cmpy_id and pcr_itm_ctl_no = prd_itm_ctl_no       
    where prd_frm = Form and prd_Grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_whs = Whs and prd_invt_sts = ''S'' ) as AgeAvg,      
    (select t.SummaryOrder from #tmpSummaryOrder t where t.Form = t1.Form and t.Grade = t1.Grade and t.Size = t1.Size and t.Finish = t1.Finish      
    and t.Whs = t1.Whs) as SummaryOrders ,      
       (select t.BacklogOrder from #tmpBacklogOrder t where t.Form = t1.Form and t.Grade = t1.Grade and t.Size = t1.Size and t.Finish = t1.Finish      
    and t.Whs = t1.Whs) as BacklogOrders       
    from tbl_itech_MonthlyInventoryReportSUMMARYS t1 where [Database] = ''' +  @DB + ''' '          
                                    
     print( @sqltxt)                           
    set @execSQLtxt = @sqltxt;                           
   EXEC (@execSQLtxt);                          
   END                          
                               
if(@IncludeSizeFinish = '0')      
begin     
    Select  [Database], Product,Form,Grade, 0 as Size, 0 as  Finish, Whs, MatGroup, SUM(ISNULL( Months3InvoicedWeight,0)) as Months3InvoicedWeight,      
	SUM(ISNULL(Months6InvoicedWeight,0)) as Months6InvoicedWeight, SUM(ISNULL(Months12InvoicedWeight,0)) as Months12InvoicedWeight,      
	SUM(ISNULL(ReplCost,0)) as ReplCost,  SUM(ISNULL(OpenPOWgt,0)) as OpenPOWgt,  SUM(ISNULL(OpenSOWgt,0)) as OpenSOWgt,      
	SUM(ISNULL(Avg3Month,0)) as Avg3Month, SUM(ISNULL(POMOSupply,0)) as POMOSupply,SUM(ISNULL(StockMOSupply,0)) as StockMOSupply,      
	SUM(ISNULL(OhdStock,0)) as OhdStock,SUM(ISNULL(OhdStockCost,0)) as OhdStockCost , SUM(ISNULL(InStockCostWgt,0)) as InStockCostWgt, '' as CustID,                 
	'' as MktSeg, SUM(ISNULL(Excess,0)) as Excess,SUM(ISNULL(Excess1500,0)) as Excess1500,SUM(ISNULL(Excess2500,0)) as Excess2500,      
	SUM(ISNULL(ExcessValue,0)) as ExcessValue ,Product as  ProductWithSize, SUM(ISNULL(Months3Sales,0)) as Months3Sales, SUM(ISNULL(COGS,0)) as COGS ,      
	SUM(ISNULL(ReservedWeight,0)) as ReservedWeight, max(ISNULL(AvgAge,0)) as AvgAge ,SUM(ISNULL(SummaryOrders,0)) as SummaryOrders,     
	SUM(ISNULL(BacklogOrders,0)) as BacklogOrders  from (     
                                 
	select [Database],RTrim(Ltrim(Form)) + '/'+ RTRIM(Ltrim(Grade)) + '/' + RTRIM(LTRIM(SizeDesc)) + '/' + RTRIM(LTRIM(finish)) as 'Product',Form,Grade, Size, Finish, Whs,                        
	CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,                          
	Months3InvoicedWeight,                          
	Months6InvoicedWeight,                          
	 Months12InvoicedWeight,                         
	 ReplCost,                         
	 OpenPOWgt,                          
	 OpenSOWgt,                          
	 Avg3Month,                           
	 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',                          
	 cast(OhdStock/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',                           
	 OhdStock,                           
	 OhdStockCost,                           
	 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt' ,                        
	 CustID ,                        
	 MktSeg,                      
	 (OhdStock - ISNULL(Months6InvoicedWeight, 0)) AS Excess,                      
	 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 1500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess1500,                          
	 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 2500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess2500,                      
	 Ltrim(Form) + '/'+ Ltrim(Grade) + ' ' + Size   as ProductWithSize,            
	CONVERT (DECIMAL(10,2), ((CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END)  * (OhdStock - ISNULL(Months6InvoicedWeight, 0)))) AS ExcessValue              
	  ,Months3Sales  , COGS,ReservedWeight ,AvgAge ,SummaryOrders, BacklogOrders          
	 from #tmp    
	 WHERE  (                          
	(@InventoryStatus = 'False')OR                          
	(@InventoryStatus = 'True'  and OhdStock>0)                          
	)     
	 ) as t group by [Database], Product, Whs, MatGroup                           
	 order by Product ;                         
      
End    
else    
Begin    
                            
	select [Database],RTrim(Ltrim(Form)) + '/'+ RTRIM(Ltrim(Grade)) + '/' + RTRIM(LTRIM(SizeDesc)) + '/' + RTRIM(LTRIM(finish)) as 'Product',Form,Grade, Size, Finish, Whs,                        
	CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,                          
	 Months3InvoicedWeight,                          
	 Months6InvoicedWeight,                          
	 Months12InvoicedWeight,                         
	 ReplCost,                         
	 OpenPOWgt,                          
	 OpenSOWgt,                          
	 Avg3Month,                           
	 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',                          
	 cast(OhdStock/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',                           
	 OhdStock,                           
	OhdStockCost,                           
	 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt' ,                        
	 CustID ,                        
	 MktSeg,                      
	 (OhdStock - ISNULL(Months6InvoicedWeight, 0)) AS Excess,                      
	 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 1500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess1500,                          
	 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 2500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess2500,                      
	 Ltrim(Form) + '/'+ Ltrim(Grade) + ' ' + Size   as ProductWithSize,            
	CONVERT (DECIMAL(10,2), ((CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END)  * (OhdStock - ISNULL(Months6InvoicedWeight, 0)))) AS ExcessValue              
	  ,Months3Sales  , COGS,ReservedWeight ,AvgAge ,SummaryOrders, BacklogOrders          
	 from #tmp    
	 WHERE  (                          
	(@InventoryStatus = 'False')OR                          
	(@InventoryStatus = 'True'  and OhdStock>0)                          
	)                           
	 order by Product ;     
End      
    
drop table #tmpSummaryOrder;      
drop table #tmpBacklogOrder;                                           
   drop table #tmp                          
END                          
--exec [sp_itech_InventoryExcess] 'US',  'False'  ,'0','0','1' -- 3911,3927         
-- exec sp_itech_MonthlyInventoryReportSUMMARYSizeFnsh_Avg 'UK',  'TRUE'      
-- exec sp_itech_MonthlyInventoryReportSUMMARYSizeFnsh_Avg  'CA',  'TRUE'          
--exec [sp_itech_InventoryExcess] 'UK',  'True'     -- 00:00:07        
--exec [sp_itech_InventoryExcess] 'CA',  'True'     -- 00:00:03   -- 267       
--exec [sp_itech_InventoryExcess] 'CN',  'True'     -- 00:00:17        
--exec [sp_itech_InventoryExcess] 'TW',  'True'     -- 00:00:16        
--exec [sp_itech_InventoryExcess] 'US',  'True'     -- 00:00:16                   
--exec [sp_itech_InventoryExcess] 'ALL',  'True'                          
/*            
Date:20200407        
Mail sub:Inventory Excess Report 
Date: 20200824
select Form and Grade
Mail: RE: Excess
*/


GO
