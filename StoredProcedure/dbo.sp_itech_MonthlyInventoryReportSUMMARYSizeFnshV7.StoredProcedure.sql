USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReportSUMMARYSizeFnshV7]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  <Mukesh >                
-- Create date: <13 Jan 2017>                
             
              
-- =============================================                
CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReportSUMMARYSizeFnshV7] @DBNAME varchar(50), @InventoryStatus varchar(10),@IncludePierce char = '0', @IncludeInterco char = '0',  @IncludeSizeFinish Char = '1',  @CurrencyInUS Char = '1'               
                
AS                
BEGIN                
         
                 
 SET NOCOUNT ON;                
declare @sqltxt Varchar(8000) 
declare @execSQLtxt Varchar(8000)
declare @DB varchar(100)                
declare @12FD varchar(10)                
declare @3FD varchar(10)                
declare @6FD varchar(10)                
declare @TD varchar(10)                
declare @NOOfCust varchar(15)                
DECLARE @ExchangeRate varchar(15)           
DECLARE @CurrenyRate varchar(15)             
                
set @DB=  @DBNAME                
--set @FD = select CONVERT(VARCHAR(10), DateAdd(mm, -12, GetDate()) , 120)                
                
                
set @3FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 3, 0) , 120)   --First day of previous 3 month                
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)   --First day of previous 6 month                
set @12FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)    ---Last day of previous month                
       
if(@IncludeSizeFinish = '0')  
Begin  
exec [sp_itech_MonthlyInventoryReportSUMMARY_NO_SizeFnshV6] @DBNAME,  @InventoryStatus, @IncludePierce, @IncludeInterco, @CurrencyInUS  
return;  
End  
                
CREATE TABLE #tmp (   [Database]   VARCHAR(10)                
        , Form     Varchar(65)                
        , Grade     Varchar(65)                
        , Size     Varchar(65)                
        , Finish    Varchar(65)      
        , Whs Varchar(10)              
        , Months3InvoicedWeight   DECIMAL(20, 2)                
        , Months6InvoicedWeight   DECIMAL(20, 2)                
        , Months12InvoicedWeight   DECIMAL(20, 2)                
        , OpenSOWgt DECIMAL(20, 2)                
        , OpenPOWgt DECIMAL(20, 2)                
                     , Avg3Month     DECIMAL(20, 2)                
                     ,OhdStock DECIMAL(20, 2)                
                     ,OhdStockCost DECIMAL(20, 2)                
                     ,ReplCost DECIMAL(20, 2)                  
                     ,CustID Varchar(max)               
                     ,MktSeg Varchar(max)   
                     , Months3Sales   DECIMAL(20, 2)      
                     , COGS   DECIMAL(20, 2)   
                     , ReservedWeight Decimal (20,2)  
                     ,TotalPOCost  Decimal (20,2)           
                 );                 
                 
CREATE TABLE #tmp1 (   [Database]   VARCHAR(10)                
        , Form     Varchar(65)                
        , Grade     Varchar(65)                
        , Size     Varchar(65)                
        , Finish    Varchar(65)      
        , Whs Varchar(10)              
        , Months3InvoicedWeight   DECIMAL(20, 2)                
        , Months6InvoicedWeight   DECIMAL(20, 2)                
        , Months12InvoicedWeight   DECIMAL(20, 2)                
        , OpenSOWgt DECIMAL(20, 2)                
        , OpenPOWgt DECIMAL(20, 2)                
                     , Avg3Month     DECIMAL(20, 2)                
                     ,OhdStock DECIMAL(20, 2)                
                     ,OhdStockCost DECIMAL(20, 2)                
                     ,ReplCost DECIMAL(20, 2)                  
                     ,CustID Varchar(max)               
                     ,MktSeg Varchar(max)   
                     , Months3Sales   DECIMAL(20, 2)      
                     , COGS   DECIMAL(20, 2)   
                     , ReservedWeight Decimal (20,2)  
                     ,TotalPOCost  Decimal (20,2)           
                 );                 
                 
CREATE TABLE #tmp2 (   [Database]   VARCHAR(10)                
        , Form     Varchar(65)                
        , Grade     Varchar(65)                
        , Size     Varchar(65)                
        , Finish    Varchar(65)      
        , Whs Varchar(10)              
        , Months3InvoicedWeight   DECIMAL(20, 2)                
        , Months6InvoicedWeight   DECIMAL(20, 2)                
        , Months12InvoicedWeight   DECIMAL(20, 2)                
        , OpenSOWgt DECIMAL(20, 2)                
        , OpenPOWgt DECIMAL(20, 2)                
                     , Avg3Month     DECIMAL(20, 2)                
                     ,OhdStock DECIMAL(20, 2)                
                     ,OhdStockCost DECIMAL(20, 2)                
                     ,ReplCost DECIMAL(20, 2)                  
                     ,CustID Varchar(max)               
                     ,MktSeg Varchar(max)   
                     , Months3Sales   DECIMAL(20, 2)      
                     , COGS   DECIMAL(20, 2)   
                     , ReservedWeight Decimal (20,2)  
                     ,TotalPOCost  Decimal (20,2)           
                 );                 
                
DECLARE @DatabaseName VARCHAR(35);                
DECLARE @Prefix VARCHAR(35);                
DECLARE @Name VARCHAR(15);          
     
  
              
                
IF @DBNAME = 'ALL'                
 BEGIN                
 IF (@IncludePierce = '0')  
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
   OPEN ScopeCursor;    
    END    
    ELSE    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    END                 
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
     WHILE @@FETCH_STATUS = 0                
       BEGIN                
        DECLARE @query NVARCHAR(max);                   
      SET @DB= @Prefix         
       
     IF (@CurrencyInUS = '0')  
     Begin  
     set @CurrenyRate = 1;  
     End  
     Else  
     Begin      
               
      IF (UPPER(@DB) = 'TW')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
   End                  
   Else if (UPPER(@DB) = 'NO')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
   End                  
   Else if (UPPER(@DB) = 'CA')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
   End                  
   Else if (UPPER(@DB) = 'CN')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
 End                  
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
   begin            
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
   End                  
   Else if(UPPER(@DB) = 'UK')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
   End       
   Else if(UPPER(@DB) = 'DE')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
   End       
     
   End              
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
        Begin          
               
                
                      
           SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,
           ReservedWeight,TotalPOCost )                
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh, prd_whs,               
                       
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs              
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs              
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs              
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                
                     
       (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +') end) as replacementCost    
       from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh  order by  ppb_rct_expy_dt desc         
        ) as MonthlyAvgReplCost,              
                       
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
                       
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                 
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                 
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no   
       and orh_shpg_whs = prd_whs and                 
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                 
       where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,                
                       
       (Select sum(SAT_BLG_WGT * 2.20462)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh              
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                
       sum(prd_ohd_wgt * 2.20462)   ,                
                     
      isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +')),   
         
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )               
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                
        FOR XML PATH('''')),1,1,'''')) AS custIDs,              
                      
             (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )               
    FROM ' + @DB + '_sahsat_rec                
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
         where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh               
        FOR XML PATH('''')),1,1,'''')) AS MktSeg   ,  
        (Select SUM(ISNULL(SAT_tot_val,0)) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales ,  
           
        (select Sum(ISNULL(csi_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_cttcsi_rec  
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm  
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg  ,  
         
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )* 2.20462) as reserveWgt,
       
       (select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost    
                       
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and  prd_fnsh = prm_fnsh                
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
        where  prd_invt_sts = ''S''                
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs
       
       Union 

SElect ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,                
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
0  as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt , 0 as TotalPOCost
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool    
       where  prd_invt_sts != ''S''   
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)   
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs ;'                
        End                
        Else                
        Begin                
            SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,
            ReservedWeight,TotalPOCost )                
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,  prd_whs,           
                       
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                
                 
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                
                     
       (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +') end) as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh            
         order by  ppb_rct_expy_dt desc  ) as MonthlyAvgReplCost,             
                       
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
                       
       (select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec             
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                 
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no   
       and orh_shpg_whs = prd_whs and                 
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                 
       where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh )  as OpenSOWgt,                
                       
       (Select sum(SAT_BLG_WGT)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and  prm_size = sat_size and prm_fnsh = sat_fnsh              
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                
       sum(prd_ohd_wgt)   ,                 
         
   --SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,1)) / acp_tot_qty) * '+ @CurrenyRate +'),    
   isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +')),   
     
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )               
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                
        FOR XML PATH('''')),1,1,'''')) AS custIDs,              
                      
             (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )               
    FROM ' + @DB + '_sahsat_rec                
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
         where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh               
        FOR XML PATH('''')),1,1,'''')) AS MktSeg  ,  
          
        (Select SUM(ISNULL(SAT_tot_val,0)) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales  ,  
           
        (select Sum(ISNULL(csi_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_cttcsi_rec  
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm  
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg  ,  
         
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as reserveWgt ,
         
       (select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
		and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost    
                       
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size    and prd_fnsh = prm_fnsh                
         left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
         where prd_invt_sts = ''S''               
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs
       
       Union 

SElect ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,                
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
0  as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt , 0 as TotalPOCost
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool    
       where  prd_invt_sts != ''S''   
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)   
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs ;'                
        End                
                       
      print @query;                
        EXECUTE sp_executesql @query;                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
       END                 
    CLOSE ScopeCursor;                
    DEALLOCATE ScopeCursor;                
  END                
  ELSE                
     BEGIN            
       
     IF (@CurrencyInUS = '0')  
     Begin  
     set @CurrenyRate = 1;  
     End  
     Else  
     Begin          
    -- This is for setting currency rate by mrinal        
   IF (UPPER(@DB) = 'TW')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@DB) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@DB) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@DB) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@DB) = 'UK')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End        
    Else if(UPPER(@DB) = 'DE')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End        
   -- Currency block end        
    End         
               
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
   Begin         
           
          
                  
       SET @sqltxt ='INSERT INTO #tmp1 ([Database],Form,Grade,Size, Finish,Whs, Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,
       ReservedWeight, TotalPOCost )                
   select ''' +  @DB + '''  as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs,                 
                       
       (Select  SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh   and sat_shpg_whs = prd_whs           
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs            
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                
                     
       (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +') end)   
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh      order by  ppb_rct_expy_dt desc         
      ) as MonthlyAvgReplCost,  
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
       
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                 
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                 
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and orh_shpg_whs = prd_whs  and                 
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                 
       where  ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,    
       (Select sum(SAT_BLG_WGT * 2.20462)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh              
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                
       sum(prd_ohd_wgt * 2.20462)   ,  
  isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +')),   
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )               
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                
        FOR XML PATH('''')),1,1,'''')) AS custIDs,  
    (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )               
    FROM ' + @DB + '_sahsat_rec                
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
         where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh               
        FOR XML PATH('''')),1,1,'''')) AS MktSeg , 
        (Select SUM(ISNULL(SAT_tot_val,0)) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales , 
        (select Sum(ISNULL(csi_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_cttcsi_rec  
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm  
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg   ,  
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt ))* 2.20462) as reserveWgt ,
       (select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
      and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost    
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                       
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
        Where  prd_invt_sts = ''S''                 
       group by PRm_FRM, Prm_GRD,PRm_size, PRm_fnsh, prd_whs '
       print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt); 
       
 SET @sqltxt ='INSERT INTO #tmp2 ([Database],Form,Grade,Size, Finish,Whs, Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,
       ReservedWeight, TotalPOCost ) 
       
SElect ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,                
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,
0  as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt , 
(select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
      and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost    
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool    
       where  prd_invt_sts != ''S''   
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)   
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs ' 
       print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt); 
                  
   End                
   Else                
   Begin        
             
                   
        SET @sqltxt ='INSERT INTO #tmp1 ([Database],Form,Grade,Size,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg,Months3Sales,COGS,
        ReservedWeight,TotalPOCost)                
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs ,               
                       
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs            
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
                       
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs            
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                
                     
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst + ppb_frt_in_cst)  * '+ @CurrenyRate +') end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  PRm_FRM
         and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh       order by  ppb_rct_expy_dt desc        
      ) as MonthlyAvgReplCost,             
                      
                       
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,                
                       
       (select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec                 
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                 
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                 
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''  and orh_shpg_whs = prd_whs    
       where ipd_FRM=PRm_FRM              
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt, 
        (Select sum(SAT_BLG_WGT)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh               
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                
       sum(prd_ohd_wgt)   ,                
                    
 isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +' )),   
   
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )               
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                
        FOR XML PATH('''')),1,1,'''')) AS custIDs,  
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )               
    FROM ' + @DB + '_sahsat_rec                
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
         where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh               
        FOR XML PATH('''')),1,1,'''')) AS MktSeg ,  
        (Select SUM(ISNULL(SAT_tot_val,0))* '+ @CurrenyRate + ' from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales,  
        (select Sum(ISNULL(csi_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_cttcsi_rec  
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm  
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs             
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg ,
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as reserveWgt ,
       (select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0
       where ipd_FRM=PRm_FRM   and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in 
       (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost    
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool    
       where  prd_invt_sts = ''S''              
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs  '
       
print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt);  
   
SET @sqltxt ='INSERT INTO #tmp2 ([Database],Form,Grade,Size,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost, CustID,MktSeg,Months3Sales,COGS,
        ReservedWeight,TotalPOCost)        
SElect ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,                
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 
(select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm 
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,                
0  as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt ,
(select sum(csi_bas_cry_val)* '+ @CurrenyRate +' from ' + @DB + '_tctipd_rec 
       join ' + @DB + '_cttcsi_rec on csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm 
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm 
        and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0
       where ipd_FRM=PRm_FRM   and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in 
       (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool    
       where  prd_invt_sts != ''S''   
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)   
       group by PRm_FRM, PRm_GRD,PRm_size, PRm_fnsh, prd_whs  
       ;'   
       print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt); 
                
   End                
                          
                    
   END                
       insert into #tmp 
       SElect * from #tmp1
       Union
       SElect * from #tmp2;         
                   
select [Database],Ltrim(Form) + '/'+ Ltrim(Grade) as 'Product', Size, Finish, Whs,              
--select [Database],Ltrim(Form) + '/'+ Ltrim(Grade)   as 'Product', Size, Finish,               
CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,                
 Months3InvoicedWeight,                
 Months6InvoicedWeight,                
 Months12InvoicedWeight,               
 ReplCost,               
 OpenPOWgt,                
 OpenSOWgt,                
 Avg3Month,                 
 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',                
 --cast(OpenSOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',                 
 cast(OhdStock/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',                 
 OhdStock,                 
 OhdStockCost,                 
 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt' ,              
 CustID ,              
 MktSeg,            
 --(Months12InvoicedWeight * 6) AS Months6Weight,            
 (OhdStock - ISNULL(Months6InvoicedWeight, 0)) AS Excess,            
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 1500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess1500,                
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 2500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess2500,            
 Ltrim(Form) + '/'+ Ltrim(Grade) + ' ' + Size   as ProductWithSize,  
   
CONVERT (DECIMAL(10,2), ((CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END)  * (OhdStock - ISNULL(Months6InvoicedWeight, 0)))) AS ExcessValue    
  ,Months3Sales  , COGS,ReservedWeight ,TotalPOCost  
 from #tmp                 
 WHERE  (                
(@InventoryStatus = 'False')OR                
(@InventoryStatus = 'True'  )                -- and OhdStock>0
)                 
--and Form = 'TIRC' and Grade = '64'  and Size = '2X3'              
 order by Product                
                                 
   drop table #tmp  ;   
   drop table #tmp1; 
	drop table #tmp2; 
END                
   -- =IIF(Fields!OhdStock.Value>0,Fields!OhdStockCost.Value/Fields!OhdStock.Value,0)      
   --SELECT CONVERT(DECIMAL(10,2),10000)   
   --, CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END as stk              
--exec [sp_itech_MonthlyInventoryReportSUMMARYSizeFnshV7] 'US',  'False'  ,'0','0','1' -- 3911,3927  
--exec [sp_itech_MonthlyInventoryReportSUMMARYSizeFnshV7] 'US',  'True'                
              
--10\20DIN912    /  
/*  
-- 2016-08-02  
 Mail:inventory summary 3  
 We currently have the available total on the report weight we would like to add a column stating the total available basically the total weigh minus any reserved stock  
   
-- 2016-05-13  
Please modify the version 4 - inventory report summary and add:  
1) Sales Dollars on 3 months of sales  
2) COGS only on material 


-- 2017-01-31
Mail sub: inventory Summary Report


  
*/

GO
