USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ipjtrh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_ipjtrh]  
   
AS  
BEGIN  
 -- SET USCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.DE_ipjtrh_rec', 'U') IS NOT NULL          
  drop table dbo.DE_ipjtrh_rec;  
  
    -- Insert statements for procedure here  
SELECT *  
into  dbo.DE_ipjtrh_rec 
FROM [LIVEDESTX].[livedestxdb].[informix].[ipjtrh_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
