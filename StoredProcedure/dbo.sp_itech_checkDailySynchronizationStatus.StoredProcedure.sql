USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_checkDailySynchronizationStatus]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <mukesh >              
-- Create date: <11 Feb 2013>              
-- Description: <Getting top 50 customers for SSRS reports>              
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_checkDailySynchronizationStatus]  @DBNAME varchar(50)              
              
AS              
BEGIN               
 SET NOCOUNT ON;              
declare @sqltxt varchar(Max)              
declare @execSQLtxt varchar(Max)              
declare @DB varchar(100)              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(5);              
              
CREATE TABLE #tmp (  Prefix varchar(150)               
        ,tablename Varchar(150)              
        ,localcount Decimal(20,0)              
        ,livecount Decimal(20,0)              
        )              
SET @DB  = @DBNAME;               
              
              
                    
              
IF @DBNAME = 'UK'              
 BEGIN                
  INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','sahsat',(Select Count(*) from UK_sahsat_rec),(Select Count(*) from[LIVEUK_IW].[liveukstxdb_iw].[informix].[sahsat_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','plswrw',(Select Count(*) from UK_plswrw_rec),(Select Count(*) from[LIVEUK_IW].[liveukstxdb_iw].[informix].[plswrw_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','sasmsu',(Select Count(*) from UK_sasmsu_rec),(Select Count(*) from[LIVEUK_IW].[liveukstxdb_iw].[informix].[sasmsu_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','glbacb',(Select Count(*) from UK_glbacb_rec),(Select Count(*) from[LIVEUKGL].[liveukgldb].[informix].glbacb_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','gltoje',(Select Count(*) from UK_gltoje_rec),(Select Count(*) from[LIVEUKGL].[liveukgldb].[informix].gltoje_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','glbbud',(Select Count(*) from UK_glbbud_rec),(Select Count(*) from[LIVEUKGL].[liveukgldb].[informix].glbbud_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','glhgld',(Select Count(*) from UK_glhgld_rec),(Select Count(*) from[LIVEUKGL].[liveukgldb].[informix].glhgld_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','gltojd',(Select Count(*) from UK_gltojd_rec),(Select Count(*) from[LIVEUKGL].[liveukgldb].[informix].gltojd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','glrbga',(Select Count(*) from UK_glrbga_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].glrbga_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrslp',(Select Count(*) from UK_scrslp_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrslp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','aptvch',(Select Count(*) from UK_aptvch_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].aptvch_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','tctipd',(Select Count(*) from UK_tctipd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].tctipd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','cttcsi',(Select Count(*) from UK_cttcsi_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].cttcsi_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','trtsrd',(Select Count(*) from UK_trtsrd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].trtsrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','trjtph',(Select Count(*) from UK_trjtph_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].trjtph_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','arrstx',(Select Count(*) from UK_arrstx_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].arrstx_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','trjtud',(Select Count(*) from UK_trjtud_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].trjtud_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','trttrn',(Select Count(*) from UK_trttrn_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].trttrn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','tcttsh',(Select Count(*) from UK_tcttsh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].tcttsh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjvd',(Select Count(*) from UK_ivjjvd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjvd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjvs',(Select Count(*) from UK_ivjjvs_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjvs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','mcrmss',(Select Count(*) from UK_mcrmss_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].mcrmss_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortcht',(Select Count(*) from UK_ortcht_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortcht_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','cctcta',(Select Count(*) from UK_cctcta_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].cctcta_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','plrrpd',(Select Count(*) from UK_plrrpd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].plrrpd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','injitv',(Select Count(*) from UK_injitv_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].injitv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','artrvh',(Select Count(*) from UK_artrvh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].artrvh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','injitd',(Select Count(*) from UK_injitd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].injitd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjct',(Select Count(*) from UK_ivjjct_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjct_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','injtdc',(Select Count(*) from UK_injtdc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].injtdc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ipjtrt',(Select Count(*) from UK_ipjtrt_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ipjtrt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','iprpwc',(Select Count(*) from UK_iprpwc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].iprpwc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','potpoh',(Select Count(*) from UK_potpoh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].potpoh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ipjsoh',(Select Count(*) from UK_ipjsoh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ipjsoh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrbrh',(Select Count(*) from UK_scrbrh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrbrh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjit',(Select Count(*) from UK_ivjjit_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','cctatv',(Select Count(*) from UK_cctatv_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].cctatv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','pntssp',(Select Count(*) from UK_pntssp_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].pntssp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','pohpra',(Select Count(*) from UK_pohpra_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].pohpra_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortxro',(Select Count(*) from UK_ortxro_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortxro_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','pohrhr',(Select Count(*) from UK_pohrhr_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].pohrhr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','aprshf',(Select Count(*) from UK_aprshf_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].aprshf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrpti',(Select Count(*) from UK_inrpti_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrpti_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','aptpyh',(Select Count(*) from UK_aptpyh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].aptpyh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','sctpal',(Select Count(*) from UK_sctpal_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].sctpal_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortbka',(Select Count(*) from UK_ortbka_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortbka_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','mxtarh',(Select Count(*) from UK_mxtarh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].mxtarh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','intprd',(Select Count(*) from UK_intprd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].intprd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','aprven',(Select Count(*) from UK_aprven_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].aprven_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','arbcoc',(Select Count(*) from UK_arbcoc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].arbcoc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','arrcrd',(Select Count(*) from UK_arrcrd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].arrcrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','arrcuc',(Select Count(*) from UK_arrcuc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].arrcuc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','arrshp',(Select Count(*) from UK_arrshp_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].arrshp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','frtfrt',(Select Count(*) from UK_frtfrt_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].frtfrt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ipjtrh',(Select Count(*) from UK_ipjtrh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ipjtrh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrprm',(Select Count(*) from UK_inrprm_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrprm_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','intacp',(Select Count(*) from UK_intacp_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].intacp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','injith',(Select Count(*) from UK_injith_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].injith_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrwhs',(Select Count(*) from UK_scrwhs_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrwhs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjvh',(Select Count(*) from UK_ivjjvh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjvh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','mxrusr',(Select Count(*) from UK_mxrusr_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].mxrusr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortbki',(Select Count(*) from UK_ortbki_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortbki_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','apjglj',(Select Count(*) from UK_apjglj_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].apjglj_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','nctncf',(Select Count(*) from UK_nctncf_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].nctncf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ccratp',(Select Count(*) from UK_ccratp_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ccratp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','pohphr',(Select Count(*) from UK_pohphr_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].pohphr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrfrm',(Select Count(*) from UK_inrfrm_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrfrm_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortoit',(Select Count(*) from UK_ortoit_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortoit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivtivs',(Select Count(*) from UK_ivtivs_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivtivs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivjjpd',(Select Count(*) from UK_ivjjpd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivjjpd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','apjjvc',(Select Count(*) from UK_apjjvc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].apjjvc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','iptrps',(Select Count(*) from UK_iptrps_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].iptrps_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrcrx',(Select Count(*) from UK_scrcrx_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrcrx_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','cctcay',(Select Count(*) from UK_cctcay_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].cctcay_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT 'UK','iptjbs',(Select Count(*) from UK_iptjbs_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].iptjbs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrgrd',(Select Count(*) from UK_inrgrd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrgrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','iptjso',(Select Count(*) from UK_iptjso_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].iptjso_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','rvtres',(Select Count(*) from UK_rvtres_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].rvtres_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ipjssh',(Select Count(*) from UK_ipjssh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ipjssh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','potpoi',(Select Count(*) from UK_potpoi_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].potpoi_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortchl',(Select Count(*) from UK_ortchl_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortchl_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','cctctk',(Select Count(*) from UK_cctctk_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].cctctk_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortmbk',(Select Count(*) from UK_ortmbk_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortmbk_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrmat',(Select Count(*) from UK_inrmat_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrmat_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortord',(Select Count(*) from UK_ortord_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortord_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','orrprs',(Select Count(*) from UK_orrprs_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].orrprs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortorh',(Select Count(*) from UK_ortorh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortorh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrloc',(Select Count(*) from UK_inrloc_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrloc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortorl',(Select Count(*) from UK_ortorl_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortorl_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrmpt',(Select Count(*) from UK_scrmpt_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrmpt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','intpcr',(Select Count(*) from UK_intpcr_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].intpcr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','pfhdpf',(Select Count(*) from UK_pfhdpf_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].pfhdpf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrpyt',(Select Count(*) from UK_scrpyt_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrpyt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','potpod',(Select Count(*) from UK_potpod_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].potpod_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivtivd',(Select Count(*) from UK_ivtivd_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivtivd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrrsn',(Select Count(*) from UK_scrrsn_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrrsn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ortxre',(Select Count(*) from UK_ortxre_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ortxre_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','rprcds',(Select Count(*) from UK_rprcds_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].rprcds_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','sahstn',(Select Count(*) from UK_sahstn_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].sahstn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','ivtivh',(Select Count(*) from UK_ivtivh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].ivtivh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrcva',(Select Count(*) from UK_scrcva_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrcva_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrcvt',(Select Count(*) from UK_scrcvt_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrcvt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','nctnit',(Select Count(*) from UK_nctnit_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].nctnit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','nctnhh',(Select Count(*) from UK_nctnhh_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].nctnhh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','xcrixv',(Select Count(*) from UK_xcrixv_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].xcrixv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','perppb',(Select Count(*) from UK_perppb_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].perppb_rec) ;                
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','xctava',(Select Count(*) from UK_xctava_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].xctava_rec) ;                
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','inrmil',(Select Count(*) from UK_inrmil_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].inrmil_rec) ;                
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','potpor',(Select Count(*) from UK_potpor_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].potpor_rec) ;                       
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)  SELECT  'UK','scrspv',(Select Count(*) from UK_scrspv_rec),(Select Count(*) from[LIVEUKSTX].[liveukstxdb].[informix].scrspv_rec) ;                       
  END              
  ELSE IF @DBNAME = 'US'              
     BEGIN                  
     -- To fix daily process              
    -- Exec US_plswrw;              
    -- exec us_ortchl;              
                    
--     INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)              
 INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','sahsat',(Select Count(*) from US_sahsat_rec),(Select Count(*) from [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] ) ;              
 INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','plswrw',(Select Count(*) from US_plswrw_rec),(Select Count(*) from [LIVEUS_IW].[liveusstxdb_iw].[informix].[plswrw_rec] ) ;              
 INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','sasmsu',(Select Count(*) from US_sasmsu_rec),(Select Count(*) from [LIVEUS_IW].[liveusstxdb_iw].[informix].[sasmsu_rec] ) ;              
               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','glbacb',(Select Count(*) from US_glbacb_rec),(Select Count(*) from [LIVEUSGL].[liveusgldb].[informix].glbacb_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','gltoje',(Select Count(*) from US_gltoje_rec),(Select Count(*) from [LIVEUSGL].[liveusgldb].[informix].gltoje_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','glbbud',(Select Count(*) from US_glbbud_rec),(Select Count(*) from [LIVEUSGL].[liveusgldb].[informix].glbbud_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','glhgld',(Select Count(*) from US_glhgld_rec),(Select Count(*) from [LIVEUSGL].[liveusgldb].[informix].glhgld_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','gltojd',(Select Count(*) from US_gltojd_rec),(Select Count(*) from [LIVEUSGL].[liveusgldb].[informix].gltojd_rec) ;              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','glrbga',(Select Count(*) from US_glrbga_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].glrbga_rec) ;                   
                   
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrslp',(Select Count(*) from   US_scrslp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrslp_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','aptvch',(Select Count(*) from US_aptvch_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].aptvch_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','tctipd',(Select Count(*) from US_tctipd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].tctipd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cttcsi',(Select Count(*) from US_cttcsi_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cttcsi_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','trtsrd',(Select Count(*) from US_trtsrd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].trtsrd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','trjtph',(Select Count(*) from US_trjtph_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].trjtph_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arrstx',(Select Count(*) from US_arrstx_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arrstx_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','trjtud',(Select Count(*) from US_trjtud_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].trjtud_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','trttrn',(Select Count(*) from US_trttrn_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].trttrn_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','tcttsh',(Select Count(*) from US_tcttsh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].tcttsh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjvd',(Select Count(*) from US_ivjjvd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjvd_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjvs',(Select Count(*) from US_ivjjvs_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjvs_rec) ;               
              
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','mcrmss',(Select Count(*) from US_mcrmss_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].mcrmss_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortcht',(Select Count(*) from US_ortcht_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortcht_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cctcta',(Select Count(*) from US_cctcta_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cctcta_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','plrrpd',(Select Count(*) from US_plrrpd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].plrrpd_rec) ;          
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','injitv',(Select Count(*) from US_injitv_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].injitv_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','artrvh',(Select Count(*) from US_artrvh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].artrvh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','injitd',(Select Count(*) from US_injitd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].injitd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjct',(Select Count(*) from US_ivjjct_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjct_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','injtdc',(Select Count(*) from US_injtdc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].injtdc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ipjtrt',(Select Count(*) from US_ipjtrt_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ipjtrt_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','iprpwc',(Select Count(*) from US_iprpwc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].iprpwc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','potpoh',(Select Count(*) from US_potpoh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].potpoh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ipjsoh',(Select Count(*) from US_ipjsoh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ipjsoh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrbrh',(Select Count(*) from US_scrbrh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrbrh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjit',(Select Count(*) from US_ivjjit_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjit_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cctatv',(Select Count(*) from US_cctatv_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cctatv_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','pntssp',(Select Count(*) from US_pntssp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].pntssp_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','pohpra',(Select Count(*) from US_pohpra_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].pohpra_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortxro',(Select Count(*) from US_ortxro_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortxro_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','pohrhr',(Select Count(*) from US_pohrhr_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].pohrhr_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','aprshf',(Select Count(*) from US_aprshf_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].aprshf_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrpti',(Select Count(*) from US_inrpti_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrpti_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','aptpyh',(Select Count(*) from US_aptpyh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].aptpyh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','sctpal',(Select Count(*) from US_sctpal_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].sctpal_rec) ;          
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortbka',(Select Count(*) from US_ortbka_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortbka_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','mxtarh',(Select Count(*) from US_mxtarh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].mxtarh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','intprd',(Select Count(*) from US_intprd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].intprd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','aprven',(Select Count(*) from US_aprven_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].aprven_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arbcoc',(Select Count(*) from US_arbcoc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arbcoc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arrcrd',(Select Count(*) from US_arrcrd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arrcrd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arrcuc',(Select Count(*) from US_arrcuc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arrcuc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arrcus',(Select Count(*) from US_arrcus_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arrcus_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','arrshp',(Select Count(*) from US_arrshp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].arrshp_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','frtfrt',(Select Count(*) from US_frtfrt_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].frtfrt_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ipjtrh',(Select Count(*) from US_ipjtrh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ipjtrh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrprm',(Select Count(*) from US_inrprm_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrprm_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','intacp',(Select Count(*) from US_intacp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].intacp_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','injith',(Select Count(*) from US_injith_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].injith_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrwhs',(Select Count(*) from US_scrwhs_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrwhs_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjvh',(Select Count(*) from US_ivjjvh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjvh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','mxrusr',(Select Count(*) from US_mxrusr_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].mxrusr_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortbki',(Select Count(*) from US_ortbki_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortbki_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','apjglj',(Select Count(*) from US_apjglj_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].apjglj_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','nctncf',(Select Count(*) from US_nctncf_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].nctncf_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ccratp',(Select Count(*) from US_ccratp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ccratp_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','pohphr',(Select Count(*) from US_pohphr_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].pohphr_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','intpic',(Select Count(*) from US_intpic_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].intpic_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrfrm',(Select Count(*) from US_inrfrm_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrfrm_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortoit',(Select Count(*) from US_ortoit_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortoit_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivtivs',(Select Count(*) from US_ivtivs_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivtivs_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivjjpd',(Select Count(*) from US_ivjjpd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivjjpd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','apjjvc',(Select Count(*) from US_apjjvc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].apjjvc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','iptrps',(Select Count(*) from US_iptrps_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].iptrps_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrcrx',(Select Count(*) from US_scrcrx_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrcrx_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cctcay',(Select Count(*) from US_cctcay_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cctcay_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','iptjbs',(Select Count(*) from US_iptjbs_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].iptjbs_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrgrd',(Select Count(*) from US_inrgrd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrgrd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','iptjso',(Select Count(*) from US_iptjso_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].iptjso_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','rvtres',(Select Count(*) from US_rvtres_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].rvtres_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ipjssh',(Select Count(*) from US_ipjssh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ipjssh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','mchqms',(Select Count(*) from US_mchqms_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].mchqms_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','potpoi',(Select Count(*) from US_potpoi_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].potpoi_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortchl',(Select Count(*) from US_ortchl_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortchl_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cctctk',(Select Count(*) from US_cctctk_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cctctk_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortmbk',(Select Count(*) from US_ortmbk_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortmbk_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrmat',(Select Count(*) from US_inrmat_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrmat_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortord',(Select Count(*) from US_ortord_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortord_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','orrprs',(Select Count(*) from US_orrprs_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].orrprs_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortorh',(Select Count(*) from US_ortorh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortorh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrloc',(Select Count(*) from US_inrloc_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrloc_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortorl',(Select Count(*) from US_ortorl_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortorl_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrmpt',(Select Count(*) from US_scrmpt_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrmpt_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','intpcr',(Select Count(*) from US_intpcr_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].intpcr_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','pfhdpf',(Select Count(*) from US_pfhdpf_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].pfhdpf_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrpyt',(Select Count(*) from US_scrpyt_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrpyt_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','potpod',(Select Count(*) from US_potpod_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].potpod_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivtivd',(Select Count(*) from US_ivtivd_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivtivd_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrrsn',(Select Count(*) from US_scrrsn_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrrsn_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ortxre',(Select Count(*) from US_ortxre_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ortxre_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','rprcds',(Select Count(*) from US_rprcds_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].rprcds_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','sahstn',(Select Count(*) from US_sahstn_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].sahstn_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','ivtivh',(Select Count(*) from US_ivtivh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].ivtivh_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','mchqds',(Select Count(*) from US_mchqds_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].mchqds_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrcva',(Select Count(*) from US_scrcva_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrcva_rec) ;               
              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrcvt',(Select Count(*) from US_scrcvt_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrcvt_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','nctnit',(Select Count(*) from US_nctnit_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].nctnit_rec) ;               
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','nctnhh',(Select Count(*) from US_nctnhh_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].nctnhh_rec) ;              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','xcrixv',(Select Count(*) from US_xcrixv_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].xcrixv_rec) ;              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','perppb',(Select Count(*) from US_perppb_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].perppb_rec) ;             
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','cprchp',(Select Count(*) from US_cprchp_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].cprchp_rec) ;                       
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','xctava',(Select Count(*) from US_xctava_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].xctava_rec) ;                       
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','inrmil',(Select Count(*) from US_inrmil_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].inrmil_rec) ;                       
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','potpor',(Select Count(*) from US_potpor_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].potpor_rec) ;                       
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'US','scrspv',(Select Count(*) from US_scrspv_rec),(Select Count(*) from [LIVEUSSTX].[liveusstxdb].[informix].scrspv_rec) ;                       
                   
      End                  
       ELSE IF @DBNAME = 'CA'              
     BEGIN                  
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','sahsat',(Select Count(*) from CA_sahsat_rec),(Select Count(*) from[LIVECA_IW].[livecastxdb_iw].[informix].[sahsat_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','plswrw',(Select Count(*) from CA_plswrw_rec),(Select Count(*) from[LIVECA_IW].[livecastxdb_iw].[informix].[plswrw_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','sasmsu',(Select Count(*) from CA_sasmsu_rec),(Select Count(*) from[LIVECA_IW].[livecastxdb_iw].[informix].[sasmsu_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','glbacb',(Select Count(*) from CA_glbacb_rec),(Select Count(*) from[LIVECAGL].[livecagldb].[informix].glbacb_rec)         ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','gltoje',(Select Count(*) from CA_gltoje_rec),(Select Count(*) from[LIVECAGL].[livecagldb].[informix].gltoje_rec)         ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','glbbud',(Select Count(*) from CA_glbbud_rec),(Select Count(*) from[LIVECAGL].[livecagldb].[informix].glbbud_rec)         ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','glhgld',(Select Count(*) from CA_glhgld_rec),(Select Count(*) from[LIVECAGL].[livecagldb].[informix].glhgld_rec)         ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','gltojd',(Select Count(*) from CA_gltojd_rec),(Select Count(*) from[LIVECAGL].[livecagldb].[informix].gltojd_rec)         ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','glrbga',(Select Count(*) from CA_glrbga_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].glrbga_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrslp',(Select Count(*) from CA_scrslp_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrslp_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','aptvch',(Select Count(*) from CA_aptvch_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].aptvch_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','tctipd',(Select Count(*) from CA_tctipd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].tctipd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','cttcsi',(Select Count(*) from CA_cttcsi_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].cttcsi_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','trtsrd',(Select Count(*) from CA_trtsrd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].trtsrd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','trjtph',(Select Count(*) from CA_trjtph_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].trjtph_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','arrstx',(Select Count(*) from CA_arrstx_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].arrstx_rec)      ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','trjtud',(Select Count(*) from CA_trjtud_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].trjtud_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','trttrn',(Select Count(*) from CA_trttrn_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].trttrn_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','tcttsh',(Select Count(*) from CA_tcttsh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].tcttsh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjvd',(Select Count(*) from CA_ivjjvd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjvd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjvs',(Select Count(*) from CA_ivjjvs_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjvs_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','mcrmss',(Select Count(*) from CA_mcrmss_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].mcrmss_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortcht',(Select Count(*) from CA_ortcht_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortcht_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','cctcta',(Select Count(*) from CA_cctcta_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].cctcta_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','plrrpd',(Select Count(*) from CA_plrrpd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].plrrpd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','injitv',(Select Count(*) from CA_injitv_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].injitv_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','artrvh',(Select Count(*) from CA_artrvh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].artrvh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','injitd',(Select Count(*) from CA_injitd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].injitd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjct',(Select Count(*) from CA_ivjjct_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjct_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','injtdc',(Select Count(*) from CA_injtdc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].injtdc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ipjtrt',(Select Count(*) from CA_ipjtrt_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ipjtrt_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','iprpwc',(Select Count(*) from CA_iprpwc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].iprpwc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','potpoh',(Select Count(*) from CA_potpoh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].potpoh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ipjsoh',(Select Count(*) from CA_ipjsoh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ipjsoh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrbrh',(Select Count(*) from CA_scrbrh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrbrh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjit',(Select Count(*) from CA_ivjjit_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjit_rec)      ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','cctatv',(Select Count(*) from CA_cctatv_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].cctatv_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','pntssp',(Select Count(*) from CA_pntssp_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].pntssp_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','pohpra',(Select Count(*) from CA_pohpra_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].pohpra_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortxro',(Select Count(*) from CA_ortxro_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortxro_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','pohrhr',(Select Count(*) from CA_pohrhr_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].pohrhr_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','aprshf',(Select Count(*) from CA_aprshf_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].aprshf_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrpti',(Select Count(*) from CA_inrpti_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrpti_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','aptpyh',(Select Count(*) from CA_aptpyh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].aptpyh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','sctpal',(Select Count(*) from CA_sctpal_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].sctpal_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortbka',(Select Count(*) from CA_ortbka_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortbka_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','mxtarh',(Select Count(*) from CA_mxtarh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].mxtarh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','intprd',(Select Count(*) from CA_intprd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].intprd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','aprven',(Select Count(*) from CA_aprven_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].aprven_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','arbcoc',(Select Count(*) from CA_arbcoc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].arbcoc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','arrcrd',(Select Count(*) from CA_arrcrd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].arrcrd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','arrcuc',(Select Count(*) from CA_arrcuc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].arrcuc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','arrshp',(Select Count(*) from CA_arrshp_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].arrshp_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','frtfrt',(Select Count(*) from CA_frtfrt_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].frtfrt_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ipjtrh',(Select Count(*) from CA_ipjtrh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ipjtrh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrprm',(Select Count(*) from CA_inrprm_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrprm_rec)      ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','intacp',(Select Count(*) from CA_intacp_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].intacp_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','injith',(Select Count(*) from CA_injith_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].injith_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrwhs',(Select Count(*) from CA_scrwhs_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrwhs_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjvh',(Select Count(*) from CA_ivjjvh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjvh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','mxrusr',(Select Count(*) from CA_mxrusr_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].mxrusr_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortbki',(Select Count(*) from CA_ortbki_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortbki_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','apjglj',(Select Count(*) from CA_apjglj_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].apjglj_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','nctncf',(Select Count(*) from CA_nctncf_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].nctncf_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ccratp',(Select Count(*) from CA_ccratp_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ccratp_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','pohphr',(Select Count(*) from CA_pohphr_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].pohphr_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrfrm',(Select Count(*) from CA_inrfrm_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrfrm_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortoit',(Select Count(*) from CA_ortoit_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortoit_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivtivs',(Select Count(*) from CA_ivtivs_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivtivs_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivjjpd',(Select Count(*) from CA_ivjjpd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivjjpd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','apjjvc',(Select Count(*) from CA_apjjvc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].apjjvc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','iptrps',(Select Count(*) from CA_iptrps_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].iptrps_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrcrx',(Select Count(*) from CA_scrcrx_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrcrx_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','cctcay',(Select Count(*) from CA_cctcay_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].cctcay_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','iptjbs',(Select Count(*) from CA_iptjbs_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].iptjbs_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrgrd',(Select Count(*) from CA_inrgrd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrgrd_rec)      ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','iptjso',(Select Count(*) from CA_iptjso_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].iptjso_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','rvtres',(Select Count(*) from CA_rvtres_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].rvtres_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ipjssh',(Select Count(*) from CA_ipjssh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ipjssh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','potpoi',(Select Count(*) from CA_potpoi_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].potpoi_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortchl',(Select Count(*) from CA_ortchl_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortchl_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','cctctk',(Select Count(*) from CA_cctctk_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].cctctk_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortmbk',(Select Count(*) from CA_ortmbk_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortmbk_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrmat',(Select Count(*) from CA_inrmat_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrmat_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortord',(Select Count(*) from CA_ortord_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortord_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','orrprs',(Select Count(*) from CA_orrprs_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].orrprs_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortorh',(Select Count(*) from CA_ortorh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortorh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrloc',(Select Count(*) from CA_inrloc_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrloc_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortorl',(Select Count(*) from CA_ortorl_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortorl_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrmpt',(Select Count(*) from CA_scrmpt_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrmpt_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','intpcr',(Select Count(*) from CA_intpcr_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].intpcr_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','pfhdpf',(Select Count(*) from CA_pfhdpf_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].pfhdpf_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrpyt',(Select Count(*) from CA_scrpyt_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrpyt_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','potpod',(Select Count(*) from CA_potpod_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].potpod_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivtivd',(Select Count(*) from CA_ivtivd_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivtivd_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrrsn',(Select Count(*) from CA_scrrsn_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrrsn_rec)      ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ortxre',(Select Count(*) from CA_ortxre_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ortxre_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','rprcds',(Select Count(*) from CA_rprcds_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].rprcds_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','sahstn',(Select Count(*) from CA_sahstn_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].sahstn_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','ivtivh',(Select Count(*) from CA_ivtivh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].ivtivh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrcva',(Select Count(*) from CA_scrcva_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrcva_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrcvt',(Select Count(*) from CA_scrcvt_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrcvt_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','nctnit',(Select Count(*) from CA_nctnit_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].nctnit_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','nctnhh',(Select Count(*) from CA_nctnhh_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].nctnhh_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','xcrixv',(Select Count(*) from CA_xcrixv_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].xcrixv_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','perppb',(Select Count(*) from CA_perppb_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].perppb_rec)       ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','xctava',(Select Count(*) from CA_xctava_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].xctava_rec)       ;          
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','inrmil',(Select Count(*) from CA_inrmil_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].inrmil_rec)       ;          
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','potpor',(Select Count(*) from CA_potpor_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].potpor_rec)       ;                     
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'CA','scrspv',(Select Count(*) from CA_scrspv_rec),(Select Count(*) from[LIVECASTX].[livecastxdb].[informix].scrspv_rec)       ;                     
                     
                   
      End                 
                    
       ELSE IF @DBNAME = 'TW'              
     BEGIN                  
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','sahsat',(Select Count(*) from TW_sahsat_rec),(Select Count(*) from[LIVETW_IW].[livetwstxdb_iw].[informix].[sahsat_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','plswrw',(Select Count(*) from TW_plswrw_rec),(Select Count(*) from[LIVETW_IW].[livetwstxdb_iw].[informix].[plswrw_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','sasmsu',(Select Count(*) from TW_sasmsu_rec),(Select Count(*) from[LIVETW_IW].[livetwstxdb_iw].[informix].[sasmsu_rec] ) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','glbacb',(Select Count(*) from TW_glbacb_rec),(Select Count(*) from[LIVETWGL].[livetwgldb].[informix].glbacb_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','gltoje',(Select Count(*) from TW_gltoje_rec),(Select Count(*) from[LIVETWGL].[livetwgldb].[informix].gltoje_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','glbbud',(Select Count(*) from TW_glbbud_rec),(Select Count(*) from[LIVETWGL].[livetwgldb].[informix].glbbud_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','glhgld',(Select Count(*) from TW_glhgld_rec),(Select Count(*) from[LIVETWGL].[livetwgldb].[informix].glhgld_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','gltojd',(Select Count(*) from TW_gltojd_rec),(Select Count(*) from[LIVETWGL].[livetwgldb].[informix].gltojd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','glrbga',(Select Count(*) from TW_glrbga_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].glrbga_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrslp',(Select Count(*) from TW_scrslp_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrslp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','aptvch',(Select Count(*) from TW_aptvch_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].aptvch_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','tctipd',(Select Count(*) from TW_tctipd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].tctipd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','cttcsi',(Select Count(*) from TW_cttcsi_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].cttcsi_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','trtsrd',(Select Count(*) from TW_trtsrd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].trtsrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','trjtph',(Select Count(*) from TW_trjtph_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].trjtph_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','arrstx',(Select Count(*) from TW_arrstx_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].arrstx_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','trjtud',(Select Count(*) from TW_trjtud_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].trjtud_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','trttrn',(Select Count(*) from TW_trttrn_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].trttrn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','tcttsh',(Select Count(*) from TW_tcttsh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].tcttsh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivjjvd',(Select Count(*) from TW_ivjjvd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjvd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivjjvs',(Select Count(*) from TW_ivjjvs_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjvs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','mcrmss',(Select Count(*) from TW_mcrmss_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].mcrmss_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortcht',(Select Count(*) from TW_ortcht_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortcht_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','cctcta',(Select Count(*) from TW_cctcta_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].cctcta_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','plrrpd',(Select Count(*) from TW_plrrpd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].plrrpd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','injitv',(Select Count(*) from TW_injitv_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].injitv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','artrvh',(Select Count(*) from TW_artrvh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].artrvh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','injitd',(Select Count(*) from TW_injitd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].injitd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivjjct',(Select Count(*) from TW_ivjjct_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjct_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','injtdc',(Select Count(*) from TW_injtdc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].injtdc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ipjtrt',(Select Count(*) from TW_ipjtrt_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ipjtrt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','iprpwc',(Select Count(*) from TW_iprpwc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].iprpwc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','potpoh',(Select Count(*) from TW_potpoh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].potpoh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ipjsoh',(Select Count(*) from TW_ipjsoh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ipjsoh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrbrh',(Select Count(*) from TW_scrbrh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrbrh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivjjit',(Select Count(*) from TW_ivjjit_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','cctatv',(Select Count(*) from TW_cctatv_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].cctatv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','pntssp',(Select Count(*) from TW_pntssp_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].pntssp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','pohpra',(Select Count(*) from TW_pohpra_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].pohpra_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortxro',(Select Count(*) from TW_ortxro_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortxro_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','pohrhr',(Select Count(*) from TW_pohrhr_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].pohrhr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','aprshf',(Select Count(*) from TW_aprshf_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].aprshf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrpti',(Select Count(*) from TW_inrpti_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrpti_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','aptpyh',(Select Count(*) from TW_aptpyh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].aptpyh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','sctpal',(Select Count(*) from TW_sctpal_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].sctpal_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortbka',(Select Count(*) from TW_ortbka_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortbka_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','mxtarh',(Select Count(*) from TW_mxtarh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].mxtarh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','intprd',(Select Count(*) from TW_intprd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].intprd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','aprven',(Select Count(*) from TW_aprven_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].aprven_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','arbcoc',(Select Count(*) from TW_arbcoc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].arbcoc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','arrcrd',(Select Count(*) from TW_arrcrd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].arrcrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','arrcuc',(Select Count(*) from TW_arrcuc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].arrcuc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','arrshp',(Select Count(*) from TW_arrshp_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].arrshp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','frtfrt',(Select Count(*) from TW_frtfrt_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].frtfrt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ipjtrh',(Select Count(*) from TW_ipjtrh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ipjtrh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrprm',(Select Count(*) from TW_inrprm_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrprm_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','intacp',(Select Count(*) from TW_intacp_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].intacp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','injith',(Select Count(*) from TW_injith_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].injith_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrwhs',(Select Count(*) from TW_scrwhs_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrwhs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivjjvh',(Select Count(*) from TW_ivjjvh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjvh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','mxrusr',(Select Count(*) from TW_mxrusr_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].mxrusr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortbki',(Select Count(*) from TW_ortbki_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortbki_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','apjglj',(Select Count(*) from TW_apjglj_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].apjglj_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','nctncf',(Select Count(*) from TW_nctncf_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].nctncf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ccratp',(Select Count(*) from TW_ccratp_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ccratp_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','pohphr',(Select Count(*) from TW_pohphr_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].pohphr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrfrm',(Select Count(*) from TW_inrfrm_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrfrm_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortoit',(Select Count(*) from TW_ortoit_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortoit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivtivs',(Select Count(*) from TW_ivtivs_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivtivs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT 'TW','ivjjpd',(Select Count(*) from TW_ivjjpd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivjjpd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','apjjvc',(Select Count(*) from TW_apjjvc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].apjjvc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','iptrps',(Select Count(*) from TW_iptrps_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].iptrps_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrcrx',(Select Count(*) from TW_scrcrx_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrcrx_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','cctcay',(Select Count(*) from TW_cctcay_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].cctcay_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','iptjbs',(Select Count(*) from TW_iptjbs_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].iptjbs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrgrd',(Select Count(*) from TW_inrgrd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrgrd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','iptjso',(Select Count(*) from TW_iptjso_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].iptjso_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','rvtres',(Select Count(*) from TW_rvtres_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].rvtres_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ipjssh',(Select Count(*) from TW_ipjssh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ipjssh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','potpoi',(Select Count(*) from TW_potpoi_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].potpoi_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortchl',(Select Count(*) from TW_ortchl_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortchl_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','cctctk',(Select Count(*) from TW_cctctk_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].cctctk_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortmbk',(Select Count(*) from TW_ortmbk_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortmbk_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrmat',(Select Count(*) from TW_inrmat_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrmat_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortord',(Select Count(*) from TW_ortord_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortord_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','orrprs',(Select Count(*) from TW_orrprs_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].orrprs_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortorh',(Select Count(*) from TW_ortorh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortorh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrloc',(Select Count(*) from TW_inrloc_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrloc_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortorl',(Select Count(*) from TW_ortorl_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortorl_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrmpt',(Select Count(*) from TW_scrmpt_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrmpt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','intpcr',(Select Count(*) from TW_intpcr_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].intpcr_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','pfhdpf',(Select Count(*) from TW_pfhdpf_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].pfhdpf_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrpyt',(Select Count(*) from TW_scrpyt_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrpyt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','potpod',(Select Count(*) from TW_potpod_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].potpod_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivtivd',(Select Count(*) from TW_ivtivd_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivtivd_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrrsn',(Select Count(*) from TW_scrrsn_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrrsn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ortxre',(Select Count(*) from TW_ortxre_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ortxre_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','rprcds',(Select Count(*) from TW_rprcds_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].rprcds_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','sahstn',(Select Count(*) from TW_sahstn_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].sahstn_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','ivtivh',(Select Count(*) from TW_ivtivh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].ivtivh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrcva',(Select Count(*) from TW_scrcva_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrcva_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrcvt',(Select Count(*) from TW_scrcvt_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrcvt_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','nctnit',(Select Count(*) from TW_nctnit_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].nctnit_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','nctnhh',(Select Count(*) from TW_nctnhh_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].nctnhh_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','xcrixv',(Select Count(*) from TW_xcrixv_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].xcrixv_rec) ;            
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','perppb',(Select Count(*) from TW_perppb_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].perppb_rec) ;              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','xctava',(Select Count(*) from TW_xctava_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].xctava_rec) ;                 
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','inrmil',(Select Count(*) from TW_inrmil_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].inrmil_rec) ;                 
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','potpor',(Select Count(*) from TW_potpor_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].potpor_rec) ;                              
INSERT INTO #tmp ( Prefix,tablename,localcount,livecount) SELECT  'TW','scrspv',(Select Count(*) from TW_scrspv_rec),(Select Count(*) from[LIVETWSTX].[livetwstxdb].[informix].scrspv_rec) ;                              
      End                
                    
--       ELSE IF @DBNAME = 'NO'              
--     BEGIN                  
--     INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)              
              
--SELECT  'NO','sahsat',(Select Count(*) from NO_sahsat_rec),(Select Count(*) from [LIVENO_IW].[livenostxdb_iw].[informix].[sahsat_rec] ) Union               
-- SELECT  'NO','plswrw',(Select Count(*) from NO_plswrw_rec),(Select Count(*) from [LIVENO_IW].[livenostxdb_iw].[informix].[plswrw_rec] ) Union               
-- SELECT  'NO','sasmsu',(Select Count(*) from NO_sasmsu_rec),(Select Count(*) from [LIVENO_IW].[livenostxdb_iw].[informix].[sasmsu_rec] ) Union                   
              
--SELECT  'NO','glbacb',(Select Count(*) from NO_glbacb_rec),(Select Count(*) from [LIVENOGL].[livenogldb].[informix].glbacb_rec) Union               
--SELECT  'NO','gltoje',(Select Count(*) from NO_gltoje_rec),(Select Count(*) from [LIVENOGL].[livenogldb].[informix].gltoje_rec) Union               
--SELECT  'NO','glbbud',(Select Count(*) from NO_glbbud_rec),(Select Count(*) from [LIVENOGL].[livenogldb].[informix].glbbud_rec) Union               
--SELECT  'NO','glhgld',(Select Count(*) from NO_glhgld_rec),(Select Count(*) from [LIVENOGL].[livenogldb].[informix].glhgld_rec) Union               
--SELECT  'NO','gltojd',(Select Count(*) from NO_gltojd_rec),(Select Count(*) from [LIVENOGL].[livenogldb].[informix].gltojd_rec) Union              
--SELECT  'NO','glrbga',(Select Count(*) from NO_glrbga_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].glrbga_rec) Union               
                   
--SELECT  'NO','scrslp',(Select Count(*) from   NO_scrslp_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrslp_rec) Union               
--SELECT  'NO','aptvch',(Select Count(*) from NO_aptvch_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].aptvch_rec) Union               
--SELECT  'NO','tctipd',(Select Count(*) from NO_tctipd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].tctipd_rec) Union               
--SELECT  'NO','cttcsi',(Select Count(*) from NO_cttcsi_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].cttcsi_rec) Union               
--SELECT  'NO','trtsrd',(Select Count(*) from NO_trtsrd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].trtsrd_rec) Union               
--SELECT  'NO','trjtph',(Select Count(*) from NO_trjtph_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].trjtph_rec) Union               
--SELECT  'NO','arrstx',(Select Count(*) from NO_arrstx_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].arrstx_rec) Union               
--SELECT  'NO','trjtud',(Select Count(*) from NO_trjtud_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].trjtud_rec) Union               
--SELECT  'NO','trttrn',(Select Count(*) from NO_trttrn_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].trttrn_rec) Union               
--SELECT  'NO','tcttsh',(Select Count(*) from NO_tcttsh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].tcttsh_rec) Union               
--SELECT  'NO','ivjjvd',(Select Count(*) from NO_ivjjvd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjvd_rec) Union               
              
--SELECT  'NO','ivjjvs',(Select Count(*) from NO_ivjjvs_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjvs_rec) Union               
              
              
--SELECT  'NO','mcrmss',(Select Count(*) from NO_mcrmss_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].mcrmss_rec) Union               
              
--SELECT  'NO','ortcht',(Select Count(*) from NO_ortcht_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortcht_rec) Union               
              
--SELECT  'NO','cctcta',(Select Count(*) from NO_cctcta_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].cctcta_rec) Union               
--SELECT  'NO','plrrpd',(Select Count(*) from NO_plrrpd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].plrrpd_rec) Union               
--SELECT  'NO','injitv',(Select Count(*) from NO_injitv_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].injitv_rec) Union               
--SELECT  'NO','artrvh',(Select Count(*) from NO_artrvh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].artrvh_rec) Union               
--SELECT  'NO','injitd',(Select Count(*) from NO_injitd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].injitd_rec) Union               
--SELECT  'NO','ivjjct',(Select Count(*) from NO_ivjjct_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjct_rec) Union               
--SELECT  'NO','injtdc',(Select Count(*) from NO_injtdc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].injtdc_rec) Union               
--SELECT  'NO','ipjtrt',(Select Count(*) from NO_ipjtrt_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ipjtrt_rec) Union               
--SELECT  'NO','iprpwc',(Select Count(*) from NO_iprpwc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].iprpwc_rec) Union               
--SELECT  'NO','potpoh',(Select Count(*) from NO_potpoh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].potpoh_rec) Union               
--SELECT  'NO','ipjsoh',(Select Count(*) from NO_ipjsoh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ipjsoh_rec) Union               
--SELECT  'NO','scrbrh',(Select Count(*) from NO_scrbrh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrbrh_rec) Union               
--SELECT  'NO','ivjjit',(Select Count(*) from NO_ivjjit_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjit_rec) Union               
--SELECT  'NO','cctatv',(Select Count(*) from NO_cctatv_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].cctatv_rec) Union               
--SELECT  'NO','pntssp',(Select Count(*) from NO_pntssp_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].pntssp_rec) Union               
--SELECT  'NO','pohpra',(Select Count(*) from NO_pohpra_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].pohpra_rec) Union               
--SELECT  'NO','ortxro',(Select Count(*) from NO_ortxro_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortxro_rec) Union               
--SELECT  'NO','pohrhr',(Select Count(*) from NO_pohrhr_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].pohrhr_rec) Union               
--SELECT  'NO','aprshf',(Select Count(*) from NO_aprshf_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].aprshf_rec) Union               
--SELECT  'NO','inrpti',(Select Count(*) from NO_inrpti_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrpti_rec) Union               
--SELECT  'NO','aptpyh',(Select Count(*) from NO_aptpyh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].aptpyh_rec) Union               
--SELECT  'NO','sctpal',(Select Count(*) from NO_sctpal_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].sctpal_rec) Union               
--SELECT  'NO','ortbka',(Select Count(*) from NO_ortbka_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortbka_rec) Union               
--SELECT  'NO','mxtarh',(Select Count(*) from NO_mxtarh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].mxtarh_rec) Union               
--SELECT  'NO','intprd',(Select Count(*) from NO_intprd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].intprd_rec) Union               
              
--SELECT 'NO','aprven',(Select Count(*) from NO_aprven_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].aprven_rec) Union               
--SELECT  'NO','arbcoc',(Select Count(*) from NO_arbcoc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].arbcoc_rec) Union               
--SELECT  'NO','arrcrd',(Select Count(*) from NO_arrcrd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].arrcrd_rec) Union               
--SELECT  'NO','arrcuc',(Select Count(*) from NO_arrcuc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].arrcuc_rec) Union               
--SELECT  'NO','arrshp',(Select Count(*) from NO_arrshp_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].arrshp_rec) Union               
--SELECT  'NO','frtfrt',(Select Count(*) from NO_frtfrt_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].frtfrt_rec) Union               
--SELECT  'NO','ipjtrh',(Select Count(*) from NO_ipjtrh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ipjtrh_rec) Union               
--SELECT  'NO','inrprm',(Select Count(*) from NO_inrprm_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrprm_rec) Union               
--SELECT  'NO','intacp',(Select Count(*) from NO_intacp_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].intacp_rec) Union               
--SELECT  'NO','injith',(Select Count(*) from NO_injith_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].injith_rec) Union               
--SELECT  'NO','scrwhs',(Select Count(*) from NO_scrwhs_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrwhs_rec) Union               
--SELECT  'NO','ivjjvh',(Select Count(*) from NO_ivjjvh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjvh_rec) Union               
--SELECT  'NO','mxrusr',(Select Count(*) from NO_mxrusr_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].mxrusr_rec) Union               
              
--SELECT  'NO','ortbki',(Select Count(*) from NO_ortbki_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortbki_rec) Union               
--SELECT  'NO','apjglj',(Select Count(*) from NO_apjglj_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].apjglj_rec) Union               
--SELECT  'NO','nctncf',(Select Count(*) from NO_nctncf_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].nctncf_rec) Union               
--SELECT  'NO','ccratp',(Select Count(*) from NO_ccratp_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ccratp_rec) Union               
--SELECT  'NO','pohphr',(Select Count(*) from NO_pohphr_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].pohphr_rec) Union               
--SELECT  'NO','inrfrm',(Select Count(*) from NO_inrfrm_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrfrm_rec) Union               
--SELECT  'NO','ortoit',(Select Count(*) from NO_ortoit_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortoit_rec) Union               
--SELECT  'NO','ivtivs',(Select Count(*) from NO_ivtivs_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivtivs_rec) Union               
--SELECT  'NO','ivjjpd',(Select Count(*) from NO_ivjjpd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivjjpd_rec) Union               
--SELECT  'NO','apjjvc',(Select Count(*) from NO_apjjvc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].apjjvc_rec) Union               
--SELECT  'NO','iptrps',(Select Count(*) from NO_iptrps_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].iptrps_rec) Union               
--SELECT  'NO','scrcrx',(Select Count(*) from NO_scrcrx_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrcrx_rec) Union               
--SELECT  'NO','cctcay',(Select Count(*) from NO_cctcay_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].cctcay_rec) Union               
--SELECT  'NO','iptjbs',(Select Count(*) from NO_iptjbs_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].iptjbs_rec) Union               
--SELECT  'NO','inrgrd',(Select Count(*) from NO_inrgrd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrgrd_rec) Union               
--SELECT  'NO','iptjso',(Select Count(*) from NO_iptjso_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].iptjso_rec) Union               
              
--SELECT  'NO','rvtres',(Select Count(*) from NO_rvtres_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].rvtres_rec) Union               
--SELECT  'NO','ipjssh',(Select Count(*) from NO_ipjssh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ipjssh_rec) Union               
--SELECT  'NO','potpoi',(Select Count(*) from NO_potpoi_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].potpoi_rec) Union               
--SELECT  'NO','ortchl',(Select Count(*) from NO_ortchl_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortchl_rec) Union               
--SELECT  'NO','cctctk',(Select Count(*) from NO_cctctk_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].cctctk_rec) Union               
--SELECT  'NO','ortmbk',(Select Count(*) from NO_ortmbk_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortmbk_rec) Union               
--SELECT  'NO','inrmat',(Select Count(*) from NO_inrmat_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrmat_rec) Union               
--SELECT  'NO','ortord',(Select Count(*) from NO_ortord_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortord_rec) Union               
--SELECT  'NO','orrprs',(Select Count(*) from NO_orrprs_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].orrprs_rec) Union               
--SELECT  'NO','ortorh',(Select Count(*) from NO_ortorh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortorh_rec) Union               
--SELECT  'NO','inrloc',(Select Count(*) from NO_inrloc_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrloc_rec) Union               
--SELECT  'NO','ortorl',(Select Count(*) from NO_ortorl_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortorl_rec) Union               
--SELECT  'NO','scrmpt',(Select Count(*) from NO_scrmpt_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrmpt_rec) Union               
--SELECT  'NO','intpcr',(Select Count(*) from NO_intpcr_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].intpcr_rec) Union               
--SELECT  'NO','pfhdpf',(Select Count(*) from NO_pfhdpf_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].pfhdpf_rec) Union               
--SELECT  'NO','scrpyt',(Select Count(*) from NO_scrpyt_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrpyt_rec) Union               
--SELECT  'NO','potpod',(Select Count(*) from NO_potpod_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].potpod_rec) Union               
--SELECT  'NO','ivtivd',(Select Count(*) from NO_ivtivd_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivtivd_rec) Union               
--SELECT  'NO','scrrsn',(Select Count(*) from NO_scrrsn_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrrsn_rec) Union               
--SELECT  'NO','ortxre',(Select Count(*) from NO_ortxre_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ortxre_rec) Union               
              
--SELECT  'NO','rprcds',(Select Count(*) from NO_rprcds_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].rprcds_rec) Union               
--SELECT  'NO','sahstn',(Select Count(*) from NO_sahstn_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].sahstn_rec) Union               
--SELECT  'NO','ivtivh',(Select Count(*) from NO_ivtivh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].ivtivh_rec) Union               
--SELECT  'NO','scrcva',(Select Count(*) from NO_scrcva_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrcva_rec) Union               
              
--SELECT  'NO','scrcvt',(Select Count(*) from NO_scrcvt_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrcvt_rec) Union               
--SELECT  'NO','nctnit',(Select Count(*) from NO_nctnit_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].nctnit_rec) Union               
--SELECT  'NO','nctnhh',(Select Count(*) from NO_nctnhh_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].nctnhh_rec) Union              
--SELECT  'NO','xcrixv',(Select Count(*) from NO_xcrixv_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].xcrixv_rec) Union                    
--SELECT  'NO','xctava',(Select Count(*) from NO_xctava_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].xctava_rec) Union         
--SELECT  'NO','inrmil',(Select Count(*) from NO_inrmil_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].inrmil_rec) Union         
--SELECT  'NO','perppb',(Select Count(*) from NO_perppb_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].perppb_rec) Union              
--SELECT  'NO','potpor',(Select Count(*) from NO_potpor_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].potpor_rec) Union                           
--SELECT  'NO','scrspv',(Select Count(*) from NO_scrspv_rec),(Select Count(*) from [LIVENOSTX].[livenostxdb].[informix].scrspv_rec)      
--      End                
                    
       ELSE IF @DBNAME = 'CN'              
     BEGIN                  
     INSERT INTO #tmp ( Prefix,tablename,localcount,livecount)              
                   
 SELECT  'CN','sahsat',(Select Count(*) from CN_sahsat_rec),(Select Count(*) from [LIVECN_IW].[livecnstxdb_iw].[informix].[sahsat_rec] ) Union               
 SELECT  'CN','plswrw',(Select Count(*) from CN_plswrw_rec),(Select Count(*) from [LIVECN_IW].[livecnstxdb_iw].[informix].[plswrw_rec] ) Union               
 SELECT  'CN','sasmsu',(Select Count(*) from CN_sasmsu_rec),(Select Count(*) from [LIVECN_IW].[livecnstxdb_iw].[informix].[sasmsu_rec] ) Union              
             SELECT  'CN','glbacb',(Select Count(*) from CN_glbacb_rec),(Select Count(*) from [LIVECNGL].[livecngldb].[informix].glbacb_rec) Union               
SELECT  'CN','gltoje',(Select Count(*) from CN_gltoje_rec),(Select Count(*) from [LIVECNGL].[livecngldb].[informix].gltoje_rec) Union               
SELECT  'CN','glbbud',(Select Count(*) from CN_glbbud_rec),(Select Count(*) from [LIVECNGL].[livecngldb].[informix].glbbud_rec) Union               
SELECT  'CN','glhgld',(Select Count(*) from CN_glhgld_rec),(Select Count(*) from [LIVECNGL].[livecngldb].[informix].glhgld_rec) Union               
SELECT  'CN','gltojd',(Select Count(*) from CN_gltojd_rec),(Select Count(*) from [LIVECNGL].[livecngldb].[informix].gltojd_rec) Union              
SELECT  'CN','glrbga',(Select Count(*) from CN_glrbga_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].glrbga_rec) Union               
                   
SELECT  'CN','scrslp',(Select Count(*) from   CN_scrslp_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrslp_rec) Union               
SELECT  'CN','aptvch',(Select Count(*) from CN_aptvch_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].aptvch_rec) Union               
SELECT  'CN','tctipd',(Select Count(*) from CN_tctipd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].tctipd_rec) Union               
SELECT  'CN','cttcsi',(Select Count(*) from CN_cttcsi_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].cttcsi_rec) Union               
SELECT  'CN','trtsrd',(Select Count(*) from CN_trtsrd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].trtsrd_rec) Union               
SELECT  'CN','trjtph',(Select Count(*) from CN_trjtph_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].trjtph_rec) Union               
SELECT  'CN','arrstx',(Select Count(*) from CN_arrstx_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].arrstx_rec) Union               
SELECT  'CN','trjtud',(Select Count(*) from CN_trjtud_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].trjtud_rec) Union               
SELECT  'CN','trttrn',(Select Count(*) from CN_trttrn_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].trttrn_rec) Union               
SELECT  'CN','tcttsh',(Select Count(*) from CN_tcttsh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].tcttsh_rec) Union               
SELECT  'CN','ivjjvd',(Select Count(*) from CN_ivjjvd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjvd_rec) Union               
              
SELECT  'CN','ivjjvs',(Select Count(*) from CN_ivjjvs_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjvs_rec) Union               
              
              
SELECT  'CN','mcrmss',(Select Count(*) from CN_mcrmss_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].mcrmss_rec) Union               
              
SELECT  'CN','ortcht',(Select Count(*) from CN_ortcht_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortcht_rec) Union               
              
SELECT  'CN','cctcta',(Select Count(*) from CN_cctcta_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].cctcta_rec) Union               
SELECT  'CN','plrrpd',(Select Count(*) from CN_plrrpd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].plrrpd_rec) Union         
SELECT  'CN','injitv',(Select Count(*) from CN_injitv_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].injitv_rec) Union               
SELECT  'CN','artrvh',(Select Count(*) from CN_artrvh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].artrvh_rec) Union               
SELECT  'CN','injitd',(Select Count(*) from CN_injitd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].injitd_rec) Union               
SELECT  'CN','ivjjct',(Select Count(*) from CN_ivjjct_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjct_rec) Union               
SELECT  'CN','injtdc',(Select Count(*) from CN_injtdc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].injtdc_rec) Union               
SELECT  'CN','ipjtrt',(Select Count(*) from CN_ipjtrt_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ipjtrt_rec) Union               
SELECT  'CN','iprpwc',(Select Count(*) from CN_iprpwc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].iprpwc_rec) Union               
SELECT  'CN','potpoh',(Select Count(*) from CN_potpoh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].potpoh_rec) Union               
SELECT  'CN','ipjsoh',(Select Count(*) from CN_ipjsoh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ipjsoh_rec) Union               
SELECT  'CN','scrbrh',(Select Count(*) from CN_scrbrh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrbrh_rec) Union               
SELECT  'CN','ivjjit',(Select Count(*) from CN_ivjjit_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjit_rec) Union               
SELECT  'CN','cctatv',(Select Count(*) from CN_cctatv_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].cctatv_rec) Union               
SELECT  'CN','pntssp',(Select Count(*) from CN_pntssp_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].pntssp_rec) Union               
SELECT  'CN','pohpra',(Select Count(*) from CN_pohpra_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].pohpra_rec) Union               
SELECT  'CN','ortxro',(Select Count(*) from CN_ortxro_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortxro_rec) Union               
SELECT  'CN','pohrhr',(Select Count(*) from CN_pohrhr_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].pohrhr_rec) Union               
SELECT  'CN','aprshf',(Select Count(*) from CN_aprshf_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].aprshf_rec) Union               
SELECT  'CN','inrpti',(Select Count(*) from CN_inrpti_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrpti_rec) Union               
SELECT  'CN','aptpyh',(Select Count(*) from CN_aptpyh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].aptpyh_rec) Union               
SELECT  'CN','sctpal',(Select Count(*) from CN_sctpal_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].sctpal_rec) Union               
SELECT  'CN','ortbka',(Select Count(*) from CN_ortbka_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortbka_rec) Union               
SELECT  'CN','mxtarh',(Select Count(*) from CN_mxtarh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].mxtarh_rec) Union               
SELECT  'CN','intprd',(Select Count(*) from CN_intprd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].intprd_rec) Union               
              
SELECT  'CN','aprven',(Select Count(*) from CN_aprven_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].aprven_rec) Union               
SELECT  'CN','arbcoc',(Select Count(*) from CN_arbcoc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].arbcoc_rec) Union               
SELECT  'CN','arrcrd',(Select Count(*) from CN_arrcrd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].arrcrd_rec) Union               
SELECT  'CN','arrcuc',(Select Count(*) from CN_arrcuc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].arrcuc_rec) Union               
SELECT  'CN','arrshp',(Select Count(*) from CN_arrshp_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].arrshp_rec) Union               
SELECT  'CN','frtfrt',(Select Count(*) from CN_frtfrt_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].frtfrt_rec) Union               
SELECT  'CN','ipjtrh',(Select Count(*) from CN_ipjtrh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ipjtrh_rec) Union               
SELECT  'CN','inrprm',(Select Count(*) from CN_inrprm_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrprm_rec) Union               
SELECT  'CN','intacp',(Select Count(*) from CN_intacp_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].intacp_rec) Union               
SELECT  'CN','injith',(Select Count(*) from CN_injith_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].injith_rec) Union               
SELECT  'CN','scrwhs',(Select Count(*) from CN_scrwhs_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrwhs_rec) Union               
SELECT  'CN','ivjjvh',(Select Count(*) from CN_ivjjvh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjvh_rec) Union               
SELECT  'CN','mxrusr',(Select Count(*) from CN_mxrusr_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].mxrusr_rec) Union               
              
SELECT  'CN','ortbki',(Select Count(*) from CN_ortbki_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortbki_rec) Union               
SELECT  'CN','apjglj',(Select Count(*) from CN_apjglj_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].apjglj_rec) Union               
SELECT  'CN','nctncf',(Select Count(*) from CN_nctncf_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].nctncf_rec) Union               
SELECT  'CN','ccratp',(Select Count(*) from CN_ccratp_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ccratp_rec) Union               
SELECT  'CN','pohphr',(Select Count(*) from CN_pohphr_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].pohphr_rec) Union               
SELECT  'CN','inrfrm',(Select Count(*) from CN_inrfrm_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrfrm_rec) Union               
SELECT  'CN','ortoit',(Select Count(*) from CN_ortoit_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortoit_rec) Union               
SELECT  'CN','ivtivs',(Select Count(*) from CN_ivtivs_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivtivs_rec) Union               
SELECT  'CN','ivjjpd',(Select Count(*) from CN_ivjjpd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivjjpd_rec) Union               
SELECT  'CN','apjjvc',(Select Count(*) from CN_apjjvc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].apjjvc_rec) Union               
SELECT  'CN','iptrps',(Select Count(*) from CN_iptrps_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].iptrps_rec) Union               
SELECT  'CN','scrcrx',(Select Count(*) from CN_scrcrx_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrcrx_rec) Union               
SELECT  'CN','cctcay',(Select Count(*) from CN_cctcay_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].cctcay_rec) Union               
SELECT  'CN','iptjbs',(Select Count(*) from CN_iptjbs_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].iptjbs_rec) Union               
SELECT  'CN','inrgrd',(Select Count(*) from CN_inrgrd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrgrd_rec) Union               
SELECT  'CN','iptjso',(Select Count(*) from CN_iptjso_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].iptjso_rec) Union               
              
SELECT  'CN','rvtres',(Select Count(*) from CN_rvtres_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].rvtres_rec) Union               
SELECT  'CN','ipjssh',(Select Count(*) from CN_ipjssh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ipjssh_rec) Union               
SELECT  'CN','potpoi',(Select Count(*) from CN_potpoi_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].potpoi_rec) Union               
SELECT  'CN','ortchl',(Select Count(*) from CN_ortchl_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortchl_rec) Union               
SELECT  'CN','cctctk',(Select Count(*) from CN_cctctk_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].cctctk_rec) Union               
SELECT  'CN','ortmbk',(Select Count(*) from CN_ortmbk_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortmbk_rec) Union               
SELECT  'CN','inrmat',(Select Count(*) from CN_inrmat_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrmat_rec) Union               
SELECT  'CN','ortord',(Select Count(*) from CN_ortord_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortord_rec) Union               
SELECT  'CN','orrprs',(Select Count(*) from CN_orrprs_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].orrprs_rec) Union               
SELECT  'CN','ortorh',(Select Count(*) from CN_ortorh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortorh_rec) Union               
SELECT  'CN','inrloc',(Select Count(*) from CN_inrloc_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrloc_rec) Union               
SELECT  'CN','ortorl',(Select Count(*) from CN_ortorl_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortorl_rec) Union               
SELECT  'CN','scrmpt',(Select Count(*) from CN_scrmpt_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrmpt_rec) Union               
SELECT  'CN','intpcr',(Select Count(*) from CN_intpcr_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].intpcr_rec) Union               
SELECT  'CN','pfhdpf',(Select Count(*) from CN_pfhdpf_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].pfhdpf_rec) Union               
SELECT  'CN','scrpyt',(Select Count(*) from CN_scrpyt_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrpyt_rec) Union               
SELECT  'CN','potpod',(Select Count(*) from CN_potpod_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].potpod_rec) Union               
SELECT  'CN','ivtivd',(Select Count(*) from CN_ivtivd_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivtivd_rec) Union               
SELECT  'CN','scrrsn',(Select Count(*) from CN_scrrsn_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrrsn_rec) Union               
SELECT  'CN','ortxre',(Select Count(*) from CN_ortxre_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ortxre_rec) Union               
              
SELECT  'CN','rprcds',(Select Count(*) from CN_rprcds_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].rprcds_rec) Union               
SELECT  'CN','sahstn',(Select Count(*) from CN_sahstn_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].sahstn_rec) Union               
SELECT  'CN','ivtivh',(Select Count(*) from CN_ivtivh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].ivtivh_rec) Union               
SELECT  'CN','mchqds',(Select Count(*) from CN_mchqds_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].mchqds_rec) Union               
SELECT  'CN','scrcva',(Select Count(*) from CN_scrcva_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrcva_rec) Union               
              
SELECT  'CN','scrcvt',(Select Count(*) from CN_scrcvt_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrcvt_rec) Union               
SELECT  'CN','nctnit',(Select Count(*) from CN_nctnit_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].nctnit_rec) Union               
SELECT  'CN','nctnhh',(Select Count(*) from CN_nctnhh_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].nctnhh_rec) Union              
SELECT  'CN','xcrixv',(Select Count(*) from CN_xcrixv_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].xcrixv_rec) Union                     
SELECT  'CN','xctava',(Select Count(*) from CN_xctava_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].xctava_rec) Union          
SELECT  'CN','inrmil',(Select Count(*) from CN_inrmil_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].inrmil_rec) Union          
SELECT  'CN','perppb',(Select Count(*) from CN_perppb_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].perppb_rec) Union      
SELECT  'CN','potpor',(Select Count(*) from CN_potpor_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].potpor_rec) Union              
SELECT  'CN','scrspv',(Select Count(*) from CN_scrspv_rec),(Select Count(*) from [LIVECNSTX].[livecnstxdb].[informix].scrspv_rec)       
                   
      End              
                    
                    
      INSERT INTO tblDailySynchronizationStatus(Prefix, tablename, localcount, livecount, pct,CreatedOn )                
      select Prefix,tablename,localcount,livecount, (Case When localcount = 0 then 0 else ((livecount-localcount)/localcount)*100 end) as pct, getdate() from #tmp  order by pct desc;                  
      drop table  #tmp;              
                    
END              
              
-- exec sp_itech_checkDailySynchronizationStatus 'US'              
              
-- exec [sp_itech_checkDailySynchronizationStatus] UK              
              
-- select * from tblDailySynchronizationStatus order by CreatedOn desc;

/*              
Date : 20170118              
Sub:Problem with replacement cost              
              
              
*/
GO
