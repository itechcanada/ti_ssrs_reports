USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_iptjob]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: jun 25, 2019   
-- Description: <Description,,>    
-- =============================================    
create PROCEDURE [dbo].[US_iptjob]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
    IF OBJECT_ID('dbo.US_iptjob_rec', 'U') IS NOT NULL    
  drop table dbo.US_iptjob_rec;    
            
SELECT *    
into  dbo.US_iptjob_rec    
FROM [LIVEUSSTX].[liveusstxdb].[informix].[iptjob_rec];    
    
END    
-- select * from US_iptjob_rec
GO
