USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetCustomerName_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Mayank >      
-- Create date: <11 Feb 2013>      
-- Description: <Getting top 50 customers for SSRS reports>      
-- =============================================      
CREATE PROCEDURE [dbo].[sp_itech_GetCustomerName_V1]  @DBNAME varchar(50), @SearchText Varchar(100)=''     
      
AS      
BEGIN      
       
 SET NOCOUNT ON;      
declare @sqltxt varchar(6000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(100)      
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(5);      
      
CREATE TABLE #tmp (  Value varchar(15)       
        ,text Varchar(100)      
        ,temp varchar(3)      
        )      
      
IF @DBNAME = 'ALL'      
 BEGIN      
       
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Prefix from tbl_itech_DatabaseName_PS       
    OPEN ScopeCursor;      
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
        DECLARE @query NVARCHAR(4000);         
           
      SET @query ='INSERT INTO #tmp ( Value,text,temp)      
                  select cus_cus_id as ''Value'',RTRIM(LTRIM(cus_cus_id)) + ''::'' +  cus_cus_nm as ''text'', ''C'' as temp  from  '+ @Prefix +'_arrcus_rec      
            
       '      
        print(@query);      
        EXECUTE sp_executesql @query;      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN          
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)      
                   select cus_cus_id as ''Value'',RTRIM(LTRIM(cus_cus_id)) + ''::'' + cus_cus_nm as ''text'', ''C'' as temp  from  '+ @DBNAME +'_arrcus_rec      
           
       Union       
       Select ''ALL'' as ''Value'',''All Customer'' as ''text'',''A'' as temp      
       Order by temp,text      
       '      
     print(@sqltxt);       
    set @execSQLtxt = @sqltxt;       
   EXEC (@execSQLtxt);      
      End      
            
      IF @DBNAME = 'ALL'      
 BEGIN      
      select Value,Replace(text,'IMPLANT-NT', 'IMPLANT-NT') as text, temp  from #tmp      
       Where text like '%' + @SearchText + '%'       
      Union       
   Select 'ALL' as 'Value','All Customer' as 'text','A' as temp      
   Order by temp,text      
      End      
      ELSE      
      BEGIN      
      select Value, Replace(text,'IMPLANT-NT', 'IMPLANT-NT') as text, temp from #tmp     
      Where text like '%' + @SearchText + '%'      
      END      
      drop table  #tmp      
END      
      
-- exec sp_itech_GetCustomerName_V1  'US' ,''    
--select * from US_arrcus_rec Where cus_cus_nm like '%Glaxo%'      
--select cus_cus_id as 'Value',cus_cus_nm as 'text', 'C' as temp  from  CA_arrcus_rec      
--       Union      
--       Select CAST(id as varchar(500)) as 'Value',UPPER(contractor_cust_nm) as 'text',      
--       'B' as temp from tbl_itech_CA_Sub_Contactor      
--       Union       
--       Select 'ALL' as 'Value','All Customer' as 'text','A' as temp      
--       Order by temp,text      
      
--
GO
