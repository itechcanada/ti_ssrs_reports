USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesJournal]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <25 Dec 2014>          
-- Description: <Getting result of Sales Journal Details >          
-- Last changes Date: 25 Dec 2014    
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database    
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_SalesJournal] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50)    
AS          
BEGIN          
           
           
 SET NOCOUNT ON;          
declare @sqltxt varchar(max)          
declare @execSQLtxt varchar(max)          
declare @DB varchar(100)        
declare @FD varchar(10)          
declare @TD varchar(10)          
DECLARE @CurrenyRate varchar(15)          
          
SET @DB=@DBNAME;          
          
CREATE TABLE #tmp (  InvoiceRef varchar(25)           
        ,TransPfx Varchar(2)          
        ,TransNo Varchar(20)
        ,PklistNo Varchar(10)          
        ,InvoicePfx Varchar(2)          
        ,InvDt varchar(15)  
        ,ShpItem Varchar(10)         
        ,CustomerName   varchar(30)
         ,OrdNo Varchar(50)
         ,OrdItem Varchar(10)
         ,OrdRslNo Varchar(10)
         ,Blg_pcs Varchar(10)
         ,BlgWgt Decimal(20,2)           
        ,DivRegBranch Varchar(15) 
        , Product   VARCHAR(250)           
        ,OtherSales  DECIMAL(20, 2)
        ,MtlValue DECIMAL(20, 2)            
        ,TotalValue DECIMAL(20, 2)          
        ,MtlPPct DECIMAL(20, 2)
        ,MtlPfValue DECIMAL(20, 2)          
        ,TotalPPct DECIMAL(20, 2)         
         ,Databases Varchar(10) 
                 );           
          
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(5);          
DECLARE @Name VARCHAR(15);          
Declare @Value as varchar(500);          
    
  --set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month          
  --set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month          
    set @FD = CONVERT(VARCHAR(10), @FromDate , 120)   --First day of previous month          
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) --Last Day of previous month
  
IF @DBNAME = 'ALL'          
 BEGIN          
          
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
        DECLARE @query NVARCHAR(4000);           
                  
        IF (UPPER(@Prefix) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@Prefix) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@Prefix) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@Prefix) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@Prefix) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End             
    Else if(UPPER(@Prefix) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End             
            
      SET @query = 'INSERT INTO #tmp (Databases,  InvoiceRef,TransPfx,TransNo,PklistNo, InvoicePfx,InvDt,ShpItem,CustomerName,OrdNo,OrdItem,OrdRslNo,
     Blg_pcs,BlgWgt,MtlValue,TotalValue,MtlPPct,TotalPPct,OtherSales,Product,DivRegBranch,MtlPfValue)          
       Select ''' + @Prefix + ''', jvh_upd_ref, jvs_transp_pfx , jvs_transp_no, jvs_pk_list_no,jvh_inv_pfx,jvh_inv_dt,jvd_ref_itm as shipmentItm,cus_cus_nm,jvd_ord_no,jvd_ord_itm,
       jvd_ord_rls_no, jvd_blg_pcs, ' 
       
       if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')      
       BEGIN      
       SET @query = @query + ' jvd_blg_wgt * 2.20462 as Weight, '      
       END      
       ELSE      
       BEGIN      
       SET @query = @query + ' jvd_blg_wgt as Weight, '      
       END 
            
        SET @query = @query + ' jct_tot_mtl_val * '+ @CurrenyRate +', jct_tot_val * '+ @CurrenyRate +',jit_mpft_avg_pct,jit_npft_avg_pct, (jct_tot_val - jct_tot_mtl_val) * '+ @CurrenyRate +',
		Rtrim(jpd_frm) + ' + '''/''' + ' + Rtrim(jpd_grd) + ' + '''/''' + ' + Rtrim(jpd_size) + ' + '''/''' + ' + Rtrim(jpd_fnsh), 
		brh_divn + ' + '''-''' + ' + brh_rgn + ' + '''-''' + ' + brh_brh,jit_mpft_avg_val * '+ @CurrenyRate +'
        from  ' + @Prefix + '_ivjjvs_rec
        join  ' + @Prefix + '_ivjjvd_rec on jvd_cmpy_id = jvs_cmpy_id and jvd_ref_pfx = jvs_ref_pfx and jvd_ref_no = jvs_ref_no
        join  ' + @Prefix + '_ivjjpd_rec on jpd_cmpy_id = jvd_cmpy_id and jpd_ref_pfx = jvd_ref_pfx and jpd_ref_no = jvd_ref_no and jpd_ref_itm = jvd_ref_itm
        join  ' + @Prefix + '_ivjjct_rec on jct_cmpy_id = jvd_cmpy_id and jct_ref_pfx = jvd_ref_pfx and jct_ref_no = jvd_ref_no and jct_ref_itm = jvd_ref_itm and jct_ref_itm <>0 and jct_ref_itm <>999 and jct_tot_typ= ''T''
        join  ' + @Prefix + '_ivjjit_rec on jit_cmpy_id = jvd_cmpy_id and jit_ref_pfx = jvd_ref_pfx and jit_ref_no = jvd_ref_no and jit_ref_itm = jvd_ref_itm and jit_ref_itm <>0 and jit_ref_itm <>999 
        join  ' + @Prefix + '_ivjjvh_rec  on  jvh_cmpy_id =jvs_cmpy_id and  jvh_upd_ref_no=jvs_upd_ref_no 
        join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = jvh_cmpy_id and jvh_sld_cus_id = cus_cus_id    
        join ' + @Prefix + '_scrbrh_rec on brh_cmpy_id = jvh_cmpy_id and brh_brh = jvh_inv_brh   
       where jvh_inv_dt >= '''+ @FD +''' and jvh_inv_dt <= '''+ @TD +''' '          
           
                  print(@query);          
        EXECUTE sp_executesql @query;          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN           
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')        
                
     IF (UPPER(@DB) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@DB) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@DB) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@DB) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@DB) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End            
    Else if(UPPER(@DB) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End            
            
          
     SET @sqltxt ='INSERT INTO #tmp (Databases, InvoiceRef,TransPfx,TransNo,PklistNo, InvoicePfx,InvDt,ShpItem,CustomerName,OrdNo,OrdItem,OrdRslNo,
     Blg_pcs,BlgWgt,MtlValue,TotalValue,MtlPPct,TotalPPct,OtherSales,Product,DivRegBranch,MtlPfValue)          
       Select ''' + @DB + ''', jvh_upd_ref, jvs_transp_pfx , jvs_transp_no, jvs_pk_list_no,jvh_inv_pfx,jvh_inv_dt,jvd_ref_itm as shipmentItm,cus_cus_nm,jvd_ord_no,jvd_ord_itm,
       jvd_ord_rls_no, jvd_blg_pcs,  '      
       if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
       BEGIN      
       SET @sqltxt = @sqltxt + ' jvd_blg_wgt * 2.20462 as Weight, '      
       END      
       ELSE      
       BEGIN      
       SET @sqltxt = @sqltxt + ' jvd_blg_wgt as Weight, '      
       END      
        SET @sqltxt = @sqltxt + ' jct_tot_mtl_val * '+ @CurrenyRate +', jct_tot_val * '+ @CurrenyRate +',jit_mpft_avg_pct,jit_npft_avg_pct, (jct_tot_val - jct_tot_mtl_val) * '+ @CurrenyRate +',
		Rtrim(jpd_frm) + ' + '''/''' + ' + Rtrim(jpd_grd) + ' + '''/''' + ' + Rtrim(jpd_size) + ' + '''/''' + ' + Rtrim(jpd_fnsh),      
       brh_divn + ' + '''-''' + ' + brh_rgn + ' + '''-''' + ' + brh_brh,jit_mpft_avg_val * '+ @CurrenyRate +'
        from  ' + @DB + '_ivjjvs_rec
        join  ' + @DB + '_ivjjvd_rec on jvd_cmpy_id = jvs_cmpy_id and jvd_ref_pfx = jvs_ref_pfx and jvd_ref_no = jvs_ref_no
        join  ' + @DB + '_ivjjpd_rec on jpd_cmpy_id = jvd_cmpy_id and jpd_ref_pfx = jvd_ref_pfx and jpd_ref_no = jvd_ref_no and jpd_ref_itm = jvd_ref_itm
        join  ' + @DB + '_ivjjct_rec on jct_cmpy_id = jvd_cmpy_id and jct_ref_pfx = jvd_ref_pfx and jct_ref_no = jvd_ref_no and jct_ref_itm = jvd_ref_itm and jct_ref_itm <>0 and jct_ref_itm <>999 and jct_tot_typ= ''T''
        join  ' + @DB + '_ivjjit_rec on jit_cmpy_id = jvd_cmpy_id and jit_ref_pfx = jvd_ref_pfx and jit_ref_no = jvd_ref_no and jit_ref_itm = jvd_ref_itm and jit_ref_itm <>0 and jit_ref_itm <>999 
        join  ' + @DB + '_ivjjvh_rec  on  jvh_cmpy_id =jvs_cmpy_id and  jvh_upd_ref_no=jvs_upd_ref_no 
        join ' + @DB + '_arrcus_rec on cus_cmpy_id = jvh_cmpy_id and jvh_sld_cus_id = cus_cus_id    
        join ' + @DB + '_scrbrh_rec on brh_cmpy_id = jvh_cmpy_id and brh_brh = jvh_inv_brh   
       where jvh_inv_dt >= '''+ @FD +''' and jvh_inv_dt <= '''+ @TD +''' '          
           
          --print(@sqltxt);          
     print(@DB);          
     print(@sqltxt);           
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);          
     END          
  SELECT * FROM #tmp           
  DROP TABLE #tmp          
          
END          
          
 -- exec sp_itech_SalesJournal '12/01/2014', '12/04/2014' ,'ALL' 
 
GO
