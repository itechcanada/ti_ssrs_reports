USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_FreightVendorActivtiyDetail]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_itech_FreightVendorActivtiyDetail] @DBNAME varchar(3), @VendorID varchar(8), @FromDate date, @ToDate date
As Begin

SET NOCOUNT ON;  
declare @sqltxt varchar(6000);      
declare @execSQLtxt varchar(7000); 
declare @updateFromDate varchar(10);
declare @updateToDate varchar(10);
DECLARE @CurrenyRate varchar(15);  

set @updateFromDate =  CONVERT(varchar(10), @FromDate,120);  
set @updateToDate = CONVERT(varchar(10), @ToDate,120);

Create table #tmpFv ( VendorID varchar(8)
					,TrPfx varchar(2)
					,TrNo int
					,CostAmt decimal(20,2)
					,CostUM varchar(3)
					,FuelCost decimal(20,2)
					,FuelCostUM varchar(3)
					,InvoiceNo int
					,ShipDate varchar(10)
					,HeaderCharge int
					,HeaderChargeAmt decimal(20,2)
					,LineChargeAmount decimal(20,2)
					,WeightLBS decimal(20,2)
					,SoldToCustomerID varchar(8)
					,PostalCode varchar(10)
					,CarrRefNo varchar(22)
					);
		

IF (UPPER(@DBNAME) = 'TW')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
    End      
    Else if (UPPER(@DBNAME) = 'CA')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
    End      
    Else if (UPPER(@DBNAME) = 'CN')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
    End      
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')      
    begin      
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
    End      
    Else if(UPPER(@DBNAME) = 'UK')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
    End      
    Else if(UPPER(@DBNAME) = 'DE')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
    End      
    
set @sqltxt = ' insert into #tmpFV (VendorID,TrPfx,TrNo,CostAmt,CostUM,FuelCost,FuelCostUM,InvoiceNo,ShipDate,HeaderCharge,HeaderChargeAmt,LineChargeAmount,
WeightLBS,SoldToCustomerID,PostalCode,CarrRefNo)

Select tph_frt_ven_id           AS VendorID, 
       jvs_transp_pfx           AS TrPfx, 
		   jvs_transp_no            AS TrNo, 
		   tph_frt_cst * '+ @CurrenyRate +'          AS CostAmt, 
		   tph_frt_cst_um           AS CostUM, 
		   tph_fl_cst               AS FuelCost, 
		   tph_fl_cst_um            AS FuelCostUM, 
		   jvs_upd_ref_no           AS InvoiceNo, 
		   jvs_upd_dt               AS Shipdate, 
		   jch_chrg_no              AS HeaderCharge, 
		   jch_chrg_val * '+ @CurrenyRate +'         AS HeaderChargeAmt, 
		   jcl_chrg_val * '+ @CurrenyRate +'        AS LineChargeAmount, 
		   tph_cls_frt_wgt          AS Weight, 
		   jvs_sld_cus_id           AS SoldToCustomerID, 
		   (SELECT TOP 1 nad_pcd 
			FROM   ' + @DBNAME +'_tctnad_rec
				   JOIN ' + @DBNAME +'_sahsat_rec 
					 ON sat_cmpy_id = nad_cmpy_id 
						AND nad_ref_pfx = ''SO'' 
						AND nad_ref_no = sat_ord_no 
						AND nad_addr_typ = ''S'' 
						AND sat_cmpy_id = jvs_cmpy_id 
						AND sat_transp_pfx = jvs_transp_pfx 
						AND sat_transp_no = jvs_transp_no 
						AND nad_pcd IS NOT NULL 
			ORDER  BY nad_pcd DESC) AS PostalCode ,
			tph_crr_ref_no
	FROM   ' + @DBNAME +'_ivjjvs_rec 
		   LEFT JOIN ' + @DBNAME +'_trjtph_rec 
				  ON tph_cmpy_id = jvs_cmpy_id 
					 AND tph_transp_pfx = jvs_transp_pfx 
					 AND tph_transp_no = jvs_transp_no 
		   LEFT JOIN ' + @DBNAME +'_ivjjch_rec 
				  ON jch_cmpy_id = jvs_cmpy_id 
					 AND jch_upd_ref_no = jvs_upd_ref_no 
					 AND jch_chrg_no = 3 
		   LEFT JOIN ' + @DBNAME +'_ivjjcl_rec 
				  ON jcl_cmpy_id = jvs_cmpy_id 
					 AND jcl_ref_pfx = jvs_ref_pfx 
					 AND jcl_ref_no = jvs_ref_no 
					 AND jcl_upd_dt = jvs_upd_dt 
					 AND jcl_chrg_no = 3 
	WHERE  ( tph_frt_ven_id = '''+ @VendorID +''' OR '''+ @VendorID +''' = '''' ) 
		   AND CONVERT(VARCHAR(10), jvs_upd_dt, 120) BETWEEN ''' + @updateFromDate + ''' AND ''' + @updateToDate + '''; '

print(@sqltxt)      
 set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);  
 
SELECT * FROM #tmpFv  where VendorID is not null;      
 DROP TABLE  #tmpFv;      

End

/*
-- exec sp_itech_FreightVendorActivtiyDetail 'US', '2648', '2019-01-01', '2019-06-15'
exec sp_itech_FreightVendorActivtiyDetail 'UK', '', '2019-01-01', '2019-06-15'
*/
GO
