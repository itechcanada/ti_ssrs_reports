USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ortmbk]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,10/5/2012,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[US_ortmbk]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.US_ortmbk_rec ;	
	
Insert into dbo.US_ortmbk_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortmbk_rec]

END
GO
