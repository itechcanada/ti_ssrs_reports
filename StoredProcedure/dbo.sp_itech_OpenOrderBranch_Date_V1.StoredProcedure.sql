USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenOrderBranch_Date_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- Author:  <Sumit>                      
-- Create date: <30 Sept 2020>                      
-- Description: <Getting Total Open Order for branch with Date>                      
CREATE PROCEDURE [dbo].[sp_itech_OpenOrderBranch_Date_V1] (@DBNAME varchar(50), @FromDate datetime, @ToDate datetime )      
as      
Begin      
      
SET NOCOUNT ON;                      
declare @execSQLtxt nvarchar(max)                      
declare @DB varchar(50)                      
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @Prefix VARCHAR(5);         
DECLARE @Name VARCHAR(15);        
DECLARE @CurrenyRate varchar(15);                          
      
CREATE TABLE #tmp (        
 DataBases varchar(5)                       
 ,Branch Varchar(10)                      
    ,ActivityDate Date                      
    ,TotalOrder   int   
 ,TotalOrderValue decimal(13,2)  
  );      
      
 set @DB=  @DBNAME                                                     
 IF @DBNAME = 'ALL'                                                            
 BEGIN                                                            
 DECLARE ScopeCursor CURSOR FOR                                                
 select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_V2                                                 
 OPEN ScopeCursor;                                                
 FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                      
 WHILE @@FETCH_STATUS = 0                                                            
 BEGIN                                                            
  SET @DB= @Prefix         
  
  IF (UPPER(@DB) = 'TW')                        
  begin                        
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))                        
  End                        
  Else if (UPPER(@DB) = 'NO')                        
  begin                        
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))                        
  End               
  Else if (UPPER(@DB) = 'CA')                        
  begin                        
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))                        
  End                        
  Else if (UPPER(@DB) = 'CN')                        
  begin                        
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))                        
  End                        
  Else if (UPPER(@DB) = 'US' OR UPPER(@Prefix) = 'PS')                        
  begin              
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))                        
  End                        
  Else if(UPPER(@DB) = 'UK')                
  begin                        
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))                        
  End  
  Else if(UPPER(@DB) = 'DE')                                    
  begin                                    
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))                                    
  End     
  print (@DB +'-'+ @CurrenyRate)  
  
  set @execSQLtxt = 'insert into #tmp (Databases, Branch,ActivityDate,TotalOrder,TotalOrderValue)      
  select '''+ @DB +''', bka_brh, bka_actvy_dt, count(distinct bka_ord_no) as TotalOrder, (sum(ISNULL(bka_tot_val,0)) * '''+ @CurrenyRate +''') as TotalValue from '+ @DB +'_ortbka_rec where      
  (CONVERT(varchar(10),bka_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and         
  (CONVERT(varchar(10),bka_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')        
  and bka_trs_md = ''A'' and bka_ord_pfx = ''SO'' and bka_ord_itm != 999  group by bka_brh, bka_actvy_dt'      
      
  print @execSQLtxt      
  EXECUTE sp_executesql @execSQLtxt;                                                        
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                            
 END      
 CLOSE ScopeCursor;                                                            
 DEALLOCATE ScopeCursor;                                              
 END      
 Else      
 Begin  
   
 IF (UPPER(@DB) = 'TW')                        
 begin                        
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))                        
 End                        
 Else if (UPPER(@DB) = 'NO')                        
 begin                        
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))                        
 End               
 Else if (UPPER(@DB) = 'CA')                        
 begin                        
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))                        
 End                        
 Else if (UPPER(@DB) = 'CN')                        
 begin                        
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))                        
 End                        
 Else if (UPPER(@DB) = 'US' OR UPPER(@Prefix) = 'PS')                        
 begin              
  SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))                        
 End                        
 Else if(UPPER(@DB) = 'UK')                
 begin                        
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))                        
 End   
 Else if(UPPER(@DB) = 'DE')                                    
 begin                                    
  SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))                                    
 End  
 print (@DB +'-'+ @CurrenyRate)  
  
 set @execSQLtxt = 'insert into #tmp (Databases, Branch,ActivityDate,TotalOrder,TotalOrderValue)      
 select '''+ @DB +''', bka_brh, bka_actvy_dt, count(distinct bka_ord_no) as TotalOrder, (sum(ISNULL(bka_tot_val,0)) * '''+ @CurrenyRate +''') as TotalValue from '+ @DB +'_ortbka_rec where      
 (CONVERT(varchar(10),bka_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and         
 (CONVERT(varchar(10),bka_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')        
 and bka_trs_md = ''A'' and bka_ord_pfx = ''SO'' and bka_ord_itm != 999  group by bka_brh, bka_actvy_dt'      
      
 print @execSQLtxt      
 EXECUTE sp_executesql @execSQLtxt;       
 End      
      
 select databases, Branch, convert(varchar(10), ActivityDate, 103) as OrderDate, TotalOrder, TotalOrderValue from #tmp order by ActivityDate    
      
 Drop table #tmp      
End      
      
/*      
exec sp_itech_OpenOrderBranch_Date_Test 'US', '2020-07-01', '2020-09-29'      
exec sp_itech_OpenOrderBranch_Date 'ALL', '2020-09-01', '2020-09-29'      
exec sp_itech_OpenOrderBranch_Date_V1 'ALL', '2020-10-19', '2020-11-18'     
2020/11/18 Sumit  
Add germany database, add order total value  
Mail: Fwd: Orders.xlsx  
*/  
  
  
  
  
  
  
GO
