USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_JobYield_Summary_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================                  
-- Author:  <Mukesh >                  
-- Create date: <29 NOV 2018>                  
-- Description: <Getting top 50 customers for SSRS reports>                  
-- =============================================                  
CREATE PROCEDURE [dbo].[sp_itech_JobYield_Summary_V1]  @ActivityFromDate datetime, @ActivityToDate datetime, @DBNAME varchar(50), @PRS VARCHAR(50)                 
                  
AS                  
BEGIN                  
                   
                   
 SET NOCOUNT ON;                  
declare @sqltxt1 varchar(8000)                  
declare @sqltxt2 varchar(8000)                  
declare @execSQLtxt varchar(7000)                  
declare @DB varchar(100)                  
declare @NOOfCust varchar(15)                  
DECLARE @ExchangeRate varchar(15)                         
DECLARE @CurrenyRate varchar(15)                
declare @FD varchar(10)                  
declare @TD varchar(10)                  
set @FD = CONVERT(VARCHAR(10), @ActivityFromDate , 120)                  
set @TD = CONVERT(VARCHAR(10), @ActivityToDate , 120)                  
                  
set @DB=  @DBNAME                  
IF @PRS = 'ALL'                  
BEGIN                  
SET @PRS = ''                  
END                  
                  
                  
CREATE TABLE #tmp ( [Database]   VARCHAR(10)                 
  ,branch Varchar(3)                 
  , actvy_whs   VARCHAR(3)                  
        , actvy_dt   datetime                  
        , pwc    Varchar(3)                  
        , lgn_id   Varchar(8)                  
        , salesOrder Varchar(12)                
        , transportNo Varchar(8)                
        , trh_prs_1   Varchar(3)                  
        , trh_prs_2   Varchar(3)                   
        , pcs  int                  
        , costVal Decimal(20,2)                
        , trt_tprod_wgt  int                  
        , metalVarl Decimal(20,2)                
        , netprofit Decimal (20,2)                
       -- , netprofitPCT Decimal (20,2)                
       , netprofitDoller Decimal (20,2)                
                 );                   
                                                  
                  
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @Prefix VARCHAR(35);                  
DECLARE @Name VARCHAR(15);                  
                  
IF @DBNAME = 'ALL'                  
 BEGIN                  
                    
  DECLARE ScopeCursor CURSOR FOR                  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                    
    OPEN ScopeCursor;                  
                  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
     WHILE @@FETCH_STATUS = 0                  
       BEGIN                  
        DECLARE @query1 nVARCHAR(max);                     
        DECLARE @query2 nVARCHAR(max);                     
      SET @DB= @Prefix                   
                      
      IF (UPPER(@DB) = 'TW')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                
   End                                
   Else if (UPPER(@DB) = 'NO')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                
   End                                
   Else if (UPPER(@DB) = 'CA')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                        
   End                                
   Else if (UPPER(@DB) = 'CN')                                
   begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                           
 End                                
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                
   begin       
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                
   End                                
   Else if(UPPER(@DB) = 'UK')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                
 End                    
   Else if(UPPER(@DB) = 'DE')                                
   begin                                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                
 End                    
                   
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
        Begin                  
           SET @query1 ='INSERT INTO #tmp ([Database],branch, actvy_whs, actvy_dt,pwc, lgn_id, salesOrder,transportNo, trh_prs_1, trh_prs_2, pcs,costVal,trt_tprod_wgt                  
             , metalVarl,netprofit,netprofitDoller)                  
             SELECT distinct ''' +  @DB + ''' as [Database],case When orh_sld_cus_id = ''5833'' then ''MTL'' When orh_sld_cus_id = ''11980'' then ''TAI''                 
             When orh_sld_cus_id = ''2046'' then ''SHA'' When orh_sld_cus_id = ''10993'' then ''BHM'' else  orh_ord_brh end as branch,trh_actvy_whs,trh_actvy_dt,                
             trh_pwc,ith_upd_lgn_id,Cast(stn_ord_no as varchar(8)) + ''-''+ Cast(stn_ord_itm as varchar(3)),stn_transp_no,trh_prs_1,trh_prs_2,stn_blg_pcs,                
            (select Sum(ISNULL(tdc_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_injtdc_rec join ' + @DB + '_ctrccr_rec on tdc_cst_no = ccr_cc_no where tdc_cmpy_id = stn_cmpy_id and tdc_ref_pfx = trh_ref_pfx    
   and tdc_ref_no = trh_ref_no and tdc_ref_itm = trh_ref_itm and ccr_cc_grp =''PRS'' and ccr_cc_sub_grp = trh_prs_1),                
   ROUND(stn_blg_wgt*2.20462,0),stn_mtl_avg_val* '+ @CurrenyRate +', stn_tot_avg_val* '+ @CurrenyRate +' ,stn_npft_avg_val* '+ @CurrenyRate +'                
   From ' + @DB + '_sahstn_rec                
   join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no                 
   join ' + @DB + '_trjtud_rec on tud_cmpy_id = stn_cmpy_id and tud_sprc_pfx = stn_sprc_pfx and tud_sprc_no = stn_sprc_no                
   join ' + @DB + '_ipjtrh_rec on trh_cmpy_id = tud_cmpy_id and trh_jbs_pfx = tud_jbs_pfx and trh_jbs_no = tud_jbs_no                
   join ' + @DB + '_injith_rec on trh_cmpy_id = ith_cmpy_id AND trh_ref_pfx = ith_ref_pfx and trh_ref_no = ith_ref_no AND trh_ref_itm = ith_ref_itm                
    AND trh_ref_sbitm = ith_ref_sbitm                
                   
   WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' ) ;'                                     
        End                  
        Else                  
        Begin                  
           SET @query1 ='INSERT INTO #tmp ([Database],branch, actvy_whs, actvy_dt,pwc, lgn_id, salesOrder,transportNo, trh_prs_1, trh_prs_2, pcs,costVal,trt_tprod_wgt                  
             , metalVarl,netprofit,netprofitDoller)                  
             SELECT distinct ''' +  @DB + ''' as [Database],case When orh_sld_cus_id = ''5833'' then ''MTL'' When orh_sld_cus_id = ''11980'' then ''TAI''                 
             When orh_sld_cus_id = ''2046'' then ''SHA'' When orh_sld_cus_id = ''10993'' then ''BHM'' else  orh_ord_brh end as branch,trh_actvy_whs,trh_actvy_dt,                
             trh_pwc,ith_upd_lgn_id,Cast(stn_ord_no as varchar(8)) + ''-''+ Cast(stn_ord_itm as varchar(3)),stn_transp_no,trh_prs_1,trh_prs_2,stn_blg_pcs,                
             (select Sum(ISNULL(tdc_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_injtdc_rec join ' + @DB + '_ctrccr_rec on tdc_cst_no = ccr_cc_no where tdc_cmpy_id = stn_cmpy_id and tdc_ref_pfx = trh_ref_pfx    
 and tdc_ref_no = trh_ref_no and tdc_ref_itm = trh_ref_itm and ccr_cc_grp =''PRS'' and ccr_cc_sub_grp = trh_prs_1),                  
   ROUND(stn_blg_wgt,0),stn_mtl_avg_val* '+ @CurrenyRate +', stn_tot_avg_val* '+ @CurrenyRate +' ,stn_npft_avg_val* '+ @CurrenyRate +'                
   From ' + @DB + '_sahstn_rec                
   join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no                 
   join ' + @DB + '_trjtud_rec on tud_cmpy_id = stn_cmpy_id and tud_sprc_pfx = stn_sprc_pfx and tud_sprc_no = stn_sprc_no       
   join ' + @DB + '_ipjtrh_rec on trh_cmpy_id = tud_cmpy_id and trh_jbs_pfx = tud_jbs_pfx and trh_jbs_no = tud_jbs_no                
   join ' + @DB + '_injith_rec on trh_cmpy_id = ith_cmpy_id AND trh_ref_pfx = ith_ref_pfx and trh_ref_no = ith_ref_no AND trh_ref_itm = ith_ref_itm                
    AND trh_ref_sbitm = ith_ref_sbitm                
                   
   WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' );'                    
        End                  
                  
       print @query1;                  
        EXECUTE sp_executesql @query1;  --@query2                  
                       
                          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
       END                   
    CLOSE ScopeCursor;                  
    DEALLOCATE ScopeCursor;                  
  END               
  ELSE                  
     BEGIN                  
                     
     IF (UPPER(@DB) = 'TW')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                
    End                                
    Else if (UPPER(@DB) = 'NO')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                
    End                                
    Else if (UPPER(@DB) = 'CA')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                
    End                                
    Else if (UPPER(@DB) = 'CN')                                
    begin                                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                
    End                                
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                
    begin                                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                
    End                                
    Else if(UPPER(@DB) = 'UK')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                
    End                 
    Else if(UPPER(@DB) = 'DE')                                
    begin                                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                
    End                 
                    
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
   Begin                  
       SET @sqltxt1 ='INSERT INTO #tmp ([Database],branch, actvy_whs, actvy_dt,pwc, lgn_id, salesOrder,transportNo, trh_prs_1, trh_prs_2, pcs,costVal,trt_tprod_wgt                  
             , metalVarl,netprofit,netprofitDoller)                  
             SELECT distinct ''' +  @DB + ''' as [Database],case When orh_sld_cus_id = ''5833'' then ''MTL'' When orh_sld_cus_id = ''11980'' then ''TAI''                 
             When orh_sld_cus_id = ''2046'' then ''SHA'' When orh_sld_cus_id = ''10993'' then ''BHM'' else  orh_ord_brh end as branch,trh_actvy_whs,trh_actvy_dt,                
             trh_pwc,ith_upd_lgn_id,Cast(stn_ord_no as varchar(8)) + ''-''+ Cast(stn_ord_itm as varchar(3)),stn_transp_no,trh_prs_1,trh_prs_2,stn_blg_pcs,                
             (select Sum(ISNULL(tdc_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_injtdc_rec join ' + @DB + '_ctrccr_rec on tdc_cst_no = ccr_cc_no where tdc_cmpy_id = stn_cmpy_id and tdc_ref_pfx = trh_ref_pfx    
and tdc_ref_no = trh_ref_no and tdc_ref_itm = trh_ref_itm and ccr_cc_grp =''PRS'' and ccr_cc_sub_grp = trh_prs_1),               
   ROUND(stn_blg_wgt*2.20462,0),stn_mtl_avg_val*'+ @CurrenyRate +', stn_tot_avg_val*'+ @CurrenyRate +' ,stn_npft_avg_val* '+ @CurrenyRate +'                
   From ' + @DB + '_sahstn_rec                
   join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no                 
   join ' + @DB + '_trjtud_rec on tud_cmpy_id = stn_cmpy_id and tud_sprc_pfx = stn_sprc_pfx and tud_sprc_no = stn_sprc_no             
   join ' + @DB + '_ipjtrh_rec on trh_cmpy_id = tud_cmpy_id and trh_jbs_pfx = tud_jbs_pfx and trh_jbs_no = tud_jbs_no                
   join ' + @DB + '_injith_rec on trh_cmpy_id = ith_cmpy_id AND trh_ref_pfx = ith_ref_pfx and trh_ref_no = ith_ref_no AND trh_ref_itm = ith_ref_itm                
    AND trh_ref_sbitm = ith_ref_sbitm                
                   
   WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' ) ;'                  
   End                  
   Else                  
   Begin                  
                  
        SET @sqltxt1 ='INSERT INTO #tmp ([Database],branch, actvy_whs, actvy_dt,pwc, lgn_id, salesOrder,transportNo, trh_prs_1, trh_prs_2, pcs,costVal,trt_tprod_wgt                  
             , metalVarl,netprofit,netprofitDoller)                  
             SELECT distinct ''' +  @DB + ''' as [Database],case When orh_sld_cus_id = ''5833'' then ''MTL'' When orh_sld_cus_id = ''11980'' then ''TAI''                 
             When orh_sld_cus_id = ''2046'' then ''SHA'' When orh_sld_cus_id = ''10993'' then ''BHM'' else  orh_ord_brh end as branch,trh_actvy_whs,trh_actvy_dt,                
             trh_pwc,ith_upd_lgn_id,Cast(stn_ord_no as varchar(8)) + ''-''+ Cast(stn_ord_itm as varchar(3)),stn_transp_no,trh_prs_1,trh_prs_2,stn_blg_pcs,                
              (select Sum(ISNULL(tdc_bas_cry_val,0))* '+ @CurrenyRate +' from ' + @DB + '_injtdc_rec join ' + @DB + '_ctrccr_rec on tdc_cst_no = ccr_cc_no where tdc_cmpy_id = stn_cmpy_id and tdc_ref_pfx = trh_ref_pfx    
 and tdc_ref_no = trh_ref_no and tdc_ref_itm = trh_ref_itm and ccr_cc_grp =''PRS'' and ccr_cc_sub_grp = trh_prs_1),   
   ROUND(stn_blg_wgt,0),stn_mtl_avg_val* '+ @CurrenyRate +', stn_tot_avg_val* '+ @CurrenyRate +' ,stn_npft_avg_val* '+ @CurrenyRate +'                
   From ' + @DB + '_sahstn_rec                
   join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no                 
   join ' + @DB + '_trjtud_rec on tud_cmpy_id = stn_cmpy_id and tud_sprc_pfx = stn_sprc_pfx and tud_sprc_no = stn_sprc_no                
   join ' + @DB + '_ipjtrh_rec on trh_cmpy_id = tud_cmpy_id and trh_jbs_pfx = tud_jbs_pfx and trh_jbs_no = tud_jbs_no                
   join ' + @DB + '_injith_rec on trh_cmpy_id = ith_cmpy_id AND trh_ref_pfx = ith_ref_pfx and trh_ref_no = ith_ref_no AND trh_ref_itm = ith_ref_itm                
    AND trh_ref_sbitm = ith_ref_sbitm   
   WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' );'                   
   End                  
                            
     print(@sqltxt1)                   
    --set @execSQLtxt = @sqltxt;                   
   EXEC (@sqltxt1 );                  
   END                  
                     
select * from #tmp                    
 where (trh_prs_1 = '' + @PRS + '' OR '' + @PRS + '' = '')  --and transportNo = '253620'                
 --and trh_prs_1 in ('WJT')  -- and costVal >0 --,'PSW','WJT'            
 order by  [Database], actvy_whs, actvy_dt                
             
               
   drop table #tmp;                  
                     
END                  
                  
-- exec [sp_itech_JobYield_Summary] '2018-10-01','2018-10-31','UK', 'ALL'                 
                  
-- exec [sp_itech_JobYield_Summary] '2018-01-01','2018-12-31','US', 'ALL' -- 12484                
/*                
Date:20190128                
Mail Sub: Job Yield Summary Report.xls                
I removed stn_npft_avg_pct and add stn_npft_avg_val  
20201123	Sumit
join ctrccr_rec on the basis of tdc_cst_no = ccr_cc_no for process cost
Mail: RE: Job Yield Summary Report
*/
GO
