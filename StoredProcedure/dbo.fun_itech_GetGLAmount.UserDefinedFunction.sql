USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_GetGLAmount]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh>
-- Create date: <2017-03-16>
-- Description:	<Get GL Amount>  

-- =============================================
CREATE FUNCTION [dbo].[fun_itech_GetGLAmount] 
(
	@DB Varchar(3), @bscGLAcct Varchar(100), @gldSacct Varchar(100), @FDMTD VArchar(10)
)
RETURNS decimal(20,2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @glAmount decimal(20,2);
declare @sqltxt varchar(6000)   ;
	-- Add the T-SQL statements to compute the return value here
	SET @sqltxt = ' select @glAmount = SUM(gld_cr_amt) - SUM(gld_dr_amt) from '+ @DB +'_glhgld_rec
	 where gld_bsc_gl_acct  in ( ' + @bscGLAcct + ') and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + '
and gld_sacct = ' + @gldSacct + ''
exec @sqltxt;
	-- Return the result of the function
	RETURN @glAmount;

END
GO
