USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TOP_ALLOYS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,1/11/2012>
-- Description:	<Description,Top 10 Alloys>
-- =============================================
CREATE PROCEDURE [dbo].[TOP_ALLOYS]
	-- Add the parameters for the stored procedure here
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT TOP 10 WITH TIES 
b.sat_grd,
'3 Months'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) in (0,1,2) THEN b.sat_blg_wgt END),
'6 Months'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) in (0,1,2,3,4,5) THEN b.sat_blg_wgt END),
'12 Months'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) in (0,1,2,3,4,5,6,7,8,9,10,11) THEN b.sat_blg_wgt END)
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcus_rec] a 
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE 
a.cus_actv=1 AND
b.sat_inv_pfx='SE' AND
a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) in (0,1,2,3,4,5,6,7,8,9,10,11)
AND b.sat_grd IN ('13-8','15-5','1537','316L','316LVM', '625', '625 PLUS','6B','718','S240')
GROUP BY b.sat_grd
ORDER BY SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) in (0,1,2) THEN b.sat_blg_wgt END) DESC

END

GO
