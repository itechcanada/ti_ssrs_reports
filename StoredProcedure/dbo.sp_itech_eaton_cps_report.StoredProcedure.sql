USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_eaton_cps_report]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Author,Mrnal Jha>          
-- Create date: <Create Date,29/10/2012,>         
-- Last updated By mrinal       
-- Last updated Date Oct 05, 2015       
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_eaton_cps_report]           
           
AS          
BEGIN        
    
    
                
 Select clg_cus_ven_id as primaryCustNo, clg_part as PartNo, cpr_part_desc25 as partDesc, chp_chrg as charg, chp_chrg_um as chargUM from     
 US_cprclg_rec join US_cprcpr_rec on     
 cpr_cmpy_id = clg_cmpy_id and cpr_part_sts = clg_part_sts and cpr_part_ctl_no = clg_part_ctl_no    
 JOIN US_cprchp_rec ON chp_cmpy_id = clg_cmpy_id AND chp_part_sts = clg_part_sts AND chp_part_ctl_no = clg_part_ctl_no    
 Where clg_cus_ven_id IN ('8960','ROMAA','293','612','1684','2603','2826','3770','5307','5487','7627','12651','ADTUA','AEMAA','CLMEA','CLTUA','DISCA','EAJAA','EDMAA','INAEA','KAMAA','LC11900','LCO8000','MACOA','PEMAA','SCMAA','TUMAA','855','10120','11930','12088','12217','12645','6302','9309','12681','13211','13219','141','2373','654','9890','3451','5567','5724','6010','614','7566','522','1089','1242','1749','1806','2372','2382','2631','3532','4242','4457','12535','4894','9886')    
END        
    
-- exec [sp_itech_eaton_cps_report]       
/*  
20170504  
sub:ACCT# 9886 EATON GLENOLDEN, PA  
-- add 9886  in eaton group  
*/
GO
