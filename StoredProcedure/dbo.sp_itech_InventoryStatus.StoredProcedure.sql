USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_InventoryStatus]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <17 Apr 2017>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_InventoryStatus]   
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  Value varchar(15)   
        ,text Varchar(100)  
        ,temp varchar(3)  
        )  
  
     
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)  
                    Select ''R'' as ''Value'',''Released'' as ''text'',''B'' as temp  
       union 
        Select ''D'' as ''Value'',''Drop'' as ''text'',''B'' as temp  
       union 
        Select ''P'' as ''Value'',''Planned'' as ''text'',''B'' as temp  
        union 
        Select ''S'' as ''Value'',''Stock'' as ''text'',''B'' as temp  
        union 
        Select ''I'' as ''Value'',''Incoming'' as ''text'',''B'' as temp  
        union 
        Select ''N'' as ''Value'',''Non Specific'' as ''text'',''B'' as temp  
       Union   
       Select ''ALL'' as ''Value'',''All Inventory'' as ''text'',''A'' as temp  
       Order by temp,text  
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
        
   select * from #tmp;
 
      drop table  #tmp 
END  
  
-- exec sp_itech_InventoryStatus  
  
  /*
  2016-05-19
  Changed by mukesh
  */
GO
