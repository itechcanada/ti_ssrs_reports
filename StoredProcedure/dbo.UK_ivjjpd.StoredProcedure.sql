USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ivjjpd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[UK_ivjjpd]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_ivjjpd_rec', 'U') IS NOT NULL
		drop table dbo.UK_ivjjpd_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.UK_ivjjpd_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[ivjjpd_rec] ; 
  
END
-- select * from UK_ivjjpd_rec 
GO
