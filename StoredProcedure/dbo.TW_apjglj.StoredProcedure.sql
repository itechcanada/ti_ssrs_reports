USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_apjglj]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 6, 2015,>
-- Description:	<Description,gl account,>

-- =============================================
create PROCEDURE [dbo].[TW_apjglj]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_apjglj_rec', 'U') IS NOT NULL
		drop table dbo.TW_apjglj_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.TW_apjglj_rec
  from [LIVETWSTX].[livetwstxdb].[informix].[apjglj_rec]; 
  
END
-- select * from TW_apjglj_rec
GO
