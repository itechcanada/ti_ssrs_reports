USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_LTA_Inventory]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
-- =============================================                    
-- Author:  <Mukesh>                    
-- Create date: <2019-07-19>                    
-- Description: <LTA Inventory Reports>                   
-- Last updated by Mukesh                   
-- =============================================                    
CREATE PROCEDURE [dbo].[sp_itech_LTA_Inventory]  @AverageCost decimal(20,2), @PriorYearUsage decimal(20,2), @YTDUsage decimal(20,2),               
@TotalStockAvailable decimal(20,2), @BranchStockAvailable decimal(20,2), @IncomingStock decimal(20,2), @NextIncomingQuantity decimal(20,2),              
@NextIncomingDate date              
AS                    
BEGIN                    
                  
 -- SET NOCOUNT ON added to prevent extra result sets from                    
 -- interfering with SELECT statements.                    
 SET NOCOUNT ON;                    
declare @DB varchar(100);                    
declare @sqltxt varchar(6000);                    
declare @execSQLtxt varchar(7000);                    
DECLARE @CountryName VARCHAR(25);                       
DECLARE @prefix VARCHAR(15);                       
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @CurrenyRate varchar(15);                      
declare @PYFD varchar(10)                        
declare @PYTD varchar(10)                    
declare @YTFD varchar(10)                        
declare @YTTD varchar(10)                 
              
set @PYFD = CONVERT(VARCHAR(10), DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0),120)                    
 set @PYTD = CONVERT(VARCHAR(10), DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)),120)                 
 set @YTFD = CONVERT(VARCHAR(10), DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),120)                    
 set @YTTD = CONVERT(VARCHAR(10), GETDATE(),120)                 
                    
IF @AverageCost is null                    
 BEGIN                    
  set @AverageCost = 0                    
 END              
         
IF @PriorYearUsage is null                    
 BEGIN                    
  set @PriorYearUsage = 0                    
 END           
         
 IF @YTDUsage is null                    
 BEGIN                    
  set @YTDUsage = 0                    
 END           
        
IF @TotalStockAvailable is null                    
 BEGIN                    
  set @TotalStockAvailable = 0                    
 END          
         
IF @BranchStockAvailable is null                    
 BEGIN                    
  set @BranchStockAvailable = 0                    
 END             
         
IF @IncomingStock is null                    
 BEGIN                    
  set @IncomingStock = 0                    
 END              
             
IF @NextIncomingQuantity is null                    
 BEGIN                    
  set @NextIncomingQuantity = 0                    
 END            
         
 IF @NextIncomingDate is null                    
 BEGIN                    
  set @NextIncomingDate = '2019-01-01'                    
 END          
                  
 --print '@Branch =' + @Branch;                  
 declare @start varchar = ''                  
-- SET @start = @Month;                   
-- print  @start;                  
 --PRINT CONVERT(VARCHAR(10), @Month, 120)                  
                    
CREATE TABLE #temp ( --Dbname   VARCHAR(10)                    
     ShortTIPN VARCHAR(40)                   
     ,AverageCost  Decimal(20,2)                 
     ,PriorYearUsage Decimal(20,2)                     
     ,YTDUsage Decimal(20,2)                    
     ,TotalStockAvailable Decimal(20,2)                    
     ,ROCStockAvailable Decimal(20,2)                    
     ,IncomingStock Decimal(20,2)                    
     ,NextIncomingQuantity Decimal(20,2)                 
     ,NextIncomingDate varchar(10)    
     ,SixMonthUsage Decimal(20,2)    
     ,ExcessInventory Decimal(20,2)   
     ,LAXStockAvailable Decimal(20,2)     
     --,CustID Varchar(15)         
     --,CustName Varchar(35)                 
     );                    
           
               
--IF @DBNAME = 'ALL'                    
-- BEGIN                    
                   
--  DECLARE ScopeCursor CURSOR FOR                    
--   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                     
--    OPEN ScopeCursor;                    
                      
--  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                    
--  WHILE @@FETCH_STATUS = 0                    
--  BEGIN                    
--   DECLARE @query NVARCHAR(MAX);                    
--   IF (UPPER(@Prefix) = 'TW')                    
--    begin                    
--       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
--    End                    
--    Else if (UPPER(@Prefix) = 'NO')                    
--    begin                    
--     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
--    End                    
--    Else if (UPPER(@Prefix) = 'CA')                    
--    begin                    
--     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
--    End                    
--    Else if (UPPER(@Prefix) = 'CN')                    
--    begin                    
--     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
--    End                    
--    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                    
--    begin                    
--     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
--    End                    
--    Else if(UPPER(@Prefix) = 'UK')                    
--    begin                    
--       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                   
--    End                    
--    Else if(UPPER(@Prefix) = 'DE')                    
--    begin                    
--       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                   
--    End                    
                        
--    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                     
--    BEGIN                    
--    SET @query = '              
--          '                   
                        
--    END                    
--    ELSE                    
--    BEGIN                    
--     SET @query = ' '                   
                      
  SET @sqltxt ='INSERT INTO #temp (ShortTIPN ,AverageCost,PriorYearUsage ,YTDUsage ,TotalStockAvailable,ROCStockAvailable,IncomingStock ,              
  NextIncomingQuantity ,NextIncomingDate,SixMonthUsage,ExcessInventory,LAXStockAvailable)                    
      SELECT shortTIPartNo, ISNULL(avgCost,0) as AverageCost,           
      ( select ISNULL(SUM(Stn_BLG_WGT),0) from US_sahstn_rec           
      join US_arrcus_rec on cus_cus_id = stn_sld_cus_id               
 join US_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no          
 left join US_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
 where Ltrim(RTrim(Stn_FRM)) + ''/'' + Ltrim(RTrim(stn_GRD)) + ''/'' + Ltrim(RTrim(stn_size)) + ''/'' + Ltrim(RTrim(stn_fnsh)) = shortTIPartNo              
and Stn_INV_DT  between '''+ @PYFD +''' and '''+ @PYTD +''' and STN_SHPT_BRH not in (''SFS'') and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) ) as PriorYearUsage ,          
( select ISNULL(SUM(Stn_BLG_WGT),0) from US_sahstn_rec           
join US_arrcus_rec on cus_cus_id = stn_sld_cus_id               
 join US_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no        
 left join US_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
 where Ltrim(RTrim(Stn_FRM)) + ''/'' + Ltrim(RTrim(stn_GRD)) + ''/'' + Ltrim(RTrim(stn_size)) + ''/'' + Ltrim(RTrim(stn_fnsh)) = shortTIPartNo              
and Stn_INV_DT  between '''+ @YTFD +''' and '''+ @YTTD +'''  and STN_SHPT_BRH not in (''SFS'') and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null) ) as YTDUsage ,          
ISNULL(AvailableWeight,0) as TotalStockAvailable,ISNULL(ROSQTY,0) as ROCStockAvailable,ISNULL(OpenPOWgt,0) as IncomingStock ,          
ISNULL(OpenPOWgt,0) as NextIncomingQuantity ,(select Min(pod_arr_to_dt) from US_tctipd_rec             
join US_potpod_rec on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''          
join US_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm           
Where Ltrim(RTrim(ipd_FRM)) + ''/'' + Ltrim(RTrim(ipd_GRD)) + ''/'' + Ltrim(RTrim(ipd_size)) + ''/'' + Ltrim(RTrim(ipd_fnsh)) = shortTIPartNo  and poi_bal_wgt > 0             
 ) as NextIncomingDate ,  
Months6InvoicedWeight,ExcessInventory, ISNULL(LAXQTY,0) as LAXStockAvailable             
  FROM tbl_itech_short_ti_part left join tbl_itech_ReplacementPricing on ProductShort = shortTIPartNo and [Database]= ''US''               
        
         '                   
                            
  --END                    
 print(@sqltxt)                    
 set @execSQLtxt = @sqltxt;                     
 EXEC (@execSQLtxt);                    
--END                    
 SELECT distinct * FROM #temp where (AverageCost = @AverageCost OR @AverageCost = 0) and (PriorYearUsage = @PriorYearUsage OR @PriorYearUsage = 0)         
 and (YTDUsage = @YTDUsage OR @YTDUsage = 0) and (TotalStockAvailable = @TotalStockAvailable OR @TotalStockAvailable = 0)         
 and (ROCStockAvailable = @BranchStockAvailable OR @BranchStockAvailable = 0) and (IncomingStock = @IncomingStock OR @IncomingStock = 0)         
 and (NextIncomingQuantity = @NextIncomingQuantity OR @NextIncomingQuantity = 0)         
 and (NextIncomingDate = CONVERT(VARCHAR(10), @NextIncomingDate,120) OR @NextIncomingDate = '2019-01-01')         
 -- @NextIncomingDate        
 ;           
 -- CONVERT(VARCHAR(10), GETDATE(),120) ,NextIncomingDate                 
         
 DROP TABLE  #temp;                    
END                    
              
-- @AverageCost Decimal(20,2), @PriorYearUsage Decimal(20,2), @YTDUsage Decimal(20,2),               
--@TotalStockAvailable Decimal(20,2), @BranchStockAvailable Decimal(20,2), @IncomingStock Decimal(20,2), @NextIncomingQuantity Decimal(20,2),              
--@NextIncomingDate date                    
--EXEC [sp_itech_LTA_Inventory] 0,0.0,0.0,0.0,0.0,0.0,0.0,''                    
    
/*    
DATE: 20190827    
Mail Sub:LTA Inventory Report Update Request    
SET @sqltxt ='INSERT INTO #temp (ShortTIPN ,AverageCost,PriorYearUsage ,YTDUsage ,TotalStockAvailable,BranchStockAvailable,IncomingStock ,                  
  NextIncomingQuantity ,NextIncomingDate,CustID,CustName)                        
      SELECT shortTIPartNo,(select top 1 acp.acp_tot_mat_cst  from US_intacp_rec acp where         
      Ltrim(RTrim(acp.acp_frm)) + ''/'' + Ltrim(RTrim(acp.acp_grd)) + ''/'' + Ltrim(RTrim(acp.acp_size)) + ''/'' + Ltrim(RTrim(acp.acp_fnsh)) = shortTIPartNo        
      and acp.acp_cus_id = ppg_cus_ven_id) as AverageCost,               
      ( select ISNULL(SUM(Stn_BLG_WGT),0) from US_sahstn_rec               
      join US_arrcus_rec on cus_cus_id = stn_sld_cus_id                   
 join US_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no              
 left join US_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
 where Ltrim(RTrim(Stn_FRM)) + ''/'' + Ltrim(RTrim(stn_GRD)) + ''/'' + Ltrim(RTrim(stn_size)) + ''/'' + Ltrim(RTrim(stn_fnsh)) = shortTIPartNo                  
and Stn_INV_DT  between '''+ @PYFD +''' and '''+ @PYTD +''' and STN_SHPT_BRH not in (''SFS'') and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null)         
and cus_cus_id = ppg_cus_ven_id) as PriorYearUsage ,        
( select ISNULL(SUM(Stn_BLG_WGT),0) from US_sahstn_rec               
join US_arrcus_rec on cus_cus_id = stn_sld_cus_id                   
 join US_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no            
 left join US_arrcuc_rec on cuc_cus_cat = cus_cus_cat               
 where Ltrim(RTrim(Stn_FRM)) + ''/'' + Ltrim(RTrim(stn_GRD)) + ''/'' + Ltrim(RTrim(stn_size)) + ''/'' + Ltrim(RTrim(stn_fnsh)) = shortTIPartNo                  
and Stn_INV_DT  between '''+ @YTFD +''' and '''+ @YTTD +'''  and STN_SHPT_BRH not in (''SFS'') and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null)        
and cus_cus_id = ppg_cus_ven_id) as YTDUsage ,         
             
ISNULL(AvailableWeight,0) as TotalStockAvailable,ISNULL(ROSQTY,0) as BranchStockAvailable,ISNULL(OpenPOWgt,0) as IncomingStock ,              
ISNULL(OpenPOWgt,0) as NextIncomingQuantity ,(select Min(pod_arr_to_dt) from US_tctipd_rec                 
join US_potpod_rec on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''              
join US_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm               
Where Ltrim(RTrim(ipd_FRM)) + ''/'' + Ltrim(RTrim(ipd_GRD)) + ''/'' + Ltrim(RTrim(ipd_size)) + ''/'' + Ltrim(RTrim(ipd_fnsh)) = shortTIPartNo  and poi_bal_wgt > 0                 
 ) as NextIncomingDate, cus_cus_id, Ltrim(Rtrim(cus_cus_long_nm))        
  FROM tbl_itech_short_ti_part         
  left join [LIVEUSSTX].[liveusstxdb].[informix].cprppg_rec on  Ltrim(RTrim(ppg_FRM)) + ''/'' + Ltrim(RTrim(ppg_GRD)) + ''/'' + Ltrim(RTrim(ppg_size)) + ''/''         
  + Ltrim(RTrim(ppg_fnsh)) = shortTIPartNo and ppg_cus_ven_typ = ''C''      
  left join US_arrcus_rec on cus_cus_id = ppg_cus_ven_id      
 left join tbl_itech_ReplacementPricing on ProductShort = shortTIPartNo and [Database]= ''US''                   
            
Mail sub:Home > STRATIXReports > Development > Arconic LTA Inventory Report  
Date:20191118  
        
*/
GO
