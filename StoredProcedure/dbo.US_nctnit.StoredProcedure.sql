USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_nctnit]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,mrinal>
-- Create date: <Create Date,Aug 6, 2015,>
-- Description:	<Description,gl account,>

-- =============================================
CREATE PROCEDURE [dbo].[US_nctnit]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_nctnit_rec', 'U') IS NOT NULL
		drop table dbo.US_nctnit_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_nctnit_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[nctnit_rec]; 
  
END
-- select * from US_apjglj_rec
GO
