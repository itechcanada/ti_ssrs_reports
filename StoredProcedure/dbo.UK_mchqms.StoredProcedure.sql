USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_mchqms]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Aug 27, 2019,>    
-- Description: <Description,Open Orders,>    
    
-- =============================================    
create PROCEDURE [dbo].[UK_mchqms]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.UK_mchqms_rec', 'U') IS NOT NULL    
  drop table dbo.UK_mchqms_rec ;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.UK_mchqms_rec    
  from [LIVEUKSTX].[liveukstxdb].[informix].[mchqms_rec] ;     
      
END    
-- select * from UK_mchqms_rec 
GO
