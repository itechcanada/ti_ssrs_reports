USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_test_graph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_test_graph] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT (select avg(s.stn_blg_wgt) from US_sahstn_rec s) as avgWgt,sum(stn_blg_wgt) as stn_blg_Wgt, stn_sld_cus_id, stn_upd_mth from US_sahstn_REc 
	group by  stn_blg_wgt, stn_sld_cus_id, stn_upd_mth	;
END
GO
