USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_potpor]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Sumit       
-- Create date: OCt 30, 2020      
-- Description: <Description,Germany Purchase Order Item Release>      
-- =============================================      
create PROCEDURE [dbo].[DE_potpor]       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
SET NOCOUNT ON;
-- Insert statements for procedure here      
          
          
    IF OBJECT_ID('dbo.DE_potpor_rec', 'U') IS NOT NULL      
  drop table dbo.DE_potpor_rec;      
          
              
SELECT *      
into  dbo.DE_potpor_rec      
FROM [LIVEDESTX].[livedestxdb].[informix].[potpor_rec];      
      
END            
-- select * from DE_potpor_rec  
GO
