USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ortord]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,11/19/2020,>    
-- Description: <Description,Germany ortord,>    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_ortord]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
SET NOCOUNT ON;    

truncate table dbo.DE_ortord_rec;       

-- Insert statements for procedure here    
insert into DE_ortord_rec 
SELECT * FROM [LIVEDESTX].[livedestxdb].[informix].[ortord_rec]    
    
    
END    
GO
