USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ortipr]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: April 4, 2013  
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[US_ortipr]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.US_ortipr_rec', 'U') IS NOT NULL  
  drop table dbo.US_ortipr_rec;  
          
SELECT *  
into  dbo.US_ortipr_rec  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortipr_rec];  
  
END  
-- select * from US_ortipr_rec
GO
