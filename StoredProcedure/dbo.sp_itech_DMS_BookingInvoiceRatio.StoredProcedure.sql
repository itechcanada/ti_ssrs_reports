USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_DMS_BookingInvoiceRatio]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukes>              
-- Create date: <03-04-2019>              
-- Description: <Booking to Invoice Ratio>             
          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_DMS_BookingInvoiceRatio]  @DBNAME varchar(50), @Branch varchar(3)            
AS              
BEGIN              
            
 -- SET NOCOUNT ON added to prevent extra result sets from              
 SET NOCOUNT ON;              
declare @DB varchar(100);              
declare @sqltxt varchar(6000);              
declare @execSQLtxt varchar(7000);              
DECLARE @CountryName VARCHAR(25);                 
DECLARE @prefix VARCHAR(15);                 
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @CurrenyRate varchar(15);            
declare @FD varchar(10)                
declare @TD varchar(10)         
declare @TDInv varchar(10)                     
              
   set @FD = CONVERT(VARCHAR(10), DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0),120)    -- First date of current month      
 set @TDInv = CONVERT(VARCHAR(10), GETDATE(),120)  -- Current date       
 set @TD = CONVERT(VARCHAR(10), DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0)) ,120)      -- Last date of current month    
            
             
IF @Branch = 'ALL'              
 BEGIN              
  set @Branch = ''              
 END    
            
              
CREATE TABLE #temp ( Dbname   VARCHAR(10)              
    -- ,Branch   VARCHAR(3)              
     ,BookedValue   decimal(20,0)              
     , TotalSlsVal decimal(20,0)             
     );              
             
IF @DBNAME = 'ALL'              
 BEGIN              
               
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
                
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  WHILE @@FETCH_STATUS = 0              
  BEGIN              
   DECLARE @query NVARCHAR(MAX);              
   IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End              
                  
    SET @query = 'INSERT INTO #temp (Dbname,BookedValue, TotalSlsVal)              
      SELECT '''+ @Prefix +''' as Country,    
      (select ISNULL(sum(cast(a.mbk_tot_val as decimal(20,0))* '+ @CurrenyRate +'),0) FROM ' + @Prefix + '_ortmbk_rec a          
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id          
      WHERE a.mbk_ord_pfx=''SO''           
        AND a.mbk_ord_itm<>999           
         and a.mbk_trs_md = ''A''            
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''     
 AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')),  
 (select ISNULL(SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +'),0)  from ' + @Prefix + '_sahstn_rec  ou  where stn_inv_Dt >=''' + @FD + '''  and stn_inv_dt <= ''' + @TDInv + '''   
          and stn_frm <> ''XXXX'' and STN_SHPT_BRH not in (''SFS'')  
          and STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
         '             
   print @query;              
   EXECUTE sp_executesql @query;              
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  END                 
  CLOSE ScopeCursor;              
  DEALLOCATE ScopeCursor;              
 END              
ELSE              
BEGIN              
             
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)              
 IF (UPPER(@DBNAME) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DBNAME) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DBNAME) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DBNAME) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DBNAME) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@DBNAME) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End              
    Else if(UPPER(@DBNAME) = 'TWCN')              
    begin              
       SET @DB ='TW'              
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
                  
                
  SET @sqltxt ='INSERT INTO #temp (Dbname,BookedValue, TotalSlsVal)              
      SELECT '''+ @DBNAME +''' as Country,    
      (select ISNULL(sum(cast(a.mbk_tot_val as decimal(20,0))* '+ @CurrenyRate +'),0) FROM ' + @DBNAME + '_ortmbk_rec a          
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id          
      WHERE a.mbk_ord_pfx=''SO''           
        AND a.mbk_ord_itm<>999           
         and a.mbk_trs_md = ''A''            
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''     
 AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')),  
 (select ISNULL(SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +'),0)  from ' + @DBNAME + '_sahstn_rec  ou  where stn_inv_Dt >=''' + @FD + '''  and stn_inv_dt <= ''' + @TDInv + '''   
          and stn_frm <> ''XXXX'' and STN_SHPT_BRH not in (''SFS'')  
          and (STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= ''''))  
          '             
 print(@sqltxt)              
 set @execSQLtxt = @sqltxt;               
 EXEC (@execSQLtxt);              
END              
 SELECT BookedValue ,  TotalSlsVal  
 FROM #temp ;            
 DROP TABLE  #temp;              
END              
              
              
-- @DBNAME varchar(50), @Branch varchar(3)              
-- EXEC [sp_itech_DMS_BookingInvoiceRatio] 'US','ALL'           
/*      
    
*/ 
GO
