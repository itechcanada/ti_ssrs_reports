USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_orrprs]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 6, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[CA_orrprs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_orrprs_rec', 'U') IS NOT NULL
		drop table dbo.CA_orrprs_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CA_orrprs_rec
  from [LIVECASTX].[livecastxdb].[informix].[orrprs_rec] ; 
  
END
-- select * from CA_orrprs_rec
GO
