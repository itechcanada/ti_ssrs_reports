USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_scrcrx]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
create  PROCEDURE [dbo].[UK_scrcrx] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.UK_scrcrx_rec', 'U') IS NOT NULL
		drop table dbo.UK_scrcrx_rec;
    
        
SELECT *
into  dbo.UK_scrcrx_rec
FROM [LIVEUKSTX].[liveukstxdb].[informix].[scrcrx_rec];

END
-- select * from UK_scrcrx_rec
GO
