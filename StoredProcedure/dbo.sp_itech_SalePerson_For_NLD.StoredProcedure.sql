USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalePerson_For_NLD]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <2 DEC 2016>
-- Description:	<Getting Sale Person form each database for NLD Account for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_SalePerson_For_NLD]  @DBNAME varchar(50)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt1 varchar(8000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
Set @DB = @DBNAME

CREATE TABLE #tmp ( 
					[Database]		 VARCHAR(10)
					, salePerson	 VARCHAR(65)
   	            );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query1 NVARCHAR(max);  	
  				SET @DB= @Prefix 
			    SET @query1 ='INSERT INTO #tmp ([Database], salePerson)
			                SELECT  Distinct ''' +  @Prefix + ''' as [Database],
									slp_slp AS SalePerson from  ' + @DB + '_scrslp_rec;'
  				 print @query1;
  	  			EXECUTE sp_executesql @query1; 
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
			     SET @sqltxt1 ='INSERT INTO #tmp ([Database], salePerson)
			                SELECT  Distinct ''' +  @DB + ''' as [Database],
									slp_slp AS SalePerson from  ' + @DB + '_scrslp_rec;'
					print(@sqltxt1)	
			EXEC (@sqltxt1);
   END
   
select salePerson AS Value ,salePerson AS text, 'B' AS temp from #tmp
Union  
Select 'ALL' as Value,'All Sales Person' as text,'A' as temp
 order by temp,text
   drop table #tmp
END

-- Exec [sp_itech_SalePerson_For_NLD] 'ALL'
-- Exec [sp_itech_SalePerson_For_NLD] 'US'
GO
