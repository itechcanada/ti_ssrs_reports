USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ivtivs]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 01, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[US_ivtivs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_ivtivs_rec', 'U') IS NOT NULL
		drop table dbo.US_ivtivs_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_ivtivs_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[ivtivs_rec] ; 
  
END
-- select * from US_ivtivd_rec 
GO
