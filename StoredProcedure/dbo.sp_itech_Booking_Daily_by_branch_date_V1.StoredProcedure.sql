USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Booking_Daily_by_branch_date_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
-- =============================================                    
-- Author:  <MRIANL>                    
-- Create date: <21-03-2016>                    
-- Description: <Booking Daily Reports>                   
-- Last updated by Mukesh                   
-- Last updated Date 16 Jun 2015                  
-- Last updated desc: add IS and OS sales person                    
-- =============================================                    
CREATE PROCEDURE [dbo].[sp_itech_Booking_Daily_by_branch_date_V1]  @DBNAME varchar(50), @version char = '0',@Branch varchar(50),@Month as Datetime                    
AS                    
BEGIN                    
                  
 -- SET NOCOUNT ON added to prevent extra result sets from                    
 -- interfering with SELECT statements.                    
 SET NOCOUNT ON;           
   
  
declare @DB varchar(100);                    
declare @sqltxt varchar(6000);                    
declare @execSQLtxt varchar(7000);                    
DECLARE @CountryName VARCHAR(25);                       
DECLARE @prefix VARCHAR(15);                       
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @CurrenyRate varchar(15);                      
                    
                  
IF @Branch = 'ALL'                    
 BEGIN                    
  set @Branch = ''                    
                    
 END                   
 print '@Branch =' + @Branch;                  
 declare @start varchar = ''                  
-- SET @start = @Month;                   
-- print  @start;                  
 PRINT CONVERT(VARCHAR(10), @Month, 120)                  
                    
CREATE TABLE #temp ( Dbname   VARCHAR(10)                    
     ,DBCountryName VARCHAR(25)                   
     ,ISlp  VARCHAR(4)                  
     ,OSlp  VARCHAR(4)                     
     ,CusID   VARCHAR(10)                    
     ,CusLongNm  VARCHAR(40)                    
     ,Branch   VARCHAR(3)                    
     ,ActvyDT  VARCHAR(10)                    
     ,OrderNo  NUMERIC                    
     ,OrderItm  NUMERIC                    
     ,Product  VARCHAR(500)                    
     ,Wgt   decimal(20,0)                    
     ,TotalMtlVal decimal(20,0)                    
     ,ReplCost  decimal(20,0)                    
     ,Profit   decimal(20,1)                    
     , TotalSlsVal decimal(20,0)                   
     ,Market Varchar(35)              
     ,DueDate varchar(10)           
     ,BookingProcessMethod varchar(1)            
     );                    
                    
IF @DBNAME = 'ALL'                    
 BEGIN                    
   IF @version = '0'                    
  BEGIN                    
  DECLARE ScopeCursor CURSOR FOR                    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                    
    OPEN ScopeCursor;                    
  END                    
  ELSE                    
  BEGIN                    
  DECLARE ScopeCursor CURSOR FOR                    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                     
    OPEN ScopeCursor;                    
  END                    
                      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                    
  WHILE @@FETCH_STATUS = 0                    
  BEGIN                    
   DECLARE @query NVARCHAR(MAX);                    
   IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                    
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')            
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End                    
    Else if(UPPER(@Prefix) = 'DE')            
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End                    
                        
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR                     
    BEGIN                    
    SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost,            
  Profit, TotalSlsVal,Market,DueDate,BookingProcessMethod)                    
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,                    
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +'  ,'' ''                   
       ,orl_due_to_dt ,a.mbk_trs_md           
      FROM ' + @Prefix + '_ortmbk_rec a                    
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id            
      join ' + @Prefix + '_ortorl_rec on orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no and orl_ord_itm = mbk_ord_itm             
      -- and orl_ord_rls_no = mbk_ln_no                        
      WHERE a.mbk_ord_pfx=''SO''                     
          AND a.mbk_ord_itm<>999                      
         and a.mbk_trs_md = ''A''                    
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, '''+CONVERT(VARCHAR(10), @Month, 120)+''')                   
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                   
                            
    END                    
    ELSE                    
    BEGIN                    
     SET @query = 'INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal,             
     ReplCost, Profit, TotalSlsVal,Market,DueDate,BookingProcessMethod)                    
      SELECT '''+ @Prefix +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,                    
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,'' ''                    
       ,orl_due_to_dt ,a.mbk_trs_md           
      FROM ' + @Prefix + '_ortmbk_rec a                    
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id             
       join ' + @Prefix + '_ortorl_rec on orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no and orl_ord_itm = mbk_ord_itm             
      -- and orl_ord_rls_no = mbk_ln_no                    
      WHERE a.mbk_ord_pfx=''SO''                     
         AND a.mbk_ord_itm<>999                      
        and a.mbk_trs_md = ''A''                     
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, '''+CONVERT(VARCHAR(10), @Month, 120)+''')                   
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                   
                           
    END                    
   print @query;                    
   EXECUTE sp_executesql @query;                    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                    
  END                       
  CLOSE ScopeCursor;                    
  DEALLOCATE ScopeCursor;                    
 END                    
ELSE                    
BEGIN                    
IF @version = '0'                    
  BEGIN                    
  SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)                    
  END                    
  ELSE                    
  BEGIN                 
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)                    
  END                    
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)                    
 IF (UPPER(@DBNAME) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
   Else if (UPPER(@DBNAME) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DBNAME) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End                    
    Else if(UPPER(@DBNAME) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End                    
    Else if(UPPER(@DBNAME) = 'TWCN')                    
    begin                    
       SET @DB ='TW'                    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
                        
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR                     
   BEGIN                    
   SET @sqltxt ='INSERT INTO #temp (Dbname,DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost,            
    Profit, TotalSlsVal, Market,DueDate,BookingProcessMethod)                    
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,                    
      a.mbk_wgt*(2.20462), a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' , '' ''                   
       ,orl_due_to_dt,a.mbk_trs_md            
      FROM ' + @DBNAME + '_ortmbk_rec a                    
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and a.mbk_sld_cus_id=b.cus_cus_id             
       join ' + @DBNAME + '_ortorl_rec on orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no and orl_ord_itm = mbk_ord_itm             
      -- and orl_ord_rls_no = mbk_ln_no                    
      WHERE a.mbk_ord_pfx=''SO''                     
         AND a.mbk_ord_itm<>999                       
        and a.mbk_trs_md = ''A''                    
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, '''+CONVERT(VARCHAR(10), @Month, 120)+''')                   
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                   
                           
  END                    
  ELSE                    
  BEGIN                    
  SET @sqltxt ='INSERT INTO #temp (Dbname, DBCountryName, ISlp, OSlp, CusID, CusLongNm, Branch, ActvyDT, OrderNo, OrderItm, Product, Wgt, TotalMtlVal, ReplCost,            
   Profit, TotalSlsVal, Market,DueDate,BookingProcessMethod)                    
      SELECT '''+ @DBNAME +''' as Country, ''' + @CountryName + ''', mbk_is_slp, mbk_os_slp, b.cus_cus_id,b.cus_cus_long_nm,a.mbk_brh,a.mbk_actvy_dt, a.mbk_ord_no, a.mbk_ord_itm, a.mbk_frm+a.mbk_grd+a.mbk_size+a.mbk_fnsh as Product,                    
      a.mbk_wgt, a.mbk_tot_mtl_val* '+ @CurrenyRate +', ''ReplCost''= ('+ @CurrenyRate +')*(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END),                    
      ''Profit''=CASE WHEN a.mbk_tot_mtl_val=0 THEN 0 ELSE(a.mbk_tot_mtl_val-(CASE WHEN a.mbk_mtl_repl_val=0 THEN a.mbk_mtl_avg_val ELSE a.mbk_mtl_repl_val END))/a.mbk_tot_mtl_val*100 END, a.mbk_tot_val* '+ @CurrenyRate +' ,'' ''             
      ,orl_due_to_dt,a.mbk_trs_md            
      FROM ' + @DBNAME + '_ortmbk_rec a                    
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_cmpy_id = b.cus_cmpy_id and  a.mbk_sld_cus_id=b.cus_cus_id            
      join ' + @DBNAME + '_ortorl_rec on orl_cmpy_id = mbk_cmpy_id and orl_ord_pfx = mbk_ord_pfx and orl_ord_no = mbk_ord_no and orl_ord_itm = mbk_ord_itm             
      -- and orl_ord_rls_no = mbk_ln_no                        
      WHERE a.mbk_ord_pfx=''SO''                     
        AND a.mbk_ord_itm<>999                     
         and a.mbk_trs_md = ''A''                      
         AND CAST(a.mbk_actvy_dt AS datetime)= CONVERT (date, '''+CONVERT(VARCHAR(10), @Month, 120)+''')                   
         AND (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''') '                   
                            
  END                    
 print(@sqltxt)                    
 set @execSQLtxt = @sqltxt;                     
 EXEC (@execSQLtxt);                    
END                    
 SELECT * FROM #temp ;                    
 DROP TABLE  #temp;                    
END                    
                    
--EXEC [sp_itech_Booking_Daily_by_branch_date_V1] 'ALL'                    
--EXEC [sp_itech_Booking_Daily_by_branch_date_V1] 'US', '0'  , 'ALL' ,'2019-12-06'                  
--EXEC [sp_itech_Booking_Daily_by_branch_date_V1] 'US', '0'  , 'CRP' ,'2016-03-21'                  
--select * from tbl_itech_DatabaseName                  
--exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch 'US','LAX','2016-03-18','ALL',0             
/*            
Date:20191206            
Mail sub:Booking Daily Report By Branch Update Request            
          
Date:20191211          
Mail sub:Booking Daily Report - Additional Request          
            
*/
GO
