USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_AM_Report_V4]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Mukesh >                            
-- Create date: <29 Aug 2019>                            
-- Description: <AM Report>                     
--                             
-- =============================================                            
CREATE PROCEDURE [dbo].[sp_itech_AM_Report_V4] @DBNAME Varchar(50), @Branch Varchar(50) , @RunMonth varchar(1), @version char = '0'                           
                            
AS                            
BEGIN                            
SET NOCOUNT ON;                            
                            
declare @sqltxt varchar(6000)                            
declare @execSQLtxt varchar(7000)                            
declare @DB varchar(100)                            
--declare @LIVEDB varchar(100)                            
declare @FDYTD varchar(10)                            
declare @FDMTD varchar(10)                           
declare @FDMTDPM varchar(10)  -- from date of previous month                          
declare @TDYTD varchar(10)                  
declare @PTDYTD varchar(10)  -- Previous date from current date                          
declare @TDYTDPM varchar(10)   --To Days of previous month                          
declare @DaysYTD varchar(4)                          
declare @DaysYTDPM varchar(4)  --Days up to previous month                          
declare @DaysMTD varchar(2)                          
declare @DaysMTDPM varchar(2)  --Total Days of previous month                          
declare @LastDateOfCurrentMonth varchar(10)                          
                          
declare @ForecastMonth varchar (10)                          
declare @ForecastMonthPM varchar (10)                          
                            
set @DaysYTD = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',CONVERT(varchar(10),getdate(),120))                            
set @FDYTD = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(getdate())) + '-01-01', 120)                               
Set @FDMTD = CONVERT(varchar(7), getdate(),126) + '-01'                            
set @TDYTD = CONVERT(VARCHAR(10), getdate(), 120)                 
--set @TDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -2),120);  For testing 20200406               
set @PTDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -1),120);                              
--set @PTDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -3),120); For testing 20200406      
set @DaysMTD = DATEDIFF(day,@FDMTD,GETDATE())                          
set @ForecastMonth = convert(varchar(7),GetDate(), 126)                          
Set @LastDateOfCurrentMonth = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)), 120)                          
                          
if @RunMonth = 1                          
Begin                          
set @FDMTD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 1, 0) , 120)   --First day of previous 13 month                          
set @TDYTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month                          
set @DaysYTD = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',@TDYTD)                           
set @ForecastMonth =  convert(varchar(7),@FDMTD, 126)                          
set @DaysMTD = dbo.DATEDIFF_WorkingDaysOnly(@FDMTD, @TDYTD)                          
                          
End                          
ELSE                          
BEgin                          
set @DaysMTD = dbo.DATEDIFF_WorkingDaysOnly(@FDMTD, GETDATE())                          
End                          
                          
-- For previous month                          
SET @TDYTDPM = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@TDYTD),0)), 120)                            
SET @DaysYTDPM = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',@TDYTDPM)                           
SET @FDMTDPM = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @FDMTD) - 1, 0) , 120)                          
SET @DaysMTDPM = dbo.DATEDIFF_WorkingDaysOnly(@FDMTDPM, @TDYTDPM)                          
SET @ForecastMonthPM =  convert(varchar(7),@FDMTDPM, 126)                          
                          
print '1' + @FDMTD;                          
print '2' + @TDYTD;                          
print @DaysMTD;                          
                          
print @FDMTDPM;                          
print @TDYTDPM;                          
PRint @DaysYTDPM;                          
--set @DaysMTD = dbo.DATEDIFF_WorkingDaysOnly(@FDMTD, GETDATE())                          
                
-- comment below if clause during taking backup of AM report                
if @RunMonth = 1                          
BEGIN                          
-- Compare 1st day of  PRevious Month  with UpdDtTm and Databse = Database and Brh and Brh=                           
if @DBNAME = 'ALL'                          
BEGIN                          
-- select CONVERT(varchar(7),  UpdDtTm,126)  from tbl_itech_AM_HISTORY                 
 --select 0 as DailyShpt, 0 as DailyBooking, * from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + '-01' = @FDMTD and Databases != 'US' and Branch != 'DEU'    ;    
 select 0 as DailyShpt, 0 as DailyBooking, * from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + '-01' = @FDMTD and Databases not in ('US','DE') and Branch != 'DEU' ;  
print '****';                
print @FDMTD;                
                
END                          
ELSE                           
BEGIN                  
                
IF(@DBNAME = 'UK' AND @Branch = 'DEU' )                
Begin                
   select  0 as DailyShpt,0 as DailyBooking, * from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + '-01' = @FDMTD and Databases = 'UK' and BRANCH = 'DEU'                             
END                
                
else                
Begin                        
IF(@Branch = 'IND')                            
           Begin                            
             select  0 as DailyShpt,0 as DailyBooking, * from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + '-01' = @FDMTD and Databases = 'US' and BRANCH = 'IND'                            
                            
            end                            
          Else                            
            Begin                            
     select   0 as DailyShpt,0 as DailyBooking, * from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + '-01' = @FDMTD and Databases = 'US' and BRANCH not in('IND','CRP') order by seqno   -- ,'SHA'                        
                    
            End                            
END                   
END                       
return                          
END                          
                          
 CREATE TABLE #tempInvt (  InvtVal  DECIMAL(20)                            
     ,TotVal   DECIMAL(20)                            
     , Branch     varchar(20)                          
     ) ;                          
                            
CREATE TABLE #tmp (  ShipmentsAvgDaily  DECIMAL(20)                          
  ,PrvMonthShipmentsAvgDaily  DECIMAL(20)                          
  ,PrvMonthShipmentsMTD  DECIMAL(20)                             
     ,ShipmentsMTD   DECIMAL(20)                            
     ,Budget     DECIMAL(20)                            
     ,Forecast    DECIMAL(20)                            
     ,GPBudget    DECIMAL(20)                            
     ,GPForecast    DECIMAL(20)                         
     ,GPPctForecast   DECIMAL(20,2)                         
     ,LBSShippedPlan   DECIMAL(20)                            
     ,PrvMonthLBSShippedPlan   DECIMAL(20)                            
     ,WarehseFees   DECIMAL(20)                            
     ,PrvMonthWarehseFees   DECIMAL(20)                            
     ,MTDOrdersCount   int                           
     ,OpenOrdersMTD   DECIMAL(20)                            
     ,TotalOpenOrders   DECIMAL(20)                            
     ,PrvMonthTotalOpenOrders   DECIMAL(20)                            
     ,PrvMonthMTDOrdersCount   int                           
     ,PrvMonthOpenOrdersMTD   DECIMAL(20)                            
       ,sDate      VARCHAR(15)                            
        ,LBShippedMTD   DECIMAL(20)                            
        ,PrvMonthLBShippedMTD   DECIMAL(20)                            
        ,BookingAvgDaily  DECIMAL(20)                            
        ,BookingMTD    DECIMAL(20)                            
        ,PrvMonthBookingAvgDaily  DECIMAL(20)                            
        ,PrvMonthBookingMTD    DECIMAL(20)                            
        ,Inventory    DECIMAL(20)                            
         ,PrvMonthInventory    DECIMAL(20)                           
        ,GP$MTD     DECIMAL(20)                            
        ,PrvMonthGP$MTD     DECIMAL(20)                            
        ,Databases    varchar(50)                            
        ,Branch     varchar(20)          
        ,SeqNo  int                
        ,DailyShpt  Decimal(20)                 
        ,DailyBooking  Decimal(20)            
        ,BackLogOrders   DECIMAL(20)           
        ,PrvMonthBackLogOrders   DECIMAL(20)                   
        );                            
                                    
DECLARE @company VARCHAR(15);                             
DECLARE @prefix VARCHAR(15);                             
DECLARE @DatabaseName VARCHAR(35);                              
DECLARE @CurrenyRate varchar(15);                            
                                  
 IF @DBNAME = 'ALL'                            
 BEGIN                            
  DECLARE ScopeCursor CURSOR FOR                            
   select Name, company,prefix from tbl_itech_DatabaseName where (Prefix != 'US' and Prefix != 'PS' and Prefix != 'DE')                    
    OPEN ScopeCursor;                            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                            
     WHILE @@FETCH_STATUS = 0                            
       BEGIN                            
        DECLARE @query NVARCHAR(1500);                               
       set @DB= @prefix                             
      --print(@DB)                            
      --set @LIVEDB = '[LIVE' + UPPER(@DB) + 'GL].[live' + LOWER(@DB) + 'gldb].[informix].[glhgld_rec]'                            
    IF (UPPER(@Prefix) = 'TW')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
    End                            
    Else if (UPPER(@Prefix) = 'NO')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CA')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
    End                            
    Else if (UPPER(@Prefix) = 'CN')                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
    End                            
    Else if (UPPER(@Prefix) = 'US')                            
    begin                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
    End                            
    Else if(UPPER(@Prefix) = 'UK')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    End
	Else if(UPPER(@Prefix) = 'DE')                            
    begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
    End

             -- Daily shipment added by mukesh 20151228 DailyShpt                
             SET @query = 'INSERT INTO #tmp(sDate,DailyShpt, Branch,Databases)                            
    select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             (SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +')) as DailyShpt,                                       
           '''+ @DatabaseName +''', '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec   join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @PTDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''                 
           and stn_shpt_brh not in (''IND'',''DEU'')                            
           group by convert(varchar(7),stn_inv_Dt, 126) +''-01'' order by sDate desc'                            
      print @query;                                     
             EXECUTE sp_executesql @query;                    
                                
          SET @query = 'INSERT INTO #tmp(sDate,ShipmentsAvgDaily, Branch,Databases)                            
    select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             (SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +')/' + @DaysYTD + ') as ShipmentsAvgDaily,                                
           '''+ @DatabaseName +''', '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec   join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @FDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''                             
           and stn_shpt_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),stn_inv_Dt, 126) +''-01'' order by sDate desc'                            
      print @query;                                     
             EXECUTE sp_executesql @query;                          
             --Separate ShipmentMTD Value and apply the condition remove Interco Transaction                          
             SET @query = 'INSERT INTO #tmp(sDate,ShipmentsMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as ShipmentsMTD,  '''+ @DatabaseName +''', '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''                            
           and stn_shpt_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),stn_inv_Dt, 126) +''-01'' order by sDate desc'                            
      print @query;                                     
             EXECUTE sp_executesql @query;                            
                                       
          SET @query = 'INSERT INTO #tmp(sDate,GP$MTD,LBShippedMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
           SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as GP$MTD,'                            
           if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')               
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as LBShippedMTD,'                            
           ELSE                            
            SET @query = @query + ' SUM(stn_blg_wgt) as LBShippedMTD,'                            
                                       
           SET @query = @query + ''''+ @DatabaseName +''', '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec                       
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id                       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                            
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +'''                       
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                            
           and stn_shpt_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),stn_inv_Dt, 126) +''-01'' order by sDate desc'                            
      print @query;                                     
             EXECUTE sp_executesql @query;                          
                                       
     --For previous month                          
     SET @query = 'INSERT INTO #tmp(PrvMonthShipmentsAvgDaily,PrvMonthShipmentsMTD,PrvMonthGP$MTD,PrvMonthLBShippedMTD,PrvMonthBookingAvgDaily,        
     PrvMonthBookingMTD,PrvMonthMTDOrdersCount,PrvMonthOpenOrdersMTD,PrvMonthWarehseFees,PrvMonthLBSShippedPlan, PrvMonthTotalOpenOrders, PrvMonthBackLogOrders,        
     PrvMonthInventory, Branch,Databases)                            
                             select ShipmentsAvgDaily,ShipmentsMTD,GP$MTD,LBShippedMTD,BookingAvgDaily,BookingMTD,MTDOrdersCount,OpenOrdersMTD,WarehseFees,        
                             LBSShippedPlan,TotalOpenOrders,BackLogOrders,Inventory, '''+ @DatabaseName +''', '''+ @DB +'''                            
                             from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + ''-01'' = ''' + @FDMTDPM + '''and Databases = ''' + @DB + ''' '                            
      print @query;                                     
             EXECUTE sp_executesql @query;                            
                                    
               --DailyBooking                 
                SET @query =    'INSERT INTO #tmp(sDate,DailyBooking,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                            
           (SUM(a.bka_tot_val * '+ @CurrenyRate +')) as BookingDaily,                            
           '''+ @DatabaseName +''' , '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                       
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                      
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @PTDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
           and a.bka_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'''                            
          print @query;                            
          EXECUTE sp_executesql @query;                  
                                     
          SET @query =    'INSERT INTO #tmp(sDate,BookingAvgDaily,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM(a.bka_tot_val * '+ @CurrenyRate +')/' + @DaysYTD + ') as BookingAvgDaily,                            
           '''+ @DatabaseName +''' , '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                       
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                      
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
       and a.bka_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'''                            
          print @query;                            
          EXECUTE sp_executesql @query;                            
          SET @query =    'INSERT INTO #tmp(sDate,BookingMTD,Branch,Databases)                       
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           SUM(a.bka_tot_val * '+ @CurrenyRate +') as BookingMTD,                                   '''+ @DatabaseName +''' , '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                       
            join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                              
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                         
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                         
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
           and a.bka_brh not in (''IND'',''DEU'')                
           group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'''            
          print @query;                            
          EXECUTE sp_executesql @query;                          
                                      
          SET @query = 'INSERT INTO #tmp ( MTDOrdersCount,Branch,Databases)                          
            select count(*), '''+ @DatabaseName +''' ,'''+ @DB +''' from (                           
       select ord_ord_no                          
       from ' + @DB + '_ortord_rec                       
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                  
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                          
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT''                
       and ord_ord_brh not in (''IND'',''DEU'') '                          
       if(@RunMonth = 0)                          
       BEGIN                          
       set @query = @query + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)  '                          
       END                          
       ELSE                          
     BEGIN        
       set @query = @query + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)  '                          
       END                             
       -- and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
       set @query = @query + 'group by ord_ord_no) as t'                            
    print @query;                            
        EXECUTE sp_executesql @query;                          
                                      
        SET @query = 'INSERT INTO #tmp ( sDate,OpenOrdersMTD,Branch,Databases)                            
       select orl_due_to_dt as sDate,(chl_chrg_val * '+ @CurrenyRate +') as ChargeValue,'''+ @DatabaseName +''' ,'''+ @DB +'''                             
       from ' + @DB + '_ortord_rec                      
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                      
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                      
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT''                
       and ord_ord_brh not in (''IND'',''DEU'') '                          
         if(@RunMonth = 0)                          
       BEGIN                           
       set @query = @query + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)  '                          
       END                          
       ELSE                          
       BEGIN                          
       set @query = @query + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)  '                          
       END                          
       set @query = @query + ' order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                            
    print @query;                            
        EXECUTE sp_executesql @query;                            
                                   
        SET @query = 'INSERT INTO #tmp ( TotalOpenOrders,Branch,Databases)                            
       select (chl_chrg_val * '+ @CurrenyRate +') as ChargeValue,'''+ @DatabaseName +''' ,'''+ @DB +'''                             
       from ' + @DB + '_ortord_rec                      
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                      
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                      
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT''                             
       and ord_ord_brh not in (''IND'',''DEU'')                
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                            
    print @query;                            
        EXECUTE sp_executesql @query;                   
-- BackLog 20200327        
SET @query = 'INSERT INTO #tmp ( BackLogOrders,Branch,Databases)                            
       select (chl_chrg_val * '+ @CurrenyRate +') as ChargeValue,'''+ @DatabaseName +''' ,'''+ @DB +'''                             
       from ' + @DB + '_ortorh_rec join ' + @DB + '_ortord_rec  on ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id         
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                      
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                      
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
  and ord_ord_pfx <>''QT'' and orh_ord_typ <> ''J''                            
       and ord_ord_brh not in (''IND'',''DEU'')                
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                            
    print @query;                            
        EXECUTE sp_executesql @query;          
                                
                        
                  SET @query = 'INSERT INTO #tmp ( sDate,Inventory,Branch,Databases)                            
       select CURRENT_TIMESTAMP as sDate, (sum(acb_bgn_dr_amt + acb_dr_amt_1 + acb_dr_amt_2 + acb_dr_amt_3 + acb_dr_amt_4 + acb_dr_amt_5 + acb_dr_amt_6 +                
        acb_dr_amt_7 + acb_dr_amt_8 + acb_dr_amt_9 + acb_dr_amt_10 + acb_dr_amt_11 + acb_dr_amt_12 + acb_dr_amt_13)                
  - sum(acb_cr_amt_1 + acb_cr_amt_2 + acb_cr_amt_3 + acb_cr_amt_4 + acb_cr_amt_5 + acb_cr_amt_6 + acb_cr_amt_7 + acb_cr_amt_8 + acb_cr_amt_9 +                 
  acb_cr_amt_10 + acb_cr_amt_11 + acb_cr_amt_12 + acb_cr_amt_13 +  acb_bgn_cr_amt))* '+ @CurrenyRate +'  as Inventory, '''+ @DatabaseName +''' as Branch  ,'''+ @DB +'''                             
      from ' + @DB + '_glbacb_rec  where acb_fis_yr = Year(GETDATE()) and acb_bsc_gl_acct In (1400,1401,1410,1420,1425,1430) ;'                           
        print @query;                            
        EXECUTE sp_executesql @query;                 
                                    
        SET @query = 'INSERT INTO #tmp (WarehseFees,Branch,Databases)                            
       select SUM(gld_cr_amt * '+ @CurrenyRate +') - SUM(gld_dr_amt * '+ @CurrenyRate +') as WarehseFees,'''+ @DatabaseName +''' ,'''+ @DB +''' from ' + @DB + '_glhgld_rec                            
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','')                             
    print @query;                            
        EXECUTE sp_executesql @query;                           
                                    
        SET @query = 'INSERT INTO #tmp ( Budget,Forecast,GPBudget,GPForecast,GPPctForecast,LBSShippedPlan,Branch,Databases)                            
        SELECT top 1 Budget,Forecast,GPBudget,GPForecast,GPForecastPct,LBSShippedPlan,'''+ @DatabaseName +''' ,'''+ @DB +'''   FROM tbl_itech_Forecast_AM where YearMonth =  ''' +  @ForecastMonth + '''                           
    and branch = '''+ @DatabaseName +''''                            
    print @query;                            
        EXECUTE sp_executesql @query;                    
                       
                       
                                  
        SET @query = 'INSERT INTO #tmp ( SeqNo,Branch,Databases)'                          
        if(@DB = 'TW')                          
        BEGIN                          
    SET @query = @query + ' Select 1,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END                          
          else IF (@DB = 'CN')                          
          BEGIN                          
          SET @query = @query + ' Select 2,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END                          
          else IF (@DB = 'CA')                          
          BEGIN                          
         SET @query = @query + ' Select 3,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END                          
          else IF (@DB = 'UK')                          
          BEGIN                          
          SET @query = @query + ' Select 4,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END                          
          else If(@DB = 'NO')                          
          BEGIN                          
          SET @query = @query + ' Select 5,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END
		  else If(@DB = 'DE')                          
          BEGIN                          
          SET @query = @query + ' Select 6,'''+ @DatabaseName +''' ,'''+ @DB +''''                          
          END 

   print @query;                            
        EXECUTE sp_executesql @query;                            
                                      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                            
       END                 
    CLOSE ScopeCursor;                            
    DEALLOCATE ScopeCursor;                            
  END                            
  else                            
  begin                   
                  
  --IF (UPPER(@DB) = 'TW')                            
  --  begin                            
  --     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
  --  End                            
  --  Else if (UPPER(@DB) = 'NO')                            
  --  begin                            
  --   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
  --  End                            
  --  Else if (UPPER(@DB) = 'CA')                            
  --  begin                            
  --   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
  --  End                            
  --  Else if (UPPER(@DB) = 'CN')                            
  --  begin                            
  --   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
  --  End                            
  --  Else if (UPPER(@DB) = 'US')                            
  --  begin                            
  --   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
  --  End                            
  --  Else if(UPPER(@DB) = 'UK')                            
  --  begin                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
    --End                  
set @DB = @DBNAME                
-- UK DEU Branch                
if(@DB = 'UK' AND @Branch = 'DEU')                
Begin                
Print 'UK DEU';                
--     For UK DEU branch                
     SET @sqltxt = 'INSERT INTO #tmp(sDate,DailyShpt, Branch,Databases)                            
    select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +' as DailyShpt,                                       
                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec    join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @PTDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''                     
            and stn_shpt_brh = ''DEU''                            
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
-- For UK DEU                
SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsAvgDaily, Branch,Databases)                            
 select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             (SUM(ISNULL(stn_tot_val,0)* '+ @CurrenyRate +' )/' + @DaysYTD + ') as ShipmentsAvgDaily,                                       
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec    join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @FDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''  and stn_shpt_brh = ''DEU''                
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                     
            
 --Previous Month data UK DEU                          
     SET @sqltxt = 'INSERT INTO #tmp(PrvMonthShipmentsAvgDaily,PrvMonthShipmentsMTD,PrvMonthGP$MTD,PrvMonthLBShippedMTD,PrvMonthBookingAvgDaily,        
     PrvMonthBookingMTD,PrvMonthMTDOrdersCount,PrvMonthOpenOrdersMTD,PrvMonthWarehseFees,PrvMonthLBSShippedPlan, PrvMonthTotalOpenOrders,PrvMonthBackLogOrders,        
     PrvMonthInventory,Branch,Databases)                            
 select ShipmentsAvgDaily,ShipmentsMTD,GP$MTD,LBShippedMTD,BookingAvgDaily,BookingMTD,MTDOrdersCount,OpenOrdersMTD,WarehseFees,LBSShippedPlan,TotalOpenOrders,        
 BackLogOrders,Inventory,Branch, '''+ @DB +'''                            
          from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + ''-01'' = ''' + @FDMTDPM + '''and Databases = ''' + @DB + ''' and Branch = ''DEU'''                           
      print @sqltxt;                            
             set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
                                 
-- For UK DEU                
     SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0)* '+ @CurrenyRate +' ) as ShipmentsMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec  join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''  and stn_shpt_brh = ''DEU''              
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                                           
-- For UK DEU                
 SET @sqltxt = 'INSERT INTO #tmp(sDate,GP$MTD,LBShippedMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
           SUM(ISNULL(stn_npft_avg_val,0))* '+ @CurrenyRate +' as GP$MTD,                            
           SUM(stn_blg_wgt* 2.20462) as LBShippedMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec                      
           join UK_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id                       
           left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                               
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                
            and stn_shpt_brh = ''DEU''                
             group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
-- For UK DEU                
SET @sqltxt =    'INSERT INTO #tmp(sDate,DailyBooking,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0))* '+ @CurrenyRate +') as DailyBooking,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                         
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                            
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
            and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @PTDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
            and a.bka_brh = ''DEU''                
             group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                      
-- For UK DEU                
SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingAvgDaily,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0)* '+ @CurrenyRate +')/' + @DaysYTD + ') as BookingAvgDaily,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                         
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                         
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
            and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
            and a.bka_brh = ''DEU'' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
-- For UK DEU                
          SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingMTD,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           SUM( ISNULL(a.bka_tot_val,0)* '+ @CurrenyRate +' ) as BookingMTD,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                        
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                              
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                
            and a.bka_brh = ''DEU''                           
             group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                     
-- For UK DEU                
          SET @sqltxt = 'INSERT INTO #tmp ( MTDOrdersCount,Branch,Databases)                          
          select COUNT(*),t.Branch,'''+ @DB +'''   from (                                   
       select ord_ord_no, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch                          
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                            
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                      
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       IF(@RunMonth = 0)                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSE                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                             
                                
          SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''DEU'' group by ord_ord_brh,ord_ord_no                          
       ) as t group by t.Branch'                            
     print(@sqltxt);                             
set @execSQLtxt = @sqltxt;                           
     EXEC (@execSQLtxt);                            
-- For UK DEU                
 SET @sqltxt = 'INSERT INTO #tmp ( OpenOrdersMTD,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                         
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       if(@RunMonth = 0)                          
       BEGIN                    
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSe                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                             
                               
          SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''DEU'' group by ord_ord_brh'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
     --for UK DEU                
     SET @sqltxt = 'INSERT INTO #tmp ( TotalOpenOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
   UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
                       
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                          
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'' and ord_ord_brh = ''DEU''                         
           group by ord_ord_brh'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
             
     --for UK DEU                
     SET @sqltxt = 'INSERT INTO #tmp ( BackLogOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortorh_rec join UK_ortord_rec  on ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id          
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
   UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                          
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'' and ord_ord_brh = ''DEU''  and orh_ord_typ <> ''J''                        
           group by ord_ord_brh'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
             
-- For UK DEU                
   SET @sqltxt = ' INSERT INTO #tempInvt ( InvtVal,TotVal,Branch)  select SUM(prd_ohd_mat_val) as InvtVal,                           
        sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,1)) / acp_tot_qty) ) as TotVal , (case prd_brh when ''PSM'' then Replace(prd_brh, ''PSM'', ''PIERCE'') else Replace(prd_brh, ''ROC'', ''ROS'') end) as Branch                            
  from UK_intprd_rec left join UK_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool                          
       where prd_invt_sts = ''S''  and (acp_tot_qty <> 0 or acp_tot_qty is null)  and     prd_brh = ''DEU'' group by prd_brh'                            
    print @sqltxt;                            
        set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                   
                     
      SET @sqltxt = 'INSERT INTO #tmp ( sDate,Inventory,Branch,Databases)                            
       select CURRENT_TIMESTAMP as sDate,InvtVal+ TotVal as Inventory, Branch,'''+ @DB +'''                             
       from  #tempInvt ;'                           
                               
    print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                               
     delete from #tempInvt;                                          
                     
     -- Warehouse fee UK DEU                
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 9,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''DEU'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
                
-- budget forecase UK DEU                     
     SET @sqltxt = 'INSERT INTO #tmp (Budget,Forecast,GPBudget,GPForecast,GPPctForecast,LBSShippedPlan,Branch,Databases)                            
       SELECT Budget, Forecast, GPBudget, GPForecast,GPForecastPct,LBSShippedPlan, BRANCH,'''+ @DB +'''                             
       FROM tbl_itech_Forecast_AM  Where Databases = ''UK'' and YearMonth =  ''' +  @ForecastMonth + ''' and BRANCH = ''DEU'''                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                   
                     
End                 
                 
else                 
begin                
                 
                             
  set @DB = 'US'                            
    --set @LIVEDB = '[LIVE' + UPPER(@DB) + 'GL].[live' + LOWER(@DB) + 'gldb].[informix].[glhgld_rec]'                            
          SET @sqltxt = 'INSERT INTO #tmp(sDate,DailyShpt, Branch,Databases)                            
    select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0)) as DailyShpt,                                       
                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec    join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
           where stn_inv_Dt >= '''+ @PTDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco'' '                     
           IF(@Branch = 'IND')                            
           Begin                          
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh = ''IND'''                            
           end                            
           ELSE                            
           Begin                  
           SET @sqltxt = @sqltxt + ' and stn_shpt_brh not in (''IND'')'                -- ,''SHA'' 20191025            
           END                            
          SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
                
                                
       SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsAvgDaily, Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             (SUM(ISNULL(stn_tot_val,0) )/' + @DaysYTD + ') as ShipmentsAvgDaily,                                       
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec    join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @FDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco'' '                     
           IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh = ''IND'''                            
           end                            
           ELSE                            
           Begin                            
           SET @sqltxt = @sqltxt + ' and stn_shpt_brh not in (''IND'')'     -- ,''SHA'' 20191025                       
           END                            
          SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                
     --Previous Month data                          
     SET @sqltxt = 'INSERT INTO #tmp(PrvMonthShipmentsAvgDaily,PrvMonthShipmentsMTD,PrvMonthGP$MTD,PrvMonthLBShippedMTD,PrvMonthBookingAvgDaily,        
     PrvMonthBookingMTD,PrvMonthMTDOrdersCount,PrvMonthOpenOrdersMTD,PrvMonthWarehseFees,PrvMonthLBSShippedPlan, PrvMonthTotalOpenOrders,PrvMonthBackLogOrders,        
     PrvMonthInventory,Branch,Databases)                            
  select ShipmentsAvgDaily,ShipmentsMTD,GP$MTD,LBShippedMTD,BookingAvgDaily,BookingMTD,MTDOrdersCount,OpenOrdersMTD,WarehseFees,LBSShippedPlan,        
  TotalOpenOrders,BackLogOrders,Inventory,Branch, '''+ @DB +'''                            
          from tbl_itech_AM_HISTORY where CONVERT(varchar(7),  UpdDtTm,126) + ''-01'' = ''' + @FDMTDPM + '''and Databases = ''' + @DB + ''' '                           
                              IF(@Branch = 'IND')                            
          Begin                            
         SET @sqltxt = @sqltxt + ' and Branch = ''IND'''                            
          end                            
          ELSE                                Begin                            
          SET @sqltxt = @sqltxt + ' and Branch not in (''IND'') ' -- ,''SHA'' 20191025            
          END                             
      print @sqltxt;                            
             set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                               
     --Separate ShipmentMTD Value and apply the condition remove Interco Transaction                          
          SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsMTD,Branch,Databases)                            
                     select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0) ) as ShipmentsMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec  join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco'' '                            
           IF(@Branch = 'IND')                            
     Begin                            
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh = ''IND'''                            
            end                            
            Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
           SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                  
                     
                                     
           SET @sqltxt = 'INSERT INTO #tmp(sDate,GP$MTD,LBShippedMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
           SUM(ISNULL(stn_npft_avg_val,0)) as GP$MTD,                            
           SUM(stn_blg_wgt) as LBShippedMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from '+ @DB +'_sahstn_rec                      
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id                       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                               
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )'                            
           IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh = ''IND'''                            
            end                            
            Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and stn_shpt_brh not in (''IND'')'   -- ,''SHA'' 20191025                         
            End                            
           SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
                     
            --DailyBooking                
              SET @sqltxt =    'INSERT INTO #tmp(sDate,DailyBooking,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0))) as DailyBooking,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                         
            join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                         
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
            and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @PTDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
 IF(@Branch = 'IND')                            
         Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh = ''IND'''                            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
          SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
                     
                                     
          SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingAvgDaily,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0))/' + @DaysYTD + ') as BookingAvgDaily,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                       
            join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                         
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
          and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                  
           IF(@Branch = 'IND')                            
         Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh = ''IND'''            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh not in (''IND'')'   -- ,''SHA'' 20191025                         
            End                            
          SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                      
                
                                
          SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingMTD,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           SUM( ISNULL(a.bka_tot_val,0) ) as BookingMTD,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM ['+ @DB +'_ortbka_rec] a                        
            join '+ @DB +'_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                              
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
           IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh = ''IND'''                            
            end                            
         Else                            
      Begin                            
             SET @sqltxt = @sqltxt + ' and a.bka_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
          SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                
         SET @sqltxt = 'INSERT INTO #tmp ( MTDOrdersCount,Branch,Databases)                          
          select COUNT(*),t.Branch,'''+ @DB +'''   from (                                   
       select ord_ord_no, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch                          
       from ' + @DB + '_ortord_rec                      
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                            
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                      
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       IF(@RunMonth = 0)                     
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSE                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                  
                                 
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'''                            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh not in (''IND'')' -- ,''SHA'' 20191025            
            End                            
          SET @sqltxt = @sqltxt + ' group by ord_ord_brh,ord_ord_no                          
       ) as t group by t.Branch'                            
       -- order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                
                                      
        SET @sqltxt = 'INSERT INTO #tmp ( OpenOrdersMTD,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0)) as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from ' + @DB + '_ortord_rec                      
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec               
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                         
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       if(@RunMonth = 0)                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSe                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                             
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'''                            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh not in (''IND'') '  -- ,''SHA'' 20191025                          
            End                            
          SET @sqltxt = @sqltxt + ' group by ord_ord_brh'                            
       -- order by ord_ord_no,ord_ord_itm,orl_due_to_dt                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;          
     EXEC (@execSQLtxt);                            
                
                                      
     SET @sqltxt = 'INSERT INTO #tmp ( TotalOpenOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0)) as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from ' + @DB + '_ortord_rec                      
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                            
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'''                            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
          SET @sqltxt = @sqltxt + ' group by ord_ord_brh'                            
       -- order by ord_ord_no,ord_ord_itm,orl_due_to_dt                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;      
     EXEC (@execSQLtxt);                            
                     
-- BackLog Orders 2020327 mail sub:AM Report        
SET @sqltxt = 'INSERT INTO #tmp ( BackLogOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0)) as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from ' + @DB + '_ortorh_rec join ' + @DB + '_ortord_rec  on ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id          
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       ' + @DB + '_ortorl_rec,                            
       ' + @DB + '_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                  
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'' and orh_ord_typ <> ''J'' '                            
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'''                            
            end                            
          Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' and ord_ord_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
          SET @sqltxt = @sqltxt + ' group by ord_ord_brh'                            
       -- order by ord_ord_no,ord_ord_itm,orl_due_to_dt                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                                  
                               
--SET @sqltxtPrd = 'select * from mxrusr_rec '                           
        SET @sqltxt = ' INSERT INTO #tempInvt ( InvtVal,TotVal,Branch)  select SUM(prd_ohd_mat_val) as InvtVal,                           
        sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,1)) / acp_tot_qty) ) as TotVal , (case prd_brh when ''PSM'' then Replace(prd_brh, ''PSM'', ''PIERCE'') else Replace(prd_brh, ''ROC'', ''ROS'') end) as Branch                            
  
   
       from ' + @DB + '_intprd_rec left join ' + @DB + '_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool                          
       where prd_invt_sts = ''S''  and (acp_tot_qty <> 0 or acp_tot_qty is null)  and   ' ;                          
       -- exec sp_itech_AM_Report  'US',''  ,'0'                          
-- exec sp_itech_AM_Report  'US','IND', '1'                            
        print 'END1 INTPRD'                          
                                  
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + '  prd_brh = ''IND'''                       
            end                            
         Else                            
            Begin                            
             SET @sqltxt = @sqltxt + ' prd_brh not in (''IND'')'  -- ,''SHA'' 20191025                          
            End                            
         SET @sqltxt = @sqltxt + ' group by prd_brh'                            
    print @sqltxt;                            
        set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                    
                
                                    
        SET @sqltxt = 'INSERT INTO #tmp ( sDate,Inventory,Branch,Databases)                            
       select CURRENT_TIMESTAMP as sDate,InvtVal+ TotVal as Inventory, Branch,'''+ @DB +'''                             
       from  #tempInvt ;'                           
                               
    print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                
                               
     delete from #tempInvt;                          
                               
     IF (@version = '1' and @Branch <> 'IND')                          
     BEGIN                          
  SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsAvgDaily, Branch,Databases)                            
                  select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                  (SUM(ISNULL(stn_tot_val,0) )/' + @DaysYTD + ') as ShipmentsAvgDaily,                                       
      ''PIERCE'', '''+ @DB +'''                            
      from PS_sahstn_rec    join PS_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
      where stn_inv_Dt >= '''+ @FDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco'' '                    
      SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01''                          
      order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                          
 SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsMTD,Branch,Databases)                            
             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0) ) as ShipmentsMTD,                            
            ''PIERCE'', '''+ @DB +'''                   
           from PS_sahstn_rec  join PS_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco'' '                            
           SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01''                           
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                                 
  SET @sqltxt = 'INSERT INTO #tmp(sDate,GP$MTD,LBShippedMTD,Branch,Databases)                            
                   select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                   SUM(ISNULL(stn_npft_avg_val,0)) as GP$MTD,                            
       SUM(stn_blg_wgt) as LBShippedMTD,                            
        ''PIERCE'', '''+ @DB +'''                            
       from PS_sahstn_rec                        
       join PS_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id                       
           left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
       where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null ) '                            
       SET @sqltxt = @sqltxt + ' group by convert(varchar(7),stn_inv_Dt, 126) +''-01''                          
       order by sDate desc'                            
    print(@sqltxt);                  
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                            
  SET @sqltxt = 'INSERT INTO #tmp(sDate,BookingAvgDaily,Branch,Databases)                            
                     SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                            
       (SUM( ISNULL(a.bka_tot_val,0))/' + @DaysYTD + ') as BookingAvgDaily,                            
     ''PIERCE'', '''+ @DB +''' FROM PS_ortbka_rec a                        
     join PS_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                             
       WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                      
      --AND a.bka_wgt > 0                            
       and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
    SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'''                            
    print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                             
         SET @sqltxt =  'INSERT INTO #tmp(sDate,BookingMTD,Branch,Databases)                            
                    SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
       SUM( ISNULL(a.bka_tot_val,0) ) as BookingMTD,                            
     ''PIERCE'', '''+ @DB +''' FROM [PS_ortbka_rec] a                       
     join PS_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
 WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                      
       --AND a.bka_wgt > 0                            
       and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
     SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'''                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                    
     EXEC (@execSQLtxt);                           
                              
        SET @sqltxt = 'INSERT INTO #tmp ( MTDOrdersCount,Branch,Databases)                          
      select COUNT(*),''PIERCE'','''+ @DB +'''   from (                                   
      select ord_ord_no,  ''PIERCE'' as Branch                          
      from PS_ortord_rec                      
      join PS_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
      PS_ortorl_rec,  PS_ortchl_rec                            
      where                            
      ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                       
      and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
      and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
      and orl_bal_qty > 0                             
      and chl_chrg_cl= ''E''                            
      and ord_sts_actn=''A''                            
      and ord_ord_pfx <>''QT'''                          
      IF(@RunMonth = 0)                          
      BEGIN                          
      SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
      END                          
      ELSE                          
      BEGIN                          
      SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
      END                             
   SET @sqltxt = @sqltxt + ' group by ord_ord_no                          
   ) as t '                            
    print(@sqltxt);                             
    set @execSQLtxt = @sqltxt;                             
    EXEC (@execSQLtxt);                          
                                      
      SET @sqltxt = 'INSERT INTO #tmp ( OpenOrdersMTD,Branch,Databases)                            
      select Sum(ISNULL(chl_chrg_val,0)) as ChargeValue, ''PIERCE'' as Branch,'''+ @DB +'''                             
      from PS_ortord_rec                      
      join PS_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                      
      PS_ortorl_rec,  PS_ortchl_rec                            
      where                            
      ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                       
      and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                            
      and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
      and orl_bal_qty > 0                             
      and chl_chrg_cl= ''E''                            
      and ord_sts_actn=''A''                            
      and ord_ord_pfx <>''QT'''                          
      if(@RunMonth = 0)                          
      BEGIN                          
      SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
      END                          
      ELSe                          
      BEGIN                          
      SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
      END                             
      SET @sqltxt = @sqltxt + ''                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
   EXEC (@execSQLtxt);                          
                                 
     SET @sqltxt = 'INSERT INTO #tmp ( TotalOpenOrders,Branch,Databases)                            
      select Sum(ISNULL(chl_chrg_val,0)) as ChargeValue, ''PIERCE'' as Branch,'''+ @DB +'''                             
      from PS_ortord_rec                      
      join PS_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join PS_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                      
       PS_ortorl_rec,  PS_ortchl_rec                            
      where                            
      ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
      and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
      and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
      and orl_bal_qty > 0                 
      and chl_chrg_cl= ''E''                            
      and ord_sts_actn=''A''                            
      and ord_ord_pfx <>''QT'''                            
     SET @sqltxt = @sqltxt + ' '                            
    print(@sqltxt);                             
    set @execSQLtxt = @sqltxt;                             
    EXEC (@execSQLtxt);                            
                               
    SET @sqltxt = ' INSERT INTO #tempInvt ( InvtVal,TotVal,Branch)  select SUM(prd_ohd_mat_val) as InvtVal,                           
    sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,1)) / acp_tot_qty) ) as TotVal , ''PIERCE'' as Branch                              
      from PS_intprd_rec left join PS_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool                          
      where prd_invt_sts = ''S''  and (acp_tot_qty <> 0 or acp_tot_qty is null)  ' ;                          
    print 'END1 INTPRD'                          
     SET @sqltxt = @sqltxt + ' '                           
    print @sqltxt;                            
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                             
                                    
     SET @sqltxt = 'INSERT INTO #tmp ( sDate,Inventory,Branch,Databases)                            
      select CURRENT_TIMESTAMP as sDate,InvtVal+ TotVal as Inventory, Branch,'''+ @DB +'''                             
      from  #tempInvt ;'                           
    print(@sqltxt);                             
    set @execSQLtxt = @sqltxt;                             
    EXEC (@execSQLtxt);                            
     delete from #tempInvt;                           
  END                             
                                 
     IF(@Branch <> 'IND')                            
           Begin                              
                                 
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 5, SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''SFS'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''800000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                                 
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 2, SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''HIB'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                  
                                 
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 8,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''WDL'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
                                 
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 1,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''EXP'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                               
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 3,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''JAC'' ,'''+ @DB +'''                             
     from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000'' '                   
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                               
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 4,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''ROS'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000'' '                          
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                          
                               
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 6,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''CRP'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000'' '                           
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                                
                         
       SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 7,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''PIERCE'' ,'''+ @DB +'''                             
       from PS_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                         
      SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 8,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''SEA'' ,'''+ @DB +'''                         
       from US_glhgld_rec                           
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                                 
      end                            
          Else                            
            Begin                            
     SET @sqltxt = 'INSERT INTO #tmp (SeqNo,WarehseFees,Branch,Databases)                            
       select 9,SUM(gld_cr_amt) - SUM(gld_dr_amt) as WarehseFees,''IND'' ,'''+ @DB +'''                             
       from ' + @DB + '_glhgld_rec                       
       where gld_bsc_gl_acct = 5300 and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FDMTD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000'' '                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
       End                           
     SET @sqltxt = 'INSERT INTO #tmp (Budget,Forecast,GPBudget,GPForecast,GPPctForecast,LBSShippedPlan,Branch,Databases)                            
       SELECT Budget, Forecast, GPBudget, GPForecast,GPForecastPct,LBSShippedPlan, BRANCH,'''+ @DB +'''                             
       FROM tbl_itech_Forecast_AM  Where Databases = ''US'' and YearMonth =  ''' +  @ForecastMonth + ''' '                            
       IF(@Branch = 'IND')                            
           Begin                            
             SET @sqltxt = @sqltxt + ' and BRANCH = ''IND'''                            
            end                            
          Else                            
            Begin                            
     SET @sqltxt = @sqltxt + ' and BRANCH not in (''IND'',''SHA'')'                            
            End                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
        end                  
                        
if(@Branch = 'IND')                
Begin                
--     For UK IND branch                
     SET @sqltxt = 'INSERT INTO #tmp(sDate,DailyShpt, Branch,Databases)                            
    select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +' as DailyShpt,                                       
         
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec    join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @PTDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''                     
            and stn_shpt_brh = ''IND''                            
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
-- For UK IND                
SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsAvgDaily, Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             (SUM(ISNULL(stn_tot_val,0)* '+ @CurrenyRate +' )/' + @DaysYTD + ') as ShipmentsAvgDaily,                               
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec    join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                          
           where stn_inv_Dt >= '''+ @FDYTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''  and stn_shpt_brh = ''IND''                
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_shpt_brh                            
           order by sDate desc'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
-- For UK IND                
     SET @sqltxt = 'INSERT INTO #tmp(sDate,ShipmentsMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
                             SUM(ISNULL(stn_tot_val,0)* '+ @CurrenyRate +' ) as ShipmentsMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec  join UK_arrcus_rec on cus_cus_id = stn_sld_cus_id                          
   left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and cuc_desc30 <> ''Interco''  and stn_shpt_brh = ''IND''                
            group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                                           
-- For UK IND                
 SET @sqltxt = 'INSERT INTO #tmp(sDate,GP$MTD,LBShippedMTD,Branch,Databases)                            
                             select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,                             
           SUM(ISNULL(stn_npft_avg_val,0))* '+ @CurrenyRate +' as GP$MTD,                            
           SUM(stn_blg_wgt* 2.20462) as LBShippedMTD,                            
            Replace(stn_shpt_brh, ''PSM'', ''PIERCE''), '''+ @DB +'''                            
           from UK_sahstn_rec                      
           join UK_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id                       
           left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                               
           where stn_inv_Dt >= '''+ @FDMTD +''' and stn_inv_dt <= '''+ @TDYTD +''' and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                
            and stn_shpt_brh = ''IND''                
             group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', stn_shpt_brh                            
           order by sDate desc'                            
      print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                           
-- For UK IND                
SET @sqltxt =    'INSERT INTO #tmp(sDate,DailyBooking,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0))* '+ @CurrenyRate +') as DailyBooking,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                         
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                         
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
            and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @PTDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
            and a.bka_brh = ''IND''                
             group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                  
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                 
-- For UK IND                
SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingAvgDaily,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           (SUM( ISNULL(a.bka_tot_val,0)* '+ @CurrenyRate +')/' + @DaysYTD + ') as BookingAvgDaily,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                         
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                                                         
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                        
            and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                           
           and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDYTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                            
            and a.bka_brh = ''IND'' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
-- For UK IND                
          SET @sqltxt =    'INSERT INTO #tmp(sDate,BookingMTD,Branch,Databases)                            
                           SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,                             
           SUM( ISNULL(a.bka_tot_val,0)* '+ @CurrenyRate +' ) as BookingMTD,                            
            Replace(a.bka_brh, ''PSM'', ''PIERCE''), '''+ @DB +''' FROM [UK_ortbka_rec] a                        
            join UK_arrcus_rec on cus_cmpy_id = a.bka_cmpy_id and cus_cus_id = a.bka_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat                       
           WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999                       
           and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                              
    and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)                
            and a.bka_brh = ''IND''                           
             group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',a.bka_brh'                            
         print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                     
-- For UK IND                
          SET @sqltxt = 'INSERT INTO #tmp ( MTDOrdersCount,Branch,Databases)                          
          select COUNT(*),t.Branch,'''+ @DB +'''   from (                                   
       select ord_ord_no, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch                          
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                            
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                      
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       IF(@RunMonth = 0)                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSE                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                             
                                
          SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'' group by ord_ord_brh,ord_ord_no                          
       ) as t group by t.Branch'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
-- For UK IND                
 SET @sqltxt = 'INSERT INTO #tmp ( OpenOrdersMTD,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                         
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                         
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                         
       and chl_chrg_cl= ''E''                            
  and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'''                          
       if(@RunMonth = 0)                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @LastDateOfCurrentMonth +''', 120)'                            
       END                          
       ELSe                          
       BEGIN                          
       SET @sqltxt = @sqltxt + ' and  CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FDMTD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TDYTD +''', 120)'                            
       END                             
                               
          SET @sqltxt = @sqltxt + ' and ord_ord_brh = ''IND'' group by ord_ord_brh'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                         
     EXEC (@execSQLtxt);                            
     --for UK IND                
     SET @sqltxt = 'INSERT INTO #tmp ( TotalOpenOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortord_rec                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where                            
                       
       ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                          
and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'' and ord_ord_brh = ''IND''                         
           group by ord_ord_brh'                            
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);            
-- BackLog 20200327        
SET @sqltxt = 'INSERT INTO #tmp ( BackLogOrders,Branch,Databases)                            
       select Sum(ISNULL(chl_chrg_val,0))* '+ @CurrenyRate +' as ChargeValue, Replace(ord_ord_brh, ''PSM'', ''PIERCE'') as Branch,'''+ @DB +'''                             
       from UK_ortorh_rec join UK_ortord_rec  on ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id                      
       join UK_arrcus_rec on cus_cmpy_id = ord_cmpy_id and cus_cus_id = ord_sld_cus_id left join UK_arrcuc_rec on cuc_cus_cat = cus_cus_cat ,                            
       UK_ortorl_rec,                            
       UK_ortchl_rec                            
       where   ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                        
       and     (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )                          
and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                            
       and orl_bal_qty > 0                             
       and chl_chrg_cl= ''E''                            
       and ord_sts_actn=''A''                            
       and ord_ord_pfx <>''QT'' and orh_ord_typ <> ''J'' and ord_ord_brh = ''IND''                         
           group by ord_ord_brh'                    
     print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);           
                                  
-- For UK IND                
   SET @sqltxt = ' INSERT INTO #tempInvt ( InvtVal,TotVal,Branch)  select SUM(prd_ohd_mat_val) as InvtVal,                           
        sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,1)) / acp_tot_qty) ) as TotVal , (case prd_brh when ''PSM'' then Replace(prd_brh, ''PSM'', ''PIERCE'') else Replace(prd_brh, ''ROC'', ''ROS'') end) as Branch                            
  
   
       from UK_intprd_rec left join UK_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool                          
       where prd_invt_sts = ''S''  and (acp_tot_qty <> 0 or acp_tot_qty is null)  and     prd_brh = ''IND'' group by prd_brh'                            
    print @sqltxt;                            
        set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                   
                     
      SET @sqltxt = 'INSERT INTO #tmp ( sDate,Inventory,Branch,Databases)                            
       select CURRENT_TIMESTAMP as sDate,InvtVal+ TotVal as Inventory, Branch,'''+ @DB +'''                             
       from  #tempInvt ;'                           
                               
    print(@sqltxt);                             
     set @execSQLtxt = @sqltxt;                             
     EXEC (@execSQLtxt);                            
                               
     delete from #tempInvt;                                          
                     
End                                  
                 
end                
                
                 
                CREATE TABLE #Main (item   Varchar(50)                            
            ,value   DECIMAL(20,1)                            
            ,Databases  varchar(15)                             
            ,Branch   Varchar(20)                            
                   );                            
                      
    INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'DailyShpt',DailyShpt,Branch,Databases  FROM #tmp where DailyShpt is not null             
                              
   INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'ShipmentsAvgDaily',ShipmentsAvgDaily,Branch,Databases  FROM #tmp where ShipmentsAvgDaily is not null                            
                              
    INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'PrvMonthShipmentsAvgDaily',PrvMonthShipmentsAvgDaily,Branch,Databases  FROM #tmp where PrvMonthShipmentsAvgDaily is not null                            
                              
   INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'BookingAvgDaily',BookingAvgDaily,Branch,Databases  FROM #tmp where BookingAvgDaily is not null                            
                              
    INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'PrvMonthBookingAvgDaily',PrvMonthBookingAvgDaily,Branch,Databases  FROM #tmp where PrvMonthBookingAvgDaily is not null                            
                               
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'ShipmentsMTD',ShipmentsMTD,Branch,Databases  FROM #tmp where ShipmentsMTD is not null                            
                               
     INSERT INTO #Main (item,value,Branch, Databases)                            
    SELECT  'PrvMonthShipmentsMTD',PrvMonthShipmentsMTD,Branch,Databases  FROM #tmp where PrvMonthShipmentsMTD is not null                            
                              
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'GP$MTD',GP$MTD,Branch,Databases  FROM #tmp where GP$MTD is not null                       
                              
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthGP$MTD',PrvMonthGP$MTD,Branch,Databases  FROM #tmp where PrvMonthGP$MTD is not null                            
                               
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'LBShippedMTD',LBShippedMTD,Branch,Databases  FROM #tmp where LBShippedMTD is not null                            
                              
INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthLBShippedMTD',PrvMonthLBShippedMTD,Branch,Databases  FROM #tmp where PrvMonthLBShippedMTD is not null                            
--DailyBooking                 
                
 INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'DailyBooking',DailyBooking,Branch,Databases  FROM #tmp where DailyBooking is not null                  
                                  
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'BookingMTD',BookingMTD,Branch,Databases  FROM #tmp where BookingMTD is not null                            
                              
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthBookingMTD',PrvMonthBookingMTD,Branch,Databases  FROM #tmp where PrvMonthBookingMTD is not null                            
                               
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'OpenOrdersMTD',OpenOrdersMTD,Branch,Databases  FROM #tmp where OpenOrdersMTD is not null                            
                              
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthOpenOrdersMTD',PrvMonthOpenOrdersMTD,Branch,Databases  FROM #tmp where PrvMonthOpenOrdersMTD is not null                            
                                
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'TotalOpenOrders',TotalOpenOrders,Branch,Databases  FROM #tmp where TotalOpenOrders is not null                           
-- BackLog BackLogOrders        
 INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'BackLogOrders',BackLogOrders,Branch,Databases  FROM #tmp where BackLogOrders is not null         
                                  
  INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthTotalOpenOrders',PrvMonthTotalOpenOrders,Branch,Databases  FROM #tmp where PrvMonthTotalOpenOrders is not null                     
                               
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'MTDOrdersCount',MTDOrdersCount,Branch,Databases FROM #tmp where MTDOrdersCount is not null                            
                              
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthMTDOrdersCount',PrvMonthMTDOrdersCount,Branch,Databases FROM #tmp where PrvMonthMTDOrdersCount is not null                              
                                
   INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'Inventory',Inventory,Branch,Databases FROM #tmp where Inventory is not null                            
                              
    INSERT INTO #Main (item,value,Branch,Databases)                            
    SELECT  'PrvMonthInventory',PrvMonthInventory,Branch,Databases FROM #tmp where PrvMonthInventory is not null                            
                                
   INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'Budget',Budget,Branch,Databases FROM #tmp where Budget is not null                            
                                 
     INSERT INTO #Main (item,value,Branch,Databases)               
     SELECT 'Forecast',Forecast,Branch,Databases FROM #tmp where Forecast is not null                            
                            
 INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'GPBudget',GPBudget,Branch,Databases FROM #tmp where GPBudget is not null                            
                               
INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'GPForecast',GPForecast,Branch,Databases FROM #tmp where GPForecast is not null                          
                             
 INSERT INTO #Main (item,value,Branch,Databases)                            
 SELECT 'GPPctForecast',GPPctForecast,Branch,Databases FROM #tmp where GPPctForecast is not null                             
                            
   INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'LBSShippedPlan',ISNULL(LBSShippedPlan,0),Branch,Databases FROM #tmp where LBSShippedPlan is not null                            
                               
     INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'PrvMonthLBSShippedPlan',ISNULL(PrvMonthLBSShippedPlan,0),Branch,Databases FROM #tmp where PrvMonthLBSShippedPlan is not null                            
                                 
     INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'WarehseFees',WarehseFees,Branch,Databases FROM #tmp where WarehseFees is not null                            
                               
     INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'PrvMonthWarehseFees',PrvMonthWarehseFees,Branch,Databases FROM #tmp where PrvMonthWarehseFees is not null                            
                                 
     INSERT INTO #Main (item,value,Branch,Databases)                            
     SELECT 'SeqNo',SeqNo,Branch,Databases FROM #tmp where SeqNo is not null                           
                               
   --  select * from #tmp order by sDate Desc;              
               -- Uncomment below insert statement during taking backup of AM report                 
--   insert   into tbl_itech_AM_HISTORY                     
-- Change upddtTm during taking backup of AM report                
   Select '2016-12-01 00:00:00.000' as UpdDtTm, Databases,Branch,                  
   -- Added start by mukesh 20150201                  
   -- Comment below two column when runing AM reprot history                
    ISNULL(DailyShpt,0) as  DailyShpt,                
    ISNULL(DailyBooking,0) as DailyBooking,                 
   (Case @DaysMTD when 0 then ISNULL(ShipmentsMTD,0) else ISNULL(ShipmentsMTD,0) / @DaysMTD end) AS ShipmentsAvgDaily,                     
   -- Added end                    
   ISNULL(PrvMonthShipmentsMTD,0) / @DaysMTDPM AS PrvMonthShipmentsAvgDaily,                    
   ISNULL(PrvMonthShipmentsMTD,0) AS PrvMonthShipmentsMTD,                        
   ISNULL(ShipmentsMTD,0) AS ShipmentsMTD,ISNULL(Budget,0) AS Budget,                            
   ISNULL(Forecast,0) AS Forecast,ISNULL(PrvMonthGP$MTD,0) AS PrvMonthGP$MTD,ISNULL(GP$MTD,0) AS GP$MTD,ISNULL(GPBudget,0) AS GPBudget,ISNULL(GPForecast,0) AS GPForecast,                            
   (Case ISNULL(ShipmentsMTD,0) When 0 then 0 else (ISNULL(GP$MTD,0)/ ShipmentsMTD)*100 end )AS GPPctMTD,     
   (Case ISNULL(Budget,0) When 0 then 0 else (ISNULL(GPBudget,0)/ Budget)*100 end )AS GPPctBdgYear,                          
   --(Case ISNULL(Forecast,0) When 0 then 0 else (ISNULL(GPForecast,0)/ Forecast)*100 end )AS GPPctForecast,                            
   ISNULL(GPPctForecast,0) AS GPPctForecast,                        
   ISNULL(PrvMonthBookingAvgDaily,0) AS PrvMonthBookingAvgDaily,                            
   ISNULL(PrvMonthBookingMTD,0) AS PrvMonthBookingMTD,ISNULL(BookingAvgDaily,0) AS BookingAvgDaily,                            
   ISNULL(BookingMTD,0) AS BookingMTD,ISNULL(TotalOpenOrders,0) AS TotalOpenOrders,ISNULL(BackLogOrders,0) AS BackLogOrders,ISNULL(PrvMonthTotalOpenOrders,0) AS PrvMonthTotalOpenOrders,ISNULL(PrvMonthOpenOrdersMTD,0) AS PrvMonthOpenOrdersMTD,            
  ISNULL(PrvMonthMTDOrdersCount,0) AS PrvMonthMTDOrdersCount,ISNULL(OpenOrdersMTD,0) AS OpenOrdersMTD,                          
   ISNULL(MTDOrdersCount,0) AS MTDOrdersCount,ISNULL(PrvMonthLBShippedMTD,0) AS PrvMonthLBShippedMTD,                            
   ISNULL(PrvMonthLBSShippedPlan,0) AS PrvMonthLBSShippedPlan,ISNULL(LBShippedMTD,0) AS LBShippedMTD,                            
   ISNULL(LBSShippedPlan,0) AS LBSShippedPlan,(Case ISNULL(LBSShippedPlan,0) When 0 then 0 else (ISNULL(LBShippedMTD,0)/ LBSShippedPlan)*100 end )AS PctShippedPlan,                            
   ISNULL(Inventory,0) AS Inventory,ISNULL(PrvMonthInventory,0) AS PrvMonthInventory,ISNULL(PrvMonthWarehseFees,0) AS PrvMonthWarehseFees, ISNULL(WarehseFees,0) AS WarehseFees,                            
   (Case ISNULL(ShipmentsMTD,0) When 0 then 0 else ((ISNULL(WarehseFees,0) + ISNULL(GP$MTD,0))/ ShipmentsMTD)*100 end )AS GPPctFeeMTD                            
     ,SeqNo                    
                         
   from (                            
 SELECT  item, Sum(value) as val , Databases,Branch from #Main                            
 Where Branch not in('BHM','MTL','ROC','CRP','SFS','PIERCE')   -- ,'TAI'  ,'SHA' 20191025                       
     group by item,Branch,Databases ) as s                            
     PIVOT                            
(                            
    Sum(val)                            
    FOR item IN (DailyShpt,DailyBooking,ShipmentsAvgDaily,PrvMonthShipmentsAvgDaily,PrvMonthShipmentsMTD, ShipmentsMTD,Budget,Forecast,PrvMonthGP$MTD,GP$MTD,              
    GPBudget,GPForecast,GPPctForecast,PrvMonthBookingAvgDaily,PrvMonthBookingMTD,BookingAvgDaily,BookingMTD,TotalOpenOrders, BackLogOrders,               
    PrvMonthTotalOpenOrders,PrvMonthOpenOrdersMTD,PrvMonthMTDOrdersCount,OpenOrdersMTD,MTDOrdersCount,PrvMonthLBShippedMTD,PrvMonthLBSShippedPlan,LBShippedMTD,LBSShippedPlan,Inventory,PrvMonthInventory,PrvMonthWarehseFees,WarehseFees,SeqNo)               
  
)AS p order by SeqNo                           
                         
 drop table #Main                            
 drop Table #tmp                             
 drop table #tempInvt                          
   print @DaysMTD                      
   print 'mukesh'                        
END                            
                            
-- exec sp_itech_AM_Report_V2  'ALL','' , '0','1'                        
-- exec sp_itech_AM_Report_V2  'TW','' , '1','1';                        
-- exec sp_itech_AM_Report  'US','TAI' , '0','1';                       
-- exec sp_itech_AM_Report  'ALL',''  , '1', '1'  ;                         
-- exec sp_itech_AM_Report_V2  'US','IND' , '0','1'   exec sp_itech_AM_Report  'US','IND' , '1','1'                           
---- exec sp_itech_AM_Report_V2  'ALL',''  , '0', '1'   exec sp_itech_AM_Report  'ALL',''  , '1', '1'                          
-- Previous Month                          
-- exec sp_itech_AM_Report_V2  'ALL','' , '1','1'                     
-- exec sp_itech_AM_Report_V2  'US','IND', '1' , '1'                            
---- exec sp_itech_AM_Report_V2  'ALL',''  , '1', '1'                           
-- select * from tbl_itech_AM_HISTORY order by updDtTm desc                    
                    
  --select * from tbl_itech_DatabaseName where Prefix != 'US'                            
                       
-- insert into tbl_itech_Forecast_AM                    
--Select [Budget],[Forecast],[GPBudget],[LBSShippedPlan],[WarehseFees],[Branch],[Databases],convert(varchar(7),GetDate(), 126),convert(varchar(10),GetDate(), 120)                    
--,[InvtValBrh],[GPForecast],[GPForecastPct] from [tbl_itech_Forecast_AM]  Where YearMonth = '2016-12'                
-- --  exec sp_itech_AM_Report_V4  'ALL','ALL' , '0','1'         
-- --  exec sp_itech_AM_Report_V4  'ALL','ALL' , '1','1'         
-- --  exec sp_itech_AM_Report_V4  'UK','DEU' , '0','1'         
-- --  exec sp_itech_AM_Report_V4  'UK','DEU' , '1','1'         
-- --  exec sp_itech_AM_Report_V4  'US','IND' , '0','1'         
-- --  exec sp_itech_AM_Report_V4  'US','IND' , '1','1'                   
-- --  exec sp_itech_AM_Report_V4  'US','ALL' , '0','1'           
-- --  exec sp_itech_AM_Report_V4  'US','ALL' , '1','1'         
        
        
/*        
Date: 20200327        
Mail Sub:AM Report        
20210104 Sumit  
removing DE database for previous month all database query  
Mail : RE: AM Report current and previous mismatch  
*/
GO
