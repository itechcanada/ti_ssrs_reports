USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_tctipd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[TW_tctipd]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.TW_tctipd_rec', 'U') IS NOT NULL          
  drop table dbo.TW_tctipd_rec;          
              
                  
SELECT *          
into  dbo.TW_tctipd_rec   
FROM [LIVETWSTX].[livetwstxdb].[informix].[tctipd_rec]     
WHERE ipd_ref_no not in (8675,8668,6039,5941)     
    
END    
    
GO
