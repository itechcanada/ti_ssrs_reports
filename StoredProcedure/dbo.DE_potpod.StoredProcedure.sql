USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_potpod]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Author,Sumit>      
-- Create date: <Create Date,10/29/2020>      
-- Description: <Germany Purchase Order Item Distribution>      
-- =============================================      
CREATE PROCEDURE [dbo].[DE_potpod]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
IF OBJECT_ID('dbo.DE_potpod_rec', 'U') IS NOT NULL      
  drop table dbo.DE_potpod_rec ;       

-- Insert statements for procedure here      
SELECT *      
into  dbo.DE_potpod_rec      
FROM [LIVEDESTX].[livedestxdb].[informix].[potpod_rec]      
     
 MERGE tbl_itech_potpod_history AS T      
USING DE_potpod_rec AS S      
ON ( T.pod_cmpy_id = S.pod_cmpy_id and T.pod_po_pfx = S.pod_po_pfx and T.pod_po_no = S.pod_po_no      
and T.pod_po_itm = s.pod_po_itm and T.pod_po_dist = S.pod_po_dist       
)       
WHEN NOT MATCHED BY TARGET       
    THEN INSERT(pod_cmpy_id,pod_po_pfx,pod_po_no,pod_po_itm,pod_po_dist,pod_arr_dt_ent,pod_arr_fm_dt,pod_arr_to_dt,recordCreatedDate)       
    VALUES(S.pod_cmpy_id, S.pod_po_pfx, S.pod_po_no, S.pod_po_itm,S.pod_po_dist,       
(case len(S.pod_arr_dt_ent) when 8 then  cast(cast(S.pod_arr_dt_ent as varchar(10)) as date) else Null end ) ,S.pod_arr_fm_dt,S.pod_arr_to_dt, GETDATE() );      
     
      
END      
GO
