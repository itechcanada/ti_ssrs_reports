USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_funSub_ContactorID]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Function to retrieve 
CREATE FUNCTION  [dbo].[fun_itech_funSub_ContactorID](@DBNAME Varchar(5), @CustomerID Varchar(15)) RETURNS varchar(500)
AS
BEGIN

DECLARE @Value Varchar(500)
declare @DB varchar(100)


--set @DB= UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'

    IF (UPPER(@DBNAME) = 'TW')
    begin
	   SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_TW_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
	End
	Else if (UPPER(@DBNAME) = 'NO')
    begin
	    SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_NO_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
    Else if(UPPER(@DBNAME) = 'UK')
    begin
       SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_UK_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
    Else if(UPPER(@DBNAME) = 'US')
    begin
     --  SET @Value = (select ISNULL(contractor_cust_id,Null)  from tbl_itech_US_Sub_Contactor where id =@CustomerID)
     SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_US_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
    Else if(UPPER(@DBNAME) = 'CN')
    begin
       SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_CN_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
     Else if(UPPER(@DBNAME) = 'PS')
    begin
       SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_PS_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
    Else if(UPPER(@DBNAME) = 'CA')
    begin
       SET @Value = (SELECT STUFF(
             (SELECT ',''' + cast(contractor_cust_id AS varchar(15) ) + ''''
              FROM tbl_itech_CA_Sub_Contactor
              FOR XML PATH (''))
             , 1, 1, ''))
    End
if @Value='' or @Value is null
Begin
 set @Value='''0'''
End
	
RETURN @Value

END
--select dbo.fun_itech_funSub_ContactorID('US','1111111111')




--SELECT dbo.Fun_itech_funCurrencyConvert('NO','USD','NOK') AS TEST


GO
