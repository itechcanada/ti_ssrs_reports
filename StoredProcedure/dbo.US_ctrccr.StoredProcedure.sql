USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ctrccr]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                                                              
-- Author:  <Author,Sumit>                                                              
-- Create date: <Create Date,11/20/2020,>                                                              
-- Description: <Description,US Process Cost Number,>  
-- =============================================                                                              
CREATE PROCEDURE [dbo].[US_ctrccr]                                
AS                                                              
BEGIN                                                              
 -- SET NOCOUNT ON added to prevent extra result sets from                                                              
 -- interfering with SELECT statements.                                                              
SET NOCOUNT ON;                                                              

truncate table dbo.US_ctrccr_rec ;                                                               
-- Insert statements for procedure here                                                              
Insert into dbo.US_ctrccr_rec                                     
SELECT *                                                              
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ctrccr_rec] ;                                     
                                    
                                          
END         
        
        
/*

*/
GO
