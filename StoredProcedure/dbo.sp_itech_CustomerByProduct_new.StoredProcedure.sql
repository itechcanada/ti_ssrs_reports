USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerByProduct_new]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <11 Feb 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_CustomerByProduct_new]@FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID  varchar(Max),@DateRange int,@CustmomerNoTxt varchar(Max)  
  
AS  
BEGIN  
  
 
-- kill  
   
 SET NOCOUNT ON;  



declare @sqltxt varchar(max)  
declare @execSQLtxt varchar(max)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
SET @DB=@DBNAME;  
  
CREATE TABLE #tmp (  CustID varchar(15)   
        ,CustName Varchar(65)  
        ,InvDt varchar(15)  
        ,Product   varchar(300)  
        , Inches    DECIMAL(20, 2)  
        , Weight  DECIMAL(20, 2)  
        , Part   VARCHAR(50)   
        , Date     varchar(65)  
        ,Form  varchar(35)  
        ,Grade  varchar(35)  
        ,Size  varchar(35)  
        ,Finish  varchar(35)  
        ,Measure  DECIMAL(20, 2)  
        ,TotalValue DECIMAL(20, 2)  
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
DECLARE @Name VARCHAR(15);  
DECLARE @CusID varchar(max);  
Declare @Value as varchar(500);  
DECLARE @CustIDLength int;   ---SELECT DATALENGTH(yourtextfield)  
  
--SET @CustomerID= '1111111111,1003'  
if @CustmomerNoTxt <> ''  
 BEGIN  
  set @CustomerID = @CustmomerNoTxt  
 END  
  
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));  
  
if @CustomerID = ''  
 BEGIN  
  set @CustomerID = '0'  
 END  
  
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month  
 End  
else if @DateRange=2 -- Last 7 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=3 -- Last 14 days (excluding today)  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day  
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day  
 End  
else if @DateRange=4 --Last 12 months excluding all the days in the current month  
 BEGIN  
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month  
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month  
 End  
else  
 Begin  
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
 End  
  
        --->> Input customer data  
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)  
  SELECT @pos=0  
  SELECT @input =@CustomerID   
  SELECT @input = @input + ','  
  CREATE TABLE #tempTable (temp varchar(max) )  
  WHILE CHARINDEX(',',@input) > 0  
  BEGIN  
   SELECT @pos=CHARINDEX(',',@input)  
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))  
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))  
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)  
  END  
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable  
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)  
  DROP TABLE #tempTable  
    
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
     -- SET @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
      SET @query =  
       'INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue )  
       SELECT  stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, stn_blg_wgt as Weight,  
        frm_Desc25 + '' '' + grd_desc25 as Product,    
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  stn_upd_cy as Date ,stn_blg_MSR as Measure,STN_TOT_VAL  
       FROM  ' + @Prefix + '_sahstn_rec,  ' + @Prefix + '_inrfrm_rec,    
       ' + @Prefix + '_inrgrd_rec, ' + @Prefix + '_arrcus_rec  
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm   
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'  
         
     if @CustomerID Like '0'  
      BEGIN  
      Set @query += ' and  (stn_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')'   
      END  
     ELSE  
      BEGIN  
         if @CustomerID Like '1111111111'  
       BEGIN  
          
        Set @Value= (select dbo.fun_itech_funSub_ContactorID(@Prefix,@CustomerID))  
          
        Set @query += ' and  stn_sld_cus_id IN ('+ @Value +')'   
       END  
       Else  
       BEGIN  
         Set @query += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(cast(@CustomerID as varchar(Max))))+')'  
       END  
      END  
        
      Set @query += ' and stn_Frm <> ''XXXX'''  
  
                  print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
     SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue )  
       SELECT  stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, stn_blg_wgt as Weight,  
        frm_Desc25 + '' '' + grd_desc25 as Product,    
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  stn_upd_cy as Date,stn_blg_MSR as Measure ,STN_TOT_VAL  
       FROM  ' + @DB + '_sahstn_rec,  ' + @DB + '_inrfrm_rec,    
       ' + @DB + '_inrgrd_rec, ' + @DB + '_arrcus_rec  
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm   
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'  
     if @CusID Like '%''0''%'  
      BEGIN  
      Set @sqltxt += ' and  (stn_sld_cus_id = '''' or ''''= '''')'   
      END  
     Else  
      BEGIN  
          if @CusID Like '%''1111111111''%' --- Eaton Group  
        BEGIN  
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))  
          Set @CusID= @CusID +','+ @Value  
          Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'   
        END  
       Else  
        BEGIN  
             Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'  
        END  
      END  
      Set @sqltxt += ' and stn_Frm <> ''XXXX'''  
        
     print(@DB)  
     --print(@CustomerID)  
     print(@CustIDLength)  
     print(@CusID)  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     END  
  SELECT * FROM #tmp order by CustID   
  DROP TABLE #tmp  
END  
  
-- exec sp_itech_CustomerByProduct_new '04/01/2013', '5/13/2013' ,'US','1111111111','5',''  
  
GO
