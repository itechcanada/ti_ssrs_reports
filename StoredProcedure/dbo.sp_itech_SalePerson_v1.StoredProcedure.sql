USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalePerson_v1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mrinal >  
-- Create date: <20 NOV 2013>  
-- Description: <Getting Sale Person form each database for SSRS reports> 
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_SalePerson_v1]  @DBNAME varchar(50),@version char = '0'  
  
AS  
BEGIN  
   
   
   
 SET NOCOUNT ON;  
declare @sqltxt1 varchar(8000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
  
CREATE TABLE #tmp (   
     [Database]   VARCHAR(10)  
     , salePerson  VARCHAR(65)  
                );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
   
 IF @version = '0'    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
   OPEN ScopeCursor;    
    END    
    ELSE    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    END   
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query1 NVARCHAR(max);     
       SET @query1 ='INSERT INTO #tmp ([Database], salePerson)                     
                   SELECT  Distinct ''' +  @Prefix + ''' as [Database],  
         cta_crtd_lgn_id AS SalePerson from  ' + @Prefix + '_cctcta_rec   
         UNION  
         SELECT  Distinct ''' +  @Prefix + ''' as [Database],  
         cay_prfd_by AS SalePerson from  ' + @Prefix + '_cctcay_rec ;'   
       print @query1;  
        EXECUTE sp_executesql @query1;   
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
    IF @version = '0'    
    BEGIN    
    set @Prefix=  (select Prefix from tbl_itech_DatabaseName where Prefix=''+ @DBNAME +'')  
    END  
    ELSE  
    BEGIN  
    set @Prefix=  (select Prefix from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME +'')  
    END  
        SET @sqltxt1 ='INSERT INTO #tmp ([Database], salePerson)                     
                   SELECT  Distinct ''' +  @Prefix + ''' as [Database],  
         cta_crtd_lgn_id AS SalePerson from  ' + @Prefix + '_cctcta_rec   
         UNION  
         SELECT  Distinct ''' +  @Prefix + ''' as [Database],  
         cay_prfd_by AS SalePerson from  ' + @Prefix + '_cctcay_rec ;'   
     print(@sqltxt1)   
   EXEC (@sqltxt1);  
   END  
     
select salePerson AS Value ,salePerson AS text, 'B' AS temp from #tmp  
Union    
Select 'ALL' as Value,'All Sale Person' as text,'A' as temp  
 order by temp,text  
   drop table #tmp  
END  
  
  
-- Exec [sp_itech_SalePerson_v1] 'ALL','1'

GO
