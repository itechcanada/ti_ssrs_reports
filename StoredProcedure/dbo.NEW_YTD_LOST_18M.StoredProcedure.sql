USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[NEW_YTD_LOST_18M]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[NEW_YTD_LOST_18M]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 'YTD New Accounts' as account, DATEPART(month,b.coc_frst_sls_dt) as Mon, a.cus_admin_brh, COUNT(b.coc_frst_sls_dt) as total
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrcus_rec] a
	INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
	a.cus_actv=1
	AND CAST(b.coc_frst_sls_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY DATEPART(month,b.coc_frst_sls_dt),a.cus_admin_brh
UNION	
SELECT 'Lost Accounts 18 months' as account, DATEPART(month,b.coc_lst_sls_dt) as Mon, a.cus_admin_brh,COUNT(b.coc_lst_sls_dt) as total
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrcus_rec] a
	INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
WHERE
	a.cus_actv=1
	AND DATEDIFF(month,CAST(b.coc_lst_sls_dt AS datetime),@ToDate)>18 AND DATEDIFF(month,CAST(b.coc_lst_sls_dt AS datetime),@ToDate)<21
GROUP BY DATEPART(month,b.coc_lst_sls_dt), a.cus_admin_brh



END



GO
