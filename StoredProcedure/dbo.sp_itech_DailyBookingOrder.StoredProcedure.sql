USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_DailyBookingOrder]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_DailyBookingOrder] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@DateRange int, @version char = '0'

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @CurrenyRate varchar(15)
DECLARE @WgtConvert varchar(15)
set @WgtConvert =1

set @DB= @DBNAME 

if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )
	BEGIN
		set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month
		set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month
	End
else if @DateRange=2 -- Last 7 days (excluding today)
	BEGIN
		set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day
		set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day
	End
else if @DateRange=3 -- Last 14 days (excluding today)
	BEGIN
		set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day
		set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day
	End
else if @DateRange=4 --Last 12 months excluding all the days in the current month
	BEGIN
		set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month
		set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month
	End
else if @DateRange=5 -- Taday
	BEGIN
		set @FD =  CONVERT(VARCHAR(10), GETDATE() , 120)   -- Current Date
		set @TD = CONVERT(VARCHAR(10), GETDATE(), 120)  -- Current Date
	End
else
	Begin
		set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
		set @TD = CONVERT(VARCHAR(10), @ToDate , 120)
	End

CREATE TABLE #tmp ( 
   					 OrderNo varchar(10)
   					,Item   varchar(3)
   					,Branch varchar(10)
   					,CompanyID varchar(3)
   					,CustID		 VARCHAR(10)
   					,CustName 			 VARCHAR(65)
   					,Product       VARCHAR(500) 
   					,Date			  varchar(65)
   					,Wgt  decimal(20,2)
   					,Booked  decimal(20,2)
   					,ReplCost  decimal(20,2)
   					,Profit  decimal(20,2)
   					,Market	 varchar(65),
   					FormGrade varchar(35),
   					Size varchar(20),
   					Finish varchar(20),
   					DBName varchar(2)
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(5);
DECLARE @Name VARCHAR(15);

if @Market ='ALL'
 BEGIN
 set @Market = ''
 END
 
 if @Branch ='ALL'
 BEGIN
 set @Branch = ''
 END

IF @DBNAME = 'ALL'
	BEGIN
	
	
	
	
	
		  IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000);  	
  				SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				
  				 IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK')) 
     set @WgtConvert = 2.20462       
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
       set @WgtConvert = 2.20462        
    End 
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
    End 
  				
  				
  				
  				
  				
  				
  				SET @query =' INSERT INTO #tmp ( CompanyID,CustID,CustName,Market,Branch,Date,OrderNo,Item,Product,Wgt,Booked,ReplCost,Profit, FormGrade, Size, Finish, DBName )
							SELECT b.CUS_CMPY_ID,b.cus_cus_id as id,b.cus_cus_long_nm as CustName,cuc_desc30 as Market ,a.bka_brh as Branch,
							a.bka_actvy_dt as Date, a.bka_ord_no as Ord,
							a.bka_ord_itm as itm, a.bka_frm+a.bka_grd+a.bka_size+a.bka_fnsh as Product,a.bka_wgt * '+ @WgtConvert + ' as wgt,
							a.bka_tot_val * '+ @CurrenyRate +'  as booked,
							''ReplCost''=CASE WHEN a.bka_mtl_repl_val=0 THEN a.bka_mtl_avg_val *  '+ @CurrenyRate +' ELSE a.bka_mtl_repl_val * '+ @CurrenyRate +' END,
							''Profit''=CASE WHEN a.bka_tot_mtl_val=0 THEN 0 ELSE(a.bka_tot_mtl_val-(CASE WHEN a.bka_mtl_repl_val=0 THEN a.bka_mtl_avg_val ELSE a.bka_mtl_repl_val END)) /a.bka_tot_mtl_val  *100 END,
							Rtrim(LTrim(a.bka_frm)) + ''/'' + RTrim(LTRIM(a.bka_grd)),RTrim(LTrim(a.bka_size)), RTrim(LTrim(a.bka_fnsh)), ''' + @DB + '''
							FROM ['+ @DB +'_ortbka_rec] a
							INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
							left JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
							WHERE a.bka_ord_pfx=''SO''  
							-- AND a.bka_ord_itm<>999 
							-- and  a.bka_trs_md <> ''D''
							-- AND a.bka_wgt > 0
							and (a.bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							and (a.bka_brh not in (''SFS''))
							and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120) 
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
								BEGIN
								  SET @query= @query + ' and (cuc_desc30 <> ''Interco'' OR c.cuc_desc30 is null) '
								END
								
							SET @query= @query +   ' order by a.bka_brh, a.bka_ord_no'

print(@query);	
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
  BEgin
  
  Print (' I am done here');
				IF (UPPER(@DBNAME) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@DBNAME) = 'NO')
				begin
					SET @CurrenyRate = (SElECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
					set @WgtConvert = 2.20462
				End
				Else if (UPPER(@DBNAME) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@DBNAME) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
				End
				Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')
				begin
					SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@DBNAME) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				   set @WgtConvert =2.20462
				End
				Else if(UPPER(@DBNAME) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
				Else if(UPPER(@DBNAME) = 'TWCN')
				begin
				   SET @DB ='TW'
				   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				
  
    
			  SET @sqltxt ='INSERT INTO #tmp ( CompanyID,CustID,CustName,Market,Branch,Date,OrderNo,Item,Product,Wgt,Booked,ReplCost,Profit, FormGrade, Size, Finish, DBName )
							SELECT b.CUS_CMPY_ID,b.cus_cus_id as id,b.cus_cus_long_nm as CustName,cuc_desc30 as Market ,a.bka_brh as Branch,a.bka_actvy_dt as Date, a.bka_ord_no as Ord,
							a.bka_ord_itm as itm, a.bka_frm+a.bka_grd+a.bka_size+a.bka_fnsh as Product,a.bka_wgt * ' +@WgtConvert +'  as wgt,
							a.bka_tot_val * '+ @CurrenyRate +'  as booked,
							''ReplCost''=CASE WHEN a.bka_mtl_repl_val=0 THEN a.bka_mtl_avg_val * '+ @CurrenyRate +' ELSE a.bka_mtl_repl_val * '+ @CurrenyRate +' END,
							''Profit''=CASE WHEN a.bka_tot_mtl_val=0 THEN 0 ELSE(a.bka_tot_mtl_val-(CASE WHEN a.bka_mtl_repl_val=0 THEN a.bka_mtl_avg_val ELSE a.bka_mtl_repl_val END))/a.bka_tot_mtl_val*100 END
							,Rtrim(LTrim(a.bka_frm)) + ''/'' + RTrim(LTRIM(a.bka_grd)),RTrim(LTrim(a.bka_size)),RTrim(LTrim(a.bka_fnsh)), ''' + @DB + '''
							FROM ['+ @DB +'_ortbka_rec] a
							INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
							left JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
							WHERE a.bka_ord_pfx=''SO'' 
							-- AND a.bka_ord_itm<>999 
							-- and a.bka_trs_md <> ''D''
							-- AND a.bka_wgt > 0
							and (a.bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							and (a.bka_brh not in (''SFS''))
							and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120) 
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
								BEGIN
								  SET @sqltxt= @sqltxt + ' and (cuc_desc30 <> ''Interco'' OR c.cuc_desc30 is null) '
								END
								
							SET @sqltxt= @sqltxt +   ' order by a.bka_brh, a.bka_ord_no'
					
					print(@sqltxt);	
				set @execSQLtxt = @sqltxt; 
		   EXEC (@execSQLtxt);
     END
   
  -- SELECT * FROM #tmp;-- order by CustID 
  select *, (select COUNT(*) from tbl_itechTipProduct t where t.FormGrade = #tmp.FormGrade and t.Size = #tmp.Size and t.Finish = #tmp.Finish and t.DBName = #tmp.DBName) as iTipCtr from #tmp order by OrderNo
 --select * from #tmp  left  join  tbl_itechTipProduct t on  t.Product = #tmp.FormGrade and t.Finish = #tmp.Finish and t.DBName = #tmp.DBName order by OrderNo
   drop table #tmp 
END


-- 



--exec sp_itech_DailyBookingOrder '09/01/2015', '09/30/2015' , 'US','ALL','ALL',0
GO
