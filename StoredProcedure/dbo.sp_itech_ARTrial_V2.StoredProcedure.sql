USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ARTrial_V2]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                  
-- Author:  <Sumit >                                  
-- Create date: <11/24/2020>                                  
-- Description: <AR Trial Report>    
-- =============================================   
CREATE PROCEDURE [dbo].[sp_itech_ARTrial_V2] @DBNAME varchar(50), @Branch varchar(15) , @AgingDate datetime                              
AS                                  
BEGIN                                  
                                   
 SET NOCOUNT ON;                           
   
 declare @sqltxt varchar(max)                                  
 declare @execSQLtxt varchar(max)                                  
 declare @DB varchar(100)        
 declare @AgeDate varchar(10)
                                   
 set @DB=  @DBNAME                               
 set @AgeDate = convert(varchar(10), @AgingDate,120)
                               
 CREATE TABLE #tmp1 ( DBName   VARCHAR(100)   
  , Branch   VARCHAR(3)  
  , CustID Varchar(50)  
  , CustLongName     VARCHAR(100)                                  
        , CustCry   VARCHAR(15)     
  , CustFirstNM  Varchar(100)                                  
        , CustLastNM  Varchar(100)  
  , CustPCD varchar(10)    
  , CustPhone Varchar(30)                          
        , CustEmail Varchar(100)  
  , TotalBal Decimal(20,2)                                  
  , CurrBal Decimal(20,2)  
  , CurrBalPct Decimal(20,2)  
        , Bal30     Decimal(20,2)  
  , Bal30Pct  Decimal(20,2)  
  , Bal60     Decimal(20,2)                     
        , Bal60Pct  Decimal(20,2)  
  , Bal90     Decimal(20,2)  
  , Bal90Pct  Decimal(20,2)  
  , Bal120     Decimal(20,2)  
  , Bal120Pct  Decimal(20,2)  
  , Bal999     Decimal(20,2)  
  , Bal999Pct  Decimal(20,2)  
  );  
    
 --CREATE TABLE #tmp2 ( DBName   VARCHAR(100)   
 -- , Branch VARCHAR(3)  
 -- , CustID Varchar(50)  
 -- , TotalBal Decimal(20,2)                                  
 -- , CurrBal Decimal(20,2)  
 -- , CurrBalPct float  
 --       , Bal30     Decimal(20,2)  
 -- , Bal30Pct  float  
 -- , Bal60     Decimal(20,2)                     
 --       , Bal60Pct  float  
 -- , Bal90     Decimal(20,2)  
 -- , Bal90Pct  float  
 -- , Bal120     Decimal(20,2)  
 -- , Bal120Pct  float  
 -- , Bal999     Decimal(20,2)  
 -- , Bal999Pct  float  
 -- );  
    
 DECLARE @DatabaseName VARCHAR(35);                                  
 DECLARE @Prefix VARCHAR(5);                                  
 DECLARE @Name VARCHAR(15);                                  
 DECLARE @CurrencyRate varchar(15);  
   
 if @Branch ='ALL'                                  
 BEGIN                                  
  set @Branch = ''                                  
 END                                  
                                  
 IF @DBNAME = 'ALL'                                  
 BEGIN                                  
  DECLARE ScopeCursor CURSOR FOR                                  
  select DatabaseName, Name,prefix from tbl_itech_DatabaseName                                   
  OPEN ScopeCursor;                                  
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                  
  WHILE @@FETCH_STATUS = 0                                  
  BEGIN                                  
   SET @DB= @Prefix                                  
   print(@DB)                             
                                       
   IF (UPPER(@DB) = 'TW')                                  
   begin                                  
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                  
   End                                  
   Else if (UPPER(@DB) = 'NO')                                  
   begin                         
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                  
   End                                  
   Else if (UPPER(@DB) = 'CA')                                  
   begin                                  
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                  
   End                                  
   Else if (UPPER(@DB) = 'CN')                                  
   begin                                  
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                  
   End                                  
   Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                                  
   begin                                  
    SET @CurrencyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                  
   End                         
   Else if(UPPER(@DB) = 'UK')                                  
   begin                                  
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                  
   End                                  
   Else if(UPPER(@DB) = 'DE')                                  
   begin                                  
    SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                  
   End                                  
   Else if(UPPER(@DB) = 'TWCN')                                  
   begin                                  
    SET @DB ='TW'                                  
    SET @CurrencyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                  
   End                                  
     
   print 'Ending else';                                  
   print @DB +' - '+ @CurrencyRate ;  
     
   set @sqltxt = 'insert into #tmp1(DBName, Branch, CustID, CustLongName, CustCry, CustFirstNM, CustLastNM, CustPCD, CustPhone, CustEmail  
   , TotalBal, CurrBal, Bal30, Bal60, Bal90, Bal120, Bal999)  
   select distinct '''+ @DB +''', cus_admin_brh, cus_cus_id, cus_cus_long_nm, rvh_cry, cvt_frst_nm , cvt_lst_nm , LTRIM(RTRIM(cvt_tel_area_cd)) , cvt_tel_no , cvt_email  
   ,sum(ISNULL(rvh_balamt * rvh_exrt ,0) * ' + @CurrencyRate + '  )   
   , SUM((case when datediff(DAY, rvh_due_dt , GETDATE()) <= 0 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''current''  
   ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 1 and 30 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal30''  
   ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 31 and 60 then ISNULL(rvh_balamt  * rvh_exrt,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal60''  
   ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 61 and 90 then ISNULL(rvh_balamt  * rvh_exrt,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal90''  
   ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 91 and 120 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal120''  
   ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) > 120 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal999''  
   from '+ @DB +'_arrcus_rec   
   join '+ @DB +'_artrvh_rec on rvh_cmpy_id = cus_cmpy_id and rvh_cr_ctl_cus_id = cus_cus_id  
   join '+ @DB +'_arrcrd_rec on crd_cmpy_id = rvh_cmpy_id and crd_cus_id = rvh_cr_ctl_cus_id  
   join '+ @DB +'_arrshp_rec on shp_cmpy_id = rvh_cmpy_id and shp_cus_id = rvh_cr_ctl_cus_id and shp_shp_to = ''0''  
   join '+ @DB +'_arbcoc_rec on coc_cmpy_id = rvh_cmpy_id AND coc_cus_id = rvh_cr_ctl_cus_id   
   left outer join '+ @DB +'_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_cus_ven_id = cus_cus_id and cvt_addr_typ = ''L'' and cvt_cus_ven_typ = ''C'' and cvt_cntc_typ = ''AP'' and cvt_cntc_no = ''1''  
   where (cus_admin_brh = '''+ @Branch +''' OR  '''+ @Branch +''' = '''')  
   and convert(varchar(10),rvh_due_dt,120) <= '''+ @AgeDate +'''
   group by cus_admin_brh,cus_cus_id, cus_cus_long_nm,rvh_cry, cvt_frst_nm, cvt_lst_nm, cvt_tel_area_cd, cvt_tel_no, cvt_email  
   '  
   print (@sqltxt);  
   exec (@sqltxt);  
                                       
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                  
  END                                   
  CLOSE ScopeCursor;             
  DEALLOCATE ScopeCursor;                                  
 END                                  
 ELSE                                  
    BEGIN                                   
  print 'starting' ;                           
  --Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')            
                                    
  IF (UPPER(@DB) = 'TW')                                  
  begin                                  
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                  
  End                                  
  Else if (UPPER(@DB) = 'NO')                                  
  begin                                  
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                  
  End                                  
  Else if (UPPER(@DB) = 'CA')                                  
  begin                                  
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                  
  End                                  
  Else if (UPPER(@DB) = 'CN')                                  
  begin                       
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                  
  End                                  
  Else if (UPPER(@DB) = 'US' OR UPPER(@DBNAME) = 'PS')                                  
  begin                                  
   SET @CurrencyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                  
  End                                  
  Else if(UPPER(@DB) = 'UK')                                  
  begin                                  
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                  
  End                                  
  Else if(UPPER(@DB) = 'DE')                                  
  begin                                  
   SET @CurrencyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                  
  End                                  
  Else if(UPPER(@DB) = 'TWCN')                                  
  begin                                  
   SET @DB ='TW'                                  
   SET @CurrencyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                  
  End  
    
  print 'Ending else';                                  
  print @DB +' - '+ @CurrencyRate ;                                  
    
  set @sqltxt = 'insert into #tmp1(DBName, Branch, CustID, CustLongName, CustCry, CustFirstNM, CustLastNM, CustPCD, CustPhone, CustEmail  
  , TotalBal, CurrBal, Bal30, Bal60, Bal90, Bal120, Bal999)  
  select distinct '''+ @DB +''', cus_admin_brh, cus_cus_id, cus_cus_long_nm, rvh_cry, cvt_frst_nm , cvt_lst_nm , LTRIM(RTRIM(cvt_tel_area_cd)) , cvt_tel_no , cvt_email  
  ,sum(ISNULL(rvh_balamt * rvh_exrt,0) * ' + @CurrencyRate +' )   
  , SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) <= 0 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''current''  
  ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 1 and 30 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal30''  
  ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 31 and 60 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal60''  
  ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 61 and 90 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal90''  
  ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) between 91 and 120 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal120''  
  ,SUM((case when datediff(DAY, rvh_due_dt ,  GETDATE()) > 120 then ISNULL(rvh_balamt * rvh_exrt ,0) else 0 end) * ' + @CurrencyRate + '  ) as ''Bal999''  
  from '+ @DB +'_arrcus_rec   
  join '+ @DB +'_artrvh_rec on rvh_cmpy_id = cus_cmpy_id and rvh_cr_ctl_cus_id = cus_cus_id  
  join '+ @DB +'_arrcrd_rec on crd_cmpy_id = rvh_cmpy_id and crd_cus_id = rvh_cr_ctl_cus_id  
  join '+ @DB +'_arrshp_rec on shp_cmpy_id = rvh_cmpy_id and shp_cus_id = rvh_cr_ctl_cus_id and shp_shp_to = ''0''  
  join '+ @DB +'_arbcoc_rec on coc_cmpy_id = rvh_cmpy_id AND coc_cus_id = rvh_cr_ctl_cus_id   
  left outer join '+ @DB +'_scrcvt_rec on cvt_cmpy_id = cus_cmpy_id and cvt_addr_typ = ''L'' and cvt_cus_ven_id = cus_cus_id and cvt_cus_ven_typ = ''C'' and cvt_cntc_typ = ''AP'' and cvt_cntc_no = ''1''  
  where (cus_admin_brh = '''+ @Branch +''' OR  '''+ @Branch +''' = '''' )  
  and convert(varchar(10),rvh_due_dt,120) <= '''+ @AgeDate +''' 
  group by cus_admin_brh,cus_cus_id, cus_cus_long_nm,rvh_cry, cvt_frst_nm, cvt_lst_nm, cvt_tel_area_cd, cvt_tel_no, cvt_email'  
  print (@sqltxt);  
  exec (@sqltxt);  
 END    
   
 set @sqltxt = 'update #tmp1 set CurrBalPct = case when TotalBal = 0.00 then 0.00 else ((CurrBal/TotalBal)*100) end, Bal30Pct = case when TotalBal = 0.00 then 0.00 else ((Bal30/TotalBal)*100) end,   
 Bal60Pct = case when TotalBal = 0.00 then 0.00 else ((Bal60/TotalBal)*100) end, Bal90Pct = case when TotalBal = 0.00 then 0.00 else ((Bal90/TotalBal)*100) end,   
 Bal120Pct = case when TotalBal = 0.00 then 0.00 else ((Bal120/TotalBal)*100) end, Bal999Pct = case when TotalBal = 0.00 then 0.00 else ((Bal999/TotalBal)*100) end'  
 print (@sqltxt);  
 exec (@sqltxt);  
  
 select  DBName , Branch, CustID, CustLongName, CustCry, TRIM(CustFirstNM +' '+ CustLastNM +' '+ (case when CustPCD != '' then '('+ CustPCD +')' end)  +' '+  
 CustPhone +' '+ CustEmail) as CustContact , TotalBal                                  
 , CurrBal, CurrBalPct, Bal30, Bal30Pct, Bal60, Bal60Pct, Bal90, Bal90Pct, Bal120, Bal120Pct, Bal999, Bal999Pct from #tmp1   
 where TotalBal <> 0
 order by DBName,Branch, CustID  
      
END                      
                 
/*                              
exec sp_itech_ARTrial_V1 'ALL','ALL'  
exec sp_itech_ARTrial_V2 'DE','ALL' , '2025-12-31'  
  
  
*/
GO
