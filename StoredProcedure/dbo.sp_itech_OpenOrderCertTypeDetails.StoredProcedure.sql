USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenOrderCertTypeDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mayank >        
-- Create date: <22 Dec 2016>        
-- Description: <Getting top 50 customers for SSRS reports>        
    
     
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_OpenOrderCertTypeDetails] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@OrderType varchar(10),@version char = '0',@DateRange int = 0, @CustomerID varchar(10) = ''      
  
        
AS        
BEGIN       
  
   
         
 SET NOCOUNT ON;        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)        
declare @NOOfCust varchar(15)        
DECLARE @ExchangeRate varchar(15)    
  
      
     if(@FromDate = '' OR @FromDate = null OR @FromDate = '1900-01-01' )  
     begin  
     set @FromDate = '2009-01-01'  
      End  
        
      if (@ToDate = '' OR @ToDate = null OR @ToDate = '1900-01-01')  
      begin  
      set @ToDate = CONVERT(VARCHAR(10), GETDATE() , 120)  
      end  
        
set @DB= @DBNAME -- UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'    
if @DateRange=6 -- YTD (excluding today)        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) , 120)   -- first Day of Year       
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) --     
 End         
else if @DateRange=2 -- Last 7 days (excluding today)        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day        
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day        
 End        
Else        
Begin         
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)        
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)        
End        
        
        
CREATE TABLE #tmp (  CompanyId varchar(5)         
        ,OrderPrefix Varchar(2)        
        ,OrderNo varchar(10)        
        ,OrderItem   varchar(3)        
        ,Branch varchar(10)        
        ,CustID   VARCHAR(10)         
        , CustName     VARCHAR(65)        
        , CustomerCat   VARCHAR(65)         
        , ChargeValue    DECIMAL(20, 2)        
        , BalPcs  DECIMAL(20, 2)        
        , BalWgt           DECIMAL(20, 2)        
        , BalMsr    DECIMAL(20, 2)        
        , StatusAction               varchar(2)        
        , Salesperson     varchar(65)        
        , DueDate    varchar(20)        
        , ProductName   VARCHAR(100)         
        , OrderType     varchar(65)        
        ,Form  varchar(35)        
        ,Grade  varchar(35)        
        ,Size  varchar(35)        
        ,Finish  varchar(35)       
        ,CustPO   VARCHAR(30)     
        ,CustAdd VARCHAR(170)     
         ,SalesPersonIs Varchar(10)      
        ,SalesPersonOs Varchar(10)     
        ,NetAmt Decimal(20,2)    
        ,NetPct Decimal(20,2)     
        ,CertType Varchar(max)  
        ,AdminBrh Varchar(5)  
        , ShipTo    Varchar(5)    
                 );    
                 CREATE TABLE #tmp2 (  CompanyId varchar(5)         
        ,OrderPrefix Varchar(2)        
        ,OrderNo varchar(10)        
        ,OrderItem   varchar(3)  
        ,CertType Varchar(max) );  
               
CREATE TABLE #tmp1 (  CompanyId varchar(5)         
        ,OrderPrefix Varchar(2)        
        ,OrderNo varchar(10)        
        ,OrderItem   varchar(3)        
        ,Branch varchar(10)        
        ,CustID   VARCHAR(10)         
        , CustName     VARCHAR(65)        
        , CustomerCat   VARCHAR(65)         
        , ChargeValue    DECIMAL(20, 2)        
        , BalPcs  DECIMAL(20, 2)        
        , BalWgt           DECIMAL(20, 2)        
        , BalMsr    DECIMAL(20, 2)        
        , StatusAction               varchar(2)        
        , Salesperson     varchar(65)        
        , DueDate    varchar(20)        
        , ProductName   VARCHAR(100)         
        , OrderType     varchar(65)        
        ,Form  varchar(35)        
        ,Grade  varchar(35)        
        ,Size  varchar(35)        
        ,Finish  varchar(35)       
        ,CustPO   VARCHAR(30)     
        ,CustAdd VARCHAR(170)     
         ,SalesPersonIs Varchar(10)      
        ,SalesPersonOs Varchar(10)     
        ,NetAmt Decimal(20,2)    
        ,NetPct Decimal(20,2)     
                 );         
        
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @Prefix VARCHAR(5);        
DECLARE @Name VARCHAR(15);        
DECLARE @CurrenyRate varchar(15);         
        
if @Market ='ALL'        
 BEGIN        
 set @Market = ''        
 END        
         
 if @Branch ='ALL'        
 BEGIN        
 set @Branch = ''        
 END        
        
if @OrderType ='ALL'        
 BEGIN        
 set @OrderType = ''        
 END        
  -- All database query is wrong.      
IF @DBNAME = 'ALL'        
 BEGIN        
 IF @version = '0'          
    BEGIN          
    DECLARE ScopeCursor CURSOR FOR          
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName          
   OPEN ScopeCursor;          
    END          
    ELSE          
    BEGIN          
    DECLARE ScopeCursor CURSOR FOR          
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS           
   OPEN ScopeCursor;          
    END          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query nvarchar(max);           
      SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'        
      IF (UPPER(@Prefix) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@Prefix) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@Prefix) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@Prefix) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                
    begin                
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@Prefix) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End           
    Else if(UPPER(@Prefix) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End           
      SET @query =        
       'INSERT INTO #tmp1 ( CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, ChargeValue,BalPcs, BalMsr, BalWgt, StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd, SalesPersonIs,
   
SalesPersonOs,NetAmt, NetPct)        
       select ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_ord_brh as Branch,        
       ord_sld_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as CustomerCat,chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,        
       orl_bal_pcs as BalPcs,orl_bal_msr as BalMsr,'                
           if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                   
             SET @query = @query + ' orl_bal_wgt * 2.20462 as BalWgt,'                
           ELSE                
            SET @query = @query + ' orl_bal_wgt as BalWgt, '                
                           
           SET @query = @query + ' ord_sts_actn as StatusAction ,usr_nm as Salesperson        
       ,orl_due_to_dt as DueDate,LTRIM(RTRIM(ipd_frm)) + ''-'' + LTRIM(RTRIM(ipd_grd)) + ''-'' + LTRIM(RTRIM(ipd_size)) + ''-'' + LTRIM(RTRIM(ipd_fnsh))   as ProductName,cds_desc        
       ,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)), LTRIM(RTRIM(ord_cus_po))     
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)) + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))           
       ,sis.slp_lgn_id, os.slp_lgn_id,oit_npft_avg_val * '+ @CurrenyRate +',      
       (Case ISNULL(cht_tot_val,0) When 0 then 0 else (ISNULL(oit_npft_avg_val,0)/ISNULL(cht_tot_val,0))*100 end ) as netpct    
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0     
         JOIN ' + @DB + '_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0      
            LEFT JOIN ' + @DB + '_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp      
        LEFT JOIN ' + @DB + '_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp ,        
       ' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,        
       ' + @DB + '_tctipd_rec, ' + @DB + '_ortchl_rec        
       , ' + @DB + '_scrslp_rec ou,' + @DB + '_mxrusr_rec        
       ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec , ' + @DB + '_ortoit_rec , ' + @DB + '_ortcht_rec  where        
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id        
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id        
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no        
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm        
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id        
       and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ     
       and oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm     
       AND  ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm  
        and cht_tot_typ = ''T''          
       and orl_bal_qty >= 0         
       and chl_chrg_cl= ''E''        
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''        
       and orh_sts_actn <> ''C''        
       and ord_ord_pfx <>''QT''         
       and (   (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )    
       or  (orl_due_to_dt is null and orh_ord_typ =''J''))    
       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
       and (orh_ord_typ = '''+ @OrderType +''' or '''+ @OrderType +'''= '''')  
       and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')  
        and  ord_ord_brh not in (''SFS'')      
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'        
        
     print(@query);     
        EXECUTE sp_executesql @query;      
          
        declare @query1 nvarchar(4000);  
        set @query1 = 'INSERT INTO #tmp2 ( CompanyId,OrderPrefix ,OrderNo,OrderItem,CertType)  
        select itc_cmpy_id, itc_ref_pfx, itc_ref_no, itc_ref_itm,itc_cert_typ from ' + @DB + '_tctitc_rec  
        '  
           print(@query1);     
        EXECUTE sp_executesql @query1;    
       /* insert into #tmp  
         select CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, ChargeValue,BalPcs, BalMsr, BalWgt, StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd, SalesPersonIs,   
SalesPersonOs,NetAmt, NetPct,  
       (SELECT STUFF((SELECT Distinct ',' + RTRIM(itc_cert_typ )               
    FROM  ''@Prefix'' +_tctitc_rec where   itc_ref_pfx = OrderPrefix and itc_ref_no = OrderNo and itc_ref_itm = OrderItem               
        FOR XML PATH('')),1,1,'')) AS CertType   from #tmp1 */  
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
    IF @version = '0'          
     BEGIN            
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')          
     END        
     ELSE        
     BEGIN        
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')          
     END        
             
     IF (UPPER(@DB) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@DB) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@DB) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@DB) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@DB) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End          
    Else if(UPPER(@DB) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End          
            
     SET @sqltxt ='INSERT INTO #tmp ( CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, ChargeValue,BalPcs, BalMsr, BalWgt, StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd ,   
     SalesPersonIs, SalesPersonOs ,NetAmt, NetPct,CertType,AdminBrh,ShipTo)        
       select ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_ord_brh as Branch,        
       ord_sld_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as CustomerCat,chl_chrg_val * '+ @CurrenyRate +' as ChargeValue,        
       orl_bal_pcs as BalPcs,orl_bal_msr as BalMsr,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                               
            SET @sqltxt = @sqltxt + ' orl_bal_wgt * 2.20462 as BalWgt,'                
           ELSE                
            SET @sqltxt = @sqltxt + ' orl_bal_wgt as BalWgt, '                
                           
           SET @sqltxt = @sqltxt + ' ord_sts_actn as StatusAction ,usr_nm as Salesperson        
       ,orl_due_to_dt as DueDate,LTRIM(RTRIM(ipd_frm)) + ''-'' + LTRIM(RTRIM(ipd_grd)) + ''-'' + LTRIM(RTRIM(ipd_size)) + ''-'' + LTRIM(RTRIM(ipd_fnsh))   as ProductName,cds_desc        
       ,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)), LTRIM(RTRIM(ord_cus_po))    
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city)) + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))        
       ,sis.slp_lgn_id, os.slp_lgn_id,oit_npft_avg_val * '+ @CurrenyRate +',     
       (Case ISNULL(cht_tot_val,0) When 0 then 0 else (ISNULL(oit_npft_avg_val,0)/ISNULL(cht_tot_val,0))*100 end ) as netpct ,  
       itc_cert_typ AS CertType  ,cus_admin_brh,ord_shp_to   
       from ' + @DB + '_ortord_rec  
       join ' + @DB + '_ortorh_rec on  ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id    
        LEFT JOIN ' + @DB + '_scrslp_rec os on os.slp_cmpy_id = orh_cmpy_id and os.slp_slp = orh_os_slp      
        LEFT JOIN ' + @DB + '_scrslp_rec sis on sis.slp_cmpy_id = orh_cmpy_id and sis.slp_slp = orh_is_slp   
        left join ' + @DB + '_tctitc_rec on itc_cmpy_id = ord_cmpy_id and itc_ref_pfx = ord_ord_pfx and itc_ref_no = ord_ord_no and itc_ref_itm = ord_ord_itm   
       ,' + @DB + '_arrcus_rec left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0     
        ,' + @DB + '_ortorl_rec,        
       ' + @DB + '_tctipd_rec, ' + @DB + '_ortchl_rec        
       , ' + @DB + '_scrslp_rec ou,' + @DB + '_mxrusr_rec        
       ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec , ' + @DB + '_ortoit_rec, ' + @DB + '_ortcht_rec where        
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id        
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no        
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm        
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id        
       and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ      
       and oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm AND     
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm   
        and cht_tot_typ = ''T''        
       and orl_bal_qty >= 0         
       and chl_chrg_cl= ''E''        
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''        
       and orh_sts_actn <> ''C''        
       and ord_ord_pfx <>''QT''         
       and  (     
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )    
       or    
       (orl_due_to_dt is null and orh_ord_typ =''J''))    
           
       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
       and (orh_ord_typ = '''+ @OrderType +''' or '''+ @OrderType +'''= '''')   
       and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')    
       and  ord_ord_brh not in (''SFS'')        
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'        
       print('test')  ;    
     print(@sqltxt);         
    set @execSQLtxt = @sqltxt;         
       EXEC (@execSQLtxt);        
   --and ord_ord_pfx=''SO''         
     END        
   SELECT distinct * FROM #tmp     
   --Where NetPct < -100     
   order by CustID         
END        
        
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@OrderType varchar(10)        
        
-- exec [sp_itech_OpenOrderCertTypeDetails] '11/01/2016', '11/30/2016' , 'US','ALL','ALL','ALL','ALL' ,6       
    
/*   
  
date : 2016-12-22  
MAil sub: FYI  
*/
GO
