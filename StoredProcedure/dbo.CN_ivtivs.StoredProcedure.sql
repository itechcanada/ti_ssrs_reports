USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_ivtivs]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 01, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[CN_ivtivs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CN_ivtivs_rec', 'U') IS NOT NULL
		drop table dbo.CN_ivtivs_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CN_ivtivs_rec
  from [LIVECNSTX].[livecnstxdb].[informix].[ivtivs_rec] ; 
  
END
-- select * from CN_ivtivs_rec 
GO
