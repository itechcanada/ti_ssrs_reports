USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrcrx]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: 10/29/2020  
-- Description: <Germany Currency description>  
-- =============================================  
create PROCEDURE [dbo].[DE_scrcrx]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.DE_scrcrx_rec', 'U') IS NOT NULL  
  drop table dbo.DE_scrcrx_rec;  
      
          
SELECT *  
into  dbo.DE_scrcrx_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[scrcrx_rec];  
  
END  
GO
