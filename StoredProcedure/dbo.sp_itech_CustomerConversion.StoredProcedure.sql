USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerConversion]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <05 Jan 2017>    
-- Description: <Customer Conversion Date>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_CustomerConversion] @DBNAME varchar(50), @Branch varchar(3), @fromdate  datetime, @todate datetime    
    
AS    
BEGIN    
    
     
 SET NOCOUNT ON;    
declare @sqltxt varchar(max)    
declare @execSQLtxt varchar(max)    
declare @DB varchar(10)    
DECLARE @ExchangeRate varchar(15)    
DECLARE @CurrenyRate varchar(15)     
declare @FD varchar(10)        
declare @TD varchar(10)   
    
SET @DB=@DBNAME;    
    
CREATE TABLE #tmp ( Databases varchar(3)      
  ,CustID   VARCHAR(100)      
        , CustName     VARCHAR(100)    
         ,CustCreationDate  Varchar(10)    
        ,CustConversationDate  Varchar(10)   
        ,conversionday  int 
        ,CustFirstslsDate  Varchar(10)  --   
        ,CustBranch  Varchar(3)
                 );     
                     
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)        
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
    
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
DECLARE @Name VARCHAR(15);    

IF @Branch = 'ALL'
Begin 
set @Branch = '';
End

    
      
IF @DBNAME = 'ALL'    
 BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
    OPEN ScopeCursor;    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(4000);     
        IF (UPPER(@Prefix) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@Prefix) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@Prefix) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@Prefix) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@Prefix) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End       
    Else if(UPPER(@Prefix) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End       
              
       SET @query = 'INSERT INTO #tmp (Databases,CustID, CustName,CustCreationDate,CustConversationDate,conversionday,CustFirstslsDate,CustBranch)    
       SElect '''+ @Prefix + ''' as Databases, cus_cus_id, cus_cus_nm, crd_acct_opn_dt, Cast(Convert(Varchar(10),ava_attr_val_var ) as date),  
DATEDIFF(DAY ,crd_acct_opn_dt,Cast(Convert(Varchar(10),ava_attr_val_var ) as date)),dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) ,
cus_admin_brh 
from '+ @Prefix + '_arrcus_rec  
 join '+ @Prefix + '_arrcrd_rec on cus_cmpy_id = crd_cmpy_id and cus_cus_id = crd_cus_id   
 LEFT JOIN '+ @Prefix + '_xctava_rec on ava_key_val = ltrim(cus_cmpy_id) + ltrim(cus_cus_id) and ava_tbl_nm = ''arrcus'' and ava_atmpl = ''CNV-DT''   
 and ava_attr = ''CNV-DT''  
 left join '+ @Prefix + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id 
 Where Cast(Convert(Varchar(10),ava_attr_val_var ) as date) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)  
 and (cus_admin_brh = ''' + @Branch + ''' or ''' + @Branch + ''' = '''')
   '    
         
               
           
                  print(@query);    
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
      IF (UPPER(@DB) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@DB) = 'NO')     
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@DB) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@DB) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@DB) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End      
    Else if(UPPER(@DB) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End      
        
         
       -- * 2.20462 as Weight        
       SET @sqltxt =  'INSERT INTO #tmp (Databases,CustID, CustName,CustCreationDate,CustConversationDate,conversionday,CustFirstslsDate,CustBranch)    
       SElect '''+ @DB + ''' as Databases, cus_cus_id, cus_cus_nm, crd_acct_opn_dt, Cast(Convert(Varchar(10),ava_attr_val_var ) as date),  
DATEDIFF(DAY ,crd_acct_opn_dt,Cast(Convert(Varchar(10),ava_attr_val_var ) as date)),dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) ,
 cus_admin_brh
from '+ @DB + '_arrcus_rec  
 join '+ @DB + '_arrcrd_rec on cus_cmpy_id = crd_cmpy_id and cus_cus_id = crd_cus_id   
 LEFT JOIN '+ @DB + '_xctava_rec on ava_key_val = ltrim(cus_cmpy_id) + ltrim(cus_cus_id) and ava_tbl_nm = ''arrcus'' and ava_atmpl = ''CNV-DT''   
 and ava_attr = ''CNV-DT''  
 left join '+ @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id 
 Where Cast(Convert(Varchar(10),ava_attr_val_var ) as date) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)
 and (cus_admin_brh = ''' + @Branch + ''' or ''' + @Branch + ''' = '''')    
       '             
        
      
     print(@sqltxt);     
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
     END    
   SELECT  * FROM #tmp ;    
  DROP TABLE #tmp    
END    
    
-- exec [sp_itech_CustomerConversion] 'US','ALL','2016-01-01','2017-01-05'    
  /*  
Date: 20170103  
Sub:New Report  
    
Report Name: Customer Conversion Date   
Only Available for US Database   
Input:   
From Date  : Compared to Customer_Conversion_Date  
To Date: Compared to Customer_Conversion_Date  
Output  
• STRATIX Customer ID  
• Customer Name  
• Customer Creation Date   
• Customer Conversion Date (If Customer Conversion Date is BLANK then we will show blank  
• Number of Days take for conversion : Customer Conversion Date - Customer Creation Date (If Customer Conversion Date is BLANK then we will show blank)  
Order by Customer ID   
SQL  
select cus_cus_id, cus_cus_nm, crd_acct_opn_dt, ava_attr_val_var as Customer_Conversion_Date from US_arrcus_rec join   
US_arrcrd_rec on cus_cmpy_id = crd_cmpy_id   
and cus_cus_id = crd_cus_id LEFT JOIN [LIVEUSSTX].[liveusstxdb].[informix].[xctava_rec]   
on ava_key_val = ltrim(cus_cmpy_id) + ltrim(cus_cus_id) and ava_tbl_nm = 'arrcus' and ava_atmpl = 'CNV-DT' and ava_attr = 'CNV-DT'  
where crd_cus_id =  '33'  
  
  Date : 2017-01-06
  Mail Body: This report is looking good I would make the attached recommendations
  
  Date: 20170131
  Mail sub: can i get a branch option added to this report please and to look at all branches we may should put the CSX option in every environment.
  
  */
GO
