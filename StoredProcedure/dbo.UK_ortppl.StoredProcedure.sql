USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ortppl]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: jun 25, 2019 
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[UK_ortppl]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.UK_ortppl_rec', 'U') IS NOT NULL  
  drop table dbo.UK_ortppl_rec;  
          
SELECT *  
into  dbo.UK_ortppl_rec  
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ortppl_rec];  
  
END  
-- select * from UK_ortppl_rec
GO
