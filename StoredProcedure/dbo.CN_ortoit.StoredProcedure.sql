USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_ortoit]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[CN_ortoit]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CN_ortoit_rec', 'U') IS NOT NULL
		drop table dbo.CN_ortoit_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CN_ortoit_rec
  from [LIVECNSTX].[livecnstxdb].[informix].[ortoit_rec] ; 
  
END
-- select * from CN_ortoit_rec 
GO
