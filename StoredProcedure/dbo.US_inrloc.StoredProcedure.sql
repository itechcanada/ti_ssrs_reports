USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_inrloc]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 7, 2015,>
-- Description:	<Description,Material,>

-- =============================================
create PROCEDURE [dbo].[US_inrloc]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_inrloc_rec', 'U') IS NOT NULL
		drop table dbo.US_inrloc_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_inrloc_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[inrloc_rec] ; 
  
END
-- select * from US_inrloc_rec
GO
