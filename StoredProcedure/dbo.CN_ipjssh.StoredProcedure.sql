USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_ipjssh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 6, 2015,>  
-- Description: <Description,gl account,>  
  
-- =============================================  
create PROCEDURE [dbo].[CN_ipjssh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_ipjssh_rec', 'U') IS NOT NULL  
  drop table dbo.CN_ipjssh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_ipjssh_rec  
  from [LIVECNSTX].[livecnstxdb].[informix].[ipjssh_rec];   
    
END  
-- select * from CN_ipjssh_rec
GO
