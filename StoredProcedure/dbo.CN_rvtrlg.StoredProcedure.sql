USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_rvtrlg]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Sumit>
-- Create date: <Create Date,Oct 6, 2021,>
-- Description:	<Description,Reservation Update>

-- =============================================
CREATE PROCEDURE [dbo].[CN_rvtrlg]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.CN_rvtrlg_rec', 'U') IS NOT NULL        
  drop table dbo.CN_rvtrlg_rec;        
            
                
SELECT *        
into  dbo.CN_rvtrlg_rec  
FROM [LIVECNSTX].[livecnstxdb].[informix].[rvtrlg_rec]  
  
  
END
GO
