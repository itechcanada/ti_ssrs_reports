USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrbrh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: 10/29/2020  
-- Description: <Germany Database Branch>  
-- =============================================  
create PROCEDURE [dbo].[DE_scrbrh]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
    -- Insert statements for procedure here 
    IF OBJECT_ID('dbo.DE_scrbrh_rec', 'U') IS NOT NULL  
  drop table dbo.DE_scrbrh_rec;  
          
SELECT *  
into  dbo.DE_scrbrh_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[scrbrh_rec];  
  
END  
-- select * from DE_scrbrh_rec  
GO
