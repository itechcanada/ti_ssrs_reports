USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_plrrpd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 23, 2014,>  
-- Description: <Description, Replenishment Produc>  
  
-- =============================================  
Create PROCEDURE [dbo].[TW_plrrpd]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_plrrpd_rec', 'U') IS NOT NULL  
  drop table dbo.TW_plrrpd_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_plrrpd_rec  
  from [LIVETW_IW].[livetwstxdb_iw].[informix].[plrrpd_rec] ;   
    
END  
-- select * from TW_plrrpd_rec
GO
