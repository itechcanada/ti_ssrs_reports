USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_nctncf]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mukesh>  
-- Create date: <Create Date,Oct 05, 2015,>  
-- Description: <Description,NCR,>  
  
-- =============================================  
create PROCEDURE [dbo].[CA_nctncf]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CA_nctncf_rec', 'U') IS NOT NULL  
  drop table dbo.CA_nctncf_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CA_nctncf_rec  
  from [LIVECASTX].[livecastxdb].[informix].[nctncf_rec];   
    
END  
-- select * from CA_nctncf_rec
GO
