USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ortorl]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,10/1/2012,>
-- Description:	<Description,Warehouse Shipment On Time Performance,>

-- =============================================
CREATE PROCEDURE [dbo].[TW_ortorl]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.TW_ortorl_rec	
Insert into dbo.TW_ortorl_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVETWSTX].[livetwstxdb].[informix].[ortorl_rec]


END
GO
