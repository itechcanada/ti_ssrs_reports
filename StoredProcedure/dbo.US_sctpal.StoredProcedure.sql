USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sctpal]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[US_sctpal] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_sctpal_rec', 'U') IS NOT NULL
		drop table dbo.US_sctpal_rec;
    
        
SELECT *
into  dbo.US_sctpal_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[sctpal_rec];

END
 --  exec  US_sctpal
-- select * from US_sctpal_rec
GO
