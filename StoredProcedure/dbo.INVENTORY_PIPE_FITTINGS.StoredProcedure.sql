USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[INVENTORY_PIPE_FITTINGS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,2/3/2012>
-- Description:	<Description,Pipe and Fittings Inventory for USA only>
-- =============================================
CREATE PROCEDURE [dbo].[INVENTORY_PIPE_FITTINGS]
	-- Add the parameters for the stored procedure here
	 @EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
CREATE TABLE #PRODUCT
(
[type] varchar(10),
product varchar(50),
frmgrd varchar(25),
size varchar(25),
[3m] float,
[6m] float,
[12m] float,
po float,
so float,
stock float,
stock_cost float,
stock_value float,
stock_mo_supply float,
po_mo_supply float
)

INSERT INTO #PRODUCT
SELECT 'FITTINGS' , prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,prm_size,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL 
FROM [US_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
UNION
SELECT 'PIPE' , prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,prm_size,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL 
FROM [US_inrprm_rec]
WHERE prm_frm+prm_grd IN ('TISP  2','TIWP  2')
/*Shipment*/
CREATE TABLE #SHIPMENT
(
[type] varchar(10),
product varchar(50),
[3m] float,
[6m] float,
[12m] float
)

INSERT INTO #SHIPMENT
SELECT 
'FITTINGS',
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_pcs END),
'6m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_pcs END),
'12m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_pcs END)
FROM [US_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' and
sat_sls_cat<> 'CO' AND
sat_frm+sat_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'PIPE',
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_msr END)/12,0),
'6m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_msr END)/12,0),
'12m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_msr END)/12,0)
FROM [US_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO' AND
sat_frm+sat_grd IN ('TISP  2','TIWP  2')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh


/*Open PO*/
CREATE TABLE #OPEN_PO
(
[type] varchar(10),
product varchar(50),
po float
)
INSERT INTO #OPEN_PO
SELECT 'FITTINGS', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_pcs)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh

INSERT INTO #OPEN_PO
SELECT 'PIPE', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, ROUND(SUM(a.pod_bal_msr)/12,0)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh


/*OPEN SO*/
CREATE TABLE #OPEN_SO
(
[type] varchar(10),
product varchar(50),
so float
)
INSERT INTO #OPEN_SO
SELECT 'FITTINGS', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_pcs) 
FROM [US_ortchl_rec] a
INNER JOIN [US_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A' AND
c.ipd_frm+c.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO
SELECT 'PIPE', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product, ROUND(SUM(b.ord_bal_msr)/12,0)
FROM [US_ortchl_rec] a
INNER JOIN [US_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A' AND
c.ipd_frm+c.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh




/*STOCK STATUS*/
CREATE TABLE #STOCK_STATUS1
(
[type] varchar(10),
product varchar(50),
stock float,
stock_cost float,
stock_value float
)
INSERT INTO #STOCK_STATUS1
SELECT 'FITTINGS' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_pcs, NULL, NULL
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
UNION
SELECT 'PIPE' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_msr, NULL, NULL
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TISP  2','TIWP  2')



UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b,[US_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.[type]='FITTINGS'

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [US_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.[type]='PIPE'

CREATE TABLE #STOCK_STATUS2
(
[type] varchar(10),
product varchar(50),
stock float,
stock_cost float,
stock_value float
)
INSERT INTO #STOCK_STATUS2
SELECT [type], product, SUM(stock), AVG(stock_cost), AVG(stock_cost)*SUM(stock)
FROM #STOCK_STATUS1
WHERE [type]='FITTINGS'
GROUP BY [type], product
UNION
SELECT [type], product, ROUND(SUM(stock)/12,0), AVG(stock_cost), AVG(stock_cost)/12*SUM(stock)
FROM #STOCK_STATUS1
WHERE [type]='PIPE'
GROUP BY [type], product


UPDATE #PRODUCT
SET [3m]=b.[3m],
    [6m]=b.[6m],
    [12m]=b.[12m]
FROM #PRODUCT a, #SHIPMENT b
WHERE a.[type]=b.[type] AND a.product=b.product
AND (b.[3m] IS NOT NULL OR b.[6m] IS NOT NULL OR b.[12m] IS NOT NULL)


UPDATE #PRODUCT
SET po=b.po
FROM #PRODUCT a ,#OPEN_PO b
WHERE a.[type]=b.[type] AND a.product=b.product
AND b.po IS NOT NULL

UPDATE #PRODUCT
SET so=b.so
FROM #PRODUCT a , #OPEN_SO b
WHERE a.[type]=b.[type] AND a.product=b.product
AND b.so IS NOT NULL

UPDATE #PRODUCT
SET stock=b.stock,
    stock_cost=b.stock_cost,
    stock_value=b.stock_value
FROM #PRODUCT a,#STOCK_STATUS2 b
WHERE a.[type]=b.[type] AND a.product=b.product
AND (b.stock IS NOT NULL OR b.stock_cost IS NOT NULL OR b.stock_value IS NOT NULL)

UPDATE #PRODUCT
SET stock_mo_supply=stock/([3m]/3),
    po_mo_supply=po/([3m]/3)
FROM #PRODUCT
WHERE [3m] <>0

SELECT * FROM #PRODUCT
WHERE [3m] IS NOT NULL OR
[6m] IS NOT NULL OR
[12m]  IS NOT NULL OR
po  IS NOT NULL OR
so  IS NOT NULL OR
stock  IS NOT NULL OR
stock_cost  IS NOT NULL OR
stock_value  IS NOT NULL OR
stock_mo_supply  IS NOT NULL OR
po_mo_supply  IS NOT NULL 



DROP TABLE #PRODUCT
DROP TABLE #SHIPMENT
DROP TABLE #OPEN_PO
DROP TABLE #OPEN_SO
DROP TABLE #STOCK_STATUS1
DROP TABLE #STOCK_STATUS2

END


GO
