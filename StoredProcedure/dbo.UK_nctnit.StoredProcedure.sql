USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_nctnit]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mrinal>  
-- Create date: <Create Date,sep 21, 2015,>  
-- Description: <Description,,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_nctnit]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_nctnit_rec', 'U') IS NOT NULL  
  drop table dbo.UK_nctnit_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_nctnit_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[nctnit_rec];   
    
END  
-- select * from UK_nctnit_rec
GO
