USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_inrmil]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Jun 16, 2017  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_inrmil]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.UK_inrmil_rec', 'U') IS NOT NULL  
  drop table dbo.UK_inrmil_rec;  
      
          
SELECT *  
into  dbo.UK_inrmil_rec  
FROM [LIVEUKSTX].[liveukstxdb].[informix].[inrmil_rec];  
  
END  
-- select * from UK_inrmil_rec
GO
