USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CostChargeAnalysisByCustomer]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Mukesh >                  
-- Create date: <16 Nov 2016>                  
-- Description: <Getting top 50 customers for SSRS reports>            
              
-- =============================================                  
CREATE PROCEDURE [dbo].[sp_itech_CostChargeAnalysisByCustomer] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID  varchar(50),            
@InvoiceNo varchar(20)               
AS                  
BEGIN        
    
                   
 SET NOCOUNT ON;   
   
  
declare @sqltxt varchar(max)                  
declare @execSQLtxt varchar(max)                  
declare @DB varchar(100)                
declare @FD varchar(10)                  
declare @TD varchar(10)                  
declare @NOOfCust varchar(15)                  
DECLARE @CurrenyRate varchar(15)                  
DECLARE @IsExcInterco char(1)        
                  
SET @DB=@DBNAME;                  
                  
CREATE TABLE #tmp (  Databases varchar(15),    
CustID varchar(15)                   
        ,CustName Varchar(65)                  
        ,InvDt varchar(15)                  
        ,InvNo Varchar(50)        
        ,InvItem Varchar(10)      
        ,Form  varchar(35)                  
        ,Grade  varchar(35)                  
        ,Size  varchar(35)                  
        ,Finish  varchar(35)     
        ,CstNo   varchar(10)                    
        ,CstDesc Varchar(25)                    
        , Cost    DECIMAL(20, 4)                    
        , CostUM  Varchar(3)                    
        , BasCryVal   Decimal(20,2)     
        ,Branch Varchar(3)             
                 );                   
               
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @Prefix VARCHAR(5);                  
DECLARE @Name VARCHAR(15);                  
                  
if @CustomerID = 'ALL'                  
 BEGIN                  
  set @CustomerID = '0'                  
 END      
     
 if @InvoiceNo = 'ALL' OR @InvoiceNo = ''                
 BEGIN                  
  set @InvoiceNo = '0'                  
 END                  
                  
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                  
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                  
    
IF @DBNAME = 'ALL'                  
 BEGIN                  
  DECLARE ScopeCursor CURSOR FOR                  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                  
    OPEN ScopeCursor;                  
     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
     WHILE @@FETCH_STATUS = 0                  
       BEGIN                  
        DECLARE @query NVARCHAR(max);                   
                          
        IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))           
    End
	Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))           
    End
                 
      SET @query = 'INSERT INTO #tmp ( Databases,CustID,CustName ,InvDt,InvNo,InvItem ,Form ,Grade,Size  ,Finish,CstNo,CstDesc ,Cost,CostUM,BasCryVal,Branch )       
      SELECT  ''' + @Prefix + ''' , stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt,     
       stn_upd_ref,stn_upd_ref_itm,stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,chl_chrg_no, chl_chrg_desc20, chl_chrg* '+ @CurrenyRate +', chl_chrg_um, chl_chrg_val * '+ @CurrenyRate +'                 
       ,stn_shpt_brh    
       FROM  ' + @Prefix + '_sahstn_rec       
       join ' + @Prefix + '_ortchl_rec on  chl_cmpy_id = stn_cmpy_id and  chl_ref_pfx = stn_shpt_pfx and chl_ref_no = stn_shpt_no and chl_ref_itm = stn_shpt_itm        
       join ' + @Prefix + '_arrcus_rec  on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id              
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and (stn_upd_ref = ''' + @InvoiceNo + ''' OR ''0'' = ''' + @InvoiceNo + ''')    
       and  (stn_sld_cus_id = ''' + @CustomerID + ''' OR ''0'' = ''' + @CustomerID + ''') and  chl_chrg_cl = ''E''     
       '                   
                  
                  print(@query);                  
        EXECUTE sp_executesql @query;                  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                  
       END                   
    CLOSE ScopeCursor;                  
    DEALLOCATE ScopeCursor;                  
  END                  
  ELSE                  
     BEGIN                   
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')                
                        
     IF (UPPER(@DB) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DB) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End
	Else if(UPPER(@DB) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End 
                    
               
     SET @sqltxt ='INSERT INTO #tmp ( Databases,CustID,CustName ,InvDt,InvNo,InvItem ,Form ,Grade,Size  ,Finish,CstNo,CstDesc ,Cost,CostUM,BasCryVal,Branch  )                  
       SELECT  ''' + @DB + ''' , stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt,     
       stn_upd_ref,stn_upd_ref_itm,stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,chl_chrg_no, chl_chrg_desc20, chl_chrg* '+ @CurrenyRate +', chl_chrg_um, chl_chrg_val * '+ @CurrenyRate +'    
       ,stn_shpt_brh               
       FROM  ' + @DB + '_sahstn_rec       
       join ' + @DB + '_ortchl_rec on  chl_cmpy_id =stn_cmpy_id and  chl_ref_pfx = stn_shpt_pfx and chl_ref_no = stn_shpt_no and chl_ref_itm = stn_shpt_itm        
       join ' + @DB + '_arrcus_rec  on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id              
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and (stn_upd_ref = ''' + @InvoiceNo + ''' OR ''0'' = ''' + @InvoiceNo + ''')    
       and  (stn_sld_cus_id = ''' + @CustomerID + ''' OR ''0'' = ''' + @CustomerID + ''')  and chl_chrg_cl = ''E''  '                  
             
                         
                     
     print(@sqltxt);                   
    set @execSQLtxt = @sqltxt;                   
   EXEC (@execSQLtxt);                  
     END                  
          
     
          
  SELECT * FROM #tmp   order by CustID;           
      
  DROP TABLE #tmp                  
                  
END                  
                  
 -- exec sp_itech_CostChargeAnalysisByCustomer '2016-09-15', '2016-11-15' ,'ALL','ALL','1151201'           
        
 /*    
 Date: 20171020    
 Mail:invoice charge details
 20210128	Sumit
 Add germany Database
 */ 
GO
