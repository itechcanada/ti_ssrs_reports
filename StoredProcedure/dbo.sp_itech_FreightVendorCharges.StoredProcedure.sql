USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_FreightVendorCharges]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <21 Dec 2016>  
-- Description: <Getting Freight Vendor Charges on Invoice for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_FreightVendorCharges] @DBNAME varchar(50), @VendorID varchar(10),@UpdateDateFrom datetime,@UpdateDateTo datetime  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(10)  
DECLARE @CurrenyRate varchar(15)  
declare @FD varchar(10)  
declare @TD varchar(10)  
  
set @DB= @DBNAME   
set @FD = CONVERT(VARCHAR(10), @UpdateDateFrom , 120);  
Set @TD = CONVERT(VARCHAR(10), @UpdateDateTo , 120);  
  
CREATE TABLE #tmp (  DBName varchar(2)  
         ,jvs_upd_ref_no varchar(10)  
        ,jvs_upd_dt   varchar(10)  
        ,tph_transp_pfx varchar(3)  
        ,tph_transp_no   VARCHAR(10)  
        ,tph_frt_ven_id   VARCHAR(10)  
        ,jch_chrg_no  varchar(10)  
        ,jch_chrg_val  decimal(20,2)  
        ,jcl_chrg_no  varchar(10)  
        ,jcl_chrg_val  decimal(20,2)  
          
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
     
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
      SET @DB= @Prefix ;  
        
       IF (UPPER(@Prefix) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@Prefix) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))   
    End          
    Else if (UPPER(@Prefix) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@Prefix) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@Prefix) = 'US')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@Prefix) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End
	Else if(UPPER(@Prefix) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End
        
      SET @query =' INSERT INTO #tmp ( DBName,jvs_upd_ref_no, jvs_upd_dt, tph_transp_pfx, tph_transp_no, tph_frt_ven_id, jch_chrg_no, jch_chrg_val, jcl_chrg_no, jcl_chrg_val)  
       Select ''' + @DB + ''', jvs_upd_ref_no, jvs_upd_dt, tph_transp_pfx, tph_transp_no, tph_frt_ven_id, jch_chrg_no,  
        jch_chrg_val *  '+ @CurrenyRate +', jcl_chrg_no, jcl_chrg_val *  '+ @CurrenyRate +'  
      from US_ivjjvs_rec   
      left join '+ @DB +'_trjtph_rec on tph_cmpy_id = jvs_cmpy_id and tph_transp_pfx = jvs_transp_pfx and tph_transp_no = jvs_transp_no  
      left join '+ @DB +'_ivjjch_rec on jch_cmpy_id = jvs_cmpy_id  and jch_upd_ref_no = jvs_upd_ref_no and jch_chrg_no = 3  
      left join '+ @DB +'_ivjjcl_rec on jcl_cmpy_id = jvs_cmpy_id and  jcl_ref_pfx = jvs_ref_pfx and jcl_ref_no = jvs_ref_no and jcl_upd_dt = jvs_upd_dt and   
       jcl_chrg_no = 3  
       Where (tph_frt_ven_id = ''' + @VendorID + ''' OR ''' + @VendorID +''' = '''')   
       and CONVERT(VARCHAR(10), jvs_upd_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120) '  
  
    print(@query);   
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
  BEgin  
    
    IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SElECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End
	Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End
      
    
      
     SET @sqltxt =' INSERT INTO #tmp ( DBName,jvs_upd_ref_no, jvs_upd_dt, tph_transp_pfx, tph_transp_no, tph_frt_ven_id, jch_chrg_no, jch_chrg_val, jcl_chrg_no, jcl_chrg_val)  
       Select ''' + @DB + ''', jvs_upd_ref_no, jvs_upd_dt, tph_transp_pfx, tph_transp_no, tph_frt_ven_id, jch_chrg_no,  
        jch_chrg_val *  '+ @CurrenyRate +', jcl_chrg_no, jcl_chrg_val *  '+ @CurrenyRate +'  
      from US_ivjjvs_rec   
      left join '+ @DB +'_trjtph_rec on tph_cmpy_id = jvs_cmpy_id and tph_transp_pfx = jvs_transp_pfx and tph_transp_no = jvs_transp_no  
      left join '+ @DB +'_ivjjch_rec on jch_cmpy_id = jvs_cmpy_id  and jch_upd_ref_no = jvs_upd_ref_no and jch_chrg_no = 3  
      left join '+ @DB +'_ivjjcl_rec on jcl_cmpy_id = jvs_cmpy_id and  jcl_ref_pfx = jvs_ref_pfx and jcl_ref_no = jvs_ref_no and jcl_upd_dt = jvs_upd_dt and   
       jcl_chrg_no = 3  
       Where (tph_frt_ven_id = ''' + @VendorID + ''' OR ''' + @VendorID +''' = '''')   
       and CONVERT(VARCHAR(10), jvs_upd_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120) '  
       
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
     EXEC (@execSQLtxt);  
     END  
     
  SElect * from #tmp;  
   drop table #tmp ;  
END  

--exec sp_itech_FreightVendorCharges 'ALL',''  
/*
20210128	Sumit
Add germany Database
*/
GO
