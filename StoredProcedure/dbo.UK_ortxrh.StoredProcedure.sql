USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ortxrh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================        
-- Author:  <Author,Mukesh>        
-- Create date: <Create Date,2020-02-03,>        
-- Description: <Description,Open Orders,>        
        
-- =============================================        
CREATE PROCEDURE [dbo].[UK_ortxrh]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
IF OBJECT_ID('dbo.UK_ortxrh_rec', 'U') IS NOT NULL                  
  drop table dbo.UK_ortxrh_rec;            
        
    -- Insert statements for procedure here   
    select [xrh_cmpy_id]  
,[xrh_ord_pfx]  
,[xrh_ord_no]  
,[xrh_ord_itm]  
,[xrh_ord_rls_no]  
--,[xrh_ord_brh]  
--,[xrh_sld_cus_id]  
--,[xrh_cus_po]  
--,[xrh_frm]  
--,[xrh_grd]  
--,[xrh_num_size1]  
--,[xrh_num_size2]  
--,[xrh_num_size3]  
--,[xrh_num_size4]  
--,[xrh_num_size5]  
--,[xrh_size]  
--,[xrh_fnsh]  
--,[xrh_ef_evar]  
--,[xrh_idia]  
--,[xrh_odia]  
--,[xrh_ga_size]  
--,[xrh_wdth]  
--,[xrh_lgth]  
--,[xrh_part_cus_id]  
--,[xrh_part]  
--,[xrh_part_revno]  
,[xrh_sld_to_long_nm]  
,[xrh_shp_to_long_nm]  
--,[xrh_prd_desc50a]  
--,[xrh_prd_desc50b]  
--,[xrh_prd_desc50c]  
--,[xrh_wgt_um]   
--,[xrh_lgth_disp_fmt]   
--,[xrh_rls_pcs]   
--,[xrh_rls_pcs_typ]   
--,[xrh_rls_msr]   
--,[xrh_rls_msr_typ]   
--,[xrh_rls_wgt]   
--,[xrh_rls_wgt_typ]   
--,[xrh_upd_dtts]   
--,[xrh_upd_dtms]   
--,[xrh_cls_dt]   
--,[xrh_is_slp]   
--,[xrh_os_slp]   
--,[xrh_tkn_slp]  
--,[xrh_job_id]   
--,[xrh_grp_ord_itm]   
--,[xrh_sts_actn]   
  
into dbo.UK_ortxrh_rec    
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ortxrh_rec]      
        
END        
  
--select * from UK_ortxrh_rec
GO
