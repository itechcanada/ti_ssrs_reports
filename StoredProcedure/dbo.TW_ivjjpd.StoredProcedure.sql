USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ivjjpd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
      
-- =============================================      
-- Author:  <Author,Mukesh>      
-- Create date: <Create Date,June 28, 2015,>      
-- Description: <Description,Open Orders,>      
      
-- =============================================      
CREATE PROCEDURE [dbo].[TW_ivjjpd]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
     IF OBJECT_ID('dbo.TW_ivjjpd_rec', 'U') IS NOT NULL      
  drop table dbo.TW_ivjjpd_rec ;       
      
       
    -- Insert statements for procedure here      
--SELECT *      
--into  dbo.TW_ivjjpd_rec      
--  from [LIVETWSTX].[livetwstxdb].[informix].[ivjjpd_rec] ;       
      
      
      
  SELECT     
jpd_cmpy_id,jpd_ref_pfx,jpd_ref_no,jpd_ref_itm,jpd_upd_dt,jpd_frm,jpd_grd,jpd_num_size1,jpd_num_size2,jpd_num_size3,jpd_num_size4,jpd_num_size5,jpd_size,    
jpd_fnsh,jpd_ef_evar,jpd_wdth,jpd_lgth,jpd_dim_dsgn,jpd_octg_ent_md,jpd_idia,jpd_odia,jpd_ga_size,jpd_ga_typ,jpd_alt_size,jpd_rdm_dim_1,    
jpd_rdm_dim_2,jpd_rdm_dim_3,jpd_rdm_dim_4,jpd_rdm_dim_5,jpd_rdm_dim_6,jpd_rdm_dim_7,jpd_rdm_dim_8,jpd_rdm_area,jpd_cus_ven_id,jpd_ent_msr,jpd_lgth_disp_fmt,jpd_frc_fmt,    
jpd_dia_frc_fmt,jpd_ord_lgth_typ,jpd_fmt_desc_frc,jpd_fmt_size_desc,jpd_part_cus_id,'-' as jpd_part,jpd_part_revno,jpd_part_acs,jpd_part_ctl_no,jpd_grs_wdth,jpd_grs_lgth,jpd_grs_pcs,    
jpd_grs_msr,jpd_grs_wgt,jpd_grs_qty,jpd_grs_wdth_intgr,jpd_grs_wdth_nmr,jpd_grs_wdth_dnm,jpd_grs_lgth_intgr,jpd_grs_lgth_nmr,jpd_grs_lgth_dnm,jpd_wdth_intgr,jpd_wdth_nmr,    
jpd_wdth_dnm,jpd_lgth_intgr,jpd_lgth_nmr,jpd_lgth_dnm,jpd_idia_intgr,jpd_idia_nmr,jpd_idia_dnm,jpd_odia_intgr,jpd_odia_nmr,jpd_odia_dnm    
into  dbo.TW_ivjjpd_rec      
  from [LIVETWSTX].[livetwstxdb].[informix].[ivjjpd_rec] ;      
      
        
END      
-- select * from TW_ivjjpd_rec 
GO
