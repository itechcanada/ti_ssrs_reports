USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_scrdtr]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Jan 13, 2020    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[CN_scrdtr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.CN_scrdtr_rec', 'U') IS NOT NULL    
  drop table dbo.CN_scrdtr_rec;    
        
            
SELECT *    
into  dbo.CN_scrdtr_rec    
FROM [LIVECNSTX].[livecnstxdb].[informix].[scrdtr_rec];    
    
END    
--- exec CN_scrpyt    
-- select * from CN_scrdtr_rec
GO
