USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReport_DetailNew]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <12 Jun 2014>
-- Description:	<Monthly inventory Report >
-- =============================================

CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReport_DetailNew] @DBNAME varchar(50)
As 
BEGIN

CREATE TABLE #PRODUCT
(
country varchar(10),
form varchar(25),
size varchar(25),
product varchar(50),
frmgrd varchar(25),
[3m_lbs] float,
[6m_lbs] float,
[12m_lbs] float,
po_wgt float,
so_wgt float,
stock_lbs float,
stock_cost float,
stock_value float,
stock_mo_supply float,
po_mo_supply float
)
CREATE TABLE #SHIPMENT
(
country varchar(10),
product varchar(50),
[3m_lbs] float,
[6m_lbs] float,
[12m_lbs] float
)
/*Open PO*/
CREATE TABLE #OPEN_PO
(
country varchar(10),
product varchar(50),
po_wgt float
)
CREATE TABLE #OPEN_SO
(
country varchar(10),
product varchar(50),
so_wgt float
)
CREATE TABLE #STOCK_STATUS1
(
country varchar(10),
product varchar(50),
stock_lbs float,
stock_cost float,
stock_value float
)
CREATE TABLE #STOCK_STATUS2
(
country varchar(10),
product varchar(50),
stock_lbs float,
stock_cost float,
stock_value float
)
declare @DB varchar(50)
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);
DECLARE @ENDDATE varchar(10);
--set @ENDDATE = '2014-04-30';

DECLARE @CurrenyRate varchar(15);
IF (UPPER(@DBNAME) = 'TW')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
			End
			Else if (UPPER(@DBNAME) = 'NO')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
			End
			Else if (UPPER(@DBNAME) = 'CA')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
			End
			Else if (UPPER(@DBNAME) = 'CN')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
			End
			Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')
			begin
				SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
			End
			Else if(UPPER(@DBNAME) = 'UK')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
			End
			Else if(UPPER(@DBNAME) = 'DE')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
			End

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB= @Prefix 
  				IF (UPPER(@Prefix) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@Prefix) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if (UPPER(@Prefix) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@Prefix) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
				End
				Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')
				begin
					SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@Prefix) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@Prefix) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
				
  				SET @query ='INSERT INTO #PRODUCT
				SELECT ''' +  @DB + ''' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM [' + @DB + '_inrprm_rec]
				WHERE prm_frm<>''XXXX'''
				print @query;
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			SET @query ='INSERT INTO #SHIPMENT
					SELECT ''' +  @DB + ''' as country, sat_frm+sat_grd+sat_size+sat_fnsh AS product,
					''3m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
					''6m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
					''12m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' AND 
					sat_sls_cat<> ''CO'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND sat_frm+sat_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					SET @query = @query + ' AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				
				IF @DB = 'US'
				BEGIN
				/*Fittings and Pipe*/
				SET @query ='INSERT INTO #SHIPMENT
					SELECT ''' +  @DB + ''', sat_frm+sat_grd+sat_size+sat_fnsh AS product,
					''3m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_pcs END),
					''6m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_pcs END),
					''12m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_pcs END)
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' and
					sat_sls_cat<> ''CO'' AND
					sat_frm+sat_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
					AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
					UNION
					SELECT ''' +  @DB + ''', sat_frm+sat_grd+sat_size+sat_fnsh AS product,
					''3m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_msr END)/12,0),
					''6m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_msr END)/12,0),
					''12m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_msr END)/12,0)
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' AND
					sat_sls_cat<> ''CO'' AND
					sat_frm+sat_grd IN (''TISP  2'',''TIWP  2'')
					AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				END
				
			SET @query ='INSERT INTO #OPEN_PO
					SELECT ''' +  @DB + ''' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
					FROM [' + @DB + '_potpod_rec] a
					INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
					WHERE a.pod_po_pfx=''PO'' AND
					a.pod_bal_wgt>0 AND
					a.POD_TRCOMP_STS=''O'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND b.ipd_frm+b.ipd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					SET @query = @query + ' GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				
			/*Fitting and Pipe*/
			SET @query ='INSERT INTO #OPEN_PO
				SELECT ''' + @DB + ''', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_pcs)
				FROM [' + @DB + '_potpod_rec] a
				INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
				WHERE a.pod_po_pfx=''PO'' AND
				a.pod_bal_wgt>0 AND
				a.POD_TRCOMP_STS=''O'' AND
				b.ipd_frm+b.ipd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
				GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh

				INSERT INTO #OPEN_PO
				SELECT ''' + @DB + ''', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, ROUND(SUM(a.pod_bal_msr)/12,0)
				FROM [' + @DB + '_potpod_rec] a
				INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
				WHERE a.pod_po_pfx=''PO'' AND
				a.pod_bal_wgt>0 AND
				a.POD_TRCOMP_STS=''O'' AND
				b.ipd_frm+b.ipd_grd IN (''TISP  2'',''TIWP  2'')
				GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh'
				print @query;
  	  			EXECUTE sp_executesql @query;

			/*OPEN SO*/
			SET @query = 'INSERT INTO #OPEN_SO
				SELECT ''' + @DB + ''' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
				FROM [' + @DB + '_ortchl_rec] a
				INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
				INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
				WHERE 
				a.chl_chrg_no = 1 AND
				a.chl_chrg_cl=''E'' AND
				b.ord_ord_pfx= ''SO'' AND
				b.ord_sts_actn=''A'''
				IF @DB = 'US'
				BEGIN
				SET @query = @query  + ' AND c.ipd_frm+c.ipd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
				END
				SET @query = @query  + ' GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh'
				print @query;
  	  			EXECUTE sp_executesql @query;
				  	  			
				/*Fitting and Pipe*/
				IF @DB  = 'US'
				BEGIN
				SET @query = 'INSERT INTO #OPEN_SO
					SELECT ''' + @DB + ''', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_pcs) 
					FROM [' + @DB + '_ortchl_rec] a
					INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
					INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
					WHERE 
					a.chl_chrg_no = 1 AND
					a.chl_chrg_cl=''E'' AND
					b.ord_ord_pfx= ''SO'' AND
					b.ord_sts_actn=''A'' AND
					c.ipd_frm+c.ipd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
					GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

					INSERT INTO #OPEN_SO
					SELECT ''' + @DB + ''', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product, ROUND(SUM(b.ord_bal_msr)/12,0)
					FROM [' + @DB + '_ortchl_rec] a
					INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
					INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
					WHERE 
					a.chl_chrg_no = 1 AND
					a.chl_chrg_cl=''E'' AND
					b.ord_ord_pfx= ''SO'' AND
					b.ord_sts_actn=''A'' AND
					c.ipd_frm+c.ipd_grd IN (''TISP  2'',''TIWP  2'')
					GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh'
					print @query;
  	  				EXECUTE sp_executesql @query;
				END

				/*STOCK STATUS*/
				SET @query = 'INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_wgt, NULL, NULL
					FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND b.prd_frm+b.prd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					print @query;
  	  				EXECUTE sp_executesql @query;
  	  				
					/*Fittings and Pipe*/
					SET @query = 'INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_pcs, NULL, NULL
					FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'' AND
					b.prd_frm+b.prd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')

					INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_msr, NULL, NULL
					FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'' AND
					b.prd_frm+b.prd_grd IN (''TISP  2'',''TIWP  2'')'
					print @query;
  	  				EXECUTE sp_executesql @query;

					SET @query = 'UPDATE #STOCK_STATUS1
					SET stock_cost=a.acp_tot_mat_cst *  '+ @CurrenyRate + ' FROM #STOCK_STATUS1 b, [' + @DB + '_intacp_rec] a
					WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
					AND b.country=''' + @DB + ''''
					print @query;
  	  				EXECUTE sp_executesql @query;
				
					IF @DB = 'US'
					BEGIN
					SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
						
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)			
						
						
						
						
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7) NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				
  	  				/*Fitting and Pipe*/
						SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)			
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7)  IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product

						INSERT INTO #STOCK_STATUS2
						SELECT country, product, ROUND(SUM(stock_lbs)/12,0), AVG(stock_cost), 
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7)  IN (''TISP  2'',''TIWP  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				END
  	  				ELSE
  	  				BEGIN
  	  				SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)
						FROM #STOCK_STATUS1
						WHERE country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				END
				
				SET @query = 'UPDATE #PRODUCT
					SET [3m_lbs]=b.[3m_lbs],
						[6m_lbs]=b.[6m_lbs],
						[12m_lbs]=b.[12m_lbs]
					FROM #PRODUCT a, #SHIPMENT b
					WHERE a.country=b.country AND a.product=b.product
					AND (b.[3m_lbs] IS NOT NULL OR b.[6m_lbs] IS NOT NULL OR b.[12m_lbs] IS NOT NULL)

					UPDATE #PRODUCT
					SET po_wgt=b.po_wgt
					FROM #PRODUCT a ,#OPEN_PO b
					WHERE a.country=b.country AND a.product=b.product
					AND b.po_wgt IS NOT NULL

					UPDATE #PRODUCT
					SET so_wgt=b.so_wgt
					FROM #PRODUCT a , #OPEN_SO b
					WHERE a.country=b.country AND a.product=b.product
					AND b.so_wgt IS NOT NULL

					UPDATE #PRODUCT
					SET stock_lbs=b.stock_lbs,
						stock_cost=b.stock_cost,
						stock_value=b.stock_value
					FROM #PRODUCT a,#STOCK_STATUS2 b
					WHERE a.country=b.country AND a.product=b.product
					AND (b.stock_lbs IS NOT NULL OR b.stock_cost IS NOT NULL OR b.stock_value IS NOT NULL)

					UPDATE #PRODUCT
					SET stock_mo_supply=stock_lbs/([3m_lbs]/3),
						po_mo_supply=po_wgt/([3m_lbs]/3)
					FROM #PRODUCT
					WHERE [3m_lbs] <>0'
					print @query;
  	  				EXECUTE sp_executesql @query;

  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
END
ELSE
BEGIN
	SET @DB = @DBNAME
	SET @query ='INSERT INTO #PRODUCT
				SELECT ''' +  @DB + ''' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM [' + @DB + '_inrprm_rec]
				WHERE prm_frm<>''XXXX'''
				print @query;
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			SET @query ='INSERT INTO #SHIPMENT
					SELECT ''' +  @DB + ''' as country, sat_frm+sat_grd+sat_size+sat_fnsh AS product,'
					 if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')  
           Begin 
           SET @query = @query + ' 
					''3m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_wgt * 2.20462 END),
					''6m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_wgt * 2.20462 END),
					''12m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_wgt * 2.20462 END)'
           END
           ELSE
           BEGIN
           SET @query = @query + '
					''3m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
					''6m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
					''12m_lbs''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_wgt END)'
           END
					SET @query = @query + '
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' AND 
					sat_sls_cat<> ''CO'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND sat_frm+sat_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					SET @query = @query + ' AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				
				IF @DB = 'US'
				BEGIN
				/*Fittings and Pipe*/
				SET @query ='INSERT INTO #SHIPMENT
					SELECT ''' +  @DB + ''', sat_frm+sat_grd+sat_size+sat_fnsh AS product,
					''3m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_pcs END),
					''6m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_pcs END),
					''12m''=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_pcs END)
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' and
					sat_sls_cat<> ''CO'' AND
					sat_frm+sat_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
					AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
					UNION
					SELECT ''' +  @DB + ''', sat_frm+sat_grd+sat_size+sat_fnsh AS product,
					''3m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 2 THEN sat_blg_msr END)/12,0),
					''6m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 5 THEN sat_blg_msr END)/12,0),
					''12m''=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11 THEN sat_blg_msr END)/12,0)
					FROM [' + @DB + '_sahsat_rec] 
					WHERE 
					sat_inv_pfx=''SE'' AND
					sat_frm <> ''XXXX'' AND
					sat_sls_cat<> ''CO'' AND
					sat_frm+sat_grd IN (''TISP  2'',''TIWP  2'')
					AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), getdate()) BETWEEN 0 AND 11
					GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				END
				
			SET @query ='INSERT INTO #OPEN_PO
					SELECT ''' +  @DB + ''' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product,'
					if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')  
           Begin 
           SET @query = @query + ' SUM(a.pod_bal_wgt * 2.20462) as po_wgt '
            END
            ELSE
            BEGIN
            SET @query = @query + ' SUM(a.pod_bal_wgt) as po_wgt '
            END
			SET @query = @query + '	FROM [' + @DB + '_potpod_rec] a
					INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
					WHERE a.pod_po_pfx=''PO'' AND
					a.pod_bal_wgt>0 AND
					a.POD_TRCOMP_STS=''O'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND b.ipd_frm+b.ipd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					SET @query = @query + ' GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh'
					print @query;
  	  			EXECUTE sp_executesql @query;
				
			/*Fitting and Pipe*/
			SET @query ='INSERT INTO #OPEN_PO
				SELECT ''' + @DB + ''', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_pcs)
				FROM [' + @DB + '_potpod_rec] a
				INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
				WHERE a.pod_po_pfx=''PO'' AND
				a.pod_bal_wgt>0 AND
				a.POD_TRCOMP_STS=''O'' AND
				b.ipd_frm+b.ipd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
				GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh

				INSERT INTO #OPEN_PO
				SELECT ''' + @DB + ''', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, ROUND(SUM(a.pod_bal_msr)/12,0)
				FROM [' + @DB + '_potpod_rec] a
				INNER JOIN [' + @DB + '_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
				WHERE a.pod_po_pfx=''PO'' AND
				a.pod_bal_wgt>0 AND
				a.POD_TRCOMP_STS=''O'' AND
				b.ipd_frm+b.ipd_grd IN (''TISP  2'',''TIWP  2'')
				GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh'
				print @query;
  	  			EXECUTE sp_executesql @query;

			/*OPEN SO*/
			SET @query = 'INSERT INTO #OPEN_SO
				SELECT ''' + @DB + ''' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product, '
				if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')  
           Begin 
           SET @query = @query + ' SUM(b.ord_bal_wgt * 2.20462) as so_wgt '
           END
           ELSE
           BEGIN
           SET @query = @query + ' SUM(b.ord_bal_wgt) as so_wgt '
           END
		SET @query = @query + '	FROM [' + @DB + '_ortchl_rec] a
				INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
				INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
				WHERE 
				a.chl_chrg_no = 1 AND
				a.chl_chrg_cl=''E'' AND
				b.ord_ord_pfx= ''SO'' AND
				b.ord_sts_actn=''A'''
				IF @DB = 'US'
				BEGIN
				SET @query = @query  + ' AND c.ipd_frm+c.ipd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
				END
				SET @query = @query  + ' GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh'
				print @query;
  	  			EXECUTE sp_executesql @query;
				  	  			
				/*Fitting and Pipe*/
				IF @DB  = 'US'
				BEGIN
				SET @query = 'INSERT INTO #OPEN_SO
					SELECT ''' + @DB + ''', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_pcs) 
					FROM [' + @DB + '_ortchl_rec] a
					INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
					INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
					WHERE 
					a.chl_chrg_no = 1 AND
					a.chl_chrg_cl=''E'' AND
					b.ord_ord_pfx= ''SO'' AND
					b.ord_sts_actn=''A'' AND
					c.ipd_frm+c.ipd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
					GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

					INSERT INTO #OPEN_SO
					SELECT ''' + @DB + ''', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product, ROUND(SUM(b.ord_bal_msr)/12,0)
					FROM [' + @DB + '_ortchl_rec] a
					INNER JOIN [' + @DB + '_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
					INNER JOIN [' + @DB + '_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
					WHERE 
					a.chl_chrg_no = 1 AND
					a.chl_chrg_cl=''E'' AND
					b.ord_ord_pfx= ''SO'' AND
					b.ord_sts_actn=''A'' AND
					c.ipd_frm+c.ipd_grd IN (''TISP  2'',''TIWP  2'')
					GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh'
					print @query;
  	  				EXECUTE sp_executesql @query;
				END

				/*STOCK STATUS*/
				SET @query = 'INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, '
					if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')  
           Begin 
           SET @query = @query + ' b.prd_ohd_wgt * 2.20462, NULL, NULL '
           END
           ELSE
           BEGIN
           SET @query = @query + ' b.prd_ohd_wgt, NULL, NULL '
           END
				SET @query = @query + '	FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'''
					IF @DB = 'US'
					BEGIN
					SET @query = @query + ' AND b.prd_frm+b.prd_grd NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')'
					END
					print @query;
  	  				EXECUTE sp_executesql @query;
  	  				
					/*Fittings and Pipe*/
					SET @query = 'INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_pcs, NULL, NULL
					FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'' AND
					b.prd_frm+b.prd_grd IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')

					INSERT INTO #STOCK_STATUS1
					SELECT ''' + @DB + ''' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_msr, NULL, NULL
					FROM [' + @DB + '_intprd_rec] b                                                                    
					WHERE
					b.prd_invt_sts = ''S'' AND
					b.prd_frm+b.prd_grd IN (''TISP  2'',''TIWP  2'')'
					print @query;
  	  				EXECUTE sp_executesql @query;

					SET @query = ' UPDATE #STOCK_STATUS1
					SET stock_cost=a.acp_tot_mat_cst * '+ @CurrenyRate +' 	FROM #STOCK_STATUS1 b, [' + @DB + '_intacp_rec] a
					WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
					AND b.country=''' + @DB + ''''
					print @query;
  	  				EXECUTE sp_executesql @query;
				
					IF @DB = 'US'
					BEGIN
					SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)			
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7) NOT IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'',''TISP  2'',''TIWP  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				
  	  				SET @query = 'INSERT INTO #STOCK_STATUS2
								SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
								( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)			
								FROM #STOCK_STATUS1
								WHERE
								country<>''US''
								GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				
  	  				
  	  				
  	  				/*Fitting and Pipe*/
						SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, SUM(stock_lbs), AVG(stock_cost), 
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7)  IN (''TIER  2'',''TICR  2'',''TIEL  2'',''TIFL  2'',''TISE  2'',''TITE  2'',''TIFT  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product

						INSERT INTO #STOCK_STATUS2
						SELECT country, product, ROUND(SUM(stock_lbs)/12,0),  AVG(stock_cost),
						( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)
						FROM #STOCK_STATUS1
						WHERE LEFT(product,7)  IN (''TISP  2'',''TIWP  2'')
						AND country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				END
  	  				ELSE
  	  				BEGIN
  	  				SET @query = 'INSERT INTO #STOCK_STATUS2
						SELECT country, product, '
						if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')  
           Begin 
           SET @query = @query + ' SUM(stock_lbs * 2.20462), AVG(stock_cost),                                 
          ( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh)			
            '
           
           END
           ELSE
           BEGIN
           SET @query = @query + ' SUM(stock_lbs), AVG(stock_cost), 
           ( select isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))  from
            ' + @DB + '_intprd_rec left join ' + @DB +'_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool  
       where prd_invt_sts = ''S''   and (acp_tot_qty <> 0 or acp_tot_qty is null) AND product = prD_frm+prd_grd+prD_size+prd_fnsh) '
           END
			SET @query = @query + ' FROM #STOCK_STATUS1
        
						Where  country=''' + @DB + '''
						GROUP BY country, product'
						print @query;
  	  				EXECUTE sp_executesql @query;
  	  				END
				
				SET @query = 'UPDATE #PRODUCT
					SET [3m_lbs]=b.[3m_lbs],
						[6m_lbs]=b.[6m_lbs],
						[12m_lbs]=b.[12m_lbs]
					FROM #PRODUCT a, #SHIPMENT b
					WHERE a.country=b.country AND a.product=b.product
					AND (b.[3m_lbs] IS NOT NULL OR b.[6m_lbs] IS NOT NULL OR b.[12m_lbs] IS NOT NULL)

					UPDATE #PRODUCT
					SET po_wgt=b.po_wgt
					FROM #PRODUCT a ,#OPEN_PO b
					WHERE a.country=b.country AND a.product=b.product
					AND b.po_wgt IS NOT NULL

					UPDATE #PRODUCT
					SET so_wgt=b.so_wgt
					FROM #PRODUCT a , #OPEN_SO b
					WHERE a.country=b.country AND a.product=b.product
					AND b.so_wgt IS NOT NULL

					UPDATE #PRODUCT
					SET stock_lbs=b.stock_lbs,
						stock_cost=b.stock_cost,
						stock_value=b.stock_value
					FROM #PRODUCT a,#STOCK_STATUS2 b
					WHERE a.country=b.country AND a.product=b.product
					AND (b.stock_lbs IS NOT NULL OR b.stock_cost IS NOT NULL OR b.stock_value IS NOT NULL)

					UPDATE #PRODUCT
					SET stock_mo_supply=stock_lbs/([3m_lbs]/3),
						po_mo_supply=po_wgt/([3m_lbs]/3)
					FROM #PRODUCT
					WHERE [3m_lbs] <>0'
					print @query;
  	  				EXECUTE sp_executesql @query;
END

DELETE FROM HPM_DETAIL
INSERT INTO HPM_DETAIL
SELECT country, product, 
CASE WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<250 THEN ''
                WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<750  THEN 'C' 
                WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<1250 THEN 'B' 
                WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]>1249 THEN 'A'
                WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<1000 THEN ''
                WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<3000 THEN 'C' 
                WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<5000 THEN 'B' 
                WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]>4999 THEN 'A' 
                WHEN form='TIRD' AND [6m_lbs]<1500 THEN ' '
                WHEN form='TIRD' AND [6m_lbs]<3000 THEN 'C' 
                WHEN form='TIRD' AND [6m_lbs]<5000 THEN 'B' 
                WHEN form='TIRD' AND [6m_lbs]>4999 THEN 'A'    
                WHEN form='TIPL' AND [6m_lbs]<1000 THEN ' '
                WHEN form='TIPL' AND [6m_lbs]<2000 THEN 'C' 
                WHEN form='TIPL' AND [6m_lbs]<4000 THEN 'B' 
                WHEN form='TIPL' AND [6m_lbs]>3999 THEN 'A' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<250 THEN ''
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<750  THEN 'C' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<1250 THEN 'B' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]>1249 THEN 'A'
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<1000 THEN ''
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<3000 THEN 'C' 
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<5000 THEN 'B' 
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]>4999 THEN 'A'
     WHEN form='NIRD' AND [6m_lbs]<1500 THEN ''
     WHEN form='NIRD' AND [6m_lbs]<3000 THEN 'C' 
     WHEN form='NIRD' AND [6m_lbs]<5000 THEN 'B' 
     WHEN form='NIRD' AND [6m_lbs]>4999 THEN 'A'
                WHEN form='TISH' AND [6m_lbs]<499 THEN ''
                WHEN form='TISH' AND [6m_lbs]<1499  THEN 'C' 
                WHEN form='TISH' AND [6m_lbs]<2000 THEN 'B' 
                WHEN form='TISH' AND [6m_lbs]>1999 THEN 'A' END
,stock_lbs,stock_value,stock_cost,[3m_lbs],[6m_lbs],[12m_lbs],stock_mo_supply,so_wgt,po_wgt,po_mo_supply
FROM #PRODUCT

--For Testing purpose
--SELECT * FROM #PRODUCT
--WHERE [3m_lbs] IS NOT NULL OR
--[6m_lbs] IS NOT NULL OR
--[12m_lbs] IS NOT NULL OR
--po_wgt IS NOT NULL OR
--so_wgt IS NOT NULL OR
--stock_lbs IS NOT NULL OR
--stock_cost IS NOT NULL OR
--stock_value IS NOT NULL OR
--stock_mo_supply IS NOT NULL OR
--po_mo_supply IS NOT NULL

SELECT country, CASE WHEN SUBSTRING(form,1,1) = 'T' OR SUBSTRING(form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup, 
product as frmgrd, 
sum(stock_lbs) as stock_lbs, sum(Stock_Value)as Stock_Value,Sum(Stock_Cost) as Stock_Cost,Sum([3m_lbs]) as [3m_lbs],sum([6m_lbs]) as [6m_lbs],
sum([12m_lbs]) as [12m_lbs],sum(Stock_Mo_Supply) as Stock_Mo_Supply,sum(SO_Wgt) as SO_Wgt,sum(PO_Wgt) as PO_Wgt,sum(PO_Mo_Supply) as PO_Mo_Supply FROM #PRODUCT
WHERE [3m_lbs] IS NOT NULL OR
[6m_lbs] IS NOT NULL OR
[12m_lbs]  IS NOT NULL OR
po_wgt  IS NOT NULL OR
so_wgt  IS NOT NULL OR
stock_lbs  IS NOT NULL OR
stock_cost  IS NOT NULL OR
stock_value  IS NOT NULL OR
stock_mo_supply  IS NOT NULL OR
po_mo_supply  IS NOT NULL 
group by country,form,
product
order by  
form 


DROP TABLE #PRODUCT
DROP TABLE #SHIPMENT
DROP TABLE #OPEN_PO
DROP TABLE #OPEN_SO
DROP TABLE #STOCK_STATUS1
DROP TABLE #STOCK_STATUS2
END

-- Exec [sp_itech_MonthlyInventoryReport_DetailNew] 'US'
GO
