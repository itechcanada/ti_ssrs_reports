USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_tctipd]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                                                                
-- Author:  <Author,Clayton Daigle>                                                                                
-- Create date: <Create Date,10/5/2012,>                                                                                
-- Modified date: <Modified Date,10/8/2017,>                                                                              
-- Modified by: <Modified by, Mukesh,>                                                                              
-- Description: <Description,Open Orders,>                                                                                
                                                                                
-- =============================================                                                                                
CREATE PROCEDURE [dbo].[US_tctipd]                                                                                
                                                                                 
AS                                                                                
BEGIN                                                                                
 -- SET NOCOUNT ON added to prevent extra result sets from                                                                                
 -- interfering with SELECT statements.                                                                           
 -- prob col ipd_part                                                                           
 SET NOCOUNT ON;                                                  
                                   
 IF OBJECT_ID('dbo.US_tctipd_rec', 'U') IS NOT NULL                                    
  drop table dbo.US_tctipd_rec;                                    
                                                                  
                                                                                
    -- Insert statements for procedure here                                                                                
--SELECT *                                                                                
--FROM [LIVEUSSTX].[liveusstxdb].[informix].[tctipd_rec]                                                                                
                                                                                              
                                                                              
SELECT   *   into dbo.US_tctipd_rec                                                                            
FROM [LIVEUSSTX].[liveusstxdb].[informix].[tctipd_rec]                                                                              
WHERE  (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101243' OR ipd_ref_itm != '2')                                                                               
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101930' OR ipd_ref_itm != '1')                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101931' OR ipd_ref_itm != '3')                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101931' OR ipd_ref_itm != '2')                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101931' OR ipd_ref_itm != '1')                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101929' OR ipd_ref_itm != '3')                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101929' OR ipd_ref_itm != '2')                        
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101929' OR ipd_ref_itm != '1')                                                         
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101928' )                                                             
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101927' )                                                         
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101925' )                                             
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101924' )                         
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101922' )                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101921' )                                               
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101922' )                                                                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '101920' )                                                   
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '102067' )                                                                           
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '102066' )                                                                           
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '102065' )                                                                          
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '103417' )                                                          
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '103728' )                                                                
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '104225' )                                                                 
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '110195' )                                                              
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '111457' )                                                                
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '112061' )                                                              
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '114591' )                                                            
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '116177' )                                                         
And (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '116907' )                                                       
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '117557' )                                                     
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '117938' )                                                   
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '142510' )                                                  
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '143107' )                                               
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '143524' )                                              
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '143721' )                                                                                  
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '155879' )                                           
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '186096' )                                        
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '156875' )                                         
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'QT' OR ipd_ref_no != '59900' )                           
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '189677' )                      
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '165597' )                                
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '196186' )                             
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '173581' )                            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '173847' )                    
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '204887' )                        
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '204997' )                      
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '174799' )                    
--20170823                  
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '205991' )                    
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '205988' )                 
-- 20171027                
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '181377' )                  
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'QT' OR ipd_ref_no != '65196' )                 
-- 20171028              
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '213012' )              
--20180115            
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '188713' )             
--20180117          
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '220787' )           
-- 20180201        
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '222592' )        
-- 20180827      
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SO' OR ipd_ref_no != '213003' )     
-- 20180828    
AND (ipd_cmpy_id != 'USS' OR ipd_ref_pfx != 'SI' OR ipd_ref_no != '246620' )    
AND (ipd_cmpy_id != 'USS'  OR ipd_ref_no != '81480' )                 
END                           
                          
/*                          
                          
*/
GO
