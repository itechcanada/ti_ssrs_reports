USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrcva]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================        
-- Author:  <Author,Sumit>        
-- Create date: <Create Date, 01/18/2021,>        
-- Description: <Description,Customer/Vendor Addresses,>        
        
-- =============================================        
CREATE PROCEDURE [dbo].[DE_scrcva]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
Truncate table dbo.DE_scrcva_rec ;         
        
-- Insert statements for procedure here        
insert into dbo.DE_scrcva_rec      
SELECT *        
  from [LIVEDESTX].[livedestxdb].[informix].[scrcva_rec] where cva_cus_ven_id != '135';   
  
insert into dbo.DE_scrcva_rec
SELECT cva_cmpy_id, cva_ref_pfx, cva_cus_ven_typ, cva_cus_ven_id, cva_addr_no, cva_addr_typ, '', cva_nm2, cva_addr1, cva_addr2, cva_addr3, cva_city, cva_pcd, cva_latd, cva_lngtd,
cva_cty, cva_st_prov, cva_transp_zn, cva_tel_area_cd, cva_tel_no, cva_tel_ext, cva_fax_area_cd, cva_fax_no, cva_fax_ext, cva_email, cva_tmzn, cva_upd_dtts, cva_upd_dtms 
from [LIVEDESTX].[livedestxdb].[informix].[scrcva_rec] where cva_cus_ven_id = '135'

          
END   
/*  
20210716 Sumit  
add condition where cva_cus_ven_id != '135'  , add select statement without cva_nm1 and insert
Error: Could not convert the data value due to reasons other than sign mismatch or overflow.   
*/
GO
