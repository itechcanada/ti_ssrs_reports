USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_glhgld]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/6/2012,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[TW_glhgld]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_glhgld_rec', 'U') IS NOT NULL
		drop table dbo.TW_glhgld_rec ;	

	
    -- Insert statements for procedure here
SELECT gld_cr_amt,gld_dr_amt, gld_bsc_gl_acct, gld_actvy_Dt, gld_sacct , gld_acctg_per
into  dbo.TW_glhgld_rec
  from [LIVETWGL].[livetwgldb].[informix].[glhgld_rec] ;
  
END



















GO
