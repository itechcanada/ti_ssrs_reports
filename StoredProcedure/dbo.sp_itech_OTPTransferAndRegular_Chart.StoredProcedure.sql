USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OTPTransferAndRegular_Chart]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_itech_OTPTransferAndRegular_Chart] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50)

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)


CREATE TABLE #tmp (   CompanyName		 VARCHAR(10)
   					, ShpgWhs 			 VARCHAR(65)
   					, Pcs			 DECIMAL(20, 2)
   					, Wgt			 DECIMAL(20, 2)
   					,Late			 integer
   					,TranspNO			 integer 	
   					,OTPType			 VARCHAR(35) 
   					,OrderNo   varchar(15)
   					,ShpDate    Datetime
   							
   	             );
   	             
   	             
   	             CREATE TABLE #tmpTransfer (   CompanyName		 VARCHAR(10)
   					, ShpgWhs 			 VARCHAR(65)
   					, Pcs			 DECIMAL(20, 2)
   					, Wgt			 DECIMAL(20, 2)
   					,Late			 integer
   					,TranspNO			 integer 	
   					,OTPType			 VARCHAR(35)
   					,ShpDate    Datetime 		
   	             );
   	             
   	         
DECLARE @company VARCHAR(35);
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
  	   
 IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName, company,prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				 set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
  				print(@DB)
							
							SET @query = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShpDate)
					      	select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'',
							sum(dpf_shp_pcs) as ''Pcs'',SUM(dpf_shp_wgt) as ''Wgt'',Max(dpf_lte_shpt) as ''Late'',
							dpf_transp_no as ''TranspNO'',
							''Regular'' as OTPType, dpf_ord_no as ''orderNO'',dpf_shpg_dtts as ''ShpDate''
							from '+ @DB +'.[pfhdpf_rec]
							JOIN '+ @DB +'.[ortorl_rec] 
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no
							where  dpf_shpg_dtts >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and dpf_shpg_dtts <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)
							and dpf_sprc_pfx = ''SH''
							group by dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,
							dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),dpf_shpg_dtts  '
							
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			 SET @query = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShpDate)
				SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',sum(b.tud_comp_pcs) as ''Pcs'',sum(b.tud_comp_wgt) as ''Wgt'',
				case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType ,a.tph_sch_dtts as ''ShpDate''
				FROM ['+ @prefix +'_trjtph_rec] a
				left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
				left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
				WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN '''+ @FD +''' AND '''+ @TD +'''
				AND b.tud_prnt_no<>0 and TPH_NBR_STP =1 
				 and b.tud_sprc_pfx<>''SH'' and b.tud_sprc_pfx<>''IT''
				group by tph_CMPY_ID ,b.tud_trpln_whs ,
				a.tph_sch_dtts , a.tph_transp_no ,a.tph_sch_dtts'
  	  			
  	 -- 		   SET @query = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShpDate)
				--SELECT tph_CMPY_ID as ''CompanyName'',c.orl_shpg_whs as ''ShpgWhs'',sum(b.tud_comp_pcs) as ''Pcs'',sum(b.tud_comp_wgt) as ''Wgt'',
				--case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType ,a.tph_sch_dtts as ''ShpDate''
				--FROM ['+ @prefix +'_trjtph_rec] a
				--INNER JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
				--INNER JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
				--WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN '''+ @FD +''' AND '''+ @TD +'''
				--AND b.tud_prnt_no<>0
				-- and b.tud_sprc_pfx<>''SH'' and b.tud_sprc_pfx<>''IT''
				--group by tph_CMPY_ID ,c.orl_shpg_whs ,
				--a.tph_sch_dtts , a.tph_transp_no ,a.tph_sch_dtts '
						
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			print('232')
			  Set @DatabaseName=(select DatabaseName from tbl_itech_DatabaseName where company=''+ @DBNAME + '')
			  set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
			  print(@DB)  
			  
			  Set @prefix=(select prefix from tbl_itech_DatabaseName where company=''+ @DBNAME + '')
					SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShpDate)
					      	select  dpf_cmpy_id as ''CompanyName'',dpf_shpg_whs as ''ShpgWhs'',
							sum(dpf_shp_pcs) as ''Pcs'',SUM(dpf_shp_wgt) as ''Wgt'',Max(dpf_lte_shpt) as ''Late'',
							dpf_transp_no as ''TranspNO'',
							''Regular'' as OTPType, dpf_ord_no as ''orderNO'' ,dpf_shpg_dtts as ''ShpDate''
							from '+ @DB +'.[pfhdpf_rec]
							JOIN '+ @DB +'.[ortorl_rec] 
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no
							where  dpf_shpg_dtts >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and dpf_shpg_dtts <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)
							and dpf_sprc_pfx = ''SH''
							group by dpf_cmpy_id ,dpf_shpg_whs , dpf_sprc_pfx ,
							dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,dpf_transp_no ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),dpf_shpg_dtts '
						
				--print(@sqltxt)
			set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
		
		SET @sqltxt =	'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShpDate)
				SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',sum(b.tud_comp_pcs) as ''Pcs'',sum(b.tud_comp_wgt) as ''Wgt'',
				case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType ,a.tph_sch_dtts as ''ShpDate''
				FROM ['+ @prefix +'_trjtph_rec] a
				left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
				left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
				WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN '''+ @FD +''' AND '''+ @TD +'''
				AND b.tud_prnt_no<>0 and TPH_NBR_STP =1 
				 and b.tud_sprc_pfx<>''SH'' and b.tud_sprc_pfx<>''IT''
				group by tph_CMPY_ID ,b.tud_trpln_whs ,
				a.tph_sch_dtts , a.tph_transp_no ,a.tph_sch_dtts'
			print(@sqltxt)
		set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
     END
   	            
--SELECT COUNT(TranspNO) as 'TranspNO', CompanyName, ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType , right(convert(varchar, ShpDate, 106), 8) as 'ShpDate'
--FROM #tmpTransfer 
--group by CompanyName, ShpgWhs,OTPType,right(convert(varchar, ShpDate, 106), 8)
--union 
--select COUNT(TranspNO) as 'TranspNO', CompanyName, ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType, right(convert(varchar, ShpDate, 106), 8)  as 'ShpDate'
-- from #tmp
-- group by CompanyName, ShpgWhs,OTPType,right(convert(varchar, ShpDate, 106), 8)
-- order by ShpDate
 
 
 SELECT COUNT(TranspNO) as 'TranspNO', CompanyName, SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType , convert(varchar(7),ShpDate, 126) +'-01' as 'ShpDate'  --right(convert(varchar, ShpDate, 106), 8)  as 'ShpDate' -- right(convert(varchar, ShpDate, 106), 8) as 'ShpDate'
FROM #tmpTransfer 
group by CompanyName, OTPType,convert(varchar(7),ShpDate, 126)
union 
select COUNT(TranspNO) as 'TranspNO', CompanyName, SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',OTPType,   convert(varchar(7),ShpDate, 126) +'-01' as 'ShpDate' 
 from #tmp
 group by CompanyName,OTPType,convert(varchar(7),ShpDate, 126)
 order by ShpDate


END

-- exec sp_itech_OTPTransferAndRegular_Chart '03/01/2012', '02/28/2013' , 'USS' 



GO
