USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Transport_History]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Mukesh>
-- Create date: <09 Jan 2014>
-- Description:	<Transport Information>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_Transport_History]  @DBNAME varchar(50), @FromDate datetime, @Todate datetime, @version char = '0'
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @DB varchar(100);
declare @FD varchar(10);
declare @TD varchar(10);
declare @sqltxt varchar(6000);
declare @execSQLtxt varchar(7000);
DECLARE @company VARCHAR(15);   
DECLARE @prefix VARCHAR(15);   
DECLARE @DatabaseName VARCHAR(35);    

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @Todate , 120)

CREATE TABLE #temp ( Dbname		VARCHAR(10) 
					,CmpyID		VARCHAR(3)
					,Whs		VARCHAR(3)
					,Trm_trd	VARCHAR(3)
					,Ven_id		VARCHAR(10)
					,VenLong_nm	VARCHAR(50)
					,countwhs	int
					);

IF @DBNAME = 'ALL'
	BEGIN
		IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
		FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @query NVARCHAR(MAX);
			SET @DB =@prefix
			SET @query = 'INSERT INTO #temp (Dbname, CmpyID, Whs, Trm_trd, Ven_id, VenLong_nm, countwhs)
						Select '''+ @Prefix +''' ,ven_cmpy_id,Replace(tph_trpln_whs,''SFS'',''LAX''),frt_trm_trd,frt_frt_ven_id,ven_ven_long_nm, COUNT(tph_trpln_whs) from
						(SELECT distinct b.ven_cmpy_id,a.tph_trpln_whs, d.frt_trm_trd, d.frt_frt_ven_id,a.tph_transp_no, b.ven_ven_long_nm
						 FROM ' + @DB + '_trjtph_rec a 
						 JOIN ' + @DB + '_aprven_rec b ON a.tph_frt_ven_id=b.ven_ven_id
						 JOIN ' + @DB + '_trjtud_rec c ON a.tph_transp_pfx=c.tud_transp_pfx AND a.tph_transp_no= c.tud_transp_no
						 JOIN ' + @DB + '_frtfrt_rec d ON c.tud_prnt_pfx=d.frt_ref_pfx AND c.tud_prnt_no=d.frt_ref_no
						 WHERE c.tud_prnt_no<>0 AND CAST(a.tph_sch_dtts AS datetime) BETWEEN ''' + @FD + ''' AND ''' + @TD + ''' 
						) as t
						group by  ven_cmpy_id,tph_trpln_whs,frt_frt_ven_id,frt_trm_trd,ven_ven_long_nm '
			print @query;
			EXECUTE sp_executesql @query;
			FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;
		END			
		CLOSE ScopeCursor;
		DEALLOCATE ScopeCursor;
	END
ELSE
BEGIN
		IF @version = '0'
		BEGIN
	  SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')
	  END
	  ELSE
	  BEGIN
	  SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DBNAME +'')
	  END
	  SET @DB = @DBNAME
	  SET @sqltxt ='INSERT INTO #temp (Dbname, CmpyID, Whs, Trm_trd, Ven_id, VenLong_nm, countwhs)
						Select '''+ @DBNAME +''' , ven_cmpy_id,Replace(tph_trpln_whs,''SFS'',''LAX''),frt_trm_trd,frt_frt_ven_id,ven_ven_long_nm, COUNT(tph_trpln_whs) from
						(SELECT distinct b.ven_cmpy_id,a.tph_trpln_whs, d.frt_trm_trd, d.frt_frt_ven_id,a.tph_transp_no, b.ven_ven_long_nm
						 FROM ' + @DB + '_trjtph_rec a 
						 JOIN ' + @DB + '_aprven_rec b ON a.tph_frt_ven_id=b.ven_ven_id
						 JOIN ' + @DB + '_trjtud_rec c ON a.tph_transp_pfx=c.tud_transp_pfx AND a.tph_transp_no= c.tud_transp_no
						 JOIN ' + @DB + '_frtfrt_rec d ON c.tud_prnt_pfx=d.frt_ref_pfx AND c.tud_prnt_no=d.frt_ref_no
						 WHERE c.tud_prnt_no<>0 AND CAST(a.tph_sch_dtts AS datetime) BETWEEN ''' + @FD + ''' AND ''' + @TD + ''' 
						) as t
						group by  ven_cmpy_id,tph_trpln_whs,frt_frt_ven_id,frt_trm_trd,ven_ven_long_nm '
	print(@sqltxt)
	set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
END
	SELECT * FROM #temp ;
	--Where Whs = 'ROC';
	--Where Whs = 'ROC' and Trm_trd = 'CHG';
	DROP TABLE  #temp;
END

--EXEC [sp_itech_Transport_History] 'ALL', '2013-01-01', '2013-12-31'
--EXEC [sp_itech_Transport_History] 'PS', '2013-01-01', '2014-12-31','1'
--select Prefix, * from tbl_itech_DatabaseName where DatabaseName='US'
GO
