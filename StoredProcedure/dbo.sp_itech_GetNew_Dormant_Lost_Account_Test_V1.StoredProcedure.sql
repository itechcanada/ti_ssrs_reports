USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Test_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- sp_helptext sp_itech_GetNew_Dormant_Lost_Account_Test        
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <18 Feb 2016>          
-- Description: <Getting OTP>          
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Test_V1]  @DBNAME varchar(50),@Branch varchar(50),@ISCombineData int,@Account varchar(50),@Month Datetime,@Market varchar(50), @version char = '0'          
        
AS        
BEGIN        
SET NOCOUNT ON;      

        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @NFD varchar(10)        
declare @NTD varchar(10)        
declare @D3MFD varchar(10)        
declare @D3MTD varchar(10)        
declare @D6MFD varchar(10)        
declare @D6MTD varchar(10)        
declare @LFD varchar(10)        
declare @LTD varchar(10)        
declare @FD varchar(10)        
declare @TD varchar(10)        
declare @RFD varchar(10)        
declare @RTD varchar(10)        
declare @AFD varchar(10)        
declare @ATD varchar(10)        
declare @YTDFD varchar(10)        
declare @YTDTD varchar(10)        
declare @YTD14FD varchar(10)        
declare @YTD14TD varchar(10)         
        
set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month) - 12, 0) , 120)   --First day of previous 12 month        
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)+1,0)), 120)  --Last Day of current month        
        
set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-1,0)) , 120)   -- New Account        
set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)  -- New Account        
        
set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-4,0)) , 120)   -- Dormant 3 Month        
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-3,0)), 120)  -- Dormant 3 Month        
        
set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-7,0)) , 120)   --Dormant 6 Month        
set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-6,0)), 120)  -- Dormant 6 Month        
        
set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Lost Account        
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-18,0)), 120)  -- Lost Account        
        
        
set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Reactivated Account        
set @RTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)), 120)  -- Reactivated Account        
        
set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)    -- Active Accounts         
set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)        
        
set @YTDFD = CONVERT(VARCHAR(10),  DATEADD(yy, -1,DateAdd(yy,DATEDIFF(yy,0,getdate()), 0)), 120)        
set @YTD14FD = CONVERT(VARCHAR(10),  DATEADD(yy, DATEDIFF(yy,0,getdate()), 0), 120)        
        
IF YEAR(@NTD) = YEAR(getdate())-1         
 BEGIN        
  SET @YTDTD = @NTD        
  SET @YTD14TD = @NTD        
 END        
ELSE        
 BEGIN        
  SET @YTDTD = CONVERT(VARCHAR(10),  DATEADD(yy, -1,DATEADD(yy, DATEDIFF(yy,0,getdate()) + 1, -1)), 120)       
  SET @YTD14TD = @NTD        
 END        
        
print @NTD        
print @YTDTD   
print @YTD14TD;       
 -- IF @ISCombineData =1        
 --BEGIN        
 --  set @NFD =@FD         
 --  set @NTD = @TD        
 --  set @D3MFD = @FD        
 --  set @D3MTD = @TD        
 --  set @D6MFD = @FD        
 --  set @D6MTD = @TD        
 --  set @LFD = @FD        
 --  set @LTD = @TD        
 --  END        
         
--1 New Accounts        
--2 Dormant Accounts 3M        
--3 Dormant Accounts 6M        
--4 Lost Accounts        
--5 Total Reactivated Accounts        
--6 Current Month Total Active Customers        
--0 ALL        
CREATE TABLE #yearACCount ( SalePersonName VARCHAR (50),        
       Branch Varchar(15),        
       AccountCount int)        
CREATE TABLE #yearACCountFinal (SalePersonName VARCHAR (50),        
         Branch Varchar(15),        
         AccountCount int)        
CREATE TABLE #yearACCount2014 ( SalePersonName VARCHAR (50),        
       Branch Varchar(15),        
       AccountCount14 int)        
CREATE TABLE #yearACCountFinal2014 (SalePersonName VARCHAR (50),        
         Branch Varchar(15),        
         AccountCount14 int)        
        
CREATE TABLE #tmp (   AccountType varchar(50)        
                        ,CustomerID  VARCHAR(15)        
      ,AccountName VARCHAR(150)        
      ,AccountDate varchar(15)        
      ,FirstSaleDate Varchar(15)        
      ,Branch  VARCHAR(15)        
      ,Category  VARCHAR(100)        
      ,SalePersonName  VARCHAR(50)        
      ,DatabaseName Varchar(3)        
      ,YTD2013 int        
      ,YTD2014 int        
        );        
                
CREATE TABLE #tmpFinal (      AccountType varchar(50)        
                        ,CustomerID  VARCHAR(15)        
      ,AccountName VARCHAR(150)        
      ,AccountDate varchar(15)        
      ,FirstSaleDate Varchar(15)        
      ,Branch  VARCHAR(15)        
      ,Category  VARCHAR(100)        
      ,SalePersonName  VARCHAR(50)        
      ,DatabaseName Varchar(3)        
      ,YTD2013 int        
      ,YTD2014 int        
        );                
                
CREATE TABLE #tactive (  CustomerID  VARCHAR(15)        
      ,FirstInvOfMonth Date        
      ,STN_INV_DT date        
      ,STN_OS_SLP varchar(75)        
        );        
CREATE TABLE #tYTDactive (  CustomerID  VARCHAR(15)        
      ,FirstInvOfMonth Date        
      ,STN_INV_DT date        
      ,STN_OS_SLP varchar(75)        
        );                
CREATE TABLE #tYTD14active (  CustomerID  VARCHAR(15)        
  ,FirstInvOfMonth Date        
  ,STN_INV_DT date        
  ,STN_OS_SLP varchar(75)        
 );                
                
CREATE TABLE #tTtlactive (  CustomerID  VARCHAR(15)        
 ,FirstInvOfMonth Date        
 ,STN_INV_DT date        
 ,STN_OS_SLP varchar(75)        
);                
                                
DECLARE @company VARCHAR(15);         
DECLARE @prefix VARCHAR(15);         
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @CurrenyRate varchar(15);        
DECLARE @Category varchar(50);        
        
set @DB= @DBNAME        
              
IF @Branch = 'ALL'        
 BEGIN        
  set @Branch = ''        
 END        
         
 IF @Market = 'ALL'          
   set @Market = ''        
 ELSE IF @Market ='Unknown'        
  Begin        
   set @Category='Unknown'        
   set @Market = ''        
  END        
         
              
 IF @DBNAME = 'ALL'        
 BEGIN        
 IF @version = '0'        
   BEGIN        
   DECLARE ScopeCursor CURSOR FOR      
    select DatabaseName, company,prefix from tbl_itech_DatabaseName       
     OPEN ScopeCursor;       
   END        
   ELSE        
   BEGIN        
   DECLARE ScopeCursor CURSOR FOR      
    select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS      
     OPEN ScopeCursor;       
   END       
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query NVARCHAR(4000);           
       set @DB= @prefix            
               
       CREATE TABLE #tactive1 (  CustomerID  VARCHAR(15)        
             ,FirstInvOfMonth Date        
             ,STN_INV_DT date        
             ,STN_OS_SLP varchar(75)        
               );        
       CREATE TABLE #tYTDactive1 (  CustomerID  VARCHAR(15)        
             ,FirstInvOfMonth Date        
             ,STN_INV_DT date        
             ,STN_OS_SLP varchar(75)        
       );        
       CREATE TABLE #tYTD14active1 (  CustomerID  VARCHAR(15)        
         ,FirstInvOfMonth Date        
             ,STN_INV_DT date        
             ,STN_OS_SLP varchar(75)        
       );        
               
                             CREATE TABLE #tTtlactive1 (  CustomerID  VARCHAR(15)        
              ,FirstInvOfMonth Date        
              ,STN_INV_DT date        
                  ,STN_OS_SLP varchar(75)        
             );          
        IF @Account = '5'        
         BEGIN        
         SET @query = ' INSERT INTO #tYTDactive1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
                  select  stn_sld_cus_id, '''+  @YTDFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
                from '+ @DB +'_sahstn_rec        
                  where STN_INV_DT >= '''+  @YTDFD + ''' and STN_INV_DT <= ''' + @YTDTD +'''        
                   group by stn_sld_cus_id ,STN_INV_DT'        
                  Print @query;            
                  EXECUTE sp_executesql @query;        
         SET @query = ' INSERT INTO #tYTD14active1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
                  select  stn_sld_cus_id, '''+  @YTD14FD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
                  from '+ @DB +'_sahstn_rec        
                  where STN_INV_DT >= '''+  @YTD14FD + ''' and STN_INV_DT <= ''' + @YTD14TD +'''        
                   group by stn_sld_cus_id ,STN_INV_DT'        
                  Print @query;            
                  EXECUTE sp_executesql @query;                
         END                     
               
       SET @query = ' INSERT INTO #tactive1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''        
               group by stn_sld_cus_id ,STN_INV_DT'        
        
           EXECUTE sp_executesql @query;        
                   
         -- Total Active        
           SET @query = ' INSERT INTO #tTtlactive1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''        
              group by stn_sld_cus_id ,STN_INV_DT'        
                       
               print @query;        
        
           EXECUTE sp_executesql @query;            
if(@DB = 'US' and @ATD > ('2015-11-01'))   
begin  
      
           -- Total Active        
           SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''         
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                 
              ORDER BY 9 DESC '        
             EXECUTE sp_executesql @query;        
                    
                   
                     
             -- Reactivated         
            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName,YTD2013, YTD2014)        
              SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate        
              , CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt         
              ORDER BY 9 DESC '        
               EXECUTE sp_executesql @query;        
                       
        -- NEW        
            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName,'''+ @DB +''' ,0,0        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')   
                and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''        
                  
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)'        
          print  @query        
             EXECUTE sp_executesql @query;        
                     
             IF @Account = '1'        
           BEGIN        
             SET @query = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
             left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')     
               and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
                      
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt,  dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)) as t        
               group by  SalePersonName,CUS_ADMIN_BRH;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt, dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')     
               and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''       
                      
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt,  dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)) as t                   group by  SalePersonName,CUS_ADMIN_BRH'        
           print  @query        
             EXECUTE sp_executesql @query;        
             END        
           ELSE IF @Account = '5'        
           BEGIN        
           SET @query = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTDactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTDFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTDFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH;         
                       
              INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)         
               select SalePersonName,CUS_ADMIN_BRH, COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTD14active1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTD14FD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTD14FD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH '        
              print  @query        
              EXECUTE sp_executesql @query;        
           END        
         SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @DB +''',0,0        
           FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH         
           Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''        
                   
            EXECUTE sp_executesql @query;        
                    
                      SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @DB +''',0,0        
           FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''        
                   
            EXECUTE sp_executesql @query;             
                         
                 SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP),'''+ @DB +''',0,0        
               FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''        
                   
            EXECUTE sp_executesql @query;     
End  
else  
Begin  
-- Total Active        
           SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''         
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                 
              ORDER BY 9 DESC '        
             EXECUTE sp_executesql @query;        
                    
                   
                     
             -- Reactivated         
            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName,YTD2013, YTD2014)        
              SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate        
              , CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')      
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt         
              ORDER BY 9 DESC '        
               EXECUTE sp_executesql @query;        
                       
        -- NEW        
            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName,'''+ @DB +''' ,0,0        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''     
                and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))     
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt'        
          print  @query        
             EXECUTE sp_executesql @query;        
                     
             IF @Account = '1'        
           BEGIN        
             SET @query = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt) as t        
               group by  SalePersonName,CUS_ADMIN_BRH;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt) as t                   group by  SalePersonName,CUS_ADMIN_BRH'        
           print  @query        
             EXECUTE sp_executesql @query;        
             END        
           ELSE IF @Account = '5'        
           BEGIN        
           SET @query = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTDactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTDFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTDFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH;         
                       
              INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)         
               select SalePersonName,CUS_ADMIN_BRH, COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTD14active1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive1 t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTD14FD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTD14FD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH '        
              print  @query        
              EXECUTE sp_executesql @query;        
           END        
         SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @DB +''',0,0        
           FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH         
           Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''        
                   
            EXECUTE sp_executesql @query;        
                    
                      SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @DB +''',0,0        
           FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''        
                   
            EXECUTE sp_executesql @query;             
                         
                 SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP),'''+ @DB +''',0,0        
               FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''        
                   
            EXECUTE sp_executesql @query;    
End     
                                
            Drop table #tactive1;        
            DROP table #tYTDactive1;        
            DROP table #tYTD14active1;        
            Drop table #tTtlactive1;        
                    
            if @DB='US'        
        Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')     
     else        
      Insert into #tmpFinal select * from #tmp         
             
     delete from #tmp        
             
      if @DB='US'        
     Insert into #yearACCountFinal Select * from #yearACCount where Branch  not in ('BHM','MTL','TAI')        
     else        
     Insert into #yearACCountFinal Select * from #yearACCount         
     delete from #yearACCount        
             
      if @DB='US'        
     Insert into #yearACCountFinal2014 Select * from #yearACCount2014 where Branch  not in ('BHM','MTL','TAI')        
     else        
     Insert into #yearACCountFinal2014 Select * from #yearACCount2014         
     delete from #yearACCount2014        
                    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
     IF @Account = '5'        
     BEGIN        
      SET @sqltxt = ' INSERT INTO #tYTDactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @YTDFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT >= '''+  @YTDFD + ''' and STN_INV_DT <= ''' + @YTDTD +'''        
               group by stn_sld_cus_id ,STN_INV_DT'        
              Print @sqltxt;            
                set @execSQLtxt = @sqltxt;         
                EXEC (@execSQLtxt);        
      SET @sqltxt = ' INSERT INTO #tYTD14active(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @YTD14FD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT >= '''+  @YTD14FD + ''' and STN_INV_DT <= ''' + @YTD14TD +'''        
               group by stn_sld_cus_id ,STN_INV_DT'        
              Print @sqltxt;            
              set @execSQLtxt = @sqltxt;         
              EXEC (@execSQLtxt);        
     END        
             
           SET @sqltxt = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''        
               group by stn_sld_cus_id ,STN_INV_DT'        
              Print @sqltxt;            
                set @execSQLtxt = @sqltxt;         
                EXEC (@execSQLtxt);        
           -- Total Active        
           SET @sqltxt = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP)        
              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP)        
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''        
               group by stn_sld_cus_id ,STN_INV_DT'        
                Print @sqltxt;       
               set @execSQLtxt = @sqltxt;         
                                       EXEC (@execSQLtxt);        
        if(@DB = 'US' and @ATD > ('2015-11-01'))   
              
             begin           
           -- Total Active        
           SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''         
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                 
              ORDER BY 9 DESC '    
             -- print(@sqltxt)      
            set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
                     
             -- Reactivated         
            SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,        
              SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt)  from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate        
              , CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
   left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt        
              ORDER BY 9 DESC  '        
           print(@sqltxt)        
                set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
               
               
        -- NEW        
            SET @sqltxt = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName,'''+ @DB +''' ,0,0        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
                 
               and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''   
                    
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)'        
  
          print(@sqltxt)    ;  
         set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
                 
         IF @Account = '1'        
         BEGIN        
         SET @sqltxt = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)) as t        
               group by  SalePersonName,CUS_ADMIN_BRH;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)) as t        
               group by  SalePersonName,CUS_ADMIN_BRH'        
          print @sqltxt      ;  
             set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
         END        
       ELSE IF @Account = '5'        
       BEGIN        
       SET @sqltxt = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt)  from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTDactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
  left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTDFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTDFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH ;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTD14active t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTD14FD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTD14FD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH '        
          print  @sqltxt        
             set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
       END        
              
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
               FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''        
                   
        
       --print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
               
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
                FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''        
        
       --print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
       EXEC (@execSQLtxt);         
               
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
                FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''        
        
       print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
       EXEC (@execSQLtxt);         
End  
Else  
Begin  
-- Total Active        
           SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''         
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                 
              ORDER BY 9 DESC '    
              print(@sqltxt)      
            set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
                     
             -- Reactivated         
            SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,        
              SalePersonName,DatabaseName, YTD2013, YTD2014)        
              SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate        
              , CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName,'''+ @DB +''',0,0        
              FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
   left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt        
              ORDER BY 9 DESC  '        
           print(@sqltxt)        
                set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
               
               
        -- NEW        
            SET @sqltxt = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName,'''+ @DB +''' ,0,0        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''     
                and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))     
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt'        
                    
          print(@sqltxt)    ;  
         set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
                 
         IF @Account = '1'        
         BEGIN        
         SET @sqltxt = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt) as t        
               group by  SalePersonName,CUS_ADMIN_BRH;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(t.SalePersonName) from (        
               select STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,        
               Min(STN_OS_SLP) as SalePersonName        
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID         
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID        
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
               and coc_frst_sls_dt Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt) as t        
               group by  SalePersonName,CUS_ADMIN_BRH'        
          print @sqltxt      ;  
             set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
         END        
       ELSE IF @Account = '5'        
       BEGIN        
       SET @sqltxt = ' INSERT INTO #yearACCount(SalePersonName, Branch, AccountCount)        
               select SalePersonName,CUS_ADMIN_BRH,COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTDactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTDFD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @YTDFD + ''' And ''' + @YTDTD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTDFD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH ;        
                       
               INSERT INTO #yearACCount2014(SalePersonName, Branch, AccountCount14)        
               select SalePersonName,CUS_ADMIN_BRH, COUNT(tYTD.SalePersonName) from (        
               SELECT CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,         
              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate,        
              CUS_ADMIN_BRH,cuc_desc30 as category,        
              Min(t.STN_OS_SLP) as SalePersonName        
              FROM #tYTD14active t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
              where         
              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and        
              t.CustomerID Not IN         
              ( select distinct t1.CustomerID  from #tactive t1,        
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID         
              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' + @YTD14FD + ''')        
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec         
              where coc_frst_sls_dt Between  ''' + @YTD14FD + ''' And ''' + @YTD14TD +'''        
              )        
              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @YTD14FD + '''        
              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt ) as tYTD        
               group by  SalePersonName,CUS_ADMIN_BRH '        
          --print  @sqltxt        
             set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
       END        
              
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
               FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''        
                   
        
       --print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
         EXEC (@execSQLtxt);        
               
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
                FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''        
        
       --print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
       EXEC (@execSQLtxt);         
               
       SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName, YTD2013, YTD2014)        
               SELECT   ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category        
               ,Min(STN_OS_SLP),'''+ @DB +''',0,0        
                FROM '+ @DB +'_sahstn_rec        
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID        
           Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat        
           where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')        
           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
           and STN_INV_DT <= ''' + @NTD +'''        
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH        
           Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''        
        
       print(@sqltxt)        
       set @execSQLtxt = @sqltxt;         
       EXEC (@execSQLtxt);         
End        
              
       if @DB='US'        
        Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')        
     else        
        Insert into #tmpFinal select * from #tmp         
             
     delete from #tmp        
             
      if @DB='US'        
     Insert into #yearACCountFinal Select * from #yearACCount where Branch  not in ('BHM','MTL','TAI')        
     else        
     Insert into #yearACCountFinal Select * from #yearACCount         
     delete from #yearACCount        
             
     if @DB='US'        
     Insert into #yearACCountFinal2014 Select * from #yearACCount2014 where Branch  not in ('BHM','MTL','TAI')        
     else        
     Insert into #yearACCountFinal2014 Select * from #yearACCount2014         
     delete from #yearACCount2014        
               
     END        
          
        IF @Category='Unknown'        
           BEGIN        
                IF @ISCombineData =1        
     BEGIN        
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName,DatabaseName        
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType        
        where AccountType=@Account         
       -- and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))        
         and Category is null        
        group by Name,Category,DatabaseName        
     END         
     Else IF @ISCombineData =2        
     BEGIN        
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,DatabaseName        
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType        
        where AccountType=@Account         
        --and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))        
        and Category is null        
        group by Name,Branch,DatabaseName        
     END         
      Else        
     BEGIN        
       select *, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category        
       from #tmpFinal where AccountType=@Account          
     --  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))        
       and Category is null        
       order by CustomerID --and AccountDate <= '2013-03-31' and AccountDate >='2013-03-01'        
     END        
           END        
           ELSE        
           BEGIN        
                 IF @ISCombineData =1        
     BEGIN        
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName,DatabaseName        
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType        
        where AccountType=@Account         
      --  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))        
        group by Name,Category,DatabaseName        
     END         
     Else IF @ISCombineData =2        
     BEGIN        
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,DatabaseName        
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType        
        where AccountType=@Account         
        --and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))        
        group by Name,Branch,DatabaseName        
     END         
      Else        
     BEGIN        
     --select *, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category        
     --  from #tmpFinal         
     --   where AccountType=@Account          
     --  order by SalePersonName --Added by mukesh 2013-12-11        
     --select * from #yearACCountFinal order by AccountCount14        
             
      select tf.AccountType,tf.CustomerID,tf.AccountName,tf.AccountDate,tf.FirstSaleDate,tf.Category,tf.SalePersonName,tf.DatabaseName, tf.Branch, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category, ISNULL(yaf.AccountCount,0) as AccountCount,
       ISNULL(yaf14.AccountCount14,0) AS AccountCount14        
       from #tmpFinal tf         
       --Join #yearACCountFinal yaf on yaf.SalePersonName = tf.SalePersonName and yaf.Branch = tf.Branch        
       -- Join #yearACCountFinal2014 yaf14 on yaf14.SalePersonName = tf.SalePersonName and yaf14.Branch = tf.Branch        
     LEFT OUTER Join #yearACCountFinal yaf on yaf.SalePersonName = tf.SalePersonName and yaf.Branch = tf.Branch        
    LEFT OUTER    Join #yearACCountFinal2014 yaf14 on yaf14.SalePersonName = tf.SalePersonName and yaf14.Branch = tf.Branch               
        where AccountType=@Account          
       order by tf.SalePersonName        
      -- select * from #tmpFinal         
       --select * from #yearACCountFinal        
       --select * from #yearACCountFinal2014         
        --Added by mukesh 2013-12-11        
     END        
           END        
                   
            Drop table #tTtlactive;        
            Drop table #tmpFinal;         
            Drop table #yearACCountFinal;        
             Drop table #yearACCountFinal2014;        
   Drop table #tactive;           
   Drop Table #tYTDactive;        
   Drop Table #tYTD14active;        
  Drop table #tmp          
  Drop table #yearACCount;        
  Drop table #yearACCount2014;        
          
END        
          
-- exec [sp_itech_GetNew_Dormant_Lost_Account_Test_V1] 'TW','ALL',0,'5','2018-01-12','ALL' 
GO
