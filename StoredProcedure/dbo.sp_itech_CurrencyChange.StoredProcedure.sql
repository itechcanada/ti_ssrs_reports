USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CurrencyChange]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_CurrencyChange] @DBNAME varchar(50),@Market varchar(50)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)

set @DB=  @DBNAME
--Set @DB = UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'
--Set @DB = @DBNAME
CREATE TABLE #tmp (   Cat		VARCHAR(10)
                     , CustID		 VARCHAR(10)
   					, Category 			 VARCHAR(100)
   					, UpdateDt	Datetime
   					,Databases   varchar(10)
   					
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

if @Market ='ALL'
 BEGIN
 set @Market = ''
 END

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);
  				 SET @DB=  UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				SET @query =
  				        'INSERT INTO #tmp (Cat, CustID,Category,UpdateDt,Databases) 
  				        select Substring(pal_rec_txt,1,3) as CurFrom, Substring(pal_rec_txt,4,3) as CurTo,
 Substring(pal_rec_txt,7,4) + ''.'' +  Substring(pal_rec_txt,11,7)  as OldRate, pal_upd_dtts, '''+ @Prefix +''' as ''DataBases''
						from ' + @DB + '.sctpal_rec 
						where pal_tbl_nm = ''scrcrx'' 						
						order by pal_upd_dtts desc'
						--print(@query)
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			set @DatabaseName=  (select DatabaseName  from tbl_itech_DatabaseName where Prefix=''+ @DBNAME +'')
			Set @DB = UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
			SET @sqltxt ='INSERT INTO #tmp (Cat, CustID,Category,UpdateDt, Databases) 
  				        select Substring(pal_rec_txt,1,3) as CurFrom, Substring(pal_rec_txt,4,3) as CurTo,
 Substring(pal_rec_txt,7,4) + ''.'' +  Substring(pal_rec_txt,11,7)  as OldRate, pal_upd_dtts,
  				        '''+ @DBNAME +''' as ''DataBases''
						from ' + @DB + '.sctpal_rec 
						where pal_tbl_nm = ''scrcrx'' 					
						order by  pal_upd_dtts desc'
				print(@sqltxt)
			set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
 SELECT * FROM #tmp 
END

-- exec sp_itech_CurrencyChange 'UK','ALL'


GO
