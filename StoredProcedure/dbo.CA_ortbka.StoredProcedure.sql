USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_ortbka]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CA_ortbka] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_ortbka_rec', 'U') IS NOT NULL
		drop table dbo.CA_ortbka_rec;
    
        
SELECT *
into  dbo.CA_ortbka_rec
FROM [LIVECASTX].[livecastxdb].[informix].[ortbka_rec];

END
--- exec CA_ortbka
-- select * from CA_ortbka_rec
GO
