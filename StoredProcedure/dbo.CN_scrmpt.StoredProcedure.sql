USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_scrmpt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CN_scrmpt] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_scrmpt_rec', 'U') IS NOT NULL
		drop table dbo.CN_scrmpt_rec;
    
        
SELECT *
into  dbo.CN_scrmpt_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[scrmpt_rec];

END
--- exec CN_scrmpt
-- select * from CN_scrmpt_rec
GO
