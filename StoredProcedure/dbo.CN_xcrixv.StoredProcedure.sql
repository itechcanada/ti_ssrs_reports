USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_xcrixv]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Mukesh       
-- Create date: Feb 05, 2016      
-- Description: <Description,,>      
-- =============================================      
Create PROCEDURE [dbo].[CN_xcrixv]       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
    -- Insert statements for procedure here      
          
          
    IF OBJECT_ID('dbo.CN_xcrixv_rec', 'U') IS NOT NULL      
  drop table dbo.CN_xcrixv_rec;      
          
              
SELECT *      
into  dbo.CN_xcrixv_rec      
FROM [LIVECNSTX].[livecnstxdb].[informix].[xcrixv_rec];      
      
END      
--  exec  CN_xcrixv      
-- select * from CN_xcrixv_rec
GO
