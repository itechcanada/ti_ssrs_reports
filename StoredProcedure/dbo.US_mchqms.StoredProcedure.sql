USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_mchqms]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,OCT 30, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[US_mchqms]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_mchqms_rec', 'U') IS NOT NULL
		drop table dbo.US_mchqms_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_mchqms_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[mchqms_rec] ; 
  
END
-- select * from US_mchqms_rec 
GO
