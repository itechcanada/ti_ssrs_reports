USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_tcttsa]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <Create Date,August,30 2015,>


-- =============================================
CREATE PROCEDURE [dbo].[UK_tcttsa]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

insert into  [Stratix_US].[dbo].[UK_tcttsa_rec]
 SELECT *
  from [LIVEUKSTX].[liveukstxdb].[informix].[tcttsa_rec] ; 
  
END
-- select * from uk_tcttsa_rec 

GO
