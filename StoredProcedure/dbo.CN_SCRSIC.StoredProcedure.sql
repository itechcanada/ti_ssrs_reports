USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_SCRSIC]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Sumit>  
-- Create date: <30 Sept 2020>  
-- Description: <to get Sub Classification/Market>  
  
-- =============================================  
create PROCEDURE [dbo].[CN_SCRSIC]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_scrsic_rec', 'U') IS NOT NULL  
  drop table dbo.CN_scrsic_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_scrsic_rec  
from [LIVECNSTX].[livecnstxdb].[informix].scrsic_rec;   
  
END
GO
