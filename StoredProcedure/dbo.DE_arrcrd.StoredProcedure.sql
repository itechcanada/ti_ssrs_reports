USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_arrcrd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
        
-- =============================================        
-- Author:  <Author,Sumit>        
-- Create date: <Create Date,01/25/2021,>        
-- Description: <Description,Customer Credit,>        
        
-- =============================================        
CREATE PROCEDURE [dbo].[DE_arrcrd]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
Truncate table dbo.DE_arrcrd_rec ;         
        
-- Insert statements for procedure here        
insert into dbo.DE_arrcrd_rec (crd_cmpy_id, crd_cus_id, crd_acct_opn_dt, crd_inv_freq, crd_inv_mthd, crd_comb_inv, crd_cus_cl, crd_po_reqd, crd_bnk_nm, crd_bnk_acct_no, crd_int_cr_pct, crd_rmt_to_addr, crd_dun_no, crd_dun_rtg,  
crd_dun_dt, crd_pdx, crd_pdx_dt, crd_fis_id, crd_vat_id, crd_non_stk_sls, crd_sch_prty, crd_cr_ctl_cus_id, crd_payg_cus_id, crd_pmt_trm, crd_disc_trm, crd_pd_mth_excl1, crd_pd_mth_excl2, crd_pd_mth_excl3,  
crd_mthd_pmt, crd_brh_trm, crd_lte_chrg, crd_lchrg_int_pct, crd_lchrg_grc_dy, crd_lchrg_min, crd_cr_auth, crd_cr_ins_clsfcn, crd_ins_co, crd_ins_agt_no, crd_pv_ins_lim, crd_pv_ins_eff_dt, crd_ins_lim,  
crd_ins_eff_dt, crd_ins_rnw_dt, crd_co_lim, crd_co_lim_eff_dt, crd_co_lim_rnw_dt, crd_co_lim_apvd_by, crd_addnl_lim, crd_addnl_eff_dt, crd_addnl_rnw_dt, crd_addnl_apvd_by, crd_lim_pct, crd_cr_rvw_dt,  
crd_cr_rcvb_pct, crd_avg_agng_dy, crd_ovd_grc_dy, crd_cr_ctl,crd_cr_rmk)      
SELECT crd_cmpy_id, crd_cus_id, crd_acct_opn_dt, crd_inv_freq, crd_inv_mthd, crd_comb_inv, crd_cus_cl, crd_po_reqd, crd_bnk_nm, crd_bnk_acct_no, crd_int_cr_pct, crd_rmt_to_addr, crd_dun_no, crd_dun_rtg,  
crd_dun_dt, crd_pdx, crd_pdx_dt, crd_fis_id, crd_vat_id, crd_non_stk_sls, crd_sch_prty, crd_cr_ctl_cus_id, crd_payg_cus_id, crd_pttrm, crd_disc_trm, crd_pd_mth_excl1, crd_pd_mth_excl2, crd_pd_mth_excl3,  
crd_mthd_pmt, crd_brh_trm, crd_lte_chrg, crd_lchrg_int_pct, crd_lchrg_grc_dy, crd_lchrg_min, crd_cr_auth, crd_cr_ins_clsfcn, crd_ins_co, crd_ins_agt_no, crd_pv_ins_lim, crd_pv_ins_eff_dt, crd_ins_lim,  
crd_ins_eff_dt, crd_ins_rnw_dt, crd_co_lim, crd_co_lim_eff_dt, crd_co_lim_rnw_dt, crd_co_lim_apvd_by, crd_addnl_lim, crd_addnl_eff_dt, crd_addnl_rnw_dt, crd_addnl_apvd_by, crd_lim_pct, crd_cr_rvw_dt,  
crd_cr_rcvb_pct, crd_avg_agng_dy, crd_ovd_grc_dy, crd_cr_ctl ,''       
  from [LIVEDESTX].[livedestxdb].[informix].[arrcrd_rec] ;         
          
END   
  
/*  
20210413  Sumit  
sign mismatch or overflow error for crd_cr_rmk column  
Skipped the column   
20210526	Sumit
modify local table column name from crd_pttrm to crd_pmt_trm
*/  
  
GO
