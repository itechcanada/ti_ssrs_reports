USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Process_Work_Center]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <24 NOV 2013>    
-- Description: <Getting Distinct Process form each database for JOB Yield for SSRS reports>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_Process_Work_Center]  @DBNAME varchar(50), @version char = '0'    
    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
declare @sqltxt1 varchar(8000)    
declare @execSQLtxt varchar(7000)    
declare @DB varchar(100)    
        
CREATE TABLE #tmp (     
     [Database]   VARCHAR(10)    
     , prs  VARCHAR(65)    
                );     
    
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(35);    
DECLARE @Name VARCHAR(15);    
    
IF @DBNAME = 'ALL'    
 BEGIN    
    IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query1 NVARCHAR(max);       
        SET @DB=  @prefix; --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix].orrprs_rec'    
             SET @query1 ='INSERT INTO #tmp ([Database], prs)    
                   select distinct ''' + @prefix + ''' as ''Database'', prs_prs from ' + @DB + '_orrprs_rec;'    
       print @query1;    
        EXECUTE sp_executesql @query1;     
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN    
    SET @Prefix = @DBNAME   
     IF @version = '0'
     BEGIN
     SET @DatabaseName = (SELECT DatabaseName from tbl_itech_DatabaseName WHERE Prefix = '' + @Prefix + '')    
     END
     ELSE
     BEGIN
     SET @DatabaseName = (SELECT DatabaseName from tbl_itech_DatabaseName_PS WHERE Prefix = '' + @Prefix + '')    
     END 
    
    SET @DB= @prefix; --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix].orrprs_rec'    
        SET @sqltxt1 ='INSERT INTO #tmp ([Database], prs)    
                   select distinct ''' + @prefix + ''' as ''Database'', prs_prs from ' + @DB + '_orrprs_rec;'    
     print(@sqltxt1)     
   EXEC (@sqltxt1);    
   END    
       
select prs AS Value ,prs AS text, 'B' AS temp from #tmp    
Union      
Select 'ALL' as Value,'ALL PWC' as text,'A' as temp    
 order by temp,text    
   drop table #tmp    
END    
    
-- Exec [sp_itech_Process_Work_Center] 'ALL' ,'1'   
-- Exec [sp_itech_Process_Work_Center] 'PS','1'
GO
