USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Excess6MonthInventory]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <19 Apr 2016>  
-- Description: <Excess6MonthInventory> 
 
-- =============================================  
CREATE  PROCEDURE [dbo].[sp_itech_Excess6MonthInventory]  @DBNAME varchar(50), @CurrentMonth datetime, @Branch Varchar(3)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
declare @DB varchar(100);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);  
declare @FD varchar(10)    
declare @TD varchar(10)
declare @FD1 varchar(10)    
declare @TD1 varchar(10)
declare @FD2 varchar(10)    
declare @TD2 varchar(10)
declare @FD3 varchar(10)    
declare @TD3 varchar(10)
declare @FD4 varchar(10)    
declare @TD4 varchar(10)
declare @FD5 varchar(10)    
declare @TD5 varchar(10)


set @FD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-0, 0),120)   -- current month from date
set @TD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-0, -1),120)  --current month To date

set @FD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-1, 0),120)   -- current month from date
set @TD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-1, -1),120)  --current month To date

set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-2, 0),120)   -- current month from date
set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-2, -1),120)  --current month To date

set @FD3 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-3, 0),120)   -- current month from date
set @TD3 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-3, -1),120)  --current month To date

set @FD4 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-4, 0),120)   -- current month from date
set @TD4 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-4, -1),120)  --current month To date

set @FD5 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentMonth)-5, 0),120)   -- current month from date
set @TD5 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @CurrentMonth)-5, -1),120)  --current month To date
         
CREATE TABLE #temp (   
     Branch   VARCHAR(3)  
     , Blgweight decimal(20,0)  
     );  
 
      
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR   
   BEGIN  
   SET @sqltxt ='INSERT INTO #temp (Branch, Blgweight)  
     Select  prd_brh, SUM(Excess) from (

-- Mar 2016		
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  ) AS InvoiceMonthTotal, SUM(ISNULL(prd_ohd_wgt,0)) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
		and UpdateDtTm  >= ''' + @FD + ''' and UpdateDtTm  <= ''' + @TD + '''          
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh

-- Feb 2016		
Union ALL
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD1 + ''' and ''' + @TD1 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
		and UpdateDtTm  >= ''' + @FD1 + ''' and UpdateDtTm  <= ''' + @TD1 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh
-- Jan 2016	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD2 + ''' and ''' + @TD2 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD2 + ''' and UpdateDtTm  <= ''' + @TD2 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh
-- Dec 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD3 + ''' and ''' + @TD3 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD3 + ''' and UpdateDtTm  <= ''' + @TD3 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
-- Nov 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD4 + ''' and ''' + @TD4 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD4 + ''' and UpdateDtTm  <= ''' + @TD4 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
-- Oct 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD5 + ''' and ''' + @TD5 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD5 + ''' and UpdateDtTm  <= ''' + @TD5 + '''          
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
) as oquery group by oquery.prd_brh'
  END  
  ELSE  
  BEGIN  
  SET @sqltxt ='INSERT  INTO #temp (Branch, Blgweight)  
     Select  prd_brh, SUM(Excess) from (

-- Mar 2016		
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD + '''  And ''' + @TD + '''  ) AS InvoiceMonthTotal, SUM(ISNULL(prd_ohd_wgt,0)) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
		and UpdateDtTm  >= ''' + @FD + ''' and UpdateDtTm  <= ''' + @TD + '''          
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh

-- Feb 2016		
Union ALL
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD1 + ''' and ''' + @TD1 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
		and UpdateDtTm  >= ''' + @FD1 + ''' and UpdateDtTm  <= ''' + @TD1 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh
-- Jan 2016	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD2 + ''' and ''' + @TD2 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD2 + ''' and UpdateDtTm  <= ''' + @TD2 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh
-- Dec 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD3 + ''' and ''' + @TD3 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD3 + ''' and UpdateDtTm  <= ''' + @TD3 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
-- Nov 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD4 + ''' and ''' + @TD4 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD4 + ''' and UpdateDtTm  <= ''' + @TD4 + '''           
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
-- Oct 2015	
Union ALL	
 SELECT (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) as Excess , prd_brh
		FROM 
		(
		SELECT  (SELECT SUM(ISNULL(SAT_BLG_WGT,0)) FROM US_sahsat_rec WHERE prd_frm = SAT_FRM AND prd_Grd = SAT_GRD  AND prd_size = sat_size AND prd_fnsh = sat_fnsh
		AND SAT_INV_DT  between ''' + @FD5 + ''' and ''' + @TD5 + '''  ) AS InvoiceMonthTotal, SUM(prd_ohd_wgt) as prd_ohd_wg
		 , prd_brh
		FROM US_intprd_rec_history              
		WHERE  prd_invt_sts = ''S''    AND (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') 
		and UpdateDtTm  >= ''' + @FD5 + ''' and UpdateDtTm  <= ''' + @TD5 + '''          
		GROUP BY prd_brh,prd_frm, prd_Grd,prd_size, prd_fnsh
		) AS t 
		GROUP BY prd_brh		
) as oquery group by oquery.prd_brh'
               
         
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ;  
 DROP TABLE  #temp;  
END  
  
--EXEC [sp_itech_Excess6MonthInventory] 'US' , '2016-03-05','' 
GO
