USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetStates]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <17 Jan 2018>  
-- Description: <Getting list of All States for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetStates]  @DBNAME varchar(50) 
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  Value varchar(15)   
        ,text Varchar(500)  
        ,temp varchar(3)  
        )  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( Value,text,temp)  
                  SELECT distinct spv_st_prov AS ''Value'', spv_desc30 AS ''text'',''B'' AS temp  
                            FROM         '+ @Prefix +'_scrspv_rec where spv_desc30 != ''''  '  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)  
                    SELECT  distinct   spv_st_prov AS ''Value'', spv_desc30 AS ''text'',''B'' AS temp  
                            FROM         '+ @DBNAME +'_scrspv_rec where spv_desc30 != '''' 
       Union   
       Select ''ALL'' as ''Value'',''All State'' as ''text'',''A'' as temp  
       Order by temp,text  
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
        
      IF @DBNAME = 'ALL'  
 BEGIN  
      select * from #tmp  
      Union   
   Select 'ALL' as 'Value','All State' as 'text','A' as temp  
   Order by temp,text  
      End  
      ELSE  
      BEGIN  
                   select * from #tmp ; 
      END  
      drop table  #tmp  
END  
  
-- exec sp_itech_GetStates  'UK'  
 
GO
