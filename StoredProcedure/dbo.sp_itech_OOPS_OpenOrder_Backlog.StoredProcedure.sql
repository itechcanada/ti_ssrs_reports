USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OOPS_OpenOrder_Backlog]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <06 Mar 2019>            
-- Description: <Getting top 50 customers for SSRS reports>            
         
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_OOPS_OpenOrder_Backlog]  @DBNAME varchar(50),@Branch varchar(10)  
            
AS            
BEGIN           
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(7000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
declare @NOOfCust varchar(15)            
DECLARE @ExchangeRate varchar(15)   
declare @FDRed varchar(10)            
declare @TDRed varchar(10)     
declare @FDNR varchar(10)   
declare @FDYel varchar(10)   
declare @TDYel varchar(10)   
declare @FDGre varchar(10)   
--declare @TDGre varchar(10)   
declare @FDTod varchar(10)   
      
set @FDRed = CONVERT(varchar(10), '2010-03-29',120);  
set @TDRed = CONVERT(varchar(10), [dbo].[WorkDay] (GETDATE(),-6),120);  
  
set @FDNR = CONVERT(varchar(10),getdate() - 30,120);  
  
set @FDYel = CONVERT(varchar(10), [dbo].[WorkDay] (GETDATE(),-5),120);  
set @TDYel = CONVERT(varchar(10), [dbo].[WorkDay] (GETDATE(),-2),120);  
  
set @FDGre = CONVERT(varchar(10), [dbo].[WorkDay] (GETDATE(),-1),120);  
--set @TDGre = CONVERT(varchar(10), [dbo].[WorkDay] (GETDATE(),-1),120);  
  
set @FDTod = CONVERT(varchar(10),getdate() ,120);  
  
set @FD = CONVERT(varchar(10),getdate() - 360,120);  
set @TD = CONVERT(varchar(10),getdate() + 360,120);  
  
--set @FD = convert (Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0), 120); --First day of previous month          
--set @TD = CONVERT(VARCHAR(10), dateadd(day, 0 - day(dateadd(month, 2 , GETDATE())), dateadd(month, 2 , GETDATE())) , 120); -- LAst date of next month  
            
set @DB= @DBNAME   
       
CREATE TABLE #tmpFinal (    
Category Varchar(250)  
,OpenOrderCount int  
,seq int  
)  
           
CREATE TABLE #tmp (  CompanyId varchar(5)             
        --,OrderPrefix Varchar(2)            
        --,OrderNo varchar(10)            
        --,OrderItem   varchar(3)            
        ,Branch varchar(10)            
        --,CustID   VARCHAR(10)             
        --, CustName     VARCHAR(65)            
        --, CustomerCat   VARCHAR(65)             
        , ChargeValue    DECIMAL(20, 2)            
        --, BalPcs  DECIMAL(20, 2)            
        --, BalWgt           DECIMAL(20, 2)            
        --, BalMsr    DECIMAL(20, 2)            
        --, StatusAction               varchar(2)            
        --, Salesperson     varchar(65)            
        , DueDate    varchar(10)            
        --, ProductName   VARCHAR(100)             
        , OrderType     varchar(65)            
        --,Form  varchar(35)            
        --,Grade  varchar(35)            
        --,Size  varchar(35)            
        --,Finish  varchar(35)           
        --,CustPO   VARCHAR(30)         
        --,CustAdd VARCHAR(170)         
        -- ,SalesPersonIs Varchar(10)          
        --,SalesPersonOs Varchar(10)         
        --,NetAmt Decimal(20,2)        
        --,NetPct Decimal(20,2)        
        --,ShipWhs Varchar(3)       
        --,PrdFull varchar(70)        
                 );             
            
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(5);            
DECLARE @Name VARCHAR(15);            
DECLARE @CurrenyRate varchar(15);             
 
 -- 20200428
 if @Branch ='ROC'            
 BEGIN            
 set @Branch = 'ROS'            
 END  
 -- 

 if @Branch ='ALL'            
 BEGIN            
 set @Branch = ''            
 END            
    
IF @DBNAME = 'ALL'      
 BEGIN            
          
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
   OPEN ScopeCursor;              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
   BEGIN            
        DECLARE @query NVARCHAR(Max);               
      SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'            
      IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End               
    Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End               
      SET @query =            
       'INSERT INTO #tmp ( CompanyId,Branch ,ChargeValue, DueDate,OrderType)            
       select ord_cmpy_id as CompanyId,ord_ord_brh as Branch,chl_chrg_val * ' + @CurrenyRate + ' as ChargeValue,orl_due_to_dt as DueDate,cds_desc            
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
       ' + @DB + '_ortchl_rec ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec where            
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id            
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ          
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and  (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
       and  ord_ord_brh not in (''SFS'') and cds_desc in (''Normal Order'', ''Job Detail Order'')            
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'            
            
     print(@query);         
        EXECUTE sp_executesql @query;            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN             
            
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
                 
     IF (UPPER(@DB) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
  Else if (UPPER(@DB) = 'NO')       
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End              
    Else if(UPPER(@DB) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End              
                
     SET @sqltxt ='INSERT INTO #tmp ( CompanyId,Branch , ChargeValue, DueDate,OrderType)            
       select ord_cmpy_id as CompanyId,ord_ord_brh as Branch,chl_chrg_val * ' + @CurrenyRate + ' as ChargeValue, orl_due_to_dt as DueDate,cds_desc            
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
       ' + @DB + '_ortchl_rec ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec where            
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id            
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ          
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and  (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
       and  ord_ord_brh not in (''SFS'') and cds_desc in (''Normal Order'', ''Job Detail Order'')           
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'            
       print('test')  ;        
     print(@sqltxt);             
    set @execSQLtxt = @sqltxt;             
       EXEC (@execSQLtxt);            
   --and ord_ord_pfx=''SO''             
     END            
   -- SELECT  * FROM #tmp  
   --insert into #tmpFinal(LateOpenOrderCount ,OpenOrderCount)  
   --select (select COUNT(*)as thisweek from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)),120)   
   --and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)), 120)),  
   --(select COUNT(*)as thisweek from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0) ,120)   
   --and DueDate <= CONVERT(VARCHAR(10),dateadd(day, 0 - day(dateadd(month, 1 , GETDATE())), dateadd(month, 1 , GETDATE())), 120))  
   --;  
     
insert into #tmpFinal     
   select 'RED BACKLOG  
(6+ Days Late)'  as 'Category' ,   
   (select COUNT(*) from #tmp where DueDate >= @FDRed and DueDate <= @TDRed  
   ) as OpenOrderCount    
   , 1 as seq  
   Union     
   select 'NON-RESIDUAL RED BACKLOG  
(6+ days LATE)'  as 'Category' ,   
   (select COUNT(*) from #tmp where DueDate > @FDNR and DueDate <= @TDRed and ChargeValue >= 200  
   ) as OpenOrderCount    
   , 2 as seq  
   Union  
   select 'YELLOW BACKLOG  
(2-5 Days Late)'  as 'Category' ,   
   (select COUNT(*) from #tmp where DueDate >= @FDYel   
   and DueDate <= @TDYel  
   ) as OpenOrderCount    
   , 3 as seq  
  Union   
   select 'GREEN BACKLOG  
(1 Day Late)'  as 'Category' ,   
   (select COUNT(*) from #tmp where DueDate = @FDGre   
   ) as OpenOrderCount     
   , 4 as seq  
 Union   
   select 'TODAY'  as 'Category' ,   
   (select COUNT(*) from #tmp where DueDate = @FDTod   
   ) as OpenOrderCount     
   , 5 as seq  
     
   order by seq;  
     
select *, (select SUM(la.OpenOrderCount) from #tmpFinal la where seq != 5 ) as 'Late', (select SUM(la.OpenOrderCount) from #tmpFinal la ) as 'Open'  
from   #tmpFinal ;    
     
   drop table #tmp;      
   drop table #tmpFinal;     
END            
            
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@OrderType varchar(10)            
            
-- exec [sp_itech_OpenOrder] '01/02/2018', '12/31/2018' , 'US','ALL','ALL','ALL'       
-- exec [sp_itech_OOPS_OpenOrder_Backlog] 'UK', 'BHM'   
-- exec [sp_itech_OOPS_OpenOrder_Backlog] 'US', 'ROS'       
/*
Date:20200428
Mail sub:Home > STRATIXReports > Development > Warehouse Slides
*/
GO
