USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_sahsax]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Nov 26, 2019    
-- Description: <Description,,>    
-- =============================================    
create PROCEDURE [dbo].[TW_sahsax]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.TW_sahsax_rec', 'U') IS NOT NULL    
  drop table dbo.TW_sahsax_rec;    
        
            
SELECT *    
into  dbo.TW_sahsax_rec    
FROM [LIVETW_IW].[livetwstxdb_iw].[informix].[sahsax_rec];    
    
END    
-- select * from TW_sahsax_rec
GO
