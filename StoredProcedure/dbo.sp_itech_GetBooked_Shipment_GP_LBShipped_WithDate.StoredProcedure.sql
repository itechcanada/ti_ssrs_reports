USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped_WithDate]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <07 Jan 2015>
-- Description:	<Getting results using date filter>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped_WithDate] @DBNAME varchar(50),@Market varchar(100), @IncludePierce Char(1), @FromDate datetime, @ToDate datetime

AS
BEGIN
SET NOCOUNT ON;
				  	   
				IF @Market = 'ALL'
				 BEGIN
					 set @Market = ''
				 END
				
				
				declare @FD varchar(10)
				declare @TD varchar(10)

				set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
				set @TD = CONVERT(VARCHAR(10), @ToDate , 120) 
				
				
				IF @Market = ''
				 Begin
				     IF @DBNAME = 'ALL'
				    
						 begin	
							 IF @IncludePierce = '1'
								begin
									select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
									where (Category <> 'Interco' ) and item <> 'GP%' 
									--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
									and actvy_dt between @FD and @TD
									group by item,Category,actvy_dt
									UNION
									select item,Category,actvy_dt, CASE WHEN sum(tot_val) = 0 THEN 0 ELSE(sum(npft_avg_val)/ISNULL(sum(tot_val),0))*100 END  
									as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
									where (Category <> 'Interco' ) and item = 'GP%' 
									--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
									and actvy_dt between @FD and @TD
									group by item,Category,actvy_dt
									
									order by item,Category,actvy_dt
								End
								Else
								begin
									select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
									where (Category <> 'Interco' ) and item <> 'GP%' and Databases != 'PS' 
									--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
									and actvy_dt between @FD and @TD
									group by item,Category,actvy_dt
									UNION
									select item,Category,actvy_dt, CASE WHEN sum(tot_val) = 0 THEN 0 ELSE(sum(npft_avg_val)/ISNULL(sum(tot_val),0))*100 END  
									as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
									where (Category <> 'Interco' ) and item = 'GP%' and Databases != 'PS' 
									--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
									and actvy_dt between @FD and @TD
									group by item,Category,actvy_dt
									
									order by item,Category,actvy_dt
								End
						 End
						 ELSE
						 begin	
						 
							select item,Category,actvy_dt,sum(Value) as Value,databases from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
							where Databases= @DBNAME and (Category <> 'Interco' )
							--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
							and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt,databases
							order by item,Category,actvy_dt,databases
			         End	
				 End
				 ELSE IF @Market = 'ALLIND'
				 Begin
				     IF @DBNAME = 'ALL'
						 begin
							 IF @IncludePierce = '1'
								begin
								select item,'Industrial-All' as Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category in('Industrial','Motorsports','Recreational') and actvy_dt between @FD and @TD and item <> 'GP%'  
								group by item,actvy_dt
								UNION
								select item,'Industrial-All' as Category,actvy_dt,CASE WHEN sum(tot_val) = 0 THEN 0 ELSE (sum(npft_avg_val)/nullif(sum(tot_val),0))*100 END  
								as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category in('Industrial','Motorsports','Recreational') and actvy_dt between @FD and @TD and item = 'GP%'  
								group by item,actvy_dt
								order by item,Category,actvy_dt
							end
							else
							begin
								select item,'Industrial-All' as Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category in('Industrial','Motorsports','Recreational') and actvy_dt between @FD and @TD and item <> 'GP%'  and Databases != 'PS' 
								group by item,actvy_dt
								UNION
								select item,'Industrial-All' as Category,actvy_dt,CASE WHEN sum(tot_val) = 0 THEN 0 ELSE (sum(npft_avg_val)/nullif(sum(tot_val),0))*100 END  
								as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category in('Industrial','Motorsports','Recreational') and actvy_dt between @FD and @TD and item = 'GP%'  and Databases != 'PS' 
								group by item,actvy_dt
								order by item,Category,actvy_dt
							end
						 End
						 ELSE
						 begin
							select item,'Industrial-All' as Category,actvy_dt,sum(Value) as Value,databases from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
							where Databases= @DBNAME and Category in('Industrial','Motorsports','Recreational') and actvy_dt between @FD and @TD
							group by item,actvy_dt,databases
							order by item,Category,actvy_dt,databases
							print @FD;
							print @TD;
			         End	
				 End
				 Else
				 Begin
				     IF @DBNAME = 'ALL'
						 begin
							IF @IncludePierce = '1'
								begin
								select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category IN (@Market) and actvy_dt between @FD and @TD and item <> 'GP%'  
								group by item,Category,actvy_dt
								UNION
								select item,Category,actvy_dt,CASE WHEN sum(tot_val) = 0 THEN 0 ELSE (sum(npft_avg_val)/nullif(sum(tot_val),0))*100 END  
								as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category IN (@Market) and actvy_dt between @FD and @TD and item = 'GP%'  
								group by item,Category,actvy_dt
								
								order by item,Category,actvy_dt
							End
							Else
							Begin
								select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category IN (@Market) and actvy_dt between @FD and @TD and item <> 'GP%' and Databases != 'PS' 
								group by item,Category,actvy_dt
								UNION
								select item,Category,actvy_dt,CASE WHEN sum(tot_val) = 0 THEN 0 ELSE (sum(npft_avg_val)/nullif(sum(tot_val),0))*100 END  
								as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
								where Category IN (@Market) and actvy_dt between @FD and @TD and item = 'GP%'  and Databases != 'PS' 
								group by item,Category,actvy_dt
								
								order by item,Category,actvy_dt
							End
						 End
						 ELSE
						 begin
							select item,Category,actvy_dt,sum(Value) as Value,databases from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
							where Databases= @DBNAME and Category IN (@Market) and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt,databases
							order by item,Category,actvy_dt,databases
							print @FD;
							print @TD;
			         End	
				 End
				
			
 
END

-- exec sp_itech_GetBooked_Shipment_GP_LBShipped_GP_20Nov  'US','ALLIND','0' 
GO
