USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTop40Customer_WithEmail_Lastyear]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  <Mukesh >                
-- Create date: <29 Sep 2015>                
-- Description: <Getting top 40 customers for SSRS reports>                
-- =============================================                
CREATE PROCEDURE [dbo].[sp_itech_GetTop40Customer_WithEmail_Lastyear] @DBNAME varchar(50),@Location int, @ReportType int,@Market varchar(65),@Branch varchar(10),@version char = '0'                
                
AS                
BEGIN                
              
                 
 SET NOCOUNT ON;                
declare @sqltxt varchar(6000)                
declare @execSQLtxt varchar(7000)                
declare @DB varchar(100)                
declare @FD varchar(10)                
declare @TD varchar(10)                
                
set @DB=  @DBNAME                 
                
-- unCommented by mukesh 20200211 for reason to show data 2019 versus 2020  -- Commented start                
--IF (@ReportType=1)                
--  BEGIN                
--    set @FD = CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0), 120)  --FirstDateOfPreviousYear                
--    set @TD = CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)), 120)  --LastdateOFPreviousYear                
--  End                
--  Else                
--  BEGIN                
--    set @FD = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear                
--    set @TD = CONVERT(VARCHAR(10),GETDATE(), 120)  --TodayOFCurrentYear                
--  End                
              
  -- UnCommented End               
             
 --  commented start by mukesh 20200211 (for last year data - January activity)           
  IF (@ReportType=1)                
  BEGIN                
    set @FD = CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -2, GETDATE())), 0), 120)  --FirstDateOfPreviousYear                
    set @TD = CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, -1, DATEADD(YEAR, -2, GETDATE())) + 0, 0)), 120)  --LastdateOFPreviousYear                
  End                
  Else                
  BEGIN                
   set @FD =  CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0), 120)  --FirstDateOfPreviousYear                
    set @TD =  CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)), 120)  --LastdateOFPreviousYear                
   End             
   -- commented end               
                    
                
print(@FD)                
print(@TD)                
CREATE TABLE #tmp1 (    CustID   VARCHAR(10)                
        , CustName     VARCHAR(65)                
        , Market   VARCHAR(65)                 
        , Branch   VARCHAR(65)                 
                    , TotalValue    DECIMAL(20, 2)                
        , NProfitPercentage               DECIMAL(20, 2)                
        , TotalMatValue    DECIMAL(20, 2)                
        , GProfitPercentage    DECIMAL(20, 2)                
        , Databases   VARCHAR(15)                
        ,Cry           Varchar(5)                
        , TotalNetProfitValue    DECIMAL(20, 2)                
        , TotalMatProfitValue    DECIMAL(20, 2)                 
        ,ContactAdd  Varchar(100)                
        ,CustEmail Varchar(100)               
        ,SalesPersonIs Varchar(10)                
        ,SalesPersonOs Varchar(10)                       
                   );                 
                
DECLARE @DatabaseName VARCHAR(35);                
DECLARE @Prefix VARCHAR(5);                
DECLARE @Name VARCHAR(15);                
                
if @Market ='ALL'                
 BEGIN                
 set @Market = ''                
 END                
                
if @Branch ='ALL'                 BEGIN                
 set @Branch = ''                
 END                
                
DECLARE @CurrenyRate varchar(15);                
                
IF @DBNAME = 'ALL'                
 BEGIN     
 IF @version = '0'                
  BEGIN                
  DECLARE ScopeCursor CURSOR FOR                
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                
 OPEN ScopeCursor;                
  END                
  ELSE                
  BEGIN                
  DECLARE ScopeCursor CURSOR FOR                
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                 
    OPEN ScopeCursor;                
  END                
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
     WHILE @@FETCH_STATUS = 0                
       BEGIN                
      SET @DB= @Prefix                
     -- print(@DB)                
        DECLARE @query NVARCHAR(4000);                
         IF (UPPER(@Prefix) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@Prefix) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@Prefix) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@Prefix) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@Prefix) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End                
    Else if(UPPER(@Prefix) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End                
                        
        SET @query = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry, TotalNETProfitValue, TotalMatProfitValue,ContactAdd,CustEmail, SalesPersonIs,SalesPersonOs)               
       select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh as Branch,'                
      if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                  
      BEGIN                
      SET @query= @query +   '    SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue, CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage,                 
            SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,                 
            AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,                
             SUM(stn_npft_avg_val * '+ @CurrenyRate +') as TotalNETProfitValue,                
            SUM(stn_mpft_avg_val * '+ @CurrenyRate +')  as TotalMatProfitValue,                
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else                
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else                
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else                
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end                  
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = stn_sld_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),                
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = stn_sld_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail                    
            ,sis.slp_lgn_id, os.slp_lgn_id              
                          
                            
            from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                
            JOIN '+ @DB +'_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                
            LEFT JOIN '+ @DB +'_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                
        LEFT JOIN '+ @DB +'_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp               
                         
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
            Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''                    
                            
                           
            where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''                 
            and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                
            and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'                
            --if @Market =''                
            --BEGIN                
            -- SET @query= @query + ' and cuc_desc30 <> ''Interco'''                
            --END                
                            
      END                
      Else                
      BEGIN                
      SET @query= @query +   ' SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,                 
            CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage,                 
            SUM(stn_tot_mtl_val * '+ @CurrenyRate +')  as TotalMatValue ,                 
            AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,                
            SUM(stn_npft_avg_val * '+ @CurrenyRate +')  as TotalNETProfitValue,                 
            SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as TotalMatProfitValue,                
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else                
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else                
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else                
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end                  
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = stn_sld_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),                
             IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = stn_sld_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail              
             ,sis.slp_lgn_id, os.slp_lgn_id              
                          
                            
            from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                
            JOIN '+ @DB +'_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                
            LEFT JOIN '+ @DB +'_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                
        LEFT JOIN '+ @DB +'_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp               
                         
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
            Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''              
                           
            where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''                 
            and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                
            and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'                
            --if @Market =''                
            --BEGIN                
            -- SET @query= @query + ' and cuc_desc30 <> ''Interco'''                
            --END                
       END                 
     IF (@Location=1)                
      BEGIN                
      SET @query= @query +   ' group by stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_cry,crx_xexrt  ,sis.slp_lgn_id, os.slp_lgn_id              
                           order by TotalValue desc '                
      END                
      Else                
      BEGIN                
      SET @query= @query +   ' group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt ,sis.slp_lgn_id, os.slp_lgn_id                 
                           order by TotalValue desc '                              
      END                
                       
     print(@query)                  
        EXECUTE sp_executesql @query;                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
       END                 
    CLOSE ScopeCursor;                
    DEALLOCATE ScopeCursor;                
  END                
  ELSE                
     BEGIN                 
       print 'starting' ;                
      IF (UPPER(@DBNAME) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@DBNAME) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@DBNAME) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@DBNAME) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@DBNAME) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End                
    Else if(UPPER(@DBNAME) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End                
    Else if(UPPER(@DBNAME) = 'TWCN')                
    begin                
       SET @DB ='TW'                
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
     print 'Ending';                
     print @CurrenyRate ;                
     if (UPPER(@DBNAME) = 'TWCN')                
                   Set @Name='Taiwan - China'                
              else IF @version = '0'                
     BEGIN                
                   Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')                
                   END                
                   ELSE                
                   BEGIN                
                   Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')                
                   END                
                                   
                   print ('select Name from tbl_itech_DatabaseName where Prefix='''+ @DBNAME + '''')                
                     
     --and cva_cty <> ''CHN''                
     --if  (UPPER(@DBNAME) = 'TW')                
     --       BEGIN                
     --  SET @sqltxt= @sqltxt +   ' inner join ' + @DB + '_scrcva_rec on cva_cmpy_id = stn_cmpy_id                 
     --       and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = stn_sld_cus_id  and cva_addr_no = 0                 
     --        and cva_addr_typ = ''L'' and CVA_REF_PFX=''CU'''                
     --       END                
     --       Else                 
      print 'Before 1';                
     SET @sqltxt = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry, TotalNETProfitValue, TotalMatProfitValue, ContactAdd,CustEmail  , SalesPersonIs,SalesPersonOs)              
 
       select  stn_sld_cus_id  as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,case when ('''+@DBNAME +''' = ''TWCN'') then ''TAI - SHA'' else stn_shpt_brh end as Branch,'                
     if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' or UPPER(@DBNAME) = 'TWCN')                   
      BEGIN                
      SET @sqltxt= @sqltxt +   ' SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue, CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage,                 
            SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,                 
            AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,                
            SUM(stn_npft_avg_val * '+ @CurrenyRate +') as TotalNETProfitValue,                 
            SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as TotalMatProfitValue,                
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else                
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else                
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else                
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end                  
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = stn_sld_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),                
             IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = stn_sld_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail                 
              ,sis.slp_lgn_id, os.slp_lgn_id              
                          
                            
            from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                
            JOIN '+ @DB +'_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                
            LEFT JOIN '+ @DB +'_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                
        LEFT JOIN '+ @DB +'_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp               
                         
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
            Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''              
            '                
                           
            if (UPPER(@DBNAME) = 'TWCN')                
            BEGIN                
       SET @sqltxt= @sqltxt +   ' inner join ' + @DB + '_scrcva_rec on cva_cmpy_id = stn_cmpy_id                 
            and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = stn_sld_cus_id  and cva_addr_no = 0                 
            and cva_cty = ''CHN'' and cva_addr_typ = ''L'' and CVA_REF_PFX=''CU'''                
            END                                          
       SET @sqltxt= @sqltxt +   ' where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''                
            and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                
            and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'                
                            
            --if @Market =''                
            --BEGIN                
            -- SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''                
            --END                
      END                
      Else                
                       
   -- print 'Before 3';                
     -- print(@sqltxt)                
      BEGIN                
      print 'Before 4' + @CurrenyRate + @Name ;                
      SET @sqltxt= @sqltxt +   ' SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,                 
            CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage,                 
            SUM(stn_tot_mtl_val * '+ @CurrenyRate +')  as TotalMatValue ,                 
            AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry,                
            SUM(stn_npft_avg_val * '+ @CurrenyRate +')  as TotalNETProfitValue,                 
            SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as TotalMatProfitValue,                
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else                
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else                
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else                
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end                  
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = stn_sld_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),                
             IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = stn_sld_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail                   
              ,sis.slp_lgn_id, os.slp_lgn_id              
                          
                            
            from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id                
            JOIN '+ @DB +'_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                
            LEFT JOIN '+ @DB +'_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                
        LEFT JOIN '+ @DB +'_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp                
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
            Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''               
                             
            where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''                 
            and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                
            and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')'                
    --if @Market =''                
            --BEGIN                
            -- SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''                
            --END                
       END                
       print 'Before 5';                 
     --   print('A:' + @sqltxt)                
       print 'Before 6';                
                    
    IF (@Location=1)                
      BEGIN                
      SET @sqltxt= @sqltxt +   ' group by stn_shpt_brh, stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_cry,crx_xexrt , os.slp_lgn_id, sis.slp_lgn_id              
                           order by TotalValue desc '                
      END                
      Else                
      BEGIN                
      SET @sqltxt= @sqltxt +   ' group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt, os.slp_lgn_id, sis.slp_lgn_id                
                           order by TotalValue desc '                
                      
      END                
  print('B:' + @sqltxt)                   
  -- print(@sqltxt)                
  set @execSQLtxt = @sqltxt;                 
  EXEC (@execSQLtxt);                
     END                
                   
                   
   if @Market =''                
 BEGIN                
 SET @sqltxt=  '                 
     SELECT  CustID, CustName,                
     CustEmail,                
     Market,Branch,SUM(TotalValue) as TotalValue, SUM(TotalNETProfitValue)/nullif(SUM(TotalValue), 0) * 100 as NProfitPercentage,                
  sUM(TotalMatValue) as TotalMatValue,SUM(TotalMatProfitValue)/nullif(SUM(TotalMatValue), 0) * 100 as GProfitPercentage,Databases,'''' as Cry,ContactAdd, SalesPersonIs,SalesPersonOs FROM #tmp1                 
   where (Market <> ''Interco'' OR Market is Null) '      
if(@DB = 'US' AND @Branch = '')      
BEGIN      
SET @sqltxt=  @sqltxt + ' and  Branch in (''ROS'',''JAC'',''DET'',''WDL'',''HIB'',''SEA'',''LAX'') '         
END                   
  SET @sqltxt=  @sqltxt + '                
  group by CustID, CustName,Market,Branch,Databases,ContactAdd,CustEmail , SalesPersonIs,SalesPersonOs               
  order by TotalValue desc;'                
 END                
 Else                
 Begin                
 SET @sqltxt=  '                 
  SELECT  CustID, CustName,                 
CustEmail,                  
Market,Branch,SUM(TotalValue) as TotalValue, SUM(TotalNETProfitValue)/nullif(SUM(TotalValue), 0) * 100 as NProfitPercentage,                
  sUM(TotalMatValue) as TotalMatValue,SUM(TotalMatProfitValue)/nullif(SUM(TotalMatValue), 0) * 100 as GProfitPercentage,Databases,'''' as Cry,ContactAdd,      
   SalesPersonIs,SalesPersonOs FROM #tmp1 '      
if(@DB = 'US' AND @Branch = '')      
BEGIN      
SET @sqltxt=  @sqltxt + ' Where  Branch in (''ROS'',''JAC'',''DET'',''WDL'',''HIB'',''SEA'',''LAX'') '         
END                   
  SET @sqltxt=  @sqltxt + ' group by CustID, CustName,Market,Branch,Databases,ContactAdd,CustEmail, SalesPersonIs,SalesPersonOs                
  order by TotalValue desc;'                
 End                
                 
                    
  print(@sqltxt)                
  set @execSQLtxt = @sqltxt;                 
  EXEC (@execSQLtxt);                
   drop table #tmp1                
END                
                
-- exec [sp_itech_GetTop40Customer_WithEmail] 'ALL',1,0,'ALL','ALL' ---Privious                 
-- exec sp_itech_GetTop40Customer_WithEmail 'US',1,0,'ALL','ALL'       --- current                 
-- SELECT  CustID, CustName,Market,Branch,SUM(TotalValue) as TotalValue,avg(NProfitPercentage) as NProfitPercentage,   
--sUM(TotalMatValue) as TotalMatValue,AVG(GProfitPercentage) as GProfitPercentage,Databases,'' as 'Cry' FROM #tmp1                 
--group by CustID, CustName,Market,Branch,Databases                
--order by CustID,TotalValue desc;                
--SELECT    CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry                
   --FROM (SELECT                
   -- ROW_NUMBER() OVER ( PARTITION BY Branch ORDER BY TotalValue DESC ) AS 'RowNumber',                
   --CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry                
   -- FROM #tmp                
   -- ) dt                
   -- WHERE RowNumber <= 40                
 /*               
Date 20170116            
Sub:Regarding Monthly Reactivated Activity By Branch and Monthly New Account Activity By Branch report            
uncomment the line    where (Market <> ''Interco'' OR Market is Null)             
            
20170117            
sub: Top 40 Report            
            
20170207            
sub: Top 40 Report           
        
20200211        
sub:Top 40 accounts        
      
20200417      
Mail sub:Top 40 Accounts for US Sales Report     
20200118 Sumit  
Comment Current year condition and uncomment last year - jan activity  
Mail: Fwd: FW: TOP 40 Accounts  
*/
GO
