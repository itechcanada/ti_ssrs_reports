USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenPoDetail_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                    
-- Author:  <Sumit>                    
-- Create date: <14 July 2020>                    
-- Description: <Getting All Open PO Detail>                    
-- =============================================    
CREATE proc [dbo].[sp_itech_OpenPoDetail_V1]
(  
 @DBNAME varchar(50)  
)  
as   
begin  
SET NOCOUNT ON;    
DECLARE @query1 nvarchar(max)    
DECLARE @query2 nvarchar(max)    
DECLARE @DB varchar(100)                                                     
DECLARE @DatabaseName VARCHAR(35)                                                      
DECLARE @Prefix VARCHAR(35)                         
DECLARE @Name VARCHAR(15)          
  
CREATE TABLE #temp1 (       
 DBName varchar(10), 
 poh_cmpy_id varchar(10),
 poh_po_brh varchar(10),  
 whs_mng_brh varchar(10),  
 poh_ven_id varchar(10),  
 nad_nm1 varchar(150),  
 poh_shp_fm int,  
 pod_po_pfx varchar(5),  
 pod_po_no varchar(10),  
 pod_po_itm int,  
 prd_desc1 varchar(150),  
 prd_desc2 varchar(50),  
 pod_po_dist int,   
 poh_po_pl_dt date,  
 poh_byr varchar(10),  
 poh_mil varchar(10),  
 por_mil_ord_no varchar(50),  
 por_roll_sch varchar(50),  
 poi_ord_wgt_um varchar(5),  
 poi_ord_qty int,  
 poi_bal_qty int,  
 poi_ord_wgt int,  
 poi_bal_wgt int,  
 poi_pur_cat varchar(5),  
 pod_arr_to_dt date,  
 pod_shp_to_whs varchar(5),  
 bal_pcs int,  
 bal_msr int,  
 bal_wgt int,  
 bal_qty int,  
 pod_lnd_cst numeric(13,4),  
 arr_year int,  
 arr_month int,  
 po_month int,  
 pum varchar(5),  
 quantity_display_um varchar(5),  
 mat_cst_dcml int,  
 prm_pcs_ctl int,  
 frm_invt_ctl varchar(5),  
 frm_non_invt int,  
 por_po_intl_rls_no int,  
 por_due_fm_dt date,  
 latepcs int,  
 latewgt int,  
 lateqty int,  
 currpcs int,  
 currwgt int,
 currqty int
 );
 CREATE TABLE #temp2 (       
 DBName varchar(10),     
 pod_cmpy_id varchar(10),
 pod_po_pfx varchar(5),  
 pod_po_no varchar(10),  
 pod_po_itm int,  
 janpcs int,  
 janwgt int,  
 janqty int,  
 febpcs int,  
 febwgt int,  
 febqty int,  
 marpcs int,  
 marwgt int,  
 marqty int,  
 aprpcs int,  
 aprwgt int,  
 aprqty int,  
 maypcs int,  
 maywgt int,  
 mayqty int,  
 junepcs int,  
 junewgt int,  
 juneqty int,  
 julypcs int,  
 julywgt int,  
 julyqty int,  
 augpcs int,  
 augwgt int,  
 augqty int,  
 septpcs int,  
 septwgt int,  
 septqty int,  
 octpcs int,  
 octwgt int,  
 octqty int,  
 novpcs int,  
 novwgt int,  
 novqty int,  
 decpcs int,  
 decwgt int,  
 decqty int,  
  );  
  
set @DB=  @DBNAME    
  
 IF @DBNAME = 'ALL'   
 BEGIN                                                        
  DECLARE ScopeCursor CURSOR FOR                                            
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                             
  OPEN ScopeCursor;                                            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                  
  WHILE @@FETCH_STATUS = 0                                                        
  BEGIN                                                        
   SET @DB= @Prefix  
  
   set @query1 = 'insert into #temp1 select ''' + @DB + ''' as db_name, poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,pod_po_no ,pod_po_itm , mat_extd_desc as prd_desc1, prm_shrt_size_desc as prd_desc2,
	pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,
	poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,(pod_ord_pcs - pod_rcvd_pcs ) as bal_pcs,(pod_ord_msr - pod_rcvd_msr ) bal_msr,
	(pod_ord_wgt - pod_rcvd_wgt ) as bal_wgt,(pod_ord_qty - pod_rcvd_qty ) as bal_qty ,pod_lnd_cst ,
	((YEAR (pod_arr_to_dt )) as arr_year ,(MONTH (pod_arr_to_dt )) as arr_month ,
	((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) as po_month ,
	CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_disp_um  ELSE mat_m_qty_disp_um  END as PUM,
	CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_cst_um  ELSE mat_m_qty_cst_um  END as quantity_display_UM ,
	mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_pcs else 0 end as LatePcs,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_wgt else 0 end as LateWgt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_qty else 0 end as Lateqty,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_pcs else 0 end as CurrPcs,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_wgt else 0 end as CurrWgt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_qty else 0 end as Currqty
	--into #temp1
	from ' + @DB + '_tctnad_rec , ' + @DB + '_potpoh_rec, ' + @DB + '_potpoi_rec , ' + @DB + '_potpod_rec, ' + @DB + '_scrwhs_rec, 
	' + @DB + '_tctipd_rec, ' + @DB + '_inrprm_rec, ' + @DB + '_inrmat_rec, ' + @DB + '_inrfrm_rec, ' + @DB + '_potpor_rec
	where 
	nad_cmpy_id = poh_cmpy_id and nad_ref_pfx = poh_po_pfx and nad_ref_no = poh_po_no and nad_ref_itm =0 and nad_addr_typ=''B'' 
	and poh_cmpy_id = poi_cmpy_id and poh_po_pfx = poi_po_pfx and poh_po_no = poi_po_no 
	and poi_cmpy_id = pod_cmpy_id and poi_po_pfx = pod_po_pfx and poi_po_no = pod_po_no and poi_po_itm = pod_po_itm and pod_trcomp_sts != ''C''
	and pod_cmpy_id = whs_cmpy_id and pod_shp_to_whs = whs_whs
	and poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm
	and ipd_frm = prm_frm and ipd_grd = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh
	and prm_frm = mat_frm and prm_grd = mat_grd
	and ipd_frm = frm_frm
	and poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm and pod_po_intl_rls_no = por_po_intl_rls_no'
	print @query1;
	EXECUTE sp_executesql @query1;
  set @query2 =
	  'insert into #temp2 select ''' + @DB + ''' as db_name, pod_cmpy_id,pod_po_pfx, pod_po_no, pod_po_itm,
	case when MONTH(pod_arr_to_dt) = 1 then pod_bal_pcs else 0 end as JanPcs,  
	case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_wgt as int) else 0 end as JanWgt,  
	case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_qty as int) else 0 end as Janqty,  
	case when MONTH(pod_arr_to_dt) = 2 then pod_bal_pcs else 0 end as FebPcs,  
	case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_wgt as int) else 0 end as FebWgt,  
	case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_qty as int) else 0 end as Febqty,  
	case when MONTH(pod_arr_to_dt) = 3 then pod_bal_pcs else 0 end as MarPcs,  
	case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_wgt as int) else 0 end as MarWgt,  
	case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_qty as int) else 0 end as Marqty,  
	case when MONTH(pod_arr_to_dt) = 4 then pod_bal_pcs else 0 end as AprPcs,  
	case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_wgt as int) else 0 end as AprWgt,  
	case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_qty as int) else 0 end as Aprqty,  
	case when MONTH(pod_arr_to_dt) = 5 then pod_bal_pcs else 0 end as MayPcs,  
	case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_wgt as int) else 0 end as MayWgt,  
	case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_qty as int) else 0 end as Mayqty,  
	case when MONTH(pod_arr_to_dt) = 6 then pod_bal_pcs else 0 end as JunePcs,  
	case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_wgt as int) else 0 end as JuneWgt,  
	case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_qty as int) else 0 end as Juneqty,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_pcs else 0 end as JulyPcs,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_wgt else 0 end as JulyWgt,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_qty else 0 end as Julyqty,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_pcs else 0 end as AugPcs,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_wgt else 0 end as AugWgt,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_qty else 0 end as Augqty,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_pcs else 0 end as SeptPcs,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_wgt else 0 end as SeptWgt,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_qty else 0 end as Septqty,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_pcs else 0 end as OctPcs,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_wgt else 0 end as OctWgt,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_qty else 0 end as Octqty,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_pcs else 0 end as NovPcs,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_wgt else 0 end as NovWgt,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_qty else 0 end as Novqty,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_pcs else 0 end as DecPcs,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_wgt else 0 end as DecWgt,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_qty else 0 end as Decqty
	--into #temp2
	from '+ @DB +'_potpod_rec where pod_trcomp_sts !=''C'''  
  print @query2
  EXECUTE sp_executesql @query2;                                                         
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                        
  END                                                         
  CLOSE ScopeCursor;                                                        
  DEALLOCATE ScopeCursor;                                          
 END    
 else  
 Begin  
  set @query1 = 'insert into #temp1 select ''' + @DB + ''' as dbname, poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,pod_po_no ,pod_po_itm , mat_extd_desc as prd_desc1, prm_shrt_size_desc as prd_desc2,
	pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,
	poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,(pod_ord_pcs - pod_rcvd_pcs ) as bal_pcs,(pod_ord_msr - pod_rcvd_msr ) bal_msr,
	(pod_ord_wgt - pod_rcvd_wgt ) as bal_wgt,(pod_ord_qty - pod_rcvd_qty ) as bal_qty ,pod_lnd_cst ,
	(YEAR (pod_arr_to_dt )) as arr_year ,(MONTH (pod_arr_to_dt )) as arr_month ,
	((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) as po_month ,
	CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_disp_um  ELSE mat_m_qty_disp_um  END as PUM,
	CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_cst_um  ELSE mat_m_qty_cst_um  END as quantity_display_UM ,
	mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_pcs else 0 end as LatePcs,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_wgt else 0 end as LateWgt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_qty else 0 end as Lateqty,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_pcs else 0 end as CurrPcs,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_wgt else 0 end as CurrWgt,
	case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_qty else 0 end as Currqty
	--into #temp1
	from ' + @DB + '_tctnad_rec , ' + @DB + '_potpoh_rec, ' + @DB + '_potpoi_rec , ' + @DB + '_potpod_rec, ' + @DB + '_scrwhs_rec, 
	' + @DB + '_tctipd_rec, ' + @DB + '_inrprm_rec, ' + @DB + '_inrmat_rec, ' + @DB + '_inrfrm_rec, ' + @DB + '_potpor_rec
	where 
	nad_cmpy_id = poh_cmpy_id and nad_ref_pfx = poh_po_pfx and nad_ref_no = poh_po_no and nad_ref_itm =0 and nad_addr_typ=''B'' 
	and poh_cmpy_id = poi_cmpy_id and poh_po_pfx = poi_po_pfx and poh_po_no = poi_po_no 
	and poi_cmpy_id = pod_cmpy_id and poi_po_pfx = pod_po_pfx and poi_po_no = pod_po_no and poi_po_itm = pod_po_itm and pod_trcomp_sts != ''C''
	and pod_cmpy_id = whs_cmpy_id and pod_shp_to_whs = whs_whs
	and poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm
	and ipd_frm = prm_frm and ipd_grd = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh
	and prm_frm = mat_frm and prm_grd = mat_grd
	and ipd_frm = frm_frm
	and poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm and pod_po_intl_rls_no = por_po_intl_rls_no'
	print @query1;
	EXECUTE sp_executesql @query1;
  set @query2 =
	  'insert into #temp2 select ''' + @DB + ''' as dbname, pod_cmpy_id,pod_po_pfx, pod_po_no, pod_po_itm,
	case when MONTH(pod_arr_to_dt) = 1 then pod_bal_pcs else 0 end as JanPcs,  
	case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_wgt as int) else 0 end as JanWgt,  
	case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_qty as int) else 0 end as Janqty,  
	case when MONTH(pod_arr_to_dt) = 2 then pod_bal_pcs else 0 end as FebPcs,  
	case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_wgt as int) else 0 end as FebWgt,  
	case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_qty as int) else 0 end as Febqty,  
	case when MONTH(pod_arr_to_dt) = 3 then pod_bal_pcs else 0 end as MarPcs,  
	case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_wgt as int) else 0 end as MarWgt,  
	case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_qty as int) else 0 end as Marqty,  
	case when MONTH(pod_arr_to_dt) = 4 then pod_bal_pcs else 0 end as AprPcs,  
	case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_wgt as int) else 0 end as AprWgt,  
	case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_qty as int) else 0 end as Aprqty,  
	case when MONTH(pod_arr_to_dt) = 5 then pod_bal_pcs else 0 end as MayPcs,  
	case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_wgt as int) else 0 end as MayWgt,  
	case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_qty as int) else 0 end as Mayqty,  
	case when MONTH(pod_arr_to_dt) = 6 then pod_bal_pcs else 0 end as JunePcs,  
	case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_wgt as int) else 0 end as JuneWgt,  
	case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_qty as int) else 0 end as Juneqty,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_pcs else 0 end as JulyPcs,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_wgt else 0 end as JulyWgt,
	case when MONTH(pod_arr_to_dt) = 7 then pod_bal_qty else 0 end as Julyqty,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_pcs else 0 end as AugPcs,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_wgt else 0 end as AugWgt,
	case when MONTH(pod_arr_to_dt) = 8 then pod_bal_qty else 0 end as Augqty,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_pcs else 0 end as SeptPcs,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_wgt else 0 end as SeptWgt,
	case when MONTH(pod_arr_to_dt) = 9 then pod_bal_qty else 0 end as Septqty,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_pcs else 0 end as OctPcs,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_wgt else 0 end as OctWgt,
	case when MONTH(pod_arr_to_dt) = 10 then pod_bal_qty else 0 end as Octqty,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_pcs else 0 end as NovPcs,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_wgt else 0 end as NovWgt,
	case when MONTH(pod_arr_to_dt) = 11 then pod_bal_qty else 0 end as Novqty,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_pcs else 0 end as DecPcs,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_wgt else 0 end as DecWgt,
	case when MONTH(pod_arr_to_dt) = 12 then pod_bal_qty else 0 end as Decqty
	--into #temp2
	from '+ @DB +'_potpod_rec where pod_trcomp_sts !=''C'''  
  print @query2
  EXECUTE sp_executesql @query2;  
 end  
  
 /* set @query1 = 'select * from #temp1 t1 left outer join #temp2 t2
	on t1.DBName = t2.DBName and t1.poh_cmpy_id = t2.pod_cmpy_id and t1.pod_po_pfx = t2.pod_po_pfx and t1.pod_po_no = t2.pod_po_no and t1.pod_po_itm = t2.pod_po_itm' 
	*/
set @query1 = 'select t1.*, t2.*, csi_bas_cry_val from #temp1 t1 left outer join #temp2 t2
	on t1.DBName = t2.DBName and t1.poh_cmpy_id = t2.pod_cmpy_id and t1.pod_po_pfx = t2.pod_po_pfx and t1.pod_po_no = t2.pod_po_no and t1.pod_po_itm = t2.pod_po_itm
	left join '+ @DB +'_cttcsi_rec  on csi_cmpy_id = t1.poh_cmpy_id and csi_ref_pfx = t1.pod_po_pfx and csi_ref_no = t1.pod_po_no and csi_ref_itm = t1.pod_po_itm and csi_cst_no=1	' 

	

print @query1
execute sp_executesql @query1;

drop table #temp1;
drop table #temp2;
end  
/*  
  
2020/07/14 Sumit  
SP created  
exec [dbo].[sp_itech_OpenPoDetail_V1] 'US'
  
*/  
GO
