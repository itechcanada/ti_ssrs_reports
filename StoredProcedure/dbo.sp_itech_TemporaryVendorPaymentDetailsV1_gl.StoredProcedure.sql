USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_TemporaryVendorPaymentDetailsV1_gl]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <mrinal >  
-- Create date: <8 aug 2015>  
-- Description: <Getting Receipt Details for a Voucher SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_TemporaryVendorPaymentDetailsV1_gl] @DBNAME varchar(50),  @VendorNo Varchar(20),@FromDate datetime, @ToDate datetime  
As  
Begin  



declare @DB varchar(100)  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @FD varchar(10)  
declare @TD varchar(10)  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
IF @VendorNo = 'ALL' OR @VendorNo = ''  
BEGIN  
SET @VendorNo = ''  
END  
  Set @VendorNo = RTRIM(LTRIM(@VendorNo));
  
CREATE TABLE #tmp (   [Database]   VARCHAR(10)  
        , VendorID    VARCHAR(8)  
        , VendorLongNM   VARCHAR(35)  
        , VendorTaxID   VARCHAR(15)  
        , RefYear    VARCHAR(4)  
        , OrigAmt     decimal(20,2)  
        , DiscAmt    decimal(20,2)  
        , GlAccount   VARCHAR(15) 
        ,VendorName VARCHAR(35)
        )  
          
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);     
       SET @query ='INSERT INTO #tmp ([Database], VendorID, VendorLongNM, VendorTaxID, RefYear, OrigAmt, DiscAmt,GlAccount, VendorName)
       Select [Database],	jvc_ven_id,	ven_ven_long_nm,ven_1099_tx_id,	[Year],Sum(VoucherAmt),sum(discAmt),MAX(gl_account)   as gl_account  ,ven_ven_nm  	
        from (  
       SELECT ''' +  @Prefix + ''' as [Database],  jvc_ven_id, ven_ven_long_nm, ven_1099_tx_id, Year(jvc_ent_dt) as [Year],  
         Sum(jvc_vchr_amt) as VoucherAmt, SUM(jvc_disc_amt) as discAmt ,ven_ven_nm 
         , 
	     	CASE WHEN(SELECT top 1 glj_bsc_gl_acct FROM ' + @Prefix + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 0) > 0
	     	THEN
	     	(SELECT top 1 glj_bsc_gl_acct FROM ' + @Prefix + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 0)
	     	ELSE 	
	     	(SELECT top 1 glj_bsc_gl_acct FROM ' + @Prefix + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 1)
	     	   	END
	     	as gl_account
          from ' + @Prefix + '_apjjvc_rec   
         join ' + @Prefix + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id and ven_ven_id = jvc_ven_id  
         where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''' )  
          group by jvc_ven_id,ven_ven_long_nm,ven_1099_tx_id,Year(jvc_ent_dt) , jvc_cmpy_id,jvc_vchr_pfx,jvc_vchr_no  ,ven_ven_nm 
          ) AS t1  group by [Database],	jvc_ven_id,	ven_ven_long_nm,ven_1099_tx_id,	[Year] ,ven_ven_nm 
          '         
    print @query;  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
        SET @sqltxt ='INSERT INTO #tmp ([Database], VendorID, VendorLongNM, VendorTaxID, RefYear, OrigAmt, DiscAmt,GlAccount, VendorName)  
        Select [Database],	jvc_ven_id,	ven_ven_long_nm,ven_1099_tx_id,	[Year],Sum(VoucherAmt),sum(discAmt),MAX(gl_account)   as gl_account ,ven_ven_nm 	
        from (
        SELECT ''' +  @DBNAME + ''' as [Database],  jvc_ven_id, ven_ven_long_nm, ven_1099_tx_id, Year(jvc_ent_dt) as [Year],  
         Sum(jvc_vchr_amt) as VoucherAmt, SUM(jvc_disc_amt) as discAmt, ven_ven_nm
         , 
	     	CASE WHEN(SELECT top 1 glj_bsc_gl_acct FROM ' + @DBNAME + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 0) > 0
	     	THEN
	     	(SELECT top 1 glj_bsc_gl_acct FROM ' + @DBNAME + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 0)
	     	ELSE 	
	     	(SELECT top 1 glj_bsc_gl_acct FROM ' + @DBNAME + '_apjglj_rec WHERE jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no
	     	AND glj_vchr_itm = 1)
	     	   	END
	     	as gl_account
         from ' + @DBNAME + '_apjjvc_rec  
         join ' + @DBNAME + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id and ven_ven_id = jvc_ven_id  
         where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''')   
         group by jvc_ven_id,ven_ven_long_nm,ven_1099_tx_id,Year(jvc_ent_dt), jvc_cmpy_id,jvc_vchr_pfx,jvc_vchr_no ,ven_ven_nm
         ) AS t1  group by [Database],	jvc_ven_id,	ven_ven_long_nm,ven_1099_tx_id,	[Year],ven_ven_nm
         '  
     print(@sqltxt)   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
   END  
   Select * from #tmp order by [Database],VendorID  
   Drop table #tmp  
End   
  
          
-- Exec [sp_itech_TemporaryVendorPaymentDetailsV1] 'US', '1011', '2015-01-01', '2015-12-31'  
-- Exec [sp_itech_TemporaryVendorPaymentDetailsV1] 'ALL', '1011', '2015-01-01', '2015-12-31'  
GO
