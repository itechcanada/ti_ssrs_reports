USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ipjssh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,01/25/2021,>    
-- Description: <Description,Production Recording Sales Orders Process Streams History,>  
-- =============================================    
create PROCEDURE [dbo].[DE_ipjssh] 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
Truncate table dbo.DE_ipjssh_rec ;     
    
-- Insert statements for procedure here    
insert into dbo.DE_ipjssh_rec  
SELECT *    
  from [LIVEDESTX].[livedestxdb].[informix].[ipjssh_rec] ;     
      
END    
GO
