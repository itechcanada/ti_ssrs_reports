USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_gltojd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,19/11/2015,>    
-- Description: <Description,Open Orders,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[UK_gltojd]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.UK_gltojd_rec', 'U') IS NOT NULL    
  drop table dbo.UK_gltojd_rec ;     
    
     
    -- Insert statements for procedure here    
SELECT ojd_oje_pfx,ojd_bsc_gl_acct, ojd_sacct, ojd_dr_amt, ojd_cr_amt,ojd_oje_no ,ojd_lgr_id    
into  dbo.UK_gltojd_rec    
  from [LIVEUKGL].[liveukgldb].[informix].[gltojd_rec] ;    
      
END    
    
-- Select * from UK_gltojd_rec 
GO
