USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReview]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      r
-- Author:  <Mukesh >      
-- Create date: <20 Dec 2016>      
-- Description: <Getting top 50 customers for SSRS reports>      
  
   
-- =============================================      
CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReview] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50)      
      
AS      
BEGIN     

 
       
 SET NOCOUNT ON;      
declare @sqltxt varchar(6000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(100)      
declare @FD varchar(10)      
declare @TD varchar(10)      
declare @NOOfCust varchar(15)      
DECLARE @ExchangeRate varchar(15)  

set @FD= CONVERT(VARCHAR(10), @FromDate, 120)
set @TD= CONVERT(VARCHAR(10), @ToDate, 120)
    
     if(@FromDate = '' OR @FromDate = null OR @FromDate = '1900-01-01' )
     begin
     set @FromDate = '2009-01-01'
      End
      
      if (@ToDate = '' OR @ToDate = null OR @ToDate = '1900-01-01')
      begin
      set @ToDate = CONVERT(VARCHAR(10), GETDATE() , 120)
      end
      
set @DB= @DBNAME -- UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'  
      
--Else      
--Begin       
--set @FD = CONVERT(VARCHAR(10), @FromDate , 120)      
--set @TD = CONVERT(VARCHAR(10), @ToDate , 120)      
--End      
      
      
CREATE TABLE #tmp (  
Databse varchar(10)      
        ,Form  varchar(35)      
        ,Grade  varchar(35)      
        ,Size  varchar(35)      
        ,Finish  varchar(35)     
        ,Sales  Decimal(20,2)  
        ,CostOfGoods  Decimal(20,2)  
        ,Margin Decimal(20,2)  
         ,BilledWeight Decimal(20,2)  
        ,CurrentInventoryVal Decimal(20,2)  
        ,CurrentInventory Decimal(20,2)  
        ,InventAtBegPer varchar(20)   
        ,InventAtEndPer varchar(20)   
        ,Beginning Decimal(20,2)  
        ,Ending Decimal(20,2)
        ,Product Varchar(200)  
                 );  
                 
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(5);      
DECLARE @Name VARCHAR(15);      
DECLARE @CurrenyRate varchar(15);       
           
      
IF @DBNAME = 'ALL'      
 BEGIN  
 
 DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor; 
    
             
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
        DECLARE @query nvarchar(max);         
      SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'      
      IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')              
    begin              
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End 
    Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End 
    
     if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK' )--UPPER(@DBNAME) = 'TW' OR     
   BEGIN 
     SET @sqltxt ='
     INSERT INTO #tmp (Databse,Product,Form,Grade,Size,Finish,Sales,CostOfGoods,Margin,BilledWeight,CurrentInventoryVal,CurrentInventory,InventAtBegPer,InventAtEndPer,Beginning, Ending)                  
     select ''' +  @DB + '''
      , (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DB + '_inrprm_rec 
where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product
     
     ,stn_Frm, stn_grd, stn_size,stn_fnsh, SUM(stn_tot_val) * '+ @CurrenyRate +' AS Sales, (SUM(stn_tot_val) - SUM(stn_npft_avg_val)) * '+ @CurrenyRate +' as CostOfGoods
,SUM(stn_npft_avg_val) * '+ @CurrenyRate +' as MArgin,SUM(stn_blg_wgt) *(2.20462) as BilledWeight,
(select (isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  ))* '+ @CurrenyRate +') as TotValNEW
from '+@DB+'_intprd_rec left join '+@DB+'_intacp_rec on prd_avg_cst_pool = acp_avg_cst_pool
where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S''   ) as CurrentInventoryVal


,(Select sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh
and prd_invt_sts = ''S'' ) as CurrentInventory

, '''' as InvAtBegPer
, '''' as InvAtEndPer
, (SELECT Sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and 
prd_fnsh = stn_fnsh and prd_invt_sts = ''S''  and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @FD + ''', 126)) AS BEGNING
, (SELECT Sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and 
prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @TD + ''', 126)) AS ENDING
 from '+@DB+'_sahstn_rec 
 Where  stn_inv_dt between  ''' + @FD + '''  And ''' + @TD + '''
Group by stn_frm,stn_grd,stn_size,stn_fnsh
     '
     End
     ELSE
     BEGIN
            
      SET @query =       '
      INSERT INTO #tmp (Databse,Product,Form,Grade,Size,Finish,Sales,CostOfGoods,Margin,BilledWeight,CurrentInventoryVal,CurrentInventory,InventAtBegPer,InventAtEndPer,Beginning, Ending)                  
     select ''' +  @DB + '''
     , (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DB + '_inrprm_rec 
where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product

     ,stn_Frm, stn_grd, stn_size,stn_fnsh, SUM(stn_tot_val) * '+ @CurrenyRate +' AS Sales, (SUM(stn_tot_val) - SUM(stn_npft_avg_val)) * '+ @CurrenyRate +' as CostOfGoods
,SUM(stn_npft_avg_val) * '+ @CurrenyRate +' as MArgin,SUM(stn_blg_wgt) as BilledWeight,
(select (isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  ))* '+ @CurrenyRate +') as TotValNEW
from '+@DB+'_intprd_rec left join '+@DB+'_intacp_rec on prd_avg_cst_pool = acp_avg_cst_pool
where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' ) as CurrentInventoryVal


,(Select sum(prd_ohd_wgt) from '+@DB+'_intprd_rec where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh
and prd_invt_sts = ''S'' ) as CurrentInventory

, '''' as InvAtBegPer
, '''' as InvAtEndPer
, (SELECT Sum(prd_ohd_wgt) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh 
and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @FD + ''', 126)) AS BEGNING
, (SELECT Sum(prd_ohd_wgt) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh 
and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @TD + ''', 126)) AS ENDING
 from  '+@DB+'_sahstn_rec 
 where  stn_inv_dt between  ''' + @FD + '''  And ''' + @TD + '''
Group by stn_frm,stn_grd,stn_size,stn_fnsh
      '
   END    
     print(@query);   
        EXECUTE sp_executesql @query;    
        
      
        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN       
     IF (UPPER(@DB) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DB) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DB) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DB) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DB) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End        
    Else if(UPPER(@DB) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End        
          if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK' )--UPPER(@DBNAME) = 'TW' OR     
   BEGIN 
     SET @sqltxt ='
     INSERT INTO #tmp (Databse,Product,Form,Grade,Size,Finish,Sales,CostOfGoods,Margin,BilledWeight,CurrentInventoryVal,CurrentInventory,InventAtBegPer,InventAtEndPer,Beginning, Ending)                  
     select ''' +  @DB + '''
      , (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DB + '_inrprm_rec 
where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product
     
     ,stn_Frm, stn_grd, stn_size,stn_fnsh, SUM(stn_tot_val) * '+ @CurrenyRate +' AS Sales, (SUM(stn_tot_val) - SUM(stn_npft_avg_val)) * '+ @CurrenyRate +' as CostOfGoods
,SUM(stn_npft_avg_val) * '+ @CurrenyRate +' as MArgin,SUM(stn_blg_wgt) *(2.20462) as BilledWeight,
(select (isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  ))* '+ @CurrenyRate +') as TotValNEW
from '+@DB+'_intprd_rec left join '+@DB+'_intacp_rec on prd_avg_cst_pool = acp_avg_cst_pool
where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' ) as CurrentInventoryVal


,(Select sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh
and prd_invt_sts = ''S'' ) as CurrentInventory

, '''' as InvAtBegPer
, '''' as InvAtEndPer
, (SELECT Sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and 
prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @FD + ''', 126)) AS BEGNING
, (SELECT Sum(prd_ohd_wgt) *(2.20462) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and 
prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @TD + ''', 126)) AS ENDING
 from '+@DB+'_sahstn_rec 
 Where  stn_inv_dt between  ''' + @FD + '''  And ''' + @TD + '''
Group by stn_frm,stn_grd,stn_size,stn_fnsh
     '
     End
     ELSE
     BEGIN
		SET @sqltxt ='
     INSERT INTO #tmp (Databse,Product,Form,Grade,Size,Finish,Sales,CostOfGoods,Margin,BilledWeight,CurrentInventoryVal,CurrentInventory,InventAtBegPer,InventAtEndPer,Beginning, Ending)                  
     select ''' +  @DB + '''
     , (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) from ' + @DB + '_inrprm_rec 
where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product

     ,stn_Frm, stn_grd, stn_size,stn_fnsh, SUM(stn_tot_val) * '+ @CurrenyRate +' AS Sales, (SUM(stn_tot_val) - SUM(stn_npft_avg_val)) * '+ @CurrenyRate +' as CostOfGoods
,SUM(stn_npft_avg_val) * '+ @CurrenyRate +' as MArgin,SUM(stn_blg_wgt) as BilledWeight,
(select (isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  ))* '+ @CurrenyRate +') as TotValNEW
from '+@DB+'_intprd_rec left join '+@DB+'_intacp_rec on prd_avg_cst_pool = acp_avg_cst_pool
where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S'' ) as CurrentInventoryVal


,(Select sum(prd_ohd_wgt) from '+@DB+'_intprd_rec where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh
and prd_invt_sts = ''S'' ) as CurrentInventory

, '''' as InvAtBegPer
, '''' as InvAtEndPer
, (SELECT Sum(prd_ohd_wgt) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh 
and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @FD + ''', 126)) AS BEGNING
, (SELECT Sum(prd_ohd_wgt) from '+@DB+'_intprd_rec_history where prd_frm = stn_frm and prd_grd = stn_grd and prd_size = stn_size and prd_fnsh = stn_fnsh 
and prd_invt_sts = ''S'' and convert(varchar(7),UpdateDtTm, 126) =  convert(varchar(7),''' + @TD + ''', 126)) AS ENDING
 from  '+@DB+'_sahstn_rec 
 where  stn_inv_dt between  ''' + @FD + '''  And ''' + @TD + '''
Group by stn_frm,stn_grd,stn_size,stn_fnsh
     '
     END
       print(@FD)  ;  
       print(@TD)  ;  
     print(@sqltxt);       
    set @execSQLtxt = @sqltxt;       
       EXEC (@execSQLtxt);      
   --and ord_ord_pfx=''SO''       
     END      
   SELECT distinct * FROM #tmp   
         
END      
      
      
-- exec [sp_itech_MonthlyInventoryReview] '11/01/2016', '11/30/2016' , 'US'
-- exec [sp_itech_MonthlyInventoryReview] '2017-01-01', '2017-05-01' , 'UK'
-- exec [sp_itech_MonthlyInventoryReview] '05/01/2017', '05/31/2017' , 'ALL'
-- exec [sp_itech_MonthlyInventoryReview] '05/01/2017', '05/31/2017' , 'US'
  

--select CONVERT(VARCHAR(10), '11/01/2016', 101)
GO
