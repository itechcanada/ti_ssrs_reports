USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Customer_Snap_Shot]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Mukesh>          
-- Create date: <23 Oct 2017>          
-- Description: <sp_itech_Customer_Snap_Shot>         
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_Customer_Snap_Shot]  @DBNAME varchar(3), @CustomerID Varchar(10), @Branch varchar(3), @CompareToYear char(1) = '3'      
AS          
BEGIN          
        
        
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
declare @DB varchar(100);          
declare @sqltxt varchar(6000);          
declare @execSQLtxt varchar(7000);          
DECLARE @CountryName VARCHAR(25);             
DECLARE @prefix VARCHAR(15);             
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @CurrenyRate varchar(15);            
DECLARE @StartDate VARCHAR(10);            
DECLARE @EndDate varchar(10);          
declare @LastDateToCompare DATE;      
          
CREATE TABLE #temp ( Dbname   VARCHAR(10)          
     ,CusID   VARCHAR(10)          
     ,ActvyDT  VARCHAR(10)       
     , TotalSlsVal decimal(20,2)       
     ,Wgt   decimal(20,2)           
     ,CusLongNm  VARCHAR(40)          
     ,Branch   VARCHAR(3)          
     --,Product  VARCHAR(500)          
     --,TotalMtlVal decimal(20,0)          
     --,Profit   decimal(20,2)          
     --, NetPct decimal(20,0)         
     --, NetAvgAmt decimal(20,0)         
     --,Market Varchar(35)         
     --,MtlAvgVal decimal(20,0)        
     );          
        
          
 --------  +++++  Created Table for last 24 months ++++++++          
Create TABLE #Last24Months (   StartDate varchar(15)          
                               ,EndDate  VARCHAR(15)          
                            );          
         
--declare @start DATE = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate())-1, 0) , 120);   
if(@CompareToYear='3')      
begin      
set @LastDateToCompare  = CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-12-01'  , 120);      
end           
else if(@CompareToYear='2')      
begin      
set @LastDateToCompare  = CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-12-01'  , 120);      
end      
else if( @CompareToYear='1')    
begin      
set @LastDateToCompare = CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-12-01'  , 120);      
end      
else     
begin      
set @LastDateToCompare = CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-12-01'  , 120);      
end      
      
declare @start DATE = @LastDateToCompare;      
 with CTEE(Last24Months)          
AS          
(          
    SELECT @start          
     UNION   all          
          
    SELECT DATEADD(month,-1,Last24Months)          
    from CTEE          
    where DATEADD(month,-1,Last24Months)>=DATEADD(month,-23,@start)          
)       
         
INSERT INTO #Last24Months(StartDate,EndDate)          
select Last24Months as StartDate,CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Last24Months)+1,0)), 120) as EndDate  from CTEE          
      
if @Branch ='ALL'                
 BEGIN                
 set @Branch = ''                
 END                                         
if @CustomerID ='ALL'                
 BEGIN                
 set @CustomerID = ''                
 END             
      
          
IF @DBNAME = 'ALL'          
 BEGIN          
           
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;          
  WHILE @@FETCH_STATUS = 0          
  BEGIN          
   DECLARE @query NVARCHAR(MAX);          
   IF (UPPER(@Prefix) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@Prefix) = 'NO')          
    begin     
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@Prefix) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@Prefix) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@Prefix) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@Prefix) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
              
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR           
    BEGIN          
    DECLARE ScopeCursor1 CURSOR FOR          
       select StartDate,EndDate from  #Last24Months          
        OPEN ScopeCursor1;          
        FETCH NEXT FROM ScopeCursor1 INTO @StartDate,@EndDate;          
         WHILE @@FETCH_STATUS = 0          
           BEGIN          
            DECLARE @query1 NVARCHAR(max)           
     SET @query1 = 'INSERT INTO #temp (Dbname,CusID,ActvyDT, TotalSlsVal,Wgt,CusLongNm,Branch)          
      SELECT '''+ @Prefix +''' as Country, stn_sld_cus_id, '''+ @StartDate +''', SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue,SUM(stn_blg_wgt)*(2.20462)      
      ,cus_cus_long_nm,STN_SHPT_BRH      
      FROM ' + @Prefix + '_sahstn_rec       
      join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
      where STN_INV_DT >= ''' + @StartDate +''' and STN_INV_DT <= ''' + @EndDate +'''      
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''      
      and (STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
      group by STN_SHPT_BRH,stn_sld_cus_id,cus_cus_long_nm '       
      print(@query1)          
         EXECUTE sp_executesql @query1;       
               
         FETCH NEXT FROM ScopeCursor1 INTO @StartDate,@EndDate;          
        END           
        CLOSE ScopeCursor1;          
        DEALLOCATE ScopeCursor1;          
    END          
    ELSE          
    BEGIN       
    DECLARE ScopeCursor1 CURSOR FOR          
       select StartDate,EndDate from  #Last24Months          
        OPEN ScopeCursor1;          
        FETCH NEXT FROM ScopeCursor1 INTO @StartDate,@EndDate;          
         WHILE @@FETCH_STATUS = 0          
           BEGIN          
            DECLARE @query2 NVARCHAR(max)           
     SET @query2 = 'INSERT INTO #temp (Dbname,CusID,ActvyDT, TotalSlsVal,Wgt,CusLongNm,Branch)          
      SELECT '''+ @Prefix +''' as Country, stn_sld_cus_id, '''+ @StartDate +''', SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue,SUM(stn_blg_wgt)      
   ,cus_cus_long_nm,STN_SHPT_BRH      
      FROM ' + @Prefix + '_sahstn_rec       
      join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
      where STN_INV_DT >= ''' + @StartDate +''' and STN_INV_DT <= ''' + @EndDate +'''      
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''      
      and (STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
      group by STN_SHPT_BRH,stn_sld_cus_id,cus_cus_long_nm '       
      print(@query2)          
         EXECUTE sp_executesql @query2;       
               
         FETCH NEXT FROM ScopeCursor1 INTO @StartDate,@EndDate;          
        END           
        CLOSE ScopeCursor1;          
        DEALLOCATE ScopeCursor1;           
    END          
   print @query;          
   EXECUTE sp_executesql @query;          
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;          
  END             
 CLOSE ScopeCursor;          
  DEALLOCATE ScopeCursor;          
 END          
ELSE          
BEGIN          
 IF (UPPER(@DBNAME) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@DBNAME) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@DBNAME) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@DBNAME) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@DBNAME) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@DBNAME) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
    Else if(UPPER(@DBNAME) = 'TWCN')          
    begin          
       SET @DB ='TW'          
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
              
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )      
   BEGIN        
    DECLARE ScopeCursor CURSOR FOR          
       select StartDate,EndDate from  #Last24Months          
        OPEN ScopeCursor;          
        FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
         WHILE @@FETCH_STATUS = 0          
           BEGIN          
            DECLARE @query3 NVARCHAR(max)        
   SET @query3 ='INSERT INTO #temp (Dbname,CusID,ActvyDT, TotalSlsVal,Wgt,CusLongNm,Branch)          
       SELECT '''+ @DBNAME +''' as Country, stn_sld_cus_id, '''+ @StartDate +''', SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue,SUM(stn_blg_wgt)*(2.20462)      
      ,cus_cus_long_nm,STN_SHPT_BRH      
      FROM ' + @DBNAME + '_sahstn_rec       
      join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
      where STN_INV_DT >= ''' + @StartDate +''' and STN_INV_DT <= ''' + @EndDate +'''      
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''      
      and (STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
      group by STN_SHPT_BRH,stn_sld_cus_id,cus_cus_long_nm'         
       print(@query3)          
         EXECUTE sp_executesql @query3;       
               
         FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
        END           
        CLOSE ScopeCursor;          
        DEALLOCATE ScopeCursor;        
  END          
  ELSE          
  BEGIN          
    DECLARE ScopeCursor CURSOR FOR          
       select StartDate,EndDate from  #Last24Months          
        OPEN ScopeCursor;          
        FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
         WHILE @@FETCH_STATUS = 0          
           BEGIN          
            DECLARE @query4 NVARCHAR(max)      
                  
  SET @query4 ='INSERT INTO #temp (Dbname,CusID,ActvyDT, TotalSlsVal,Wgt,CusLongNm,Branch)          
      SELECT '''+ @DBNAME +''' as Country, stn_sld_cus_id, '''+ @StartDate +''', SUM(stn_tot_val * '+ @CurrenyRate +')  as TotalValue,SUM(stn_blg_wgt)      
      ,cus_cus_long_nm,STN_SHPT_BRH      
      FROM ' + @DBNAME + '_sahstn_rec       
      join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
      where STN_INV_DT >= ''' + @StartDate +''' and STN_INV_DT <= ''' + @EndDate +'''      
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''      
      and (STN_SHPT_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
      group by STN_SHPT_BRH,stn_sld_cus_id,cus_cus_long_nm      
      '    
      print(@query4)          
         EXECUTE sp_executesql @query4;       
               
         FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
        END           
        CLOSE ScopeCursor;          
        DEALLOCATE ScopeCursor;           
  END          
          
END        
  
if(@CompareToYear='3')      
begin      
SELECT *,       
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-03-01', 120)) as PrvQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-06-01', 120)) as PrvQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-09-01', 120)) as PrvQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-12-01', 120)) as PrvQ4,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-03-01', 120)) as CurrQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-06-01', 120)) as CurrQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-09-01', 120)) as CurrQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate()) as varchar(4)) + '-12-01', 120)) as CurrQ4      
 FROM #temp ;          
end      
else if(@CompareToYear='2')      
begin      
SELECT *,       
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-03-01', 120)) as PrvQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-06-01', 120)) as PrvQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-09-01', 120)) as PrvQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-12-01', 120)) as PrvQ4,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-03-01', 120)) as CurrQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-06-01', 120)) as CurrQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-09-01', 120)) as CurrQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-1 as varchar(4)) + '-12-01', 120)) as CurrQ4      
 FROM #temp ;          
end      
else if(@CompareToYear='1')      
begin      
SELECT *,       
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-03-01', 120)) as PrvQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-06-01', 120)) as PrvQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-09-01', 120)) as PrvQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-12-01', 120)) as PrvQ4,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-03-01', 120)) as CurrQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-06-01', 120)) as CurrQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-09-01', 120)) as CurrQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-2 as varchar(4)) + '-12-01', 120)) as CurrQ4      
 FROM #temp ;          
end    
else       
begin      
SELECT *,       
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-03-01', 120)) as PrvQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-06-01', 120)) as PrvQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-09-01', 120)) as PrvQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-4 as varchar(4)) + '-12-01', 120)) as PrvQ4,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-01-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-03-01', 120)) as CurrQ1,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-04-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-06-01', 120)) as CurrQ2,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-07-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-09-01', 120)) as CurrQ3,      
 (select SUM(TotalSlsVal) from #temp q1 where ActvyDT between CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-10-01', 120) and CONVERT(VARCHAR(10), cast(YEAR(getdate())-3 as varchar(4)) + '-12-01', 120)) as CurrQ4      
 FROM #temp ;          
end      
        
       
 DROP TABLE  #temp ;          
END          
          
--EXEC [sp_itech_Customer_Snap_Shot] 'US' ,'LSH3600','ALL','0'         
--EXEC [sp_itech_Customer_Snap_Shot] 'UK' ,'1738'      
--EXEC [sp_itech_Customer_Snap_Shot] 'ALL','1738'          
  
  
        
/*        
Mail sub:Changing Reports to support year 2020 in Titanium  
Date:20200103        
*/
GO
