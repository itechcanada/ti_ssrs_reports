USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_arrshp]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CA_arrshp]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.CA_arrshp_rec', 'U') IS NOT NULL          
  drop table dbo.CA_arrshp_rec;          
              
                  
SELECT *          
into  dbo.CA_arrshp_rec   
FROM [LIVECASTX].[livecastxdb].[informix].[arrshp_rec]     
    
END    
    
GO
