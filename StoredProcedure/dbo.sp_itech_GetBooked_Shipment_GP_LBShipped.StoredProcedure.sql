USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped] @DBNAME varchar(50),@Market varchar(50)

AS
BEGIN
SET NOCOUNT ON;

				--declare @sqltxt varchar(6000)
				--declare @execSQLtxt varchar(7000)
				--declare @DB varchar(100)
				--declare @FD varchar(10)
				--declare @TD varchar(10)

				--set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 13, 0) , 120)   --First day of previous 12 month
				--set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month

				--CREATE TABLE #tmp (  Shipped		DECIMAL(20, 2)
   	--								, sDate 			 VARCHAR(15)
   	--								 ,LBShipped  DECIMAL(20, 2)
   	--								, Booked  DECIMAL(20, 2)
   	--								,Market  varchar(50)
   	--								,GP     DECIMAL(20, 2)
   	--								, TotalNetProfitValue  	 DECIMAL(20, 2)
   	--								);
				   					
				--DECLARE @company VARCHAR(15); 
				--DECLARE @prefix VARCHAR(15); 
				--DECLARE @DatabaseName VARCHAR(35);  
				--DECLARE @CurrenyRate varchar(15);

				--set @DB= @DBNAME
				  	   
				IF @Market = 'ALL'
				 BEGIN
					 set @Market = ''
				 END
				  	   
				-- IF @DBNAME = 'ALL'
				--	BEGIN
				--		DECLARE ScopeCursor CURSOR FOR
				--			select DatabaseName, company,prefix from tbl_itech_DatabaseName
				--		  OPEN ScopeCursor;
				--				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  		--					WHILE @@FETCH_STATUS = 0
  		--					  BEGIN
  	 -- 							DECLARE @query NVARCHAR(1500);  	
  		--						 set @DB= @prefix    --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
  		--						print(@DB)
				--							   IF (UPPER(@Prefix) = 'TW')
				--								begin
				--								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				--								End
				--								Else if (UPPER(@Prefix) = 'NO')
				--								begin
				--									SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				--								End
				--								Else if(UPPER(@Prefix) = 'UK')
				--								begin
				--								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				--								End
				--								Else if(UPPER(@Prefix) = 'DE')
				--								begin
				--								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				--								End
											
				--							SET @query = '  INSERT INTO #tmp(sDate,Shipped,TotalNETProfitValue,GP,LBShipped,Market)
				--											select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate, '
				--											if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
				--											BEGIN
				--											 SET @query= @query +   'Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as Shipped,
				--																	 Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as TotalNETProfitValue,  '
				--											END
				--											 Else
				--											BEGIN
				--											 SET @query= @query +   'Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as Shipped,
				--																	 case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else  SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalNETProfitValue, '
				--											END
				--											SET @query= @query +   'AVG(stn_npft_avg_pct) as ''GP'','
															
				--										--	if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
				--										--	  SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as LBShipped,'
				--										--	  ELSE
				--												SET @query = @query + '	SUM(stn_blg_wgt) as LBShipped,'
															
				--											SET @query = @query + 'cuc_desc30 as Market
				--											from '+ @DB +'_sahstn_rec join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id
				--											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
				--											Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
				--											where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''
				--											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
				--											if @Market =''
				--											BEGIN
				--												 SET @query= @query + ' and cuc_desc30 <> ''Interco'''
				--											END
				--											SET @query = @query + 'group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', cuc_desc30,stn_cry,crx_xexrt
				--											order by sDate desc'
				  	  			  
  	 -- 								 EXECUTE sp_executesql @query;
				  	  					
  	 -- 									SET @query =    'INSERT INTO #tmp(sDate,Booked,Market)
  	 -- 													SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,' 
				--										if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
				--										BEGIN
				--										 SET @query= @query +   'Case when (a.bka_cry = ''USD'') then SUM(a.bka_tot_val * '+ @CurrenyRate +') else  SUM(a.bka_tot_val * ISNULL(crx_xexrt, 1)) end as Booked,'
				--										END
				--										 Else
				--										BEGIN
				--										 SET @query= @query +   'Case when (a.bka_cry = ''USD'') then SUM(a.bka_tot_val) else  SUM(a.bka_tot_val * ISNULL(crx_xexrt, 1)) end as Booked,'
				--										END
				--										SET @query= @query +   ' 
				--										cuc_desc30
				--										FROM ['+ @DB +'_ortbka_rec] a
				--										INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
				--										INNER JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
				--										Left Join  '+ @DB +'_scrcrx_rec on bka_cry= crx_orig_cry and crx_eqv_cry = ''USD''
				--										WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999 AND a.bka_wgt > 0
				--										and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
				--										and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)'
				--										if @Market =''
				--										BEGIN
				--											 SET @query= @query + ' and cuc_desc30 <> ''Interco'''
				--										END
				--										SET @query = @query + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',cuc_desc30,bka_cry,crx_xexrt'
				  	  			
  	 -- 							  EXECUTE sp_executesql @query;
  	 -- 							FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  		--					  END 
				--		  CLOSE ScopeCursor;
				--		  DEALLOCATE ScopeCursor;
				--  END
				--  ELSE
				--	 BEGIN 
				--					IF (UPPER(@DBNAME) = 'TW')
				--					begin
				--					   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				--					End
				--					Else if (UPPER(@DBNAME) = 'NO')
				--					begin
				--						SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				--					End
				--					Else if(UPPER(@DBNAME) = 'UK')
				--					begin
				--					   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				--					End
				--					Else if(UPPER(@DBNAME) = 'DE')
				--					begin
				--					   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				--					End
									
				--					SET @sqltxt = '    INSERT INTO #tmp(sDate,Shipped,TotalNETProfitValue,GP,LBShipped,Market)
				--										   select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate, '
				--											if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
				--											BEGIN
				--											 SET @sqltxt= @sqltxt +   'Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as Shipped,
				--																	   Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as TotalNETProfitValue,   '
				--											END
				--											 Else
				--											BEGIN
				--											 SET @sqltxt= @sqltxt +   'Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else  SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end as Shipped,
				--																	   case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else  SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end as TotalNETProfitValue, '
				--											END
				--											SET @sqltxt= @sqltxt +   'AVG(stn_npft_avg_pct) as ''GP'','
															
				--										--	if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
				--									--		  SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt * 2.20462) as ''LBShipped'','
				--									--		  ELSE
				--												SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt) as ''LBShipped'','
															
				--											SET @sqltxt = @sqltxt + ' cuc_desc30 as Market
				--											from '+ @DB +'_sahstn_rec join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id
				--											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
				--											Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
				--											where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''
				--											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
				--											if @Market =''
				--											BEGIN
				--												 SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
				--											END
				--											SET @sqltxt = @sqltxt + 'group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', cuc_desc30,stn_cry,crx_xexrt
				--											order by sDate desc'
										
				--								print(@sqltxt)
				--							set @execSQLtxt = @sqltxt; 
				--							EXEC (@execSQLtxt);
					        
				--						 SET @sqltxt =    'INSERT INTO #tmp(sDate,Booked,Market)
  	 -- 													SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,' 
				--										if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
				--										BEGIN
				--										 SET @sqltxt= @sqltxt +   'Case when (a.bka_cry = ''USD'') then SUM(a.bka_tot_val * '+ @CurrenyRate +') else  SUM(a.bka_tot_val * ISNULL(crx_xexrt, 1)) end as Booked,'
				--										END
				--										 Else
				--										BEGIN
				--										 SET @sqltxt= @sqltxt +   'Case when (a.bka_cry = ''USD'') then SUM(a.bka_tot_val) else  SUM(a.bka_tot_val * ISNULL(crx_xexrt, 1)) end as Booked,'
				--										END
				--										SET @sqltxt= @sqltxt +   ' 
				--										cuc_desc30
				--										FROM ['+ @DB +'_ortbka_rec] a
				--										INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
				--										INNER JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
				--										Left Join  '+ @DB +'_scrcrx_rec on bka_cry= crx_orig_cry and crx_eqv_cry = ''USD''
				--										WHERE a.bka_ord_pfx=''SO'' AND a.bka_ord_itm<>999 AND a.bka_wgt > 0
				--										and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
				--										and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)'
				--										if @Market =''
				--										BEGIN
				--											 SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
				--										END
				--										SET @sqltxt = @sqltxt + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',cuc_desc30,bka_cry,crx_xexrt'
					        
				--							print(@sqltxt)
				--							set @execSQLtxt = @sqltxt; 
				--							EXEC (@execSQLtxt);
				--	 END
   	--							CREATE TABLE #Main (item  Varchar(50)
   	--								, Category  Varchar(150)
   	--								, actvy_dt	  VARCHAR(15)
   	--								, Value   DECIMAL(20, 2) 
   	--								, TotalNetProfitValue  	 DECIMAL(20, 2)
   	--							   );
				   	               
				    
				--   INSERT INTO #Main (item,Category,actvy_dt,Value)
				--	SELECT  '$ Invoiced',Market,sDate,Shipped  FROM #tmp where Shipped is not null
				--   order by sDate
				   
				--   INSERT INTO #Main (item,Category,actvy_dt,Value)
				--	SELECT  '$ Booked',Market,sDate,Booked  FROM #tmp where Booked is not null
				--   order by sDate
				   
				--   INSERT INTO #Main (item,Category,actvy_dt,Value)
				--	SELECT  'LB Shipped',Market,sDate,LBShipped  FROM #tmp where LBShipped is not null
				--   order by sDate
				   
				--   INSERT INTO #Main (item,Category,actvy_dt,Value,TotalNetProfitValue)
				--	SELECT  'GP%',Market,sDate,  Shipped ,TotalNetProfitValue FROM #tmp where GP is not null
				--   order by sDate
				   
				--  select item,Category,actvy_dt,case item when 'GP%' then  SUM(TotalNetProfitValue)/nullif(SUM(Value), 0) * 100 else sum(Value) end as 'Value' from #Main
				--  group by item,Category,actvy_dt
				  

				-- drop table #Main
				-- drop Table #tmp 
				
				declare @FD varchar(10)
				declare @TD varchar(10)

				set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 13, 0) , 120)   --First day of previous 12 month
				set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month
				
				--declare @TDate varchar(10)
				--Set @TDate= convert(varchar(7),GETDATE(), 126) +'-01'
				
				IF @Market = ''
				 Begin
				     IF @DBNAME = 'ALL'
				    
						 begin	
							select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where (Category <> 'Interco' ) and item <> 'GP%' 
							--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
							and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt
							UNION
							select item,Category,actvy_dt,avg(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where (Category <> 'Interco' ) and item = 'GP%' 
							--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
							and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt
							
							order by item,Category,actvy_dt
						 End
						 ELSE
						 begin	
						 
							select item,Category,actvy_dt,sum(Value) as Value,databases from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where Databases= @DBNAME and (Category <> 'Interco' )
							--and convert(varchar(7),UpdateDtTm, 126) +'-01'=@TDate
							and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt,databases
							order by item,Category,actvy_dt,databases
			         End	
				 End
				 Else
				 Begin
				     IF @DBNAME = 'ALL'
						 begin
							select item,Category,actvy_dt,sum(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where Category= @Market and actvy_dt between @FD and @TD and item <> 'GP%'  
							group by item,Category,actvy_dt
							UNION
							select item,Category,actvy_dt,avg(Value) as Value from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where Category= @Market and actvy_dt between @FD and @TD and item = 'GP%'  
							group by item,Category,actvy_dt
							
							order by item,Category,actvy_dt
						 End
						 ELSE
						 begin
							select item,Category,actvy_dt,sum(Value) as Value,databases from tbl_itech_GetBooked_Shipment_GP_LBShipped_History
							where Databases= @DBNAME and Category= @Market and actvy_dt between @FD and @TD
							group by item,Category,actvy_dt,databases
							order by item,Category,actvy_dt,databases
							print @FD;
							print @TD;
			         End	
				 End
				
			
 
END

-- exec sp_itech_GetBooked_Shipment_GP_LBShipped  'US','Aerospace'
-- select * from tbl_itech_GetBooked_Shipment_GP_LBShipped_History where (Category = 'Aerospace' ) and 
-- item = 'GP%' and Databases= 'US' order by 1 desc

GO
