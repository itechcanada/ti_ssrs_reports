USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_apjglj]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 6, 2015,>
-- Description:	<Description,gl account,>

-- =============================================
CREATE PROCEDURE [dbo].[UK_apjglj]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_apjglj_rec', 'U') IS NOT NULL
		drop table dbo.UK_apjglj_rec;	

	
    -- Insert statements for procedure here
    select  glj_cmpy_id,glj_vchr_pfx,glj_vchr_no,glj_vchr_itm,glj_bsc_gl_acct,glj_sacct,glj_dr_amt,glj_cr_amt,'-' as glj_dist_rmk
into  dbo.UK_apjglj_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[apjglj_rec]; 
  
END
-- select * from UK_apjglj_rec
GO
