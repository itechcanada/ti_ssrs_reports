USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_potpod]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
-- =============================================        
-- Author:  <Author,Clayton Daigle>        
-- Create date: <Create Date,11/6/2012,>        
-- Description: <Description,Open Orders,>        
        
-- =============================================        
CREATE PROCEDURE [dbo].[US_potpod]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
IF OBJECT_ID('dbo.US_potpod_rec', 'U') IS NOT NULL      
  drop table dbo.US_potpod_rec ;          
         
--Insert into dbo.US_potpod_rec         
        
--(    
--pod_cmpy_id,pod_po_pfx,pod_po_no,pod_po_itm,pod_po_dist,pod_po_intl_rls_no,pod_dist_typ,pod_shp_to_whs,pod_ord_pfx,pod_ord_no,pod_ord_itm,pod_bgt_for,pod_bgt_for_cus_id,pod_bgt_for_part,    
--pod_bgt_for_slp,pod_ord_pcs,pod_ord_pcs_typ,pod_ord_msr,pod_ord_msr_typ,pod_ord_wgt,pod_ord_wgt_typ,pod_ord_qty,pod_ord_qty_typ,pod_rcvd_pcs,pod_rcvd_msr,pod_rcvd_wgt,pod_rcvd_qty,pod_bal_pcs,    
--pod_bal_msr,pod_bal_wgt,pod_bal_qty,pod_trnst_dy,pod_arr_dt_qlf,pod_arr_dt_fmt,pod_arr_dt_ent,pod_arr_fm_dt,pod_arr_to_dt,pod_arr_dt_wk_no,pod_arr_dt_wk_cy,pod_frt_cst,pod_frt_cst_um,    
--pod_frt_ven_id,pod_frt_cry,pod_frt_ex_rt,pod_frt_ex_rt_typ,pod_lnd_cst,pod_inq_cst,pod_inq_upd,pod_inq_rmk,pod_itm_ctl_no,pod_trcomp_sts,pod_ven_id    
--)        
SELECT     
pod_cmpy_id,pod_po_pfx,pod_po_no,pod_po_itm,pod_po_dist,pod_po_intl_rls_no,pod_dist_typ,pod_shp_to_whs,pod_ord_pfx,pod_ord_no,pod_ord_itm,pod_bgt_for,pod_bgt_for_cus_id,pod_bgt_for_part,    
pod_bgt_for_slp,pod_ord_pcs,pod_ord_pcs_typ,pod_ord_msr,pod_ord_msr_typ,pod_ord_wgt,pod_ord_wgt_typ,pod_ord_qty,pod_ord_qty_typ,pod_rcvd_pcs,pod_rcvd_msr,pod_rcvd_wgt,pod_rcvd_qty,pod_bal_pcs,    
pod_bal_msr,pod_bal_wgt,pod_bal_qty,pod_trnst_dy,pod_arr_dt_qlf,pod_arr_dt_fmt,pod_arr_dt_ent,pod_arr_fm_dt,pod_arr_to_dt,pod_arr_dt_wk_no,pod_arr_dt_wk_cy,pod_frt_cst,pod_frt_cst_um,    
pod_frt_ven_id,pod_frt_cry,pod_frt_exrt,pod_frt_ex_rt_typ,pod_lnd_cst,pod_inq_cst,pod_inq_upd,'-' as pod_inq_rmk,pod_itm_ctl_no,pod_trcomp_sts,pod_ven_id  
into dbo.US_potpod_rec      
FROM [LIVEUSSTX].[liveusstxdb].[informix].[potpod_rec]    
  
-- Insert record in potpod_history table.  
-- insert into tbl_itech_potpod_history (pod_cmpy_id,pod_po_pfx,pod_po_no,pod_po_itm,pod_po_dist,pod_arr_dt_ent,pod_arr_fm_dt,pod_arr_to_dt,recordCreatedDate)  
MERGE tbl_itech_potpod_history AS T  
USING US_potpod_rec AS S  
ON ( T.pod_cmpy_id = S.pod_cmpy_id and T.pod_po_pfx = S.pod_po_pfx and T.pod_po_no = S.pod_po_no  
and T.pod_po_itm = s.pod_po_itm and T.pod_po_dist = S.pod_po_dist   
)   
WHEN NOT MATCHED BY TARGET   
    THEN INSERT(pod_cmpy_id,pod_po_pfx,pod_po_no,pod_po_itm,pod_po_dist,pod_arr_dt_ent,pod_arr_fm_dt,pod_arr_to_dt,recordCreatedDate)   
    VALUES(S.pod_cmpy_id, S.pod_po_pfx, S.pod_po_no, S.pod_po_itm,S.pod_po_dist,   
(case len(S.pod_arr_dt_ent) when 8 then  cast(cast(S.pod_arr_dt_ent as varchar(10)) as date) else Null end ) ,S.pod_arr_fm_dt,S.pod_arr_to_dt, GETDATE() );  
  
        
        
END        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
GO
