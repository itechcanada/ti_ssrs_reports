USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetFullBranchesName]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <14 Mar 2018>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetFullBranchesName]  @DBNAME varchar(50),@BRANCH varchar(50) 
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  branchName varchar(55)   
         
        ) 

if @BRANCH ='ALL'          
 BEGIN          
 set @BRANCH = ''          
 END            
  
IF @DBNAME = 'ALL'  
 BEGIN  
  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( branchName)  
                  select brh_brh_nm FROM         '+ @Prefix +'_scrbrh_rec where (brh_brh = '''+ @BRANCH +''' or ''' + @BRANCH + ''' = '''')'  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( branchName)  
                  select brh_brh_nm FROM         '+ @DBNAME +'_scrbrh_rec where (brh_brh = '''+ @BRANCH +''' or ''' + @BRANCH + ''' = '''') '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
        
      select * from #tmp;
      drop table  #tmp  ;
END  
  
-- exec sp_itech_GetBranches  'ALL'  
  
  /*
  2016-05-19
  Changed by mukesh
  */
GO
