USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TASK_SCHEDULER]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,1/18/2012,>
-- Description:	<Description,To view only error message>
-- =============================================
CREATE PROCEDURE [dbo].[TASK_SCHEDULER]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DISTINCT a.cti_ctlg_nm, a.cti_tsk_nm, b.tct_strt_dtts, b.tct_comp_dtts, b.tct_lgn_id,b.tct_clnt_host_nm,c.msg_msg_var
FROM [LIVEUSSTX].[liveusstxdb].[informix].[mxtcti_rec] a
 INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[mxttct_rec] b ON a.cti_tsk_nm=b.tct_tsk_nm
 INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[sctmsg_rec] c ON b.tct_clnt_pid=c.msg_clnt_pid
WHERE b.tct_rtn_sts=1
ORDER BY b.tct_strt_dtts DESC
   
 
 
END
GO
