USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_aptpyh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 03, 2016,>  
-- Description: <Description,Open Job history,>  
  
-- =============================================  
create PROCEDURE [dbo].[UK_aptpyh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_aptpyh_rec', 'U') IS NOT NULL  
  drop table dbo.UK_aptpyh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_aptpyh_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[aptpyh_rec] ;   
    
END  
-- select * from UK_aptpyh_rec
GO
