USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ortord]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
      
      
      
      
-- =============================================      
-- Author:  <Author,Clayton Daigle>      
-- Create date: <Create Date,10/5/2012,>      
-- Description: <Description,Open Orders,>      
      
-- =============================================      
CREATE PROCEDURE [dbo].[UK_ortord]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
IF OBJECT_ID('dbo.UK_ortord_rec', 'U') IS NOT NULL              
  drop table dbo.UK_ortord_rec;                
      
    -- Insert statements for procedure here      
SELECT *      
into  dbo.UK_ortord_rec    
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ortord_rec]      
    where ord_ord_no != '12364' -- 20180115  
    and ord_ord_no != '12502' --20180205
   --order by ord_ord_no desc   
END      
      
 /*  
 problem in column ord_cus_po  
 */     
      
      
      
      
      
      
      
      
      
      
      
GO
