USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_PandLByBranch_bck11102021]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Bablu >        
-- Create date: <11 NOV 2013>        
-- Description: <Getting top 50 customers for SSRS reports>        
-- Last changed date <18 Feb 2015>      
-- Last changed description : add the joining to get cus_adm_brh and apply filter     
-- Last change Date: 29 Jun 2015  
-- Last changes By: Bablu  
-- Last changes Desc: Remove the live connection of database   
-- Last change Date: 20 Jan 2016  
-- Last changes By: Bablu  
-- Last changes Desc: Include interco option in filter   
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_PandLByBranch_bck11102021]  @DBNAME varchar(50), @AcctPeriod varchar(6), @BranchName Varchar(30)        
        
AS        
BEGIN        
  
  
 SET NOCOUNT ON;        
declare @sqltxt1 varchar(8000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
DECLARE @IsExcInterco char(1)  
DECLARE @TotalRevenue numeric(12,2)
DECLARE @CostofSales numeric(12,2)
DECLARE @GrossMargin  numeric(12,2)
DECLARE @TotalOperatingExpenses numeric(12,2)
DECLARE @OperatingIncome numeric(12,2)
DECLARE @TotalNonOperatingExpenses numeric(12,2)
DECLARE @TotalNonOperatingIncome numeric(12,2)
DECLARE @NetIncome numeric(12,2)
DECLARE @NetIncomeper varchar(100)
DECLARE @OperatingIncomeMargin varchar(100)
DECLARE @GrossMarginper  varchar(100)
set @DB=  @DBNAME        

 select  @TotalRevenue = sum(Amount)  from
  (
  select 1 as Ctr, @AcctPeriod As AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue'
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName 
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION  
  select 2 as Ctr,@AcctPeriod As AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs'
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName
  group by  pl_cat, subAccountDesc, gld_sacct
  ) as x group by AcctPeriod
  -- set @CostofSales
  select @CostofSales = (sum(gld_cr_amt - gld_dr_amt) *-1)  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  -- set @TotalOperatingExpenses
  select @TotalOperatingExpenses=sum(Amount)  from
  (
 select 4 as Ctr, @AcctPeriod As AcctPeriod, subAccountDesc, gld_bsc_gl_acct as gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 5 as Ctr, @AcctPeriod As AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  ) as TotalOperatingExpenses group by AcctPeriod

  -- set @TotalNonOperatingExpenses
  select @TotalNonOperatingExpenses= (sum(gld_cr_amt - gld_dr_amt) *-1)  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 

  -- set @TotalNonOperatingIncome
  select @TotalNonOperatingIncome = (sum(gld_cr_amt - gld_dr_amt)) from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 

  set @GrossMargin=@TotalRevenue-@CostofSales

  set @GrossMarginper = (@GrossMargin/@TotalRevenue)*100 

  set @OperatingIncome =@GrossMargin - @TotalOperatingExpenses

  set @OperatingIncomeMargin = (@OperatingIncome / @TotalRevenue) *100

  set @NetIncome= @OperatingIncome -@TotalNonOperatingExpenses + @TotalNonOperatingIncome

  set @NetIncomeper = (@NetIncome/@TotalRevenue)*100 

  select 0 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Revenue' as AccountType,@TotalRevenue as Amount 
  union
  select 1 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount 
  union
  select 1 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Cost of Sales' as AccountType,@CostofSales as Amount 
  union
  select 2 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount   
  union
  select 2 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Gross Margin' as AccountType,@GrossMargin as Amount 
  union 
  select 3 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'0' gld_sacct,'Gross Margin %  ' as AccountType,@GrossMarginper as Amount 
  union 
  select 4 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount 
  
  --select 0 as Ctr, AcctPeriod,@BranchName as subAccountDesc,'a' gld_sacct,'Revenue' as AccountType,sum(Amount)  from
  --(
  --select 1 as Ctr, @AcctPeriod As AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  --join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  --join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct
  --UNION  
  --select 2 as Ctr,@AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  --join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  --join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct
  --) as x group by AcctPeriod
  --UNION  
  --select 3 as Ctr,@AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  --join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  --join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct  
  UNION 
 
  select 4 as Ctr, @AcctPeriod, subAccountDesc, gld_bsc_gl_acct , pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 5 as Ctr, @AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 6 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Total Operating Expenses' as AccountType,@TotalOperatingExpenses as Amount 
  union 
  select 7 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount   
  UNION
  select 7 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Operating Income' as AccountType,@OperatingIncome as Amount 
 UNION
 select 8 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'0' gld_sacct,'Operating Profit Margin %' as AccountType,@OperatingIncomeMargin as Amount 
 union 
  select 9 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount   
 UNION
    select 9 as Ctr, @AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 10 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Total Non Operating Expenses' as AccountType,@TotalNonOperatingExpenses as Amount 
  union 
  select 11 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount   
  UNION
    select 11 as Ctr, @AcctPeriod,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt)  as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 12 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Total Non Operating Income' as AccountType,@TotalNonOperatingIncome as Amount 
  union 
  select 13 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'' as AccountType,null as Amount   
  UNION
  select 13 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'' gld_sacct,'Net Income' as AccountType,@NetIncome as Amount 
  UNION
  select 14 as Ctr,  @AcctPeriod,@BranchName as subAccountDesc,'0' gld_sacct,'Net Income %' as AccountType,@NetIncomeper as Amount 

  order by Ctr, AccountType;

     
END        
        
-- exec [sp_itech_PandLByBranch]'US' , '202109','LAX'
-- select distinct gld_acctg_per from US_glhgld_rec order by gld_acctg_per desc
-- select subAccountDesc from tbl_itech_US_SubAccounts

GO
