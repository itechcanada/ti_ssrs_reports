USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[LCS_SHIPMENTS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,4/17/2012>
-- Description:	<Description,It's for Jerry Young and Mary Downes. >
-- =============================================
CREATE PROCEDURE [dbo].[LCS_SHIPMENTS]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


SELECT *
INTO #TEMP1
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] 
WHERE sat_ord_pfx='SO'
AND sat_inv_dt BETWEEN @FromDate AND @ToDate

SELECT a.* 
INTO #TEMP2
FROM [LIVEUSSTX].[liveusstxdb].[informix].[pntssp_rec] a
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[mcrmss_rec] b ON a.ssp_mss_ctl_no=b.mss_mss_ctl_no
WHERE ssp_ref_pfx='SO'
AND b.mss_sdo = 'PWA'


SELECT a.sat_upd_ref, a.sat_upd_ref_itm, a.sat_inv_dt, a.sat_grd, a.sat_size, a.sat_sld_cus_id, a.sat_nm1, a.sat_tot_val, a.sat_blg_wgt, a.sat_blg_msr,
'product'=CASE WHEN a.sat_frm='TIPL' THEN 'TI PLATE'
WHEN a.sat_frm='TIRD' THEN 'TI BAR' 
WHEN a.sat_frm='NIPL' THEN 'NI PLATE' 
WHEN a.sat_frm='NIRD' THEN 'NI BAR' 
WHEN a.sat_frm='TISH' THEN 'TI SHEET' ELSE a.sat_frm END
FROM #TEMP1 a
INNER JOIN #TEMP2 b ON a.sat_ord_pfx=b.ssp_ref_pfx AND a.sat_ord_no=b.ssp_ref_no AND a.sat_ord_itm=b.ssp_ref_itm


DROP TABLE #TEMP1
DROP TABLE #TEMP2

END
GO
