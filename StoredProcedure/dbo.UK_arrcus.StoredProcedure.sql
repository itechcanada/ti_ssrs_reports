USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_arrcus]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================      
-- Author:  <Author,Clayton Daigle>      
-- Create date: <Create Date,10/5/2012,>      
-- Description: <Description,Open Orders,>      
-- Problem in column cus_cus_nm      
-- =============================================      
CREATE PROCEDURE [dbo].[UK_arrcus]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
Delete from dbo.UK_arrcus_rec ;       
       
Insert into dbo.UK_arrcus_rec       
      
    -- Insert statements for procedure here      
SELECT *      
FROM [LIVEUKSTX].[liveukstxdb].[informix].[arrcus_rec]      
where cus_cus_id not in ( '3439' -- 20180117    
,'3720' -- 20180606  
,'4003') -- 20180622    
END      
GO
