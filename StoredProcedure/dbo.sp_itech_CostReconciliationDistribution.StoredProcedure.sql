USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CostReconciliationDistribution]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <21 Dec 2015>  
  
-- =============================================  
-- Description: <Cost Reconciliation Distribution for SSRS reports>  
  
CREATE PROCEDURE [dbo].[sp_itech_CostReconciliationDistribution] @DBNAME varchar(50)  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(7000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
DECLARE @FromYear varchar(10)  
DECLARE @ToYear varchar(10)  
  
set @DB=  @DBNAME  
--set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
--set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
  
  
CREATE TABLE #tmp ( Databases   VARCHAR(2)  
     ,  VchPfx   VARCHAR(2)  
        , VchNo    VARCHAR(10)  
        , VchItm   VARCHAR(10)   
        , CostReconRefNo Varchar(10)  
        , CostRecBranch  Varchar(3)  
        , TransRefPfx       Varchar(2)  
                    , TransRefNo    Varchar(10)  
        , TransRefItm  Varchar(10)  
        , TransSubItm  Varchar(10)  
        , ExternalRef  Varchar(30)  
        , CostCode   Decimal(20,2)   
        , DistributedQty Decimal(20,2)   
        ,DistUOM   VARCHAR(5)   
        ,DistAmt   Decimal(20,2)   
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
DECLARE @CurrenyRate varchar(15);  
  
  
IF @DBNAME = 'ALL'  
 BEGIN  
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);    
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
       IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End
		Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End
      
       
      SET @query =  'INSERT INTO #tmp (Databases, VchPfx,  VchNo, VchItm, CostReconRefNo, CostRecBranch, TransRefPfx, TransRefNo, TransRefItm, TransSubItm, ExternalRef, CostCode, DistributedQty,DistUOM,DistAmt)       
      select ''' + @Prefix + ''' , jci_vchr_pfx, jci_vchr_no, jci_vchr_itm, jci_crcn_no, jci_crcn_brh,jci_trs_pfx, jci_trs_no,jci_trs_itm, jci_trs_sbitm,  
      jci_extl_ref,jci_cc_no, jci_dsqty,jci_dsqty_um,jci_dsamt  
       from ' +  @Prefix + '_apjjci_rec  
       '  
         
         
        EXECUTE sp_executesql @query;  
        print(@query)   
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
         
       /*  
       EXECUTE sp_executesql @query;  
          
          
        print(@query)  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
         
       */  
         
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
         
    Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')    
    print('2:' + @Name);  
    print('3:' + @DBNAME);  
     
    IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End
       
     SET @sqltxt ='INSERT INTO #tmp (Databases, VchPfx,  VchNo, VchItm, CostReconRefNo, CostRecBranch, TransRefPfx, TransRefNo, TransRefItm, TransSubItm, ExternalRef, CostCode, DistributedQty,DistUOM,DistAmt)       
          
      select ''' + @DBNAME + ''' , jci_vchr_pfx, jci_vchr_no, jci_vchr_itm, jci_crcn_no, jci_crcn_brh,jci_trs_pfx, jci_trs_no,jci_trs_itm, jci_trs_sbitm,  
      jci_extl_ref,jci_cc_no, jci_dsqty,jci_dsqty_um,jci_dsamt  
       from ' +  @DBNAME + '_apjjci_rec  
        
       '  
    print(@sqltxt)  ;  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
     END  
      
      
         
       
     select * FROM #tmp;  
       
     drop table #tmp  
       
END  
  
-- exec [sp_itech_CostReconciliationVoucher] '07/01/2015', '07/31/2015','ALL' ;  
--aptvch  
/*
20210128	Sumit
Add Germany Database
*/
GO
