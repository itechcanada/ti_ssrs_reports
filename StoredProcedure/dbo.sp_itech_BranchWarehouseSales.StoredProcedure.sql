USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_BranchWarehouseSales]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <20 Jun 2017>  
-- Description: <Branch Warehouse Sales Reports> 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_BranchWarehouseSales]  @DBNAME varchar(3), @Branch varchar(3), @Warehouse varchar(3), @FromDate Datetime, @ToDate DateTime
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
declare @DB varchar(10);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);  
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15);    
DECLARE @CountryName VARCHAR(25); 
DECLARE @FD VARCHAR(10); 
DECLARE @TD VARCHAR(10); 
set @FD = CONVERT(varchar(10),@FromDate,120);  
set @TD = CONVERT(varchar(10),@ToDate,120);
  
CREATE TABLE #temp ( DBName   VARCHAR(3)  
     ,Branch		VARCHAR(3) 
     ,Warehouse		VARCHAR(3)
     ,InvoiceDate   VArchar(10)
     ,TotalSales		Decimal(20,2)   
     ,TotalWeight   Decimal(20,2)  
     ); 
     

IF @Branch = 'ALL'          
 BEGIN          
  set @Branch = ''          
 END            
           
 IF @Warehouse = 'ALL'          
 BEGIN          
  set @Warehouse = ''          
 END    
  
IF @DBNAME = 'ALL'  
 BEGIN  
   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR   
    BEGIN  
    SET @query = 'INSERT INTO #temp (DBName,Branch,Warehouse,InvoiceDate,TotalSales,TotalWeight)  
           
      SElect  '''+ @Prefix +''', orh_ord_brh,stn_shpg_whs, stn_inv_dt,SUM(stn_tot_val)* '+ @CurrenyRate +', SUM(stn_blg_wgt)*2.20462 from ' + @Prefix + '_sahstn_rec 
join ' + @Prefix + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no
where (orh_ord_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')  and (stn_shpg_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''') 
and stn_inv_dt >= ''' + @FD + ''' and stn_inv_dt <= ''' + @TD + '''
group by orh_ord_brh,stn_shpg_whs,stn_inv_dt '  
  
    END  
    ELSE  
    BEGIN  
     SET @query = 'INSERT INTO #temp (DBName,Branch,Warehouse,InvoiceDate,TotalSales,TotalWeight)  
           
      SElect  '''+ @Prefix +''', orh_ord_brh,stn_shpg_whs,stn_inv_dt, SUM(stn_tot_val)* '+ @CurrenyRate +', SUM(stn_blg_wgt) from ' + @Prefix + '_sahstn_rec 
join ' + @Prefix + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no
where (orh_ord_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')  and (stn_shpg_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''') 
and stn_inv_dt >= ''' + @FD + ''' and stn_inv_dt <= ''' + @TD + '''
group by orh_ord_brh,stn_shpg_whs,stn_inv_dt'  

    END  
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
 
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)  
  
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)  
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
      
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )
   BEGIN  
   SET @sqltxt ='INSERT INTO #temp (DBName,Branch,Warehouse,InvoiceDate,TotalSales,TotalWeight)  
     SElect  '''+ @DBNAME +''', orh_ord_brh,stn_shpg_whs, stn_inv_dt,SUM(stn_tot_val)* '+ @CurrenyRate +', SUM(stn_blg_wgt)*2.20462 from ' + @DBNAME + '_sahstn_rec 
join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no
where (orh_ord_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')  and (stn_shpg_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''') 
and stn_inv_dt >= ''' + @FD + ''' and stn_inv_dt <= ''' + @TD + '''
group by orh_ord_brh,stn_shpg_whs,stn_inv_dt
'
  END  
  ELSE  
  BEGIN  
  SET @sqltxt ='INSERT INTO #temp (DBName,Branch,Warehouse,InvoiceDate,TotalSales,TotalWeight)  
      
SElect  '''+ @DBNAME +''', orh_ord_brh,stn_shpg_whs,stn_inv_dt, SUM(stn_tot_val)* '+ @CurrenyRate +', SUM(stn_blg_wgt) from ' + @DBNAME + '_sahstn_rec 
join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no
where (orh_ord_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')  and (stn_shpg_whs = ''' + @Warehouse + ''' OR ''' + @Warehouse + ''' = '''') 
and stn_inv_dt >= ''' + @FD + ''' and stn_inv_dt <= ''' + @TD + '''
group by orh_ord_brh,stn_shpg_whs,stn_inv_dt'
               
         
  END  
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ;  
 DROP TABLE  #temp;  
END  
  
--EXEC [sp_itech_BranchWarehouseSales] 'ALL','ALL','ALL','2017-01-01','2017-06-20'  
--select * from tbl_itech_DatabaseName
GO
