USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_cttcsi]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: Dec 12, 2015  
-- Description: <Description,,>  
-- =============================================  
Create PROCEDURE [dbo].[TW_cttcsi]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.TW_cttcsi_rec', 'U') IS NOT NULL  
  drop table dbo.TW_cttcsi_rec;  
      
          
SELECT *  
into  dbo.TW_cttcsi_rec  
FROM [LIVETWSTX].[livetwstxdb].[informix].[cttcsi_rec];  
  
END  
--  exec  TW_cttcsi  
-- select * from TW_cttcsi_rec
GO
