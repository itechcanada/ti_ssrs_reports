USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[gnc_get_previous_n_month]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[gnc_get_previous_n_month](@ToDate datetime)
returns varchar(50)
AS
BEGIN
DECLARE @vc_month varchar(2) =  DATEPART(month,@ToDate)
DECLARE @vc_year varchar(5) = DATEPART(year,@ToDate)
DECLARE @vc_no_of_days varchar(2) = ''
DECLARE @vc_complete_date varchar(30)  = ''

DECLARE @dtDate DATETIME
SELECT @dtDate= dateadd(mm,datediff(mm,0,@ToDate),0)
SELECT @vc_no_of_days = datediff(dd,@dtDate,dateadd(mm,1,@dtDate))
select @vc_complete_date = @vc_year + '-' + @vc_month + '-' + @vc_no_of_days
return @vc_complete_date

END
GO
