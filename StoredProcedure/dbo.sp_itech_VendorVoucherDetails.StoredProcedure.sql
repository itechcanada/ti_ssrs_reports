USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_VendorVoucherDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <19 July 2016>    
-- Description: <Getting Vendor Voucher SSRS reports>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_VendorVoucherDetails] @DBNAME varchar(50),  @VendorNo Varchar(20),@FromDate datetime, @ToDate datetime , @GLCode Varchar(20),@GLSubAcct Varchar(50) ,@Branch Varchar(3)  
As    
Begin    
declare @DB varchar(100)    
declare @sqltxt varchar(6000)    
declare @execSQLtxt varchar(7000)    
declare @FD varchar(10)    
declare @TD varchar(10)    
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)    
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)   
DECLARE @ExchangeRate varchar(15)   
    
IF @VendorNo = 'ALL' OR @VendorNo = ''    
BEGIN    
SET @VendorNo = ''    
END   
   
IF @GLCode = 'ALL' OR @GLCode = ''    
BEGIN    
SET @GLCode = ''    
END  
  
IF @GLSubAcct = 'ALL'    
BEGIN    
SET @GLSubAcct = ''    
END  
  
IF @Branch = 'ALL'    
BEGIN    
SET @Branch = ''    
END  
  
  
  
Set @VendorNo = RTRIM(LTRIM(@VendorNo));    
Set @GLCode = RTRIM(LTRIM(@GLCode));    
    
CREATE TABLE #tmp (   [Database]   VARCHAR(10)    
        , VoucherPfx   VARCHAR(4)    
        , VoucherNo    int    
        , RefDate    date    
        , InvNo     varchar(50)    
        , InvBrh    varchar(3)    
        , OrigAmt     decimal(20,2)    
        , DueDate    date    
        , DiscAmt    decimal(20,2)    
        , VendorNme   VARCHAR(40)  
        , VendorID    VARCHAR(8)   
        , VendorTaxID   VARCHAR(15)   
        , InvDate     date   
        , UpdateDate     date    
        , GlAccount   VARCHAR(20)  
        , GlSubAccount   VARCHAR(50)  
        , GlAccountName   VARCHAR(30)  
        )    
            
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(35);    
DECLARE @Name VARCHAR(15);    
DECLARE @CurrenyRate varchar(15);  
    
IF @DBNAME = 'ALL'    
 BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS    
    OPEN ScopeCursor;    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(max);   
           
        IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End
		Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End 
             
       SET @query ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, RefDate, InvNo, InvBrh, OrigAmt, DueDate, DiscAmt,VendorNme,VendorID,VendorTaxID,  
        InvDate,UpdateDate,GlAccount,GlSubAccount,GlAccountName )    
        SELECT ''' +  @Prefix + ''' as [Database],  jvc_vchr_pfx, jvc_vchr_no, jvc_ent_dt, jvc_ven_inv_no, jvc_vchr_brh,    
         jvc_vchr_amt * '+ @CurrenyRate +', jvc_due_dt, jvc_disc_amt * '+ @CurrenyRate +' ,ven_ven_nm,jvc_ven_id,ven_1099_tx_id,jvc_ven_inv_dt,jvc_upd_dt  
           
         ,(SELECT top 1 glj_bsc_gl_acct FROM ' + @Prefix + '_apjglj_rec where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as glj_bsc_gl_acct  
         ,(SELECT top 1 glj_sacct FROM ' + @Prefix + '_apjglj_rec where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as glSubAccount  
         ,(SELECT top 1 bga_desc30 FROM ' + @Prefix + '_glrbga_rec join ' + @Prefix + '_apjglj_rec on bga_bsc_gl_acct = glj_bsc_gl_acct where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as gl_AccountName  
         from ' + @Prefix + '_apjjvc_rec  
         join ' + @Prefix + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id and ven_ven_id = jvc_ven_id    
          where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and   
          (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''')  
           
          ;'           
    print @query;    
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN    
         IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End 
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End
     
        SET @sqltxt ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, RefDate, InvNo, InvBrh, OrigAmt, DueDate, DiscAmt,VendorNme,VendorID,VendorTaxID,  
        InvDate,UpdateDate,GlAccount,GlSubAccount,GlAccountName )    
        SELECT ''' +  @DBNAME + ''' as [Database],  jvc_vchr_pfx, jvc_vchr_no, jvc_ent_dt, jvc_ven_inv_no, jvc_vchr_brh,    
         jvc_vchr_amt * '+ @CurrenyRate +', jvc_due_dt, jvc_disc_amt * '+ @CurrenyRate +' ,ven_ven_nm,jvc_ven_id,ven_1099_tx_id,jvc_ven_inv_dt,jvc_upd_dt  
           
         ,(SELECT top 1 glj_bsc_gl_acct FROM ' + @DBNAME + '_apjglj_rec where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as glj_bsc_gl_acct  
         ,(SELECT top 1 glj_sacct FROM ' + @DBNAME + '_apjglj_rec where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as glSubAccount  
         ,(SELECT top 1 bga_desc30 FROM ' + @DBNAME + '_glrbga_rec join ' + @DBNAME + '_apjglj_rec on bga_bsc_gl_acct = glj_bsc_gl_acct where jvc_cmpy_id = glj_cmpy_id AND jvc_vchr_pfx = glj_vchr_pfx AND jvc_vchr_no = glj_vchr_no) as gl_AccountName  
         from ' + @DBNAME + '_apjjvc_rec  
         join ' + @DBNAME + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id and ven_ven_id = jvc_ven_id    
          where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and   
          (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''')  
           ;'    
     print(@sqltxt)     
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
   END    
   Select *,  
   case when substring(GlSubAccount,0,3) = '01' then 'Corporate'   
                   when substring(GlSubAccount,0,3) = '02' then 'Purchase /Accounting'   
                   when substring(GlSubAccount,0,3) = '04' then 'IT'   
                   when substring(GlSubAccount,0,3) = '05' then 'Warehouse'   
                   when substring(GlSubAccount,0,3) = '10' then 'Sales'   
                   when substring(GlSubAccount,0,3) = '40' then 'Jacksonville'   
                   when substring(GlSubAccount,0,3) = '55' then 'Hillsboro'   
                   when substring(GlSubAccount,0,3) = '60' then 'Aurora'  
                   when substring(GlSubAccount,0,3) = '70' then 'Exports'  
                   when substring(GlSubAccount,0,3) = '85' then 'Seattle'  
                   when substring(GlSubAccount,0,3) = '81' then 'Garden Grove'  
                   when substring(GlSubAccount,0,3) = '95' then 'India' else   
                   substring(GlSubAccount,0,3) end as Department  
                     
    from #tmp  Where (GlAccount = @GLCode OR @GLCode = '') and (GlSubAccount = @GLSubAcct OR @GLSubAcct = '') and (InvBrh = @Branch OR @Branch = '')  
    order by [Database],RefDate,VoucherNo    
   Drop table #tmp    
End     
    
            
-- Exec [sp_itech_VendorVoucherDetails] 'ALL', 'ALL', '2016-06-01', '2016-06-30' ,'','ALL' ,'MTL'  
    
--select distinct jvc_vchr_pfx from [LIVECASTX].[livecastxdb].[informix].apjjvc_rec   
--  -- and (jvc_vchr_brh = ''' + @Branch + ''' OR ''' + ''' = ''' + @Branch + ''')  
/*
20210128	Sumit
Add germany Database
*/
GO
