USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OOPS_OpenOrder_Week]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukesh >              
-- Create date: <21 Jan 2019>              
-- Description: <Getting top 50 customers for SSRS reports>              
           
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_OOPS_OpenOrder_Week]  @DBNAME varchar(50),@Branch varchar(10)    
              
AS              
BEGIN             
               
 SET NOCOUNT ON;              
declare @sqltxt varchar(7000)              
declare @execSQLtxt varchar(7000)              
declare @DB varchar(100)              
declare @FD varchar(10)              
declare @TD varchar(10)              
declare @NOOfCust varchar(15)              
DECLARE @ExchangeRate varchar(15)     
        
set @FD = convert (Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0), 120); --First day of previous month            
set @TD = CONVERT(VARCHAR(10), dateadd(day, 0 - day(dateadd(month, 2 , GETDATE())), dateadd(month, 2 , GETDATE())) , 120); -- LAst date of next month    
              
set @DB= @DBNAME     
         
CREATE TABLE #tmpFinal (      
ThisWeekOpenOrderCount int    
,ThisMonthOpenOrderCount int    
)    
              
CREATE TABLE #tmp (  CompanyId varchar(5)               
        --,OrderPrefix Varchar(2)              
        --,OrderNo varchar(10)              
        --,OrderItem   varchar(3)              
        ,Branch varchar(10)              
        --,CustID   VARCHAR(10)               
        --, CustName     VARCHAR(65)              
        --, CustomerCat   VARCHAR(65)               
        --, ChargeValue    DECIMAL(20, 2)              
        --, BalPcs  DECIMAL(20, 2)              
        --, BalWgt           DECIMAL(20, 2)              
        --, BalMsr    DECIMAL(20, 2)              
        --, StatusAction               varchar(2)              
        --, Salesperson     varchar(65)              
        , DueDate    varchar(10)              
        --, ProductName   VARCHAR(100)               
        , OrderType     varchar(65)              
        --,Form  varchar(35)              
        --,Grade  varchar(35)              
        --,Size  varchar(35)              
        --,Finish  varchar(35)             
        --,CustPO   VARCHAR(30)           
        --,CustAdd VARCHAR(170)           
        -- ,SalesPersonIs Varchar(10)            
        --,SalesPersonOs Varchar(10)           
        --,NetAmt Decimal(20,2)          
        --,NetPct Decimal(20,2)          
        --,ShipWhs Varchar(3)         
        --,PrdFull varchar(70)          
                 );               
              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(5);              
DECLARE @Name VARCHAR(15);              
DECLARE @CurrenyRate varchar(15);               
  
-- 20200428  
  
--if @Branch ='ROC'              
-- BEGIN              
-- set @Branch = 'ROS'              
-- END      
  
--  
  
 if @Branch ='ALL'              
 BEGIN              
 set @Branch = ''              
 END              
      
IF @DBNAME = 'ALL'        
 BEGIN              
            
    DECLARE ScopeCursor CURSOR FOR                
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                 
   OPEN ScopeCursor;                
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(Max);                 
      SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'              
      IF (UPPER(@Prefix) = 'TW')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                      
    End                      
    Else if (UPPER(@Prefix) = 'NO')                      
    begin                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End                      
    Else if (UPPER(@Prefix) = 'CA')                      
    begin                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
    End                      
    Else if (UPPER(@Prefix) = 'CN')                   
    begin                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                      
    End                      
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                      
    begin                      
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                      
    End                      
    Else if(UPPER(@Prefix) = 'UK')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                      
    End                 
    Else if(UPPER(@Prefix) = 'DE')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                      
    End                 
      SET @query =              
       'INSERT INTO #tmp ( CompanyId,Branch , DueDate,OrderType)              
       select ord_cmpy_id as CompanyId,ord_ord_brh as Branch,orl_due_to_dt as DueDate,cds_desc              
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,              
       ' + @DB + '_ortchl_rec ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec where              
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id              
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id              
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no              
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id              
       and cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ            
       and orl_bal_qty >= 0               
       and chl_chrg_cl= ''E''              
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''              
       and orh_sts_actn <> ''C''              
       and ord_ord_pfx <>''QT''               
       and  (           
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )          
       or          
       (orl_due_to_dt is null and orh_ord_typ =''J''))  ' 
	   
	    if(@Branch = 'ROC')  
		begin 
		SET @query = @query + ' and (orh_shpg_whs = '''+ @Branch +''' or '''+ @Branch +'''= '''') '
		End
		else
		begin
		SET @query = @query + ' and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  '
		End

       SET @query = @query + '  and  ord_ord_brh not in (''SFS'') and cds_desc in (''Normal Order'', ''Job Detail Order'')              
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'              
              
     print(@query);           
        EXECUTE sp_executesql @query;              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE              
     BEGIN               
              
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')                
                   
     IF (UPPER(@DB) = 'TW')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                      
    End                      
  Else if (UPPER(@DB) = 'NO')                      
    begin                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                      
    End                      
    Else if (UPPER(@DB) = 'CA')                      
    begin                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
    End                      
    Else if (UPPER(@DB) = 'CN')                      
    begin                      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                      
    End                      
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                      
    begin                      
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                      
    End                      
    Else if(UPPER(@DB) = 'UK')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                      
    End                
    Else if(UPPER(@DB) = 'DE')                      
    begin                      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                      
    End                
                  
     SET @sqltxt ='INSERT INTO #tmp ( CompanyId,Branch , DueDate,OrderType)              
       select ord_cmpy_id as CompanyId,ord_ord_brh as Branch,orl_due_to_dt as DueDate,cds_desc              
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,              
       ' + @DB + '_ortchl_rec ,' + @DB + '_arrcuc_rec, ' + @DB + '_rprcds_rec where              
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id              
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id              
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no              
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id              
       and cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ            
       and orl_bal_qty >= 0               
       and chl_chrg_cl= ''E''              
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''              
       and orh_sts_actn <> ''C''              
       and ord_ord_pfx <>''QT''               
       and  (           
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )          
       or          
       (orl_due_to_dt is null and orh_ord_typ =''J'')) ' 
	   
	    if(@Branch = 'ROC')  
		begin 
		SET @sqltxt = @sqltxt + ' and (orh_shpg_whs = '''+ @Branch +''' or '''+ @Branch +'''= '''') '
		End
		else
		begin
		SET @sqltxt = @sqltxt + ' and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  '
		End

       SET @sqltxt = @sqltxt + ' and  ord_ord_brh not in (''SFS'') and cds_desc in (''Normal Order'', ''Job Detail Order'')             
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'              
       print('test')  ;          
     print(@sqltxt);               
    set @execSQLtxt = @sqltxt;               
       EXEC (@execSQLtxt);              
   --and ord_ord_pfx=''SO''               
     END              
   -- SELECT  * FROM #tmp    
   insert into #tmpFinal(ThisWeekOpenOrderCount,ThisMonthOpenOrderCount)    
   select (select COUNT(*)as thisweek from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)), 120)),    
   (select COUNT(*)as thisweek from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0) ,120)     
   and DueDate <= CONVERT(VARCHAR(10),dateadd(day, 0 - day(dateadd(month, 1 , GETDATE())), dateadd(month, 1 , GETDATE())), 120))    
   ;    
       
       
   select 'Week ' + cast(DATEPART(wk, GETDATE()) -3 as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 21), CAST(GETDATE() - 21-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 21), CAST(GETDATE() - 21-1 AS DATE)), 120)     
   ) as OpenOrderCount  -- previous 3 Week    
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount    
   , 1 as seq    
   Union       
   select 'Week ' + cast(DATEPART(wk, GETDATE()) -2 as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 14), CAST(GETDATE() - 14-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 14), CAST(GETDATE() - 14-1 AS DATE)), 120)     
   ) as OpenOrderCount  -- previous 2 Week    
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount    
   , 2 as seq    
   Union    
   select 'Week ' + cast(DATEPART(wk, GETDATE())-1  as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 7), CAST(GETDATE() - 7-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 7), CAST(GETDATE() - 7-1 AS DATE)), 120)     
   ) as OpenOrderCount   -- previous 1 Week    
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount    
   , 3 as seq    
  Union     
   select 'Week ' + cast(DATEPART(wk, GETDATE()) as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE()-1 AS DATE)), 120)     
   ) as OpenOrderCount   -- current Week    
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount    
   , 4 as seq    
Union    
select 'Week ' + cast(DATEPART(wk, GETDATE()) + 1 as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() + 7), CAST(GETDATE() + 7-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() + 7), CAST(GETDATE()+7-1 AS DATE)), 120)     
   ) as OpenOrderCount  --next 1 week    
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount    
   , 5 as seq    
Union    
select 'Week ' + cast(DATEPART(wk, GETDATE()) + 2 as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() + 14), CAST(GETDATE() + 14-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() + 14), CAST(GETDATE()+14-1 AS DATE)), 120)     
   ) as OpenOrderCount --next 2 week      
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount       
   , 6 as seq          
Union    
select 'Week ' + cast(DATEPART(wk, GETDATE()) + 3 as varchar(2))  as 'Category' ,     
   (select COUNT(*) from #tmp where DueDate >= CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() + 21), CAST(GETDATE() + 21-1 AS DATE)),120)     
   and DueDate <= CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() + 21), CAST(GETDATE()+ 21-1 AS DATE)), 120)     
   ) as OpenOrderCount --next 3 week     
   , (select ThisWeekOpenOrderCount from #tmpFinal) as ThisWeekOpenOrderCount    
   , (select ThisMonthOpenOrderCount from #tmpFinal) as ThisMonthOpenOrderCount                
   , 7 as seq    
       
   order by seq;    
       
   drop table #tmp;        
   drop table #tmpFinal;       
END              
              
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@OrderType varchar(10)              
              
-- exec [sp_itech_OpenOrder] '01/02/2018', '12/31/2018' , 'US','ALL','ALL','ALL'         
-- exec [sp_itech_OOPS_OpenOrder_Week] 'UK', 'BHM'         
-- exec [sp_itech_OOPS_OpenOrder_Week] 'US', 'ROC'         
/*  
Mail sub:Home > STRATIXReports > Development > Warehouse Slides  

Date: 20200430
Mail sub:Home > STRATIXReports > Development > Warehouse Slides
*/
GO
