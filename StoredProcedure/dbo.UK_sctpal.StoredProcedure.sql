USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_sctpal]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
           
-- =============================================                            
-- Author:  <Author,Mukesh>                            
-- Create date: <Create Date,June 28, 2015,>                            
-- Description: <Description,Open Orders,>                            
                            
-- =============================================                            
CREATE PROCEDURE [dbo].[UK_sctpal]                            
                             
AS                            
BEGIN                            
 -- SET NOCOUNT ON added to prevent extra result sets from                            
 -- interfering with SELECT statements.                            
 SET NOCOUNT ON;                            
     IF OBJECT_ID('dbo.UK_sctpal_rec', 'U') IS NOT NULL                            
  drop table dbo.UK_sctpal_rec ;                             
                            
                             
    -- Insert statements for procedure here                            
SELECT *                            
into  dbo.UK_sctpal_rec                            
  from [LIVEUKSTX].[liveukstxdb].[informix].[sctpal_rec] where pal_aud_ctl_no not in ('413211','413214','428996','429000','429005','429011'                      
  ,'430674'      -- 2018020                      
  ,'436007','436016','436011'  --20180606                     
  ,'436084'  -- 20180608                 
  ,'436324','436331'  -- 20180622             
  ,'454382'            
  ,'454383'               
  ,'442270','480051','480056', '480065','483517','488123','489463'); -- 20190115                     
END                            
-- select * from UK_sctpal_rec                         
                        
/*                        
problem in column pal_rec_txt which is used in reprots                        
sp_itech_CurrencyChange                        
sp_itech_CategoryChange      
20200904 Sumit      
skip pal_aud_ctl_no 480051,480056, 480065      
Reason : Cannot get the current row value of column "[LIVEUKSTX].[liveukstxdb].[informix].[sctpal_rec].pal_rec_txt" from OLE DB provider "Ifxoledbc" for linked server "LIVEUKSTX".       
Could not convert the data value due to reasons other than sign mismatch or overflow.    
20201016 Sumit    
skip pal_aud_ctl_no 483517    
20210119 Sumit  
skip pal_aud_ctl_no 488123  
20210303	Sumit
skip pal_aud_ctl_no 489463
*/
GO
