USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_pfhdpf]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_pfhdpf]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.CN_pfhdpf_rec', 'U') IS NOT NULL          
  drop table dbo.CN_pfhdpf_rec;          
              
                  
SELECT *          
into  dbo.CN_pfhdpf_rec 
FROM [LIVECNSTX].[livecnstxdb].[informix].[pfhdpf_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
