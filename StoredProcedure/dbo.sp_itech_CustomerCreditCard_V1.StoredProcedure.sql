USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerCreditCard_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <01 Dec 2015>    
-- Description: <Booking Daily Reports>   
-- =============================================    
CREATE  PROCEDURE [dbo].[sp_itech_CustomerCreditCard_V1]  @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@IncludePierce char = '0'   
AS    
BEGIN    
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
   
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @CurrenyRate varchar(15);    
declare @FD varchar(10)  
declare @TD varchar(10)    
    
Set @DB =@DBNAME;  
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
CREATE TABLE #temp ( Dbname   VARCHAR(10)    
     ,CusID   VARCHAR(10)    
     ,CusLongNm  VARCHAR(40)    
     ,Branch   VARCHAR(3)    
     ,MthdPmt  VARCHAR(10)   
     , TotWeight  DECIMAL(20, 0)  
      , TotalValue    DECIMAL(20, 2)  
      , NetGP               DECIMAL(20, 2)  
      ,NetGPPct DECIMAL(20, 2)  
     );    
  
    
IF @DBNAME = 'ALL'    
 BEGIN    
     
  IF (@IncludePierce = '0')  
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
   OPEN ScopeCursor;    
    END    
    ELSE    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    END    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   DECLARE @query NVARCHAR(MAX);    
   IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End
	Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End
        
     
    SET @query = 'INSERT INTO #temp (Dbname, CusID, CusLongNm, Branch, MthdPmt,TotWeight,TotalValue,NetGP,NetGPPct)    
      select cus_cmpy_id as Company, cus_cus_id as CustomerID, cus_cus_long_nm as CustomerName, Replace(cus_admin_brh,''PSM'',''LAX'') as AdminBranch, crd_mthd_pmt as MethodOfPayment,  '  
     if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                 
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,'              
           ELSE              
            SET @query = @query + ' SUM(stn_blg_wgt) as TotWeight,'              
                         
           SET @query = @query + ' SUM(ISNULL(stn_tot_val,0)) as TotalValue, SUM(ISNULL(stn_npft_avg_val,0)) as NetGP,  
     (case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0)))*100 end ) as NetGPPct  
        
      from ' + @Prefix + '_arrcus_rec   
 join ' + @Prefix + '_arrcrd_rec  on cus_cmpy_id = crd_cmpy_id and  cus_cus_id = crd_cus_id   
 join ' + @Prefix + '_sahstn_rec on crd_cmpy_id = stn_cmpy_id and  crd_cus_id = stn_sld_cus_id   
 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +''' and crd_mthd_Pmt = ''CC''  Group By cus_cmpy_id,cus_cus_id,cus_cus_long_nm, Replace(cus_admin_brh,''PSM'',''LAX''),crd_mthd_pmt  
 '    
      
   print @query;    
   EXECUTE sp_executesql @query;    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  END       
  CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
ELSE    
BEGIN    
  
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)    
   
 IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End
	Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End
        
    
   SET @sqltxt =' INSERT INTO #temp (Dbname, CusID, CusLongNm, Branch, MthdPmt,TotWeight,TotalValue,NetGP,NetGPPct)   
    select cus_cmpy_id as Company, cus_cus_id as CustomerID, cus_cus_long_nm as CustomerName, Replace(cus_admin_brh,''PSM'',''LAX'') as AdminBranch, crd_mthd_pmt as MethodOfPayment, '  
       
     if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')                 
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,'              
           ELSE              
            SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as TotWeight,'              
                         
           SET @sqltxt = @sqltxt + '  SUM(ISNULL(stn_tot_val,0)) as TotalValue, SUM(ISNULL(stn_npft_avg_val,0)) as NetGP,  
     (case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0)))*100 end ) as NetGPPct  
      from ' + @DB + '_arrcus_rec   
 join ' + @DB + '_arrcrd_rec  on cus_cmpy_id = crd_cmpy_id and  cus_cus_id = crd_cus_id   
 join ' + @DB + '_sahstn_rec on crd_cmpy_id = stn_cmpy_id and  crd_cus_id = stn_sld_cus_id   
 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +''' and crd_mthd_Pmt = ''CC''  Group By cus_cmpy_id,cus_cus_id,cus_cus_long_nm, Replace(cus_admin_brh,''PSM'',''LAX''),crd_mthd_pmt  
 '    
   
 print(@sqltxt)    
 set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
END    
 SELECT * FROM #temp ;    
 DROP TABLE  #temp;    
END    
    
--EXEC [sp_itech_CustomerCreditCard_V1] '2015-11-04','2016-01-04','ALL'    
--EXEC [sp_itech_CustomerCreditCard_V1] '2015-11-04','2016-01-04','CA'    
/*
20210128	Sumit
Add germany database
*/
GO
