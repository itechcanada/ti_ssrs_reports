USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_injitd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,12/2/2013,>  
-- Description: <>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_injitd]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.UK_injitd_rec', 'U') IS NOT NULL        
  drop table dbo.UK_injitd_rec;        
            
                
SELECT *        
into  dbo.UK_injitd_rec 
FROM [LIVEUKSTX].[liveukstxdb].[informix].[injitd_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
