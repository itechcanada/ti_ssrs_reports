USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_DMS_OpenOrderSummary]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukes>              
-- Create date: <05-04-2019>              
-- Description: <Booking to Invoice Ratio>             
          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_DMS_OpenOrderSummary]  @DBNAME varchar(50), @Branch varchar(3)            
AS              
BEGIN              
            
 -- SET NOCOUNT ON added to prevent extra result sets from              
 SET NOCOUNT ON;              
declare @DB varchar(100);              
declare @sqltxt varchar(6000);              
declare @execSQLtxt varchar(7000);              
DECLARE @CountryName VARCHAR(25);                 
DECLARE @prefix VARCHAR(15);                 
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @CurrenyRate varchar(15);            
declare @FD varchar(10)                
declare @TD varchar(10)         
declare @TDInv varchar(10)                     
              
   set @FD = CONVERT(VARCHAR(10),  DATEADD(year, -1, GETDATE()),120)    -- One year back     
 set @TD = CONVERT(VARCHAR(10), DATEADD(year, 1, GETDATE()) ,120)      -- One year after   
            
             
IF @Branch = 'ALL'              
 BEGIN              
  set @Branch = ''              
 END    
            
              
CREATE TABLE #temp ( CompanyId varchar(5)               
        ,OrderPrefix Varchar(2)              
        ,OrderNo varchar(10)              
        ,OrderItem   varchar(3)              
        ,CustID   VARCHAR(10)               
        , ChargeValue    DECIMAL(20, 2)              
        , DueDate    varchar(20)              
        , OrderType     varchar(65)              
     );             
              
IF @DBNAME = 'ALL'              
 BEGIN              
               
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
                
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  WHILE @@FETCH_STATUS = 0              
  BEGIN              
   DECLARE @query NVARCHAR(MAX);              
   IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End              
                  
    SET @query = 'INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, ChargeValue, DueDate, OrderType)              
      select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,  
       ord_sld_cus_id as CustID  
       ,chl_chrg_val * '+ @CurrenyRate +' as ChargeValue  
       ,orl_due_to_dt as DueDate,  
       cds_desc            
       from ' + @Prefix + '_ortord_rec,' + @Prefix + '_arrcus_rec ,' + @Prefix + '_ortorh_rec ,' + @Prefix + '_ortorl_rec,' + @Prefix + '_ortchl_rec,  
       ' + @Prefix + '_arrcuc_rec, ' + @Prefix + '_rprcds_rec , ' + @Prefix + '_ortcht_rec   
      where cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no   
       and ord_cmpy_id=orh_cmpy_id and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ and ord_cmpy_id = cht_cmpy_id   AND ord_ord_pfx = cht_ref_pfx AND  
       ord_ord_no  = cht_ref_no AND ord_ord_itm = cht_ref_itm   
        and cht_tot_typ = ''T''  
       and orl_bal_qty >= 0   
       and chl_chrg_cl= ''E''   
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''  
       and orh_sts_actn <> ''C''  
       and ord_ord_pfx <>''QT''   
       and  (  
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10),''' + @TD + ''' , 120)   )  
       or   
       (orl_due_to_dt is null and orh_ord_typ =''J''))   
       and  ord_ord_brh not in (''SFS'')  
AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
         '             
   print @query;              
   EXECUTE sp_executesql @query;              
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;              
  END                 
  CLOSE ScopeCursor;              
  DEALLOCATE ScopeCursor;              
 END              
ELSE              
BEGIN              
             
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)              
 IF (UPPER(@DBNAME) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DBNAME) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DBNAME) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DBNAME) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DBNAME) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@DBNAME) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End              
    Else if(UPPER(@DBNAME) = 'TWCN')              
    begin              
       SET @DB ='TW'              
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
                  
                
  SET @sqltxt ='INSERT INTO #temp (CompanyId,OrderPrefix ,OrderNo ,OrderItem,CustID, ChargeValue, DueDate, OrderType)              
      select distinct ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,  
       ord_sld_cus_id as CustID  
       ,chl_chrg_val * '+ @CurrenyRate +' as ChargeValue  
       ,orl_due_to_dt as DueDate,  
       cds_desc            
       from ' + @DBNAME + '_ortord_rec,' + @DBNAME + '_arrcus_rec ,' + @DBNAME + '_ortorh_rec ,' + @DBNAME + '_ortorl_rec,' + @DBNAME + '_ortchl_rec,  
       ' + @DBNAME + '_arrcuc_rec, ' + @DBNAME + '_rprcds_rec , ' + @DBNAME + '_ortcht_rec   
       where cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no   
       and ord_cmpy_id=orh_cmpy_id and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
       and  cuc_cus_cat = cus_cus_cat and cds_cd=orh_ord_typ and ord_cmpy_id = cht_cmpy_id   AND ord_ord_pfx = cht_ref_pfx AND  
       ord_ord_no  = cht_ref_no AND ord_ord_itm = cht_ref_itm   
        and cht_tot_typ = ''T''  
       and orl_bal_qty >= 0   
       and chl_chrg_cl= ''E''   
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''  
       and orh_sts_actn <> ''C''  
       and ord_ord_pfx <>''QT''   
       and  (  
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), ''' + @FD + ''', 120)  and  CONVERT(VARCHAR(10),''' + @TD + ''' , 120)   )  
       or   
       (orl_due_to_dt is null and orh_ord_typ =''J''))   
       and  ord_ord_brh not in (''SFS'')  
AND (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
          '            
 print(@sqltxt)              
 set @execSQLtxt = @sqltxt;               
 EXEC (@execSQLtxt);              
END       
  
  
select '1' as seq, 'Total Open Lines' as Category, COUNT(*) as CountValue,ISNULL(SUM(ChargeValue),0) as Value, 0 as PCT  
from #temp  
Union  
select '2' as seq, 'Normal Order Lines' as Category, COUNT(*) as CountValue,ISNULL(SUM(ChargeValue),0) as Value, 
case when (select ISNULL(SUM(tt.ChargeValue),0) from #temp tt) = 0 then 0 else ISNULL(SUM(ChargeValue),0)/(select ISNULL(SUM(tt.ChargeValue),0) from #temp tt)
 end as PCT   
from #temp where OrderType='Normal Order'     
Union  
select '3' as seq, 'Job Summary Order Lines' as Category, COUNT(*) as CountValue,ISNULL(SUM(ChargeValue),0) as Value, 
case when (select ISNULL(SUM(tt.ChargeValue),0) from #temp tt) = 0 then 0 else ISNULL(SUM(ChargeValue),0)/(select ISNULL(SUM(tt.ChargeValue),0) from #temp tt) 
end as PCT   
from #temp where OrderType='Job Summary Order'     
Union  
select '4' as seq, 'Job Detail Order Lines' as Category, COUNT(*) as CountValue,ISNULL(SUM(ChargeValue),0) as Value, 
case when (select ISNULL(SUM(tt.ChargeValue),0) from #temp tt) = 0 then 0 else ISNULL(SUM(ChargeValue),0)/(select ISNULL(SUM(tt.ChargeValue),0) from #temp tt) 
end  as PCT   
from #temp where OrderType='Job Detail Order'   ;   
  
   
 DROP TABLE  #temp;             
END              
              
              
-- @DBNAME varchar(50), @Branch varchar(3)              
-- EXEC [sp_itech_DMS_OpenOrderSummary] 'UK','DEU'  -- 125.99   ,558576      
/*      
   
*/ 
GO
