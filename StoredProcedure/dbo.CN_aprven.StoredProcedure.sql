USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_aprven]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CN_aprven]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.CN_aprven_rec', 'U') IS NOT NULL          
  drop table dbo.CN_aprven_rec;          
              
                  
SELECT *          
into  dbo.CN_aprven_rec   
FROM [LIVECNSTX].[livecnstxdb].[informix].[aprven_rec]     
    
END    
    
GO
