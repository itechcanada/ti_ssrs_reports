USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_inrmat]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 8, 2015,>
-- Description:	<Description,Material,>

-- =============================================
create PROCEDURE [dbo].[US_inrmat]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_inrmat_rec', 'U') IS NOT NULL
		drop table dbo.US_inrmat_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_inrmat_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[inrmat_rec] ; 
  
END
-- select * from US_inrmat_rec
GO
