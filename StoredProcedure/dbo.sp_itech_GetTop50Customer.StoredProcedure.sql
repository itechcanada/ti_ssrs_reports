USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTop50Customer]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- Last updated by : Mukesh 
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetTop50Customer] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@NOOfTopCustomer integer, @UKExchangeRate Money, @TWExchangeRate Money, @CAExchangeRate Money, @NOExchangeRate Money

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)

set @DB=  @DBNAME 

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)
set @NOOfCust= convert(varchar,@NOOfTopCustomer)

CREATE TABLE #tmp (    CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
                    , TotalValue  	 DECIMAL(20, 2)
   					, NProfitPercentage               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, GProfitPercentage			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15)
   					,Cry           Varchar(5) 
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35); 
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				SET @DB= @prefix	
  				print(@DB)
  				if UPPER(@Name) = 'CANADA'
			      BEGIN
  				   if  @CAExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@CAExchangeRate)
  				  END    
  			  ELSE if UPPER(@Name) = 'TAIWAN'	      
  				    BEGIN
  				    if @TWExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@TWExchangeRate)
  				    END  
  			  ELSE if UPPER(@Name) = 'NORWAY'
                   BEGIN 			  
  				   if  @NOExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@NOExchangeRate)
  				   END   
  			  ELSE if UPPER(@Name) = 'UK'
  			       BEGIN
  				   if  @UKExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@UKExchangeRate)
  				   END
  			  ELSE if UPPER(@Name) = 'USA'
  			       BEGIN
  			       SET @ExchangeRate = convert(varchar,1.00)   
				   END
  				
  				SET @query ='INSERT INTO #tmp (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry)
							select  stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh as Branch,
							SUM(stn_tot_val * '+ @ExchangeRate +') as TotalValue, 
							AVG(stn_npft_avg_pct) as NProfitPercentage, 
							SUM(stn_tot_mtl_val * '+ @ExchangeRate +') as TotalMatValue , 
							AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt
							order by TotalValue desc '
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			  Set @Name=(select Name from tbl_itech_DatabaseName_PS   where Prefix =''+ @DB +'')
			  if UPPER(@Name) = 'CANADA'
			      BEGIN
  				   if  @CAExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@CAExchangeRate)
  				  END    
  			  ELSE if UPPER(@Name) = 'TAIWAN'	      
  				    BEGIN
  				    if  @TWExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@TWExchangeRate)
  				    END  
  			  ELSE if UPPER(@Name) = 'NORWAY'
                   BEGIN 			  
  				   if  @NOExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@NOExchangeRate)
  				   END   
  			  ELSE if UPPER(@Name) = 'UK'
  			       BEGIN
  				   if   @UKExchangeRate IS NOT NULL 
  				      SET @ExchangeRate = convert(varchar,@UKExchangeRate)
  				   END
  			  ELSE if UPPER(@Name) = 'USA'
  			       BEGIN
  			       SET @ExchangeRate = convert(varchar,1.00)   
				   END
				  print(@ExchangeRate)
				  --  top '+ @NOOfCust +'
			   SET @sqltxt = 'INSERT INTO #tmp (CustID, CustName,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry)
							select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,stn_shpt_brh as Branch,
							SUM(stn_tot_val * '+ @ExchangeRate +') as TotalValue, 
							AVG(stn_npft_avg_pct) as NProfitPercentage, 
							SUM(stn_tot_mtl_val * '+ @ExchangeRate +') as TotalMatValue , 
							AVG(stn_mpft_avg_pct) as GProfitPercentage, '''+ @Name +''' as Databases,stn_cry as Cry
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_sld_cus_id, cus_cus_nm, cuc_desc30,stn_shpt_brh,stn_cry,crx_xexrt
							order by TotalValue desc '
							print(@sqltxt)
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
     --if (@NOOfTopCustomer >5 And  @DBNAME = 'ALL')
  			--	 BEGIN
  			--	  SET @NOOfTopCustomer= @NOOfTopCustomer / 5
  			--	 END
     --TOP(@NOOfTopCustomer)
    -- SET @NOOfTopCustomer= @NOOfTopCustomer
  SELECT TOP(@NOOfTopCustomer) * FROM #tmp order by TotalValue desc;
END

-- exec sp_itech_GetTop50Customer '01/01/2012', '1/31/2013' , 'ALL',100,0,1,1,0 
-- exec sp_itech_GetTop50Customer '01/01/2012', '1/31/2013' , 'ALL',50,1.6,0,0,1  




GO
