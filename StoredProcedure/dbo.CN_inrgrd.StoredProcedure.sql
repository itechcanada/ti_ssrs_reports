USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_inrgrd]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[CN_inrgrd] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_inrgrd_rec', 'U') IS NOT NULL
		drop table dbo.CN_inrgrd_rec;
    
        
SELECT *
into  dbo.CN_inrgrd_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[inrgrd_rec];

END
-- select * from CN_inrgrd_rec
GO
