USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ivtivh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[UK_ivtivh] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.UK_ivtivh_rec', 'U') IS NOT NULL
		drop table dbo.UK_ivtivh_rec;
    
        
SELECT *
into  dbo.UK_ivtivh_rec
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ivtivh_rec];

END

--- exec US_ivtivh
-- select * from US_ivtivh_rec
GO
