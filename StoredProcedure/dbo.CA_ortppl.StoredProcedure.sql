USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_ortppl]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: jun 25, 2019 
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[CA_ortppl]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.CA_ortppl_rec', 'U') IS NOT NULL  
  drop table dbo.CA_ortppl_rec;  
          
SELECT *  
into  dbo.CA_ortppl_rec  
FROM [LIVECASTX].[livecastxdb].[informix].[ortppl_rec];  
  
END  
-- select * from CA_ortppl_rec
GO
