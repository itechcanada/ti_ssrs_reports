USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_rvtreh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Sumit>
-- Create date: <Create Date,Oct 6, 2021,>
-- Description:	<Description,Reservation Update>

-- =============================================
Create PROCEDURE [dbo].[DE_rvtreh]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.DE_rvtreh_rec', 'U') IS NOT NULL
		drop table dbo.DE_rvtreh_rec;	

    -- Insert statements for procedure here
SELECT *
into  dbo.DE_rvtreh_rec
  from [LIVEDESTX].[livedestxdb].[informix].[rvtreh_rec] ; 
  
END
GO
