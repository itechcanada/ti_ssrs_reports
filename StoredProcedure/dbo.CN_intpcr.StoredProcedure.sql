USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_intpcr]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Feb 11, 2016,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
create PROCEDURE [dbo].[CN_intpcr]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_intpcr_rec', 'U') IS NOT NULL  
  drop table dbo.CN_intpcr_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_intpcr_rec  
  from [LIVECNSTX].[livecnstxdb].[informix].[intpcr_rec] ;   
    
END  
-- select * from CN_intpcr_rec 
GO
