USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_tctnad]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 18, 2019,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[US_tctnad]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_tctnad_rec', 'U') IS NOT NULL
		drop table dbo.US_tctnad_rec ;	

	
    -- Insert statements for procedure here
SELECT nad_cmpy_id,nad_Ref_pfx,nad_Ref_no,nad_Ref_itm, nad_addr_typ,nad_pcd, nad_nm1 
into  dbo.US_tctnad_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[tctnad_rec] ; 
  
END
-- select * from US_tctnad_rec
GO
