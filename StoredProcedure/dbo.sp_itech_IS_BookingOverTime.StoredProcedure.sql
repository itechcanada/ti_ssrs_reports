USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_IS_BookingOverTime]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukes>            
-- Create date: <12-03-2019>            
-- Description: <Booking Daily Reports>           
        
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_IS_BookingOverTime]  @DBNAME varchar(50), @Branch varchar(3), @ISSLP varchar(4)           
AS            
BEGIN            
          
 -- SET NOCOUNT ON added to prevent extra result sets from            
 SET NOCOUNT ON;            
declare @DB varchar(100);            
declare @sqltxt varchar(6000);            
declare @execSQLtxt varchar(7000);            
DECLARE @CountryName VARCHAR(25);               
DECLARE @prefix VARCHAR(15);               
DECLARE @DatabaseName VARCHAR(35);                
DECLARE @CurrenyRate varchar(15);          
declare @FD8 varchar(10)              
declare @TD8 varchar(10)  
declare @FD7 varchar(10)              
declare @TD7 varchar(10)  
declare @FD6 varchar(10)              
declare @TD6 varchar(10)  
declare @FD5 varchar(10)              
declare @TD5 varchar(10)  
declare @FD4 varchar(10)              
declare @TD4 varchar(10)  
declare @FD3 varchar(10)              
declare @TD3 varchar(10)  
declare @FD2 varchar(10)              
declare @TD2 varchar(10)  
declare @FD1 varchar(10)              
declare @TD1 varchar(10)  
declare @FD varchar(10)              
declare @TD varchar(10)       
--declare @FD12 varchar(10)                   
  
set @FD8 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 56), CAST(GETDATE() - 56-1 AS DATE)),120)         
set @TD8 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 56), CAST(GETDATE() - 56-1 AS DATE)), 120)  
   
set @FD7 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 49), CAST(GETDATE() - 49-1 AS DATE)),120)         
set @TD7 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 49), CAST(GETDATE() - 49-1 AS DATE)), 120)  
  
set @FD6 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 42), CAST(GETDATE() - 42-1 AS DATE)),120)         
set @TD6 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 42), CAST(GETDATE() - 42-1 AS DATE)), 120)  
  
set @FD5 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 35), CAST(GETDATE() - 35-1 AS DATE)),120)         
set @TD5 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 35), CAST(GETDATE() - 35-1 AS DATE)), 120)  
  
set @FD4 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 28), CAST(GETDATE() - 28-1 AS DATE)),120)         
set @TD4 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 28), CAST(GETDATE() - 28-1 AS DATE)), 120)  
  
set @FD3 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 21), CAST(GETDATE() - 21-1 AS DATE)),120)         
set @TD3 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 21), CAST(GETDATE() - 21-1 AS DATE)), 120)  
  
set @FD2 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 14), CAST(GETDATE() - 14-1 AS DATE)),120)         
set @TD2 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 14), CAST(GETDATE() - 14-1 AS DATE)), 120)  
  
set @FD1 = CONVERT(VARCHAR(10),DATEADD(DAY, 2 - DATEPART(WEEKDAY, GETDATE() - 7), CAST(GETDATE() - 7-1 AS DATE)),120)         
set @TD1 = CONVERT(VARCHAR(10),DATEADD(DAY, 8 - DATEPART(WEEKDAY, GETDATE() - 7), CAST(GETDATE() - 7-1 AS DATE)), 120)  
   
             
set @FD = CONVERT(VARCHAR(10), DATEADD(week, -8, GETDATE()),120)          
set @TD = CONVERT(VARCHAR(10), GETDATE(),120)       
 --set @FD12 = CONVERT(VARCHAR(10), DATEADD(month, -12 ,@toDate) ,120)        
          
           
IF @Branch = 'ALL'            
 BEGIN            
  set @Branch = ''            
 END  
         
 declare @start varchar = ''          
            
CREATE TABLE #temp ( Dbname   VARCHAR(10)            
     --,DBCountryName VARCHAR(25)           
     ,ISlp  VARCHAR(4)          
     --,OSlp  VARCHAR(4)          
     --,CusID   VARCHAR(10)            
     --,CusLongNm  VARCHAR(40)            
     --,Branch   VARCHAR(3)            
     ,ActvyDT  VARCHAR(10)            
     --,OrderNo  NUMERIC            
     --,OrderItm  NUMERIC            
     --,Product  VARCHAR(500)            
     --,Wgt   decimal(20,0)            
     --,TotalMtlVal decimal(20,0)            
     --,ReplCost  decimal(20,0)            
     --,Profit   decimal(20,1)            
     , TotalSlsVal decimal(20,0)           
     --,Market Varchar(35)        
     --,ReplProfit decimal(20,0)      
     --,ObsolateInvBooking decimal(20,1)      
     );            
            
IF @DBNAME = 'ALL'            
 BEGIN            
             
  DECLARE ScopeCursor CURSOR FOR            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS             
    OPEN ScopeCursor;            
              
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;            
  WHILE @@FETCH_STATUS = 0            
  BEGIN            
   DECLARE @query NVARCHAR(MAX);            
   IF (UPPER(@Prefix) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@Prefix) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@Prefix) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@Prefix) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@Prefix) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End            
    Else if(UPPER(@Prefix) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End            
                
    SET @query = 'INSERT INTO #temp (Dbname,  ISlp, ActvyDT, TotalSlsVal)            
      SELECT '''+ @Prefix +''' as Country,  mbk_is_slp,  
      a.mbk_actvy_dt,  
       a.mbk_tot_val* '+ @CurrenyRate +'      
      FROM ' + @Prefix + '_ortmbk_rec a            
      INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id            
      WHERE a.mbk_ord_pfx=''SO''             
        AND a.mbk_ord_itm<>999             
         and a.mbk_trs_md = ''A''              
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD8 + '''  And ''' + @TD + '''   
           and mbk_is_slp = ''' + @ISSLP +''' AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
         '          
                    
            
   print @query;            
   EXECUTE sp_executesql @query;            
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;            
  END               
  CLOSE ScopeCursor;            
  DEALLOCATE ScopeCursor;            
 END            
ELSE            
BEGIN            
           
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)            
 IF (UPPER(@DBNAME) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@DBNAME) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@DBNAME) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@DBNAME) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@DBNAME) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End            
    Else if(UPPER(@DBNAME) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End            
    Else if(UPPER(@DBNAME) = 'TWCN')            
    begin            
       SET @DB ='TW'            
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
                
              
  SET @sqltxt ='INSERT INTO #temp (Dbname,  ISlp, ActvyDT, TotalSlsVal)            
      SELECT '''+ @DBNAME +''' as Country,  mbk_is_slp,  
      a.mbk_actvy_dt,  
       a.mbk_tot_val* '+ @CurrenyRate +'      
      FROM ' + @DBNAME + '_ortmbk_rec a            
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id            
      WHERE a.mbk_ord_pfx=''SO''             
        AND a.mbk_ord_itm<>999             
         and a.mbk_trs_md = ''A''              
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD8 + '''  And ''' + @TD + '''   
           and mbk_is_slp = ''' + @ISSLP +''' AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  '           
           --         
 print(@sqltxt)            
 set @execSQLtxt = @sqltxt;             
 EXEC (@execSQLtxt);            
END            


 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -8 as varchar(2))  as 'Category' , Sum(TotalSlsVal) as TotalSlsVal   
 ,'1' as seq FROM #temp   
 where ActvyDT >= @FD8 and ActvyDT <= @TD8 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -7 as varchar(2))  as 'Category' , Sum(TotalSlsVal) as TotalSlsVal   
 ,'2' as seq FROM #temp   
 where ActvyDT >= @FD7 and ActvyDT <= @TD7 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -6 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'3' as seq FROM #temp   
 where ActvyDT >= @FD6 and ActvyDT <= @TD6 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -5 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'4' as seq FROM #temp   
 where ActvyDT >= @FD5 and ActvyDT <= @TD5 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -4 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'5' as seq FROM #temp   
 where ActvyDT >= @FD4 and ActvyDT <= @TD4 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -3 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'6' as seq FROM #temp   
 where ActvyDT >= @FD3 and ActvyDT <= @TD3 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -2 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'7' as seq  FROM #temp   
 where ActvyDT >= @FD2 and ActvyDT <= @TD2 group by Dbname  
 Union  
 SELECT Dbname, 'Week ' + cast(DATEPART(wk, GETDATE()) -1 as varchar(2))  as 'Category' , SUM(TotalSlsVal) as TotalSlsVal   
 ,'8' as seq FROM #temp   
 where ActvyDT >= @FD1 and ActvyDT <= @TD1 group by Dbname  
 order by seq;   
   
   
           
 DROP TABLE  #temp;            
END            
            
            
            
-- EXEC [sp_itech_IS_BookingOverTime] 'US','LAX', 'CS'           
/*    
sp_itech_OOPS_OpenOrder_Week from where week calculation   
*/ 
GO
