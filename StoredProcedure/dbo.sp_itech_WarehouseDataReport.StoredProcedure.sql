USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_WarehouseDataReport]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mrinal >        
-- Create date: <08 Mar 2017>        
-- Description: <Getting WarehouseDataReport >  
    
-- =============================================        
 CREATE PROCEDURE [dbo].[sp_itech_WarehouseDataReport] @DBNAME varchar(50),@FromDate datetime, @ToDate datetime    
        
AS        
BEGIN        
         
 SET NOCOUNT ON;     
    
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)         
declare @NFD varchar(10)      
declare @NTD varchar(10)      
declare @D3MFD varchar(10)      
declare @D3MTD varchar(10)      
declare @D6MFD varchar(10)      
declare @D6MTD varchar(10)      
declare @LFD varchar(10)      
declare @LTD varchar(10)     
declare @AFD varchar(10)      
declare @ATD varchar(10)      
declare @FD2 varchar(10)        
declare @TD2 varchar(10)    
declare @RFD varchar(10)    
declare @RTD varchar(10)    
    
declare @ExcessFD varchar(10)    
declare @ExcessTD varchar(10)    
     
set @DB=  @DBNAME         
 set @FD = CONVERT(VARCHAR(10), @FromDate,120)    
 set @TD = CONVERT(VARCHAR(10), @ToDate,120)     
     
 set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @RTD = CONVERT(VARCHAR(10), @ToDate,120)   -- New Account      
     
 set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)  -- New Account      
      
       set @D3MFD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate)-3, 0),120)   -- Dormant 3 Month  correct    
       set @D3MTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-3, -1),120)  -- Dormant 3 Month correct     
      
       set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-6,0)) , 120)   --Dormant 6 Month      
       set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-5,0)), 120)  -- Dormant 6 Month      
           
    
  set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date    
  set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date    
    
       set @ExcessFD =  CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)    
       set @ExcessTD =   @FD    
       print 'ExFD ' + @ExcessTD    
           
       set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)   -- Lost Account      
       set @LTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-18, -1),120)  -- Lost Account  correct    
                 
       set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)    -- Active Accounts       
       set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)      
      
    
CREATE TABLE #tmp1 (    Databases   VARCHAR(15)              
        , Whs   VARCHAR(3)  
        , lbs    DECIMAL(20, 0)        
        , Pieces        int        
        , Bol varchar(100)      
        , UnadjustedOTP varchar(50)  
        , AdjustedOTP varchar(50)  
        , Hourss varchar(50)  
        , lblPerHour varchar(50)  
        , PiecesPerHour varchar(50)  
        ,BolPerHour varchar(50)  
                   );      
                       
                       
  CREATE TABLE #tmpBol1 (    Databases   VARCHAR(15)              
        , Whs   VARCHAR(3)     
        , Bol int  
        );  
        
        
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @Prefix VARCHAR(5);        
DECLARE @Name VARCHAR(15);        
        
       
        
--if @Branch ='ALL'        
-- BEGIN        
-- set @Branch = ''        
-- END        
        
DECLARE @CurrenyRate varchar(15);        
        
IF @DBNAME = 'ALL'      
 BEGIN        
         
    
  BEGIN    
         
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
End          
          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
      SET @DB= @Prefix        
     -- print(@DB)        
        DECLARE @query NVARCHAR(4000);        
         IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
          
          
          
          
        SET @query = 'insert into #tmpBol1(Databases, Whs, Bol)  
        SELECT '''+ @DB +''', whs_whs, COUNT(*) as bol FROM         
'+ @DB +'_mxtarh_Rec         
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no        
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''        
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs        
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1     
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''   
group by whs_whs   
        '  
          
    print(@query)          
    EXECUTE sp_executesql @query;   
               
              
          SET @query = 'insert into #tmp1 (Databases, Whs, lbs , Pieces , Bol, UnadjustedOTP , AdjustedOTP , Hourss, lblPerHour, PiecesPerHour,BolPerHour)   
            
          SELECT '''+ @DB +''' as Databases, stn_shpg_whs, sum(stn_blg_wgt) as lbs, SUM(stn_blg_pcs)as pcs, ISNULL(MAX(b.bol), 0) as bol, '''' as UnAdjOtp  
, '''' as AdjOtp, '''' ,'''','''',''''   
FROM  '+ @DB +'_sahstn_rec a left join #tmpBol1 b on a.stn_shpg_whs = b.Whs   
where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''   
group by a.stn_shpg_whs   '  
      
      -- drop table #tmpBol1;      
               
     print(@query)          
     EXECUTE sp_executesql @query;        
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
       print 'starting' ;        
      IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DB ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
     print 'Ending';        
     print @CurrenyRate ;   
       
     CREATE TABLE #tmpBol (    Databases   VARCHAR(15)              
        , Whs   VARCHAR(3)     
        , Bol int  
        );  
          
        SET @sqltxt = 'insert into #tmpBol1(Databases, Whs, Bol)  
        SELECT '''+ @DB +''', whs_whs, COUNT(*) as bol FROM         
'+ @DB +'_mxtarh_Rec         
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no        
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''        
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs        
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1     
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''   
group by whs_whs   
        '  
          
         print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
  EXEC (@execSQLtxt);   
               
              
          SET @sqltxt = 'insert into #tmp1 (Databases, Whs, lbs , Pieces , Bol, UnadjustedOTP , AdjustedOTP , Hourss, lblPerHour, PiecesPerHour,BolPerHour)   
            
          SELECT '''+ @DB +''' as Databases, stn_shpg_whs, sum(stn_blg_wgt) as lbs, SUM(stn_blg_pcs)as pcs, ISNULL(MAX(b.bol), 0) as bol, '''' as UnAdjOtp  
, '''' as AdjOtp, '''' ,'''','''',''''   
FROM  '+ @DB +'_sahstn_rec a left join #tmpBol1 b on a.stn_shpg_whs = b.Whs   
where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''   
group by a.stn_shpg_whs   '  
       
                  
       END         
            
     
             
  print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
  EXEC (@execSQLtxt);        
            
  select * from #tmp1 where whs <> '' ;  
 drop table #tmp1;  
 drop table #tmpBol1;  
    
 -- where  (Branch not in ('TAI') or [Databases] = 'US') ;    
  -- select * from #tmp1  
 -- select * from #NLDAccount;        
  -- drop table #tmp1    ;    
END        
        
-- exec [sp_itech_WarehouseDataReport] 'ALL','2016-03-01', '2016-03-31'     
-- exec sp_itech_WarehouseDataReport 'US','2016-06-01', '2016-06-30'     
-- exec sp_itech_WarehouseDataReport 'CA','2016-06-01', '2016-06-30'     
-- exec sp_itech_WarehouseDataReport 'UK','2016-06-01', '2016-06-30'     
    
    
GO
