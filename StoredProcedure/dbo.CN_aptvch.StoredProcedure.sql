USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_aptvch]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: Dec 12, 2015
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CN_aptvch] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_aptvch_rec', 'U') IS NOT NULL
		drop table dbo.CN_aptvch_rec;
    
        
SELECT *
into  dbo.CN_aptvch_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[aptvch_rec];

END
--  exec  CN_aptvch
-- select * from CN_aptvch_rec
GO
