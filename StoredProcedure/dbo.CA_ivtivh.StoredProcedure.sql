USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_ivtivh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CA_ivtivh] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_ivtivh_rec', 'U') IS NOT NULL
		drop table dbo.CA_ivtivh_rec;
    
        
SELECT *
into  dbo.CA_ivtivh_rec
FROM [LIVECASTX].[livecastxdb].[informix].[ivtivh_rec];

END

--- exec CA_ivtivh
-- select * from CA_ivtivh_rec
GO
