USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_glbacb]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,11/19/2020,>    
-- Description: <Description,Germany glbacb,>    
-- =============================================    
create PROCEDURE [dbo].[DE_glbacb]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
   
Truncate table dbo.DE_glbacb_rec ;     

-- Insert statements for procedure here    
insert into DE_glbacb_rec
SELECT *   
  from [LIVEDEGL].[livedegldb].[informix].[glbacb_rec] ;    
      
END    
/*

*/
GO
