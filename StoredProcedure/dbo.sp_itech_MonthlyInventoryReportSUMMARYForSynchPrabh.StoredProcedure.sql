USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReportSUMMARYForSynchPrabh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Mukesh >                            
-- Create date: <07 Feb 2017>                            
                         
                          
-- =============================================                            
CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReportSUMMARYForSynchPrabh] @DBNAME varchar(50)                          
                            
AS                            
BEGIN                            
                     
                             
 SET NOCOUNT ON;                            
declare @sqltxt Varchar(8000)             
declare @execSQLtxt Varchar(8000)            
declare @DB varchar(100)                            
declare @12FD varchar(10)                            
declare @3FD varchar(10)                            
declare @6FD varchar(10)                            
declare @TD varchar(10)                            
declare @NOOfCust varchar(15)        
    
CREATE TABLE #tmp_itech_MonthlyInventoryReportSUMMARYS    
(    
 [UpdDtTm] [varchar](25) NULL,    
 [Database] [varchar](10) NULL,    
 [Form] [varchar](65) NULL,    
 [Grade] [varchar](65) NULL,    
 [Size] [varchar](65) NULL,    
 [SizeDesc] [varchar](65) NULL,    
 [Finish] [varchar](65) NULL,    
 [Whs] [varchar](10) NULL,    
 [Months3InvoicedWeight] [decimal](20, 2) NULL,    
 [Months6InvoicedWeight] [decimal](20, 2) NULL,    
 [Months12InvoicedWeight] [decimal](20, 2) NULL,    
 [ReplCost] [decimal](20, 2) NULL,    
 [OpenPOWgt] [decimal](20, 2) NULL,    
 [OpenSOWgt] [decimal](20, 2) NULL,    
 [Avg3Month] [decimal](20, 2) NULL,    
 [OhdStock] [decimal](20, 2) NULL,    
 [OhdStockCost] [decimal](20, 2) NULL,    
 [CustID] [varchar](max) NULL,    
 [MktSeg] [varchar](max) NULL,    
 [Months3Sales] [decimal](20, 2) NULL,    
 [COGS] [decimal](20, 2) NULL,    
 [ReservedWeight] [decimal](20, 2) NULL,    
 [TotalPOCost] [decimal](20, 2) NULL      
 );                      
                       
                            
set @DB=  @DBNAME                            
                            
set @3FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 3, 0) , 120)   --First day of previous 3 month                            
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)   --First day of previous 6 month                            
set @12FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                            
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)    ---Last day of previous month                            
                               
                            
DECLARE @DatabaseName VARCHAR(35);                            
DECLARE @Prefix VARCHAR(35);                            
DECLARE @Name VARCHAR(15);                      
print SYSDATETIME();                 
                    
                   
                     
                           
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                             
   Begin                     
                      
                              
       SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + '''  as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, prm_size_desc,PRm_fnsh , prd_whs,                             
                                   
       (Select  SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                         
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                            
                                   
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh   and sat_shpg_whs = prd_whs                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                            
                                   
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                        
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                            
                                 
       (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  )/2.20462 else ((ppb_repl_cst + ppb_frt_in_cst)  ) end)               
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh      order by  ppb_rct_expy_dt desc                     
      ) as MonthlyAvgReplCost,              
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec             
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm             
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm             
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,            
                   
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                             
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                             
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and orh_shpg_whs = prd_whs  and                             
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                             
       where  ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,                
       (Select sum(SAT_BLG_WGT * 2.20462)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                          
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                            
       sum(prd_ohd_wgt * 2.20462)   ,              
  isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  )),              
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )                           
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                            
        FOR XML PATH('''')),1,1,'''')) AS custIDs,              
    (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )                           
    FROM ' + @DB + '_sahsat_rec                            
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                             
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
         where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                           
        FOR XML PATH('''')),1,1,'''')) AS MktSeg ,                     (Select SUM(ISNULL(SAT_tot_val,0)) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs
                         
 and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales ,             
        (select Sum(ISNULL(csi_bas_cry_val,0)) from ' + @DB + '_cttcsi_rec              
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm              
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                         
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg   ,              
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt ))* 2.20462) as reserveWgt ,            
       (select sum(csi_bas_cry_val) from ' + @DB + '_cttcsi_rec          
       join ' + @DB + '_tctipd_rec on ipd_cmpy_id = csi_cmpy_id and csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm             
       and (select   count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_pfx = ipd_ref_pfx  and pod_po_no = ipd_ref_no and          
        ipd_ref_itm = pod_po_itm  and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
      and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost                
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                                   
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool               
        Where  prd_invt_sts = ''S''                             
       group by PRm_FRM, Prm_GRD,PRm_size,prm_size_desc, PRm_fnsh, prd_whs '            
       print( @sqltxt)        
       print SYSDATETIME();                         
    set @execSQLtxt = @sqltxt;                             
   EXEC (@execSQLtxt);             
                   
 SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size,prm_size_desc, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,            
                        
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost,             
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec             
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm             
       and  (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm             
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,            
(select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                             
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                             
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and orh_shpg_whs = prd_whs  and                             
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                             
       where  ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,    
        0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt ,             
(select sum(csi_bas_cry_val) from ' + @DB + '_cttcsi_rec              
       join ' + @DB + '_tctipd_rec on ipd_cmpy_id = csi_cmpy_id and  csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm             
       and (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_pfx = ipd_ref_pfx and pod_po_no = ipd_ref_no and          
        ipd_ref_itm = pod_po_itm  and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
      and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost                
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                            
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool                
       where  prd_invt_sts != ''S''               
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)               
       group by PRm_FRM, PRm_GRD,PRm_size, prm_size_desc,PRm_fnsh, prd_whs '             
       print( @sqltxt)      
       print SYSDATETIME();                           
    set @execSQLtxt = @sqltxt;                             
   EXEC (@execSQLtxt);         
      
--    For open SO Weight      
SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size,prm_size_desc, PRm_fnsh , orh_shpg_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,            
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 0 as OpenPOWgt,            
sum(ord_bal_wgt) * 2.20462  as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt , 0 as TotalPOCost                
from ' + @DB + '_inrprm_rec join ' + @DB + '_tctipd_rec on ipd_FRM=prm_frm and ipd_GRD = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh                            
join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm  and ord_ord_pfx = ipd_ref_pfx and ord_bal_wgt >0               
join ' + @DB + '_ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''       
and orh_shpg_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)      
       group by PRm_FRM, PRm_GRD,PRm_size, prm_size_desc,PRm_fnsh, orh_shpg_whs '             
       print( @sqltxt)         
       print SYSDATETIME();                        
    set @execSQLtxt = @sqltxt;                             
   EXEC (@execSQLtxt);                
                              
   End                            
   Else                            
   Begin                    
                         
                               
        SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size,prm_size_desc, PRm_fnsh , prd_whs ,             
                                   
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                         
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,                            
                               
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                        
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                            
                                   
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                        
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,                            
                                 
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)  )/2.20462 else ((ppb_repl_cst +           
        ppb_frt_in_cst)  ) end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  PRm_FRM            
         and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh       order by  ppb_rct_expy_dt desc                    
      ) as MonthlyAvgReplCost,                         
                                  
                                   
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec             
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm             
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm             
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,                            
                                   
       (select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec                             
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                   
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                             
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''  and orh_shpg_whs = prd_whs                
       where ipd_FRM=PRm_FRM                       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,             
        (Select sum(SAT_BLG_WGT)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                           
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,                            
       sum(prd_ohd_wgt)   ,                            
                                
 isnull (SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end)  ), SUM(prd_ohd_mat_val  )),               
               
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(sat_sld_cus_id )                           
    FROM ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                            
        FOR XML PATH('''')),1,1,'''')) AS custIDs,              
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(cuc_desc30 )                           
    FROM ' + @DB + '_sahsat_rec                            
    left join ' + @DB + '_arrcus_rec on sat_cmpy_id = cus_cmpy_id and  cus_cus_id = sat_sld_cus_id                             
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                           
where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                           
        FOR XML PATH('''')),1,1,'''')) AS MktSeg ,              
        (Select SUM(ISNULL(SAT_tot_val,0)) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                         
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3Sales,              
        (select Sum(ISNULL(csi_bas_cry_val,0)) from ' + @DB + '_cttcsi_rec              
  join ' + @DB + '_Sahsat_rec on sat_cmpy_id = csi_cmpy_id and sat_shpt_pfx = csi_ref_pfx and sat_shpt_no = csi_ref_no and sat_shpt_itm = csi_ref_itm              
and csi_cst_no = 1 where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh  and sat_shpg_whs = prd_whs                         
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as comg ,            
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as reserveWgt ,            
       (select sum(csi_bas_cry_val) from ' + @DB + '_cttcsi_rec          
       join ' + @DB + '_tctipd_rec on ipd_cmpy_id = csi_cmpy_id and  csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm             
       and (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_pfx = ipd_ref_pfx and pod_po_no = ipd_ref_no and           
       ipd_ref_itm = pod_po_itm   and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0            
       where ipd_FRM=PRm_FRM   and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in             
       (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost                
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                            
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool                
       where  prd_invt_sts = ''S''                     
       group by PRm_FRM, PRm_GRD,PRm_size,prm_size_desc, PRm_fnsh, prd_whs  '            
                   
print( @sqltxt)     
print SYSDATETIME();                            
    set @execSQLtxt = @sqltxt;              
   EXEC (@execSQLtxt);              
               
SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + ''' as [Database], PRm_FRM as Form,          
 PRm_GRD as Grade, PRm_size,prm_size_desc, PRm_fnsh , prd_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,                            
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost,             
(select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec             
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm             
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm             
       and pod_po_pfx = ipd_ref_pfx and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                           
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as OpenPOWgt,                            
(select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec                             
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                   
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                      
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''  and orh_shpg_whs = prd_whs                
       where ipd_FRM=PRm_FRM                       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh)  as OpenSOWgt,     
       0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt ,            
(select sum(csi_bas_cry_val) from ' + @DB + '_cttcsi_rec            
       join ' + @DB + '_tctipd_rec  on ipd_cmpy_id = csi_cmpy_id and csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm             
       and (select count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id = ipd_cmpy_id and pod_po_pfx = ipd_ref_pfx and pod_po_no = ipd_ref_no and           
       ipd_ref_itm = pod_po_itm   and pod_shp_to_whs = prd_whs and pod_trcomp_sts <> ''C'') > 0            
       where ipd_FRM=PRm_FRM   and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and ipd_cus_ven_id not in             
       (SElect ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = ipd_cmpy_id and ixv_actv = 1)) as TotalPOCost            
from ' + @DB + '_inrprm_rec left join ' + @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh                            
        left join ' + @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool                
       where  prd_invt_sts != ''S''               
       and   prd_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_invt_sts = ''S'' and  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)               
       group by PRm_FRM, PRm_GRD,PRm_size,prm_size_desc, PRm_fnsh, prd_whs              
       ;'               
       print( @sqltxt)     
       print SYSDATETIME();                            
    set @execSQLtxt = @sqltxt;                             
    EXEC (@execSQLtxt);           
          
--    For open SO Weight      
SET @sqltxt ='INSERT INTO #tmp_itech_MonthlyInventoryReportSUMMARYS          
          
SElect convert(varchar(25),getdate() ,121) as UpdDtTm , ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, PRm_size, prm_size_desc,PRm_fnsh , orh_shpg_whs, 0 as Months3InvoicedWeight, 0 as Months6InvoicedWeight,            
0 as Months12InvoicedWeight,0 as MonthlyAvgReplCost, 0 as OpenPOWgt,            
sum(ord_bal_wgt) as OpenSOWgt, 0 as Avg3Month,0,0, ''0'' AS custIDs,''0'' AS MktSeg , 0 as Months3Sales,0 as comg ,0 as reserveWgt , 0 as TotalPOCost                
from ' + @DB + '_inrprm_rec join ' + @DB + '_tctipd_rec on ipd_FRM=prm_frm and ipd_GRD = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh                            
join ' + @DB + '_ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm  and ord_ord_pfx = ipd_ref_pfx and ord_bal_wgt >0               
join ' + @DB + '_ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''       
and orh_shpg_whs not in (select prd_whs from  ' + @DB + '_intprd_rec where  prd_frm = prm_frm  and  prd_grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh)      
       group by PRm_FRM, PRm_GRD,PRm_size, prm_size_desc,PRm_fnsh, orh_shpg_whs '             
       print( @sqltxt)      
       print SYSDATETIME();                           
    set @execSQLtxt = @sqltxt;                             
   EXEC (@execSQLtxt);             
                            
   End                 
                
                                    
 insert into tbl_itech_MonthlyInventoryReportSUMMARYS select * from #tmp_itech_MonthlyInventoryReportSUMMARYS;     
 drop table #tmp_itech_MonthlyInventoryReportSUMMARYS;                
                
END                            
 --      truncate table    tbl_itech_MonthlyInventoryReportSUMMARYS_UK;                
-- exec [sp_itech_MonthlyInventoryReportSUMMARYForSynchPrabh] 'US'     
    
-- select * from tbl_itech_MonthlyInventoryReportSUMMARYS_UK;    
 --  select sum(OpenPOWgt), sum(OpenSOWgt), sum(totalPOCost) from tbl_itech_MonthlyInventoryReportSUMMARYS where [Database]='US';    
          
                          
/*              
-- 2017-01-31            
Select * from tbl_itech_MonthlyInventoryReportSUMMARYS_UK;            
Mail sub: inventory Summary Report            
            
-- 20170526      
Mail sub:Inventory report            
              
*/ 
GO
