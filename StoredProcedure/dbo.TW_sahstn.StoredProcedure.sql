USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_sahstn]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
    
    
    
    
    
    
-- =============================================    
-- Author:  <Author,Clayton Daigle>    
-- Create date: <Create Date,10/5/2012,>    
-- Description: <Description,Open Orders,>    
 -- Last updated by mukesh Date 07 Jul,2015     
-- =============================================    
CREATE PROCEDURE [dbo].[TW_sahstn]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
Delete from dbo.TW_sahstn_rec ;     
     
Insert into dbo.TW_sahstn_rec     
    
    -- Insert statements for procedure here    
    -- There is a problem in column stn_part   
SELECT *    
FROM [LIVETWSTX].[livetwstxdb].[informix].[sahstn_rec]   
-- Added by mukesh 20150707   
  where (stn_upd_ref_no != 8083 OR stn_upd_ref_itm != 1 )  
AND (stn_upd_ref_no != 8138 OR stn_upd_ref_itm != 1 )
END    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
GO
