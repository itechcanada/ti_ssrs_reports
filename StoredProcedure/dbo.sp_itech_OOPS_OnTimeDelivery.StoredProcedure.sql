USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OOPS_OnTimeDelivery]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <14 Jan 2019>        
-- Description: <Getting for oops Slids OTP>        
-- =============================================        
CREATE  PROCEDURE [dbo].[sp_itech_OOPS_OnTimeDelivery] @DBNAME varchar(50),@InBranch Varchar(3) = 'ALL'        
        
AS        
BEGIN        
    
    
SET NOCOUNT ON;        
        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)     
    
-- 20200428
if(@InBranch = 'ROS')    
begin    
set @InBranch = 'ROC'    
end  ; 
-- 

if(@InBranch = 'ALL')    
begin    
set @InBranch = ''    
end  ;  
       
create table #tmpWorkingDays (  
workingDate Date  
)  
  
insert into #tmpWorkingDays  
select [dbo].[WorkDay] (GETDATE(),-10) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-9) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-8) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-7) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-6) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-5) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-4) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-3) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-2) as workingDate  
union   
select [dbo].[WorkDay] (GETDATE(),-1) as workingDate;  
        
CREATE TABLE #tmp (     
  CompanyName   VARCHAR(10)    
  ,SprcNo          VARCHAR(35)       
        , ShpgWhs     VARCHAR(65)        
        ,ShippingDate Varchar(10)   
        ,Late    integer        
        ,OTPType    VARCHAR(35)         
                 );       
                     
CREATE TABLE #tmpFinal (     
  CompanyName   VARCHAR(10)   
  ,SprcNo          VARCHAR(35)        
        , ShpgWhs     VARCHAR(65)        
         ,ShippingDate Varchar(10)    
        ,Late    integer        
        ,OTPType    VARCHAR(35)         
                 );                         
                         
CREATE TABLE #tmpTransfer (     
   CompanyName   VARCHAR(10)   
            ,SprcNo          VARCHAR(35)         
   , ShpgWhs     VARCHAR(65)        
   ,ShippingDate Varchar(10)         
   ,Late    integer        
   ,OTPType    VARCHAR(35)         
      );        
                                      
DECLARE @company VARCHAR(35);        
DECLARE @prefix VARCHAR(15);         
DECLARE @DatabaseName VARCHAR(35);          
        
   set @FD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0),120)   -- First date of current Month                     
  set @TD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1),120) -- Last date of current Month   
    
  -- set @FD = '2018-12-01'   -- First date of current Month                     
  --set @TD = '2018-12-31' -- Last date of current Month           
               
 IF @DBNAME = 'ALL'        
 BEGIN        
       
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS        
    OPEN ScopeCursor;        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query NVARCHAR(4000);           
       SET @query = 'INSERT INTO #tmp (CompanyName,SprcNo , ShpgWhs,ShippingDate,Late,OTPType)        
            select  dpf_cmpy_id as ''CompanyName'',(dpf_sprc_no) as ''SprcNo'','  
            if (UPPER(@prefix)= 'US')         
            BEGIN        
             SET @query = @query + ' Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '  
             END         
             ELSE         
             BEGIN         
             SET @query = @query + 'ISNULL((case when orh_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po   
   = us.stn_end_usr_po and  cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),  
   patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) and cast(dpf_ord_rls_no as varchar(2))  =   
   substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',  
   [dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),  
   Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '  
   END                 
           
      SET @query = @query +  ' CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),   
      case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'',        
     ''Regular'' as OTPType       
       from '+ @prefix +'_pfhdpf_rec     
       join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no         
       JOIN '+ @prefix +'_ortorl_rec         
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no        
       Join '+ @prefix +'_ortord_rec        
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
       join '+ @prefix +'_arrcus_rec        
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)        
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '        
     
        EXECUTE sp_executesql @query;        
        Print(@query);     
        -- transfer       
          SET @query = 'INSERT INTO #tmpTransfer (CompanyName,SprcNo , ShpgWhs,ShippingDate,Late,OTPType)        
    SELECT tph_CMPY_ID as ''CompanyName'',(b.tud_sprc_no) as ''SprcNo'',b.tud_trpln_whs as ''ShpgWhs'','    
     SET @query = @query + 'CONVERT(VARCHAR(10), a.tph_sch_dtts, 120),case when  (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'',    
     ''Transfer'' as OTPType      
    FROM ['+ @prefix +'_trjtph_rec] a        
    left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no        
    left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  and b.tud_prnt_sitm = c.orl_ord_rls_no        
    left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
    left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
    WHERE a.tph_sch_dtts BETWEEN '''+ @FD +''' AND '''+ @TD +'''  and ord_ord_brh not in  (''SFS'')      
     AND(        
      (b.tud_prnt_no<>0         
      and TPH_NBR_STP =1         
      and b.tud_sprc_pfx<>''SH''         
      and b.tud_sprc_pfx <>''IT'')          
      OR         
      (b.tud_sprc_pfx in (''IT'',''SH''))      
      )'        
           
           
          Print(@query);     
       EXECUTE sp_executesql @query;        
                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
           
     print(@DB)          
             
     Set @prefix= @DBNAME         
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName,SprcNo , ShpgWhs,ShippingDate,Late,OTPType)        
            select  dpf_cmpy_id as ''CompanyName'',(dpf_sprc_no) as ''SprcNo'','  
            if (UPPER(@prefix)= 'US')         
            BEGIN        
             SET @sqltxt = @sqltxt + ' Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '  
             END         
             ELSE         
             BEGIN         
             SET @sqltxt = @sqltxt + 'ISNULL((case when orh_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po   
   = us.stn_end_usr_po and  cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),  
   patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) and cast(dpf_ord_rls_no as varchar(2))  =   
   substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',  
   [dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),  
   Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '  
   END                 
           
      SET @sqltxt = @sqltxt +  ' CONVERT(VARCHAR(10), dpf_shpg_dtts, 120),   
      case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'',        
     ''Regular'' as OTPType       
       from '+ @prefix +'_pfhdpf_rec     
       join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no         
       JOIN '+ @prefix +'_ortorl_rec         
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no        
       Join '+ @prefix +'_ortord_rec        
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
       join '+ @prefix +'_arrcus_rec        
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)        
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '        
             
            
              
    print(@sqltxt)        
   set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
  SET @sqltxt = 'INSERT INTO #tmpTransfer (CompanyName,SprcNo , ShpgWhs,ShippingDate,Late,OTPType)        
    SELECT tph_CMPY_ID as ''CompanyName'',(b.tud_sprc_no) as ''SprcNo'',b.tud_trpln_whs as ''ShpgWhs'','    
     SET @sqltxt = @sqltxt + 'CONVERT(VARCHAR(10), a.tph_sch_dtts, 120),case when  (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'',    
     ''Transfer'' as OTPType      
    FROM ['+ @prefix +'_trjtph_rec] a        
    left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no        
    left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  and b.tud_prnt_sitm = c.orl_ord_rls_no        
    left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
    left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
    WHERE a.tph_sch_dtts BETWEEN '''+ @FD +''' AND '''+ @TD +'''  and ord_ord_brh not in  (''SFS'')      
     AND(        
      (b.tud_prnt_no<>0         
      and TPH_NBR_STP =1         
      and b.tud_sprc_pfx<>''SH''         
      and b.tud_sprc_pfx <>''IT'')          
      OR         
      (b.tud_sprc_pfx in (''IT'',''SH''))      
      )'        
              
          
             
   print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
   END        
  
insert into #tmpFinal  
             
select * from #tmp --regarular 1456  
union  
select * from #tmpTransfer  tns    
where tns.SprcNo not in (select reg.SprcNo from #tmp reg )   
--and tns.ShpgWhs  = 'LAX';  
  
select ShpgWhs,ShippingDate,COUNT(Late) - Sum(Late) as OnTime,COUNT(Late) as TotalLate,  
 cast(((COUNT(Late) - Sum(Late))/Cast(COUNT(Late) as decimal (20,2)))*100 as decimal(20,1)) as OTDPct  
  
from #tmpFinal   
Where (ShpgWhs = @InBranch OR @InBranch = '')  and ShippingDate in (select workingDate from #tmpWorkingDays)  
group by ShpgWhs,ShippingDate ;  
  
  
  
               
--SELECT COUNT(distinct TranspNO) as 'TranspNO', CompanyName, Replace(ShpgWhs,'SFS','LAX') as  ShpgWhs ,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',    
--OTPType,count(distinct ShipmentReceiptNo) as 'ShipmentReceiptNo' ,'ZZ Late with NO Rsn Code' as RsnCode , Branch       
--FROM #tmpTransfer tr --where OTPType = 'Transfer'        
--where tr.ShipmentReceiptNo not in (select tm.ShipmentReceiptNo from #tmp tm)    
--and (ShpgWhs = @InBranch OR Branch = @InBranch OR @InBranch = '')     
--group by CompanyName, ShpgWhs,OTPType ,Branch       
--union         
--select count(distinct TranspNO) as 'TranspNO', CompanyName, Replace(ShpgWhs,'SFS','LAX') as ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',    
--OTPType,Sum(ShipmentReceiptNo) as 'ShipmentReceiptNo'     
--,Case (RsnCode) when '' then 'ZZ Late with NO Rsn Code'     
--when null then 'ZZ Late with NO Rsn Code' else (RsnCode) end as RsnCode , Branch       
--from #tmpFinal  where (ShpgWhs = @InBranch OR Branch = @InBranch OR @InBranch = '')      
--group by CompanyName, ShpgWhs,OTPType,RsnCode,Branch        
--order by ShpgWhs        
        
        
        
        
drop table #tmp;        
drop Table #tmpTransfer;      
drop table #tmpFinal;     
drop table #tmpWorkingDays;        
        
        
END        
        
-- exec sp_itech_OOPS_OnTimeDelivery 'US', 'LAX' 
-- exec sp_itech_OOPS_OnTimeDelivery 'US', 'ROS' 
/*
Date: 20200428
Mail sub:Home > STRATIXReports > Development > Warehouse Slides

*/
GO
