USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_intpic]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author, Sumit>
-- Create date: <Create Date,Jan 27 2021,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[DE_intpic]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.DE_intpic_rec', 'U') IS NOT NULL
		drop table dbo.DE_intpic_rec ;	

	
    -- Insert statements for procedure here
select  pic_cmpy_id,pic_itm_ctl_no,pic_brh,pic_extd_prd,pic_whs,pic_sk_ctl,	pic_tag_no
into  dbo.DE_intpic_rec
    from [LIVEDESTX].[livedestxdb].[informix].intpic_rec
  
END
-- select * from DE_intpic_rec 
GO
