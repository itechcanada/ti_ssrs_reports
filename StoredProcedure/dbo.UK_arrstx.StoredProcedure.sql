USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_arrstx]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,July 10, 2014,>
-- Description:	<Description,Sales Territory,>

-- =============================================
Create PROCEDURE [dbo].[UK_arrstx]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_arrstx_rec', 'U') IS NOT NULL
		drop table dbo.UK_arrstx_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.UK_arrstx_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[arrstx_rec] ;
  
END
GO
