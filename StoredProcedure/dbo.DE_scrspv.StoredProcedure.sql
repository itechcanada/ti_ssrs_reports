USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrspv]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Jan 17, 2018 
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_scrspv]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.DE_scrspv_rec', 'U') IS NOT NULL  
  drop table dbo.DE_scrspv_rec;  
      
          
SELECT *  
into  dbo.DE_scrspv_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[scrspv_rec];  
  
END  
-- select * from DE_scrspv_rec
GO
