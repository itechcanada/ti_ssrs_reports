USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_aprven]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[US_aprven]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.US_aprven_rec', 'U') IS NOT NULL          
  drop table dbo.US_aprven_rec;          
              
                  
SELECT *          
into  dbo.US_aprven_rec   
FROM [LIVEUSSTX].[liveusstxdb].[informix].[aprven_rec]    
    
    
END    
    
GO
