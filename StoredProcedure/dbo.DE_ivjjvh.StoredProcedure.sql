USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ivjjvh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_ivjjvh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
truncate table dbo.DE_ivjjvh_rec ;   
   
Insert into dbo.DE_ivjjvh_rec   
  
    -- Insert statements for procedure here  
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[ivjjvh_rec]  
  
  
END  
 
GO
