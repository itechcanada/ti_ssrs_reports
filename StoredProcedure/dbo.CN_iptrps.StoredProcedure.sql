USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_iptrps]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 6, 2015,>  
-- Description: <Description,gl account,>  
  
-- =============================================  
create PROCEDURE [dbo].[CN_iptrps]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_iptrps_rec', 'U') IS NOT NULL  
  drop table dbo.CN_iptrps_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_iptrps_rec  
  from [LIVECNSTX].[livecnstxdb].[informix].[iptrps_rec];   
    
END  
-- select * from CN_iptrps_rec
GO
