USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_arrshp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================      
-- Author:  <Author,Mukesh>      
-- Create date: <Create Date,03 Sep 2018,>      
-- Description: <>      
-- =============================================      
CREATE PROCEDURE [dbo].[US_arrshp]      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
--IF OBJECT_ID('dbo.US_arrshp_rec', 'U') IS NOT NULL            
--  drop table dbo.US_arrshp_rec;            

--SELECT *            
--into  dbo.US_arrshp_rec     
--FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrshp_rec] 

Truncate table dbo.US_arrshp_rec ;   
                    
Insert into dbo.US_arrshp_rec   
SELECT *  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrshp_rec]  
      
      
END      
/*
20201014	Sumit
Comment table drop and select statement, add delete and select statement to keep table indexes
*/
GO
