USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ProspectCustomerNoData]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <30 Nov 2016>    
-- =============================================    
-- select * from US_mxrusr_rec Where usr_lgn_id = 'ccutro'  
  
CREATE PROCEDURE [dbo].[sp_itech_ProspectCustomerNoData] @DBNAME varchar(50), @fromDate datetime , @toDate datetime    
    
AS    
BEGIN    


     
 SET NOCOUNT ON;    
declare @sqltxt varchar(max)    
declare @execSQLtxt varchar(max)    
declare @DB varchar(100)   
declare @FD varchar(10)      
declare @TD varchar(10) 
set @FD = CONVERT(VARCHAR(10), @FromDate,120)  
 set @TD = CONVERT(VARCHAR(10), @ToDate,120) 
 
set @DB=  @DBNAME     
 
    
CREATE TABLE #tmp (    CustID   VARCHAR(100)
,customercreatedDT Varchar(10)    
        -- , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,CusType Varchar(1)  
               
                   );   
         
  
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
DECLARE @Name VARCHAR(15);    
    
    
--if @Branch ='ALL'    
-- BEGIN    
-- set @Branch = ''    
-- END    
    
    
IF @DBNAME = 'ALL'    
 BEGIN    
   DECLARE ScopeCursor CURSOR FOR    
    select DatabaseName, company,prefix from tbl_itech_DatabaseName     
     OPEN ScopeCursor;     
  
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
      SET @DB= @Prefix    
     -- print(@DB)    
         
        DECLARE @query NVARCHAR(max);    
             
        SET @query = 'INSERT INTO #tmp (Databases,CustID,customercreatedDT,CusType)  
     select  ''' + @DB + ''',cus_cus_id as CustID, cus_upd_dtts, cus_cus_acct_typ
            from ' + @DB + '_arrcus_rec     
            where cus_cus_acct_typ = ''P''  and cus_actv = 1 and cus_upd_dtts >= '''+ @FD +''' and cus_upd_dtts <= '''+ @TD +''' 
 union             
select  ''' + @DB + ''',cus_cus_id as CustID, cus_upd_dtts, cus_cus_acct_typ
            from ' + @DB + '_arrcus_rec     
            where cus_cus_acct_typ != ''P''  and cus_actv = 1  and cus_cus_id not in (select distinct STN_SLD_CUS_ID FROM ' + @DB + '_sahstn_rec    
           Where STN_SLD_CUS_ID = CUS_CUS_ID) and cus_upd_dtts >= '''+ @FD +''' and cus_upd_dtts <= '''+ @TD +''' '  
print(@query)      
        EXECUTE sp_executesql @query;   
         
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
          
                            
      
         
     SET @sqltxt = 'INSERT INTO #tmp (Databases,CustID,customercreatedDT,CusType)  
     select  ''' + @DB + ''',cus_cus_id as CustID, cus_upd_dtts, cus_cus_acct_typ
            from ' + @DB + '_arrcus_rec     
            where cus_cus_acct_typ = ''P''  and cus_actv = 1 and cus_upd_dtts >= '''+ @FD +''' and cus_upd_dtts <= '''+ @TD +''' 
 union             
select  ''' + @DB + ''',cus_cus_id as CustID, cus_upd_dtts, cus_cus_acct_typ
            from ' + @DB + '_arrcus_rec     
            where cus_cus_acct_typ != ''P''  and cus_actv = 1  and cus_cus_id not in (select distinct STN_SLD_CUS_ID FROM ' + @DB + '_sahstn_rec    
           Where STN_SLD_CUS_ID = CUS_CUS_ID) and  cus_upd_dtts >= '''+ @FD +''' and cus_upd_dtts <= '''+ @TD +'''   
'  
           
  print(@sqltxt)    
  set @execSQLtxt = @sqltxt;     
  EXEC (@execSQLtxt);    
    
   
     END    
     
     
 
  Select Databases,CustID,customercreatedDT,(case when CusType = 'P' then 'P' else 'C' end) as CusType
   from #tmp ;
 
   drop table #tmp ;  
END    
    
-- exec [sp_itech_ProspectCustomerNoData] 'US', '2016-01-01', '2016-12-01'   
GO
