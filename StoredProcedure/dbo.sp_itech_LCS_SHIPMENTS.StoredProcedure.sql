USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_LCS_SHIPMENTS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <10 Jan 2014>  
-- Description: <LCS_SHIPMENTS Report > 
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_LCS_SHIPMENTS]  @DBNAME varchar(50), @FromDate datetime, @ToDate datetime, @version char = '0'  
AS  
BEGIN  

 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
declare @DB1 varchar(100);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);  
DECLARE @company VARCHAR(15);     
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15);    
declare @FD varchar(10)  
declare @TD varchar(10)  
  
SET @FD = CONVERT( Varchar(10), @FromDate, 120);  
SET @TD = CONVERT( Varchar(10), @ToDate, 120);  
  
CREATE TABLE #temp1 ( Dbname   VARCHAR(10),  
       Upd_ref   VARCHAR(25),  
       Upd_ref_itm  NUMERIC,  
       Inv_dt   VARCHAR(20),  
       Grd    VARCHAR(10),  
       Size    VARCHAR(20),  
       Sld_cus_id  VARCHAR(10),  
       Nm1    VARCHAR(40),   
       Tot_val   DECIMAL(20,2),  
       Blg_wgt   DECIMAL(20,2),  
       Blg_msr   DECIMAL(20,2),  
       Product   VARCHAR(10),  
       Ord_pfx   VARCHAR(2),  
       Ord_no   NUMERIC,  
       Ord_itm   NUMERIC  
     );  
CREATE TABLE #TEMP2 ( Ref_pfx   VARCHAR(2),  
       Ref_no   NUMERIC,  
       Ref_itm   NUMERIC  
);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
   IF @version = '0'  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,company,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
  END  
  ELSE  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,company,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
  END  
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
     
   SET @DB1 = @prefix
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
      
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR   
    BEGIN  
    SET @query = 'INSERT INTO #temp1 (Dbname, Upd_ref, Upd_ref_itm, Inv_dt, Grd, Size, Sld_cus_id, Nm1, Tot_val, Blg_wgt, Blg_msr, Product, Ord_pfx, Ord_no, Ord_itm)  
      SELECT '''+ @Prefix +''' , sat_upd_ref, sat_upd_ref_itm, sat_inv_dt, sat_grd, sat_size, sat_sld_cus_id, sat_nm1, 
      -- Changes by mrinal 22-05-15
      sat_tot_val * '+ @CurrenyRate +', 
      sat_blg_wgt * 2.20462, sat_blg_msr,  
       ''product''= CASE WHEN sat_frm = ''TIPL'' THEN ''TI PLATE''  
       WHEN sat_frm = ''TIRD'' THEN ''TI BAR''  
       WHEN sat_frm = ''NIPL'' THEN ''NI PLATE''  
       WHEN sat_frm = ''NIRD'' THEN ''NI BAR''  
       WHEN sat_frm = ''TISH'' THEN ''TI SHEET'' ELSE sat_frm END, sat_ord_pfx, sat_ord_no, sat_ord_itm  
       FROM ' + @DB1 + '_sahsat_rec  
       WHERE sat_ord_pfx= ''SO''  
       AND sat_inv_dt BETWEEN ''' + @FD + ''' AND ''' + @TD + ''';  
         
       INSERT INTO #temp2 (Ref_pfx, Ref_no, Ref_itm)     
       SELECT ssp_ref_pfx ,ssp_ref_no ,ssp_ref_itm  
       FROM ' + @DB1 + '_pntssp_rec a  
       INNER JOIN ' + @DB1 + '_mcrmss_rec b ON a.ssp_mss_ctl_no=b.mss_mss_ctl_no  
       WHERE ssp_ref_pfx = ''SO''  
       AND b.mss_sdo = ' + '''PWA'''  
    END  
    ELSE  
    BEGIN  
     SET @query = 'INSERT INTO #temp1 (Dbname, Upd_ref, Upd_ref_itm, Inv_dt, Grd, Size, Sld_cus_id, Nm1, Tot_val, Blg_wgt, Blg_msr, Product, Ord_pfx, Ord_no, Ord_itm)  
      SELECT '''+ @Prefix +''' , sat_upd_ref, sat_upd_ref_itm, sat_inv_dt, sat_grd, sat_size, sat_sld_cus_id, sat_nm1, sat_tot_val * '+ @CurrenyRate +', sat_blg_wgt, sat_blg_msr,  
       ''product''= CASE WHEN sat_frm = ''TIPL'' THEN ''TI PLATE''  
       WHEN sat_frm = ''TIRD'' THEN ''TI BAR''  
       WHEN sat_frm = ''NIPL'' THEN ''NI PLATE''  
       WHEN sat_frm = ''NIRD'' THEN ''NI BAR''  
       WHEN sat_frm = ''TISH'' THEN ''TI SHEET'' ELSE sat_frm END, sat_ord_pfx, sat_ord_no, sat_ord_itm  
       FROM ' + @DB1 + '_sahsat_rec  
       WHERE sat_ord_pfx= ''SO''  
       AND sat_inv_dt BETWEEN ''' + @FD + ''' AND ''' + @TD + ''';  
         
       INSERT INTO #temp2 (Ref_pfx, Ref_no, Ref_itm)     
       SELECT ssp_ref_pfx ,ssp_ref_no ,ssp_ref_itm  
       FROM ' + @DB1 + '_pntssp_rec a  
       INNER JOIN ' + @DB1 + '_mcrmss_rec b ON a.ssp_mss_ctl_no=b.mss_mss_ctl_no  
       WHERE ssp_ref_pfx = ''SO''  
       AND b.mss_sdo = ' + '''PWA'''  
    END  
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @company, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB1 ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
      
    IF @version = '0'  
    BEGIN  
   SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')  
   END  
   ELSE  
   BEGIN  
   SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DBNAME +'')  
   END  
  
     
     
   SET @DB1 = @DBNAME  
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR   
   BEGIN  
   SET @sqltxt ='INSERT INTO #temp1 (Dbname, Upd_ref, Upd_ref_itm, Inv_dt, Grd, Size, Sld_cus_id, Nm1, Tot_val, Blg_wgt, Blg_msr, Product, Ord_pfx, Ord_no, Ord_itm)  
      SELECT '''+ @DBNAME +''' , sat_upd_ref, sat_upd_ref_itm, sat_inv_dt, sat_grd, sat_size, sat_sld_cus_id, sat_nm1, 
      sat_tot_val * '+ @CurrenyRate +', 
      sat_blg_wgt * 2.20462, sat_blg_msr,  
       ''product''= CASE WHEN sat_frm = ''TIPL'' THEN ''TI PLATE''  
       WHEN sat_frm = ''TIRD'' THEN ''TI BAR''  
       WHEN sat_frm = ''NIPL'' THEN ''NI PLATE''  
       WHEN sat_frm = ''NIRD'' THEN ''NI BAR''  
       WHEN sat_frm = ''TISH'' THEN ''TI SHEET'' ELSE sat_frm END, sat_ord_pfx, sat_ord_no, sat_ord_itm  
       FROM ' + @DB1 + '_sahsat_rec  
       WHERE sat_ord_pfx= ''SO''  
       AND sat_inv_dt BETWEEN ''' + @FD + ''' AND ''' + @TD + ''';  
         
       INSERT INTO #temp2 (Ref_pfx, Ref_no, Ref_itm)     
       SELECT ssp_ref_pfx ,ssp_ref_no ,ssp_ref_itm  
       FROM ' + @DB1 + '_pntssp_rec a  
       INNER JOIN ' + @DB1 + '_mcrmss_rec b ON a.ssp_mss_ctl_no=b.mss_mss_ctl_no  
       WHERE ssp_ref_pfx = ''SO''  
       AND b.mss_sdo = ' + '''PWA'''  
  END  
  ELSE  
  BEGIN  
  SET @sqltxt ='INSERT INTO #temp1 (Dbname, Upd_ref, Upd_ref_itm, Inv_dt, Grd, Size, Sld_cus_id, Nm1, Tot_val, Blg_wgt, Blg_msr, Product, Ord_pfx, Ord_no, Ord_itm)  
      SELECT  '''+ @DBNAME +''' , sat_upd_ref, sat_upd_ref_itm, sat_inv_dt, sat_grd, sat_size, sat_sld_cus_id, sat_nm1, sat_tot_val * '+ @CurrenyRate +', sat_blg_wgt, sat_blg_msr,  
       ''product''= CASE WHEN sat_frm = ''TIPL'' THEN ''TI PLATE''  
       WHEN sat_frm = ''TIRD'' THEN ''TI BAR''  
       WHEN sat_frm = ''NIPL'' THEN ''NI PLATE''  
       WHEN sat_frm = ''NIRD'' THEN ''NI BAR''  
       WHEN sat_frm = ''TISH'' THEN ''TI SHEET'' ELSE sat_frm END, sat_ord_pfx, sat_ord_no, sat_ord_itm  
       FROM ' + @DB1 + '_sahsat_rec  
       WHERE sat_ord_pfx= ''SO''  
       AND sat_inv_dt BETWEEN ''' + @FD + ''' AND ''' + @TD + ''';  
         
       INSERT INTO #temp2 (Ref_pfx, Ref_no, Ref_itm)     
       SELECT ssp_ref_pfx ,ssp_ref_no ,ssp_ref_itm  
       FROM ' + @DB1 + '_pntssp_rec a  
       INNER JOIN ' + @DB1 + '_mcrmss_rec b ON a.ssp_mss_ctl_no=b.mss_mss_ctl_no  
       WHERE ssp_ref_pfx = ''SO''  
       AND b.mss_sdo = ' + '''PWA'''  
  END  
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT DISTINCT * FROM #temp1 a  
 INNER JOIN #TEMP2 b ON a.ord_pfx=b.ref_pfx AND a.ord_no = b.ref_no AND a.ord_itm = b.ref_itm;  
 DROP TABLE #temp1;  
 DROP TABLE  #TEMP2;  
END  
  
--EXEC [sp_itech_LCS_SHIPMENTS] 'ALL', '2013-01-01', '2013-11-30'  
--EXEC [sp_itech_LCS_SHIPMENTS] 'NO', '2014-06-01', '2014-06-30','1'  
--US  
--CA  
--TW  
--NO  
--UK  
--CN  
GO
