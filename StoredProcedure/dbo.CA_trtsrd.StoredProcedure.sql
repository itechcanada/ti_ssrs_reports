USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_trtsrd]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CA_trtsrd] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_trtsrd_rec', 'U') IS NOT NULL
		drop table dbo.CA_trtsrd_rec;
    
        
SELECT *
into  dbo.CA_trtsrd_rec
FROM [LIVECASTX].[livecastxdb].[informix].[trtsrd_rec];

END
-- select * from CA_trtsrd_rec
-- exec CA_trtsrd 
GO
