USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Get_Lost_Account_NP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <05 Jan 2016>
-- Description:	<Getting Lost account>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_Get_Lost_Account_NP]  @DBNAME varchar(50), @Brnch varchar(15), @version char = '0'  

AS
BEGIN
SET NOCOUNT ON;


declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @NFD varchar(10)
declare @NTD varchar(10)
declare @LFD varchar(10)
declare @LTD varchar(10)
declare @FD varchar(10)
declare @TD varchar(10)
declare @AFD varchar(10)
declare @ATD varchar(10)

declare @Month datetime
Set @Month = GETDATE()

set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Lost Account
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-18,0)), 120)  -- Lost Account

set @AFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-12,0)) , 120)  -- for 12 month 

set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)


CREATE TABLE #tmp (      Branch varchar(50)
                        ,CustID  VARCHAR(50)
						,CustName varchar(100)
						,ContactName varchar(100)
						,CustEmail Varchar(50)
						,Market Varchar(50)
						,LastSaleDate Varchar(50)
						,Prior12MthSale Varchar(50)
						,NPAmt Varchar(50)
						,Prior12MthGp Varchar(50)
						,ISalesPerson Varchar(4)
						,OSalesPerson Varchar(4)
   					);
   					
   					CREATE TABLE #tmp1 (      
                        CustID  VARCHAR(50)
						,Prior12MthSale Varchar(50)
						,NPAmt Varchar(50)
						,Prior12MthGp Varchar(50)
   					);
   					
   					   					   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CurrenyRate varchar(15);
DECLARE @Category varchar(50);

set @DB= @DBNAME

IF @Brnch = 'ALL'
BEGIN
SET @Brnch = ''
END
  	   

  	   
 IF @DBNAME = 'ALL'
	BEGIN
	IF @version = '0'    
   BEGIN    
   DECLARE ScopeCursor CURSOR FOR  
    select DatabaseName, company,prefix from tbl_itech_DatabaseName   
     OPEN ScopeCursor;   
   END    
   ELSE    
   BEGIN  
			DECLARE ScopeCursor CURSOR FOR
			select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
End
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000);  	
  				 set @DB= @prefix    
							
						   
  	  			         
  	  			        SET @query = '   INSERT INTO #tmp(Branch,CustID,CustName,CustEmail ,ContactName,Market,LastSaleDate,Prior12MthSale,NPAmt,Prior12MthGp,ISalesPerson,OSalesPerson)
SELECT   CUS_ADMIN_BRH, CUS_CUS_ID, CUS_CUS_LONG_NM, IsNull((Select TOP 1 cvt_email from '+@DB+'_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = CUS_CUS_ID and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail    
  	  										 										
,(select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else
case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else
case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else
rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end
from '+@DB+'_scrcvt_rec where  cvt_cus_ven_id = CUS_CUS_ID and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no)	  										
  	  										
,cuc_desc30 as category 
,max(coc_lst_sls_dt) as LastSaleDate

, 0, 0 ,0,shp_is_slp, shp_os_slp

FROM ' + @DB + '_arbcoc_rec   
INNER JOIN '+@DB+'_arrcus_rec ON coc_cus_id = CUS_CUS_ID  
join '+@DB+'_arrshp_rec on shp_cmpy_id = cus_cmpy_id and shp_cus_id = cus_cus_id and shp_shp_to = 0
Left Join '+@DB+'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
 where coc_lst_sls_dt <= '''+@LTD+'''  and   (CUS_ADMIN_BRH = '''+@Brnch +''' OR  '''+@Brnch +''' = '' '') 
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,shp_is_slp, shp_os_slp   
-- Having MAX(coc_lst_sls_dt) <=  '''+@LTD+''' 
  	  										
'
-- and MAX(stn_inv_dt) >= '''+@LFD+''' AND								
	print @query;										
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			    
  	  			SET @query = ' INSERT INTO #tmp1(CustID,Prior12MthSale,NPAmt,Prior12MthGp)
  	  			    select stn_sld_cus_id,SUM(stn_tot_val * 1) as TotalValue, 
  	  			    CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',
CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage
from ' + @DB + '_sahstn_rec
where stn_inv_Dt >= '''+@NTD+''' and stn_inv_dt <= '''+@AFD+'''
group by stn_sld_cus_id '
  	  			    
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			    
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
		  
  END
  ELSE
     BEGIN 
								
							SET @sqltxt = '   INSERT INTO #tmp(Branch,CustID,CustName,CustEmail,ContactName,Market,LastSaleDate,Prior12MthSale,NPAmt,Prior12MthGp,ISalesPerson,OSalesPerson)
SELECT   CUS_ADMIN_BRH, CUS_CUS_ID, CUS_CUS_LONG_NM, IsNull((Select TOP 1 cvt_email from '+@DBNAME+'_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = CUS_CUS_ID and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail    
  	  										 										
,(select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else
case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else
case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else
rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end
from '+@DBNAME+'_scrcvt_rec where  cvt_cus_ven_id = CUS_CUS_ID and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no)	  										
  	  										
,cuc_desc30 as category 
,max(coc_lst_sls_dt) as LastSaleDate

, 0, 0 ,0,shp_is_slp, shp_os_slp

FROM ' + @DBNAME + '_arbcoc_rec  
INNER JOIN '+@DBNAME+'_arrcus_rec ON coc_cus_id = CUS_CUS_ID  
join '+@DBNAME+'_arrshp_rec on shp_cmpy_id = cus_cmpy_id and shp_cus_id = cus_cus_id and shp_shp_to = 0
Left Join '+@DBNAME+'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
 where coc_lst_sls_dt <= '''+@LTD+'''  AND (CUS_ADMIN_BRH = '''+@Brnch +''' OR  '''+@Brnch +''' = '' '')
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,shp_is_slp, shp_os_slp   
-- Having MAX(coc_lst_sls_dt) <=  '''+@LTD+''' 
  	  										
'
							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt); 
							
	SET @sqltxt = ' INSERT INTO #tmp1(CustID,Prior12MthSale,NPAmt,Prior12MthGp)
  	  			    select stn_sld_cus_id,SUM(stn_tot_val * 1) as TotalValue, 
  	  			     CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',
CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage
from ' + @DBNAME + '_sahstn_rec
where stn_inv_Dt >= '''+@AFD+''' and stn_inv_dt <= '''+@NTD+'''  
group by stn_sld_cus_id '
  	  			    
  	  			    print(@sqltxt)
  	  			    EXEC (@sqltxt); 
							
						
					--delete from #tmp
							
     END
  SELECT 
  --TOP 40 
  a.Branch,a.CustID,a.CustName,a.ISalesPerson,a.OSalesPerson, a.ContactName,a.CustEmail,a.Market,a.LastSaleDate,ISNULL(b.Prior12MthSale,0) as Prior12MthSale,
ISNULL(b.NPAmt,0) as NPAmt,  ISNULL(b.Prior12MthGp,0) as Prior12MthGp FROM #tmp a
  LEFT JOIN #tmp1 b ON a.CustID = b.CustID 
        	  
  Drop table #tmp  ;
  Drop table #tmp1  ;
END

-- exec [sp_itech_Get_Lost_Account_NP] 'ALL','ALL'
-- exec [sp_itech_Get_Lost_Account] 'ALL','ALL'
/*
Date 20170628
Mail sub:Lost Account by Branch w/email

*/
GO
