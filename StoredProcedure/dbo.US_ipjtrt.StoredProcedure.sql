USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ipjtrt]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  cdaigle   
-- Create date: decemeber2, 2013  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[US_ipjtrt]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.US_ipjtrt_rec', 'U') IS NOT NULL  
  drop table dbo.US_ipjtrt_rec;  
      
          
SELECT   [trt_cmpy_id]
      ,[trt_ref_pfx]
      ,[trt_ref_no]
      ,[trt_ref_itm]
      ,[trt_ref_sbitm]
      ,[trt_tprod_wgt]
      ,[trt_tcons_wgt]
      ,[trt_tdrp_wgt]
      ,[trt_tmst_wgt]
      ,[trt_trjct_wgt]
      ,[trt_tscr_wgt]
      ,[trt_tetrm_wgt]
      ,[trt_tecut_wgt]
      ,[trt_tkrfls_wgt]
      ,[trt_tuscr_wgt]  
into  dbo.US_ipjtrt_rec  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ipjtrt_rec];  
  
END  
  
-- exce US_ipjtrt  
-- select * from US_ipjtrt_rec 

/*
Date:20190404
Executed as user: NT AUTHORITY\SYSTEM. Cannot get the data of the row from the OLE DB provider "Ifxoledbc" for linked server "LIVEUSSTX". Could not convert the data value due to reasons other than sign mismatch or overflow. [SQLSTATE 42000] (Error 7346).  The step failed.
--,[trt_actvy_dt]
      --,[trt_tprod_pcs]
      --,[trt_tprod_msr_a]
      --,[trt_tprod_msr_l]
      --,[trt_tprod_msr_w]
      --,[trt_tprod_msr_mix]
      --,[trt_tprod_qty_u]
      --,[trt_tprod_qty_a]
      --,[trt_tprod_qty_l]
      --,[trt_tprod_qty_w]
      --,[trt_tprod_qty_mix]
      --,[trt_tprod_yld]
      --,[trt_tprod_val]
      --,[trt_tblg_pcs]
      --,[trt_tblg_msr_a]
      --,[trt_tblg_msr_l]
      --,[trt_tblg_msr_w]
      --,[trt_tblg_wgt]
      --,[trt_tblg_msr_mix]
      --,[trt_tblg_qty_u]
      --,[trt_tblg_qty_a]
      --,[trt_tblg_qty_l]
      --,[trt_tblg_qty_w]
      --,[trt_tblg_qty_mix]
      --,[trt_tblg_yld]
      --,[trt_tcons_pcs]
      --,[trt_tcons_msr_a]
      --,[trt_tcons_msr_l]
      --,[trt_tcons_msr_w]
      --,[trt_tcons_msr_mix]
      --,[trt_tcons_qty_u]
      --,[trt_tcons_qty_a]
      --,[trt_tcons_qty_l]
      --,[trt_tcons_qty_w]
      --,[trt_tcons_qty_mix]
      --,[trt_tcons_yld]
      --,[trt_tcons_val]
      --,[trt_tconsb_pcs]
      --,[trt_tconsb_msr_a]
      --,[trt_tconsb_msr_l]
      --,[trt_tconsb_msr_w]
      --,[trt_tconsb_wgt]
      --,[trt_tconsb_msr_mix]
      --,[trt_tconsb_qty_u]
      --,[trt_tconsb_qty_a]
      --,[trt_tconsb_qty_l]
      --,[trt_tconsb_qty_w]
      --,[trt_tconsb_qty_mix]
      --,[trt_tconsb_yld]
      --,[trt_tconsb_val]
      --,[trt_tdrp_pcs]
      --,[trt_tdrp_msr_a]
      --,[trt_tdrp_msr_l]
      --,[trt_tdrp_msr_w]
      --,[trt_tdrp_msr_mix]
      --,[trt_tdrp_qty_u]
      --,[trt_tdrp_qty_a]
      --,[trt_tdrp_qty_l]
      --,[trt_tdrp_qty_w]
      --,[trt_tdrp_qty_mix]
      --,[trt_tdrp_yld]
      --,[trt_tdrp_val]
      --,[trt_tmst_pcs]
      --,[trt_tmst_msr_a]
      --,[trt_tmst_msr_l]
      --,[trt_tmst_msr_w]
      ,[trt_tmst_msr_mix]
      ,[trt_tmst_qty_u]
      ,[trt_tmst_qty_a]
      ,[trt_tmst_qty_l]
      ,[trt_tmst_qty_w]
      ,[trt_tmst_qty_mix]
      ,[trt_tmst_yld]
      ,[trt_tmst_val]
      ,[trt_trjct_pcs]
      ,[trt_trjct_msr_a]
      ,[trt_trjct_msr_l]
      ,[trt_trjct_msr_w]
      ,[trt_trjct_msr_mix]
      ,[trt_trjct_qty_u]
      ,[trt_trjct_qty_a]
      ,[trt_trjct_qty_l]
      ,[trt_trjct_qty_w]
      ,[trt_trjct_qty_mix]
      ,[trt_trjct_yld]
      ,[trt_trjct_val]
      ,[trt_tetrm_pcs]
      ,[trt_tetrm_msr_a]
      ,[trt_tetrm_msr_l]
      ,[trt_tetrm_msr_w]
      ,[trt_tetrm_msr_mix]
      ,[trt_tetrm_qty_u]
      ,[trt_tetrm_qty_a]
      ,[trt_tetrm_qty_l]
      ,[trt_tetrm_qty_w]
      ,[trt_tetrm_qty_mix]
      ,[trt_tetrm_yld]
      ,[trt_tetrm_val]
      ,[trt_tecut_pcs]
      ,[trt_tecut_msr_a]
      ,[trt_tecut_msr_l]
      ,[trt_tecut_msr_w]
      ,[trt_tecut_msr_mix]
      ,[trt_tecut_qty_u]
      ,[trt_tecut_qty_a]
      ,[trt_tecut_qty_l]
      ,[trt_tecut_qty_w]
      ,[trt_tecut_qty_mix]
      ,[trt_tecut_yld]
      ,[trt_tecut_val]
      ,[trt_tkrfls_pcs]
      ,[trt_tkrfls_msr_a]
      ,[trt_tkrfls_msr_l]
      ,[trt_tkrfls_msr_w]
      ,[trt_tkrfls_msr_mix]
      ,[trt_tkrfls_qty_u]
      ,[trt_tkrfls_qty_a]
      ,[trt_tkrfls_qty_l]
      ,[trt_tkrfls_qty_w]
      ,[trt_tkrfls_qty_mix]
      ,[trt_tkrfls_yld]
      ,[trt_tkrfls_val]
      ,[trt_tscr_pcs]
      ,[trt_tscr_msr_a]
      ,[trt_tscr_msr_l]
      ,[trt_tscr_msr_w]
      ,[trt_tscr_msr_mix]
      ,[trt_tscr_qty_u]
      ,[trt_tscr_qty_a]
      ,[trt_tscr_qty_l]
      ,[trt_tscr_qty_w]
      ,[trt_tscr_qty_mix]
      ,[trt_tscr_yld]
      ,[trt_tscr_val]
      ,[trt_tuscr_pcs]
      ,[trt_tuscr_msr_a]
      ,[trt_tuscr_msr_l]
      ,[trt_tuscr_msr_w]
      ,[trt_tuscr_msr_mix]
      ,[trt_tuscr_qty_u]
      ,[trt_tuscr_qty_a]
      ,[trt_tuscr_qty_l]
      ,[trt_tuscr_qty_w]
      ,[trt_tuscr_qty_mix]
      ,[trt_tuscr_yld]
      ,[trt_tuscr_val]
      ,[trt_gn_ls_twf]
      ,[trt_gn_ls_val]
      ,[trt_only_stk_prod]
*/ 
  
GO
