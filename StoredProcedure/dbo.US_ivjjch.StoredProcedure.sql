USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ivjjch]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,December 21, 2016,>  
-- Description: <Description,>  
  
-- =============================================  
Create PROCEDURE [dbo].[US_ivjjch]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_ivjjch_rec', 'U') IS NOT NULL  
  drop table dbo.US_ivjjch_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_ivjjch_rec  
  from [LIVEUSSTX].[liveusstxdb].[informix].[ivjjch_rec] ;   
    
END  
-- select * from US_ivjjch_rec 
GO
