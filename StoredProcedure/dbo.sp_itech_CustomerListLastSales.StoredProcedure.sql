USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerListLastSales]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <03 March 2016>  
-- Description: <Get List of Customer of LAX customer that have no any sales in last 3 month>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_CustomerListLastSales]   @DBNAME varchar(3),@Branch varchar(3)
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 
 SET NOCOUNT ON;
 
   
 DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(35);              
DECLARE @Name VARCHAR(15);  
declare @DB varchar(3);
declare @sqltxt varchar(6000);
declare @execSQLtxt varchar(7000) ;
 
 set @DB=  @DBNAME  ;
 
 if (@Branch = 'ALL')
 set @Branch = '';
 
 CREATE TABLE #tmp (   [Database]   VARCHAR(3)              
        , Branch     Varchar(3)              
        , CustID     Varchar(10)              
        , CustName     Varchar(20)              
        , LastSalesDate    Varchar(10)    
                 ); 
                                
IF @DBNAME = 'ALL'              
 BEGIN              
 
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
			OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(max);                 
      SET @DB= @Prefix       
      SET @query ='INSERT INTO #tmp ([Database],Branch,CustID,CustName,LastSalesDate)
		   Select cus_cmpy_id,cus_admin_brh, cus_cus_id, cus_cus_nm,convert(Varchar(10),coc_lst_sls_dt,120) as coc_lst_sls_dt from    
		  ' + @DB + '_arrcus_rec   
		 left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id  
		Where  (cus_admin_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''') order by coc_lst_sls_dt desc '  
		print @query;              
        EXECUTE sp_executesql @query;              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE 
  Begin
   SET @sqltxt ='INSERT INTO #tmp ([Database],Branch,CustID,CustName,LastSalesDate)
   Select cus_cmpy_id,cus_admin_brh, cus_cus_id, cus_cus_nm,convert(Varchar(10),coc_lst_sls_dt,120) as coc_lst_sls_dt from    
  ' + @DB + '_arrcus_rec   
 left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id  
Where  (cus_admin_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''') order by coc_lst_sls_dt desc'
 print( @sqltxt)               
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
   END   
   
   Select * from #tmp;
   Drop table #tmp;         
END  
  
-- exec sp_itech_CustomerListLastSales 'US', 'ALL'  
/*  
--2016-05-13  
Sub:Fwd: we are going to need to make a version of this which is branch selectable so i can put it every branch folder
  
*/
GO
