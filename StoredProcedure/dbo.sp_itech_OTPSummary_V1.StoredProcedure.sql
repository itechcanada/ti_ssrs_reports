USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OTPSummary_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <14 Mar 2018>        
-- Description: <Getting OTP>        
-- =============================================        
CREATE  PROCEDURE [dbo].[sp_itech_OTPSummary_V1] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID varchar(Max),@CustmomerNoTxt varchar(Max),@DateRange int, @version char = '0', @InBranch Varchar(3) = 'ALL'        
        
AS        
BEGIN        
    
    
SET NOCOUNT ON;        
        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)     
    
if(@InBranch = 'ALL')    
begin    
set @InBranch = ''    
end    
       
        
CREATE TABLE #tmp (   CompanyName   VARCHAR(10)        
        , ShpgWhs     VARCHAR(65)        
        , Pcs    DECIMAL(20, 2)        
        , Wgt    DECIMAL(20, 2)        
        ,Late    integer        
        ,TranspNO    integer          
        ,OTPType    VARCHAR(35)         
        ,OrderNo   varchar(15)        
        ,ShipmentReceiptNo  integer         
        ,RsnType          VARCHAR(35)        
        ,RsnCode          VARCHAR(35)     
        ,Branch Varchar(3)        
                 );       
                     
CREATE TABLE #tmpFinal (   CompanyName   VARCHAR(10)        
        , ShpgWhs     VARCHAR(65)        
        , Pcs    DECIMAL(20, 2)        
        , Wgt    DECIMAL(20, 2)        
        ,Late    integer        
        ,TranspNO    integer          
        ,OTPType    VARCHAR(35)         
        --,OrderNo   varchar(15)        
        ,ShipmentReceiptNo  integer         
        ,RsnType          VARCHAR(35)        
        ,RsnCode          VARCHAR(35)      
        ,Branch Varchar(3)       
                 );                         
                         
                 CREATE TABLE #tmpTransfer (   CompanyName   VARCHAR(10)        
        , ShpgWhs     VARCHAR(65)        
        , Pcs    DECIMAL(20, 2)        
        , Wgt    DECIMAL(20, 2)        
        ,Late    integer        
        ,TranspNO    integer          
        ,OTPType    VARCHAR(35)         
        ,ShipmentReceiptNo  integer        
        ,RsnType          VARCHAR(35)        
        ,RsnCode          VARCHAR(35)      
        ,Branch Varchar(3)      
                 );        
                                      
DECLARE @company VARCHAR(35);        
DECLARE @prefix VARCHAR(15);         
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @CusID varchar(max);        
Declare @Value as varchar(500);        
DECLARE @CustIDLength int;        
        
if @CustmomerNoTxt <> ''        
 BEGIN        
  set @CustomerID = @CustmomerNoTxt        
 END        
        
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));        
        
        
if @CustomerID = ''        
 BEGIN        
  set @CustomerID = '0'        
 END        
        
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month        
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month        
 End        
else if @DateRange=2 -- Last 7 days (excluding today)        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day        
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day        
 End        
else if @DateRange=3 -- Last 14 days (excluding today)        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day        
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day        
 End        
else if @DateRange=4 --Last 12 months excluding all the days in the current month        
 BEGIN        
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month        
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month        
 End        
else        
 Begin        
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)        
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)        
 End        
        
        --->> Input customer data        
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)        
  SELECT @pos=0        
  SELECT @input =@CustomerID         
  SELECT @input = @input + ','        
  CREATE TABLE #tempTable (temp varchar(max) )        
  WHILE CHARINDEX(',',@input) > 0        
  BEGIN        
   SELECT @pos=CHARINDEX(',',@input)        
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))        
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))        
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)        
  END        
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable        
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)        
  DROP TABLE #tempTable        
               
               
 IF @DBNAME = 'ALL'        
 BEGIN        
 IF @version = '0'        
  BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName, company,prefix from tbl_itech_DatabaseName        
    OPEN ScopeCursor;        
  END        
  ELSE        
  BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS        
    OPEN ScopeCursor;        
  END        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query NVARCHAR(4000);           
       SET @query = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShipmentReceiptNo,RsnType,RsnCode,Branch)        
            select  dpf_cmpy_id as ''CompanyName'', '       if (UPPER(@prefix)= 'US')       BEGIN       SET @query = @query + '   
            Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '       END       ELSE       BEGIN       SET @query = @query + 'ISNULL((case when   
            orh_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po = us.stn_end_usr_po and     
 cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)     
 and cast(dpf_ord_rls_no as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',  
 [dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '       END               SET @query = @query + '        
       (dpf_shp_pcs) as ''Pcs'','    
       -- Changed by mrinal on 21-05    
       if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')              
     BEGIN              
     SET @query = @query + ' (dpf_shp_wgt) * 2.20462 AS ''Wgt'', '              
     END              
       ELSE              
     BEGIN        
                 
     SET @query = @query + ' (dpf_shp_wgt) AS ''Wgt'', '              
     END      
           
     SET @query = @query + 'case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'',        
      dpf_transp_no as ''TranspNO'',        
       ''Regular'' as OTPType, dpf_ord_no as ''orderNO'',dpf_sprc_no as ''ShipmentReceiptNo'',dpf_rsn_typ as ''RsnType'' ,(dpf_rsn) as ''RsnCode'',    
       dpf_ord_brh     
       from '+ @prefix +'_pfhdpf_rec    
       join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no          
       JOIN '+ @prefix +'_ortorl_rec         
 ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no        
       Join '+ @prefix +'_ortord_rec        
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
       join '+ @prefix +'_arrcus_rec        
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)        
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '        
     IF  @CusID Like '%''0''%'        
      BEGIN        
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'         
      END        
     ELSE        
      BEGIN        
          if @CusID Like '%''1111111111''%' --- Eaton Group        
        BEGIN        
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))        
          Set @CusID= @CusID +','+ @Value        
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'         
        END        
       Else        
        BEGIN        
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'        
        END        
      END        
      --Set @query += ' group by dpf_cmpy_id ,orh_shpg_whs , dpf_sprc_pfx ,dpf_transp_no ,        
      --             dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) '        
               
               
        EXECUTE sp_executesql @query;        
        Print(@query);     
        -- transfer       
          SET @query = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode,Branch)        
    SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',(b.tud_comp_pcs) as ''Pcs'','    
        
    -- Changed by mrinal on 21-05    
       if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')              
     BEGIN              
     SET @query = @query + ' (tud_comp_wgt) * 2.20462 AS ''Wgt'', '              
     END              
       ELSE              
     BEGIN        
                 
     SET @query = @query + ' (tud_comp_wgt) AS ''Wgt'', '              
     END      
        
    SET @query = @query + ' case when  (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'',a.tph_transp_no as   
    ''TranspNO'',''Transfer'' as OTPType,(b.tud_sprc_no) as ''ShipmentReceiptNo'','''' as RsnType,'''' as RsnCode        
    ,Replace(b.tud_trpln_whs,''SFS'',''LAX'') as Branch    
    FROM ['+ @prefix +'_trjtph_rec] a        
    left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no        
    left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm   and b.tud_prnt_sitm = c.orl_ord_rls_no       
    left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
    left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
    WHERE a.tph_sch_dtts  BETWEEN '''+ @FD +''' AND '''+ @TD +'''   and ord_ord_brh not in  (''SFS'')       
     AND(        
      (b.tud_prnt_no<>0         
      and TPH_NBR_STP =1         
      and b.tud_sprc_pfx<>''SH''         
      and b.tud_sprc_pfx <>''IT'')          
      OR         
      (b.tud_sprc_pfx in (''IT'',''SH''))       
      )'        
     IF  @CusID Like '%''0''%'        
      BEGIN        
      Set @query += ' and  (ord_sld_cus_id = '''' or ''''= '''')'         
      END        
     ELSE        
      BEGIN        
          if @CusID Like '%''1111111111''%' --- Eaton Group        
        BEGIN        
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))        
          Set @CusID= @CusID +','+ @Value        
          Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'         
        END        
       Else        
        BEGIN        
              Set @query += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'        
        END        
      END        
      --Set @query += 'group by tph_CMPY_ID ,b.tud_trpln_whs ,        
      --                     a.tph_sch_dtts , a.tph_transp_no'        
           
          Print(@query);     
       EXECUTE sp_executesql @query;        
                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
   --print('232')        
   --  Set @DatabaseName=(select DatabaseName from tbl_itech_DatabaseName where company=''+ @DBNAME + '')        
    -- set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'        
     print(@DB)          
             
     Set @prefix= @DBNAME         
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,orderNO,ShipmentReceiptNo,RsnType,RsnCode,Branch)        
            select  dpf_cmpy_id as ''CompanyName'','       if (UPPER(@prefix)= 'US')       BEGIN         
            SET @sqltxt = @sqltxt + '   
            Replace(dpf_shpg_whs,''SFS'',''LAX'') as ''ShpgWhs'', '       END       ELSE       BEGIN         
            SET @sqltxt = @sqltxt + 'ISNULL((case when orh_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where dpf_cus_po = us.stn_end_usr_po and     
 cast(dpf_ord_itm as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)     
 and cast(dpf_ord_rls_no as varchar(2))  = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',  
 [dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else Replace(dpf_shpg_whs,''SFS'',''LAX'') end ),Replace(dpf_shpg_whs,''SFS'',''LAX'')) as ''ShpgWhs'', '       END               SET @sqltxt = @sqltxt + '      
       dpf_shp_pcs as ''Pcs'', '    
           
       -- Changed by mrinal on 21-05    
       if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')              
     BEGIN              
     SET @sqltxt = @sqltxt + ' dpf_shp_wgt * 2.20462 AS ''Wgt'', '              
     END              
       ELSE              
     BEGIN        
                 
     SET @sqltxt = @sqltxt + ' (dpf_shp_wgt) AS ''Wgt'', '              
     END      
          
           
      SET @sqltxt = @sqltxt +  ' case when (CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) > orl_rqst_fm_dt) then 1 else 0 end as ''Late'',        
      dpf_transp_no as ''TranspNO'',        
       ''Regular'' as OTPType, dpf_ord_no as ''orderNO'',dpf_sprc_no as ''ShipmentReceiptNo'',dpf_rsn_typ as ''RsnType'' ,(dpf_rsn) as ''RsnCode''    
       ,dpf_ord_brh        
       from '+ @prefix +'_pfhdpf_rec     
       join ['+ @prefix +'_ortorh_rec]  ON dpf_cmpy_id = orh_cmpy_id and dpf_ord_pfx=orh_ord_pfx AND dpf_ord_no=orh_ord_no         
       JOIN '+ @prefix +'_ortorl_rec         
                             ON dpf_ord_pfx=orl_ord_pfx AND dpf_ord_no=orl_ord_no AND dpf_ord_itm= orl_ord_itm  And dpf_ord_rls_no = orl_ord_rls_no        
       Join '+ @prefix +'_ortord_rec        
       on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
       join '+ @prefix +'_arrcus_rec        
       on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+ @TD +''', 120)        
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') '        
             
     IF  @CusID Like '%''0''%'        
      BEGIN        
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'         
      END        
     ELSE        
      BEGIN        
          if @CusID Like '%''1111111111''%' --- Eaton Group        
        BEGIN        
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))        
          Set @CusID= @CusID +','+ @Value        
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'         
        END        
       Else        
        BEGIN        
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'        
        END        
      END        
     -- Set @sqltxt += ' group by dpf_cmpy_id ,orh_shpg_whs , dpf_sprc_pfx ,dpf_transp_no ,        
     --              dpf_ord_pfx, dpf_ord_no,dpf_ord_rls_no , dpf_ord_brh ,dpf_transp_pfx ,CONVERT(VARCHAR(10), dpf_shpg_dtts, 120) '        
              
    print(@sqltxt)        
   set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
  print(@CusID)        
  SET @sqltxt = 'INSERT INTO #tmpTransfer (CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode,Branch)        
    SELECT tph_CMPY_ID as ''CompanyName'',b.tud_trpln_whs as ''ShpgWhs'',(b.tud_comp_pcs) as ''Pcs'','    
        
     -- Changed by mrinal on 21-05    
       if  (UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')              
     BEGIN              
     SET @sqltxt = @sqltxt + ' (tud_comp_wgt) * 2.20462 AS ''Wgt'', '              
     END              
       ELSE              
     BEGIN        
                 
     SET @sqltxt = @sqltxt + ' (tud_comp_wgt) AS ''Wgt'', '              
     END      
         
        
     SET @sqltxt = @sqltxt + 'case when  (CONVERT(VARCHAR(10), a.tph_sch_dtts, 120) > (c.orl_rqst_fm_dt)) then 1 else 0 end as ''Late'',    
     a.tph_transp_no as ''TranspNO'',''Transfer'' as OTPType,(b.tud_sprc_no) as ''ShipmentReceiptNo'','''' as RsnType,'''' as RsnCode     
     ,Replace(b.tud_trpln_whs,''SFS'',''LAX'') as Branch       
    FROM ['+ @prefix +'_trjtph_rec] a        
    left JOIN ['+ @prefix +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no        
    left JOIN ['+ @prefix +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm  and b.tud_prnt_sitm = c.orl_ord_rls_no        
    left Join ['+ @prefix +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no         
    left join ['+ @prefix +'_arrcus_rec] on ord_cmpy_id = cus_cmpy_id and ord_sld_cus_id= cus_cus_id        
    WHERE a.tph_sch_dtts BETWEEN '''+ @FD +''' AND '''+ @TD +'''  and ord_ord_brh not in  (''SFS'')      
     AND(        
      (b.tud_prnt_no<>0         
      and TPH_NBR_STP =1         
      and b.tud_sprc_pfx<>''SH''         
      and b.tud_sprc_pfx <>''IT'')          
      OR         
      (b.tud_sprc_pfx in (''IT'',''SH''))      
      )'        
              
      IF  @CusID Like '%''0''%'        
      BEGIN        
      Set @sqltxt += ' and  (ord_sld_cus_id = '''' or ''''= '''')'         
      END        
     ELSE        
      BEGIN        
          if @CusID Like '%''1111111111''%' --- Eaton Group        
        BEGIN        
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@prefix,'1111111111'))        
          Set @CusID= @CusID +','+ @Value        
                  
          Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'         
        END        
       Else        
        BEGIN        
              Set @sqltxt += ' and  ord_sld_cus_id IN ('+RTRIM(LTRIM(@CusID))+')'        
        END        
      END       
      --Set @sqltxt += ' and b.tud_sprc_no not in (select ShipmentReceiptNo from #tmp)'     
      --Set @sqltxt += ' group by tph_CMPY_ID ,b.tud_trpln_whs ,        
      --                     a.tph_sch_dtts , a.tph_transp_no'        
             
   print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
   END        
             
 insert into #tmpFinal select CompanyName, Max(ShpgWhs),Sum(Pcs),Sum(Wgt),Sum(Late),TranspNO,    
 OTPType,--orderNO,    
 count(ShipmentReceiptNo),Max(RsnType),Max(RsnCode), MAX(Branch)    
 from #tmp group by CompanyName,ShpgWhs--,Branch --,ShpgWhs    
 ,TranspNO,OTPType--,orderNO    
     
 --select * from #tmp;    
               
SELECT COUNT(distinct TranspNO) as 'TranspNO', CompanyName, Replace(ShpgWhs,'SFS','LAX') as  ShpgWhs ,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',    
OTPType,count(distinct ShipmentReceiptNo) as 'ShipmentReceiptNo' ,'ZZ Late with NO Rsn Code' as RsnCode , Branch       
FROM #tmpTransfer tr --where OTPType = 'Transfer'        
where tr.ShipmentReceiptNo not in (select tm.ShipmentReceiptNo from #tmp tm)    
and (ShpgWhs = @InBranch OR Branch = @InBranch OR @InBranch = '')     
group by CompanyName, ShpgWhs,OTPType ,Branch       
union         
select count(distinct TranspNO) as 'TranspNO', CompanyName, Replace(ShpgWhs,'SFS','LAX') as ShpgWhs,SUM(Pcs) as 'PCS',sum(Wgt) as 'Wgt',sum(Late) as 'Late',    
OTPType,Sum(ShipmentReceiptNo) as 'ShipmentReceiptNo'     
,Case (RsnCode) when '' then 'ZZ Late with NO Rsn Code'     
when null then 'ZZ Late with NO Rsn Code' else (RsnCode) end as RsnCode , Branch       
from #tmpFinal  where (ShpgWhs = @InBranch OR Branch = @InBranch OR @InBranch = '')      
group by CompanyName, ShpgWhs,OTPType,RsnCode,Branch        
order by ShpgWhs        
        
        
--select CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode from #tmp        
--union        
--select CompanyName, ShpgWhs,Pcs,Wgt,Late,TranspNO,OTPType,ShipmentReceiptNo,RsnType,RsnCode from #tmpTransfer        
        
drop table #tmp        
drop Table #tmpTransfer      
drop table #tmpFinal     
        
        
        
END        
        
-- exec sp_itech_OTPSummary_V1 '10/01/2018', '10/31/2018' , 'UK','','',5,0,'BHM'    
/*    
Date:20181122    
Mail: Report Request    
*/
GO
