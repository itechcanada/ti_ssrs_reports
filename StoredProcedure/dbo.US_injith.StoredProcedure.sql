USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_injith]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,12/2/2013,>  
-- Description: <>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_injith]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;      
     IF OBJECT_ID('dbo.US_injith_rec', 'U') IS NOT NULL      
  drop table dbo.US_injith_rec;      
   
  
    -- Insert statements for procedure here  
SELECT *  into dbo.US_injith_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[injith_rec]  
  
  
END  
  
  /*
  20171031
  Change delete to drop of table US_injith_rec
  
  */
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
