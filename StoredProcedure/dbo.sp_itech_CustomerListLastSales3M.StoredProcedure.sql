USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerListLastSales3M]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh>
-- Create date: <03 March 2016>
-- Description:	<Get List of Customer of LAX customer that have no any sales in last 3 month>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_CustomerListLastSales3M]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select cus_cmpy_id, cus_cus_id, cus_cus_nm,cus_admin_brh,convert(Varchar(10),coc_lst_sls_dt,120) as coc_lst_sls_dt from  
  US_arrcus_rec 
 left join US_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id
Where --coc_lst_sls_dt < convert(varchar(10),DATEADD(month,-3, GETDATE()),120) and 
cus_admin_brh = 'LAX' order by coc_lst_sls_dt desc

END

-- exec sp_itech_CustomerListLastSales3M
/*
--2016-05-12
Sub:FW: what about the LAX issues
Can we remove the requirement to limit this to last 3 months of the sale.
Let it run for ALL the customer… with sales or no sale

*/
GO
