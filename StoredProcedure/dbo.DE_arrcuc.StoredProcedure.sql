USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_arrcuc]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/19/2020,>  
-- Description: <Description,Germany arrcuc_rec,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_arrcuc]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
truncate table dbo.DE_arrcuc_rec ;   
-- Insert statements for procedure here 
	
Insert into dbo.DE_arrcuc_rec   
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[arrcuc_rec]  
  
  
END  
/*

*/
GO
