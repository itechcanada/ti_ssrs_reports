USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ortxro]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[TW_ortxro] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.TW_ortxro_rec', 'U') IS NOT NULL
		drop table dbo.TW_ortxro_rec;
    
        
SELECT *
into  dbo.TW_ortxro_rec
FROM [LIVETWSTX].[livetwstxdb].[informix].[ortxro_rec];

END
--- exec TW_ortxro
-- select * from TW_ortxro_rec
GO
