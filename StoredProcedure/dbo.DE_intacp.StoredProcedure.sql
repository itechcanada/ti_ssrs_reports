USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_intacp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/25/2020,>  
-- Description: <Description,Germany Product Average Cost Pool,> 
-- =============================================  
CREATE PROCEDURE [dbo].[DE_intacp]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 
SET NOCOUNT ON;  

Truncate table dbo.DE_intacp_rec ;   
-- Insert statements for procedure here  

Insert into dbo.DE_intacp_rec   
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[intacp_rec]  
  
  
END  
GO
