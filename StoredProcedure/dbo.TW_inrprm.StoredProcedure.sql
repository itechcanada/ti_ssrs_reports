USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_inrprm]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[TW_inrprm]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.TW_inrprm_rec', 'U') IS NOT NULL          
  drop table dbo.TW_inrprm_rec;          
              
                  
SELECT *          
into  dbo.TW_inrprm_rec   
FROM [LIVETWSTX].[livetwstxdb].[informix].[inrprm_rec]     
    
END    
    
GO
