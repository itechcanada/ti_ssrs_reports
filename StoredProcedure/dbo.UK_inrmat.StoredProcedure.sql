USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_inrmat]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 8, 2015,>
-- Description:	<Description,Material,>

-- =============================================
create PROCEDURE [dbo].[UK_inrmat]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_inrmat_rec', 'U') IS NOT NULL
		drop table dbo.UK_inrmat_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.UK_inrmat_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[inrmat_rec] ; 
  
END
-- select * from UK_inrmat_rec
GO
