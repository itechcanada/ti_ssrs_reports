USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_rvtrlg]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Sumit>
-- Create date: <Create Date,Oct 6, 2021,>
-- Description:	<Description,Reservation Update>

-- =============================================
Create PROCEDURE [dbo].[UK_rvtrlg] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.UK_rvtrlg_rec', 'U') IS NOT NULL
		drop table dbo.UK_rvtrlg_rec;
    
        
SELECT *
into  dbo.UK_rvtrlg_rec
FROM [LIVEUKSTX].[liveukstxdb].[informix].[rvtrlg_rec];

END
GO
