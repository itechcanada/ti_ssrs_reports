USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_With_Target_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <12 Dec 2018>          
-- Description: <Getting OTP>          
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_With_Target_V1] @DBNAME varchar(50),@Branch varchar(50),@Month as Datetime ,      
@Market varchar(50),@ISCategory int, @version char = '0'--,@Account varchar(50)          
AS          
BEGIN          
	SET NOCOUNT ON;          
          
	declare @sqltxt varchar(6000)          
	declare @execSQLtxt varchar(7000)          
	declare @DB varchar(100)          
	declare @NFD varchar(10)          
	declare @NTD varchar(10)          
	declare @D3MFD varchar(10)          
	declare @D3MTD varchar(10)          
	declare @D6MFD varchar(10)          
	declare @D6MTD varchar(10)          
	declare @LFD varchar(10)          
	declare @LTD varchar(10)          
	DECLARE @StartDate VARCHAR(15);            
	DECLARE @EndDate varchar(15);          
	declare @AFD varchar(10)          
	declare @ATD varchar(10)          
	declare @RFD varchar(10)          
	declare @RTD varchar(10)          
	--------- 2020-08-21 -----
	declare @D3MNF varchar(10)
	declare @D3MNL varchar(10)
	declare @D6MNF varchar(10)
	declare @D6MNL varchar(10)

--1 New Accounts          
--2 Dormant Accounts 3M          
--3 Dormant Accounts 6M          
--4 Lost Accounts          
--5 Total Reactivated Accounts          
--6 Current Month Total Active Customers       
--10 Target Toatl Active Customers         
--0 ALL          
          
	CREATE TABLE #tmp (
	AccountType varchar(50)          
	,CustomerID  VARCHAR(15)          
	,AccountName VARCHAR(150)          
	,AccountDate varchar(15)          
	,FirstSaleDate Varchar(15)          
	,Branch  VARCHAR(15)          
	,Category  VARCHAR(100)          
	,SalePersonName  VARCHAR(50)          
	,Months varchar(15)          
	,DatabaseName Varchar(3)          
	);          
                  
	CREATE TABLE #tmpFinal (      AccountType varchar(50)          
	,CustomerID  VARCHAR(15)          
	,AccountName VARCHAR(150)          
	,AccountDate varchar(15)          
	,FirstSaleDate Varchar(15)          
	,Branch  VARCHAR(15)          
	,Category  VARCHAR(100)          
	,SalePersonName  VARCHAR(50)          
	,Months varchar(15)          
	,DatabaseName Varchar(3)          
	);                  
                  
	CREATE TABLE #tactive (  CustomerID  VARCHAR(15)          
	,FirstInvOfMonth Date          
	);          
                  
	CREATE TABLE #tTtlactive (  CustomerID  VARCHAR(15)          
	,FirstInvOfMonth Date          
	);          
                 
	DECLARE @Category varchar(50);          
                  
	set @DB= @DBNAME          
	IF @Branch = 'ALL'          
	BEGIN          
		set @Branch = ''          
	END          
           
	IF @Market = 'ALL'            
		set @Market = ''          
	ELSE IF @Market ='Unknown'          
	Begin          
		set @Category='Unknown'          
		set @Market = ''          
	END          
           
--------  +++++  Created Table for last 12 months ++++++++          
           
	Create TABLE #Last12Months (   
		StartDate varchar(15)          
		,EndDate  VARCHAR(15)          
	);          
          
	declare @start DATE = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month)-1, 0) , 120);          
          
	with CTEE(Last12MonthDate)          
	AS          
	(          
		SELECT @start          
		UNION   all          
          
		SELECT DATEADD(month,-1,Last12MonthDate)          
		from CTEE          
		where DATEADD(month,-1,Last12MonthDate)>=DATEADD(month,-12,@start)          
		--where DATEADD(month,-1,Last12MonthDate)>=DATEADD(month,0,@start)          
	)          

	INSERT INTO #Last12Months(StartDate,EndDate)          
	select Last12MonthDate as StartDate,CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Last12MonthDate)+1,0)), 120) as EndDate  
	from CTEE          
                
	DECLARE @DatabaseName VARCHAR(35);          
	DECLARE @Prefix VARCHAR(5);          
	DECLARE @Exists VARCHAR(max);          
	DECLARE @query NVARCHAR(max);           
                
	IF @DBNAME = 'ALL'      
	BEGIN          
		IF @version = '0'          
		BEGIN          
			DECLARE DBAcct CURSOR FOR          
			select DatabaseName,Prefix from tbl_itech_DatabaseName          
			OPEN DBAcct;          
		END          
		ELSE          
		BEGIN          
			DECLARE DBAcct CURSOR FOR          
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS          
			OPEN DBAcct;          
		END           
            
		FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;          
		WHILE @@FETCH_STATUS = 0          
		BEGIN          
			SET @DB= @Prefix          
			DECLARE ScopeCursor CURSOR FOR          
			select StartDate,EndDate from  #Last12Months          
			OPEN ScopeCursor;          
			FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
			WHILE @@FETCH_STATUS = 0          
			BEGIN          
				set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate),0)) , 120)   -- New Account          
				set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)  -- New Account          
          
				set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-3,0)) , 120)   -- Dormant 3 Month          
				set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-2,0)), 120)  -- Dormant 3 Month          

				set @D3MNF = DATEADD(mm, DATEDIFF(mm, 0, @D3MTD) + 1, 0)						-- Next month first day for 3 month dormant
				set @D3MNL = DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @D3MNF) + 3, 0))		-- Next 3 month last day for 3 month dormant
          
				set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-6,0)) , 120)   --Dormant 6 Month          
				set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-5,0)), 120)  -- Dormant 6 Month          
				
				set @D6MNF = DATEADD(mm, DATEDIFF(mm, 0, @D6MTD) + 1, 0)						-- Next month first day for 6 month dormant
				set @D6MNL = DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @D6MNF) + 6, 0))		-- Next 6 month last day for 6 month dormant

				set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)   -- Lost Account          
				set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-17,0)), 120)  -- Lost Account          
                     
				set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)    -- Active Accounts           
				set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)          
          
				--Drop table #tactive1;          
				CREATE TABLE #tactive1 (  
					CustomerID  VARCHAR(15)          
					,FirstInvOfMonth Date          
				);          
                     
				CREATE TABLE #tTtlactive1 (  
					CustomerID  VARCHAR(15)          
					,FirstInvOfMonth Date          
				);          
                       
				SET @query = ' INSERT INTO #tactive1(CustomerID,FirstInvOfMonth)          
				select distinct stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth           
				from '+ @DB +'_sahstn_rec          
				where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''          
				group by stn_sld_cus_id '          
				print @query; 
				EXECUTE sp_executesql @query;          
                     
-- Total Active          
				SET @query = ' INSERT INTO #tTtlactive1(CustomerID,FirstInvOfMonth)          
				select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth           
				from '+ @DB +'_sahstn_rec          
				where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''          
				group by stn_sld_cus_id '          
                         
				print @query;          
				EXECUTE sp_executesql @query;          

				if(@DB = 'US' and @StartDate >=  ('2015-11-01'))         
				begin         
-- Total Active          
					SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT ''6'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
					FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
					t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''                    
					ORDER BY 9 DESC ' 
					print @query; 
					EXECUTE sp_executesql @query;          
                        
-- NEW          
					SET @query = '  INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					select ''1'', STN_SLD_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''           
					from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
					left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID          
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')           
					and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''          
					Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30'
					print @query;
					EXECUTE sp_executesql @query;          
                       
-- Reactivated           
					SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT ''5'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
					FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
					where           
					(CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
					t.CustomerID Not IN           
					( select distinct t1.CustomerID  from #tactive1 t1,          
					'+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID           
					and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')          
					and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec           
					where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''          
					)          
					and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''          
					ORDER BY 9 DESC'
					print @query;
					EXECUTE sp_executesql @query;          
-- 3M Dormant                       
					SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
					--left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and stn_inv_dt Between  '''+ @D3MFD +''' and ''' + @D3MTD +'''   
					and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D3MNF +''' and '''+ @D3MNL +''')
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH '     
					print 'act:2';
                    print @query;    
					EXECUTE sp_executesql @query;          
                         
					SET @query = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
					--left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and stn_inv_dt Between  '''+ @D6MFD +''' and ''' + @D6MTD +'''  
					and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D6MNF +''' and '''+ @D6MNL +''')
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH '  
					print 'act:3';
                    print @query;     
					EXECUTE sp_executesql @query;               
                              
					SET @query = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,          
					Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between '''+ @LFD +''' and   '''+ @LTD +'''        
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)'          
                    print @query;    
					EXECUTE sp_executesql @query;      
              
-- Target Total Active customers start      
					SET @query = 'INSERT INTO #tmp(AccountType,CustomerID,Branch,Months,DatabaseName)          
					SELECT  ''10'',(CASE WHEN MONTH('''+ @StartDate +''') = 1 THEN [janTarget] WHEN MONTH('''+ @StartDate +''') = 2 THEN [febTarget]       
					WHEN MONTH('''+ @StartDate +''') = 3 THEN [marTarget] WHEN MONTH('''+ @StartDate +''') = 4 THEN [aprTarget] WHEN MONTH('''+ @StartDate +''') = 5       
					THEN [mayTarget] WHEN MONTH('''+ @StartDate +''') = 6 THEN [junTarget] WHEN MONTH('''+ @StartDate +''') = 7 THEN [julTarget] WHEN MONTH('''+ @StartDate +''')      
					= 8 THEN [augTarget] WHEN MONTH('''+ @StartDate +''') = 9 THEN [sepTarget] WHEN MONTH('''+ @StartDate +''') = 10 THEN [octTarget] WHEN       
					MONTH('''+ @StartDate +''') = 11 THEN [novTarget] ELSE [decTarget] END) as Target,branchName, '''+ @StartDate +''','''+ @DB +'''      
					from tbl_itech_ActiveAccountTargets      
					where (branchName = '''+ @Branch +''' or '''+ @Branch +'''= '''') and targetYear = YEAR('''+ @StartDate +''') and (databaseName = '''+ @DB +''' or '''+ @DB +'''= '''')'      
                    print @query; 
					EXECUTE sp_executesql @query;          
					
					print 'act:10';          
					--print @query;        
-- Target total Active Customers end      
                     
				End        
				else        
				Begin        
-- Total Active          
					SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT ''6'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
					FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
					t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''                    
					ORDER BY 9 DESC '
					print @query;
					EXECUTE sp_executesql @query;          
                        
-- NEW          
					SET @query = '  INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					select ''1'', STN_SLD_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''           
					from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
					left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID          
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')           
					and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''          
					and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))         
					Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30'
					print @query;
					EXECUTE sp_executesql @query;          
                       
-- Reactivated           
					SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT ''5'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
					'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
					FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
					where           
					(CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
					t.CustomerID Not IN           
					( select distinct t1.CustomerID  from #tactive1 t1,          
					'+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID           
					and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')          
					and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec           
					where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''          
					)          
					and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''          
					ORDER BY 9 DESC'
					print @query;
					EXECUTE sp_executesql @query;          
                       
					SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and STN_INV_DT <= ''' + @NTD +'''       
					and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D3MNF +''' and '''+ @D3MNL +''')
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH           
					Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''
					print 'act:2';
                    print @query; 
					EXECUTE sp_executesql @query;          
                         
					SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and STN_INV_DT <= ''' + @NTD +'''      
					and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D6MNF +''' and '''+ @D6MNL +''')
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH          
					Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +'''' 
					print 'act:3';
                    print @query;     
					EXECUTE sp_executesql @query;               
                              
					SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
					SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,          
					Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
					FROM '+ @DB +'_sahstn_rec          
					INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
					Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
					where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
					and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
					and STN_INV_DT <= ''' + @NTD +'''          
					group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH          
					Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''          
                    print @query;    
					EXECUTE sp_executesql @query;        
              
-- Target Total Active customers start      
					SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,Branch,Months,DatabaseName)          
					SELECT  ''10'',(CASE WHEN MONTH('''+ @StartDate +''') = 1 THEN [janTarget] WHEN MONTH('''+ @StartDate +''') = 2 THEN [febTarget]       
					WHEN MONTH('''+ @StartDate +''') = 3 THEN [marTarget] WHEN MONTH('''+ @StartDate +''') = 4 THEN [aprTarget] WHEN MONTH('''+ @StartDate +''') = 5       
					THEN [mayTarget] WHEN MONTH('''+ @StartDate +''') = 6 THEN [junTarget] WHEN MONTH('''+ @StartDate +''') = 7 THEN [julTarget] WHEN MONTH('''+ @StartDate +''')      
					= 8 THEN [augTarget] WHEN MONTH('''+ @StartDate +''') = 9 THEN [sepTarget] WHEN MONTH('''+ @StartDate +''') = 10 THEN [octTarget] WHEN       
					MONTH('''+ @StartDate +''') = 11 THEN [novTarget] ELSE [decTarget] END) as Target,branchName, '''+ @StartDate +''','''+ @DB +'''      
					from tbl_itech_ActiveAccountTargets      
					where (branchName = '''+ @Branch +''' or '''+ @Branch +'''= '''') and targetYear = YEAR('''+ @StartDate +''') and (databaseName = '''+ @DB +''' or '''+ @DB +'''= '''')'      
                    print 'act:10';              
                    print @query; 
					EXECUTE sp_executesql @query;          
					--print @query;        
-- Target total Active Customers end
				End                
				
				if not exists (select * from #tmp where AccountType ='1' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''1''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query;        
					EXECUTE sp_executesql @query;          
				End          
                   
				if not exists (select * from #tmp where AccountType ='2' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''2''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query;        
					EXECUTE sp_executesql @query;          
				End          
                   
				if not exists (select * from #tmp where AccountType ='3' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''3''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query          
					EXECUTE sp_executesql @query;          
				End          
                       
				if not exists (select * from #tmp where AccountType ='4' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''4''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query;        
					EXECUTE sp_executesql @query;          
				End          
                   
				if not exists (select * from #tmp where AccountType ='5' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''5''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query;        
					EXECUTE sp_executesql @query;          
				End          
                   
				if not exists (select * from #tmp where AccountType ='6' and Months=''+ @StartDate +'')          
				begin           
					SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
					SELECT ''6''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
					print @query;        
					EXECUTE sp_executesql @query;          
				End          
                      
				Drop table #tactive1;          
				Drop table #tTtlactive1;          
                      
				if @DB='US'          
					Insert into #tmpFinal select * from #tmp where Branch not in ('BHM','MTL','TAI')          
				else          
					Insert into #tmpFinal select * from #tmp           
                      
				delete from #tmp          
                      
				FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
			END           
			CLOSE ScopeCursor;          
			DEALLOCATE ScopeCursor;          
                 
			FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;          
		END           
		CLOSE DBAcct;          
		DEALLOCATE DBAcct;          
	END          
	ELSE          
	BEGIN          
		DECLARE ScopeCursor CURSOR FOR          
		select StartDate,EndDate from  #Last12Months          
		OPEN ScopeCursor;          
		FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
		WHILE @@FETCH_STATUS = 0          
		BEGIN     
			set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate),0)) , 120)   -- New Account        
			set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)  -- New Account          
          
			set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-3,0)) , 120)   -- Dormant 3 Month          
			set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-2,0)), 120)  -- Dormant 3 Month          
			
			set @D3MNF = convert(varchar(10), DATEADD(mm, DATEDIFF(mm, 0, @D3MTD) + 1, 0),120)						-- Next month first day for 3 month dormant
			set @D3MNL = convert(varchar(10), DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @D3MNF) + 3, 0)), 120)		-- Next 3 month last day for 3 month dormant
            
			set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-6,0)) , 120)   --Dormant 6 Month          
			set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-5,0)), 120)  -- Dormant 6 Month          

			set @D6MNF = convert(varchar(10), DATEADD(mm, DATEDIFF(mm, 0, @D6MTD) + 1, 0),120)						-- Next month first day for 6 month dormant
			set @D6MNL = convert(varchar(10), DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, @D6MNF) + 6, 0)),120)		-- Next 6 month last day for 6 month dormant
          
			set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)   -- Lost Account          
			set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-17,0)), 120)  -- Lost Account          
                  
			set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)    -- Active Accounts           
			set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)          
                  
			SET @query = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth)          
			select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth           
			from '+ @DB +'_sahstn_rec          
			where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''          
			group by stn_sld_cus_id '          
            print @query;          
			EXECUTE sp_executesql @query;          
                     
-- Total Active          
			SET @query = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth)          
			select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth           
			from '+ @DB +'_sahstn_rec          
			where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''          
			group by stn_sld_cus_id '          
            print 'ttlactive';          
			print @query;          
			EXECUTE sp_executesql @query;          
			
			if(@DB = 'US' and @StartDate >=  ('2015-11-01'))         
			begin        
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT ''6'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
				FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
				t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''                    
				ORDER BY 9 DESC '          
                print 'act:6';          
				print @query;          
                EXECUTE sp_executesql @query;                
-- NEW          
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				select ''1'', STN_SLD_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''           
				from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
				left join '+ @DB +'_arrcrd_rec on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID          
				left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
				and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''         
				Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 '
				print 'act:1';          
				print @query;
				EXECUTE sp_executesql @query;                  
-- Reactivated           
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT ''5'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
				FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
				left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
				where           
				(CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
				t.CustomerID Not IN           
				( select distinct t1.CustomerID  from #tactive t1,          
				'+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID           
				and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')          
				and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec           
				where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  ''' + @NFD + ''' And ''' + @NTD +'''         
				)          
				and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''          
				ORDER BY 9 DESC '          
				print 'act:5';          
				print @query;
				EXECUTE sp_executesql @query;        
--3M Dormant
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				--left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and stn_inv_dt Between  '''+ @D3MFD +''' and ''' + @D3MTD +'''  
				and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D3MNF +''' and '''+ @D3MNL +''')
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH '          
                print 'act:2';
				print @query;
				EXECUTE sp_executesql @query;          
--6M Dormant				      
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				--left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and stn_inv_dt Between  '''+ @D6MFD +''' and ''' + @D6MTD +''' 
				and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D6MNF +''' and '''+ @D6MNL +''')
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
				'     
				print 'act:3'; 
				print @query;          
				EXECUTE sp_executesql @query;         
                           
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,          
				Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat         
				left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id         
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between  '''+ @LFD +''' and ''' + @LTD +'''          
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)          
				'          
                print 'act:4';          
				print @query;     
				EXECUTE sp_executesql @query;          
		               
-- Target Total Active customers start      
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,Branch,Months,DatabaseName)          
				SELECT  ''10'',(CASE WHEN MONTH('''+ @StartDate +''') = 1 THEN [janTarget] WHEN MONTH('''+ @StartDate +''') = 2 THEN [febTarget]       
				WHEN MONTH('''+ @StartDate +''') = 3 THEN [marTarget] WHEN MONTH('''+ @StartDate +''') = 4 THEN [aprTarget] WHEN MONTH('''+ @StartDate +''') = 5       
				THEN [mayTarget] WHEN MONTH('''+ @StartDate +''') = 6 THEN [junTarget] WHEN MONTH('''+ @StartDate +''') = 7 THEN [julTarget] WHEN MONTH('''+ @StartDate +''')      
				= 8 THEN [augTarget] WHEN MONTH('''+ @StartDate +''') = 9 THEN [sepTarget] WHEN MONTH('''+ @StartDate +''') = 10 THEN [octTarget] WHEN       
				MONTH('''+ @StartDate +''') = 11 THEN [novTarget] ELSE [decTarget] END) as Target,branchName, '''+ @StartDate +''','''+ @DB +'''      
				from tbl_itech_ActiveAccountTargets      
				where (branchName = '''+ @Branch +''' or '''+ @Branch +'''= '''') and targetYear = YEAR('''+ @StartDate +''') and (databaseName = '''+ @DB +''' or '''+ @DB +'''= '''')'      
                print 'act:10';          
				print @query;       
				EXECUTE sp_executesql @query;          
				      
-- Target total Active Customers end      
                    
			end        
			else        
			begin                     
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT ''6'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
				FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
				t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''                    
				ORDER BY 9 DESC '          
                print 'act:6';          
				print @query;          
                EXECUTE sp_executesql @query;        
                      
-- NEW          
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				select ''1'', STN_SLD_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''           
				from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
				left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID          
				left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
				and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))         
				'           
                       
				-- Added by mukesh 20151228        
				--if(@Branch = 'LAX')        
				--begin        
				-- set @query = @query + ' and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))  '        
				--End        
                       
				set  @query = @query + ' and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''          
				Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30'
				print 'act:1';          
				print @query;
				EXECUTE sp_executesql @query;          
				     
-- Reactivated           
				SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT ''5'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30 as category,          
				'''' as SalePersonName,'''+  @NFD +''','''+ @DB +'''          
				FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id           
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
				left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id           
				where           
				(CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and          
				t.CustomerID Not IN           
				( select distinct t1.CustomerID  from #tactive t1,          
				'+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID           
				and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')          
				and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec           
				where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''          
				)          
				and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''          
				ORDER BY 9 DESC '          
				print 'act:5';          
				print @query; 
				EXECUTE sp_executesql @query;        
                         
--3M Dormant                    
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and STN_INV_DT <= ''' + @NTD +'''
				and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D3MNF +''' and '''+ @D3MNL +''')
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH           
				Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''          
                print 'act:2';
				print @query;     
				EXECUTE sp_executesql @query;          
--6M Dormant
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and STN_INV_DT <= ''' + @NTD +''' 
				and STN_SLD_CUS_ID not in (select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec where stn_inv_dt between '''+ @D6MNF +''' and '''+ @D6MNL +''')
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH          
				Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''          
				print 'act:3';
				print @query;
				EXECUTE sp_executesql @query;           
                       
                           
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,Months,DatabaseName)          
				SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,          
				Min(STN_OS_SLP),'''+ @StartDate +''','''+ @DB +'''          
				FROM '+ @DB +'_sahstn_rec          
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID          
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
				where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
				and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
				and STN_INV_DT <= ''' + @NTD +'''          
				group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH          
				Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''          
                print 'act:4';          
				print @query;     
				EXECUTE sp_executesql @query;          
		             
-- Target Total Active customers start      
				SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,Branch,Months,DatabaseName)          
				SELECT  ''10'',(CASE WHEN MONTH('''+ @StartDate +''') = 1 THEN [janTarget] WHEN MONTH('''+ @StartDate +''') = 2 THEN [febTarget]       
				WHEN MONTH('''+ @StartDate +''') = 3 THEN [marTarget] WHEN MONTH('''+ @StartDate +''') = 4 THEN [aprTarget] WHEN MONTH('''+ @StartDate +''') = 5       
				THEN [mayTarget] WHEN MONTH('''+ @StartDate +''') = 6 THEN [junTarget] WHEN MONTH('''+ @StartDate +''') = 7 THEN [julTarget] WHEN MONTH('''+ @StartDate +''')      
				= 8 THEN [augTarget] WHEN MONTH('''+ @StartDate +''') = 9 THEN [sepTarget] WHEN MONTH('''+ @StartDate +''') = 10 THEN [octTarget] WHEN       
				MONTH('''+ @StartDate +''') = 11 THEN [novTarget] ELSE [decTarget] END) as Target,branchName, '''+ @StartDate +''','''+ @DB +'''      
				from tbl_itech_ActiveAccountTargets      
				where (branchName = '''+ @Branch +''' or '''+ @Branch +'''= '''') and targetYear = YEAR('''+ @StartDate +''') and (databaseName = '''+ @DB +''' or '''+ @DB +'''= '''')'      
                print 'act:10';          
				print @query;     
				EXECUTE sp_executesql @query;          
-- Target total Active Customers end      
			End                  
                       
			if not exists (select * from #tmp where AccountType ='1' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''1''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query;  
				EXECUTE sp_executesql @query;       
             
				insert into #tmp      
				select '1','0','','','', @Branch,'','', @StartDate,@DB  ;     
            End          
                   
			if not exists (select * from #tmp where AccountType ='2' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''2''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query; 
				EXECUTE sp_executesql @query;      
             
				insert into #tmp      
				select '2','0','','','', @Branch,'','', @StartDate,@DB  ;         
			End          
                   
			if not exists (select * from #tmp where AccountType ='3' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''3''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query          
				EXECUTE sp_executesql @query;         
             
				insert into #tmp      
				select '3','0','','','', @Branch,'','', @StartDate,@DB  ;      
			End          
                       
			if not exists (select * from #tmp where AccountType ='4' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''4''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query; 
				EXECUTE sp_executesql @query;        
             
				insert into #tmp      
				select '4','0','','','', Branch,'','', @StartDate,@DB from #tmp group by Branch;     
			End          
                   
			if not exists (select * from #tmp where AccountType ='5' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''5''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query; 
				EXECUTE sp_executesql @query;        
             
				insert into #tmp      
				select '5','0','','','', @Branch,'','', @StartDate,@DB  ;     
			End          
                   
			if not exists (select * from #tmp where AccountType ='6' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''6''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query; 
				EXECUTE sp_executesql @query;     
             
				insert into #tmp      
				select '6','0','','','', @Branch,'','', @StartDate,@DB  ;     
			End       
-- Total Target account         
			if not exists (select * from #tmp where AccountType ='10' and Months=''+ @StartDate +'')          
			begin           
				SET @query = 'INSERT INTO #tmp(AccountType,Months,DatabaseName,Branch,Category)          
				SELECT ''10''  as A,'''+ @StartDate +''' as M ,'''+ @DB +''',''Test'','''+ @Market +''''          
				print @query; 
				EXECUTE sp_executesql @query;          
			End          
           
			if @DB='US'          
				Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')          
			else          
				Insert into #tmpFinal select * from #tmp           
      
			delete from #tmp          
                   
			FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;          
		END           
		CLOSE ScopeCursor;          
		DEALLOCATE ScopeCursor;          
	END             
	
	IF @Category='Unknown'          
	BEGIN          
		if @ISCategory=1          
		begin          
			select id, Name as AccountType ,      
			Case when id = 10 then MAX(CustomerID) else count(CustomerID) end as CustomerID      
			,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,''      
			as SalePersonName          
			from #tmpFinal inner Join   tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			and (Category = ''+ @Market +'' or ''+ @Market +''='')          
			and (Category is null or Category ='')          
			group by  id, Name,Months,Category          
			Order by id , Months asc          
		End          
		Else          
		begin          
			select id, Name as AccountType ,Case when id = 10 then MAX(CustomerID) else count(CustomerID) end as CustomerID      
			,'' as Category,'' as AccountName,'' as AccountDate, case Months when NULL then @start when '' then @start else Months end as Months,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join  tbl_itech_Account on id=AccountType where  id not in ( 0,7,8)           
			--  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI') )           
			and Category is null          
			group by  id, Name,Months          
			Order by id , Months asc          
		End          
	End          
	ELSE          
	BEGIN          
		if @ISCategory=1          
		begin          
			select id, Name as AccountType ,Case when id = 10 then MAX(CustomerID) else count(CustomerID) end as CustomerID      
			,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,'' as AccountDate,      
			case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join   tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			and (Category = ''+ @Market +'' or ''+ @Market +''='')          
			group by  id, Name,Months,Category          
			Union          
			select '1', Name as AccountType ,0 as CustomerID,'Aerospace' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '2', Name as AccountType ,0 as CustomerID,'Competitor & Mill' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '3', Name as AccountType ,0 as CustomerID,'Industrial' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '4', Name as AccountType ,0 as CustomerID,'Medical' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '5', Name as AccountType ,0 as CustomerID,'Interco' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '1', Name as AccountType ,0 as CustomerID,'Motorsports' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '1', Name as AccountType ,0 as CustomerID,'Oil & Gas' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '1', Name as AccountType ,0 as CustomerID,'Recreational' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Union          
			select '1', Name as AccountType ,0 as CustomerID,'Unknown' as Category,'' as AccountName,'' as AccountDate,case Months when NULL then @start when '' then @start else Months end as Months ,'' as Branch,'' as SalePersonName          
			from #tmpFinal inner Join tbl_itech_Account on id=AccountType where id not in ( 0,7,8)          
			Order by id , Months asc          
		End          
		Else          
		begin          
			select id, Name as AccountType ,        
			(CAse When Months >= ('2015-11-01') ANd Branch='PSM' then 0 else        
			--count(CustomerID)       
			(Case when id = 10 then MAX(CustomerID)       
			when id = 4 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0     
			when id = 1 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0     
			when id = 2 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0      
			when id = 3 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0     
			when id = 5 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0      
			when id = 6 AND  count(CustomerID)  = 1 and Max( case when CustomerID = '0' then 0 else 1 end ) = 0 then 0      
			else count(CustomerID) end      
			)      
			end) as CustomerID,        
			'' as Category,'' as AccountName,'' as AccountDate, case Months when NULL then @start when '' then @start else Months end as Months,Branch as Branch,'' as SalePersonName          
			from #tmpFinal inner Join  tbl_itech_Account on id=AccountType where  id not in ( 0,7,8)  and Branch not in ('Test')         
			-- and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI') )           
			group by  id, Name,Months,Branch          
			Order by id , Months asc          
		End          
	End          
                     
	Drop Table #tTtlactive;          
	Drop table #tmpFinal           
	Drop table #tactive;          
	Drop table #tmp            
	Drop table #Last12Months          
END          
          
-- exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_With_Target 'US','DET','2019-05-18','ALL',0   
-- exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_With_Target 'US','ROS','2020-07-31','ALL',0
-- exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_With_Target_Test 'US', 'ROS', '2020-08-31', 'ALL',0
/*      
Mail sub:Monthly Summary By Branch - IND was executed at 5/6/2019 7:00:04 AM   
20200821	Sumit
remove joining from arbcoc_rec for 3M and 6M dormant
use sahstn table stn_invt_dt for 3M and 6M dormant and add condition to check customer doesn't have record in next 3 and 6 months
*/    
GO
