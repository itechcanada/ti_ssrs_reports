USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerListByCity]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <16 JAN 2018>          
-- Description: <Getting customers list by city for SSRS reports>          
-- =============================================          
-- select * from US_mxrusr_rec Where usr_lgn_id = 'ccutro'        
        
CREATE PROCEDURE [dbo].[sp_itech_CustomerListByCity] @DBNAME varchar(50), @City varchar(100), @State varchar(3)          
          
AS          
BEGIN          
      
           
 SET NOCOUNT ON;        
declare @sqltxt varchar(max)          
declare @execSQLtxt varchar(max)          
declare @DB varchar(100)         
declare @LTD varchar(10)         
declare @D3MFD varchar(10)          
declare @D3MTD varchar(10)         
          
set @DB=  @DBNAME   

if(@State = 'ALL')
begin
set @State = '';
end

if(@City = 'ALL')
begin
set @City = '';
end
          
CREATE TABLE #tmp (    
		 Databases   VARCHAR(2)
		,CustID   VARCHAR(100)          
        ,CustName     VARCHAR(100)          
        ,cusAddress Varchar(150) 
        ,cusCity Varchar(35)      
        ,CusState Varchar(3)   
        ,cusPCD varchar(10) 
        ,CusPhone Varchar(30)    
        ,CustEmail Varchar(100) 
        ,CustomerActive Varchar(5)        
                   );           
 
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(5);          
DECLARE @Name VARCHAR(15);          
          
DECLARE @CurrenyRate varchar(15);          
          
IF @DBNAME = 'ALL'          
 BEGIN          

  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_ALL           
    OPEN ScopeCursor;          
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
      SET @DB= @Prefix          
      print(@DB)          
               
     IF (UPPER(@DB) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@DB) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@DB) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@DB) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@DB) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@DB) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
    Else if(UPPER(@DB) = 'TWCN')          
    begin          
       SET @DB ='TW'          
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
               
        DECLARE @query NVARCHAR(max);          
                   
        SET @query = 'INSERT INTO #tmp (Databases,CustID,CustName ,cusAddress ,cusCity,CusState,cusPCD,CusPhone,CustEmail,CustomerActive)        
select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),
LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)), cva_pcd,Rtrim(ltrim(cva_tel_no)),cva_email,cus_actv
from ' + @DB + '_arrcus_rec      
left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''       
and cva_addr_no = 0 and cva_addr_typ = ''L'' 
where (cva_city like ''%' + @City + '%'' or ''' + @City + ''' = '''') and (cva_st_prov = ''' + @State + ''' or ''' + @State + ''' = '''')           
 '
print(@query)            
        EXECUTE sp_executesql @query; 
        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN           
       print 'starting' ;          
      IF (UPPER(@DBNAME) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@DBNAME) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@DBNAME) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@DBNAME) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
   Else if(UPPER(@DBNAME) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
   Else if(UPPER(@DBNAME) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
    Else if(UPPER(@DBNAME) = 'TWCN')          
    begin          
       SET @DB ='TW'          
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
     print 'Ending else';          
     print @CurrenyRate ;          
               
     SET @sqltxt = 'INSERT INTO #tmp (Databases,CustID,CustName ,cusAddress ,cusCity,CusState,cusPCD,CusPhone,CustEmail,CustomerActive)        
select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),
LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)), cva_pcd,Rtrim(ltrim(cva_tel_no)),cva_email,cus_actv
from ' + @DB + '_arrcus_rec      
left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C''       
and cva_addr_no = 0 and cva_addr_typ = ''L'' 
where (cva_city like ''%' + @City + '%'' or ''' + @City + ''' = '''') and (cva_st_prov = ''' + @State + ''' or ''' + @State + ''' = '''') 
'
print(@sqltxt)          
  set @execSQLtxt = @sqltxt;           
  EXEC (@execSQLtxt); 

     END          
select * from #tmp;           
           
   drop table #tmp ;        
END          
          
-- exec [sp_itech_CustomerListByCity] 'ALL', 'ALL','ALL'   -- 10566        
-- exec [sp_itech_CustomerListByCity] 'US', 'ALL','ALL' -- 2768 --         
      
  
GO
