USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_TCRSTC]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sumit>
-- Create date: <Jan 27 2021>
-- Description:	<to get voucher status>

-- =============================================
CREATE PROCEDURE [dbo].[DE_TCRSTC]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.DE_tcrstc_rec', 'U') IS NOT NULL
		drop table dbo.DE_tcrstc_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.DE_tcrstc_rec
from [LIVEDESTX].[livedestxdb].[informix].tcrstc_rec; 

END
GO
