USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_mcrmss]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/25/2020,>  
-- Description: <Description,Germany Metal Standard,>  
  
-- =============================================  
Create PROCEDURE [dbo].[DE_mcrmss]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

truncate table dbo.DE_mcrmss_rec ;   
  
-- Insert statements for procedure here  
insert into dbo.DE_mcrmss_rec
SELECT *   
  from [LIVEDESTX].[livedestxdb].[informix].[mcrmss_rec] ;   
    
END  
GO
