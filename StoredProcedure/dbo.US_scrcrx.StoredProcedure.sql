USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_scrcrx]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[US_scrcrx] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_scrcrx_rec', 'U') IS NOT NULL
		drop table dbo.US_scrcrx_rec;
    
        
SELECT *
into  dbo.US_scrcrx_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[scrcrx_rec];

END
-- select * from US_scrcrx_rec
GO
