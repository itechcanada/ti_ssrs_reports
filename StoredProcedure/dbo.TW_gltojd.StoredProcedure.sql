USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_gltojd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,19/11/2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[TW_gltojd]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_gltojd_rec', 'U') IS NOT NULL
		drop table dbo.TW_gltojd_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.TW_gltojd_rec
  from [LIVETWGL].[livetwgldb].[informix].[gltojd_rec] ;
  
END

-- Select * from TW_gltojd_rec
GO
