USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_injitd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Author,Sumit>                      
-- Create date: <Create Date,10/27/2020,>                      
-- Description: <Description : Inventory Transaction – Detail for Open Orders,Journal Receiving>                 
-- =============================================                      
CREATE PROCEDURE [dbo].[DE_injitd]                      
AS                      
BEGIN                      
 -- SET NOCOUNT ON added to prevent extra result sets from                      
 -- interfering with SELECT statements.                      
 SET NOCOUNT ON;                      
IF OBJECT_ID('dbo.DE_injitd_rec', 'U') IS NOT NULL                    
  drop table dbo.DE_injitd_rec;                    
                        
                
-- Insert statements for procedure here
      
Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm        
,itd_itm_ctl_no,itd_brh,itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_whs,itd_mill,itd_heat,itd_ownr,itd_bgt_for, itd_invt_cat, itd_upd_dtts, itd_cst_qty_indc     
into dbo.DE_injitd_rec              
FROM [LIVEDESTX].[livedestxdb].[informix].[injitd_rec]      
      
                      
END                      
      
/*      

 */
GO
