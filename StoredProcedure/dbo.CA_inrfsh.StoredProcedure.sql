USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_inrfsh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  itech  
-- Create date:April 4, 2013  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_inrfsh]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.CA_inrfsh_rec', 'U') IS NOT NULL  
  drop table dbo.CA_inrfsh_rec;  
      
          
SELECT *  
into  dbo.CA_inrfsh_rec  
FROM [LIVECASTX].[livecastxdb].[informix].[inrfsh_rec];  
  
END  
  
-- select * from CA_inrfsh_rec
GO
