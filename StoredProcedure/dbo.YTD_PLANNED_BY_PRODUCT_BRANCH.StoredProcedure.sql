USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[YTD_PLANNED_BY_PRODUCT_BRANCH]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,5/7/2012,>
-- Description:	<Description,YTD planned by product>
-- =============================================
CREATE PROCEDURE [dbo].[YTD_PLANNED_BY_PRODUCT_BRANCH]
	-- Add the parameters for the stored procedure here
	@EndDate datetime,
	@Category varchar(25),
	@USD_UK float,
	@USD_TW float,
	@USD_NO float  --YTD_PLANNED_BY_PRODUCT_BRANCH '2012-07-31','Medical',1.4,32,0.165
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 CREATE TABLE #PRODUCT_BRANCH
(
title varchar(35),
value float,
product_branch varchar(25)
)


SELECT  'Lbs Last 12M' as title,(b.sat_blg_wgt) as wgt,b.sat_frm
INTO #USPRO
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
AND c.cuc_desc30=@Category

INSERT INTO #PRODUCT_BRANCH /*Last 12 Months*/
SELECT title, SUM(wgt), sat_frm
FROM #USPRO
GROUP BY title, sat_frm
UNION
SELECT  'Lbs Last 12M',SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Last 12M',SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12    
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Last 12M',SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12    
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Last 12M',SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm

/*sales by branches*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Last 12M',SUM(b.sat_tot_val)*1,a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Last 12M',SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 

AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12  
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_UK)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Last 12M',SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Last 12M',SUM(b.sat_tot_val)/(@USD_TW),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12    
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)/(@USD_TW)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Last 12M',SUM(b.sat_tot_val)*(@USD_NO),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12  
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_NO)>0

/*Lbs by branches*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Last 12M',SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Last 12M',SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12  
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Last 12M',SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12     
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Last 12M',SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12    
GROUP BY a.cus_admin_brh

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Last 12M',SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND  DATEDIFF(month,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12  
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh

/*Calculate GP% Last 12M*/

SELECT 'GP Last 12M' AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
INTO #GP_L12M
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Last 12M' AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Last 12M' AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Last 12M' AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Last 12M' AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 1 AND 12
GROUP BY a.cus_admin_brh

/*Prior Year*/
SELECT  'Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,(b.sat_blg_wgt) as wgt,b.sat_frm
INTO #USLBS
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category

INSERT INTO #PRODUCT_BRANCH 
SELECT title, SUM(wgt), sat_frm
FROM #USLBS
GROUP BY title, sat_frm
UNION
SELECT  'Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm

/*sales by branches Prior Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*1,a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_UK)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)/(@USD_TW),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)/(@USD_TW)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*(@USD_NO),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_NO)>0


/*Lbs by branches Prior Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh

/*Calculate GP% Prior Year*/
SELECT 'GP Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
INTO #GP_PriorYear
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
GROUP BY a.cus_admin_brh

/*Prior Year Same Period as Current YTD*/

SELECT  'Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) as title,(b.sat_blg_wgt) as wgt,b.sat_frm
INTO #USLBSPRI
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category



INSERT INTO #PRODUCT_BRANCH 
SELECT title, SUM(wgt), sat_frm
FROM #USLBSPRI
GROUP BY title, sat_frm
UNION
SELECT  'Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm

/*sales by branches Prior Year Periods as Current Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*1,a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_UK)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)/(@USD_TW),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)/(@USD_TW)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_tot_val)*(@USD_NO),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_NO)>0

/*Lbs by branches Prior Year Periods as Current Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh


/*Calculate GP% Prior Periods same as Current Year*/
SELECT 'GP Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
INTO #GP_PriorPeriod
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Period '+LEFT(DATEPART(YEAR,@EndDate)-1,4) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)-1
AND DATEPART(MONTH,b.sat_inv_dt) <=DATEPART(MONTH,@EndDate)
GROUP BY a.cus_admin_brh

/*Current Year YTD*/

SELECT  'Lbs Year '+DATENAME(YEAR,@EndDate) AS title,(b.sat_blg_wgt) as wgt,b.sat_frm
INTO #USLBSCUR
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category

INSERT INTO #PRODUCT_BRANCH 
SELECT title, SUM(wgt), sat_frm
FROM #USLBSCUR
GROUP BY title, sat_frm
UNION
SELECT  'Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt) as wgt,b.sat_frm
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm
UNION
SELECT  'Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt)*(2.2) as wgt,b.sat_frm
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY b.sat_frm

/*sales by branches Current Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_tot_val)*1,a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_UK)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_tot_val)*(@USD_UK),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*1>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_tot_val)/(@USD_TW),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)/(@USD_TW)>0

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales $ Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_tot_val)*(@USD_NO),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh
HAVING SUM(b.sat_tot_val)*(@USD_NO)>0

/*Lbs by branches Current Year*/
INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh


INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt),a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh

INSERT INTO #PRODUCT_BRANCH
SELECT  'Sales Lbs Year '+DATENAME(YEAR,@EndDate),SUM(b.sat_blg_wgt)*(2.2),a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
AND c.cuc_desc30=@Category  
GROUP BY a.cus_admin_brh

/*Calculate GP% Current Year*/
SELECT 'GP Year '+DATENAME(YEAR,@EndDate) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
INTO #GP_CurrentYear
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+DATENAME(YEAR,@EndDate) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+DATENAME(YEAR,@EndDate) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+DATENAME(YEAR,@EndDate) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh

UNION
SELECT 'GP Year '+DATENAME(YEAR,@EndDate) AS title,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff,a.cus_admin_brh 
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEPART(YEAR,b.sat_inv_dt) =DATEPART(YEAR,@EndDate)
GROUP BY a.cus_admin_brh


/*Open Order*/

CREATE TABLE #OPEN_ORDER
(
title varchar(25),
wgt float,
total float,
product_branch varchar(25)
)
/*US OPEN ORDER*/
SELECT a.chl_chrg_val,a.chl_ref_no,a.chl_ref_itm
INTO #US1
FROM [US_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT (b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_no,b.ord_ord_itm
INTO #US2
 FROM [US_ortord_rec] b 
INNER JOIN [US_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
INNER JOIN [US_arrcuc_rec]	d ON c.cus_cus_cat=d.cuc_cus_cat
WHERE 
b.ord_sts_actn= 'A' AND
d.cuc_desc30=@Category
AND b.ord_ord_pfx='SO'

INSERT INTO #OPEN_ORDER
SELECT 'Open Order', SUM(b.ord_bal_wgt), SUM(a.chl_chrg_val), b.ord_ord_brh
FROM #US1 a
INNER JOIN #US2 b ON a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
GROUP BY b.ord_ord_brh

/*UK OPEN ORDER*/
SELECT a.chl_chrg_val,a.chl_ref_no,a.chl_ref_itm
INTO #UK1
FROM [UK_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT (b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_no,b.ord_ord_itm
INTO #UK2
 FROM [UK_ortord_rec] b 
INNER JOIN [UK_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
INNER JOIN [UK_arrcuc_rec]	d ON c.cus_cus_cat=d.cuc_cus_cat
WHERE 
b.ord_sts_actn= 'A' AND
d.cuc_desc30=@Category
AND b.ord_ord_pfx='SO'

INSERT INTO #OPEN_ORDER
SELECT 'Open Order', SUM(b.ord_bal_wgt)*(2.2), SUM(a.chl_chrg_val), b.ord_ord_brh
FROM #UK1 a
INNER JOIN #UK2 b ON a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
GROUP BY b.ord_ord_brh

/*CANADA OPEN ORDER*/
SELECT a.chl_chrg_val,a.chl_ref_no,a.chl_ref_itm
INTO #CA1
FROM [CA_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT (b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_no,b.ord_ord_itm
INTO #CA2
 FROM [CA_ortord_rec] b 
INNER JOIN [CA_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
INNER JOIN [CA_arrcuc_rec]	d ON c.cus_cus_cat=d.cuc_cus_cat
WHERE 
b.ord_sts_actn= 'A' AND
d.cuc_desc30=@Category 
AND b.ord_ord_pfx='SO'

INSERT INTO #OPEN_ORDER
SELECT 'Open Order', SUM(b.ord_bal_wgt), SUM(a.chl_chrg_val), b.ord_ord_brh
FROM #CA1 a
INNER JOIN #CA2 b ON a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
GROUP BY b.ord_ord_brh

/*TAIWAN OPEN ORDER*/
SELECT a.chl_chrg_val,a.chl_ref_no,a.chl_ref_itm
INTO #TW1
FROM [TW_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT (b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_no,b.ord_ord_itm
INTO #TW2
 FROM [TW_ortord_rec] b 
INNER JOIN [TW_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
INNER JOIN [TW_arrcuc_rec]	d ON c.cus_cus_cat=d.cuc_cus_cat
WHERE 
b.ord_sts_actn= 'A' AND
d.cuc_desc30=@Category
AND b.ord_ord_pfx='SO'

INSERT INTO #OPEN_ORDER
SELECT 'Open Order', SUM(b.ord_bal_wgt), SUM(a.chl_chrg_val), b.ord_ord_brh
FROM #TW1 a
INNER JOIN #TW2 b ON a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
GROUP BY b.ord_ord_brh

/*NORWAY OPEN ORDER*/
SELECT a.chl_chrg_val,a.chl_ref_no,a.chl_ref_itm
INTO #NO1
FROM [NO_ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT (b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_no,b.ord_ord_itm
INTO #NO2
 FROM [NO_ortord_rec] b 
INNER JOIN [NO_arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
INNER JOIN [NO_arrcuc_rec]	d ON c.cus_cus_cat=d.cuc_cus_cat
WHERE 
b.ord_sts_actn= 'A' AND
d.cuc_desc30=@Category
AND b.ord_ord_pfx='SO'

INSERT INTO #OPEN_ORDER
SELECT 'Open Order', SUM(b.ord_bal_wgt)*(2.2), SUM(a.chl_chrg_val), b.ord_ord_brh
FROM #NO1 a
INNER JOIN #NO2 b ON a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
GROUP BY b.ord_ord_brh


CREATE TABLE #FINAL
(
title varchar(25),
value float,
product_branch varchar(25)
)
INSERT INTO #FINAL

SELECT title, SUM(diff)/SUM(sell_freight)*100, cus_admin_brh
FROM #GP_PriorPeriod
GROUP BY title, cus_admin_brh
UNION
SELECT title, SUM(diff)/SUM(sell_freight)*100, cus_admin_brh
FROM #GP_PriorYear
GROUP BY title, cus_admin_brh
UNION
SELECT title, SUM(diff)/SUM(sell_freight)*100, cus_admin_brh
FROM #GP_L12M
GROUP BY title, cus_admin_brh
UNION
SELECT title, SUM(diff)/SUM(sell_freight)*100, cus_admin_brh
FROM #GP_CurrentYear
GROUP BY title, cus_admin_brh
UNION
SELECT title, SUM(value), product_branch FROM #PRODUCT_BRANCH
GROUP BY title, product_branch
UNION
SELECT 'Open Order Lbs', SUM(wgt), product_branch FROM #OPEN_ORDER
GROUP BY title, product_branch
UNION
SELECT 'Open Order $', SUM(total), product_branch FROM #OPEN_ORDER
GROUP BY title, product_branch

SELECT * FROM #FINAL

DROP TABLE #USLBSCUR
DROP TABLE #USLBSPRI
DROP TABLE #USLBS
DROP TABLE #USPRO
DROP TABLE #PRODUCT_BRANCH
DROP TABLE #FINAL
DROP TABLE #GP_CurrentYear
DROP TABLE #GP_L12M
DROP TABLE #GP_PriorPeriod
DROP TABLE #GP_PriorYear
DROP TABLE #CA1
DROP TABLE #CA2
DROP TABLE #TW1
DROP TABLE #OPEN_ORDER
DROP TABLE #TW2
DROP TABLE #UK1
DROP TABLE #UK2
DROP TABLE #US1
DROP TABLE #US2
DROP TABLE #NO1
DROP TABLE #NO2




END

GO
