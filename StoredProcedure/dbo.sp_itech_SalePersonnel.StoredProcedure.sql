USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalePersonnel]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <15 Jul 2014>  
-- Description: <Getting Salesperson GP Details>  
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_SalePersonnel] @DBNAME varchar(50), @Year as varchar(4),@Branch as varchar(65), @SalePerson as Varchar(65) -- @Quater as CHAR(1) ,  
  
AS  
BEGIN  
  
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(10)  
DECLARE @CurrentDate DATE
declare @FD1Q varchar(10)  
declare @TD1Q varchar(10)  
declare @FD2Q varchar(10)  
declare @TD2Q varchar(10)  
declare @FD3Q varchar(10)  
declare @TD3Q varchar(10)  
declare @FD4Q varchar(10)  
declare @TD4Q varchar(10) 
declare @FDCQ varchar(10)  
declare @TDCQ varchar(10) 

SET @CurrentDate = GETDATE()
SET @FDCQ = CONVERT(VARCHAR(10), DATEADD(q, DATEDIFF(q, 0, @CurrentDate), 0), 120)  
SET @TDCQ = CONVERT(VARCHAR(10), DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, @CurrentDate) + 1, 0)) , 120)  
--print @TDCQ;

set @FD1Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-01-01', 120) 
set @TD1Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-03-31', 120)
set @FD2Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-04-01', 120) 
set @TD2Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-06-30', 120)
set @FD3Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-07-01', 120) 
set @TD3Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-09-30', 120)  
set @FD4Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-10-01', 120) 
set @TD4Q = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(@Year)) + '-12-31', 120)
 
set @DB=  @DBNAME 
--set @Branch = 'ALL'
IF @Branch = 'ALL'  
 BEGIN  
  set @Branch = ''  
 END  
   
--set @SalePerson ='ALL'
 IF @SalePerson = 'ALL'  
 BEGIN  
  set @SalePerson = ''  
 END  
   
CREATE TABLE #tmp ([Database]   VARCHAR(3)    
     , Branch			VARCHAR(4)   
     , SlpType			CHAR(1)  
     , IOSlp			VARCHAR(5)  
     , loginID			VARCHAR(9)  
     , UsrName			VARCHAR(40)  
     , noOfShipment		Numeric  
     , TotalSales		Decimal(20,2)  
     , GPPct1Q			Decimal(20,2)
     , GPPct2Q			Decimal(20,2)
     , GPPct3Q			Decimal(20,2)
     , GPPct4Q			Decimal(20,2)  
     --, GPPctPQ			Decimal(20,2)  
     --, GPPctDiff		Decimal(20,2) 
     , CustomerVisits	Varchar(50)
     , PhoneCalls		Varchar(50)
     , ProspectCalls	Varchar(50)
     , TotalCustomers	Varchar(50)
     , ExcessInv		Varchar(50)
     , ItineraryComp	Varchar(50)
                 );   
  CREATE TABLE #tmpAct ([Database]		 VARCHAR(3)
					, Branch			VARCHAR(4)      
					, LoginID		 VARCHAR(9)
   					, ActPhoneCall	 integer
   					, ActExisting	 integer
   					, ActProspect	 integer
   					--, TskPhoneCall	 integer
   					--, TskExisting	 integer
   					--, TskProspect	 integer
   	             );	
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15); 
DECLARE @CurrenyRate varchar(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(3000); 
        
        IF (UPPER(@Prefix) = 'TW')      
		begin      
		   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
		End      
		Else if (UPPER(@Prefix) = 'NO')      
		begin      
		 SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
		End      
		Else if (UPPER(@Prefix) = 'CA')      
		begin      
		 SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
		End      
		Else if (UPPER(@Prefix) = 'CN')      
		begin      
		 SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
		End      
		Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')      
		begin      
		 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
		End      
		Else if(UPPER(@Prefix) = 'UK')      
		begin      
		   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
		End  
		Else if(UPPER(@Prefix) = 'DE')      
		begin      
		   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
		End  
        
      SET @DB= @Prefix   
               --For Inside sales person  
       SET @query ='INSERT INTO #tmp ([Database],Branch,SlpType,IOSlp,loginID,UsrName,noOfShipment,TotalCustomers, TotalSales,GPPct1Q,GPPct2Q,GPPct3Q,GPPct4Q)   
              select ''' +  @Prefix + ''',stn.stn_shpt_brh, ''I'', stn.stn_is_slp,slp_lgn_id,usr_nm,'
              if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK' )
				BEGIN
					  SET @query = @query + ' SUM(stn.stn_blg_wgt * 2.20462), ' 
			   END
			   ELSE
			   BEGIN
					SET @query = @query + ' SUM(stn.stn_blg_wgt), ' 
			   END 
			   SET @query = @query + ' COUNT(Distinct stn.stn_sld_cus_id),  sum(ISNULL(stn.stn_tot_val,0) * '+ @CurrenyRate +') as TotalSales,   
                
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= '''+ @FD1Q + ''' and PQ.stn_inv_dt <= ''' + @TD1Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh),  
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= '''+ @FD2Q + ''' and PQ.stn_inv_dt <= ''' + @TD2Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) ,  
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= '''+ @FD3Q + ''' and PQ.stn_inv_dt <= ''' + @TD3Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) , 
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= '''+ @FD4Q + ''' and PQ.stn_inv_dt <= ''' + @TD4Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh)  
        
              from ' + @Prefix + '_sahstn_rec stn  
              join ' + @Prefix + '_scrslp_rec on slp_cmpy_id = stn_cmpy_id and slp_slp = stn_is_slp  
              join ' + @Prefix + '_mxrusr_rec on usr_usr_cmpy_id = slp_cmpy_id and usr_lgn_id = slp_lgn_id  
              Where (stn_shpt_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')  
      And (stn_is_slp = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
      and  stn_inv_Dt >= ''' + @FDCQ + ''' and stn_inv_dt <= ''' + @TDCQ + ''' and stn_frm <> ''XXXX''  
      group by stn_shpt_brh,stn_is_slp,slp_lgn_id,usr_nm  
      '  
      print(@query);  
        EXECUTE sp_executesql @query;  
          
         --For OutSide sales person  
     SET @query ='INSERT INTO #tmp ([Database],Branch,SlpType,IOSlp,loginID,UsrName,noOfShipment, TotalCustomers, TotalSales,GPPct1Q,GPPct2Q,GPPct3Q,GPPct4Q)
              select ''' +  @Prefix + ''',stn.stn_shpt_brh, ''O'', stn.stn_os_slp,slp_lgn_id,usr_nm, '
              if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK' )
				BEGIN
					  SET @query = @query + ' SUM(stn.stn_blg_wgt * 2.20462), ' 
			   END
			   ELSE
			   BEGIN
					SET @query = @query + ' SUM(stn.stn_blg_wgt), ' 
			   END 
			   SET @query = @query + ' COUNT(Distinct stn.stn_sld_cus_id),  sum(ISNULL(stn.stn_tot_val,0) * '+ @CurrenyRate +') as TotalSales,   
                
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD1Q + ''' and PQ.stn_inv_dt <= ''' + @TD1Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) ,  
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD2Q + ''' and PQ.stn_inv_dt <= ''' + @TD2Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) ,  
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD3Q + ''' and PQ.stn_inv_dt <= ''' + @TD3Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) ,
      
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @Prefix + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD4Q + ''' and PQ.stn_inv_dt <= ''' + @TD4Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh)   
        
              from ' + @Prefix + '_sahstn_rec stn  
              join ' + @Prefix + '_scrslp_rec on slp_cmpy_id = stn_cmpy_id and slp_slp = stn_os_slp  
              join ' + @Prefix + '_mxrusr_rec on usr_usr_cmpy_id = slp_cmpy_id and usr_lgn_id = slp_lgn_id  
              Where (stn_shpt_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')  
      And (stn_os_slp = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
      and  stn_inv_Dt >= ''' + @FDCQ + ''' and stn_inv_dt <= ''' + @TDCQ + ''' and stn_frm <> ''XXXX''  
      group by stn_shpt_brh,stn_os_slp,slp_lgn_id,usr_nm  
      '  
      print(@query);  
        EXECUTE sp_executesql @query; 
        
        SET @query ='INSERT INTO #tmpAct ([Database],Branch, LoginID, ActPhoneCall, ActExisting, ActProspect) 
  				        Select * from (
  				        select ''' +  @Prefix + ''' as [Database],atv_brh,atv_crtd_lgn_id, rtrim(att_desc30) As att_desc30, COUNT(*) as counts 
  				        from ' + @Prefix + '_cctatv_rec join 
                        ' + @Prefix + '_ccratt_rec on att_crm_actvy_typ = atv_crm_actvy_typ
						where CONVERT(VARCHAR(10), atv_crtd_dtts , 120) >= '''+ @FDCQ +''' and CONVERT(VARCHAR(10), atv_crtd_dtts , 120) <= '''+ @TDCQ +'''
						And (atv_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')
						And (atv_crtd_lgn_id = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')
						group by atv_brh, atv_crtd_lgn_id, att_desc30) AS Query1
						pivot (Max(counts) for att_desc30 in([PhoneCall],[Visit Existing],[Visit Prospect])) As po'
		 print(@query);  
        EXECUTE sp_executesql @query; 
						 
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN 
       
      IF (UPPER(@DB) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@DB) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if (UPPER(@DB) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@DB) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
				End
				Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')
				begin
					SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@DB) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@DB) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
				Else if(UPPER(@DB) = 'TWCN')
				begin
				   SET @DB ='TW'
				   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				
       set @DatabaseName =  (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')
    --For Inside sales person  
     SET @sqltxt ='INSERT INTO #tmp ([Database],Branch,SlpType,IOSlp,loginID,UsrName,noOfShipment, TotalCustomers, TotalSales,GPPct1Q,GPPct2Q,GPPct3Q,GPPct4Q)
              select ''' +  @DB + ''',stn.stn_shpt_brh, ''I'', stn.stn_is_slp,slp_lgn_id,usr_nm,'
               if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK' )
				BEGIN
					  SET @sqltxt = @sqltxt + ' SUM(stn.stn_blg_wgt * 2.20462), ' 
			   END
			   ELSE
			   BEGIN
					SET @sqltxt = @sqltxt + ' SUM(stn.stn_blg_wgt), ' 
			   END
              
      SET @sqltxt = @sqltxt + ' COUNT(Distinct stn.stn_sld_cus_id),    
              sum(ISNULL(stn.stn_tot_val,0) * '+ @CurrenyRate +') as TotalSales,   
              
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD1Q + ''' and PQ.stn_inv_dt <= ''' + @TD1Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh),
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD2Q + ''' and PQ.stn_inv_dt <= ''' + @TD2Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) ,
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD3Q + ''' and PQ.stn_inv_dt <= ''' + @TD3Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh),
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD4Q + ''' and PQ.stn_inv_dt <= ''' + @TD4Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_is_slp = stn.stn_is_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh)  
        
              from ' + @DB + '_sahstn_rec stn  
              join ' + @DB + '_scrslp_rec on slp_cmpy_id = stn_cmpy_id and slp_slp = stn_is_slp  
              join ' + @DB + '_mxrusr_rec on usr_usr_cmpy_id = slp_cmpy_id and usr_lgn_id = slp_lgn_id  
              Where (stn_shpt_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')  
      And (stn_is_slp = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
      and  stn_inv_Dt >= ''' + @FDCQ + ''' and stn_inv_dt <= ''' + @TDCQ + ''' and stn_frm <> ''XXXX''  
      group by stn_shpt_brh,stn_is_slp,slp_lgn_id,usr_nm  
      '  
      print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
       --For OutSide sales person  
     SET @sqltxt ='INSERT INTO #tmp ([Database],Branch,SlpType,IOSlp,loginID,UsrName,noOfShipment, TotalCustomers, TotalSales,GPPct1Q,GPPct2Q,GPPct3Q,GPPct4Q)
              select ''' +  @DB + ''',stn.stn_shpt_brh, ''O'', stn.stn_os_slp,slp_lgn_id,usr_nm, '
               if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK' )
				BEGIN
					  SET @sqltxt = @sqltxt + ' SUM(stn.stn_blg_wgt * 2.20462), ' 
			   END
			   ELSE
			   BEGIN
					SET @sqltxt = @sqltxt + ' SUM(stn.stn_blg_wgt), ' 
			   END
              
      SET @sqltxt = @sqltxt + ' COUNT(Distinct stn.stn_sld_cus_id),     
              sum(ISNULL(stn.stn_tot_val,0) * '+ @CurrenyRate +') as TotalSales,   
                
      (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD1Q + ''' and PQ.stn_inv_dt <= ''' + @TD1Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) as GPPctPrvQtr,  
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD2Q + ''' and PQ.stn_inv_dt <= ''' + @TD2Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) as GPPctPrvQtr,  
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD3Q + ''' and PQ.stn_inv_dt <= ''' + @TD3Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) as GPPctPrvQtr,  
      
       (select (CASE WHEN sum(PQ.stn_tot_val)> 0 THEN (sum(PQ.stn_mpft_avg_val)/sum(PQ.stn_tot_val))*100 ELSE 0 END)  from  
      ' + @DB + '_sahstn_rec PQ Where PQ.stn_inv_Dt >= ''' + @FD4Q + ''' and PQ.stn_inv_dt <= ''' + @TD4Q + ''' and PQ.stn_frm <> ''XXXX''   
      and PQ.stn_os_slp = stn.stn_os_slp and PQ.stn_shpt_brh = stn.stn_shpt_brh) as GPPctPrvQtr 
        
              from ' + @DB + '_sahstn_rec stn  
              join ' + @DB + '_scrslp_rec on slp_cmpy_id = stn_cmpy_id and slp_slp = stn_os_slp  
              join ' + @DB + '_mxrusr_rec on usr_usr_cmpy_id = slp_cmpy_id and usr_lgn_id = slp_lgn_id  
              Where (stn_shpt_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')  
      And (stn_os_slp = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
      and  stn_inv_Dt >= ''' + @FDCQ + ''' and stn_inv_dt <= ''' + @TDCQ + ''' and stn_frm <> ''XXXX''  
      group by stn_shpt_brh,stn_os_slp,slp_lgn_id,usr_nm  
      '  
    print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
   
    SET @sqltxt ='INSERT INTO #tmpAct ([Database],Branch, LoginID, ActPhoneCall, ActExisting, ActProspect) 
  				        Select * from (
  				        select ''' +  @DB + ''' as [Database],atv_brh,atv_crtd_lgn_id, rtrim(att_desc30) As att_desc30, COUNT(*) as counts 
  				        from ' + @DB + '_cctatv_rec join 
                        ' + @DB + '_ccratt_rec on att_crm_actvy_typ = atv_crm_actvy_typ
						where CONVERT(VARCHAR(10), atv_crtd_dtts , 120) >= '''+ @FDCQ +''' and CONVERT(VARCHAR(10), atv_crtd_dtts , 120) <= '''+ @TDCQ +'''
						And (atv_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')
						And (atv_crtd_lgn_id = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')
						group by atv_brh, atv_crtd_lgn_id, att_desc30) AS Query1
						pivot (Max(counts) for att_desc30 in([PhoneCall],[Visit Existing],[Visit Prospect])) As po'
	print(@sqltxt) ; 
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);
   
     END  
  select t.[Database],t.Branch,SlpType,IOSlp,t.loginID,UsrName,noOfShipment, TotalCustomers, TotalSales,GPPct1Q,GPPct2Q,GPPct3Q,GPPct4Q,ActExisting as CustomerVisits,
  ActPhoneCall as PhoneCalls,ActProspect as ProspectCalls from #tmp t
  left join #tmpAct ta on t.[Database] = ta.[Database] and t.Branch =ta.Branch and t.loginID = ta.LoginID;
  Drop table #tmpAct;
  Drop table #tmp;  
END  
  
  
--Exec [sp_itech_SalePersonnel] 'US','2014','ALL','ALL'  
GO
