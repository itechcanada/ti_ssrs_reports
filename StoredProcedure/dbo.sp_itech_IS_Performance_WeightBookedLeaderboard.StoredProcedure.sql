USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_IS_Performance_WeightBookedLeaderboard]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Mukes>                  
-- Create date: <14-02-2017>                  
-- Description: <Booking Daily Reports>                 
              
-- =============================================                  
CREATE PROCEDURE [dbo].[sp_itech_IS_Performance_WeightBookedLeaderboard]  @DBNAME varchar(50), @Branch varchar(3)                
AS                  
BEGIN                  
                
 -- SET NOCOUNT ON added to prevent extra result sets from                  
 SET NOCOUNT ON;                  
declare @DB varchar(100);                  
declare @sqltxt varchar(6000);                  
declare @execSQLtxt varchar(7000);                  
DECLARE @CountryName VARCHAR(25);                     
DECLARE @prefix VARCHAR(15);                     
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @CurrenyRate varchar(15);                
declare @FD varchar(10)                    
declare @TD varchar(10)             
--declare @FD12 varchar(10)         
declare @CurrentMonthTotalWorkdays int                        
declare @WorkdaysElapsed int           
                  
   set @FD = CONVERT(VARCHAR(10), DATEADD(week, -8, GETDATE()),120)                
 set @TD = CONVERT(VARCHAR(10), GETDATE(),120)             
 --set @FD12 = CONVERT(VARCHAR(10), DATEADD(month, -12 ,@toDate) ,120)              
set @CurrentMonthTotalWorkdays = dbo.[DATEDIFF_WorkingDaysOnly](DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0),DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)));              
set @WorkdaysElapsed = dbo.[DATEDIFF_WorkingDaysOnly](DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0),GETDATE());              
Print @CurrentMonthTotalWorkdays;         
print   @WorkdaysElapsed;              
IF @Branch = 'ALL'                   
 BEGIN                  
  set @Branch = ''                  
 END                 
 declare @start varchar = ''                
                  
CREATE TABLE #temp ( Dbname   VARCHAR(10)                  
     ,ISlp  VARCHAR(35)  
     ,Wgt Decimal(20,0)              
     , ActvyMonth   int        
     );                  
                  
IF @DBNAME = 'ALL'                  
 BEGIN                  
                   
  DECLARE ScopeCursor CURSOR FOR                  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                   
    OPEN ScopeCursor;                  
                    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                  
  WHILE @@FETCH_STATUS = 0                  
  BEGIN                  
   DECLARE @query NVARCHAR(MAX);                  
   IF (UPPER(@Prefix) = 'TW')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@Prefix) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@Prefix) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@Prefix) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@Prefix) = 'UK')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End                  
    Else if(UPPER(@Prefix) = 'DE')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End                  
                      
    SET @query = 'INSERT INTO #temp (Dbname,  ISlp, Wgt, ActvyMonth)                  
  SELECT '''+ @Prefix +''' as Country,  usr_nm,  '
      IF(UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK' ) 
      BEGIN
      set @query = @query + ' a.mbk_wgt*(2.20462), '
      END
      ELSE
      BEGIN
      set @query = @query + ' a.mbk_wgt, '
      END
      
      set @query = @query + ' Month(CAST(a.mbk_actvy_dt AS datetime))           
      FROM ' + @Prefix + '_ortmbk_rec a                  
INNER JOIN ' + @Prefix + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id 
 join  ' + @Prefix + '_scrslp_rec on slp_cmpy_id = a.mbk_cmpy_id and slp_slp = a.mbk_is_slp          
  join ' + @Prefix + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                   
      WHERE a.mbk_ord_pfx=''SO''                   
        AND a.mbk_ord_itm<>999                   
         and a.mbk_trs_md = ''A''                    
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''         
            AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
                       
         '                 
                          
                  
   print @query;                  
   EXECUTE sp_executesql @query;                  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;                  
  END                     
  CLOSE ScopeCursor;                  
  DEALLOCATE ScopeCursor;                  
 END                  
ELSE                  
BEGIN                  
                 
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)                  
 IF (UPPER(@DBNAME) = 'TW')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@DBNAME) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@DBNAME) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@DBNAME) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@DBNAME) = 'UK')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End                  
    Else if(UPPER(@DBNAME) = 'DE')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End                  
    Else if(UPPER(@DBNAME) = 'TWCN')                  
    begin                  
       SET @DB ='TW'                  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
                      
                    
  SET @sqltxt ='INSERT INTO #temp (Dbname,  ISlp, Wgt,ActvyMonth)                  
      SELECT '''+ @DBNAME +''' as Country,  usr_nm, '
      IF(UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' ) 
      BEGIN
      set @sqltxt = @sqltxt + ' a.mbk_wgt*(2.20462), '
      END
      ELSE
      BEGIN
      set @sqltxt = @sqltxt + ' a.mbk_wgt, '
      END
      
      set @sqltxt = @sqltxt + ' Month(CAST(a.mbk_actvy_dt AS datetime))           
      FROM ' + @DBNAME + '_ortmbk_rec a                  
      INNER JOIN ' + @DBNAME + '_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id      
      join  ' + @DBNAME + '_scrslp_rec on slp_cmpy_id = a.mbk_cmpy_id and slp_slp = a.mbk_is_slp          
  join ' + @DBNAME + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                 
      WHERE a.mbk_ord_pfx=''SO''                   
        AND a.mbk_ord_itm<>999                   
         and a.mbk_trs_md = ''A''                    
         AND CAST(a.mbk_actvy_dt AS datetime) Between  ''' + @FD + '''  And ''' + @TD + '''         
            AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
          '                 
           --   and mbk_is_slp = ''' + @ISSLP +'''              
 print(@sqltxt)                  
 set @execSQLtxt = @sqltxt;                   
 EXEC (@execSQLtxt);                  
END                  
       
 select Dbname, ISlp as Name, SUM(Wgt) as Lines, ROW_NUMBER() over( order by Count(Wgt) desc) as Ranks   from #temp where  ActvyMonth = Month(GETDATE())    
 group by Dbname, ISlp;    
        
         
 DROP TABLE  #temp;                  
END                  
                  
                  
                  
-- EXEC [sp_itech_IS_Performance_WeightBookedLeaderboard] 'ALL', 'LAX'                
/*          
        
*/ 
GO
