USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_mchqds]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,OCT 30, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[US_mchqds]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_mchqds_rec', 'U') IS NOT NULL
		drop table dbo.US_mchqds_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_mchqds_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[mchqds_rec] ; 
  
END
-- select * from US_mchqds_rec 
GO
