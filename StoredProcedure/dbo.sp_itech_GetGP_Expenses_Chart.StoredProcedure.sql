USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetGP_Expenses_Chart]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <26 Feb 2013>
-- Description:	<Getting top 40 customers for SSRS reports>
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- Last updated by : Mukesh
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetGP_Expenses_Chart] @DBNAME varchar(50)--,@ChartName varchar(30)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD_cu varchar(10)
declare @TD_cu varchar(10)
declare @FD_pv varchar(10)
declare @TD_pv varchar(10)

set @DB= @DBNAME 

set @FD_pv = CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0), 120)  --FirstDateOfPreviousYear
set @TD_pv = CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)), 120)  --LastdateOFPreviousYear
set @FD_cu = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear
set @TD_cu = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of Previous Month


CREATE TABLE #tmp (  Expenses  	 DECIMAL(20, 2)
   					, GProfitPercentage               DECIMAL(20, 2)
   					, GPInDollar			    DECIMAL(20, 2)
   					, InvDt	  VARCHAR(15)
   					,Years           Varchar(5)
   					,TotalValue  DECIMAL(20, 2)
   	               );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @prefix VARCHAR(5);
DECLARE @Name VARCHAR(15);

DECLARE @CurrenyRate varchar(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  				SET @DB=  @prefix   --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  			   DECLARE @query NVARCHAR(4000);
  			   
  			   IF (UPPER(@Prefix) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@Prefix) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if (UPPER(@Prefix) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@Prefix) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
				End
				Else if (UPPER(@Prefix) = 'US')
				begin
					SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@Prefix) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@Prefix) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
  			   
		      SET @query = 'INSERT INTO #tmp (Expenses, GProfitPercentage,TotalValue,GPInDollar,InvDt,Years)'
		        if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')		
					 BEGIN


					 SET @query= @query +   'select 
		                    SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
							AVG(stn_npft_avg_pct) as GProfitPercentage,


							SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
							SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
					 END
					 Else
					 BEGIN
					 SET @query= @query +   'select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
							AVG(stn_npft_avg_pct) as GProfitPercentage,


							SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
							SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
					 End	 
					 SET @query= @query +   ',convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years
							
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_cu +''' and stn_inv_dt <= '''+ @TD_cu +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_cry,crx_xexrt,stn_inv_Dt,YEAR(stn_inv_Dt)
							union '
							if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')		
								 BEGIN


								 SET @query= @query +   'select 
										SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
										AVG(stn_npft_avg_pct) as GProfitPercentage,


										SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
										SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
								 END
								 Else
								 BEGIN

								 SET @query= @query +   'select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
										AVG(stn_npft_avg_pct) as GProfitPercentage,


										SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
										SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
								 End	 
								 SET @query= @query +   ',convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_pv +''' and stn_inv_dt <= '''+ @TD_pv +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_cry,crx_xexrt,stn_inv_Dt,YEAR(stn_inv_Dt)
							order by InvDt'
							
					print(@query)		
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN --select TOP 10 * from US_sahstn_rec order by STN_INV_DT desc;
			  --Set @Name=(select Name from tbl_itech_DatabaseName where DatabaseName=''+ @DBNAME + '')
			
			 IF (UPPER(@DBNAME) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@DBNAME) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if (UPPER(@DBNAME) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@DBNAME) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','RMB'))
				End
				Else if (UPPER(@DBNAME) = 'US')
				begin
					SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@DBNAME) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@DBNAME) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
			
			  SET @sqltxt = 'INSERT INTO #tmp (Expenses, GProfitPercentage,TotalValue,GPInDollar,InvDt,Years)'
		        if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')		
					 BEGIN
					 SET @sqltxt= @sqltxt +   '

		                    select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
							AVG(stn_npft_avg_pct) as GProfitPercentage,


							SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
							SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
					 END
					 Else
					 BEGIN
					 SET @sqltxt= @sqltxt +   'select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
							AVG(stn_npft_avg_pct) as GProfitPercentage,


							SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
							SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
					 End	 
					 SET @sqltxt= @sqltxt +   ',convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years
							
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_cu +''' and stn_inv_dt <= '''+ @TD_cu +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_cry,crx_xexrt,stn_inv_Dt,YEAR(stn_inv_Dt)
							union '
							if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')		
								 BEGIN
								 SET @sqltxt= @sqltxt +   '

										select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
										AVG(stn_npft_avg_pct) as GProfitPercentage,


										SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
										SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
								 END
								 Else
								 BEGIN
								 SET @sqltxt= @sqltxt +   'select SUM(stn_tot_mtl_val* '+ @CurrenyRate +') as Expenses , 
										AVG(stn_npft_avg_pct) as GProfitPercentage,


										SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,
										SUM(stn_npft_avg_val * '+ @CurrenyRate +') as GPInDollar '
								 End	 
								 SET @sqltxt= @sqltxt +   ',convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_pv +''' and stn_inv_dt <= '''+ @TD_pv +''' and stn_frm <> ''XXXX'' and cuc_desc30 <> ''Interco''
							group by stn_cry,crx_xexrt,stn_inv_Dt,YEAR(stn_inv_Dt)
							order by InvDt'
					
		print(@sqltxt)
	set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
     END
         
 
   declare @MonthCount integer
   
   set @MonthCount= MONTH(GETDATE()) + 12;
   
   SELECT SUM(GPInDollar)/nullif(SUM(TotalValue), 0) * 100 as GProfitPercentage ,sum(Expenses) as Expenses,sum(GPInDollar) as GPInDollar ,Month(InvDt) as InvDt,Years,Cast(@MonthCount as varchar) + ' Month Actual' as 'Actual' FROM #tmp 
   Group by InvDt,Month(InvDt),Years 
   order by InvDt ;
   
   
END

-- exec sp_itech_GetGP_Expenses_Chart_new 'ALL'





GO
