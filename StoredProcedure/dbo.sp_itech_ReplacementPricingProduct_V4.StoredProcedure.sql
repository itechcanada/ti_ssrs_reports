USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ReplacementPricingProduct_V4]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================                                                          
-- Author:  <Mukesh >                                                          
-- Create date: <31 Mar 2017>                                                          
-- Description: <Get inventory results with size and finish>    
-- =============================================                                                          
CREATE PROCEDURE [dbo].[sp_itech_ReplacementPricingProduct_V4] @DBNAME varchar(50)  -- ,@FromDate datetime, @ToDate datetime                                                             
                                                          
AS                                                          
BEGIN                                                          
                                                           
 SET NOCOUNT ON;                                                
                         
declare @sqltxt varchar(8000)                                                          
declare @execSQLtxt varchar(8000)                                                          
declare @DB varchar(100)                                                          
declare @FD1 varchar(10)                                                  
declare @TD1 varchar(10)                                                            
declare @FD varchar(10)                                                          
 declare @6FD varchar(10)                                                          
declare @TD varchar(10)                                                          
declare @ExcessFD varchar(10)                                              
declare @ExcessTD varchar(10)                                                          
DECLARE @ExchangeRate varchar(15)                                                     
DECLARE @CurrenyRate varchar(15)                                                       
                                                          
set @DB=  @DBNAME                                                   
 set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 1, 0) , 120) -- First day of previous month                                             
 set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120) -- Last day of previous month                                                          
 set @FD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FD),120)    -- current month from date                                            
set @TD1 = @FD                                             
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)     --First day of previous 6 month                                                          
                                                        
CREATE TABLE #tmp (   [Database]   VARCHAR(10)                                                
  , SizeDesc     Varchar(65)                                                      
        , Form     Varchar(65)                                                          
        , Grade     Varchar(65)                                                          
        , Size     char(15)                                                         
        , Finish    Varchar(65)                                             
        , ReplCost DECIMAL(20,2)                                               
        , LstReplCostUpdateDT Varchar(10)                                            
        , OhdStock DECIMAL(20, 0)                                              
        , Months6InvoicedWeight   DECIMAL(20, 0)                                          
        , Months6InvoicedSales   DECIMAL(20, 0)                                              
        , AvgDaysInventory       DECIMAL(20, 2)                                              
        , ExcessInventory Decimal (20,0)                                                         
     , OpenPOWgt DECIMAL(20, 0)                                                          
  , AvailableWeight Decimal (20,0)                                    
         , Months6InvoicedNP   DECIMAL(20, 0)                                  
       , Months6TotalInvoiced   numeric                                 
         , ReplCostUpdateDT Varchar(10)                       
         , AvgCost DEcimal(20,2)                    
         ,NextIncomingDate varchar(10)              
         ,TheoWgtFct decimal(20,4)          
         ,MetalSTD varchar(50)        
                 );                                                         
                                      
DECLARE @DatabaseName VARCHAR(35);                                                          
DECLARE @Prefix VARCHAR(35);                            
DECLARE @Name VARCHAR(15);                                                    
                                               
                                            
                                                        
                                                          
IF @DBNAME = 'ALL'                                                          
 BEGIN                                                          
     DECLARE ScopeCursor CURSOR FOR                                              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_V2                                               
   OPEN ScopeCursor;                                              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                    
     WHILE @@FETCH_STATUS = 0                                                          
       BEGIN                                                          
        DECLARE @query NVARCHAR(max);                                                             
      SET @DB= @Prefix                                                   
                                              
                                                         
      IF (UPPER(@DB) = 'TW')                                                
   begin                                                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))                                                            
   End                                                
   Else if (UPPER(@DB) = 'NO')                                                            
   begin                                                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))                                                            
   End                                                  
   Else if (UPPER(@DB) = 'CA')                                                            
   begin                                                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))                                                    
   End                                                            
   Else if (UPPER(@DB) = 'CN')                                                            
   begin                                                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))                                                            
 End                                                            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                                            
   begin                                                      
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))                                                            
   End                                                            
   Else if(UPPER(@DB) = 'UK')                                                            
   begin                      
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))                                          
   End 
   Else if(UPPER(@DB) = 'DE')                                        
   begin                                        
    SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))                                        
   End 
                                                 
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                                  
   Begin                                                   
                                                            
       SET @query ='                                            
     INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight ,Months6InvoicedSales,                                 
     AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight,Months6InvoicedNP,Months6TotalInvoiced,ReplCostUpdateDT,AvgCost)                                                          
   select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,                                             
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)                                               
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc                                                       
      ) as MonthlyAvgReplCost,                                              
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,                                                         
       (SElect sum(oh.prd_ohd_wgt* 2.20462) from ' + @DB + '_intprd_rec oh where oh.prd_frm = prm_frm and oh.prd_grd = prm_grd and oh.prd_size = prm_size                                         
   and oh.prd_fnsh = prm_fnsh and oh.prd_invt_sts = ''S'') onHandWgt  ,                                                         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                                                          
       (Select SUM(SAT_tot_val) * '+ @CurrenyRate +' from ' + @DB + '_sahsat_rec join '+ @DB +'_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )                                     
       -- and sat_sld_cus_id NOT IN (''10993'',''11980'',''12896'',''2046'',''3330'',''5833'')                                    
        ) as Months6InvoicedSales,                                         
         (  select case when SUM(datediff(D, pcr_agng_dtts, getdate() )) <> 0 then SUM(datediff(D, pcr_agng_dtts, getdate() ) *  prd_ohd_wgt)/SUM(datediff(D, pcr_agng_dtts, getdate() ))                        
        else 0 end  from ' + @DB + '_intpcr_rec                                            
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size                                             
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null                                
)  as   AvgDaysInventory,                                                      
                (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then                                             
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess                                            
  FROM                                               
  (                                             
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,                                            
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where                                             
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size                                       
and sixMonthWgt.sat_fnsh = PRm_fnsh and                                             
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,                                            
                                        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM                                           
and prd_grd = Prm_GRD                                            
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''                                          
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg                                              
FROM '+ @DB +'_sahstn_rec currentPrdData                                              
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD                                            
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh                                            
                                 ) as oquery ),                                                                 
                                                  
       (select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec                                                 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm                                                 
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm                                                 
       and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                                                               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh) as OpenPOWgt,                                        
     (select   (Sum(ohh.prd_ohd_wgt - (ohh.prd_qte_res_wgt + ohh.prd_ord_res_wgt + ohh.prd_prod_res_wgt + ohh.prd_shp_res_wgt) ))* 2.20462 from                                         
       '+ @DB + '_intprd_rec ohh where ohh.prd_frm = prm_frm and ohh.prd_grd = prm_grd and ohh.prd_size = prm_size and ohh.prd_fnsh = prm_fnsh                                         
       and ohh.prd_invt_sts = ''S'') as availableWgt ,                                   
       (Select CASE WHEN (Sum(sat_tot_val)) = 0 THEN 0 ELSE SUM(sat_npft_avg_val) * '+ @CurrenyRate +' END as ''NPAmt'' from '+ @DB + '_sahsat_rec                                   
 join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )                                     
       
) as Months6InvoicedNP,                                    
(Select  count(distinct sat_upd_ref) from '+ @DB + '_sahsat_rec                                   
join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )          
                                          
) as Months6TotalInvoiced ,                              
(Select Max(pal_upd_dtts) from ' + @DB + '_sctpal_rec where pal_tbl_nm like ''%perppb%'' and LTRIM(RTRIM(SUBSTRING(pal_rec_txt,7,6))) = prm_frm and LTRIM(RTRIM(SUBSTRING(pal_rec_txt,13,8))) = prm_grd and                               
LTRIM(RTRIM(SUBSTRING(pal_rec_txt,21,15))) = prm_size and SUBSTRING(pal_rec_txt,36,8) = prm_fnsh ) ,                          
(select top 1 (case when ppb.ppb_repl_cst_um = ''KGS'' then ((ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ')/2.20462) else (ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ') end) as acp_tot_mat_cst      
from ' + @DB + '_intacp_rec acp inner join ' + @DB + '_perppb_rec ppb on acp.acp_frm = ppb.ppb_frm and acp.acp_grd = ppb.ppb_grd and acp.acp_size = ppb.ppb_size and acp.acp_fnsh = ppb.ppb_fnsh     
where acp.acp_frm = prm_frm and acp.acp_grd = prm_grd and acp.acp_fnsh = prm_fnsh and acp.acp_size = prm_size and acp.acp_cus_id is null)                                                    
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                                                                 
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool                                        
      --  Where  prd_invt_sts = ''S''                                                         
       group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc,PRm_fnsh'                                                                                   
                                                 
   End                                            
   Else                                           
   Begin                                            
  set @query='                                            
     INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight ,Months6InvoicedSales,                                
      AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight,Months6InvoicedNP,Months6TotalInvoiced,ReplCostUpdateDT,AvgCost)                                  
   select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,                                             
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)                                               
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc                                                       
      ) as MonthlyAvgReplCost,                             
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,                                                      
       (SElect sum(oh.prd_ohd_wgt) from ' + @DB + '_intprd_rec oh where oh.prd_frm = prm_frm and oh.prd_grd = prm_grd and oh.prd_size = prm_size                                   
   and oh.prd_fnsh = prm_fnsh and oh.prd_invt_sts = ''S'') onHandWgt  ,                                                           
       (Select SUM(SAT_BLG_WGT ) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                                          
       (Select SUM(SAT_tot_val) * '+ @CurrenyRate +' from ' + @DB + '_sahsat_rec join '+ @DB +'_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )                                     
       -- and sat_sld_cus_id NOT IN (''10993'',''11980'',''12896'',''2046'',''3330'',''5833'')                                     
       ) as Months6InvoicedSales,                                                         
         (  select case when SUM(datediff(D, pcr_agng_dtts, getdate() )) <> 0 then SUM(datediff(D, pcr_agng_dtts, getdate() ) *  prd_ohd_wgt)/SUM(datediff(D, pcr_agng_dtts, getdate() ))                        
        else 0 end  from ' + @DB + '_intpcr_rec                                         
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size                                             
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null                                
)  as   AvgDaysInventory,                    
                                              
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then                                             
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess                                            
  FROM                                               
  (                                             
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,                                            
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where                                             
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size                                            
and sixMonthWgt.sat_fnsh = PRm_fnsh and                                          
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,                                            
                                            
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM                                             
and prd_grd = Prm_GRD                                            
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''                                            
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg                                              
FROM '+ @DB +'_sahstn_rec currentPrdData                                              
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD                                            
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh                                            
                                                 
) as oquery ),               
                                                  
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec                                                 
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm                                                 
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm                                                 
       and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM                                               
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh) as OpenPOWgt,                                        
        (select   (Sum(ohh.prd_ohd_wgt - (ohh.prd_qte_res_wgt + ohh.prd_ord_res_wgt + ohh.prd_prod_res_wgt + ohh.prd_shp_res_wgt) )) from                                         
       '+ @DB + '_intprd_rec ohh where ohh.prd_frm = prm_frm and ohh.prd_grd = prm_grd and ohh.prd_size = prm_size and ohh.prd_fnsh = prm_fnsh                                         
       and ohh.prd_invt_sts = ''S'') as availableWgt ,                                   
       (Select CASE WHEN (Sum(sat_tot_val)) = 0 THEN 0 ELSE SUM(sat_npft_avg_val) * '+ @CurrenyRate +' END as ''NPAmt'' from '+ @DB + '_sahsat_rec                                   
 join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
  left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                                                       
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )                                     
                                          
) as Months6InvoicedNP,                                    
(Select  count(distinct sat_upd_ref) from '+ @DB + '_sahsat_rec                                   
join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id                                                  
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh                              
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )                                     
                                          
) as Months6TotalInvoiced ,                              
(Select Max(pal_upd_dtts) from ' + @DB + '_sctpal_rec where pal_tbl_nm like ''%perppb%'' and LTRIM(RTRIM(SUBSTRING(pal_rec_txt,7,6))) = prm_frm and LTRIM(RTRIM(SUBSTRING(pal_rec_txt,13,8))) = prm_grd and                               
LTRIM(RTRIM(SUBSTRING(pal_rec_txt,21,15))) = prm_size and SUBSTRING(pal_rec_txt,36,8) = prm_fnsh ),                          
(select top 1 (case when ppb.ppb_repl_cst_um = ''KGS'' then ((ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ')/2.20462) else (ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ') end) as acp_tot_mat_cst      
from ' + @DB + '_intacp_rec acp inner join ' + @DB + '_perppb_rec ppb on acp.acp_frm = ppb.ppb_frm and acp.acp_grd = ppb.ppb_grd and acp.acp_size = ppb.ppb_size and acp.acp_fnsh = ppb.ppb_fnsh     
where acp.acp_frm = prm_frm and acp.acp_grd = prm_grd and acp.acp_fnsh = prm_fnsh and acp.acp_size = prm_size and acp.acp_cus_id is null)                            
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                                                                 
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool                                             
     --   Where  prd_invt_sts = ''S''                                                         
       group by PRm_FRM, Prm_GRD,PRm_size,prm_size_desc, PRm_fnsh'                                                          
   End                                              
      print @query;                                                          
        EXECUTE sp_executesql @query;                                                          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                          
       END                                                           
    CLOSE ScopeCursor;                                                          
    DEALLOCATE ScopeCursor;                                            
  END                                              
  ELSE                                                          
     BEGIN                                                      
                     
   IF (UPPER(@DB) = 'TW')                                                            
    begin                                                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('TW','USD','TWD'))                                                            
    End                              
    Else if (UPPER(@DB) = 'NO')                                                      
    begin                                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('NO','USD','NOK'))                                                            
    End                                                            
    Else if (UPPER(@DB) = 'CA')                                                            
    begin                                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CA','USD','CAD'))                                                            
    End                                                            
    Else if (UPPER(@DB) = 'CN')                                                            
    begin                                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('CN','USD','RMB'))                                                            
    End                                                            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                                                            
    begin                                                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert_V1('US','USD','USD'),1))                                                            
    End                                                            
    Else if(UPPER(@DB) = 'UK')                                                            
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert_V1('UK','USD','GBP'))                                                            
    End
	Else if(UPPER(@DB) = 'DE')                                        
   begin                                        
    SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert_V1('DE','USD','EUR'))                                        
   End 
                                                         
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                                                           
   Begin                                                   
                                                            
       SET @sqltxt ='INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight ,      
Months6InvoicedSales, AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight,Months6InvoicedNP,Months6TotalInvoiced,ReplCostUpdateDT,AvgCost,      
NextIncomingDate,TheoWgtFct,MetalSTD)      
select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,      
(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)      
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh   order by ppb_rct_expy_dt desc      
      ) as MonthlyAvgReplCost,      
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,      
       (SElect sum(oh.prd_ohd_wgt * 2.20462) from ' + @DB + '_intprd_rec oh where oh.prd_frm = prm_frm and oh.prd_grd = prm_grd and oh.prd_size = prm_size      
   and oh.prd_fnsh = prm_fnsh and oh.prd_invt_sts = ''S'') onHandWgt,      
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh      
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,      
       (Select SUM(SAT_tot_val) * '+ @CurrenyRate +' from ' + @DB + '_sahsat_rec join '+ @DB +'_arrcus_rec on cus_cus_id = sat_sld_cus_id      
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh      
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )      
) as Months6InvoicedSales,      
         (  select case when SUM(datediff(D, pcr_agng_dtts, getdate() )) <> 0 then SUM(datediff(D, pcr_agng_dtts, getdate() ) *  prd_ohd_wgt)/SUM(datediff(D, pcr_agng_dtts, getdate() ))      
        else 0 end  from ' + @DB + '_intpcr_rec      
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size      
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null      
)  as   AvgDaysInventory,      
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then      
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess      
  FROM      
  (         
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,      
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where      
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size      
and sixMonthWgt.sat_fnsh = PRm_fnsh and      
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,      
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM      
and prd_grd = Prm_GRD      
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''      
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg      
FROM '+ @DB +'_sahstn_rec currentPrdData      
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD            
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh            
) as oquery ),            
(select sum(poi_bal_wgt)* 2.20462 from ' + @DB + '_tctipd_rec            
join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm            
and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm            
and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM            
and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh) as OpenPOWgt,            
(select   (Sum(ohh.prd_ohd_wgt - (ohh.prd_qte_res_wgt + ohh.prd_ord_res_wgt + ohh.prd_prod_res_wgt + ohh.prd_shp_res_wgt) ))* 2.20462 from            
'+ @DB + '_intprd_rec ohh where ohh.prd_frm = prm_frm and ohh.prd_grd = prm_grd and ohh.prd_size = prm_size and ohh.prd_fnsh = prm_fnsh            
and ohh.prd_invt_sts = ''S'') as availableWgt ,            
(Select CASE WHEN (Sum(sat_tot_val)) = 0 THEN 0 ELSE SUM(sat_npft_avg_val) * '+ @CurrenyRate +' END as ''NPAmt'' from '+ @DB + '_sahsat_rec            
 join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id            
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD            
+''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )            
) as Months6InvoicedNP,            
(Select  count(distinct sat_upd_ref) from '+ @DB + '_sahsat_rec            
join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id             
 left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh            
and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )            
) as Months6TotalInvoiced ,            
(Select Max(pal_upd_dtts) from tbl_itech_' + @DB + '_sctpal_rec pal where pal_tbl_nm like ''%perppb%'' and pal.form = prm_frm and pal.grad = prm_grd and            
pal.size = prm_size and pal.finish = prm_fnsh ),            
(select top 1 (case when ppb.ppb_repl_cst_um = ''KGS'' then ((ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ')/2.20462) else (ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ') end) as acp_tot_mat_cst      
from ' + @DB + '_intacp_rec acp inner join ' + @DB + '_perppb_rec ppb on acp.acp_frm = ppb.ppb_frm and acp.acp_grd = ppb.ppb_grd and acp.acp_size = ppb.ppb_size and acp.acp_fnsh = ppb.ppb_fnsh     
where acp.acp_frm = prm_frm and acp.acp_grd = prm_grd and acp.acp_fnsh = prm_fnsh and acp.acp_size = prm_size and acp.acp_cus_id is null)            
,(select Min(pod_arr_to_dt) from ' + @DB + '_tctipd_rec            
join ' + @DB + '_potpod_rec on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''            
join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm            
Where ipd_FRM = PRm_FRM  and ipd_GRD = Prm_GRD and ipd_size = PRm_size and ipd_fnsh = PRm_fnsh  and poi_bal_wgt > 0            
 ) as NextIncomingDate            
,prm_e_twf          
,(select case when RTRIM(LTRIM(MAX(mss_addnl_id))) = '''' then MAX(RTRIM(LTRIM(mss_sdo)) + ''-'' + RTRIM(LTRIM(mss_std_id)))        
else MAX(RTRIM(LTRIM(mss_sdo)) + ''-'' + RTRIM(LTRIM(mss_std_id)) + ''-'' + RTRIM(LTRIM(mss_addnl_id))) end from        
' + @DB + '_mcrmss_rec join ' + @DB + '_mchqms_rec on mss_mss_ctl_no = qms_mss_ctl_no        
join ' + @DB + '_intpcr_rec on qms_cmpy_id = pcr_cmpy_id and qms_qds_ctl_no = pcr_qds_ctl_no        
 join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size        
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null )          
from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh            
left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool             
group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc,PRm_fnsh,prm_e_twf '                  
   End                                                          
   Else                                                          
   Begin                                                  
                  
        SET @sqltxt ='INSERT INTO #tmp ([Database],SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight ,        
Months6InvoicedSales, AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight,Months6InvoicedNP,Months6TotalInvoiced,ReplCostUpdateDT,AvgCost,        
NextIncomingDate,TheoWgtFct,MetalSTD)        
select ''' +  @DB + '''  as [Database],RTRIM(LTRIM(prm_size_desc)),PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,        
(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)        
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc        
      ) as MonthlyAvgReplCost,        
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,        
      (SElect sum(oh.prd_ohd_wgt) from ' + @DB + '_intprd_rec oh where oh.prd_frm = prm_frm and oh.prd_grd = prm_grd and oh.prd_size = prm_size        
   and oh.prd_fnsh = prm_fnsh and oh.prd_invt_sts = ''S'') onHandWgt,        
       (Select SUM(SAT_BLG_WGT ) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh        
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,        
       (Select SUM(SAT_tot_val) * '+ @CurrenyRate +' from ' + @DB + '_sahsat_rec join '+ @DB +'_arrcus_rec on cus_cus_id = sat_sld_cus_id        
   left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh        
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )        
) as Months6InvoicedSales,        
        (  select case when SUM(datediff(D, pcr_agng_dtts, getdate() )) <> 0 then SUM(datediff(D, pcr_agng_dtts, getdate() ) *  prd_ohd_wgt)/SUM(datediff(D, pcr_agng_dtts, getdate() ))        
        else 0 end  from ' + @DB + '_intpcr_rec        
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size        
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null        
)  as   AvgDaysInventory,        
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then        
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess        
  FROM        
  (        
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where        
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size        
and sixMonthWgt.sat_fnsh = PRm_fnsh and        
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM        
and prd_grd = Prm_GRD        
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''            
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg        
FROM '+ @DB +'_sahstn_rec currentPrdData        
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD        
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh        
) as oquery ),        
       (select sum(poi_bal_wgt) from ' + @DB + '_tctipd_rec        
       join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm        
       and  (select  count(*) from ' + @DB + '_potpod_rec where pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm        
       and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C'') > 0 where ipd_FRM=PRm_FRM            
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh) as OpenPOWgt,        
       (select   (Sum(ohh.prd_ohd_wgt - (ohh.prd_qte_res_wgt + ohh.prd_ord_res_wgt + ohh.prd_prod_res_wgt + ohh.prd_shp_res_wgt) )) from        
       '+ @DB + '_intprd_rec ohh where ohh.prd_frm = prm_frm and ohh.prd_grd = prm_grd and ohh.prd_size = prm_size and ohh.prd_fnsh = prm_fnsh        
       and ohh.prd_invt_sts = ''S'') as availableWgt,        
       (Select CASE WHEN (Sum(sat_tot_val)) = 0 THEN 0 ELSE SUM(sat_npft_avg_val) * '+ @CurrenyRate +' END as ''NPAmt'' from '+ @DB + '_sahsat_rec        
 join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id        
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh        
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )        
) as Months6InvoicedNP,        
(Select  count(distinct sat_upd_ref) from '+ @DB + '_sahsat_rec        
join '+ @DB + '_arrcus_rec on cus_cus_id = sat_sld_cus_id        
   left join '+ @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh        
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' and (cuc_desc30<>''Interco'' or cuc_desc30 is null )        
) as Months6TotalInvoiced ,        
(Select Max(pal_upd_dtts) from tbl_itech_' + @DB + '_sctpal_rec pal where pal_tbl_nm like ''%perppb%'' and  pal.form = prm_frm and pal.grad = prm_grd and        
pal.size = prm_size and pal.finish = prm_fnsh ),        
(select top 1 (case when ppb.ppb_repl_cst_um = ''KGS'' then ((ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ')/2.20462) else (ISNULL(acp.acp_tot_mat_cst,0) * ' + @CurrenyRate + ') end) as acp_tot_mat_cst      
from ' + @DB + '_intacp_rec acp inner join ' + @DB + '_perppb_rec ppb on acp.acp_frm = ppb.ppb_frm and acp.acp_grd = ppb.ppb_grd and acp.acp_size = ppb.ppb_size and acp.acp_fnsh = ppb.ppb_fnsh     
where acp.acp_frm = prm_frm and acp.acp_grd = prm_grd and acp.acp_fnsh = prm_fnsh and acp.acp_size = prm_size and acp.acp_cus_id is null)        
,(select Min(pod_arr_to_dt) from ' + @DB + '_tctipd_rec        
join ' + @DB + '_potpod_rec on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''        
join ' + @DB + '_potpoi_rec on poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm        
Where ipd_FRM = PRm_FRM  and ipd_GRD = Prm_GRD and ipd_size = PRm_size and ipd_fnsh = PRm_fnsh and poi_bal_wgt > 0        
 ) as NextIncomingDate         
 ,prm_e_twf,        
 (select case when RTRIM(LTRIM(MAX(mss_addnl_id))) = '''' then MAX(RTRIM(LTRIM(mss_sdo)) + ''-'' + RTRIM(LTRIM(mss_std_id)))        
else MAX(RTRIM(LTRIM(mss_sdo)) + ''-'' + RTRIM(LTRIM(mss_std_id)) + ''-'' + RTRIM(LTRIM(mss_addnl_id))) end from        
' + @DB + '_mcrmss_rec join ' + @DB + '_mchqms_rec on mss_mss_ctl_no = qms_mss_ctl_no        
join ' + @DB + '_intpcr_rec on qms_cmpy_id = pcr_cmpy_id and qms_qds_ctl_no = pcr_qds_ctl_no        
 join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size        
and inp.prd_fnsh = prm_fnsh and prd_avg_cst_pool is not null )        
from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh        
left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool        
group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc, PRm_fnsh,prm_e_twf '          
   End                                                          
                                                             
                                                              
                                                                    
     print( @sqltxt)                                                           
    set @execSQLtxt = @sqltxt;                                                           
   EXEC (@execSQLtxt);                                                          
   END                                                          
                                              
select distinct [Database], Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(Size)) + '/' + Rtrim(Ltrim(Finish)) as 'ProductShort',                                 
 Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',                                             
Form, Grade, Size,   Finish,                                                        
 ReplCost,                                              
  LstReplCostUpdateDT , ReplCostUpdateDT,                                            
   OhdStock,                                        
 Months6InvoicedWeight,Months6InvoicedSales,   AvgDaysInventory , ExcessInventory  ,                                                      
 OpenPOWgt ,AvailableWeight,Months6InvoicedNP ,Months6TotalInvoiced, avgCost,                                       
 (select SUM(ISNULL(prd_ohd_wgt,0)) from UK_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'BHM' and prd_invt_sts = 'S') as BHMQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'DET' and prd_invt_sts = 'S') as DETQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'HIB' and prd_invt_sts = 'S') as HIBQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'JAC' and prd_invt_sts = 'S') as JACQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'LAX' and prd_invt_sts = 'S') as LAXQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from CA_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'MTL' and prd_invt_sts = 'S') as MTLQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'ROC' and prd_invt_sts = 'S') as ROSQTY, 
 (cast((select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'SEA' and prd_invt_sts = 'S') as decimal(20,2))
 + cast((select SUM(ISNULL(prd_ohd_wgt,0)) from DE_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'DEU' and prd_invt_sts = 'S')as decimal(20,2)))
 as SEAQTY, 
 (select SUM(ISNULL(prd_ohd_wgt,0)) from CN_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'SHA' and prd_invt_sts = 'S') as SHAQTY, 
 case when [Database] in ('US','CA') then 0 else (select SUM(ISNULL(prd_ohd_wgt,0)) from TW_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'TAI' and prd_invt_sts = 'S') end as TAIQTY, 
  
  (select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_brh = 'WDL' and prd_invt_sts = 'S') as WDLQTY , 
(select SUM(ISNULL(prd_ohd_wgt,0)) from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size = Size and prd_fnsh = Finish and prd_whs= 'ROC' and prd_loc like 'BON%' and prd_invt_sts = 'S') as BONQTY 
,NextIncomingDate,TheoWgtFct,MetalSTD  
 into #tmp1  
 from #tmp ;   -- where Form = 'CCRD' and Grade = '1537' and Size = '.75' ;                                                       
                                  
insert into tbl_itech_ReplacementPricingV1                                             
select (ISNULL(OhdStock,0) - (ISNULL(BHMQTY,0) + ISNULL(DETQTY,0) + ISNULL(HIBQTY,0) + ISNULL(JACQTY,0) + ISNULL(LAXQTY,0) + ISNULL(MTLQTY,0) + ISNULL(ROSQTY,0)            
 + ISNULL(SEAQTY,0) + ISNULL(SHAQTY,0) + ISNULL(TAIQTY,0) + ISNULL(WDLQTY,0))) as otherQTY,* -- into tbl_itech_ReplacementPricingV1              
from #tmp1 ; --where Size = '.5625';                    
--select * from #tmp;                       
                        
                                                                 
   drop table #tmp  ;                                               
   drop table #tmp1  ;                
END                                                          
-- exec [sp_itech_ReplacementPricingProduct_V3] 'US' ;  -- 19:52                   
-- exec [sp_itech_ReplacementPricingProduct_V3] 'CA' ; -- 00:29                          
-- exec [sp_itech_ReplacementPricingProduct_V3] 'UK' ; -- 00:21                             
-- exec [sp_itech_ReplacementPricingProduct_V3] 'CN' ;  -- 00:11                            
-- exec [sp_itech_ReplacementPricingProduct_V3] 'TW' ;  -- 00:31                              
 /*                                            
 Date: 20161223                                            
 Sub: Need an Adjustment Please                                            
                   
 date: 20161227                                          
 sub: New Report Request - Replacement Pricing Product Report                                          
                                          
date:20170104                                          
sub:Replacement Cost                                             
   (Sum(prd_ohd_wgt - prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt ))                                          
                                             
date:20170118                                          
Sub:Problem with replacement cost                                          
                                        
date:20170724              
Sub:New Request: Report Modification:  Replacement Pricing Product Report                                       
                                    
date:20170905                                    
sub:Interco reports                                        
                                  
Date:20171005                                  
sub:Question about Report:  Replacement Pricing and Stock Report                                  
                                  
date:20180301                                  
Sub;Report Modification Request                                  
                                
date:20180321                                
sub:Question and One Additional Modification Request                                
                              
date:20181008                              
sub:eCIM Call #332093 - Call Assigned                             
                          
                          
date:20181022                          
Mail sub:RE: Different Report Question Concerning the Replacement Pricing Product Report                           
description: I used top 1 acp.acp_tot_mat_cst due to US product acp_frm = 'TIRD' and acp_grd = '64' and acp_size = '3.543'                        
and acp_fnsh = ''  and acp_cus_id is null                        
                    
date:20190313                    
Mail sub: Report Modification Request                    
                  
date:20190807                  
Mail sub:Report Request UPDATE as of July 12, 2019                  
-- Home > STRATIXReports > Development > Replacement Pricing and Product Report Rev 2019-08     
    
Date: 20200703 Sumit    
Mail: RE: Home > STRATIXReports  > Sales Performance>  Replacement  Pricing and Product Report Rev 2019-08    
Multiply acp_tot_mat_cst with currency value to convert all value into dollars    
  
Date: 20200706 Sumit  
Mail: RE: Home > STRATIXReports  > Sales Performance>  Replacement  Pricing and Product Report Rev 2019-08    
Apply same formula for AvgCost(acp_tot_mat_cst) as applied for replacement cost ealiear 
20201125 Sumit    
Add germany database  , replace SEA warehouse with DEU (add sea and deu branch data in seaqty, use seaqty as deuqty in report design)   
Mail: RE: Report Modification Request  
 */ 
GO
