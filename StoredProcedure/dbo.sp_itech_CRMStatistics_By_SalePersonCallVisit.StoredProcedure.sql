USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CRMStatistics_By_SalePersonCallVisit]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <07 Jul 2015>          
-- Modified by: <Mukesh>         
        
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_CRMStatistics_By_SalePersonCallVisit] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch as varchar(65), @SalePerson as Varchar(65)         
          
AS          
BEGIN          
           
 SET NOCOUNT ON;          
declare @sqltxtAct varchar(6000)          
declare @execSQLtxtAct varchar(7000)          
declare @sqltxtTsk varchar(6000)          
declare @execSQLtxtTsk varchar(7000)          
declare @DB varchar(100)          
declare @FD varchar(10)          
declare @TD varchar(10)          
          
set @DB=  @DBNAME          
          
IF @Branch = 'ALL'          
 BEGIN          
  set @Branch = ''          
 END          
           
 IF @SalePerson = 'ALL'          
 BEGIN          
  set @SalePerson = ''          
 END          
           
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)          
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)          
          
CREATE TABLE #tmpAct ([Database]   VARCHAR(10)        
, Branch VARCHAR(3)             
     , LoginID   VARCHAR(50)          
        
     , MonthYear   VARCHAR(10)          
        , ActDormant  integer          
        , ActExisting  integer          
        , ActProspect  integer          
        , TskDormant  integer          
        , TskExisting  integer          
        , TskProspect  integer          
        , ActCallDormant  integer          
        , ActCallExisting  integer          
        , ActCallProspect  integer          
        , TskCallDormant  integer          
        , TskCallExisting  integer          
        , TskCallProspect  integer          
                 );           
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)             
     ,  Branch VARCHAR(3)             
     , LoginID   VARCHAR(50)     
      , SalesPersonInitial   VARCHAR(50)          
     , MonthYear   VARCHAR(10)          
        , ActDormant  integer          
        , ActExisting  integer          
        , ActProspect  integer          
        , TskDormant  integer          
        , TskExisting  integer          
        , TskProspect  integer         
        , ActCallDormant  integer          
        , ActCallExisting  integer          
        , ActCallProspect  integer          
        , TskCallDormant  integer          
        , TskCallExisting  integer          
        , TskCallProspect  integer          
                 );           
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(35);          
DECLARE @Name VARCHAR(15);          
          
IF @DBNAME = 'ALL'          
 BEGIN          
              
    DECLARE ScopeCursor CURSOR FOR            
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS             
   OPEN ScopeCursor;            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
        DECLARE @queryAct NVARCHAR(1500);             
        DECLARE @queryTsk NVARCHAR(Max);             
      SET @DB= @Prefix            
                  
        SET @queryTsk =          
              'INSERT INTO #tmpTsk ([Database],Branch, LoginID,SalesPersonInitial, MonthYear, TskCallDormant, TskCallExisting, TskCallProspect, TskDormant, TskExisting, TskProspect)        
      Select * from (         
      select ''' +  @Prefix + ''' as [Database],cus_admin_brh,cta_tsk_asgn_to,slp_slp , REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts         
       from ' + @DB + '_cctcta_rec join         
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join        
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id     
       join  ' + @DB + '_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to       
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))          
 And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')         
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')        
       group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-''), cta_tsk_asgn_to, atp_desc30, cus_admin_brh ,slp_slp        
       ) AS Query1          
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'          
        EXECUTE sp_executesql @queryTsk;          
        print(@queryTsk);          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN              
 -- task            
              
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database],Branch, LoginID,SalesPersonInitial, MonthYear, TskCallDormant, TskCallExisting, TskCallProspect, TskDormant, TskExisting, TskProspect)        
      Select * from (         
      select ''' +  @DB + ''' as [Database],cus_admin_brh,cta_tsk_asgn_to,slp_slp, REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts         
       from ' + @DB + '_cctcta_rec join         
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join        
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id    
       join  ' + @DB + '_scrslp_rec on slp_cmpy_id = cta_cmpy_id and slp_lgn_id = cta_tsk_asgn_to       
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))  
       And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')         
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')        
       group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-''), cta_tsk_asgn_to, atp_desc30, cus_admin_brh ,slp_slp       
       ) AS Query1          
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'               
              
    print(@sqltxtTsk)          
    set @execSQLtxtTsk = @sqltxtTsk;           
   EXEC (@execSQLtxtTsk);          
     END          
   -- SELECT * FROM #tmpAct Order by MonthYear desc, LoginID          
   -- SELECT * FROM #tmpTsk Order by MonthYear desc, LoginID          
   SELECT CASE WHEN act.[Database] IS NULL THEN tsk.[Database] ELSE act.[Database] END AS [Database],          
   CASE WHEN act.Branch Is NULL then tsk.Branch ELSE act.Branch END AS Branch,         
    CASE WHEN act.LoginID Is NULL then tsk.LoginID ELSE act.LoginID END AS LoginID,      
    SalesPersonInitial,         
    CASE WHEN act.MonthYear IS NULL then tsk.MonthYear ELSE act.MonthYear  END AS MonthYear,           
         
             
    --ISNULL(act.ActCallDormant,0) + ISNULL(act.ActCallExisting,0) + ISNULL(act.ActCallProspect,0) + ISNULL(act.ActDormant,0) + ISNULL(act.ActExisting,0) + ISNULL(act.ActProspect,0) AS TotalAct,          
    ISNULL(tsk.TskCallDormant,0) AS TskCallDormant,           
    ISNULL(tsk.TskCallExisting,0) AS TskCallExisting,          
    ISNULL(tsk.TskCallProspect,0) AS TskCallProspect,         
    ISNULL(tsk.TskDormant,0) AS TskDormant,           
    ISNULL(tsk.TskExisting,0) AS TskExisting,          
    ISNULL(tsk.TskProspect,0) AS TskProspect,           
    ISNULL(act.ActDormant,0)+ ISNULL(tsk.TskDormant,0) AS TotalVisitDormant,        
    ISNULL(act.ActExisting,0)+ ISNULL(tsk.TskExisting,0) AS TotalVisitExisting,        
    ISNULL(act.ActProspect,0)+ ISNULL(tsk.TskProspect,0) AS TotalVisitProspect        
            
    --ISNULL(tsk.TskCallDormant,0) + ISNULL(tsk.TskCallExisting,0) + ISNULL(tsk.TskCallProspect,0) + ISNULL(tsk.TskDormant,0) + ISNULL(tsk.TskExisting,0) + ISNULL(tsk.TskProspect,0)  AS TotalTsk         
    FROM #tmpAct as act          
    full outer  join #tmpTsk tsk on act.LoginID = tsk.LoginID and act.MonthYear = tsk.MonthYear and act.Branch = tsk.Branch         
    order by MonthYear DESC, LoginID          
              
    Drop table #tmpAct Drop table #tmpTsk          
END          
          
-- exec sp_itech_CRMStatistics_By_SalePersonCallVisit '12/01/2016', '12/31/2017' , 'ALL','ALL','jgilkens'          
-- exec sp_itech_CRMStatistics_By_SalePersonCallVisit '01/01/2016', '01/30/2018' , 'ALL','ALL', 'ALL'  
-- ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
 --      ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))           

/*
2020/07/10	Sumit
Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account
*/     
GO
