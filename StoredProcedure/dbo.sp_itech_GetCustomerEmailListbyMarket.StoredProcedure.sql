USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetCustomerEmailListbyMarket]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE  PROCEDURE [dbo].[sp_itech_GetCustomerEmailListbyMarket]  @DBNAME varchar(50),@Market varchar(50), @version char = '0'

AS
BEGIN
SET NOCOUNT ON;



declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)

CREATE TABLE #tmp (  Company    varchar(5) 
   					,Market  varchar(50)
   					,Customer     varchar(100)
   					,Email varchar(100)
   					);
   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  

set @DB= @DBNAME
  	   
 if @Market = 'ALL'
 BEGIN
	 set @Market = ''
 END
  	   
 IF @DBNAME = 'ALL'
	BEGIN
		IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				 set @DB= @prefix   
							SET @query = ' INSERT INTO #tmp(Company,Market,Customer,Email)
											select cus_cmpy_ID as Company, cuc_desc30 as Market, cus_cus_nm as Customer, cvt_email as Email  
											from '+ @DB +'_arrcus_rec  
											join '+ @DB +'_scrcvt_rec on cus_cmpy_id = cvt_cmpy_id and cus_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C'' 
											and (cvt_email Is not null and LTrim(cvt_email) <> '''')
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
											where  (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
											if @Market =''
											BEGIN
												 SET @query= @query + ' and cuc_desc30 <> ''Interco'''
											END
											SET @query = @query + 'group by cus_cmpy_ID, cuc_desc30, cus_cus_nm, cvt_email
											UNION
											select cus_cmpy_ID as Company, cuc_desc30 as Market, cus_cus_nm as Customer, cva_email as Email  
											from '+ @DB +'_arrcus_rec 
											 join '+ @DB +'_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_cus_ven_id = cus_cus_id and cva_cus_ven_typ = ''C''
											and (cva_email Is not null and LTrim(cva_email) <> '''')
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where  (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
											if @Market =''
											BEGIN
												 SET @query= @query + ' and cuc_desc30 <> ''Interco'''
											END
											SET @query = @query + 'group by cus_cmpy_ID, cuc_desc30, cus_cus_nm, cva_email
											order by 2,3'
										
  	  			     EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
							SET @sqltxt = ' INSERT INTO #tmp(Company,Market,Customer,Email)
											select cus_cmpy_ID as Company, cuc_desc30 as Market, cus_cus_nm as Customer, cvt_email as Email  
											from '+ @DB +'_arrcus_rec  
											join '+ @DB +'_scrcvt_rec on cus_cmpy_id = cvt_cmpy_id and cus_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C'' 
											and (cvt_email Is not null and LTrim(cvt_email) <> '''')
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
											where  (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
											if @Market =''
											BEGIN
												 SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
											END
											SET @sqltxt = @sqltxt + 'group by cus_cmpy_ID, cuc_desc30, cus_cus_nm, cvt_email
											UNION
											select cus_cmpy_ID as Company, cuc_desc30 as Market, cus_cus_nm as Customer, cva_email as Email  
											from '+ @DB +'_arrcus_rec 
											 join '+ @DB +'_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_cus_ven_id = cus_cus_id and cva_cus_ven_typ = ''C''
											and (cva_email Is not null and LTrim(cva_email) <> '''')
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where  (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
											if @Market =''
											BEGIN
												 SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
											END
											SET @sqltxt = @sqltxt + 'group by cus_cmpy_ID, cuc_desc30, cus_cus_nm, cva_email
											order by 2,3'
							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt);
     END
  select * from #tmp
  DROP TABLE #tmp
END

-- exec sp_itech_GetCustomerEmailListbyMarket  'PS','Aerospace','0'



GO
