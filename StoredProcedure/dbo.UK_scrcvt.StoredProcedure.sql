USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_scrcvt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_scrcvt]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
Delete from dbo.UK_scrcvt_rec ;   
   
Insert into dbo.UK_scrcvt_rec   
  
    -- Insert statements for procedure here  
SELECT *  
FROM [LIVEUKSTX].[liveukstxdb].[informix].[scrcvt_rec]  where cvt_cus_ven_id != '2558';
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
