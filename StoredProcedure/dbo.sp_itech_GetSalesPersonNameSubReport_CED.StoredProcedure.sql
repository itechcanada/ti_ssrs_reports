USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetSalesPersonNameSubReport_CED]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <08 Feb 2019>            
-- Description: <Getting top 50 customers for SSRS reports>            
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_GetSalesPersonNameSubReport_CED]  @DBNAME varchar(50),@Branch Varchar(3), @Slp Varchar(10)='ALL'           
            
AS            
BEGIN            
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(6000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(5);            
            
CREATE TABLE #tmp (  salesPersonName varchar(55)           
, LoginID varchar(10)            
  ,Branch Varchar(3)                   
        )           
          
if @Branch ='ALL'                    
 BEGIN                    
 set @Branch = ''                    
 END     
     
if @Slp = 'ALL'                      
BEGIN    
set @Slp = ''    
END    
    
    
            
IF @DBNAME = 'ALL'            
 BEGIN            
            
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query NVARCHAR(4000);               
                 
      SET @query ='INSERT INTO #tmp ( salesPersonName, LoginID,Branch)            
                  select usr_nm,slp_slp,usr_usr_brh from '+ @Prefix +'_mxrusr_rec           
                  join '+ @Prefix +'_scrslp_rec on usr_lgn_id = slp_lgn_id and slp_actv = 1 and usr_actv = 1      
                  AND (usr_usr_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')           
      '            
        print(@query);            
        EXECUTE sp_executesql @query;            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN            
     --print 'M'    ;          
     SET @sqltxt ='INSERT INTO #tmp ( salesPersonName, LoginID,Branch)            
                  select usr_nm,slp_slp,usr_usr_brh from '+ @DBNAME +'_mxrusr_rec           
                  join '+ @DBNAME +'_scrslp_rec on usr_lgn_id = slp_lgn_id and slp_actv = 1 and usr_actv = 1       
                  AND (usr_usr_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')         
                  '            
     print(@sqltxt);             
    set @execSQLtxt = @sqltxt;             
   EXEC (@execSQLtxt);            
      End            
  
         select salesPersonName, LoginID , 'B' as seq from #tmp     
      where LoginID not in (select salesPerson_ShortName from tbl_itech_SalesPerson where isActive = 0     
      OR salesPerson_Branch != Branch ) --     
      Union     
      select salesPerson_LoginID as salesPersonName , salesPerson_ShortName as LoginID ,  'B' as seq  from tbl_itech_SalesPerson where isActive = 1     
      and salesPerson_Branch in (select Branch from #tmp) and salesPerson_ShortName not in (select LoginID from #tmp )    
           
     
               
      -- select top 1 * from #tmp where (LoginID = @Slp or @Slp = '');          
      drop table  #tmp  ;          
END            
            
-- exec sp_itech_GetSalesPersonNameSubReport_CED  'CA','ALL' ,'ALL'           
            
  /*          
            
  */ 
GO
