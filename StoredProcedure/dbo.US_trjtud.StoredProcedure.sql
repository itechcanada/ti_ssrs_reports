USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_trjtud]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,10/1/2012,>  
-- Description: <Description,Warehouse Shipment On Time Performance,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_trjtud]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.US_trjtud_rec', 'U') IS NOT NULL        
  drop table dbo.US_trjtud_rec;        
            
                
SELECT *        
into  dbo.US_trjtud_rec 
FROM [LIVEUSSTX].[liveusstxdb].[informix].[trjtud_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
