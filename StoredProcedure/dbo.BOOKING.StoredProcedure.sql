USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[BOOKING]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/7/2012>
-- Description:	<Description,Booking by date range>
-- Amended To use local database
-- =============================================
CREATE PROCEDURE [dbo].[BOOKING]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime,
	@Branch varchar(3),
	@USD_UK float,
	@USD_TW float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 CREATE TABLE #BOOKING
(
cust_id varchar(10),
cust_name varchar(50),
branch varchar(3),
order_date date,
order_no varchar(10),
sales_id varchar(10),
wgt float,
total float,
total_conversion float,
gp float
)

INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt,a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)*1 as total_conversion,
	100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND a.mbk_trs_md='A'
	  AND a.mbk_brh=@Branch
	  AND a.mbk_sld_cus_id NOT IN ('10','10993','11704','11980','12204','12896','3','5833','6','7','8','9') 
	  AND a.mbk_brh NOT IN ('CRP','TAI','ROC')
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,a.mbk_actvy_dt,a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)*1 DESC
	
	  
INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt,a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)*(@USD_UK) as total_conversion,
		100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [UK_ortmbk_rec] a
INNER JOIN [UK_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO'
	  AND a.mbk_ord_itm<>999
	  AND b.cus_cus_id NOT IN ('171','3','270','467','569','576','608','757','995') 
	  AND a.mbk_wgt > 0
	  AND a.mbk_trs_md='A' 
	  AND a.mbk_brh=@Branch
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,a.mbk_actvy_dt,a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)*(@USD_UK) DESC
	
INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt,a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)*1 as total_conversion,
		100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [CA_ortmbk_rec] a
INNER JOIN [CA_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND a.mbk_trs_md='A'
	  AND b.cus_cus_id NOT IN ('261','153','3272','3645','367','380','571','880','3650')
	  AND a.mbk_brh=@Branch
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,a.mbk_actvy_dt,a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)*1 DESC
	  
INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_actvy_dt,a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)/(@USD_TW) as total_conversion,
		100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [TW_ortmbk_rec] a
INNER JOIN [TW_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND a.mbk_trs_md='A'
	  AND a.mbk_sld_cus_id NOT IN ('189','21','100','44','240')
	  AND a.mbk_brh=@Branch
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,a.mbk_actvy_dt,a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)/(@USD_TW) DESC

/*HPM*/
INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, 'HPM',CAST(a.mbk_actvy_dt AS datetime),a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)*1 as total_conversion,
		100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND a.mbk_brh<>'CRP'
	  AND a.mbk_trs_md='A'
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
	  AND a.mbk_grd IN ('13-8','15-5','1537','316L','316LVM', '625', '625 PLUS','6B','718','S240')
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,CAST(a.mbk_actvy_dt AS datetime),a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)*1 DESC


INSERT INTO #BOOKING
SELECT b.cus_cus_id, b.cus_cus_long_nm, 'IDS',CAST(a.mbk_actvy_dt AS datetime),a.mbk_ord_no,a.mbk_os_slp, SUM(a.mbk_wgt),
	 SUM(a.mbk_tot_mtl_val) as total, SUM(a.mbk_tot_mtl_val)*1 as total_conversion,
		100-CASE WHEN SUM(a.mbk_tot_mtl_val)>0 THEN SUM(a.mbk_mtl_avg_val)/SUM(a.mbk_tot_mtl_val)*100 END as gp 
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND a.mbk_trs_md='A'
	  AND a.mbk_brh<>'CRP'
	  AND CAST(a.mbk_actvy_dt AS datetime) BETWEEN @FromDate AND @ToDate
	  AND a.mbk_frm IN ('TIFT','TISP','TIWP')
GROUP BY b.cus_cus_id, b.cus_cus_long_nm, a.mbk_brh,a.mbk_ord_no,CAST(a.mbk_actvy_dt AS datetime),a.mbk_os_slp
ORDER BY SUM(a.mbk_tot_mtl_val)*1 DESC




SELECT * FROM #BOOKING WHERE branch=@Branch
DROP TABLE #BOOKING
END  


GO
