USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sahstn]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                            
                                                            
-- =============================================                                                            
-- Author:  <Author,Clayton Daigle>                                                            
-- Create date: <Create Date,10/5/2012,>                                                            
-- Description: <Description,Open Orders,>                                                            
 -- Last updated by mrinal Date 10 sep,2015                                                       
 -- Last change Desc:ignore the record stn_upd_ref_no= 113717                                                         
-- =============================================                                                            
CREATE PROCEDURE [dbo].[US_sahstn]                                                            
                                                             
AS                                                            
BEGIN                                                            
 -- SET NOCOUNT ON added to prevent extra result sets from                                                            
 -- interfering with SELECT statements.                                                            
 SET NOCOUNT ON;                                                            
Delete from dbo.US_sahstn_rec where stn_upd_cy >= YEAR(GETDATE() - 4);                                                             
                                                             
Insert into dbo.US_sahstn_rec                                                             
                                                            
    -- Insert statements for procedure here                                                            
    -- There is a problem in column stn_part   .. being used in sp_itech_CustomerByProduct_test, sp_itech_CustomerByProduct_new , sp_itech_CustomerByProduct_Goodrich                                                       
SELECT *                                                            
FROM [LIVEUSSTX].[liveusstxdb].[informix].[sahstn_rec]                                                            
where (stn_upd_ref_no != 111853 OR stn_upd_ref_itm != 1)                                                          
AND (stn_upd_ref_no != 113160 OR stn_upd_ref_itm != 1 )                                                         
AND (stn_upd_ref_no != 113717 OR stn_upd_ref_itm != 1 )                                                      
AND (stn_upd_ref_no != 116328 OR stn_upd_ref_itm != 1 )                                                     
AND (stn_upd_ref_no != 117952 OR stn_upd_ref_itm != 1 )                                                  
AND (stn_upd_ref_no != 118773 OR stn_upd_ref_itm != 1 )                                                
AND(stn_upd_ref_no != 119365 OR  stn_upd_ref_itm != 1 )                                             
AND(stn_upd_ref_no != 119744 OR  stn_upd_ref_itm != 1 )                                           
AND(stn_upd_ref_no != 144566 OR  stn_upd_ref_itm != 1 )                                        
AND(stn_upd_ref_no != 145170 OR  stn_upd_ref_itm != 1 )                                         
AND(stn_upd_ref_no != 145633 OR  stn_upd_ref_itm != 1 )                                       
AND(stn_upd_ref_no != 145789 OR  stn_upd_ref_itm != 1 )                                   
AND(stn_upd_ref_no != 145789 OR  stn_upd_ref_itm != 2 )                                 
AND(stn_upd_ref_no != 188766)                                 
AND(stn_upd_ref_no != 192641)                            
AND(stn_upd_ref_no != 198912)                             
AND(stn_upd_ref_no != 207721)                          
AND(stn_upd_ref_no != 207857 )                        
AND(stn_upd_ref_no != 208839 )  -- 20170823                      
AND(stn_upd_ref_no != 208977 ) --20170824                     
AND(stn_upd_ref_no != 216507 OR  stn_upd_ref_itm != 1 )  -- 20171101                    
AND(stn_upd_ref_no != 224166 ) --20180119            
AND(stn_upd_ref_no != 225797 ) -- 20180201              
AND(stn_upd_ref_no != 250091 ) -- 20180829            
--AND (stn_upd_ref_no != 265578 )     -- 20190118          
--and  (stn_upd_ref_no != 269876 ) -- 20190222              
-- order by stn_upd_ref_no desc         
    
--and stn_upd_cy = 2020 and stn_upd_mth = 3 and stn_upd_dy = 19    
and stn_upd_cy >= YEAR(GETDATE() - 4)   
;                                   
                                  
                                  
                             
insert into US_sahstn_rec                                     
select                                     
'USS',                                    
stn_sls_qlf,                                    
stn_upd_ref_no,                                    
stn_upd_ref_itm,                                    
stn_upd_cy,                                    
stn_upd_mth,                                    
stn_upd_dy,                                    
stn_shpt_pfx,                                    
stn_shpt_no,                                    
stn_shpt_itm,                                    
Replace(stn_shpt_brh,'PSM','LAX'),                                    
stn_inv_pfx,                                    
stn_inv_typ,                                    
stn_upd_ref,                                    
stn_inv_dt,                                    
'L' + stn_sld_cus_id,                                    
stn_shp_to,                                    
stn_nm1,                                    
stn_cus_po,                                    
stn_cus_po_dt,                                    
stn_cus_ctrct,                                    
stn_end_usr_po,                                    
stn_cus_rls,                                    
stn_job_id,                                    
stn_cry,                                    
stn_exrt,                                    
stn_ex_rt_typ,                                    
stn_is_slp,                                    
stn_os_slp,                        
stn_tkn_slp,                                    
stn_sls_cat,                                    
stn_chrg_qty_typ,                                    
stn_wfl_sls,                                    
stn_use_wgt_mult,                                    
stn_wgt_mult_pct,                                    
stn_aly_schg_cl,                            
stn_src,                                    
stn_cntr_sls,                                    
stn_fab_itm_cd,                                    
stn_ord_pfx,                                    
stn_ord_no,                                    
stn_ord_itm,                                    
stn_ord_rls_no,                                    
stn_transp_pfx,                           
stn_transp_no,                                    
stn_pk_list_no,                                    
replace(stn_shpg_whs,'PSM','LAX'),                                    
stn_shp_dt,                                    
stn_sprc_pfx,                                    
stn_sprc_no,                                    
stn_trm_trd,                                    
stn_frm,                                    
stn_grd,                                  
stn_num_size1,                                    
stn_num_size2,                                    
stn_num_size3,                                    
stn_num_size4,                                    
stn_num_size5,                                    
stn_size,                                    
stn_fnsh,                                    
stn_ef_evar,                                    
stn_wdth,                                    
stn_lgth,                                    
stn_dim_dsgn,                                    
stn_octg_ent_md,                                    
stn_idia,                                    
stn_odia,     
stn_ga_size,                                    
stn_ga_typ,                                    
stn_alt_size,                                    
stn_rdm_dim_1,                                    
stn_rdm_dim_2,                                    
stn_rdm_dim_3,                                    
stn_rdm_dim_4,          
stn_rdm_dim_5,                                    
stn_rdm_dim_6,                                    
stn_rdm_dim_7,                                    
stn_rdm_dim_8,                                    
stn_rdm_area,                                    
stn_ent_msr,                                    
stn_dia_frc_fmt,                                    
stn_ord_lgth_typ,                                    
stn_part_cus_id,                                    
stn_part,                                    
stn_part_rev_no,                                    
stn_part_acs,                                    
stn_wdth_intgr,                                    
stn_wdth_nmr,                                   
stn_wdth_dnm,                                    
stn_lgth_intgr,                                    
stn_lgth_nmr,                                    
stn_lgth_dnm,                                    
stn_idia_intgr,                                    
stn_idia_nmr,                                    
stn_idia_dnm,                                    
stn_odia_intgr,                                    
stn_odia_nmr,                                    
stn_odia_dnm,                                    
stn_blg_pcs,                                    
stn_blg_msr,                                    
stn_blg_wgt,                                    
stn_blg_qty,                                    
stn_ord_wgt_um,                                    
stn_rlvd_pcs,                                    
stn_rlvd_msr,                
stn_rlvd_wgt,                                    
stn_rlvd_qty,                                    
stn_ord_chrg,                                    
stn_mtl_chrg,                                    
stn_mtl_chrg_um,                                    
stn_tot_chrg,                                    
stn_tot_val,                                    
stn_tot_mtl_val,                                    
stn_mtl_avg_val,                                    
stn_mtl_repl_val,                                    
stn_tot_avg_val,                                    
stn_tot_repl_val,                                    
stn_mpft_avg_val,                                    
stn_mpft_repl_val,                                    
stn_npft_avg_val,                                    
stn_npft_repl_val,                                    
stn_mpft_avg_pct,                                    
stn_mpft_repl_pct,                                    
stn_npft_avg_pct,                                    
stn_npft_repl_pct                                    
from PS_sahstn_rec;                                        
END       
      
      
/*20190225      
Change the lenght of stn_part_rev_no from 2 to 8     
  
  
20200322  
Mail sub:RE: Fwd: eCIM Call #352036 - Regarding Sales History Data   
*/
GO
