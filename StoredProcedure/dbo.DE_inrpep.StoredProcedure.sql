USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_inrpep]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================        
-- Author:  <Sumit>        
-- Create date: <10/27/2020>        
-- Description: <Germany Product Enterprise Parameters>        
-- Requirement: <To get product description for journal receiving report>  
-- =============================================        
CREATE PROCEDURE [dbo].[DE_inrpep]        
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
IF OBJECT_ID('dbo.DE_inrpep_rec', 'U') IS NOT NULL              
  drop table dbo.DE_inrpep_rec;              
                  
                      
SELECT *              
into  dbo.DE_inrpep_rec       
FROM [LIVEDESTX].[livedestxdb].[informix].[inrpep_rec]    
    
END 
GO
