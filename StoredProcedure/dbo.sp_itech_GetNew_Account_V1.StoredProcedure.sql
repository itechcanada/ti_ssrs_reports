USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNew_Account_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================            
-- Author:  <Sumit>            
-- Create date: <12 Oct 2021>            
-- Description: <Getting New Account>            
-- Inherited: sp_itech_GetNew_Dormant_Lost_Account_v1
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_GetNew_Account_V1]  @DBNAME varchar(50),@Branch varchar(50),@ISCombineData int,@Account varchar(50),@Month Datetime,@Market varchar(50), @version char = '0', @SlsPerson varchar(50) = 'ALL'           
            
AS            
BEGIN            
SET NOCOUNT ON;            
            
            
declare @sqltxt varchar(6000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @NFD varchar(10)            
declare @NTD varchar(10)            
declare @D3MFD varchar(10)            
declare @D3MTD varchar(10)            
declare @D6MFD varchar(10)            
declare @D6MTD varchar(10)            
declare @LFD varchar(10)            
declare @LTD varchar(10)            
declare @FD varchar(10)            
declare @TD varchar(10)            
declare @RFD varchar(10)            
declare @RTD varchar(10)            
declare @AFD varchar(10)            
declare @ATD varchar(10)            
            
set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month) - 12, 0) , 120)   --First day of previous 12 month            
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)+1,0)), 120)  --Last Day of current month            
            
set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-1,0)) , 120)   -- New Account            
set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)  -- New Account            
            
set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-4,0)) , 120)   -- Dormant 3 Month            
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-3,0)), 120)  -- Dormant 3 Month            
            
set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-7,0)) , 120)   --Dormant 6 Month            
set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-6,0)), 120)  -- Dormant 6 Month            
            
set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Lost Account            
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-18,0)), 120)  -- Lost Account            
            
            
set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Reactivated Account            
set @RTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)), 120)  -- Reactivated Account            
            
set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)    -- Active Accounts             
set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)            
            
  print @AFD + '-' + @ATD          
            
            
 -- IF @ISCombineData =1            
 --BEGIN            
 --  set @NFD =@FD             
 --  set @NTD = @TD            
 --  set @D3MFD = @FD            
 --  set @D3MTD = @TD            
 --  set @D6MFD = @FD            
 --  set @D6MTD = @TD            
 --  set @LFD = @FD            
 --  set @LTD = @TD            
 --  END            
             
--1 New Accounts            
--2 Dormant Accounts 3M            
--3 Dormant Accounts 6M            
--4 Lost Accounts            
--5 Total Reactivated Accounts            
--6 Current Month Total Active Customers            
--0 ALL            
            
CREATE TABLE #tmp (      AccountType varchar(50)            
                        ,CustomerID  VARCHAR(15)            
      ,AccountName VARCHAR(150)            
      ,AccountDate varchar(15)            
      ,FirstSaleDate Varchar(15)            
      ,Branch  VARCHAR(15)            
      ,Category  VARCHAR(100)     
      ,SalePersonName  VARCHAR(50)            
      --,SalePersonEmail  VARCHAR(75)            
      ,CustomerEmail  VARCHAR(75)            
      ,DatabaseName Varchar(3)            
        );            
                    
CREATE TABLE #tmpFinal (      AccountType varchar(50)            
                        ,CustomerID  VARCHAR(15)            
      ,AccountName VARCHAR(150)            
      ,AccountDate varchar(15)            
      ,FirstSaleDate Varchar(15)            
      ,Branch  VARCHAR(15)            
      ,Category  VARCHAR(100)            
      ,SalePersonName  VARCHAR(50)            
      --,SalePersonEmail  VARCHAR(75)            
      ,CustomerEmail  VARCHAR(75)            
      ,DatabaseName Varchar(3)            
        );                    
                    
CREATE TABLE #tactive (  CustomerID  VARCHAR(15)            
      ,FirstInvOfMonth Date            
      ,STN_INV_DT date            
      ,STN_OS_SLP varchar(75)            
       --,USR_EMAIL varchar(75)            
       ,CUS_EMAIL varchar(75)            
        );            
                    
CREATE TABLE #tTtlactive (  CustomerID  VARCHAR(15)            
 ,FirstInvOfMonth Date            
 ,STN_INV_DT date            
 ,STN_OS_SLP varchar(75)            
 -- ,USR_EMAIL varchar(75)            
  ,CUS_EMAIL varchar(75)            
);                    
                                    
DECLARE @company VARCHAR(15);             
DECLARE @prefix VARCHAR(15);             
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @CurrenyRate varchar(15);            
DECLARE @Category varchar(50);            
            
set @DB= @DBNAME           
          
IF @SlsPerson = 'ALL'            
 BEGIN            
  set @SlsPerson = ''            
 END           
                  
IF @Branch = 'ALL'            
 BEGIN            
  set @Branch = ''            
 END            
             
 IF @Market = 'ALL'              
   set @Market = ''            
 ELSE IF @Market ='Unknown'            
  Begin            
   set @Category='Unknown'            
   set @Market = ''            
  END            
             
                  
 IF @DBNAME = 'ALL'            
 BEGIN            
 IF @version = '0'              
   BEGIN              
   DECLARE ScopeCursor CURSOR FOR            
    select DatabaseName, company,prefix from tbl_itech_DatabaseName             
     OPEN ScopeCursor;             
   END              
   ELSE              
   BEGIN              
   DECLARE ScopeCursor CURSOR FOR            
    select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS --where prefix <> 'PS'            
     OPEN ScopeCursor;             
   END             
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query NVARCHAR(4000);               
       set @DB= @prefix                
                   
       CREATE TABLE #tactive1 (  CustomerID  VARCHAR(15)            
             ,FirstInvOfMonth Date            
             ,STN_INV_DT date            
             ,STN_OS_SLP varchar(75)            
             --,USR_EMAIL varchar(75)            
             ,CUS_EMAIL varchar(75)            
               );            
                             CREATE TABLE #tTtlactive1 (  CustomerID  VARCHAR(15)            
              ,FirstInvOfMonth Date            
              ,STN_INV_DT date            
                  ,STN_OS_SLP varchar(75)            
                 -- ,USR_EMAIL varchar(75)            
                  ,CUS_EMAIL varchar(75)            
             );                           
                   
  --     SET @query = ' INSERT INTO #tactive1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP,CUS_EMAIL)            
  --            select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
  --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)             
  --            from '+ @DB +'_sahstn_rec            
  --            left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --            left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --            where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''            
  --             group by stn_sld_cus_id ,STN_INV_DT'            
       
  --print (@query);    
  --         EXECUTE sp_executesql @query;            
                       
  --       -- Total Active            
  --         SET @query = ' INSERT INTO #tTtlactive1(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP, CUS_EMAIL)            
  -- select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
  --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)             
  --            from '+ @DB +'_sahstn_rec            
  --            left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --            left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --            where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''            
  --            group by stn_sld_cus_id ,STN_INV_DT'            
                           
  --             print @query;            
            
  --         EXECUTE sp_executesql @query;                
      if(@DB = 'US' and @ATD > ('2015-11-01'))           
                      
             begin                    
           -- Total Active            
   --        SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail,DatabaseName)            
   --           SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,            
   --           Min(t.STN_OS_SLP) as SalePersonName, MIN(t.CUS_EMAIL) , '''+ @DB +'''            
   --           FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
   --           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
   --           where             
   --           (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
   --           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
   --           t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''             
   --           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                     
   --           ORDER BY 9 DESC '     
   --print (@query);    
   --          EXECUTE sp_executesql @query;            
                        
                       
                         
             -- Reactivated             
   --         SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
   --           SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,             
   --           (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate            
   --           , CUS_ADMIN_BRH,cuc_desc30 as category,            
   --           Min(t.STN_OS_SLP) as SalePersonName,  MIN(t.CUS_EMAIL) ,  '''+ @DB +'''            
   --           FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
   --           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
   --           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id              
   --           where             
   --           (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
   --           and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
   --           t.CustomerID Not IN             
   --           ( select distinct t1.CustomerID  from #tactive1 t1,            
   --           '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID             
   --           and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')            
   --           and t.CustomerID not in  (select arb.coc_cus_id from '+ @DB +'_arbcoc_rec arb      
   --left join PS_arbcoc_rec ps on ps.coc_cus_id = arb.coc_cus_id      
   --           where       
   ---- dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt)       
   --(case when SUBSTRING(arb.coc_cus_id,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
   --(case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
   --else arb.coc_frst_sls_dt end) end)      
   --Between  ''' + @NFD + ''' And ''' + @NTD +'''            
   --           )            
   --           and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''            
   --           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt             
   --           ORDER BY 9 DESC '         
   --  print (@query);    
   --            EXECUTE sp_executesql @query;            
                           
        -- NEW            
            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName, CustomerEmail, DatabaseName)            
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,      
      --dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)       
      (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)      
      as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,            
    Min(STN_OS_SLP) as SalePersonName,             
                (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)  ,            
                 '''+ @DB +'''             
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID             
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID            
               left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id             
               left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
               left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''        
      left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))       
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')             
               --and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)       
      and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)       
      Between  ''' + @NFD + ''' And ''' + @NTD +'''            
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt,       
      -- dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)      
      (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)      
      '            
   print (@query);    
             EXECUTE sp_executesql @query;            
                         
                   
    --     SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,  CustomerEmail, DatabaseName)            
    --           SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
    --  --dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    --  (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --  as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),            
    --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)  ,            
    --             '''+ @DB +'''            
    --       FROM '+ @DB +'_sahstn_rec            
    --       INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
    --       Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
    --       left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id           
    --       left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
    --       left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''       
    -- left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))      
    --       where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
    --       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
    --       --and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    -- and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    -- Between  '''+ @D3MFD +''' and ''' + @D3MTD +'''           
    --       group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,      
    -- --dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)      
    -- (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --       '            
    --        print (@query);           
    --        EXECUTE sp_executesql @query;            
                        
    --                  SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName, CustomerEmail, DatabaseName)            
    --           SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
    --  -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    --  (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    --  (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    --  (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --  as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),             
    --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end) ,             
    --             '''+ @DB +'''            
    --       FROM '+ @DB +'_sahstn_rec            
    --       INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
    --       Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
    --       left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id             
    --       left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
    --       left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''       
    -- left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))       
    --       where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
    --       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
    --       -- and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    -- and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    -- Between  '''+ @D6MFD +''' and ''' + @D6MTD +'''            
    --       group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,      
    -- -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)            
    -- (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --       '            
    --        print (@query);           
    --        EXECUTE sp_executesql @query;                 
                             
    --             SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail,DatabaseName)            
    --           SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
    --  -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    --(case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    --   (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --  as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,            
    --           Min(STN_OS_SLP),             
    --           (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end) ,              
    --            '''+ @DB +'''            
    --           FROM '+ @DB +'_sahstn_rec            
    --       INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
    --       Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
    --       left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id            
    --       left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
    --left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
    -- left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))       
    --       where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
    --       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
    --       --and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
    -- and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)       
    -- Between  '''+ @LFD +''' and ''' + @LTD +'''             
    --       group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,      
    -- -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)            
    -- (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
    -- (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
    -- (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
    --       '            
    --        print (@query);           
    --        EXECUTE sp_executesql @query;            
End          
Else          
Begin          
-- Total Active            
--           SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail,DatabaseName)            
--              SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,            
--              Min(t.STN_OS_SLP) as SalePersonName, MIN(t.CUS_EMAIL) , '''+ @DB +'''            
--              FROM #tTtlactive1 t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
--              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
--              where             
--              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
--              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
--              t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''             
--              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                     
--              ORDER BY 9 DESC '    
         
--     print (@query);    
--             EXECUTE sp_executesql @query;            
                        
                       
                         
--             -- Reactivated             
--            SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
--              SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,             
--              (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate            
--              , CUS_ADMIN_BRH,cuc_desc30 as category,            
--              Min(t.STN_OS_SLP) as SalePersonName,  MIN(t.CUS_EMAIL) ,  '''+ @DB +'''            
--              FROM #tactive1 t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
--              left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
--              left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id              
--              where             
--              (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
--              and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and t.CustomerID Not IN             
--( select distinct t1.CustomerID  from #tactive1 t1,            
--              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID             
--              and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')            
--              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec             
--              where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''            
--              )            
--              and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''            
--              group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt             
--              ORDER BY 9 DESC '     
         
--     print (@query);    
--               EXECUTE sp_executesql @query;            
                           
        -- NEW            
       SET @query = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName, CustomerEmail, DatabaseName)            
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,            
               Min(STN_OS_SLP) as SalePersonName,             
                (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)  ,            
                 '''+ @DB +'''             
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID             
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID            
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id             
               left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
               left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')             
               and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''            
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt'    
          
      print (@query);    
             EXECUTE sp_executesql @query;            
                         
                   
  --       SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,  CustomerEmail, DatabaseName)            
  --             SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),            
  --              (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)  ,            
  --               '''+ @DB +'''            
  --         FROM '+ @DB +'_sahstn_rec            
  --         INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
  --         Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
  --         left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --         where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
  --         and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
  --         and STN_INV_DT <= ''' + @NTD +'''            
  --         group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH             
  --     Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''            
       
  -- print (@query);    
  --          EXECUTE sp_executesql @query;            
                        
  --                    SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName, CustomerEmail, DatabaseName)            
  --        SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),             
  --              (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end) ,             
  --               '''+ @DB +'''            
  --         FROM '+ @DB +'_sahstn_rec            
  --         INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
  --         Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
  --  left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --         left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --         where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
  --         and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
  --         and STN_INV_DT <= ''' + @NTD +'''            
  --         group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH            
  --         Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''            
                 
  -- print (@query);     
  --          EXECUTE sp_executesql @query;                 
                             
  --               SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail,DatabaseName)            
  --             SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,            
  --             Min(STN_OS_SLP),           
  --             (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end) ,              
  --              '''+ @DB +'''            
  --             FROM '+ @DB +'_sahstn_rec            
  --         INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
  --         Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
  --         left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --         left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --         where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
  --         and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
  --         and STN_INV_DT <= ''' + @NTD +'''            
  --         group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH            
  --         Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''            
                
  -- print (@query);    
  --          EXECUTE sp_executesql @query;            
End                         
            Drop table #tactive1;            
            Drop table #tTtlactive1;            
                        
            if @DB='US'            
        Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')            
       else if( not( @DB='PS' and @ATD > '2015-11-01'))          
        Insert into #tmpFinal select * from #tmp             
                 
     delete from #tmp            
                        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END   
  ELSE            
     BEGIN            
                  
  --   SET @sqltxt = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP,CUS_EMAIL)            
  --            select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
  --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)            
  --            from '+ @DB +'_sahstn_rec            
  --            left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --            left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --            where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''            
  --     group by stn_sld_cus_id ,STN_INV_DT'            
  --print 'T active sqltxt =' + @sqltxt          
  --              set @execSQLtxt = @sqltxt;             
  --              EXEC (@execSQLtxt);            
                       
  --         -- Total Active            
  --         SET @sqltxt = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP, CUS_EMAIL)            
  --            select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
  --            (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)            
  --            from '+ @DB +'_sahstn_rec            
  --            left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
  --            left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
  --            where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''            
  --             group by stn_sld_cus_id ,STN_INV_DT'            
  --             print 'TotalActive sql = ' + @sqltxt            
  --             set @execSQLtxt = @sqltxt;             
  --                                     EXEC (@execSQLtxt);           
                                                  
    if(@DB = 'US' and @ATD > ('2015-11-01'))           
                      
             begin       
           -- Total Active            
     --      SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
     --         SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,            
     --         Min(t.STN_OS_SLP) as SalePersonName,  Min(t.CUS_EMAIL) , '''+ @DB +'''            
     --         FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
     --         left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
     --         where             
     --         (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
     --         and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
     --         t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''             
     --         group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                     
     --         ORDER BY 9 DESC '            
     --         print @sqltxt;             
     --       set @execSQLtxt = @sqltxt;             
     --    EXEC (@execSQLtxt);            
                         
     --        -- Reactivated             
     --       SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,            
     --         SalePersonName, CustomerEmail, DatabaseName)            
     --         SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,             
     --         (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate            
     --         , CUS_ADMIN_BRH,cuc_desc30 as category,            
     --         Min(t.STN_OS_SLP) as SalePersonName, Min(t.CUS_EMAIL) , '''+ @DB +'''            
     --         FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
     --         left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
     --         left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id              
     --         where             
     --         (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
     --         and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
     --         t.CustomerID Not IN             
     --         ( select distinct t1.CustomerID  from #tactive t1,            
     --         '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID             
     --         and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')            
     --         and t.CustomerID not in  (select arb.coc_cus_id from '+ @DB +'_arbcoc_rec arb       
     --left join PS_arbcoc_rec ps on ps.coc_cus_id = arb.coc_cus_id       
     --         where       
     ----dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt)       
     --(case when SUBSTRING(arb.coc_cus_id,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
     --(case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
     --else arb.coc_frst_sls_dt end) end)       
     --Between ''' + @NFD + ''' And ''' + @NTD +'''            
     --         )            
     --         and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''            
     --         group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt            
     --         ORDER BY 9 DESC  '            
     --         print 'reactive Q :' + @sqltxt;             
     --           set @execSQLtxt = @sqltxt;             
     --    EXEC (@execSQLtxt);            
                   
                   
        -- NEW            
            SET @sqltxt = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,      
      -- dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)       
      (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)       
      as coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,            
               Min(STN_OS_SLP) as SalePersonName,             
                (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),             
                '''+ @DB +'''             
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID             
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID            
               left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id             
               left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
               left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
      left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))      
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
               --and dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)       
      and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)      
      Between  ''' + @NFD + ''' And ''' + @NTD +'''       
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt,       
      -- dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt)      
      (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_frst_sls_dt else         
      (case when ps.coc_frst_sls_dt is not null or ps.coc_frst_sls_dt != '''' then (case When (arb.coc_frst_sls_dt < ps.coc_frst_sls_dt) then arb.coc_frst_sls_dt else ps.coc_frst_sls_dt end)        
      else arb.coc_frst_sls_dt end) end)      
      '            
                        
       print(@sqltxt)            
         set @execSQLtxt = @sqltxt;             
         EXEC (@execSQLtxt);            
                   
     --  SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
     --          SELECT   ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
     -- -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     -- (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     -- as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
     --          ,Min(STN_OS_SLP),             
     --           (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
     --            '''+ @DB +'''            
     --          FROM '+ @DB +'_sahstn_rec            
     --      INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
     --      Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
     --      left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id            
     --      left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
     --      left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''        
     --left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))      
     --      where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
     --      and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
     --      --and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     --Between  '''+ @D3MFD +''' and ''' + @D3MTD +'''           
     --      group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,      
     ----dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --(case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)'           
                     
     --  print(@sqltxt)            
     --  set @execSQLtxt = @sqltxt;             
     --    EXEC (@execSQLtxt);            
                   
     --  SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
     --          SELECT   ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
     -- -- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     -- (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --    (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --    (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     -- as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
     --          ,Min(STN_OS_SLP),              
     --          (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
     --           '''+ @DB +'''            
     --           FROM '+ @DB +'_sahstn_rec            
     --      INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
     --      Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
     --      left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id             
     --      left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
     --      left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''          
     --left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))      
     --      where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
     --      and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
     --      --and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     --Between  '''+ @D6MFD +''' and ''' + @D6MTD +'''            
     --      group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,      
     ----dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --(case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)       
     --      '            
            
     --  print(@sqltxt)            
     --  set @execSQLtxt = @sqltxt;             
     --  EXEC (@execSQLtxt);             
                   
     --  SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName, CustomerEmail,DatabaseName)            
     --          SELECT   ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,      
     -- --dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --(case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --   (Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --   (case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     -- as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
     --          ,Min(STN_OS_SLP),             
     --           (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
     --            '''+ @DB +'''            
     --           FROM '+ @DB +'_sahstn_rec            
     --      INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
     --      Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
     --      left join '+ @DB +'_arbcoc_rec arb on arb.coc_cmpy_id = cus_cmpy_id and arb.coc_cus_id = cus_cus_id           
     --      left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
     --      left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''       
     --left join PS_arbcoc_rec ps on ps.coc_cus_id = SUBSTRING(STN_SLD_CUS_ID,2,len(STN_SLD_CUS_ID))       
     --      where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
     --      and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
     --      --and dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --and (case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     --Between  '''+ @LFD +''' and ''' + @LTD +'''           
     --      group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,      
     ---- dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt)       
     --(case when SUBSTRING(STN_SLD_CUS_ID,1,1) ! = ''L'' then arb.coc_lst_sls_dt else         
     --(Case when arb.coc_lst_sls_dt is null or arb.coc_lst_sls_dt = '''' then ps.coc_lst_sls_dt else         
     --(case When (arb.coc_lst_sls_dt > ps.coc_lst_sls_dt) then arb.coc_lst_sls_dt else ps.coc_lst_sls_dt end) end) end)      
     --      '            
            
     --  print(@sqltxt)            
     --  set @execSQLtxt = @sqltxt;             
     --  EXEC (@execSQLtxt);             
End          
Else          
Begin          
--SET @sqltxt = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP,CUS_EMAIL)            
--              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
--              (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)            
--              from '+ @DB +'_sahstn_rec            
--              left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
--              left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''        
--              where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''            
--               group by stn_sld_cus_id ,STN_INV_DT'            
--  print 'T active sqltxt =' + @sqltxt          
--                set @execSQLtxt = @sqltxt;             
--                EXEC (@execSQLtxt);            
                       
--           -- Total Active            
--           SET @sqltxt = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth,STN_INV_DT,STN_OS_SLP, CUS_EMAIL)            
--              select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth ,STN_INV_DT,min(STN_OS_SLP),            
--              (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end)            
--              from '+ @DB +'_sahstn_rec            
--              left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
--              left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
--              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''            
--               group by stn_sld_cus_id ,STN_INV_DT'            
--               print 'TotalActive sql = ' + @sqltxt            
--               set @execSQLtxt = @sqltxt;             
--                                       EXEC (@execSQLtxt);            
                       
           -- Total Active            
         --  SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
         --     SELECT ''6'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,'''', CUS_ADMIN_BRH,cuc_desc30 as category,            
         --     Min(t.STN_OS_SLP) as SalePersonName,  Min(t.CUS_EMAIL) , '''+ @DB +'''            
         --     FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
         --     left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
         --     where             
         --     (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
         --     and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
         --     t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''             
         --     group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH                     
         --     ORDER BY 9 DESC '            
         --     print @sqltxt;             
         --   set @execSQLtxt = @sqltxt;             
         --EXEC (@execSQLtxt);            
                         
         --    -- Reactivated             
         --   SET @sqltxt = ' INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,            
         --     SalePersonName, CustomerEmail, DatabaseName)            
         --     SELECT ''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,max(t.stn_inv_dt) as InvDate,             
         --     (select max(stn_inv_dt) from '+ @DB +'_sahstn_rec s1 where s1.stn_sld_cus_id = CUS_CUS_ID and s1.stn_inv_dt <  '''+  @LTD + ''') as FirstSaleDate            
         --     , CUS_ADMIN_BRH,cuc_desc30 as category,            
         --     Min(t.STN_OS_SLP) as SalePersonName, Min(t.CUS_EMAIL) , '''+ @DB +'''            
         --     FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id             
         --     left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
         --     left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id              
         --     where             
         --     (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
         --     and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') and            
         --     t.CustomerID Not IN             
         --     ( select distinct t1.CustomerID  from #tactive t1,            
         --     '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID             
         --     and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''')            
         --     and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec             
         --     where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''            
         --     )            
         --     and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + '''            
         --     group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH , coc_frst_sls_dt            
         --     ORDER BY 9 DESC  '            
         --     print 'reactive Q :' + @sqltxt;             
         --       set @execSQLtxt = @sqltxt;             
         --EXEC (@execSQLtxt);    
                   
                   
        -- NEW            
            SET @sqltxt = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
               select ''1'', STN_SLD_CUS_ID, CUS_CUS_LONG_NM,crd_acct_opn_dt,coc_frst_sls_dt, CUS_ADMIN_BRH,cuc_desc30 as category,            
               Min(STN_OS_SLP) as SalePersonName,             
                (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),             
                '''+ @DB +'''             
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID             
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID            
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id             
               left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
              left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
               where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
               and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')             
               and coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +'''          
                and ((stn_sld_cus_id not like ''L%'' and CUS_ADMIN_BRH = ''LAX'') or (CUS_ADMIN_BRH <> ''LAX''))          
               Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 ,crd_acct_opn_dt, coc_frst_sls_dt'            
                        
       print(@sqltxt)            
         set @execSQLtxt = @sqltxt;             
         EXEC (@execSQLtxt);            
                   
 --      SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
 --              SELECT   ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
 --              ,Min(STN_OS_SLP),             
 --               (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
 --                '''+ @DB +'''            
 --              FROM '+ @DB +'_sahstn_rec            
 --          INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
 --          Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
 --          left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
 --          left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
 --          where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
 --          and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
 --          and STN_INV_DT <= ''' + @NTD +'''            
 --          group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH            
 --          Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''            
                       
 --              print(@sqltxt)            
 --      set @execSQLtxt = @sqltxt;             
 --        EXEC (@execSQLtxt);            
                   
 --      SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,CustomerEmail, DatabaseName)            
 --              SELECT   ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
 --,Min(STN_OS_SLP),              
 --              (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
 --               '''+ @DB +'''            
 --               FROM '+ @DB +'_sahstn_rec            
 --          INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
 --          Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
 --          left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
 --          left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
 --          where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
 --          and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
 --          and STN_INV_DT <= ''' + @NTD +'''            
 --          group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH            
 --          Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''            
            
 --      print(@sqltxt)            
 --      set @execSQLtxt = @sqltxt;             
 --      EXEC (@execSQLtxt);             
                   
 --      SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName, CustomerEmail,DatabaseName)            
 --              SELECT   ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category            
 --              ,Min(STN_OS_SLP),             
 --               (case When (max(cva_email) Is null OR LTrim(max(cva_email)) = '''') then max(cvt_email) else Max(cva_email) end),            
 --                '''+ @DB +'''            
 --               FROM '+ @DB +'_sahstn_rec            
 --          INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID            
 --          Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
 --          left join '+ @DB +'_scrcvt_rec on stn_cmpy_id = cvt_cmpy_id and stn_sld_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = ''C''            
 --          left join '+ @DB +'_scrcva_rec on cva_cmpy_id = stn_cmpy_id and cva_cus_ven_id = stn_sld_cus_id and cva_cus_ven_typ = ''C''             
 --          where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
 --          and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')            
 --          and STN_INV_DT <= ''' + @NTD +'''            
 --          group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH            
 --          Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''            
            
 --      print(@sqltxt)         
 --      set @execSQLtxt = @sqltxt;             
 --      EXEC (@execSQLtxt);             
End              
                  
       if @DB='US'            
        Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')            
     else if( not( @DB='PS' and @ATD > '2015-11-01'))          
        Insert into #tmpFinal select * from #tmp             
                 
     delete from #tmp            
                   
     END            
              
        IF @Category='Unknown'            
           BEGIN            
                IF @ISCombineData =1            
     BEGIN            
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName, '' as CustomerEmail, DatabaseName            
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType            
        where AccountType=@Account   and (SalePersonName = @SlsPerson OR @SlsPerson = '')          
        -- and  (Branch <> 'PSM' and @ATD >'2015-11-01')           
       -- and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
         and Category is null            
        group by Name,Category,DatabaseName            
     END             
     Else IF @ISCombineData =2            
     BEGIN            
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,'' as CustomerEmail, DatabaseName            
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType            
        where AccountType=@Account  and (SalePersonName = @SlsPerson OR @SlsPerson = '') --  and  (Branch <> 'PSM' and @ATD >'2015-11-01')           
        --and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
        and Category is null            
        group by Name,Branch,DatabaseName            
     END             
      Else            
     BEGIN            
       select *, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category            
       from #tmpFinal where AccountType=@Account and (SalePersonName = @SlsPerson OR @SlsPerson = '') -- and  (Branch <> 'PSM' and @ATD >'2015-11-01')             
     --  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
       and Category is null            
       order by CustomerID --and AccountDate <= '2013-03-31' and AccountDate >='2013-03-01'            
     END            
           END            
           ELSE            
           BEGIN            
                 IF @ISCombineData =1            
     BEGIN            
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName,'' as CustomerEmail,DatabaseName            
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType            
        where AccountType=@Account and (SalePersonName = @SlsPerson OR @SlsPerson = '')--  and  (Branch <> 'PSM' and @ATD >'2015-11-01')            
      --  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
        group by Name,Category,DatabaseName            
     END             
     Else IF @ISCombineData =2            
     BEGIN            
        select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,'' as CustomerEmail,DatabaseName            
        from #tmpFinal inner Join  tbl_itech_Account on id=AccountType            
        where AccountType=@Account and (SalePersonName = @SlsPerson OR @SlsPerson = '')--  and  (Branch <> 'PSM' and @ATD >'2015-11-01')           
        --and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
        group by Name,Branch,DatabaseName            
     END             
      Else            
     BEGIN            
               
       select * , ( Case When(CustomerID IN('4439','4440','4441','507','508','509','510')) then '' else ISNULL(Rtrim(LTrim(Category)),'Unknown') end )as New_Category            
       from #tmpFinal where AccountType=@Account and (SalePersonName = @SlsPerson OR @SlsPerson = '')-- and  (Branch <> 'PSM' and @ATD >'2015-11-01')           
       --and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))            
       order by CustomerID --and AccountDate <= '2013-03-31' and AccountDate >='2013-03-01'            
     END            
           END            
                       
            Drop table #tTtlactive;            
            Drop table #tmpFinal             
   Drop table #tactive;               
  Drop table #tmp              
              
END            
            
-- exec sp_itech_GetNew_Dormant_Lost_Account 'PS','PSM',0,'6','2015-12-15','ALL','1'    
-- exec sp_itech_GetNew_Dormant_Lost_Account 'UK','ALL',0,'6','2020-12-15','ALL','1'    
-- exec sp_itech_GetNew_Dormant_Lost_Account_V1 'US','DET',0,'6','2021-02-21','ALL','1'  
--For testing of Dormat account            
            
/*          
Date: 20161202          
Mail:NLD Main report screen shot          
20201221 Sumit      
Replace fun_itech_GetFirstSalesDate and fun_itech_GetLastSalesDate functions with query condition      
      
*/ 
GO
