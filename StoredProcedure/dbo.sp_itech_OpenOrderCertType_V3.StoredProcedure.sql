USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenOrderCertType_V3]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                    
-- Author:  <Mukesh >                                    
-- Create date: <21 OCT 2019>                                    
-- Description: <Getting top 50 customers for SSRS reports> 
-- =============================================                                    
CREATE PROCEDURE [dbo].[sp_itech_OpenOrderCertType_V3] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),                      
@OrderType varchar(10),@version char = '0',@DateRange int = 0, @CustomerID varchar(10) = ''                               
AS                                    
BEGIN                                   
                              
SET NOCOUNT ON;             
           
                                  
declare @sqltxt varchar(8000)                                    
declare @execSQLtxt varchar(8000)                                    
declare @DB varchar(100)                                    
declare @FD varchar(10)                                    
declare @TD varchar(10)                                    
declare @NOOfCust varchar(15)                                    
DECLARE @ExchangeRate varchar(15)                                
                              
                                  
     if(@FromDate = '' OR @FromDate = null OR @FromDate = '1900-01-01' )                              
     begin                              
     set @FromDate = '2009-01-01'                              
      End                              
                                    
      if (@ToDate = '' OR @ToDate = null OR @ToDate = '1900-01-01')                              
      begin                              
      set @ToDate = CONVERT(VARCHAR(10), GETDATE() , 120)                              
      end                              
                                    
set @DB= @DBNAME -- UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'                                
if @DateRange=6 -- YTD (excluding today)                                    
 BEGIN                                    
  set @FD = CONVERT(VARCHAR(10), DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) , 120)   -- first Day of Year                                   
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) --                                 
 End                                     
else if @DateRange=2 -- Last 7 days (excluding today)                                    
 BEGIN                                    
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day                                    
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                                    
 End                                    
Else                                    
Begin                                     
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                                    
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                                    
End                                    
                                    
                                    
CREATE TABLE #tmp (  CompanyId varchar(5)                                     
        ,OrderPrefix Varchar(2)                                    
        ,OrderNo varchar(10)                                    
        ,OrderItem   varchar(3)                                    
        ,Branch varchar(10)                                    
        ,CustID   VARCHAR(10)                                     
        , CustName     VARCHAR(65)                                    
        , CustomerCat   VARCHAR(65)
		, SubMarketSegment Varchar(45)
        ,Currency  Varchar(3)                                  
        , UKChargeValue    DECIMAL(20, 2)                             
        , USChargeValue    DECIMAL(20, 2)                                      
        , BalPcs  DECIMAL(20, 2)                                    
        , BalWgt           DECIMAL(20, 2)                                    
        , BalMsr DECIMAL(20, 2)                                    
        , StatusAction               varchar(2)                                    
        , Salesperson     varchar(65)                                    
        , DueDate    varchar(20)                                    
        , ProductName   VARCHAR(100)                                     
        , OrderType     varchar(65)                                    
        ,Form  varchar(35)                         
        ,Grade  varchar(35)                                    
        ,Size  varchar(35)                                    
        ,Finish  varchar(35)                                 
        ,CustPO   VARCHAR(30)                                 
        ,CustAdd VARCHAR(170)                                 
         ,SalesPersonIs Varchar(10)                                  
        ,SalesPersonOs Varchar(10)                                 
        ,NetAmt Decimal(20,2)                                
        ,NetPct Decimal(20,2)                                 
        ,CertType Varchar(max)                                
        ,RiskAssessment Varchar(50)                              
        ,ShpWhs Varchar(3)                             
        ,CustPart varchar(30)                        
        ,CusState Varchar(3)                                
        ,cusPCD varchar(10)                               
        ,CusCTY Varchar(3)                 
        ,ResWGT decimal(20,2)           
        ,ShipToCustName varchar(35)            
        ,ShipToCustID varchar(50)                       
                 );                                
        --         CREATE TABLE #tmp2 (  CompanyId varchar(5)                                     
        --,OrderPrefix Varchar(2)                             
        --,OrderNo varchar(10)                                    
        --,OrderItem   varchar(3)                              
        --,CertType Varchar(max) );                              
                                           
--CREATE TABLE #tmp1 (  CompanyId varchar(5)                                     
--        ,OrderPrefix Varchar(2)                                    
--        ,OrderNo varchar(10)                                    
--        ,OrderItem   varchar(3)                                    
--        ,Branch varchar(10)                                 
--        ,CustID   VARCHAR(10)                                     
--        , CustName     VARCHAR(65)                                    
--        , CustomerCat   VARCHAR(65)                                     
--         ,Currency  Varchar(3)                                  
--        , UKChargeValue    DECIMAL(20, 2)                                
--        , USChargeValue    DECIMAL(20, 2)                                      
--        , BalPcs  DECIMAL(20, 2)                                    
--        , BalWgt           DECIMAL(20, 2)                                  
--        , BalMsr    DECIMAL(20, 2)                                    
--        , StatusAction               varchar(2)                                    
--        , Salesperson     varchar(65)                                    
--        , DueDate    varchar(20)                                    
--        , ProductName   VARCHAR(100)                                  
--        , OrderType     varchar(65)                                    
--        ,Form  varchar(35)                                    
--        ,Grade  varchar(35)                                    
--        ,Size  varchar(35)                                    
--        ,Finish  varchar(35)                                   
--        ,CustPO   VARCHAR(30)                                 
--    ,CustAdd VARCHAR(170)                                 
--         ,SalesPersonIs Varchar(10)                                  
--        ,SalesPersonOs Varchar(10)                                 
--        ,NetAmt Decimal(20,2)     
--        ,NetPct Decimal(20,2)                               
--        ,RiskAssessment Varchar(50)                                
--        , ShpWhs  Varchar(3)                             
--        ,CustPart varchar(30)                             
-- );                                     
                                    
DECLARE @DatabaseName VARCHAR(35);                                    
DECLARE @Prefix VARCHAR(5);                                    
DECLARE @Name VARCHAR(15);                                    
DECLARE @CurrenyRate varchar(15);                                     
                                    
if @Market ='ALL'                                    
 BEGIN                                    
 set @Market = ''                                    
 END                                    
                                     
 if @Branch ='ALL'                                    
 BEGIN                            
 set @Branch = ''                                    
 END                                    
                                    
if @OrderType ='ALL'                                    
 BEGIN                                    
 set @OrderType = ''                                    
 END                                    
                                    
IF @DBNAME = 'ALL'                                    
 BEGIN                                    
 IF @version = '0'                                      
    BEGIN                                      
    DECLARE ScopeCursor CURSOR FOR                                      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                                      
   OPEN ScopeCursor;                                      
    END                                      
    ELSE                                      
    BEGIN                                      
    DECLARE ScopeCursor CURSOR FOR                                      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                       
   OPEN ScopeCursor;                                      
    END                                      
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                    
     WHILE @@FETCH_STATUS = 0                                    
       BEGIN                                    
        DECLARE @query nvarchar(max);                                       
      SET @DB= @Prefix --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'                                    
      IF (UPPER(@Prefix) = 'TW')                                            
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                            
    End                                            
    Else if (UPPER(@Prefix) = 'NO')                                            
    begin                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                            
    End                                            
    Else if (UPPER(@Prefix) = 'CA')                                            
    begin                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                            
    End                                            
    Else if (UPPER(@Prefix) = 'CN')                                            
    begin                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                                            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                                            
    begin                                            
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                            
    End                                            
    Else if(UPPER(@Prefix) = 'UK')                                            
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                            
    End                                       
    Else if(UPPER(@Prefix) = 'DE')                                            
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                            
    End                                       
      SET @query =                    
       'INSERT INTO #tmp ( CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat,SubMarketSegment, Currency, UKChargeValue, USChargeValue,BalPcs, BalMsr,                      
        BalWgt, StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd , SalesPersonIs, SalesPersonOs ,NetAmt,                       
        NetPct,CertType, RiskAssessment,ShpWhs,CustPart,CusState ,cusPCD,CusCTY,ResWGT,ShipToCustName,ShipToCustID)                                    
       select ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_ord_brh as Branch,                                    
       ord_sld_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as CustomerCat, sic_desc30 as SubMarketSegment ,                      
        orh_cry as currency ,(Case when orh_cry = ''USD'' then chl_chrg_val / '+ @CurrenyRate +' else chl_chrg_val end) as UKCharge,                          
       (Case when orh_cry IN (''GBP'',''TWD'',''CAD'',''RMB'') then chl_chrg_val * '+ @CurrenyRate +' else chl_chrg_val end) as ChargeValue,                        
   orl_bal_pcs as BalPcs,orl_bal_msr as BalMsr,'                                            
           if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                                               
             SET @query = @query + ' orl_bal_wgt * 2.20462 as BalWgt,'                                            
           ELSE                                            
            SET @query = @query + ' orl_bal_wgt as BalWgt, '                                            
                                                       
           SET @query = @query + ' ord_sts_actn as StatusAction ,usr_nm as Salesperson                                 
       ,orl_due_to_dt as DueDate,(select LTRIM(RTRIM(prm_frm)) + ''/'' + LTRIM(RTRIM(prm_grd)) + ''/'' + LTRIM(RTRIM(prm_size_desc)) + ''/'' + LTRIM(RTRIM(prm_fnsh))                               
       from ' + @DB + '_inrprm_rec where prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size= ipd_size and prm_fnsh = ipd_fnsh)   as ProductName,cds_desc                                    
       ,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)), LTRIM(RTRIM(ord_cus_po))                                 
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)) + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))                                       
       ,sis.slp_lgn_id, os.slp_lgn_id,oit_npft_avg_val * '+ @CurrenyRate +',                                  
       (Case ISNULL(cht_tot_val,0) When 0 then 0 else (ISNULL(oit_npft_avg_val,0)/ISNULL(cht_tot_val,0))*100 end ) as netpct,                              
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(itc_cert_typ )                                           
    FROM ' + @DB + '_tctitc_rec where itc_cmpy_id = ord_cmpy_id and itc_ref_pfx = ord_ord_pfx and itc_ref_no = ord_ord_no and itc_ref_itm = ord_ord_itm                                           
        FOR XML PATH('''')),1,1,'''')) AS CertType  ,                              
        (select top 1 ava_attr_val_var from ' + @DB + '_xctava_rec where ava_key_fld01_var = orh_cmpy_id and  ava_key_fld02_var = orh_ord_pfx                              
and ava_key_fld03_var = orh_ord_no and ava_attr = ''ORD-RS'' ) as riskassessment,orh_shpg_whs ,ipd_part,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty))                
 ,case when orh_ord_typ = ''N'' then (select sum(prd_ord_res_wgt) from ' + @DB + '_intprd_rec where prd_cmpy_id = ord_cmpy_id and                
  SUBSTRING(SUBSTRING(prd_ord_ffm,11,3), PATINDEX(''%[^0]%'', SUBSTRING(prd_ord_ffm,11,3)), 3) = ord_ord_itm and                
SUBSTRING(SUBSTRING(prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(prd_ord_ffm,3,8)), 8)  = ord_ord_no)                 
when orh_ord_typ = ''S'' then (select SUM(jso_prod_wgt) from ' + @DB + '_iptjso_rec where jso_cmpy_id = ord_cmpy_id and jso_ord_pfx = ord_ord_pfx             and jso_ord_no = ord_ord_no and jso_ord_itm = ord_ord_itm)                
 when orh_ord_typ = ''D'' then (select Sum(stn_rlvd_wgt) from ' + @DB + '_sahstn_rec where stn_cmpy_id = ord_cmpy_id and stn_ord_pfx = ord_ord_pfx                
 and stn_ord_no = ord_ord_no and stn_ord_itm = ord_ord_itm)                
 else                 
 (select sum(res_res_wgt) from ' + @DB + '_rvtres_rec where res_cmpy_id = ord_cmpy_id and res_ref_pfx = ord_ord_pfx and res_ref_no = ord_ord_no                
 and res_ref_itm = ord_ord_itm) end as resWGT ,xrh_shp_to_long_nm ,         
case when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''CA''        
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-Canada'' from CA_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))        
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''CN''        
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-China'' from CN_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))        
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''TW''        
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-Taiwan'' from TW_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))        
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''UK''        
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-UK'' from UK_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))        
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''US''        
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-USA'' from US_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))        
else ord_sld_cus_id        
end                                
       from ' + @DB + '_ortord_rec left join ' + @DB + '_xcrixc_rec on ixc_cmpy_id = ord_cmpy_id and ixc_cus_id = ord_sld_cus_id,' + @DB + '_arrcus_rec left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
	   left join ' + @DB + '_scrsic_rec on sic_sic = cus_sic 
       left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0                                 
         JOIN ' + @DB + '_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                                  
            LEFT JOIN ' + @DB + '_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                                  
        LEFT JOIN ' + @DB + '_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp ,                                    
       ' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec          
       left join ' + @DB + '_ortxrh_rec on xrh_cmpy_id = orl_cmpy_id and xrh_ord_pfx = orl_ord_pfx and xrh_ord_no = orl_ord_no          
       and xrh_ord_itm = orl_ord_itm and xrh_ord_rls_no = orl_ord_rls_no,          
       ' + @DB + '_tctipd_rec , ' + @DB + '_ortchl_rec                                    
       , ' + @DB + '_scrslp_rec ou,' + @DB + '_mxrusr_rec                                    
       , ' + @DB + '_rprcds_rec , ' + @DB + '_ortoit_rec , ' + @DB + '_ortcht_rec  where                                    
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id                                    
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id                                    
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                                    
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm                                    
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                                    
       and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id  and cds_cd=orh_ord_typ                                 
       and oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm                                 
       AND  ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm                              
        and cht_tot_typ = ''T''                                      
       and orl_bal_qty >= 0                                     
       and chl_chrg_cl= ''E''                                    
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''                                    
       and orh_sts_actn <> ''C''              
       and ord_ord_pfx <>''QT''                                     
       and (   (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )                                
       or  (orl_due_to_dt is null and orh_ord_typ =''J''))                                
       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                                    
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')                                    
       and (orh_ord_typ = '''+ @OrderType +''' or '''+ @OrderType +'''= '''')                              
       and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')                              
        and  ord_ord_brh not in (''SFS'')                                  
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                                    
                                    
     print(@query);                                 
        EXECUTE sp_executesql @query;                                  
                                      
       /*  declare @query1 nvarchar(4000);                              
        set @query1 = 'INSERT INTO #tmp2 ( CompanyId,OrderPrefix ,OrderNo,OrderItem,CertType)                              
        select itc_cmpy_id, itc_ref_pfx, itc_ref_no, itc_ref_itm,itc_cert_typ from ' + @DB + '_tctitc_rec                              
        '                              
           print(@query1);                                 
        EXECUTE sp_executesql @query1;                                
       insert into #tmp                              
         select CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, ChargeValue,BalPcs, BalMsr, BalWgt, StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd, SalesPersonIs,           
	SalesPersonOs,NetAmt, NetPct,                              
       (SELECT STUFF((SELECT Distinct ',' + RTRIM(itc_cert_typ )                                           
    FROM  ''@Prefix'' +_tctitc_rec where   itc_ref_pfx = OrderPrefix and itc_ref_no = OrderNo and itc_ref_itm = OrderItem                                           
        FOR XML PATH('')),1,1,'')) AS CertType   from #tmp1 */                              
                                      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                    
       END                                     
    CLOSE ScopeCursor;                                    
    DEALLOCATE ScopeCursor;             
  END                        
  ELSE                                    
     BEGIN             
    IF @version = '0'                                      
     BEGIN                                        
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')                                      
     END                                    
     ELSE                                    
     BEGIN                                    
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')                                      
     END                                    
                                         
     IF (UPPER(@DB) = 'TW')                                         
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                                            
    End                                            
    Else if (UPPER(@DB) = 'NO')                                            
    begin                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                                            
    End                                            
    Else if (UPPER(@DB) = 'CA')                                            
    begin                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                                            
    End                                            
 Else if (UPPER(@DB) = 'CN')                                            
    begin                                            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                                            
    End                                            
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                                          
    begin                                            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                                            
    End                                            
    Else if(UPPER(@DB) = 'UK')                                            
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                                            
    End                                      
    Else if(UPPER(@DB) = 'DE')                                            
    begin                                            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                                            
    End                                      
                                        
     SET @sqltxt ='INSERT INTO #tmp ( CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, SubMarketSegment, Currency, UKChargeValue, USChargeValue,BalPcs, BalMsr, BalWgt,  
     StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd , SalesPersonIs, SalesPersonOs ,NetAmt, NetPct,  
     CertType, RiskAssessment,ShpWhs,CustPart,CusState ,cusPCD,CusCTY,ResWGT,ShipToCustName,ShipToCustID)  
       select ord_cmpy_id as CompanyId, ord_ord_pfx as OrderPrefix ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,ord_ord_brh as Branch,  
       ord_sld_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as CustomerCat, sic_desc30 as SubMarketSegment ,  
        orh_cry as currency ,(Case when orh_cry = ''USD'' then chl_chrg_val / '+ @CurrenyRate +' else chl_chrg_val end) as UKCharge,  
       (Case when orh_cry IN (''GBP'',''TWD'',''CAD'',''RMB'') then chl_chrg_val * '+ @CurrenyRate +' else chl_chrg_val end) as ChargeValue,  
       orl_bal_pcs as BalPcs,orl_bal_msr as BalMsr,'  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                                                           
            SET @sqltxt = @sqltxt + ' orl_bal_wgt * 2.20462 as BalWgt,'                                            
           ELSE                                  
            SET @sqltxt = @sqltxt + ' orl_bal_wgt as BalWgt, '                                            
                                                       
           SET @sqltxt = @sqltxt + ' ord_sts_actn as StatusAction ,usr_nm as Salesperson   
       ,orl_due_to_dt as DueDate,(select LTRIM(RTRIM(prm_frm)) + ''/'' + LTRIM(RTRIM(prm_grd)) + ''/'' + LTRIM(RTRIM(prm_size_desc)) + ''/'' + LTRIM(RTRIM(prm_fnsh))  
       from ' + @DB + '_inrprm_rec where prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size= ipd_size and prm_fnsh = ipd_fnsh)   as ProductName,cds_desc   
       ,LTRIM(RTRIM(ipd_frm)) , LTRIM(RTRIM(ipd_grd)) , LTRIM(RTRIM(ipd_size)) , LTRIM(RTRIM(ipd_fnsh)), LTRIM(RTRIM(ord_cus_po))   
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city)) + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))   
       ,sis.slp_lgn_id, os.slp_lgn_id,oit_npft_avg_val * '+ @CurrenyRate +',   
       (Case ISNULL(cht_tot_val,0) When 0 then 0 else (ISNULL(oit_npft_avg_val,0)/ISNULL(cht_tot_val,0))*100 end ) as netpct ,   
       (SELECT STUFF((SELECT Distinct '','' + RTRIM(itc_cert_typ )   
    FROM ' + @DB + '_tctitc_rec where itc_cmpy_id = ord_cmpy_id and itc_ref_pfx = ord_ord_pfx and itc_ref_no = ord_ord_no and itc_ref_itm = ord_ord_itm   
        FOR XML PATH('''')),1,1,'''')) AS CertType  ,  
        (select top 1 ava_attr_val_var from ' + @DB + '_xctava_rec where ava_key_fld01_var = orh_cmpy_id and  ava_key_fld02_var = orh_ord_pfx   
 and ava_key_fld03_var = orh_ord_no and ava_attr = ''ORD-RS'' ) as riskassessment,orh_shpg_whs,ipd_part,LTRIM(RTRIM(cva_st_prov)),cva_pcd,LTRIM(RTRIM(cva_cty))   
 ,case when orh_ord_typ = ''N'' then (select sum(prd_ord_res_wgt) from ' + @DB + '_intprd_rec where prd_cmpy_id = ord_cmpy_id and   
  SUBSTRING(SUBSTRING(prd_ord_ffm,11,3), PATINDEX(''%[^0]%'', SUBSTRING(prd_ord_ffm,11,3)), 3) = ord_ord_itm and   
SUBSTRING(SUBSTRING(prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(prd_ord_ffm,3,8)), 8)  = ord_ord_no)   
when orh_ord_typ = ''S'' then (select SUM(jso_prod_wgt) from ' + @DB + '_iptjso_rec where jso_cmpy_id = ord_cmpy_id and jso_ord_pfx = ord_ord_pfx   
 and jso_ord_no = ord_ord_no and jso_ord_itm = ord_ord_itm)  
 when orh_ord_typ = ''D'' then (select Sum(stn_rlvd_wgt) from ' + @DB + '_sahstn_rec where stn_cmpy_id = ord_cmpy_id and stn_ord_pfx = ord_ord_pfx   
 and stn_ord_no = ord_ord_no and stn_ord_itm = ord_ord_itm)   
 else   
 (select sum(res_res_wgt) from ' + @DB + '_rvtres_rec where res_cmpy_id = ord_cmpy_id and res_ref_pfx = ord_ord_pfx and res_ref_no = ord_ord_no   
 and res_ref_itm = ord_ord_itm) end as resWGT ,xrh_shp_to_long_nm ,   
case when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''CA''   
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-Canada'' from CA_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))   
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''CN''   
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-China'' from CN_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))   
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''TW''   
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-Taiwan'' from TW_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))   
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''UK''   
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-UK'' from UK_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))   
when SUBSTRING(ixc_sllg_cmpy_id,0,3) = ''US''   
then (select MAX(LTRIM(RTRIM(cus_cus_id))) + ''-USA'' from US_arrcus_rec where cus_cmpy_id = ixc_sllg_cmpy_id and (cus_cus_long_nm = xrh_shp_to_long_nm or cus_cus_nm = xrh_shp_to_long_nm))   
else ord_sld_cus_id   
end                
       from ' + @DB + '_ortord_rec left join ' + @DB + '_xcrixc_rec on ixc_cmpy_id = ord_cmpy_id and ixc_cus_id = ord_sld_cus_id,        
       ' + @DB + '_arrcus_rec left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
	   left join ' + @DB + '_scrsic_rec on sic_sic = cus_sic 
       left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0                
        JOIN ' + @DB + '_arrshp_rec on cus_cus_id = shp_cus_id and cus_cmpy_id = shp_cmpy_id and shp_shp_to = 0                 
            LEFT JOIN ' + @DB + '_scrslp_rec os on os.slp_cmpy_id = shp_cmpy_id and os.slp_slp = shp_os_slp                             
        LEFT JOIN ' + @DB + '_scrslp_rec sis on sis.slp_cmpy_id = shp_cmpy_id and sis.slp_slp = shp_is_slp ,                 
       ' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec          
       left join ' + @DB + '_ortxrh_rec on xrh_cmpy_id = orl_cmpy_id and xrh_ord_pfx = orl_ord_pfx and xrh_ord_no = orl_ord_no          
       and xrh_ord_itm = orl_ord_itm and xrh_ord_rls_no = orl_ord_rls_no,          
       ' + @DB + '_tctipd_rec , ' + @DB + '_ortchl_rec                
       , ' + @DB + '_scrslp_rec ou,' + @DB + '_mxrusr_rec                 
       , ' + @DB + '_rprcds_rec , ' + @DB + '_ortoit_rec, ' + @DB + '_ortcht_rec  where                
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id                 
       and ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id                 
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no                 
       and ipd_cmpy_id = orl_cmpy_id and ipd_ref_pfx = orl_ord_pfx  and ipd_ref_no = orl_ord_no  and ipd_ref_itm = orl_ord_itm                 
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id                  
       and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id and cds_cd=orh_ord_typ                 
       and oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm AND                
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm                
        and cht_tot_typ = ''T''                 
       and orl_bal_qty >= 0                                 
       and chl_chrg_cl= ''E''                
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''                
       and orh_sts_actn <> ''C''                
       and ord_ord_pfx <>''QT''                 
       and  (                 
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )                
       or                
       (orl_due_to_dt is null and orh_ord_typ =''J''))                
       and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')                
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')                
and (orh_ord_typ = '''+ @OrderType +''' or '''+ @OrderType +'''= '''')                
       and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')                
       and  ord_ord_brh not in (''SFS'')                
       order by ord_ord_no,ord_ord_itm,orl_due_to_dt'                                    
       print('test')  ;                                
print(@sqltxt);                                     
    set @execSQLtxt = @sqltxt;                                     
       EXEC (@execSQLtxt);                                    
  --and ord_ord_pfx=''SO''                                     
     END                                    
   SELECT distinct CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat, SubMarketSegment, Currency, UKChargeValue, USChargeValue,sum(BalPcs) as BalPcs,             
   sum(BalMsr) as BalMsr, SUM(BalWgt) as BalWgt,                
     StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd , SalesPersonIs, SalesPersonOs ,NetAmt, NetPct,                
     CertType, RiskAssessment,ShpWhs,CustPart,CusState ,cusPCD,CusCTY,ResWGT,ShipToCustName,ShipToCustID FROM #tmp             
     group by CompanyId,OrderPrefix ,OrderNo,OrderItem,Branch ,CustID,CustName,CustomerCat,SubMarketSegment, Currency, UKChargeValue, USChargeValue,               
     StatusAction, Salesperson, DueDate, ProductName,OrderType,Form,Grade,Size,Finish, CustPO, CustAdd , SalesPersonIs, SalesPersonOs ,NetAmt, NetPct,                
     CertType, RiskAssessment,ShpWhs,CustPart,CusState ,cusPCD,CusCTY,ResWGT ,ShipToCustName ,ShipToCustID           
     order by CustID                 
--   where OrderNo in (228465,                
--231531,                
--251668,                
--252779,                
--255838,               
--258651,                
--72698,                
--255014,                
--260488)                
                
   --Where NetPct < -100                                 
                                    
END                                    
                                    
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market  varchar(65),@Branch varchar(10),@OrderType varchar(10),@version char = '0',@DateRange int = 0, @CustomerID varchar(10) = ''                       
                                    
-- exec [sp_itech_OpenOrderCertType_V1] '12/01/2019', '12/31/2019' , 'US','ALL','ALL','ALL'   --TW-59 UK-52  ,1 US-1398             
-- exec [sp_itech_OpenOrderCertType_V2] '12/01/2019', '12/31/2019' , 'TW','ALL','ALL','ALL'   --TW-59 UK-23  ,1 US-1403                          
-- exec [sp_itech_OpenOrderCertType_V1] '05/01/2017', '05/31/2017' , 'ALL','ALL','ALL','ALL'  
-- exec sp_itech_OpenOrderCertType_V3 '10/01/2020', '10/22/2020' , 'UK','ALL','ALL','ALL' 
 -- ;-- 582 res_res_wgt;-- orh_ord_typ ='J' mathced with res_ord_typ                
 
/*                               
                              
date : 2016-12-20                              
MAil sub: CCS / CAA                              
                              
date: 20170606                              
mail Sub:Report Modification                              
                              
date:20170907                              
Mail Sub:Report Anomaly: Need your Help                              
                              
Date: 20170928                         
Mail sub:Small Mod Request                       
                      
Date:20191021                      
Mail sub:  Home > STRATIXReports > Development > Open Order Report v2 09-2017 - BHM Inquiry as of October 14, 2019                     
                    
Date:20191022                    
Mail Sub:STRATIXReports > Branch - BHM  & Branch - IND  Update Request           
          
Date:20200203          
Mail sub:Open Order Report / Interco Orders Update Request 
20201022	Sumit
Mail: RE: Report Modification Request
add sic_desc30 as SubMarketSegment
*/
GO
