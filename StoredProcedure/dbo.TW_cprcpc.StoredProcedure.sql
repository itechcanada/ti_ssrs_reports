USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_cprcpc]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 23, 2014,>  
-- 
  
-- =============================================  
create PROCEDURE [dbo].[TW_cprcpc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_cprcpc_rec', 'U') IS NOT NULL  
  drop table dbo.TW_cprcpc_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_cprcpc_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[cprcpc_rec] ;   
    
END  
-- select * from TW_cprcpc_rec
GO
