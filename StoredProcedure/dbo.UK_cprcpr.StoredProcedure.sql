USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_cprcpr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,Mrinal>    
-- Create date: <Create Date,Sep 29, 2014,>    
--   
    
-- =============================================    
CREATE PROCEDURE [dbo].[UK_cprcpr]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.UK_cprcpr_rec', 'U') IS NOT NULL    
  drop table dbo.UK_cprcpr_rec;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.UK_cprcpr_rec    
  from [LIVEUKSTX].[liveukstxdb].[informix].cprcpr_rec ;     
      
END    
-- select * from UK_cprcpr_rec
GO
