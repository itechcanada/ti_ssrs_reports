USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_xcrixc]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Author,Mukesh>          
-- Create date: <Create Date,2020-02-04,>          
-- Description: <Description,Open Orders,>          
          
-- =============================================          
CREATE PROCEDURE [dbo].[CA_xcrixc]          
           
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
IF OBJECT_ID('dbo.CA_xcrixc_rec', 'U') IS NOT NULL                    
  drop table dbo.CA_xcrixc_rec;              
          
    -- Insert statements for procedure here     
    select *  
    
into dbo.CA_xcrixc_rec      
FROM [LIVECASTX].[livecastxdb].[informix].[xcrixc_rec]          
          
END   
-- select * from CA_xcrixc_rec;
GO
