USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GL]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <19 Nov 2015>        
-- Description: <Getting GL>        
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_GL] @DBNAME varchar(50) 
        
AS        
BEGIN    


    
SET NOCOUNT ON;        
        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10) ;           
declare @TD varchar(10)  ;  
DECLARE @CurrenyRate varchar(15);   
  --set @FD = CONVERT(VARCHAR(10), @FromDate , 120);            
  --set @TD = CONVERT(VARCHAR(10), @ToDate , 120) ;       
CREATE TABLE #tmp (   CompanyName   VARCHAR(10)        
        , BscGLAcc     VARCHAR(10)        
        , Sacct    Varchar(35)        
        , DrAmt    Decimal(20,2)        
        , CrAmt    Decimal(20,2)    
        , BudgetCRAmt    Decimal(20,2)   
        , BudgetDRAmt    Decimal(20,2)        
                 );        
                     
CREATE TABLE #tmpCR (      
         BudgetCRAmt    Decimal(20,2)   
        , BudgetDRAmt    Decimal(20,2)        
                 );                          
DECLARE @company VARCHAR(35);        
DECLARE @prefix VARCHAR(15);         
DECLARE @DatabaseName VARCHAR(35);          
               
               
 IF @DBNAME = 'ALL'        
 BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName, company,prefix from tbl_itech_DatabaseName        
    OPEN ScopeCursor;        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query NVARCHAR(4000);    
         IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
             
       SET @query = 'INSERT INTO #tmp (CompanyName, BscGLAcc, Sacct, DrAmt, CrAmt,BudgetCRAmt,BudgetDRAmt)  
     select  ''' + @prefix + ''' , ojd_bsc_gl_acct, ojd_sacct, ojd_dr_amt, ojd_cr_amt,bud_cr_amt_' + convert(varchar(2),MONTH(getdate())) + ',  
     bud_dr_amt_' + convert(varchar(2),MONTH(getdate())) + '  
      from '+ @prefix +'_gltojd_rec   
      join '+ @prefix +'_glbbud_rec on bud_bsc_gl_acct = ojd_bsc_gl_acct and bud_sacct = ojd_sacct and bud_lgr_id = ojd_lgr_id   
      and  bud_fis_yr = ' + convert(varchar(4),YEAR(getdate())) + '  
      order by ojd_bsc_gl_acct   '        
          print @query;  
       EXECUTE sp_executesql @query;        
                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
             
     Set @prefix= @DBNAME    
     IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
           
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName, BscGLAcc, Sacct, DrAmt, CrAmt,BudgetCRAmt,BudgetDRAmt)  
     select  ''' + @prefix + ''' , ojd_bsc_gl_acct, ojd_sacct, ojd_dr_amt, ojd_cr_amt,bud_cr_amt_' + convert(varchar(2),MONTH(getdate())) + ',  
     bud_dr_amt_' + convert(varchar(2),MONTH(getdate())) + '  
      from '+ @prefix +'_gltojd_rec   
      join '+ @prefix +'_glbbud_rec on bud_bsc_gl_acct = ojd_bsc_gl_acct and bud_sacct = ojd_sacct and bud_lgr_id = ojd_lgr_id   
      and  bud_fis_yr = ' + convert(varchar(4),YEAR(getdate())) + '  
      order by ojd_bsc_gl_acct   
      '    
              
    print(@sqltxt)        
   set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
         
   END  
insert into #tmpCR (BudgetCRAmt) select distinct BudgetCRAmt from #tmp -- where BscGLAcc = 4010   and Sacct in  (100000000000000000000000000000,600000000000000000000000000000) ;
insert into #tmpCR (BudgetDRAmt) select distinct BudgetDRAmt from #tmp -- where BscGLAcc = 4010   and Sacct in  (100000000000000000000000000000,600000000000000000000000000000) ;   
         
select * , (select Sum(  BudgetCRAmt) from #tmpCR) as BudgetCRAmtFinal, (select Sum(BudgetDRAmt) from #tmpCR) as BudgetDRAmtFinal from #tmp -- where BscGLAcc = 4010   and Sacct in  (100000000000000000000000000000,600000000000000000000000000000) ;
-- select Sum(  BudgetCRAmt), Sum(BudgetDRAmt) from #tmpCR ;
drop table #tmp  ;
drop table #tmpCR ;     
        
        
END        
        
-- exec [sp_itech_GL]  'US'    
        
GO
