USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_inrpti]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Sep 15, 2016,>    
-- Description: <Description,Excess inventory,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CN_inrpti]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.CN_inrpti_rec', 'U') IS NOT NULL    
  drop table dbo.CN_inrpti_rec ;     
    
     
    -- Insert statements for procedure here    
SELECT *   
into  dbo.CN_inrpti_rec    
  from [LIVECNSTX].[livecnstxdb].[informix].[inrpti_rec] ;    
      
END


-- Select * from CN_inrpti_rec
GO
