USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[SALES_CALLS_DAILY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,6/28/2012>
-- Description:	<Description,To track daily total calls from sales representives >
-- =============================================
CREATE PROCEDURE [dbo].[SALES_CALLS_DAILY]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, b1.slp_lgn_id AS inside_sales,b2.slp_lgn_id AS outside_sales, b.cmk_rvw_dt
--INTO #USA
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrcus_rec] a
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcmk_rec] b ON a.cus_cus_id=b.cmk_cus_id
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrshp_rec] c1 ON a.cus_cus_id=c1.shp_cus_id
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[scrslp_rec] b1 on c1.shp_is_slp=b1.slp_slp
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[scrslp_rec] b2 on c1.shp_os_slp=b2.slp_slp
--WHERE CAST(b.cmk_rvw_dt AS datetime)= CONVERT (date, GETDATE()-1)
WHERE
b.cmk_rvw_dt is not null

--SELECT * FROM #USA
/*
UNION
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, b1.slp_lgn_id AS inside_sales,b2.slp_lgn_id AS outside_sales, b.cmk_rvw_dt
FROM [LIVEUK_IW].[liveukstxdb_iw].[informix].[arrcus_rec] a
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[arrcmk_rec] b ON a.cus_cus_id=b.cmk_cus_id
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[arrshp_rec] c1 ON a.cus_cus_id=c1.shp_cus_id
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[scrslp_rec] b1 on c1.shp_is_slp=b1.slp_slp
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[scrslp_rec] b2 on c1.shp_os_slp=b2.slp_slp
WHERE CAST(b.cmk_rvw_dt AS datetime)= CONVERT (date, GETDATE()-1)
UNION
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, b1.slp_lgn_id AS inside_sales,b2.slp_lgn_id AS outside_sales, b.cmk_rvw_dt
FROM [LIVECA_IW].[livecastxdb_iw].[informix].[arrcus_rec] a
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[arrcmk_rec] b ON a.cus_cus_id=b.cmk_cus_id
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[arrshp_rec] c1 ON a.cus_cus_id=c1.shp_cus_id
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[scrslp_rec] b1 on c1.shp_is_slp=b1.slp_slp
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[scrslp_rec] b2 on c1.shp_os_slp=b2.slp_slp
WHERE CAST(b.cmk_rvw_dt AS datetime)= CONVERT (date, GETDATE()-1)
UNION
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, b1.slp_lgn_id AS inside_sales,b2.slp_lgn_id AS outside_sales, b.cmk_rvw_dt
FROM [LIVETW_IW].[livetwstxdb_iw].[informix].[arrcus_rec] a
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[arrcmk_rec] b ON a.cus_cus_id=b.cmk_cus_id
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[arrshp_rec] c1 ON a.cus_cus_id=c1.shp_cus_id
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[scrslp_rec] b1 on c1.shp_is_slp=b1.slp_slp
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[scrslp_rec] b2 on c1.shp_os_slp=b2.slp_slp
WHERE CAST(b.cmk_rvw_dt AS datetime)= CONVERT (date, GETDATE()-1)
UNION
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, b1.slp_lgn_id AS inside_sales,b2.slp_lgn_id AS outside_sales, b.cmk_rvw_dt
FROM [LIVENO_IW].[livenostxdb_iw].[informix].[arrcus_rec] a
INNER JOIN [LIVENO_IW].[livenostxdb_iw].[informix].[arrcmk_rec] b ON a.cus_cus_id=b.cmk_cus_id
INNER JOIN [LIVENO_IW].[livenostxdb_iw].[informix].[arrshp_rec] c1 ON a.cus_cus_id=c1.shp_cus_id
INNER JOIN [LIVENO_IW].[livenostxdb_iw].[informix].[scrslp_rec] b1 on c1.shp_is_slp=b1.slp_slp
INNER JOIN [LIVENO_IW].[livenostxdb_iw].[informix].[scrslp_rec] b2 on c1.shp_os_slp=b2.slp_slp
WHERE CAST(b.cmk_rvw_dt AS datetime)= CONVERT (date, GETDATE()-1)

*/

--DROP TABLE #USA
END
GO
