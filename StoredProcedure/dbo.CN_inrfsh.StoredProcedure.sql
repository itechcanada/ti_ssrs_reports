USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_inrfsh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  itech  
-- Create date:April 4, 2013  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_inrfsh]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.CN_inrfsh_rec', 'U') IS NOT NULL  
  drop table dbo.CN_inrfsh_rec;  
      
          
SELECT *  
into  dbo.CN_inrfsh_rec  
FROM [LIVECNSTX].[livecnstxdb].[informix].[inrfsh_rec];  
  
END  
  
-- select * from CN_inrfsh_rec
GO
