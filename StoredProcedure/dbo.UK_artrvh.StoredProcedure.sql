USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_artrvh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UK_artrvh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_artrvh_rec', 'U') IS NOT NULL  
  drop table dbo.UK_artrvh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT  rvh_cmpy_id,rvh_ar_pfx,rvh_ar_no,rvh_cr_ctl_cus_id,rvh_sld_cus_id	,rvh_extl_ref,'' as rvh_desc30,rvh_rsn_typ,rvh_rsn,rvh_rsn_ref,rvh_job_id,rvh_cry,rvh_exrt,rvh_ex_rt_typ,rvh_pttrm,rvh_disc_trm,rvh_due_nbr_dy,rvh_disc_nbr_dy,rvh_disc_rt,rvh_due_dt,rvh_disc_dt,rvh_disc_amt,rvh_ar_brh,rvh_upd_ref_no,rvh_upd_dt,rvh_upd_ref,rvh_inv_dt,rvh_flwp_dt,rvh_orig_amt,rvh_ip_amt,rvh_balamt,rvh_arch_trs,rvh_arch_dt,rvh_sls_qlf   
into  dbo.UK_artrvh_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[artrvh_rec] ;   
    
END  
GO
