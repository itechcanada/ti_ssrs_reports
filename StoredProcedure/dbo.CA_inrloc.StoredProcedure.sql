USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_inrloc]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 7, 2015,>
-- Description:	<Description,Material,>

-- =============================================
CREATE PROCEDURE [dbo].[CA_inrloc]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_inrloc_rec', 'U') IS NOT NULL
		drop table dbo.CA_inrloc_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CA_inrloc_rec
  from [LIVECASTX].[livecastxdb].[informix].[inrloc_rec] ; 
  
END
-- select * from CA_inrloc_rec
GO
