USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_AM_Report_DE_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                              
-- Author:  <Sumit >                              
-- Create date: <12 Nov 2020>                              
-- Description: <Germany Budget and Forecast>      
-- =============================================                            
CREATE PROCEDURE [dbo].[sp_itech_AM_Report_DE_V1] @RunMonth varchar(1)                             
AS                              
BEGIN                              
SET NOCOUNT ON;                              
                              
declare @sqltxt varchar(6000)                              
declare @execSQLtxt varchar(7000)                              
declare @DB varchar(100)           
  
declare @FDYTD varchar(10)                              
declare @FDMTD varchar(10)                             
declare @FDMTDPM varchar(10)  -- from date of previous month                            
declare @TDYTD varchar(10)                    
declare @PTDYTD varchar(10)  -- Previous date from current date                            
declare @TDYTDPM varchar(10)   --To Days of previous month                            
declare @DaysYTD varchar(4)                            
declare @DaysYTDPM varchar(4)  --Days up to previous month                            
declare @DaysMTD varchar(2)                            
declare @DaysMTDPM varchar(2)  --Total Days of previous month                            
declare @LastDateOfCurrentMonth varchar(10)                            
                            
declare @ForecastMonth varchar (10)                            
declare @ForecastMonthPM varchar (10)                            
                              
set @DaysYTD = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',CONVERT(varchar(10),getdate(),120))                              
set @FDYTD = CONVERT(VARCHAR(10), convert(varchar(4), YEAR(getdate())) + '-01-01', 120)                                 
Set @FDMTD = CONVERT(varchar(7), getdate(),126) + '-01'                              
set @TDYTD = CONVERT(VARCHAR(10), getdate(), 120)                   
--set @TDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -2),120);  For testing 20200406                 
set @PTDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -1),120);                                
--set @PTDYTD = CONVERT(Varchar(10), DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -3),120); For testing 20200406        
set @DaysMTD = DATEDIFF(day,@FDMTD,GETDATE())                            
set @ForecastMonth = convert(varchar(7),GetDate(), 126)                            
Set @LastDateOfCurrentMonth = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)), 120)                            
                            
if @RunMonth = 1                            
Begin                            
 set @FDMTD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 1, 0) , 120)   --First day of previous 13 month                            
 set @TDYTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month                            
 set @DaysYTD = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',@TDYTD)                             
 set @ForecastMonth =  convert(varchar(7),@FDMTD, 126)                            
 set @DaysMTD = dbo.DATEDIFF_WorkingDaysOnly(@FDMTD, @TDYTD)                            
End                            
ELSE                            
BEgin                            
 set @DaysMTD = dbo.DATEDIFF_WorkingDaysOnly(@FDMTD, GETDATE())                            
End                            
                            
-- For previous month                            
SET @TDYTDPM = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@TDYTD),0)), 120)                              
SET @DaysYTDPM = DATEDIFF(day,convert(varchar(4), YEAR(getdate())) + '-01-01',@TDYTDPM)                             
SET @FDMTDPM = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @FDMTD) - 1, 0) , 120)                            
SET @DaysMTDPM = dbo.DATEDIFF_WorkingDaysOnly(@FDMTDPM, @TDYTDPM)                            
SET @ForecastMonthPM =  convert(varchar(7),@FDMTDPM, 126)                            
  
SET @sqltxt = 'SELECT Budget, Forecast, GPBudget, GPForecast,(Case ISNULL(Budget,0) When 0 then 0 else (ISNULL(GPBudget,0)/ Budget)*100 end )AS GPPctBdgYear, GPForecastPct as GPPctForecast,
	LBSShippedPlan, BRANCH,''DE'' as Databases FROM tbl_itech_Forecast_AM  Where Databases = ''DE'' and YearMonth =  ''' +  @ForecastMonth + ''' and BRANCH = ''DE'''                            
  print(@sqltxt);                             
  set @execSQLtxt = @sqltxt;                             
  EXEC (@execSQLtxt);                             
                        
END                  
          
/*          
     
          
*/
GO
