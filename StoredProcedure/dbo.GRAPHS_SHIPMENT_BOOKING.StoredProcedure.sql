USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[GRAPHS_SHIPMENT_BOOKING]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/6/2012,>
-- Description:	<Description,Total shipment, bookings for last 13 months by customer category>
-- =============================================
CREATE PROCEDURE [dbo].[GRAPHS_SHIPMENT_BOOKING]
	-- Add the parameters for the stored procedure here
	@EndDate datetime,
	@Category varchar(30),
	@USD_UK float,
	@USD_TW float,
	@USD_NO float  ---GRAPHS_SHIPMENT_BOOKING '2012-07-31','Medical',1.4,32,0.165
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 CREATE TABLE #TOTAL
(
item varchar(30),
category varchar(25),
acct_date varchar(15),
value float
)


INSERT INTO #TOTAL
SELECT 'LB Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_blg_wgt)
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT '$ Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val)*1
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION

SELECT 'LB Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_blg_wgt)*(2.2)
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT '$ Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val)*(@USD_UK)
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT 'LB Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_blg_wgt)
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT '$ Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val)*1
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX'
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30

UNION
SELECT 'LB Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_blg_wgt)
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT '$ Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val)/(@USD_TW)
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30

UNION
SELECT 'LB Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_blg_wgt)*(2.2)
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT '$ Shipped', c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val)* (@USD_NO)
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30


/*Calculate GP%*/
SELECT 'GP%' AS title,c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff
INTO #GP
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT 'GP%' AS title,c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT 'GP%' AS title,c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE'
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30
UNION
SELECT 'GP%' AS title,c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30

UNION
SELECT 'GP%' AS title,c.cuc_desc30, CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS TMP
,SUM(b.sat_tot_val) AS sell_freight,SUM(b.sat_tot_val)-SUM(b.sat_tot_avg_val) as diff
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 12
GROUP BY CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE),c.cuc_desc30


/*BOOKINGS*/
INSERT INTO #TOTAL
SELECT '$ Booked', c.cuc_desc30,
CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,
SUM(a.mbk_tot_mtl_val)	
FROM [US_ortmbk_rec] a
INNER JOIN [US_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [US_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND c.cuc_desc30=@Category
	  AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12 	
GROUP BY  CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) ,c.cuc_desc30 	    
UNION
SELECT '$ Booked', c.cuc_desc30,
CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,
SUM(a.mbk_tot_mtl_val)*(@USD_UK)
FROM [UK_ortmbk_rec] a
INNER JOIN [UK_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND c.cuc_desc30=@Category
	  AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12 	
GROUP BY  CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE),c.cuc_desc30 	
	    	  
UNION
SELECT '$ Booked', c.cuc_desc30,
CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,
SUM(a.mbk_tot_mtl_val)		
FROM [CA_ortmbk_rec] a
INNER JOIN [CA_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND c.cuc_desc30=@Category
	  AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12 	
GROUP BY  CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) ,c.cuc_desc30 	 
	  	  
UNION
SELECT '$ Booked', c.cuc_desc30,
CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,
SUM(a.mbk_tot_mtl_val)/(@USD_TW)	
FROM [TW_ortmbk_rec] a
INNER JOIN [TW_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND c.cuc_desc30=@Category
	  AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12 	
GROUP BY  CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) ,c.cuc_desc30 	 
UNION
SELECT '$ Booked', c.cuc_desc30,
CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) AS TMP,
SUM(a.mbk_tot_mtl_val)* (@USD_NO)	
FROM [NO_ortmbk_rec] a
INNER JOIN [NO_arrcus_rec] b ON a.mbk_sld_cus_id=b.cus_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON b.cus_cus_cat=c.cuc_cus_cat
WHERE a.mbk_ord_pfx='SO' 
	  AND a.mbk_ord_itm<>999 
	  AND a.mbk_wgt > 0 
	  AND c.cuc_desc30=@Category
	  AND DATEDIFF(MONTH,CAST(a.mbk_actvy_dt AS datetime),@EndDate) BETWEEN 0 AND 12 	
GROUP BY  CAST(DATENAME(MONTH,a.mbk_actvy_dt) + ' ' + DATENAME(YEAR,a.mbk_actvy_dt) AS DATE) ,c.cuc_desc30 

/*New and Lost Accts*/
INSERT INTO #TOTAL
SELECT  'New Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_frst_sls_dt)
FROM [US_arrcus_rec] a
INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND DATEDIFF(MONTH,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND c.cuc_desc30=@Category
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

INSERT INTO #TOTAL
SELECT 'Lost Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_lst_sls_dt)
FROM [US_arrcus_rec] a
	INNER JOIN [US_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
	INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

/*UK*/	
INSERT INTO #TOTAL
SELECT  'New Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_frst_sls_dt)
FROM [UK_arrcus_rec] a
INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND c.cuc_desc30=@Category
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

INSERT INTO #TOTAL
SELECT 'Lost Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_lst_sls_dt)
FROM [UK_arrcus_rec] a
	INNER JOIN [UK_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
	INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

/*CANADA*/	
INSERT INTO #TOTAL
SELECT  'New Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_frst_sls_dt)
FROM [CA_arrcus_rec] a
INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND c.cuc_desc30=@Category
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

INSERT INTO #TOTAL
SELECT 'Lost Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_lst_sls_dt)
FROM [CA_arrcus_rec] a
	INNER JOIN [CA_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
	INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC
/*TAIWAN*/
INSERT INTO #TOTAL
SELECT  'New Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_frst_sls_dt)
FROM [TW_arrcus_rec] a
INNER JOIN [TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND c.cuc_desc30=@Category
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

INSERT INTO #TOTAL
SELECT 'Lost Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_lst_sls_dt)
FROM [TW_arrcus_rec] a
	INNER JOIN [TW_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
	INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

/*NORWAY*/
INSERT INTO #TOTAL
SELECT  'New Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_frst_sls_dt)
FROM [NO_arrcus_rec] a
INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND DATEDIFF(month,CAST(b.coc_frst_sls_dt AS datetime),@EndDate) BETWEEN 0 AND 12
AND c.cuc_desc30=@Category
GROUP BY DATENAME(MONTH,b.coc_frst_sls_dt)+' '+DATENAME(YEAR,b.coc_frst_sls_dt),CAST(DATENAME(MONTH,b.coc_frst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_frst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC

INSERT INTO #TOTAL
SELECT 'Lost Accounts',c.cuc_desc30,
CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE) AS TMP,
COUNT(b.coc_lst_sls_dt)
FROM [NO_arrcus_rec] a
	INNER JOIN [NO_arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
	INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE
a.cus_actv=1
AND c.cuc_desc30=@Category
AND DATEDIFF(MONTH,CAST(b.coc_lst_sls_dt AS datetime),@EndDate) BETWEEN 17 AND 29
GROUP BY DATENAME(MONTH,b.coc_lst_sls_dt)+' '+DATENAME(YEAR,b.coc_lst_sls_dt),CAST(DATENAME(MONTH,b.coc_lst_sls_dt) + ' ' + DATENAME(YEAR,b.coc_lst_sls_dt) AS DATE)
,c.cuc_desc30
ORDER BY TMP ASC
	
	
	

CREATE TABLE #FINAL
(
item varchar(30),
category varchar(25),
acct_date varchar(15),
value float
)


INSERT INTO #FINAL
SELECT title, cuc_desc30, TMP, SUM(diff)/SUM(sell_freight)*100
FROM #GP
WHERE cuc_desc30=@Category
GROUP BY title, cuc_desc30,TMP
UNION
SELECT item,category,acct_date,SUM(value) 
FROM  #TOTAL 
WHERE category=@Category
GROUP BY item, category, acct_date 


SELECT * FROM #FINAL

DROP TABLE #TOTAL
DROP TABLE #GP
DROP TABLE #FINAL
END

GO
