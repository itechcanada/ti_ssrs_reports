USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerList]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <11 NOV 2013>        
-- Description: <Getting top 50 customers for SSRS reports>        
-- Last changed date <18 Feb 2015>      
-- Last changed description : add the joining to get cus_adm_brh and apply filter     
-- Last change Date: 29 Jun 2015  
-- Last changes By: Mukesh  
-- Last changes Desc: Remove the live connection of database   
-- Last change Date: 20 Jan 2016  
-- Last changes By: Mukesh  
-- Last changes Desc: Include interco option in filter   
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_CustomerList]  @DBNAME varchar(50), @Cus_Ven_Type char, @Addr_typ char, @version char = '0', @CusAdminBrh Varchar(3) = 'ALL', @MktSgm Varchar(30) = 'ALL', @IncludeInterco char = '0'         
        
AS        
BEGIN        
  
  
 SET NOCOUNT ON;        
declare @sqltxt1 varchar(8000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
DECLARE @IsExcInterco char(1)  
set @DB=  @DBNAME        
        
CREATE TABLE #tmp (         
     [Database]   VARCHAR(10)        
     , cus_ven_typ  VARCHAR(1)        
     , cus_ven_id  VARCHAR(10)        
     , addr_typ   VARCHAR(1)        
     , nm1    VARCHAR(50)        
     , nm2    VARCHAR(50)        
     , addr1    VARCHAR(50)        
     , addr2    VARCHAR(50)        
     , addr3    VARCHAR(50)        
     , city    VARCHAR(50)        
     , PCD    VARCHAR(15)        
     , cty    VARCHAR(3)        
     , st_prov   VARCHAR(2)        
     , transp_zn   VARCHAR(10)        
     , tel_area_cd  VARCHAR(10)        
     , tel_no   VARCHAR(15)        
     , fax_area_cd    VARCHAR(10)        
     , fax_no   VARCHAR(15)        
     , cusAdmBrh Varchar(3)     
     , mktSgm Varchar(55)     
                 );         
        
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @Prefix VARCHAR(35);        
DECLARE @Name VARCHAR(15);       
IF @CusAdminBrh =  'ALL'      
Begin      
set @CusAdminBrh = ''      
End      
IF @MktSgm =  'ALL'      
Begin      
set @MktSgm = ''      
End        
  
if (@IncludeInterco = '0')  
BEGIN  
set @IsExcInterco ='T'  
END     
        
IF @DBNAME = 'ALL'        
 BEGIN        
  IF @version = '0'        
  BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName        
    OPEN ScopeCursor;        
  END        
  ELSE        
  BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
  END        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
        DECLARE @query1 NVARCHAR(max);     
                  
      SET @DB= @Prefix         
       SET @query1 ='INSERT INTO #tmp ([Database], cus_ven_typ, cus_ven_id, addr_typ, nm1, nm2, addr1, addr2, addr3, city, PCD, cty, st_prov        
        , transp_zn, tel_area_cd, tel_no, fax_area_cd, fax_no, cusAdmBrh, mktSgm)        
                           
                   SELECT  ''' +  @DB + ''' as [Database],        
         cva_cus_ven_typ,        
         cva_cus_ven_id,        
         cva_addr_typ,        
         cva_nm1,        
         cva_nm2,        
         cva_addr1,        
         cva_addr2,        
         cva_addr3,        
         cva_city,        
         cva_PCD,        
         cva_cty,        
         cva_st_prov,        
         cva_transp_zn,        
         cva_tel_area_cd,        
         cva_tel_no,        
         cva_fax_area_cd,        
         cva_fax_no,      
         cus_admin_brh,    
         cuc_desc30        
         from  ' + @DB + '_scrcva_rec      
         left join ' + @DB + '_arrcus_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id     
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
         Where (cuc_desc30 = ''' + @MktSgm + ''' OR '''' = ''' + @MktSgm + ''') and cva_addr_typ = ''' + @Addr_typ + '''  ;'        
       print @query1;        
        EXECUTE sp_executesql @query1;         
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN      
SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')        
             
        SET @sqltxt1 ='INSERT INTO #tmp ([Database], cus_ven_typ, cus_ven_id, addr_typ, nm1, nm2, addr1, addr2, addr3, city, PCD, cty, st_prov        
        , transp_zn, tel_area_cd, tel_no, fax_area_cd, fax_no,cusAdmBrh, mktSgm)        
                           
                   SELECT  ''' +  @DB + ''' as [Database],        
         cva_cus_ven_typ,        
         cva_cus_ven_id,        
         cva_addr_typ,        
         cva_nm1,        
         cva_nm2,        
         cva_addr1,        
         cva_addr2,        
         cva_addr3,        
         cva_city,        
         cva_PCD,        
         cva_cty,        
         cva_st_prov,        
         cva_transp_zn,        
         cva_tel_area_cd,        
         cva_tel_no,        
         cva_fax_area_cd,        
         cva_fax_no ,      
         cus_admin_brh,    
         cuc_desc30       
         from  ' + @DB + '_scrcva_rec         
         left join ' + @DB + '_arrcus_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id     
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
         Where (cuc_desc30 = ''' + @MktSgm + ''' OR '''' = ''' + @MktSgm + ''') and cva_addr_typ = ''' + @Addr_typ + '''  ;'        
           
     print(@sqltxt1)         
   EXEC (@sqltxt1);        
   END        
  
if @IsExcInterco ='T'  
 BEGIN           
select [Database], cus_ven_typ, cus_ven_id, addr_typ, nm1, nm2, addr1, addr2, addr3, city, PCD, cty, st_prov        
        , transp_zn, tel_area_cd, tel_no, fax_area_cd, fax_no,cusAdmBrh, mktSgm        
                     
 from #tmp  where cus_ven_typ = @Cus_Ven_Type  and (cusAdmBrh = @CusAdminBrh or @CusAdminBrh = '') and mktSgm <> 'Interco' order by cus_ven_id      
 END  
 ELSE  
 BEGIN  
 select [Database], cus_ven_typ, cus_ven_id, addr_typ, nm1, nm2, addr1, addr2, addr3, city, PCD, cty, st_prov        
        , transp_zn, tel_area_cd, tel_no, fax_area_cd, fax_no,cusAdmBrh, mktSgm        
                     
 from #tmp  where cus_ven_typ = @Cus_Ven_Type  and (cusAdmBrh = @CusAdminBrh or @CusAdminBrh = '') order by cus_ven_id    
 END    
   drop table #tmp        
END        
        
-- exec [sp_itech_CustomerList]'US' , 'C','L','1','ALL','ALL' ,1    
-- Select distinct cva_cus_ven_typ from CN_scrcva_rec;        
--Select distinct cva_addr_typ from US_scrcva_rec; 
GO
