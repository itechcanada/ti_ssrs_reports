USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetGP_Shipped_Chart]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <26 Feb 2013>
-- Description:	<Getting top 40 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetGP_Shipped_Chart] @DBNAME varchar(50),@Market varchar(100)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD_cu varchar(10)
declare @TD_cu varchar(10)
declare @FD_pv varchar(10)
declare @TD_pv varchar(10)

set @DB= @DBNAME--UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'

set @FD_pv = CONVERT(VARCHAR(10), DATEADD(YEAR, DATEDIFF(YEAR, 0,DATEADD(YEAR, -1, GETDATE())), 0), 120)  --FirstDateOfPreviousYear
set @TD_pv = CONVERT(VARCHAR(10),DATEADD(MILLISECOND, -3, DATEADD(YEAR, DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, GETDATE())) + 1, 0)), 120)  --LastdateOFPreviousYear
set @FD_cu = CONVERT(VARCHAR(10),DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0), 120) -- FirstDateOFCurrentYear
set @TD_cu = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of Previous Month

if @Market = 'ALL'
 BEGIN
	 set @Market = ''
 END

CREATE TABLE #tmp (   LBSShipped  DECIMAL(20, 2)
   					, GP  DECIMAL(20, 2)
   					, InvDt	  VARCHAR(15)
   					, Years   Varchar(5) 
   					,Market   Varchar(100)
   	               );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Name VARCHAR(15);
DECLARE @prefix VARCHAR(5);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  				SET @DB=@prefix;-- UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  			   DECLARE @query NVARCHAR(4000);
		      SET @query = 'INSERT INTO #tmp (GP,LBSShipped,InvDt,Years,Market)
		                    select AVG(stn_npft_avg_pct) as GP
							,SUM(stn_blg_qty) as ''LBSShipped''
							,convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years,CUC_DESC30 
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_cu +''' and stn_inv_dt <= '''+ @TD_cu +''' and stn_frm <> ''XXXX'' 
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
							BEGIN
							     SET @query= @query + ' and cuc_desc30 <> ''Interco'''
							END
							
							SET @query = @query + ' group by CUC_DESC30 ,stn_inv_Dt,YEAR(stn_inv_Dt)
							union select AVG(stn_npft_avg_pct) as GP
							,SUM(stn_blg_qty) as ''LBSShipped''
							,convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years,CUC_DESC30 
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_pv +''' and stn_inv_dt <= '''+ @TD_pv +''' and stn_frm <> ''XXXX''
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
							BEGIN
							     SET @query= @query + ' and cuc_desc30 <> ''Interco'''
							END
							
							SET @query = @query + ' group by CUC_DESC30 ,stn_inv_Dt,YEAR(stn_inv_Dt)
							order by InvDt'
							
					print(@query)		
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN ----	and  (stn_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')
			  Set @Name=(select Name from tbl_itech_DatabaseName where DatabaseName=''+ @DBNAME + '')
			  SET @sqltxt = 'INSERT INTO #tmp (GP,LBSShipped,InvDt,Years,Market)
		                    select AVG(stn_npft_avg_pct) as GP
							,SUM(stn_blg_qty) as ''LBSShipped''
							,convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years,CUC_DESC30 
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_cu +''' and stn_inv_dt <= '''+ @TD_cu +''' and stn_frm <> ''XXXX'' 
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
							BEGIN
							     SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
							END
							
							SET @sqltxt = @sqltxt + ' group by CUC_DESC30 ,stn_inv_Dt,YEAR(stn_inv_Dt)
							union select AVG(stn_npft_avg_pct) as GP
							,SUM(stn_blg_qty) as ''LBSShipped''
							,convert(varchar(7),stn_inv_Dt, 126) +''-01'' as InvDt,YEAR(stn_inv_Dt) as Years,CUC_DESC30 
							from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
							left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
							Left Join  ' + @DB + '_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
							where stn_inv_Dt >= '''+ @FD_pv +''' and stn_inv_dt <= '''+ @TD_pv +''' and stn_frm <> ''XXXX'' 
							and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
							if @Market =''
							BEGIN
							     SET @sqltxt= @sqltxt + ' and cuc_desc30 <> ''Interco'''
							END
							
							SET @sqltxt = @sqltxt + ' group by CUC_DESC30 ,stn_inv_Dt,YEAR(stn_inv_Dt)
							order by InvDt'
					
		print(@sqltxt)
	set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
     END
   
   CREATE TABLE #Main (item  Varchar(50)
   					, Category  Varchar(150)
   					, AccDate	  VARCHAR(15)
   					, Value   DECIMAL(20, 2) 
   	               );	
   INSERT INTO #Main (item,Category,AccDate,Value)
    SELECT  'LBSShipped',Market,InvDt,sum(LBSShipped)  FROM #tmp 
   Group by InvDt,Market
   order by InvDt
   
    INSERT INTO #Main (item,Category,AccDate,Value)
    SELECT  'GP',Market,InvDt,AVG(GP)  FROM #tmp 
   Group by InvDt,Market
   order by InvDt
   
   select * from #Main
   
END

-- exec sp_itech_GetGP_Shipped_Chart 'ALL','ALL'





GO
