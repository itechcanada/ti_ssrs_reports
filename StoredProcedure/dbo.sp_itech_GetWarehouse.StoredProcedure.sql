USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetWarehouse]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <04 Jun 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetWarehouse]  @DBNAME varchar(50) 
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  Value varchar(15)   
        ,text Varchar(100)  
        ,temp varchar(3)  
        )  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( Value,text,temp)  
                  

                  SELECT     distinct prd_whs  AS ''Value'', prd_whs AS ''text'',''B'' AS temp  
                            FROM         '+ @Prefix +'_intprd_rec where prd_whs not in (''SFS'')'  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)  
                    SELECT     distinct prd_whs  AS ''Value'', prd_whs AS ''text'',''B'' AS temp  
                            FROM         '+ @DBNAME +'_intprd_rec  where prd_whs not in (''SFS'') 
       Union   
       Select ''ALL'' as ''Value'',''All Warehouse'' as ''text'',''A'' as temp  
       Order by temp,text  
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
   
    IF @DBNAME = 'ALL'  
 BEGIN  
      select * from #tmp  
      Union   
   Select 'ALL' as 'Value','All Warehouse' as 'text','A' as temp  
   Order by temp,text  
      End  
      ELSE  
      BEGIN  
      select * from #tmp  
     END
      drop table  #tmp  
END  
  
-- exec [sp_itech_GetWarehouse]  'ALL'  
  
  
  
  
  
GO
