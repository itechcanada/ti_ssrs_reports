USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_nctnhh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mrinal>  
-- Create date: <Create Date,sep 21, 2015,>  
-- Description: <Description,,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_nctnhh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CA_nctnhh_rec', 'U') IS NOT NULL  
  drop table dbo.CA_nctnhh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CA_nctnhh_rec  
  from [LIVECASTX].[livecastxdb].[informix].[nctnhh_rec];   
    
END  
-- select * from CA_nctnhh_rec
GO
