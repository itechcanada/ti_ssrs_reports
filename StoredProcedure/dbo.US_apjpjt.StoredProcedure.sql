USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_apjpjt]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Mukesh       
-- Create date: Jul 15, 2019     
-- Description: <Description,,>      
-- =============================================      
create PROCEDURE [dbo].[US_apjpjt]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
    -- Insert statements for procedure here      
          
    IF OBJECT_ID('dbo.US_apjpjt_rec', 'U') IS NOT NULL      
  drop table dbo.US_apjpjt_rec;      
              
SELECT *      
into  dbo.US_apjpjt_rec      
FROM [LIVEUSSTX].[liveusstxdb].[informix].[apjpjt_rec];      
      
END      
-- select * from US_apjpjt_rec
GO
