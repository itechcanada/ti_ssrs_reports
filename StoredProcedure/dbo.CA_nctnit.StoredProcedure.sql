USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_nctnit]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mrinal>  
-- Create date: <Create Date,sep 21, 2015,>  
-- Description: <Description,,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_nctnit]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CA_nctnit_rec', 'U') IS NOT NULL  
  drop table dbo.CA_nctnit_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CA_nctnit_rec  
  from [LIVECASTX].[livecastxdb].[informix].[nctnit_rec];   
    
END  
-- select * from CA_nctnit_rec
GO
