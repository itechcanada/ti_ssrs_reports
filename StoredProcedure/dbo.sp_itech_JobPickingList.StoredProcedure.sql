USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_JobPickingList]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <17 Dec 2013>
-- Description:	<Getting Picking List SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_JobPickingList] @DBNAME varchar(50), @SONo Varchar(10)
As
Begin
declare @DB varchar(100)
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
DECLARE @intFlag INT

IF @SONo = 'ALL' OR @SONo = ''
BEGIN
SET @SONo = 0
END

CREATE TABLE #tmp (   [Database]		 VARCHAR(10)
   					, SalesOrderNo		 VARCHAR(65)
   					, OrdItm		 	 int
   					, JSNo				 VARCHAR(65)
   					, IPNo				 VARCHAR(65)
   					, Dimensions		 Varchar(100)
   					, NOFPieces			 int
   					, WeightOfPieces 	 Varchar(50)
   					, Tag 				 Varchar(65)
   					)
   					
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB=  UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				SET @intFlag = 1
				WHILE (@intFlag <=20)
				BEGIN
					SET @query ='INSERT INTO #tmp ([Database], SalesOrderNo, OrdItm, JSNo, IPNo, Dimensions, NOFPieces, WeightOfPieces, Tag)
								SELECT ''' +  @Prefix + ''' as [Database], Cast(ssh_ord_no AS VARCHAR(50)) + ''' + '-' + ''' + CAST(ssh_ord_itm AS VARCHAR(50)) + ''' + '-' + ''' + CAST(ssh_ord_sitm AS VARCHAR(50)),ssh_ord_itm,
									ssh_jbs_pfx + ''' + '-' + ''' + CAST(ssh_jbs_no AS VARCHAR(50)), tja_ref_pfx + ''' + '-' + ''' + CAST(tja_ref_no AS VARCHAR(50)),Cast(tja_wdth as varchar(50)) + ''' + ' x ' + ''' + cast(tja_lgth as varchar(50)) as Dimension,
									tja_invt_pcs, tja_invt_wgt,tja_tag_no 
									from ' + @DB + '.[ipjssh_rec] 
									join ' + @DB + '.[ipjtja_rec] on tja_cmpy_id = ssh_cmpy_id and tja_ref_pfx = ssh_job_pfx_1 and tja_ref_no = ssh_job_no_'+CAST(@intFlag AS VARCHAR(3)) +'
									where  ssh_ord_pfx = ''' + 'SO' + ''' and ssh_ord_no =  ' + @SONo + ';'
					print @query;
  	  				EXECUTE sp_executesql @query;
  	  				SET @intFlag = @intFlag + 1
				END
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
     Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')
				SET @DB=  UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
				SET @intFlag = 1
				WHILE (@intFlag <=20)
				BEGIN
					SET @sqltxt ='INSERT INTO #tmp ([Database], SalesOrderNo, OrdItm, JSNo, IPNo, Dimensions, NOFPieces, WeightOfPieces, Tag)
								SELECT ''' +  @DBNAME + ''' as [Database], Cast(ssh_ord_no AS VARCHAR(50)) + ''' + '-' + ''' + CAST(ssh_ord_itm AS VARCHAR(50)) + ''' + '-' + ''' + CAST(ssh_ord_sitm AS VARCHAR(50)),ssh_ord_itm,
								ssh_jbs_pfx + ''' + '-' + ''' + CAST(ssh_jbs_no AS VARCHAR(50)), tja_ref_pfx + ''' + '-' + ''' + CAST(tja_ref_no AS VARCHAR(50)),Cast(tja_wdth as varchar(50)) + ''' + ' x ' + ''' + cast(tja_lgth as varchar(50)) as Dimension,
								tja_invt_pcs, tja_invt_wgt,tja_tag_no 
								from ' + @DB + '.[ipjssh_rec] 
								join ' + @DB + '.[ipjtja_rec] on tja_cmpy_id = ssh_cmpy_id and tja_ref_pfx = ssh_job_pfx_1 and tja_ref_no = ssh_job_no_'+CAST(@intFlag AS VARCHAR(3)) +'
								where  ssh_ord_pfx = ''' + 'SO' + ''' and ssh_ord_no =  ' + @SONo + ';'
					print(@sqltxt)	
					set @execSQLtxt = @sqltxt; 
					EXEC (@execSQLtxt);
				SET @intFlag = @intFlag + 1
				END
			     
   END
   Select * from #tmp ORDER BY OrdItm, JSNo, IPNo
   Drop table #tmp
End 

-- Exec [sp_itech_JobPickingList] 'ALL', 71677
-- Exec [sp_itech_JobPickingList] 'US', 'ALL'
-- Exec [sp_itech_JobPickingList] 'US', 71677
GO
