USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_nctnic]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description,gl account,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_nctnic]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.DE_nctnic_rec', 'U') IS NOT NULL  
  drop table dbo.DE_nctnic_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.DE_nctnic_rec  
  from [LIVEDESTX].[livedestxdb].[informix].[nctnic_rec];   
    
END  
-- select * from DE_nctnic_rec
GO
