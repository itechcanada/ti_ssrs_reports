USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MoogPurchaseOrdHistByPartNo ]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <16 Aug 2016>            
-- Description: <Getting top 50 customers for SSRS reports>              
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_MoogPurchaseOrdHistByPartNo ] @DBNAME varchar(50), @FromDate datetime, @ToDate datetime , @PartNo Varchar(30), @VendorID Varchar(10)       
AS            
BEGIN     
  
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(max)            
declare @execSQLtxt varchar(max)            
declare @DB varchar(100)          
declare @FD varchar(10)            
declare @TD varchar(10)            
DECLARE @ExchangeRate varchar(15)
DECLARE @CurrenyRate varchar(15) 
            
CREATE TABLE #tmp (  DatabaseName	Varchar(3)
			,PartNo		Varchar(30)
			,VendorID  Varchar(10)
			,Form  varchar(35)          
        ,Grade  varchar(35)          
        ,Size  varchar(35)          
        ,Finish  varchar(35) 
        ,ReceivedDate	Varchar(10)   
        ,LocationReceived	Varchar(3)
        ,PoundsReceive Decimal(20,0)
        ,MaterialCost	Decimal (20,2)      
                 );             
  
            
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) 
  
  DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(5);      
DECLARE @Name VARCHAR(15);            
 
 set @DB=  @DBNAME  ;
 
  
  IF @DBNAME = 'ALL'      
 BEGIN      
       
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName Where Prefix in ('US','TW')        
    OPEN ScopeCursor;      
        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
      SET @DB= @Prefix      
     -- print(@DB)      
        DECLARE @query NVARCHAR(4000);            
         IF (UPPER(@Prefix) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@Prefix) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@Prefix) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@Prefix) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@Prefix) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End      
    Else if(UPPER(@Prefix) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End      
        SET @query ='INSERT INTO #tmp ( DatabaseName,PartNo, VendorID,Form,Grade,Size,Finish,ReceivedDate,LocationReceived,PoundsReceive,MaterialCost)            
        select ''' + @DB + ''', ipd_part,poh_ven_id, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, (case when Len(pod_arr_dt_ent) = 8 then Cast(Convert(Varchar(10),pod_arr_dt_ent ) as date) else '''' end),
        pod_shp_to_whs, '
        
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')          
       BEGIN          
       SET @query = @query + ' poh_tot_rcvd_wgt * 2.20462 , '
       End 
       Else
       Begin
       SET @query = @query + ' poh_tot_rcvd_wgt, '
       End
        
        
        SET @query = @query + ' pod_lnd_cst * '+ @CurrenyRate +' 
 from '+ @DB +'_potpoh_rec 
join '+ @DB +'_potpod_rec on pod_cmpy_id = poh_cmpy_id and pod_po_pfx = poh_po_pfx and pod_po_no = poh_po_no
join '+ @DB +'_tctipd_rec on ipd_cmpy_id = pod_cmpy_id and ipd_ref_pfx = pod_po_pfx and ipd_ref_no = pod_po_no and ipd_ref_itm = pod_po_itm
where (poh_ven_id = ''' + @VendorID + ''' Or ''' + @VendorID + ''' = '''') and (ipd_part = ''' + @PartNo + ''' OR ''' + @PartNo + ''' = '''')
 and pod_arr_fm_dt >= '''+ @FD +''' and pod_arr_to_dt <= '''+ @TD +''' 
  and ipd_frm <> ''XXXX'' '  
       print(@query)        
        EXECUTE sp_executesql @query;  
        
         FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN  
       IF (UPPER(@DB) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
    End            
    Else if (UPPER(@DB) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
    End            
    Else if (UPPER(@DB) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
    End            
    Else if (UPPER(@DB) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
    End            
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )            
    begin            
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
    End            
    Else if(UPPER(@DB) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
    End  
    Else if(UPPER(@DB) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
    End  
    
           SET @sqltxt ='INSERT INTO #tmp ( DatabaseName,PartNo, VendorID,Form,Grade,Size,Finish,ReceivedDate,LocationReceived,PoundsReceive,MaterialCost)            
 select ''' + @DB + ''', ipd_part,poh_ven_id, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, (case when Len(pod_arr_dt_ent) = 8 then Cast(Convert(Varchar(10),pod_arr_dt_ent ) as date) else '''' end),
 pod_shp_to_whs, '
 
 if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
       BEGIN 
 SET @sqltxt = @sqltxt + ' poh_tot_rcvd_wgt * 2.20462, '
 End
 Else
 Begin
 SET @sqltxt = @sqltxt + ' poh_tot_rcvd_wgt , '
 End
 SET @sqltxt = @sqltxt + ' pod_lnd_cst * '+ @CurrenyRate +' 
 from '+ @DB +'_potpoh_rec 
join '+ @DB +'_potpod_rec on pod_cmpy_id = poh_cmpy_id and pod_po_pfx = poh_po_pfx and pod_po_no = poh_po_no
join '+ @DB +'_tctipd_rec on ipd_cmpy_id = pod_cmpy_id and ipd_ref_pfx = pod_po_pfx and ipd_ref_no = pod_po_no and ipd_ref_itm = pod_po_itm
where (poh_ven_id = ''' + @VendorID + ''' Or ''' + @VendorID + ''' = '''') and (ipd_part = ''' + @PartNo + ''' OR ''' + @PartNo + ''' = '''')
 and pod_arr_fm_dt >= '''+ @FD +''' and pod_arr_to_dt <= '''+ @TD +''' 
  and ipd_frm <> ''XXXX'' '   
          print(@sqltxt);            
                
    set @execSQLtxt = @sqltxt;             
   EXEC (@execSQLtxt);            
  End 
  SELECT * FROM #tmp ;
       
  DROP TABLE #tmp     ;       
            
END            
            
 -- exec sp_itech_MoogPurchaseOrdHistByPartNo 'US','09/01/2015', '12/30/2015', '','439'   
 /*
  '11782',
'12326',
-- '1767',
'6748',
'748'-- ,'993'
 */
 
GO
