USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ortorl]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/19/2020,>  
-- Description: <Description,Germany ortorl_rec,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_ortorl]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

truncate table dbo.DE_ortorl_rec  

-- Insert statements for procedure here    
Insert into dbo.DE_ortorl_rec   
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[ortorl_rec]  
  
  
END  
/*

*/
GO
