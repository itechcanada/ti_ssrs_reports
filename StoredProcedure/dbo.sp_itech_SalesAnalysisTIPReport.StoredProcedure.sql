USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisTIPReport]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mayank >              
-- Create date: <11 Feb 2013>              
-- Description: <Getting top 50 customers for SSRS reports>              
-- Last updated Date 13 Apr 2015            
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt            
-- Last updated by : Mukesh             
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisTIPReport] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(15),@Market as varchar(65),@GPPct as varchar(20),@version char = '0', @IncludeInterco char = '0' ,@DateRange int = 2        
  
              
AS              
BEGIN              
 SET NOCOUNT ON;         
           
declare @sqltxt varchar(6000)              
declare @execSQLtxt varchar(7000)              
declare @DB varchar(100)              
declare @FD varchar(10)              
declare @TD varchar(10)              
declare @NOOfCust varchar(15)              
DECLARE @ExchangeRate varchar(15)             
DECLARE @IsExcInterco char(1)             
             
set @DB=  @DBNAME              
            
if (@FromDate = '' OR @FromDate = '1900-01-01')            
BEGIN              
 set @FromDate = CONVERT(VARCHAR(10),DATEADD(day, -8, GETDATE()), 120)             
 END             
            
if @ToDate = '' OR @ToDate = '1900-01-01'            
BEGIN              
 set @ToDate = CONVERT(VARCHAR(10),DATEADD(day, -1, GETDATE()), 120)             
 END             
          
if @DateRange=1 --Previous day                         
 BEGIN                          
  set @FD = CONVERT(VARCHAR(10), GETDATE()-1 , 120)                         
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120)          
 End                          
else                          
 Begin                          
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)              
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                            
 End             
            
           
             
IF @Branch = 'ALL'          
BEGIN          
SET @Branch = ''          
END             
              
CREATE TABLE #tmp (   CustID   VARCHAR(10)              
        , CustName     VARCHAR(100)              
        ,SalesPersonInside VARCHAR(10)              
        ,SalesPersonOutside VARCHAR(10)            
        ,SalesPersonTaken VARCHAR(10)            
        , Market   VARCHAR(65)               
        , Branch   VARCHAR(65)               
        , Form           Varchar(65)              
        , Grade           Varchar(65)              
        , TotWeight  DECIMAL(20, 2)              
        , WeightUM           Varchar(10)              
                    , TotalValue    DECIMAL(20, 2)              
        , NetGP               DECIMAL(20, 2)              
        , TotalMatValue    DECIMAL(20, 2)              
        , MatGP    DECIMAL(20, 2)              
        , Databases   VARCHAR(15)               
        , MarketID   integer,              
        Finish varchar(35),              
        Size varchar(35),stn_cry   varchar(5)            
        ,InvDate Varchar(10)            
        ,InvNo Varchar(25)           
        ,SalesCategory Varchar(2)          
        ,SizeDesc Varchar(65)           
        ,millName Varchar(40)            
        ,MillDlyDate Varchar(10)       
        ,ShpgWhs Varchar(3)          
                 );               
              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(35);              
DECLARE @Name VARCHAR(15);              
DECLARE @CurrenyRate varchar(15);              
              
if @Market ='ALL'              
 BEGIN              
 set @Market = ''              
 END              
            
if ( @IncludeInterco = '0')            
BEGIN            
set @IsExcInterco ='T'            
END            
              
IF @DBNAME = 'ALL'              
 BEGIN              
   IF @version = '0'              
  BEGIN              
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName              
    OPEN ScopeCursor;              
  END              
  ELSE              
  BEGIN              
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
  END              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(max);                 
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'              
        
       IF (UPPER(@Prefix) = 'TW')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
        End              
        Else if (UPPER(@Prefix) = 'NO')              
        begin             
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
        End              
        Else if (UPPER(@Prefix) = 'CA')              
        begin              
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
        End              
        Else if (UPPER(@Prefix) = 'CN')              
        begin              
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
        End              
        Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
        begin              
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
        End              
        Else if(UPPER(@Prefix) = 'UK')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
        End              
        Else if(UPPER(@Prefix) = 'DE')              
        begin              
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
        End              
                    
                    
      if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                 
                BEGIN              
         SET @query ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,        
         SalesCategory,SizeDesc,millName ,MillDlyDate,ShpgWhs)                  
           
          select stn_sld_cus_id as CustID, ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,CUS_CUS_LONG_NM as CustName, cuc_desc30 as Market,              
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade, '              
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                 
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'              
             ELSE              
               SET @query = @query + ' SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,'              
                      
        SET @query = @query + '   
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,               
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,              
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,               
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,              
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,ISNULL(stn_sls_cat,'''') as stn_sls_cat ,          
          RTRIM(LTRIM(prm_size_desc))  ,          
          (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,            
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,    
  
  (case when ou.stn_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and       
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)       
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else ou.stn_shpg_whs end ) 
  
 as ''ShpgWhs''      
         
         from ' + @DB + '_sahstn_rec ou         
         join [' + @DB + '_ortorh_rec]  ON ou.stn_cmpy_id = orh_cmpy_id and ou.stn_ord_pfx =orh_ord_pfx AND ou.stn_ord_no =orh_ord_no   
         join ' + @DB + '_arrshp_rec on shp_cmpy_id = stn_cmpy_id and shp_cus_id = stn_sld_cus_id and shp_shp_to = stn_shp_to             
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id             
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''           
         LEFT Join ' + @DB + '_inrprm_rec on stn_frm = prm_frm and stn_Grd = prm_grd and stn_size = prm_size   and stn_fnsh = prm_fnsh       
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = shp_is_slp             
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = shp_os_slp        
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp                                             
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')              
          group by ou.stn_cmpy_id, stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,stn_sls_cat, CUS_CUS_LONG_NM, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,              
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt, prm_size_desc ,stn_shpg_whs            
         order by stn_sld_cus_id '              
    END              
       Else              
      BEGIN              
        SET @query ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,  
        TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,SalesCategory,SizeDesc,millName ,MillDlyDate,ShpgWhs)                   
          select stn_sld_cus_id as CustID, ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,CUS_CUS_LONG_NM as CustName, cuc_desc30 as Market,              
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,              
        SUM(stn_blg_wgt) as TotWeight,               
        stn_ord_wgt_um as WeightUM,              
              
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,               
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,              
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,               
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,              
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref ,ISNULL(stn_sls_cat,'''') as stn_sls_cat,          
           RTRIM(LTRIM(prm_size_desc)),          
           (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,            
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) , '  
  
    
  if  (UPPER(@Prefix)= 'US')       
SET @query = @query + ' stn_shpg_whs  '      
else      
SET @query = @query + '      
  (case when ou.stn_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and       
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)       
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else ou.stn_shpg_whs end ) 
  
    
as ''ShpgWhs'' '      
 SET @query = @query + '       
         from ' + @DB + '_sahstn_rec ou      
         join [' + @DB + '_ortorh_rec]  ON ou.stn_cmpy_id = orh_cmpy_id and ou.stn_ord_pfx =orh_ord_pfx AND ou.stn_ord_no =orh_ord_no   
         join ' + @DB + '_arrshp_rec on shp_cmpy_id = stn_cmpy_id and shp_cus_id = stn_sld_cus_id and shp_shp_to = stn_shp_to               
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''          
         LEFT Join ' + @DB + '_inrprm_rec on stn_frm = prm_frm and stn_Grd = prm_grd and stn_size = prm_size   and stn_fnsh = prm_fnsh       
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = shp_is_slp             
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = shp_os_slp        
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp                        
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')              
          group by ou.stn_cmpy_id, stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id, stn_sls_cat,CUS_CUS_LONG_NM, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,              
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt, prm_size_desc,stn_shpg_whs                  
         order by stn_sld_cus_id '              
      End              
        EXECUTE sp_executesql @query;              
                      
                      
        print(@query)              
      FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE              
     BEGIN             
     IF @version = '0'              
     BEGIN                
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')              
     END            
     ELSE            
     BEGIN            
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
     END            
                 
    IF (UPPER(@DBNAME) = 'TW')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
   End              
   Else if (UPPER(@DBNAME) = 'NO')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
   End              
   Else if (UPPER(@DBNAME) = 'CA')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
   End              
   Else if (UPPER(@DBNAME) = 'CN')              
   begin              
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
   End              
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
   begin              
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
   End              
   Else if(UPPER(@DBNAME) = 'UK')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
   End              
   Else if(UPPER(@DBNAME) = 'DE')              
   begin              
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
   End              
                   
   if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')                 
                BEGIN              
         SET @sqltxt ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken , CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,      
  
         SalesCategory,SizeDesc,millName ,MillDlyDate,ShpgWhs)                
             
          select stn_sld_cus_id as CustID,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id, CUS_CUS_LONG_NM as CustName, cuc_desc30 as Market,              
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,'              
        if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')                 
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'              
             ELSE              
               SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as TotWeight,stn_ord_wgt_um as WeightUM,'               
                      
       SET @sqltxt = @sqltxt + '               
              
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,               
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,              
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,               
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,              
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,ISNULL(stn_sls_cat,'''') as stn_sls_cat ,             
           RTRIM(LTRIM(prm_size_desc)),          
            (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,            
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,    
  
        
  (case when ou.stn_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and       
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)       
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else ou.stn_shpg_whs end ) 
  
    
as ''ShpgWhs''      
            
         from ' + @DB + '_sahstn_rec ou          
         join [' + @DB + '_ortorh_rec]  ON ou.stn_cmpy_id = orh_cmpy_id and ou.stn_ord_pfx =orh_ord_pfx AND ou.stn_ord_no =orh_ord_no  
          join ' + @DB + '_arrshp_rec on shp_cmpy_id = stn_cmpy_id and shp_cus_id = stn_sld_cus_id and shp_shp_to = stn_shp_to            
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''               
         LEFT Join ' + @DB + '_inrprm_rec on stn_frm = prm_frm and stn_Grd = prm_grd and stn_size = prm_size   and stn_fnsh = prm_fnsh      
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = shp_is_slp             
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = shp_os_slp        
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp                                          
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')              
          group by  ou.stn_cmpy_id,stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id, stn_sls_cat,CUS_CUS_LONG_NM, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,              
          stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt  , prm_size_desc ,stn_shpg_whs               
         order by stn_sld_cus_id '              
    END              
       Else              
      BEGIN              
        SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,       
 
        SalesCategory,SizeDesc,millName ,MillDlyDate,ShpgWhs)                  
           
          select stn_sld_cus_id as CustID,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id, CUS_CUS_LONG_NM as CustName, cuc_desc30 as Market,              
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,              
        SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,              
              
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,               
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,              
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,               
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,              
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size ,stn_inv_dt,stn_upd_ref ,ISNULL(stn_sls_cat,'''') as stn_sls_cat,            
           RTRIM(LTRIM(prm_size_desc)),          
           (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,            
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) , '  
  
    
if  (UPPER(@DBNAME)= 'US')       
SET @sqltxt = @sqltxt + ' stn_shpg_whs  '      
else      
SET @sqltxt = @sqltxt + '      
  (case when ou.stn_sls_cat = ''DS'' then   (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and       
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6)       
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )    else ou.stn_shpg_whs end ) 
  
    
as ''ShpgWhs'''      
 SET @sqltxt = @sqltxt + '      
         from ' + @DB + '_sahstn_rec ou        
         join [' + @DB + '_ortorh_rec]  ON ou.stn_cmpy_id = orh_cmpy_id and ou.stn_ord_pfx =orh_ord_pfx AND ou.stn_ord_no =orh_ord_no   
          join ' + @DB + '_arrshp_rec on shp_cmpy_id = stn_cmpy_id and shp_cus_id = stn_sld_cus_id and shp_shp_to = stn_shp_to              
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id            
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''               
         LEFT Join ' + @DB + '_inrprm_rec on stn_frm = prm_frm and stn_Grd = prm_grd and stn_size = prm_size   and stn_fnsh = prm_fnsh       
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = shp_is_slp             
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = shp_os_slp        
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp                                         
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''              
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')              
          group by ou.stn_cmpy_id, stn_sld_cus_id, ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,stn_sls_cat,CUS_CUS_LONG_NM, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,              
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt, prm_size_desc ,stn_shpg_whs                 
         order by stn_sld_cus_id '              
      End              
      print(@sqltxt)              
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
     END              
                 
     if @IsExcInterco ='T'            
 BEGIN            
                 
     print @GPpct;              
     if(@GPPct = '')            
     begin            
   SELECT RTRIM(LTrim(CustID)) as CustID,-- RTRIM(LTrim(CustID))+'-'+Databases as CustID,          
    SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, Sum(TotWeight) as TotWeight,'LBS' as WeightUM,  
    Sum(TotalValue) as TotalValue,Sum(NetGP) as NetGP,Sum(TotalMatValue) as TotalMatValue,Sum(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,              
     (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',          
     (case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/Sum(TotalValue))* 100 end) as 'NP%',          
     InvDate,InvNo,SalesCategory           
     ,  Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',Min(millName) as millName,Min(MillDlyDate) as MillDlyDate ,ShpgWhs      
     FROM #tmp  Where (Market <> 'Interco' or Market is null)   AND Branch not in ('SFS')  and (Branch = @Branch OR @Branch = '')            
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,SalesCategory              
   ,Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)),ShpgWhs          
   order by CustID               
   end            
   else            
   begin            
   SELECT RTRIM(LTrim(CustID)) as CustID,-- RTRIM(LTrim(CustID))+'-'+Databases as CustID,           
   SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, Sum(TotWeight) as TotWeight,'LBS' as WeightUM,  
   Sum(TotalValue) as TotalValue,Sum(NetGP) as NetGP,Sum(TotalMatValue) as TotalMatValue,Sum(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,              
     (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',          
     (case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/Sum(TotalValue))* 100 end) as 'NP%',          
     InvDate,InvNo,SalesCategory           
     ,  Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',Min(millName) as millName,Min(MillDlyDate) as MillDlyDate ,ShpgWhs      
     FROM #tmp   where Branch not in ('SFS')  and (Branch = @Branch OR @Branch = '')              
     -- AND (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) <= @GPPct        
     and (Market <> 'Interco' or Market is null)             
     --CAST( MatGP AS Decimal) <= 100.25              
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size ,InvDate,InvNo,SalesCategory             
   ,Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)),ShpgWhs      
   having (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) <= @GPPct         
   order by CustID               
   end            
               
   end            
               
   else            
   Begin            
               
   print @GPpct;              
     if(@GPPct = '')            
     begin            
   SELECT RTRIM(LTrim(CustID)) as CustID,-- RTRIM(LTrim(CustID))+'-'+Databases as CustID,           
   SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, Sum(TotWeight) as TotWeight,'LBS' as WeightUM,  
   Sum(TotalValue) as TotalValue,Sum(NetGP) as NetGP,Sum(TotalMatValue) as TotalMatValue,Sum(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,              
     (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',          
     (case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/Sum(TotalValue))* 100 end) as 'NP%',          
     InvDate,InvNo,SalesCategory           
     ,  Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',Min(millName) as millName,Min(MillDlyDate) as MillDlyDate ,ShpgWhs         
     FROM #tmp where Branch not in ('SFS') and (Branch = @Branch OR @Branch = '')                
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,SalesCategory              
   ,Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)),ShpgWhs          
   order by CustID               
   end            
   else            
   begin            
   SELECT RTRIM(LTrim(CustID)) as CustID, -- RTRIM(LTrim(CustID))+'-'+Databases as CustID,           
   SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, Sum(TotWeight) as TotWeight,'LBS' as WeightUM,  
   Sum(TotalValue) as TotalValue,Sum(NetGP) as NetGP,Sum(TotalMatValue) as TotalMatValue,Sum(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,              
     (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',          
     (case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/Sum(TotalValue))* 100 end) as 'NP%',          
     InvDate,InvNo,SalesCategory           
     ,  Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',Min(millName) as millName,Min(MillDlyDate) as MillDlyDate ,ShpgWhs         
     FROM #tmp  WHERE Branch not in ('SFS') and (Branch = @Branch OR @Branch = '')                  
     --AND (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) <= @GPPct              
     --CAST( MatGP AS Decimal) <= 100.25             
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size ,InvDate,InvNo,SalesCategory             
   ,Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)),ShpgWhs       
   having (case Sum(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) <= @GPPct          
   order by CustID               
   end            
   end            
  print @GPpct;             
                 
END              
-- RTRIM(LTrim(CustID))+'-'+Databases as              
   --@Market as varchar(65),@GPPct as varchar(20),@version char = '0', @IncludeInterco char = '0' ,@DateRange int = 2               
-- exec [sp_itech_SalesAnalysisTIPReport] '03/01/2018', '05/01/2018' , 'CA','ALL','ALL','','1'   -- 395          
-- exec [sp_itech_SalesAnalysisTIPReport] '07/23/2015', '06/23/2015' , 'US','HIB','ALL','' ,            
              
   /*          
-- 2017-04-11          
Sub: STRATIX REPORTS          
Make this report as reported in same way like lost account by branch with email.             
-- 2016-08-16          
Sub: Branch Stratix Reports          
We need to add the branch selection to this report please.          
          
          
CHANGES:-          
20160525 :-          
 option out PSM and especially SFS when all databases is selected          
           
 SOLUTION:-           
 NitBranch not in ('SFS') and NitWhs not in ('SFS')           
           
 Date 20170821          
sub:Two Report Modifications Request        
      
       
Date 20170915      
Sub: Additional Request - Add Shipping Warehouse          
      
Date 20171206      
sub:Daily Sales Analysis TIP Report - HIB.xls          
      
date 20180420/20180507      
sub: Please check this report      
      
*/ 
GO
