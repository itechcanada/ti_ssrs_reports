USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_mchqms]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/25/2020,>  
-- Description: <Description,QDS Metal Standards,>  
  
-- =============================================  
create PROCEDURE [dbo].[DE_mchqms]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
Truncate table dbo.DE_mchqms_rec ;   
  
-- Insert statements for procedure here  
insert into dbo.DE_mchqms_rec
SELECT *  
  from [LIVEDESTX].[livedestxdb].[informix].[mchqms_rec] ;   
    
END  
GO
