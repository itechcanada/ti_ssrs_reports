USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_List_PO_Audit]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <02 Feb 2016>  
-- Description: <List PO audit> 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_List_PO_Audit]  @FromDate datetime, @ToDate datetime, @DBNAME varchar(50) 
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
declare @DB varchar(100);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15); 
declare @FD varchar(10) ;         
declare @TD varchar(10)  ;

set @FD = CONVERT(VARCHAR(10), @FromDate , 120);          
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) ; 
  
 
  
CREATE TABLE #temp ( Dbname   VARCHAR(10)  
     ,VendorID VARCHAR(10) 
     ,VendorName		VARCHAR(20)
     ,PONo		VARCHAR(10)   
     ,POPlacementDate   VARCHAR(10)  
     ,POArrivalDate  VARCHAR(10)  
     ,POArrivalFromDate  VARCHAR(10)    
     ,POArrivalToDate  VARCHAR(10)  
     ,POArrivalRcvDate  VARCHAR(10)  
     ,POMillACKDate  VARCHAR(10)  
     );  
  
IF @DBNAME = 'ALL'  
 BEGIN  
   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
     
    SET @query = 'INSERT INTO #temp (Dbname, VendorID, VendorName, PONo, POPlacementDate, POArrivalDate, POArrivalFromDate, POArrivalToDate, POArrivalRcvDate,POMillACKDate)  
     select '''+ @Prefix +''' as Country, poh_VEn_id, ven_ven_nm, poh_po_no as PONo,   poh_Po_Pl_dt,
     hst.pod_arr_dt_ent , 
      scr.pod_arr_fm_dt, scr.pod_arr_to_dt, rhr_rcpt_actvy_dt,
      (case when hst.pod_arr_dt_ent = (case len(scr.pod_arr_dt_ent) when 8 then  cast(cast(scr.pod_arr_dt_ent as varchar(10)) as date) else Null end ) then NULL else (case len(scr.pod_arr_dt_ent) when 8 then  cast(cast(scr.pod_arr_dt_ent as varchar(10)) as date) else Null end ) end) as cmp
      from  ' + @Prefix + '_potpoh_rec
 join ' + @Prefix + '_potpoi_rec on poi_cmpy_id = poh_cmpy_id and poi_po_pfx = poh_po_pfx and poi_po_no = poh_Po_no
join tbl_itech_potpod_history hst on poi_cmpy_id = hst.pod_cmpy_id and poi_po_pfx = hst.pod_po_pfx and poi_po_no = hst.pod_Po_no and poi_po_itm = hst.pod_po_itm
join ' + @Prefix + '_potpod_rec scr on scr.pod_cmpy_id = hst.pod_cmpy_id and scr.pod_po_pfx = hst.pod_po_pfx and scr.pod_po_no = hst.pod_po_no 
and scr.pod_po_itm = hst.pod_po_itm and scr.pod_po_dist = hst.pod_po_dist
join ' + @Prefix + '_pohrhr_rec  on poi_cmpy_id = rhr_cmpy_id and poi_po_pfx = rhr_po_pfx and poi_po_no = rhr_Po_no and poi_po_itm = rhr_po_itm
join ' + @Prefix + '_aprven_rec on poh_cmpy_id = ven_cmpy_id and poh_VEn_id = ven_VEn_id
Where CONVERT(VARCHAR(10), hst.pod_arr_fm_dt , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), hst.pod_arr_to_dt , 120) <= '''+ @TD +'''   
 order by poh_po_no desc   '  
   
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
 
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
      
    
  SET @sqltxt ='INSERT INTO #temp (Dbname, VendorID, VendorName, PONo, POPlacementDate, POArrivalDate, POArrivalFromDate, POArrivalToDate, POArrivalRcvDate,POMillACKDate)  
     select '''+ @DBNAME +''' as Country, poh_VEn_id, ven_ven_nm, poh_po_no as PONo,   poh_Po_Pl_dt,
     hst.pod_arr_dt_ent , 
      scr.pod_arr_fm_dt, scr.pod_arr_to_dt, rhr_rcpt_actvy_dt,
      (case when hst.pod_arr_dt_ent = (case len(scr.pod_arr_dt_ent) when 8 then  cast(cast(scr.pod_arr_dt_ent as varchar(10)) as date) else Null end ) then NULL else (case len(scr.pod_arr_dt_ent) when 8 then  cast(cast(scr.pod_arr_dt_ent as varchar(10)) as date) else Null end ) end) as cmp
      from  ' + @DBNAME + '_potpoh_rec
 join ' + @DBNAME + '_potpoi_rec on poi_cmpy_id = poh_cmpy_id and poi_po_pfx = poh_po_pfx and poi_po_no = poh_Po_no
join tbl_itech_potpod_history hst on poi_cmpy_id = hst.pod_cmpy_id and poi_po_pfx = hst.pod_po_pfx and poi_po_no = hst.pod_Po_no and poi_po_itm = hst.pod_po_itm
join ' + @DBNAME + '_potpod_rec scr on scr.pod_cmpy_id = hst.pod_cmpy_id and scr.pod_po_pfx = hst.pod_po_pfx and scr.pod_po_no = hst.pod_po_no 
and scr.pod_po_itm = hst.pod_po_itm and scr.pod_po_dist = hst.pod_po_dist
join ' + @DBNAME + '_pohrhr_rec  on poi_cmpy_id = rhr_cmpy_id and poi_po_pfx = rhr_po_pfx and poi_po_no = rhr_Po_no and poi_po_itm = rhr_po_itm
join ' + @DBNAME + '_aprven_rec on poh_cmpy_id = ven_cmpy_id and poh_VEn_id = ven_VEn_id
Where CONVERT(VARCHAR(10), hst.pod_arr_fm_dt , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), hst.pod_arr_to_dt , 120) <= '''+ @TD +'''   
 order by poh_po_no desc  '  
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ;  
 DROP TABLE  #temp;  
END  
  
--EXEC [sp_itech_List_PO_Audit] 'ALL'  
--EXEC [sp_itech_List_PO_Audit] '2016-04-11','2016-05-11','US'   -- 463
--EXEC [sp_itech_List_PO_Audit] '2016-04-11','2016-05-11','ALL'   -- 624
--select * from tbl_itech_DatabaseName
GO
