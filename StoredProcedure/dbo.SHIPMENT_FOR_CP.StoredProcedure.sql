USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[SHIPMENT_FOR_CP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,6/15/2012>
-- Description:	<Description,It's for customer portal,>
-- =============================================
CREATE PROCEDURE [dbo].[SHIPMENT_FOR_CP]
	-- Add the parameters for the stored procedure here
	@EndDate datetime,
	@Branch varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT a.cus_cus_id, a.cus_cus_long_nm, a.cus_admin_brh,SUM(b.sat_blg_wgt) as wgt,SUM(b.sat_tot_val) as total
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arrcus_rec] a 
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 11
AND a.cus_admin_brh=@Branch
AND c.cuc_desc30<>'Interco'
GROUP BY a.cus_admin_brh,a.cus_cus_id, a.cus_cus_long_nm
HAVING SUM(b.sat_tot_val)*1>0
ORDER BY SUM(b.sat_tot_val)*1  DESC
END
GO
