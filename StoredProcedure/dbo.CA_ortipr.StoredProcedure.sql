USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_ortipr]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: jun 25, 2019 
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[CA_ortipr]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.CA_ortipr_rec', 'U') IS NOT NULL  
  drop table dbo.CA_ortipr_rec;  
          
SELECT *  
into  dbo.CA_ortipr_rec  
FROM [LIVECASTX].[livecastxdb].[informix].[ortipr_rec];  
  
END  
-- select * from UK_ortipr_rec
GO
