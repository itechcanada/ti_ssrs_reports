USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[USAGE]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author, Wendy Wang>
-- Create date: <Create Date,2/13/2012>
-- Description:	<Description,Product usage lbs and total values by Customer>
-- =============================================
CREATE PROCEDURE [dbo].[USAGE]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime,
	@CustID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  a.cus_cus_id,a.cus_cus_long_nm, SUM(b.sat_blg_wgt) as wgt, SUM(b.sat_tot_mtl_val) as value, b.sat_frm+b.sat_grd+b.sat_size+b.sat_fnsh as Product
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcus_rec] a 
	INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE 
a.cus_cus_id=@CustID
AND b.sat_frm <> 'XXXX'
AND b.sat_inv_pfx='SE' 
AND CAST(b.sat_inv_dt AS datetime) BETWEEN @FromDate AND @ToDate
GROUP BY  a.cus_cus_id,a.cus_cus_long_nm, b.sat_frm+b.sat_grd+b.sat_size+b.sat_fnsh

END
GO
