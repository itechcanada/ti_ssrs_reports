USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ortbki]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mukesh>  
-- Create date: <Create Date,Oct 15, 2015,>  
-- Description: <Description,Due Date,>  
  
-- =============================================  
Create PROCEDURE [dbo].[TW_ortbki]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_ortbki_rec', 'U') IS NOT NULL  
  drop table dbo.TW_ortbki_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_ortbki_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[ortbki_rec];   
    
END  
-- select * from TW_ortbki_rec
GO
