USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_sahstn]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                              
-- =============================================                                                              
-- Author:  <Author,Sumit>                                                              
-- Create date: <Create Date,11/19/2020,>                                                              
-- Description: <Description,Germany Open Orders,>                                                          
-- =============================================                                                              
CREATE PROCEDURE [dbo].[DE_sahstn]                                                              
                                                               
AS                                                              
BEGIN                                                              
 -- SET NOCOUNT ON added to prevent extra result sets from                                                              
 -- interfering with SELECT statements.                                                              
 SET NOCOUNT ON;                                                              
truncate table dbo.DE_sahstn_rec ; 
-- Insert statements for procedure here                                                              
-- There is a problem in column stn_part   .. being used in sp_itech_CustomerByProduct_test, sp_itech_CustomerByProduct_new , sp_itech_CustomerByProduct_Goodrich 
	
Insert into dbo.DE_sahstn_rec                                                               
SELECT * FROM [LIVEDESTX].[livedestxdb].[informix].[sahstn_rec] ;                                     
                                         
END         

/*
exec DE_sahstn
*/
GO
