USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_mxtwmq]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: April 25, 2018  
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[US_mxtwmq]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.US_mxtwmq_rec', 'U') IS NOT NULL  
  drop table dbo.US_mxtwmq_rec;  
      
          
SELECT *  
into  dbo.US_mxtwmq_rec  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[mxtwmq_rec];  
  
END  
-- select * from US_mxtwmq_rec
GO
