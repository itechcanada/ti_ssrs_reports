USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_iptrps]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Aug 6, 2015,>    
-- Description: <Description,gl account,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[US_iptrps]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  --   IF OBJECT_ID('dbo.US_iptrps_rec', 'U') IS NOT NULL    
  --drop table dbo.US_iptrps_rec;     
-- Insert statements for procedure here    
--SELECT * into  dbo.US_iptrps_rec from [LIVEUSSTX].[liveusstxdb].[informix].[iptrps_rec];     
truncate table dbo.US_iptrps_rec;
-- Insert statements for procedure here    
insert into dbo.US_iptrps_rec select * from [LIVEUSSTX].[liveusstxdb].[informix].[iptrps_rec];
      
END
/*
20201105	Sumit
Comment drop statement, implement truncate and insert statement
*/
-- select * from US_iptrps_rec  
GO
