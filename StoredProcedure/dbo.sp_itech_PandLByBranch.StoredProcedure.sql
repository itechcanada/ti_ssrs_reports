USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_PandLByBranch]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Bablu >        
-- Create date: <11 NOV 2013>        
-- Description: <Getting top 50 customers for SSRS reports>        
-- Last changed date <18 Feb 2015>      
-- Last changed description : add the joining to get cus_adm_brh and apply filter     
-- Last change Date: 29 Jun 2015  
-- Last changes By: Bablu  
-- Last changes Desc: Remove the live connection of database   
-- Last change Date: 20 Jan 2016  
-- Last changes By: Bablu  
-- Last changes Desc: Include interco option in filter   
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_PandLByBranch]  @DBNAME varchar(50), @AcctPeriod varchar(6), @BranchName Varchar(30)        
        
AS        
BEGIN        
  
  
 SET NOCOUNT ON;        
declare @sqltxt1 varchar(8000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
DECLARE @IsExcInterco char(1)  
set @DB=  @DBNAME        

  --select 1 as Ctr, @AcctPeriod As AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  --join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  --join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct
  --UNION  
  --select 2 as Ctr,@AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  --join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  --join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct
  --UNION  
 -- select 3 as Ctr,@AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
 -- sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
 -- join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
 -- join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
 -- where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
--  group by  pl_cat, subAccountDesc, gld_sacct
 -- UNION 
 
  select 4 as Ctr, @AcctPeriod, subAccountDesc, gld_bsc_gl_acct as gld_sacct , pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 5 as Ctr, @AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  --UNION
--    select 6 as Ctr, @AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
--  sum(gld_cr_amt - gld_dr_amt) *-1 as Amount from  
--   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
--   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
--  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
--  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
--  group by  pl_cat, subAccountDesc, gld_sacct 
--UNION
  --  select 7 as Ctr, @AcctPeriod,subAccountDesc, '' as gld_sacct, pl_cat as AccountType , 
  --sum(gld_cr_amt - gld_dr_amt)  as Amount from  
  -- tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
  -- on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  --left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  --where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  --group by  pl_cat, subAccountDesc, gld_sacct 


  order by Ctr, gld_sacct;

     
END        
        
-- exec [sp_itech_PandLByBranch]'US' , '202109','LAX'
-- select distinct gld_acctg_per from US_glhgld_rec order by gld_acctg_per desc
-- select subAccountDesc from tbl_itech_US_SubAccounts

GO
