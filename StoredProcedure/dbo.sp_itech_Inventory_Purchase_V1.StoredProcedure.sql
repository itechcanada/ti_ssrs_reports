USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Inventory_Purchase_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <20 NOV 2013>            
-- Description: <Getting Inventory Purchase Details for SSRS reports>            
-- Last Changes: Add frm grd and byr             
-- Last updated Date: 24 jun 2015         
-- Last changes by mukesh date Jun 30, 2015      
          
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_Inventory_Purchase_V1] @FromDate datetime, @ToDate datetime,  @DBNAME varchar(50), @version char = '0', @frm Varchar(10) = '' , @grd Varchar(10) = ''           
            
AS            
BEGIN         
  
             
 SET NOCOUNT ON;            
declare @sqltxt1 varchar(8000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
            
set @DB=  @DBNAME            
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)            
            
IF @frm = 'ALL' OR  @frm = ''            
BEGIN            
Set @frm = ''            
END            
            
IF @grd = 'ALL' OR @grd = ''            
BEGIN             
Set @grd = ''            
END            
            
CREATE TABLE #tmp (  [Database]   VARCHAR(10)            
     , Company   VARCHAR(3)            
     , ref_pfx   VARCHAR(2)            
     , ref_no   numeric            
     , ref_itm   numeric            
     , po_pfx   VARCHAR(2)            
     , po_no    numeric         
              
     , ref_sbitm   numeric            
     , actvy_dt   Date            
     , cst_desc20  VARCHAR(20)            
     , cst_um   VARCHAR(60)            
     , cst_qty   numeric            
     , bas_cry_val  numeric            
     , frm    Varchar(10)            
     , grd    Varchar(10)         
     , size   Varchar(20)            
     , fnsh    Varchar(10)            
     , byr    Varchar(5)       
     ,voucher Varchar(20)      
     ,VendorInv Varchar(25)    
     ,mill varchar(3)     
      , VendorName   VARCHAR(40)          
                 );             
            
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(35);            
DECLARE @Name VARCHAR(15);            
            
IF @DBNAME = 'ALL'            
 BEGIN            
  IF @version = '0'            
  BEGIN            
   DECLARE ScopeCursor CURSOR FOR            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName            
    OPEN ScopeCursor;            
  END            
  ELSE            
  BEGIN            
   DECLARE ScopeCursor CURSOR FOR            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS             
    OPEN ScopeCursor;            
  END            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query1 NVARCHAR(max);               
      SET @DB= @Prefix             
       SET @query1 ='INSERT INTO #tmp ([Database], Company, ref_pfx, ref_no, ref_itm, po_pfx, po_no, ref_sbitm, actvy_dt            
            , cst_desc20, cst_um, cst_qty, bas_cry_val, frm, grd, size, fnsh, byr, voucher, VendorInv,mill,VendorName)            
                   SELECT distinct  ''' +  @DB + ''' as [Database]            
         , tdc_cmpy_id            
         , tdc_ref_pfx            
         , tdc_ref_no            
         , tdc_ref_itm            
         , rhr_po_pfx            
         , rhr_po_no            
         , tdc_ref_sbitm            
         , tdc_actvy_dt            
         , tdc_cst_desc20            
         , cast(tdc_cst as Varchar) + ''/'' + tdc_cst_um AS cst_um             
         , tdc_cst_qty             
         , tdc_bas_cry_val            
         , phr_frm            
         , phr_grd         
         , phr_size            
         , phr_fnsh          
         , phr_byr          
        ,jvc_vchr_no as voucherNo      
         , jvc_ven_inv_no as VendorInvNo, itd_mill as mill,    
         (SElect ven_ven_nm from ' + @DB + '_aprven_rec Where ven_cmpy_id = phr_cmpy_id and ven_ven_id = phr_ven_id)    
         from  ' + @DB + '_Injtdc_rec      
         left join ' + @DB + '_injitd_rec on itd_cmpy_id = tdc_cmpy_id and itd_ref_pfx =  tdc_ref_pfx and itd_ref_no = tdc_ref_no and itd_ref_itm = tdc_ref_itm    
         and itd_ref_sbitm = tdc_ref_sbitm and itd_actvy_dt = tdc_actvy_dt and itd_trs_seq_no = tdc_ref_ln_no               
         left join ' + @DB + '_pohrhr_rec on rhr_cmpy_id = tdc_cmpy_id and rhr_ref_pfx = tdc_ref_pfx and rhr_ref_no = tdc_ref_no and rhr_ref_itm = tdc_ref_itm            
         left join ' + @DB + '_pohphr_rec on phr_cmpy_id = rhr_cmpy_id and phr_po_pfx = rhr_po_pfx and phr_po_no = rhr_po_no and phr_po_itm = rhr_po_itm and phr_po_crtd_dt = rhr_po_crtd_dt      
         and (select COUNT(*) from ' + @DB + '_potpod_rec where phr_cmpy_id = pod_cmpy_id and pod_po_pfx = phr_po_pfx and pod_po_no = phr_po_no and pod_po_itm = rhr_po_itm    
          and pod_ven_id = phr_ven_id) > 0        
            left join ' + @DB + '_apjjci_rec on jci_cmpy_id = tdc_cmpy_id and jci_trs_pfx = tdc_ref_pfx and jci_trs_no = tdc_ref_no and jci_trs_itm = tdc_ref_itm      
           left join ' + @DB + '_apjjvc_rec on jvc_cmpy_id = jci_cmpy_id and jci_vchr_pfx =  jvc_vchr_pfx and jci_vchr_no =  jvc_vchr_no                           
         where tdc_ref_pfx = ''RC'' and CONVERT(VARCHAR(10), tdc_actVy_dt , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), tdc_actVy_dt , 120) <= '''+ @TD +'''            
         and (phr_frm = ''' + @frm + ''' OR ''' + @frm + ''' = '''') and (phr_grd = ''' + @grd + ''' OR ''' + @grd + ''' = '''') ;'            
       print @query1;            
        EXECUTE sp_executesql @query1;             
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN            
    if @DB = 'PS'            
    BEGIN            
    SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix = + ''+ @DB + '')            
    END            
    else            
    BEGIN            
    SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix = + ''+ @DB + '')            
    END            
                 
        SET @sqltxt1 ='INSERT INTO #tmp ([Database], Company, ref_pfx, ref_no, ref_itm, po_pfx, po_no, ref_sbitm, actvy_dt            
            , cst_desc20, cst_um, cst_qty, bas_cry_val , frm, grd, size, fnsh, byr, voucher, VendorInv,mill,VendorName)            
                   SELECT  distinct ''' +  @DB + ''' as [Database]            
         , tdc_cmpy_id            
         , tdc_ref_pfx             
         , tdc_ref_no            
         , tdc_ref_itm            
         , rhr_po_pfx            
         , rhr_po_no            
         , tdc_ref_sbitm            
         , tdc_actvy_dt            
         , tdc_cst_desc20            
         , cast(tdc_cst as Varchar) + ''/'' + tdc_cst_um AS cst_um             
         , tdc_cst_qty             
         , tdc_bas_cry_val            
         , phr_frm            
         , phr_grd          
         , phr_size           
         , phr_fnsh          
         , phr_byr       
          ,jvc_vchr_no as voucherNo      
         , jvc_ven_inv_no as VendorInvNo , itd_mill as mill  ,    
         (SElect ven_ven_nm from ' + @DB + '_aprven_rec Where ven_cmpy_id = phr_cmpy_id and ven_ven_id = phr_ven_id)    
                       
         from  ' + @DB + '_Injtdc_rec      
         left join ' + @DB + '_injitd_rec on itd_cmpy_id = tdc_cmpy_id and itd_ref_pfx =  tdc_ref_pfx and itd_ref_no = tdc_ref_no and itd_ref_itm = tdc_ref_itm    
         and itd_ref_sbitm = tdc_ref_sbitm and itd_actvy_dt = tdc_actvy_dt and itd_trs_seq_no = tdc_ref_ln_no         
         left join ' + @DB + '_pohrhr_rec on rhr_cmpy_id = tdc_cmpy_id and rhr_ref_pfx = tdc_ref_pfx and rhr_ref_no = tdc_ref_no and rhr_ref_itm = tdc_ref_itm            
         left join ' + @DB + '_pohphr_rec on phr_cmpy_id = rhr_cmpy_id and phr_po_pfx = rhr_po_pfx and phr_po_no = rhr_po_no and phr_po_itm = rhr_po_itm and phr_po_crtd_dt = rhr_po_crtd_dt        
         and (select COUNT(*) from ' + @DB + '_potpod_rec where phr_cmpy_id = pod_cmpy_id and pod_po_pfx = phr_po_pfx and pod_po_no = phr_po_no and pod_po_itm = rhr_po_itm    
          and pod_ven_id = phr_ven_id) > 0     
           left join ' + @DB + '_apjjci_rec on jci_cmpy_id = tdc_cmpy_id and jci_trs_pfx = tdc_ref_pfx and jci_trs_no = tdc_ref_no and jci_trs_itm = tdc_ref_itm      
           left join ' + @DB + '_apjjvc_rec on jvc_cmpy_id = jci_cmpy_id and jci_vchr_pfx =  jvc_vchr_pfx and jci_vchr_no =  jvc_vchr_no               
         where tdc_ref_pfx = ''RC'' and CONVERT(VARCHAR(10), tdc_actVy_dt , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), tdc_actVy_dt , 120) <= '''+ @TD +'''            
         and (phr_frm = ''' + @frm + ''' OR ''' + @frm + ''' = '''') and (phr_grd = ''' + @grd + ''' OR ''' + @grd + ''' = '''');'            
     print(@sqltxt1)             
   EXEC (@sqltxt1);            
   END            
               
select * from #tmp              
   drop table #tmp            
END            
            
-- exec [sp_itech_Inventory_Purchase_V1] '11/17/2016', '12/19/2017' , 'ALL'            
-- exec [sp_itech_Inventory_Purchase_V1] '01/01/2017', '10/31/2017' , 'US',1, '' ,''            
-- exec [sp_itech_Inventory_Purchase_V1] '11/01/2017', '11/30/2016' , 'US',1, 'ASRD' , '4140'      
    
/*    
20161115    
Sub: Inv Purch History for audit vendor invoice 2015    
Addede new column Vendor name    
    
    
    
*/
GO
