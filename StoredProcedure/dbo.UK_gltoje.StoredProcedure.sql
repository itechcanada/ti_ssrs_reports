USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_gltoje]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,08/03/2016,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_gltoje]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_gltoje_rec', 'U') IS NOT NULL  
  drop table dbo.UK_gltoje_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT oje_oje_pfx	,oje_oje_no	,oje_jrnl_ent_typ	,oje_actvy_dt	,oje_lgr_id	,oje_src_jrnl	,oje_ssn_id	,oje_lgn_id	,'' as oje_nar_desc	,
oje_trs_src,	oje_glc_id	,oje_transl_sts	,oje_fm_acctg_per	,oje_to_acctg_per	,oje_lst_per_pstd	,oje_orig_ref_pfx	,oje_orig_ref_no	,
oje_orig_ref_itm	,oje_orig_ref_sbitm	,oje_orig_cry	,oje_orig_exrt	,oje_gl_extl_id1	,oje_gl_extl_id2	,oje_gl_extl1	,
oje_gl_extl2	,oje_gl_extl_desc
into  dbo.UK_gltoje_rec  
  from [LIVEUKGL].[liveukgldb].[informix].[gltoje_rec] ;  
    
END  
  
-- Select * from UK_gltoje_rec  

GO
