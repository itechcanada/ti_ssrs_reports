USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ReplacementPricingProduct]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  <Mukesh >                
-- Create date: <16 Dec 2016>                
-- Description: <Get inventory results with size and finish>               
            
              
-- =============================================                
CREATE PROCEDURE [dbo].[sp_itech_ReplacementPricingProduct] @DBNAME varchar(50)  -- ,@FromDate datetime, @ToDate datetime                   
                
AS                
BEGIN                
                 
 SET NOCOUNT ON;                
declare @sqltxt varchar(6000)                
declare @execSQLtxt varchar(7000)                
declare @DB varchar(100)                
declare @FD1 varchar(10)        
declare @TD1 varchar(10)                  
declare @FD varchar(10)                
 declare @6FD varchar(10)                
declare @TD varchar(10)                
declare @ExcessFD varchar(10)    
declare @ExcessTD varchar(10)                
DECLARE @ExchangeRate varchar(15)           
DECLARE @CurrenyRate varchar(15)             
                
set @DB=  @DBNAME         
 set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 1, 0) , 120) -- First day of previous month   
 set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120) -- Last day of previous month                
 set @FD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FD),120)    -- current month from date  
set @TD1 = @FD   
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)     --First day of previous 6 month                
              
CREATE TABLE #tmp (   [Database]   VARCHAR(10)      
  , SizeDesc     Varchar(65)            
        , Form     Varchar(65)                
        , Grade     Varchar(65)                
        , Size     Varchar(65)                
        , Finish    Varchar(65)   
        , ReplCost DECIMAL(20, 2)     
        , LstReplCostUpdateDT Varchar(10)  
        , OhdStock DECIMAL(20, 0)    
        , Months6InvoicedWeight   DECIMAL(20, 0)    
        , AvgDaysInventory     Varchar(10)   
        , ExcessInventory Decimal (20,0)               
        , OpenPOWgt DECIMAL(20, 0)                
         , AvailableWeight Decimal (20,0)                                
                 );                 
                
DECLARE @DatabaseName VARCHAR(35);                
DECLARE @Prefix VARCHAR(35);                
DECLARE @Name VARCHAR(15);          
     
  
              
                
IF @DBNAME = 'ALL'                
 BEGIN                
     DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
     WHILE @@FETCH_STATUS = 0                
       BEGIN                
        DECLARE @query NVARCHAR(max);                   
      SET @DB= @Prefix         
    
               
      IF (UPPER(@DB) = 'TW')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
   End                  
   Else if (UPPER(@DB) = 'NO')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
   End                  
   Else if (UPPER(@DB) = 'CA')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
   End                  
   Else if (UPPER(@DB) = 'CN')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
 End                  
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
   begin            
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
   End                  
   Else if(UPPER(@DB) = 'UK')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
   End       
   Else if(UPPER(@DB) = 'DE')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
   End       
       
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
   Begin         
                  
       SET @query ='  
     INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight , AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight)                
   select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,   
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)     
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc             
      ) as MonthlyAvgReplCost,    
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,               
       sum(prd_ohd_wgt * 2.20462)   ,                
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh             
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
         (  select Avg(datediff(D, pcr_agng_dtts, getdate() ))  from ' + @DB + '_intpcr_rec  
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size   
and inp.prd_fnsh = prm_fnsh  
)  as   AvgDaysInventory,            
    
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then   
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess  
  FROM     
  (   
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size  
and sixMonthWgt.sat_fnsh = PRm_fnsh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM   
and prd_grd = Prm_GRD  
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''  
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg    
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD  
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh  
       
) as oquery ),                       
        
       (select sum(potpod_rec.pod_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM                
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  ,
     (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) ))* 2.20462 as availableWgt                  
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                       
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
      --  Where  prd_invt_sts = ''S''               
       group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc,PRm_fnsh'                                         
       
   End  
   Else   
   Begin  
  set @query='  
     INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight , AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight)                
   select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,   
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)     
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc             
      ) as MonthlyAvgReplCost,    
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,            
       sum(prd_ohd_wgt)   ,                
       (Select SUM(SAT_BLG_WGT ) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh             
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
         (  select Avg(datediff(D, pcr_agng_dtts, getdate() ))  from ' + @DB + '_intpcr_rec  
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size   
and inp.prd_fnsh = prm_fnsh  
)  as   AvgDaysInventory,            
    
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then   
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess  
  FROM     
  (   
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size  
and sixMonthWgt.sat_fnsh = PRm_fnsh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM   
and prd_grd = Prm_GRD  
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''  
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg    
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD  
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh  
       
) as oquery ),                       
        
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM                
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  ,
        (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt ))) as availableWgt               
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                       
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
     --   Where  prd_invt_sts = ''S''               
       group by PRm_FRM, Prm_GRD,PRm_size,prm_size_desc, PRm_fnsh'                
   End    
      print @query;                
        EXECUTE sp_executesql @query;                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
       END                 
    CLOSE ScopeCursor;                
    DEALLOCATE ScopeCursor;                
  END    
  ELSE                
     BEGIN            
        
   IF (UPPER(@DB) = 'TW')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@DB) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@DB) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@DB) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@DB) = 'UK')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End        
    Else if(UPPER(@DB) = 'DE')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End        
               
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
   Begin         
                  
       SET @sqltxt ='INSERT INTO #tmp ([Database], SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight , AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight)                
   select ''' +  @DB + '''  as [Database], RTRIM(LTRIM(prm_size_desc)), PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,   
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)     
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh   order by ppb_rct_expy_dt desc            
      ) as MonthlyAvgReplCost,    
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,                        
       sum(prd_ohd_wgt * 2.20462)   ,                
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh             
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
         (  select Avg(datediff(D, pcr_agng_dtts, getdate() ))  from ' + @DB + '_intpcr_rec  
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size   
and inp.prd_fnsh = prm_fnsh  
)  as   AvgDaysInventory,            
    
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then   
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess  
  FROM     
  (   
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size  
and sixMonthWgt.sat_fnsh = PRm_fnsh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM   
and prd_grd = Prm_GRD  
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''  
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg    
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD  
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh  
       
) as oquery ),                       
        
       (select sum(potpod_rec.pod_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM                
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  ,
        (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) ))* 2.20462 as availableWgt               
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                       
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
     --   Where  prd_invt_sts = ''S''               
       group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc,PRm_fnsh'                              
   End                
   Else                
   Begin        
             
                   
        SET @sqltxt ='INSERT INTO #tmp ([Database],SizeDesc,Form,Grade,Size,Finish,ReplCost, LstReplCostUpdateDT , OhdStock   , Months6InvoicedWeight , AvgDaysInventory , ExcessInventory  , OpenPOWgt,AvailableWeight)                
   select ''' +  @DB + '''  as [Database],RTRIM(LTRIM(prm_size_desc)),  
    PRm_FRM as Form, PRm_GRD as Grade, PRm_size, PRm_fnsh ,   
        (Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +')/2.20462 else ((ppb_repl_cst +  ppb_frt_in_cst)   * '+ @CurrenyRate +') end)     
       as replacementCost  from ' + @DB + '_perppb_rec  where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh order by ppb_rct_expy_dt desc              
      ) as MonthlyAvgReplCost,    
       (select MAX(ppb_rct_expy_dt) from ' + @DB + '_perppb_rec where ppb_FRM = PRm_FRM and ppb_GRD = PRm_GRD  and ppb_size = prm_size and ppb_fnsh = prm_fnsh ) as  LstReplCostUpdateDT,            
       sum(prd_ohd_wgt)   ,                
       (Select SUM(SAT_BLG_WGT ) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD and prm_size = sat_size and prm_fnsh = sat_fnsh             
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,                
        (  select Avg(datediff(D, pcr_agng_dtts, getdate() ))  from ' + @DB + '_intpcr_rec  
join  ' + @DB + '_intprd_rec inp on inp.prd_itm_ctl_no = pcr_itm_ctl_no   where inp.prd_frm=prm_frm and inp.prd_grd= prm_grd and inp.prd_size = prm_size   
and inp.prd_fnsh = prm_fnsh  
) as   AvgDaysInventory,            
    
  (select  (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else case when (prd_ohd_wg - InvoiceMonthTotal) >0  then   
  (prd_ohd_wg - InvoiceMonthTotal) else 0 end  end)  as excess  
  FROM     
  (   
  SELECT  ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = PRm_FRM and sixMonthWgt.sat_grd = Prm_GRD and sixMonthWgt.sat_size = PRm_size  
and sixMonthWgt.sat_fnsh = PRm_fnsh and   
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = PRm_FRM   
and prd_grd = Prm_GRD  
and prd_size = PRm_size and prd_fnsh = PRm_fnsh and  prd_invt_sts = ''S''  
and UpdateDtTm between ''' + @FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg    
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm = PRm_FRM and  currentPrdData.stn_grd = Prm_GRD  
and currentPrdData.stn_size = Prm_size and currentPrdData.stn_fnsh = Prm_fnsh  
       
) as oquery ),                       
        
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM                
       and ipd_GRD = PRm_GRD and ipd_size = prm_size and ipd_fnsh = prm_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  ,
       (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as availableWgt
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh                       
        left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
      --  Where  prd_invt_sts = ''S''               
       group by PRm_FRM, Prm_GRD,PRm_size, prm_size_desc, PRm_fnsh'                
   End                
                   
                    
                          
     print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt);                
   END                
    
select [Database],  Rtrim(Ltrim(Form)) + '/'+ Rtrim(Ltrim(Grade)) + '/' + Rtrim(Ltrim(SizeDesc)) + '/' + Rtrim(Ltrim(Finish)) as 'Product',   
Form, Grade, Size,   Finish,              
 ReplCost,    
  LstReplCostUpdateDT ,   
   OhdStock,   
 Months6InvoicedWeight,   AvgDaysInventory , ExcessInventory  ,            
 OpenPOWgt ,AvailableWeight              
 from #tmp    ;             
                                 
   drop table #tmp  ;     
END                
-- exec [sp_itech_ReplacementPricingProduct] 'US' ;    
-- exec [sp_itech_ReplacementPricingProduct] 'ALL' ;        
 /*  
 Date: 20161223  
 Sub: Need an Adjustment Please  
 
 date: 20161227
 sub: New Report Request - Replacement Pricing Product Report

date:20170104
sub:Replacement Cost   
   (Sum(prd_ohd_wgt - prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt ))
   
date:20170118
Sub:Problem with replacement cost   
   
 */
GO
