USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlySummaryByBranch_IND]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <12 Dec 2018>        
-- Description: <Getting OTP>        
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_MonthlySummaryByBranch_IND] @Month as Datetime      
        
AS        
BEGIN        
SET NOCOUNT ON;        
        
--1 New Accounts        
--2 Dormant Accounts 3M        
--3 Dormant Accounts 6M        
--4 Lost Accounts        
--5 Total Reactivated Accounts        
--6 Current Month Total Active Customers     
--10 Target Toatl Active Customers       
--0 ALL        
        
CREATE TABLE #tmpIND (     id int,
 AccountType varchar(50)        
,CustomerID  int        
,Category  VARCHAR(100) 
,AccountName VARCHAR(150)        
,AccountDate varchar(15)   
 ,Months varchar(15)     
      ,Branch  VARCHAR(15)        
      ,SalePersonName  VARCHAR(50)        
        );        
--CREATE TABLE #tmpFinal (      AccountType varchar(50)        
--                        ,CustomerID  VARCHAR(15)        
--      ,AccountName VARCHAR(150)        
--      ,AccountDate varchar(15)        
--      ,FirstSaleDate Varchar(15)        
--      ,Branch  VARCHAR(15)        
--      ,Category  VARCHAR(100)        
--      ,SalePersonName  VARCHAR(50)        
--      ,Months varchar(15)        
--      ,DatabaseName Varchar(3)        
--        );                
declare @start DATE = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month), 0) , 120);        


 insert into #tmpIND ( id, AccountType,  CustomerID, Category,AccountName,AccountDate,Months,Branch,SalePersonName)
 exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_IND 'UK','IND',@start,'ALL',0
 
 insert into #tmpIND
 exec sp_itech_GetNew_Dormant_Lost_Account_Combine_ByBranch_IND 'US','IND',@start,'ALL',0;
 
         
select * from #tmpIND;
drop table #tmpIND;         
     
END        
        
-- exec [sp_itech_MonthlySummaryByBranch_IND] '2019-06-07'     
/*    
Mail sub:Monthly Summary By Branch - IND was executed at 5/6/2019 7:00:04 AM    
*/  
GO
