USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Customer_Snap_Shot_Product]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <31 Oct 2017>    
-- Description: <sp_itech_Customer_Snap_Shot>   
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_Customer_Snap_Shot_Product]  @DBNAME varchar(3), @CustomerID Varchar(10), @Year Varchar(4)
AS    
BEGIN    
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @CurrenyRate varchar(15);      
DECLARE @StartDate VARCHAR(10);      
DECLARE @EndDate varchar(10);    
    
CREATE TABLE #temp ( Dbname   VARCHAR(10)    
     ,CusID   VARCHAR(10)    
     , Product Varchar(300)
     ,Wgt   decimal(20,2)     
     --,CusLongNm  VARCHAR(40)    
     --,Branch   VARCHAR(3)    
     --,TotalMtlVal decimal(20,0)    
     --,Profit   decimal(20,2)    
     --, NetPct decimal(20,0)   
     --, NetAvgAmt decimal(20,0)   
     --,Market Varchar(35)   
     --,MtlAvgVal decimal(20,0)  
     );    
  
 if @CustomerID ='ALL'          
 BEGIN          
 set @CustomerID = ''          
 END    
    
IF @DBNAME = 'ALL'    
 BEGIN    
     
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
    OPEN ScopeCursor;    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   DECLARE @query NVARCHAR(MAX);    
   IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
        
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK') --UPPER(@Prefix) = 'TW' OR     
    BEGIN    
     SET @query = 'INSERT INTO #temp (Dbname,CusID,Product,Wgt)    
      SELECT '''+ @Prefix +''' as Country, stn_sld_cus_id,  RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) ,SUM(stn_blg_wgt)*(2.20462)
      FROM ' + @Prefix + '_sahstn_rec 
      join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
       left JOIN '+ @Prefix +'_inrprm_rec on prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh 
      where Year(stn_inv_dt)= ''' + @Year +'''
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''
      group by stn_sld_cus_id,RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))' 
         
    END    
    ELSE    
    BEGIN 
    
     SET @query = 'INSERT INTO #temp (Dbname,CusID,Product,Wgt)    
      SELECT '''+ @Prefix +''' as Country, stn_sld_cus_id, RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)),SUM(stn_blg_wgt)
      FROM ' + @Prefix + '_sahstn_rec 
      join ' + @Prefix + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
      left JOIN '+ @Prefix +'_inrprm_rec on prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh 
      where Year(stn_inv_dt)= ''' + @Year +'''
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''
      group by stn_sld_cus_id,RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) ' 
         
    END    
   print @query;    
   EXECUTE sp_executesql @query;    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  END       
 CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
ELSE    
BEGIN    
 IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
        
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )
   BEGIN  
    
   SET @sqltxt ='INSERT INTO #temp (Dbname,CusID,Product,Wgt)    
       SELECT '''+ @DBNAME +''' as Country, stn_sld_cus_id, RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)),SUM(stn_blg_wgt)*(2.20462)
      FROM ' + @DBNAME + '_sahstn_rec 
      join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
      left JOIN '+ @DBNAME +'_inrprm_rec on prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh 
      where Year(stn_inv_dt)= ''' + @Year +'''
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''
      group by stn_sld_cus_id,RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))'   
       print(@sqltxt)  
END         
  ELSE    
  BEGIN    
            
  SET @sqltxt ='INSERT INTO #temp (Dbname,CusID,Product,Wgt)    
      SELECT '''+ @DBNAME +''' as Country, stn_sld_cus_id, RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)),SUM(stn_blg_wgt)
      FROM ' + @DBNAME + '_sahstn_rec 
      join ' + @DBNAME + '_arrcus_rec on cus_cus_id = stn_sld_cus_id      
       left JOIN '+ @DBNAME +'_inrprm_rec on prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh 
      where Year(stn_inv_dt)= ''' + @Year +'''
      and (stn_sld_cus_id = ''' + @CustomerID + ''' or ''' + @CustomerID + ''' = '''') and stn_frm <> ''XXXX''
      group by stn_sld_cus_id,RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))
      '  
      print(@sqltxt)    
   
  END    
    set @execSQLtxt = @sqltxt;             
   EXEC (@execSQLtxt);
END    
 SELECT top 20 * FROM #temp order by Wgt desc;    
 DROP TABLE  #temp ;    
END    
    
--EXEC [sp_itech_Customer_Snap_Shot_Product] 'US' ,'11930','2016'   
--EXEC [sp_itech_Customer_Snap_Shot_Product] 'UK' ,'1738'
--EXEC [sp_itech_Customer_Snap_Shot_Product] 'ALL','1738'    
  
  
/*  
sp_itech_GetTop40CustomerNoGrouping_WithEmail  sp_itech_GetTop40CustomerCurrentYTD_WithEmail sp_itech_GetTop40CustomerPreviousYTD_WithEmail
*/
GO
