USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CycleCountReport]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                          
-- Author:  <MRinal >                          
-- Create date: <29 July 2015>                          
-- Description: <Get inventory results with size and finish>                         
-- Last changes Date: 29 July 2015                        
-- Last update Date: 29 July 2015                       
-- Last updated : Get monthly replacement average cost directly from perppb_rec table                        
-- Last updated by : Mrinal Jha                        
                        
-- =============================================                          
CREATE PROCEDURE [dbo].[sp_itech_CycleCountReport]  (@DBNAME varchar(50),@Warehouse varchar(3), @Zone varchar(20), @Location varchar(20), @Form  varchar(20),@Grade varchar(20), @Size  varchar(20),@Finish varchar(20)  )                          
                          
AS                          
BEGIN                          
                           
                         
declare @sqltxt varchar(6000)                          
declare @execSQLtxt varchar(7000)                          
declare @DB varchar(100)                          
DECLARE @CurrenyRate varchar(15)                      
                          
set @DB=  @DBNAME                          
--set @FD = select CONVERT(VARCHAR(10), DateAdd(mm, -12, GetDate()) , 120)                          
                                       
                          
CREATE TABLE #tmp ([Database]   VARCHAR(10)             
         , Warehouse VARCHAR(3)                          
        , Zone varchar(50)            
        , Location varchar(50)            
        , Form     Varchar(65)                          
        , Grade     Varchar(65)                          
        , Size     Varchar(65)                          
        , Finish    Varchar(65)                
        , OnHandQty Varchar(100)              
        , Tag varchar(50)                      
        , UnitCost   DECIMAL(20, 4)       
        ,Unit Varchar(5)                         
        , TotalValue   DECIMAL(20, 2)                          
                              
                 );                           
                          
DECLARE @DatabaseName VARCHAR(35);                          
DECLARE @Prefix VARCHAR(35);                          
DECLARE @Name VARCHAR(15);                    
                        
       -- select * from #tmp               
 IF (@DBNAME = 'ALL' )            
 BEGIN            
 -- SELECT prd_whs,prd_orig_zn,prd_loc,prd_frm,prd_grd,prd_size,prd_fnsh, prd_ohd_qty,prd_tag_no,prd_ohd_mat_cst, prd_ohd_mat_val            
  --FROM US_intprd_rec            
              
  DECLARE ScopeCursor CURSOR FOR            
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS            
    OPEN ScopeCursor;            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query NVARCHAR(Max);               
      SET @DB= @Prefix                  
                      
                         
      IF (UPPER(@DB) = 'TW')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
   End                            
   Else if (UPPER(@DB) = 'NO')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
   End                            
   Else if (UPPER(@DB) = 'CA')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
   End                            
   Else if (UPPER(@DB) = 'CN')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
 End                            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )           
   begin                      
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
   End                           
   Else if(UPPER(@DB) = 'UK')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
   End  
   Else if(UPPER(@DB) = 'DE')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
   End 
                  
                  
                  
      SET @query ='INSERT INTO #tmp ([Database],Warehouse,Zone,Location,Form,Grade,Size,Finish,OnHandQty,Tag,UnitCost,Unit,TotalValue)                          
               SELECT ''' +  @DB + ''' as [Database], prd_whs, loc_whs_zn, prd_loc, prd_frm , prd_grd , prd_size             
               , prd_fnsh, prd_ohd_qty,prd_tag_no              
               ,      
               CASE prd_ohd_qty      
               WHEN 0 THEN 0 ELSE      
               isnull(SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) ), SUM(prd_ohd_mat_val )) / prd_ohd_qty END as unitCost      
                ,mat_e_qty_cst_um      
               , isnull(SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) ), SUM(prd_ohd_mat_val )) as TotVal      
               FROM ' + @DB + '_intprd_rec        
                join ' + @DB + '_inrmat_rec on mat_frm = prd_frm and mat_grd = prd_grd               
               left join ' + @DB + '_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool    
               left join ' + @DB + '_inrloc_rec on loc_cmpy_id  = prd_cmpy_id and loc_loc = prd_loc and loc_whs = prd_whs           
               where   (prd_whs = '''+ @Warehouse +''' or '''+ @Warehouse +'''= '''')            
               And (loc_whs_zn = ''' + @Zone + ''' OR ''' + @Zone + ''' = '''')             
               And (prd_loc = ''' + @Location + ''' OR ''' + @Location + ''' = '''')            
               And (prd_frm = ''' + @Form + ''' OR ''' + @Form + ''' = '''')            
               And (prd_grd = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')            
               And (prd_size = ''' + @Size + ''' OR ''' + @Size + ''' = '''')            
         And (prd_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
         AND prd_invt_sts = ''S''   and prd_brh not in (''SFS'') and prd_whs not in (''SFS'')           
         GROUP BY prd_whs, loc_whs_zn, prd_loc, prd_frm , prd_grd , prd_size             
               , prd_fnsh, prd_ohd_qty,prd_tag_no,prd_ohd_mat_cst ,mat_e_qty_cst_um '            
                     
          print( @query)                
                      
        EXECUTE sp_executesql @query;            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
                 
 END               
             
 ELSE            
 BEGIN                  
                      
                         
      IF (UPPER(@DB) = 'TW')                            
   begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
   End                            
   Else if (UPPER(@DB) = 'NO')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
   End                            
   Else if (UPPER(@DB) = 'CA')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
   End                            
   Else if (UPPER(@DB) = 'CN')                            
   begin                            
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
 End                            
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                            
   begin                      
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
   End                            
   Else if(UPPER(@DB) = 'UK')                            
 begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
   End
   Else if(UPPER(@DB) = 'DE')                            
	begin                            
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
   End 
             
             
 SET @sqltxt ='INSERT INTO #tmp ([Database],Warehouse,Zone,Location,Form,Grade,Size,Finish,OnHandQty,Tag,UnitCost,Unit,TotalValue)                          
           SELECT ''' +  @DB + ''' as [Database], prd_whs, loc_whs_zn, prd_loc, prd_frm , prd_grd , prd_size             
               , prd_fnsh, prd_ohd_qty,prd_tag_no        
               ,      
               CASE prd_ohd_qty      
               WHEN 0 THEN 0 ELSE      
               isnull(SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) ), SUM(prd_ohd_mat_val )) / prd_ohd_qty END as unitCost      
               ,mat_e_qty_cst_um      
               , isnull(SUM(prd_ohd_mat_val ) + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) ), SUM(prd_ohd_mat_val )) as TotVal      
                FROM ' + @DB + '_intprd_rec        
                join ' + @DB + '_inrmat_rec on mat_frm = prd_frm and mat_grd = prd_grd           
               left join ' + @DB + '_intacp_rec on  prd_avg_cst_pool = acp_avg_cst_pool   
               left join ' + @DB + '_inrloc_rec on loc_cmpy_id  = prd_cmpy_id and loc_loc = prd_loc and loc_whs = prd_whs            
               where   (prd_whs = '''+ @Warehouse +''' or '''+ @Warehouse +'''= '''')            
               And (loc_whs_zn = ''' + @Zone + ''' OR ''' + @Zone + ''' = '''')             
             And (prd_loc = ''' + @Location + ''' OR ''' + @Location + ''' = '''')            
               And (prd_frm = ''' + @Form + ''' OR ''' + @Form + ''' = '''')            
               And (prd_grd = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')            
               And (prd_size = ''' + @Size + ''' OR ''' + @Size + ''' = '''')            
         And (prd_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')      
         AND prd_invt_sts = ''S''     and prd_brh not in (''SFS'') and prd_whs not in (''SFS'')      
         GROUP BY prd_whs, loc_whs_zn, prd_loc, prd_frm , prd_grd , prd_size             
               , prd_fnsh, prd_ohd_qty,prd_tag_no,prd_ohd_mat_cst,mat_e_qty_cst_um '            
                           
   print( @sqltxt)                           
   set @execSQLtxt = @sqltxt;                           
   EXEC (@execSQLtxt);            
               
 END              
                    
   SELECT [Database],Warehouse as prd_whs,Zone as prd_orig_zn,Location as prd_loc,Form as prd_frm,Grade as prd_grd,Size as prd_size            
   ,Finish AS prd_fnsh,OnHandQty AS prd_ohd_qty,Tag AS prd_tag_no,  
   cast(UnitCost as varchar(30)) + '/' + Unit AS prd_ohd_mat_cst,TotalValue AS prd_ohd_mat_val FROM #tmp  
   --where Zone is not null;                
                 
     DROP table #tmp                   
END                
            
            
--exec [sp_itech_CycleCountReport] 'US', 'sfs','','','' ,'','','US'            
--exec [sp_itech_CycleCountReport] 'ALL', '','','','' ,'','',''  
/*
20210128	Sumit
Add Germany Database
*/
GO
