USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyKPI_Market]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <22 June 2016>        
-- Description: <Getting Monthly  Operations KPI  Cover Sheet SSRS reports By Market>        
    
-- =============================================        
 CREATE PROCEDURE [dbo].[sp_itech_MonthlyKPI_Market] @DBNAME varchar(50),@Branch varchar(10),@FromDate datetime, @ToDate datetime, @version char = '0'      
        
AS        
BEGIN        
         
 SET NOCOUNT ON;        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)         
declare @NFD varchar(10)      
declare @NTD varchar(10)      
declare @D3MFD varchar(10)      
declare @D3MTD varchar(10)      
declare @D6MFD varchar(10)      
declare @D6MTD varchar(10)      
declare @LFD varchar(10)      
declare @LTD varchar(10)     
declare @AFD varchar(10)      
declare @ATD varchar(10)      
declare @FD2 varchar(10)        
declare @TD2 varchar(10)    
declare @RFD varchar(10)    
declare @RTD varchar(10)    
    
declare @ExcessFD varchar(10)    
declare @ExcessTD varchar(10)    
     
set @DB=  @DBNAME         
 set @FD = CONVERT(VARCHAR(10), @FromDate,120)    
 set @TD = CONVERT(VARCHAR(10), @ToDate,120)     
     
 set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @RTD = CONVERT(VARCHAR(10), @ToDate,120)   -- New Account      
     
 set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)  -- New Account      
      
       set @D3MFD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate)-3, 0),120)   -- Dormant 3 Month  correct    
       set @D3MTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-3, -1),120)  -- Dormant 3 Month correct     
      
       set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-6,0)) , 120)   --Dormant 6 Month      
       set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-5,0)), 120)  -- Dormant 6 Month      
           
    
  set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date    
  set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date    
    
       set @ExcessFD =  CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)    
       set @ExcessTD =   @FD    
       print 'ExFD ' + @ExcessTD    
           
       set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)   -- Lost Account      
       set @LTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-18, -1),120)  -- Lost Account  correct    
                 
       set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)    -- Active Accounts       
       set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)      
      
    
CREATE TABLE #tmp1 (    Databases   VARCHAR(15)        
        , Branch   VARCHAR(3)         
         ,ShippedWGT    DECIMAL(20, 0)        
        , ShippedValue        DECIMAL(20, 0)        
        , GrossProfit Decimal(20,0)       
        , GProfitPercentage    DECIMAL(20, 1)        
        ,Market Varchar(30)  
                   );      
                       
        
        
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @Prefix VARCHAR(5);        
DECLARE @Name VARCHAR(15);        
        
       
        
if @Branch ='ALL'        
 BEGIN        
 set @Branch = ''        
 END        
        
DECLARE @CurrenyRate varchar(15);        
        
IF @DBNAME = 'ALL'        
 BEGIN        
         
     IF @version = '0'    
  BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
    OPEN ScopeCursor;    
  END    
  ELSE    
  BEGIN    
         
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
End          
          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
      SET @DB= @Prefix        
     -- print(@DB)        
        DECLARE @query NVARCHAR(4000);        
         IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End 
	Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End 
          
   
               
        SET @query = 'INSERT INTO #tmp1 (Databases, Branch,ShippedWGT, ShippedValue, GrossProfit, GProfitPercentage,Market)        
      select   '''+ @DB +''' as Databases, stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as LBShipped,'                
           ELSE                
            SET @query = @query + ' SUM(stn_blg_wgt) as LBShipped,'                
                           
           SET @query = @query + 'SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as Shipments,SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as GP$MTD,    
           (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct, cuc_desc30 as Market   
                           
           from '+ @DB +'_sahstn_rec           
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id     
                    
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
           where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''           
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null ) and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
           AND stn_shpt_brh NOT IN(''NOR'',''SFS'' )          
           group by stn_shpt_brh ,cuc_desc30      
            '        
                 
          
            
               
     print(@query)          
        EXECUTE sp_executesql @query;        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
       print 'starting' ;        
      IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DB ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End
	Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End

     print 'Ending';        
     print @CurrenyRate ;        
              
          SET @sqltxt = 'INSERT INTO #tmp1 (Databases, Branch,ShippedWGT, ShippedValue, GrossProfit, GProfitPercentage,Market)        
      select   '''+ @DB +''' as Databases, stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as LBShipped,'                
           ELSE                
            SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as LBShipped,'                
                           
           SET @sqltxt = @sqltxt + 'SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as Shipments,SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as GP$MTD,    
           (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct, cuc_desc30  as Market  
                           
           from '+ @DB +'_sahstn_rec           
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id     
                    
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
           where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''           
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null ) and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
           AND stn_shpt_brh NOT IN(''NOR'',''SFS'' )          
           group by stn_shpt_brh ,cuc_desc30   '        
                  
       END         
            
     
             
  print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
  EXEC (@execSQLtxt);        
             
    -- To get records for Market such as aerospace , Industrial, medical, Oil and gas  
    CREATE TABLE #tmp2 (    Databases   VARCHAR(15)        
        , Branch   VARCHAR(3)         
        ,AerospaceSales$ DECIMAL(20, 0)   
  ,AerospaceLBSSold DECIMAL(20, 0)  
  ,AerospaceNetProfit$ Decimal(20,0)   
  ,AerospaceNetProfitMargin  DECIMAL(20, 1)   
  ,IndustrialSales$ DECIMAL(20, 0)   
  ,IndustrialLBSSold DECIMAL(20, 0)  
  ,IndustrialNetProfit$ Decimal(20,0)   
  ,IndustrialNetProfitMargin  DECIMAL(20, 1)   
  ,MedicalSales$ DECIMAL(20, 0)   
  ,MedicalLBSSold DECIMAL(20, 0)  
  ,MedicalNetProfit$ Decimal(20,0)   
  ,MedicalNetProfitMargin  DECIMAL(20, 1)   
  ,OilandGasSales$ DECIMAL(20, 0)   
  ,OilandGasLBSSold DECIMAL(20, 0)  
  ,OilandGasNetProfit$ Decimal(20,0)   
  ,OilandGasNetProfitMargin  DECIMAL(20, 1)   
  ,ComptitorandMillSales$ DECIMAL(20, 0)   
  ,ComptitorandMillLBSSold DECIMAL(20, 0)  
  ,ComptitorandMillNetProfit$ Decimal(20,0)   
  ,ComptitorandMillProfitMargin  DECIMAL(20, 1)    
  );    
    
  insert into #tmp2(Databases,Branch) select distinct Databases,Branch from #tmp1  
   
  update #tmp2 set #tmp2.AerospaceSales$ = #tmp1.ShippedValue, #tmp2.AerospaceLBSSold = #tmp1.ShippedWGT, #tmp2.AerospaceNetProfit$ = #tmp1.GrossProfit,   
  #tmp2.AerospaceNetProfitMargin = #tmp1.GProfitPercentage  
  from #tmp2 join #tmp1 on #tmp2.Databases = #tmp1.Databases and #tmp2.Branch = #tmp1.Branch and ltrim(rtrim(#tmp1.Market)) = 'Aerospace'  
    
  update #tmp2 set #tmp2.IndustrialSales$ = #tmp1.ShippedValue, #tmp2.IndustrialLBSSold = #tmp1.ShippedWGT, #tmp2.IndustrialNetProfit$ = #tmp1.GrossProfit,   
  #tmp2.IndustrialNetProfitMargin = #tmp1.GProfitPercentage  
  from #tmp2 join #tmp1 on #tmp2.Databases = #tmp1.Databases and #tmp2.Branch = #tmp1.Branch and ltrim(rtrim(#tmp1.Market)) = 'Industrial'  
    
  update #tmp2 set #tmp2.MedicalSales$ = #tmp1.ShippedValue, #tmp2.MedicalLBSSold = #tmp1.ShippedWGT, #tmp2.MedicalNetProfit$ = #tmp1.GrossProfit,   
  #tmp2.MedicalNetProfitMargin = #tmp1.GProfitPercentage  
  from #tmp2 join #tmp1 on #tmp2.Databases = #tmp1.Databases and #tmp2.Branch = #tmp1.Branch and ltrim(rtrim(#tmp1.Market)) = 'Medical'  
    
  update #tmp2 set #tmp2.OilandGasSales$ = #tmp1.ShippedValue, #tmp2.OilandGasLBSSold = #tmp1.ShippedWGT, #tmp2.OilandGasNetProfit$ = #tmp1.GrossProfit,   
  #tmp2.OilandGasNetProfitMargin = #tmp1.GProfitPercentage  
  from #tmp2 join #tmp1 on #tmp2.Databases = #tmp1.Databases and #tmp2.Branch = #tmp1.Branch and ltrim(rtrim(#tmp1.Market)) = 'Oil & Gas'  
     
   update #tmp2 set #tmp2.ComptitorandMillSales$ = #tmp1.ShippedValue, #tmp2.ComptitorandMillLBSSold = #tmp1.ShippedWGT, #tmp2.ComptitorandMillNetProfit$ = #tmp1.GrossProfit,   
  #tmp2.ComptitorandMillProfitMargin = #tmp1.GProfitPercentage  
  from #tmp2 join #tmp1 on #tmp2.Databases = #tmp1.Databases and #tmp2.Branch = #tmp1.Branch and ltrim(rtrim(#tmp1.Market)) = 'Competitor & Mill'  
   
 select * from #tmp2 where ([databaseS]!='US' or BRANCH!='TAI');  
 drop table #tmp1;  
  drop table #tmp2;    
 -- where  (Branch not in ('TAI') or [Databases] = 'US') ;    
  -- select * from #tmp1  
 -- select * from #NLDAccount;        
  -- drop table #tmp1    ;    
END        
        
-- exec [sp_itech_MonthlyKPI_Market] 'ALL','ALL','2016-03-01', '2016-03-31'     
-- exec [sp_itech_MonthlyKPI_Market] 'US','ALL','2016-06-01', '2016-06-30'     
-- exec [sp_itech_MonthlyKPI_Market] 'ALL','ALL','2016-05-01', '2016-05-31'  
-- exec [sp_itech_MonthlyKPI_Market] 'ALL','ALL', '2016-01-01', '2016-04-30'    
    
    
-- titanium product CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,       
    
/*  
20161214  
Mail:   
mail body: Prabh the issue is we are missing PSM SALES information or November and December just add PSM to the KPI REPORT for tomorrow and everything should correct it self for 2015   
  
20161212  
Mail:Quick Check - Thanks  
Add CRP branch in this report  
  
-- 2016-07-22  
Adding new market Competitor & Mill  
having mail sub: Report Modification Request - Monthly Operations KPI Report  
   
-- 2016-05-12    
Prabh please modify and correct this report as indicated in addition apply the excess calculation logic used in the excess report we create to this report “J” column. We additionally would like to create a process or a simple program that will enable a  
location the ability to input their shop floor hours,  which would be accumulated for the KPI report using the hour value in the KPI report  enabling  the other fields to be calculated. Also, please use the excess inventory calculations in the aged inventory 
sales report if you have any question please feel free to contact me. These change should be at a higher level of priority please as we been playing with this report to long no one fault but we see the finish line thanks.     
20210127	Sumit
Add germany database
*/  
GO
