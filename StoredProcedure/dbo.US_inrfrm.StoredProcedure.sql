USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_inrfrm]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[US_inrfrm] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_inrfrm_rec', 'U') IS NOT NULL
		drop table dbo.US_inrfrm_rec;
    
        
SELECT *
into  dbo.US_inrfrm_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[inrfrm_rec];

END
-- select * from US_inrfrm_rec
GO
