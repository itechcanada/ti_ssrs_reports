USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerComposition_CED]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Mukesh>          
-- Create date: <27 Feb 2019>          
-- Description: <Invoice Summary CED>         
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_CustomerComposition_CED]  @DBNAME varchar(50), @Branch Varchar(3), @OSSlp varchar(4)         
AS          
BEGIN          
        
        
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
declare @DB varchar(100);          
declare @sqltxt varchar(6000);          
declare @execSQLtxt varchar(7000);          
DECLARE @CountryName VARCHAR(25);             
DECLARE @prefix VARCHAR(15);             
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @CurrenyRate varchar(15);            
declare @FDPrv varchar(10);                
declare @TDPrv varchar(10);      
declare @FDCur varchar(10);      
declare @TDCur varchar(10) ;           
          
--CREATE TABLE #tmpCustList ( Dbname   VARCHAR(10)          
--     ,CusID   VARCHAR(10)       
--     ,customerName Varchar(15)      
--     ,OSlp  VARCHAR(4)           
--     );        
           
CREATE TABLE #tmpSales ( Dbname   VARCHAR(10)          
     ,CusID   VARCHAR(10)      
     ,TotalInvoice Decimal(20,2)      
     ,InvoiceDate Varchar(10)  
     ,customerName Varchar(15)     
     );            
           
CREATE TABLE #tmpFinal ( Dbname   VARCHAR(10)       
  ,CusID VARCHAR(10)      
  ,customerName Varchar(15)         
     ,CurrentInvoice Decimal(20,2)      
     );                 
      
CREATE TABLE #tmpFinal1 ( Dbname   VARCHAR(10)       
  ,customerName Varchar(15)         
     ,CurrentInvoice Decimal(20,2)      
     ,seq varchar(2)      
     );       
      
           
set @FDPrv = CONVERT(VARCHAR(10), GETDATE()-360,120)            
set @TDPrv = CONVERT(VARCHAR(10), GETDATE()-181,120)        
set @FDCur = CONVERT(VARCHAR(10), GETDATE()-180,120)            
set @TDCur = CONVERT(VARCHAR(10), GETDATE(),120)        
    
    
if @Branch ='ALL'                    
 BEGIN                    
 set @Branch = ''                    
 END     
         
  
if @OSSlp ='ALL'                  
 BEGIN                  
 set @OSSlp = ''                  
 END   
          
IF @DBNAME = 'ALL'          
 BEGIN          
           
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;          
  WHILE @@FETCH_STATUS = 0          
  BEGIN          
   DECLARE @query NVARCHAR(MAX);          
   IF (UPPER(@Prefix) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@Prefix) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@Prefix) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@Prefix) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@Prefix) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@Prefix) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
              
 --    SET @query = 'INSERT INTO #tmpCustList (Dbname, CusID,customerName, OSlp)        
 -- select distinct '''+ @Prefix +''', stn_sld_cus_id  as CustID, cus_cus_nm, shp_os_slp      
 --from   ' + @Prefix + '_sahstn_rec       
 --join ' + @Prefix + '_arrcus_rec  on cus_cus_id = stn_sld_cus_id         
 --     left JOIN ' + @Prefix + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id        
 --      where stn_inv_Dt >=''' + @FDPrv + '''  and stn_inv_dt <''' + @TDPrv + '''  and (shp_os_slp = '''+ @OSSlp +''' or '''+ @OSSlp +'''= '''')    
 --      AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')      
 --       group by  stn_sld_cus_id, cus_cus_nm, shp_os_slp'          
 --  print @query;          
 --  EXECUTE sp_executesql @query;         
         
    SET @query = 'INSERT INTO #tmpSales (Dbname, CusID, InvoiceDate, TotalInvoice,customerName )        
  select distinct '''+ @Prefix +''', stn_sld_cus_id  as CustID, stn_inv_dt,SUM(stn_tot_val * '+ @CurrenyRate +'),cus_cus_nm       
 from   ' + @Prefix + '_sahstn_rec        
 join ' + @Prefix + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no       
  left join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id        
       where stn_inv_Dt >=''' + @FDPrv + '''  and stn_inv_dt <''' + @TDCur + '''  and stn_frm <> ''XXXX''      
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
       and STN_SHPT_BRH not in (''SFS'') group by  stn_sld_cus_id, cus_cus_nm,stn_inv_dt;'          
   print @query;          
   EXECUTE sp_executesql @query;        
         
         
          
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;          
  END             
 CLOSE ScopeCursor;          
  DEALLOCATE ScopeCursor;          
 END          
ELSE          
BEGIN          
       
 IF (UPPER(@DBNAME) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@DBNAME) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@DBNAME) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@DBNAME) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@DBNAME) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@DBNAME) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End          
    Else if(UPPER(@DBNAME) = 'TWCN')          
    begin          
       SET @DB ='TW'          
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
              
 -- SET @sqltxt ='INSERT INTO #tmpCustList (Dbname, CusID,customerName, OSlp)        
 -- select distinct '''+ @DBNAME +''', stn_sld_cus_id  as CustID, cus_cus_nm, shp_os_slp      
 --from   ' + @DBNAME + '_sahstn_rec       
 --join ' + @DBNAME + '_arrcus_rec  on cus_cus_id = stn_sld_cus_id         
 --     left JOIN ' + @DBNAME + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id        
 --      where stn_inv_Dt >=''' + @FDPrv + '''  and stn_inv_dt <''' + @TDPrv + '''  and (shp_os_slp = '''+ @OSSlp +''' or '''+ @OSSlp +'''= '''')    
 --       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
 --       group by  stn_sld_cus_id, cus_cus_nm, shp_os_slp'          
              
 --print(@sqltxt)          
 --set @execSQLtxt = @sqltxt;           
 --EXEC (@execSQLtxt);        
       
 SET @sqltxt ='INSERT INTO #tmpSales (Dbname, CusID, InvoiceDate, TotalInvoice,customerName )        
  select distinct '''+ @DBNAME +''', stn_sld_cus_id  as CustID, stn_inv_dt,SUM(stn_tot_val * '+ @CurrenyRate +'),cus_cus_nm       
 from   ' + @DBNAME + '_sahstn_rec        
 join ' + @DBNAME + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no      
 left join ' + @DBNAME + '_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id        
       where stn_inv_Dt >=''' + @FDPrv + '''  and stn_inv_dt <''' + @TDCur + '''  and stn_frm <> ''XXXX''     
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
       and STN_SHPT_BRH not in (''SFS'') group by  stn_sld_cus_id, cus_cus_nm,stn_inv_dt;  '         
      
 print(@sqltxt)          
 set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);        
END          
      
      
insert into #tmpFinal      
Select top 5 #tmpSales.Dbname , #tmpSales.CusID, #tmpSales.customerName,       
case when SUM(TotalInvoice)<0 then 0 else SUM(TotalInvoice) end as CurrentInvoice       
from #tmpSales      
where #tmpSales.InvoiceDate >= @FDCur and #tmpSales.InvoiceDate < @TDCur      
group by #tmpSales.Dbname, #tmpSales.CusID, #tmpSales.customerName      
order by CurrentInvoice desc;      
      
insert into #tmpFinal1      
select Dbname,customerName,CurrentInvoice, '1' as seq from #tmpFinal      
union       
Select #tmpSales.Dbname , 'OTHER' as customerName,       
 (SUM(TotalInvoice) - (Select SUM(tf.CurrentInvoice) from #tmpFinal tf)) as CurrentInvoice , '2' as seq      
from #tmpSales      
where #tmpSales.InvoiceDate >= @FDCur and #tmpSales.InvoiceDate < @TDCur      
group by #tmpSales.Dbname      
Order by seq,CurrentInvoice desc;      
      
select *,(CurrentInvoice)/(select SUM(CurrentInvoice) from #tmpFinal1 ) as pct from #tmpFinal1      
group by Dbname,customerName,CurrentInvoice,seq      
Order by seq,CurrentInvoice desc;      
      
      
-- from #tmpCustList;         
drop table #tmpFinal1;      
--DROP TABLE  #tmpCustList ;          
DROP TABLE  #tmpSales ;          
Drop table #tmpFinal;      
END          
          
-- EXEC [sp_itech_CustomerComposition_CED] 'US','ALL','AN'          
-- EXEC [sp_itech_CustomerComposition_CED] 'ALL','ALL','AN' 
/*
Date:20190905
Mail Sub:Urgent Visual KPI's Update Request > Home > STRATIXReports > Development > CED and SPPR Report
*/
GO
