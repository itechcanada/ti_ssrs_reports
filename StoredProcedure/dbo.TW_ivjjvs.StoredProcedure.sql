USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ivjjvs]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[TW_ivjjvs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_ivjjvs_rec', 'U') IS NOT NULL
		drop table dbo.TW_ivjjvs_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.TW_ivjjvs_rec
  from [LIVETWSTX].[livetwstxdb].[informix].[ivjjvs_rec] ; 
  
END
-- select * from TW_ivjjvs_rec 
GO
