USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_AddressType]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <11 NOV 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_AddressType]  @DBNAME varchar(50)  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt1 varchar(8000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
  
set @DB=  @DBNAME  
  
CREATE TABLE #tmp (   
     [Database]   VARCHAR(10)  
     , addr_typ   VARCHAR(1)  
                );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query1 NVARCHAR(max);     
      SET @DB= @Prefix   
       SET @query1 ='INSERT INTO #tmp ([Database], addr_typ)  
                     
                   SELECT  Distinct ''' +  @DB + ''' as [Database],  
         cva_addr_typ  
         from  ' + @DB + '_scrcva_rec;'  
       print @query1;  
        EXECUTE sp_executesql @query1;   
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
        SET @sqltxt1 ='INSERT INTO #tmp ([Database], addr_typ)  
                     
                   SELECT  Distinct ''' +  @DB + ''' as [Database],  
         cva_addr_typ  
         from  ' + @DB + '_scrcva_rec;'  
     print(@sqltxt1)   
   EXEC (@sqltxt1);  
   END  
 
select distinct addr_typ, Case When addr_typ = 'B' then 'Bought From'   
          When addr_typ = 'F' then 'Ship From'   
          When addr_typ = 'I' then 'Invoice To'   
          When addr_typ = 'L' then 'Legal Address'   
          When addr_typ = 'O' then 'Sold To'   
          When addr_typ = 'P' then 'Payment To'   
          When addr_typ = 'S' then 'Shipping To'   
        Else '' End AS Name  
 from #tmp    
   drop table #tmp  
END  
  
-- exec [sp_itech_AddressType]'US'  
--• B – Bought From  
--• F – Ship From  
--• I – Invoice To  
--• L – Legal Address  
--• O – Sold To  
--• P – Payment To  
--• S – Shipping To   
  
--Select distinct cva_addr_typ from CN_scrcva_rec;  
GO
