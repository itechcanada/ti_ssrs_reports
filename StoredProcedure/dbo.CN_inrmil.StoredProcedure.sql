USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_inrmil]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Jun 16, 2017  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_inrmil]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.CN_inrmil_rec', 'U') IS NOT NULL  
  drop table dbo.CN_inrmil_rec;  
      
          
SELECT *  
into  dbo.CN_inrmil_rec  
FROM [LIVECNSTX].[livecnstxdb].[informix].[inrmil_rec];  
  
END  
-- select * from CN_inrmil_rec
GO
