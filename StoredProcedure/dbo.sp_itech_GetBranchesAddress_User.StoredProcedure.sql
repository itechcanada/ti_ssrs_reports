USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBranchesAddress_User]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <16 Aug 2019>    
-- Description: <Getting top 50 customers for SSRS reports>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_GetBranchesAddress_User]  @DBNAME varchar(50)  ,@OSSlp varchar(4)    
    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
declare @sqltxt varchar(6000)    
declare @execSQLtxt varchar(7000)    
declare @DB varchar(100)    
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
    
CREATE TABLE #tmp (  BranchName varchar(300)     
        ,BranchAddress Varchar(500)    
        )    
    
   
 if @OSSlp ='ALL'            
 BEGIN            
 set @OSSlp = ''            
 END     
     
IF @DBNAME = 'ALL'    
 BEGIN    
    
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(4000);       
         
      SET @query ='INSERT INTO #tmp ( BranchName,BranchAddress)    
      select RTRIM(LTRIM(brh_nm1)) as branchName, LTRIM(RTrim(brh_addr1)) + LTRIM(RTRIM(brh_addr2)) + LTRIM(RTRIM(brh_addr3)) + '', '' + LTRIM(RTRIM(brh_city))   
      + '', '' + LTRIM(RTRIM(brh_st_prov)) + '' '' + LTRIM(RTRIM(brh_pcd)) + '' '' + LTRIM(RTRIM(brh_cty)) as Address from  '+ @Prefix +'_scrbrh_rec  
      where  (brh_brh = (select usr_usr_brh from '+ @Prefix +'_mxrusr_rec join '+ @Prefix +'_scrslp_rec on usr_lgn_id = slp_lgn_id and slp_actv = 1 and usr_actv = 1 where (slp_slp = '''+ @OSSlp +''') ) )   
        
       '    
        print(@query);    
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN        
     SET @sqltxt ='INSERT INTO #tmp ( BranchName,BranchAddress)    
      select RTRIM(LTRIM(brh_nm1)) as branchName, LTRIM(RTrim(brh_addr1)) + LTRIM(RTRIM(brh_addr2)) + LTRIM(RTRIM(brh_addr3)) + '', '' + LTRIM(RTRIM(brh_city))   
      + '', '' + LTRIM(RTRIM(brh_st_prov)) + '' '' + LTRIM(RTRIM(brh_pcd)) + '' '' + LTRIM(RTRIM(brh_cty)) as Address from  '+ @DBNAME +'_scrbrh_rec   
      where  (brh_brh = (select usr_usr_brh from '+ @DBNAME +'_mxrusr_rec join '+ @DBNAME +'_scrslp_rec on usr_lgn_id = slp_lgn_id and slp_actv = 1 and usr_actv = 1 where (slp_slp = '''+ @OSSlp +''') ) )   
       '    
     print(@sqltxt);     
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
      End    
          
     select * from #tmp where BranchName != '';  
      drop table  #tmp    
END    
    
-- exec sp_itech_GetBranchesAddress_User  'ALL','MMD'    
    --select * from US_mxrusr_rec where usr_nm like '%Mary%'  and usr_lgn_id and usr_usr_brh -- mdownes 
  /*  
  2016-05-19  
  Changed by mukesh  
  */  
GO
