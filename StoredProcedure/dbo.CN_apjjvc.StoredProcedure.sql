USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_apjjvc]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,1/22/2014,>  
-- Description: <Description>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_apjjvc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.CN_apjjvc_rec', 'U') IS NOT NULL        
  drop table dbo.CN_apjjvc_rec;        
            
                
SELECT *        
into  dbo.CN_apjjvc_rec  
FROM [LIVECNSTX].[livecnstxdb].[informix].[apjjvc_rec]  
  
  
END  
  
GO
