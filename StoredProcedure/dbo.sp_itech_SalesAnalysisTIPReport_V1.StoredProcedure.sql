USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisTIPReport_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <11 Feb 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- Last updated by : Mukesh 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisTIPReport_V1] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65),@GPPct as varchar(20),@version char = '0', @IncludeInterco char = '0' , @noOfDays varchar(10) 
  
AS  
BEGIN  
   
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15) 
DECLARE @IsExcInterco char(1) 
  
set @DB=  @DBNAME  

if (@FromDate = '' OR @FromDate = '1900-01-01')
BEGIN  
 set @FromDate = CONVERT(VARCHAR(10),DATEADD(day, -8, GETDATE()), 120) 
 END 

if @ToDate = '' OR @ToDate = '1900-01-01'
BEGIN  
 set @ToDate = CONVERT(VARCHAR(10),DATEADD(day, -1, GETDATE()), 120) 
 END 

if(@noOfDays = '')
Begin
set @noOfDays  = 180;
end

set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
CREATE TABLE #tmp (   CustID   VARCHAR(10)  
        , CustName     VARCHAR(65)  
        ,SalesPersonInside VARCHAR(4)  
        ,SalesPersonOutside VARCHAR(4)
        ,SalesPersonTaken VARCHAR(4)
        , Market   VARCHAR(65)   
        , Branch   VARCHAR(65)   
        , Form           Varchar(65)  
        , Grade           Varchar(65)  
        , TotWeight  DECIMAL(20, 2)  
        , WeightUM           Varchar(10)  
                    , TotalValue    DECIMAL(20, 2)  
        , NetGP               DECIMAL(20, 2)  
        , TotalMatValue    DECIMAL(20, 2)  
        , MatGP    DECIMAL(20, 2)  
        , Databases   VARCHAR(15)   
        , MarketID   integer,  
        Finish varchar(35),  
        Size varchar(35),stn_cry   varchar(5)
        ,InvDate Varchar(10)
        ,InvNo	Varchar(25)
        ,ShptPfx Varchar(2)
        ,ShptNo  Varchar(10)
        ,ShptItm  Varchar(10)
        ,Age	DATeTime 
                 );  
CREATE TABLE #tmpAge (   CustID   VARCHAR(10)  
        , Branch   VARCHAR(65)   
        , Databases   VARCHAR(15)   
        ,InvNo	Varchar(25)
        ,ShptPfx Varchar(2)
        ,ShptNo  Varchar(10)
        ,ShptItm  Varchar(10)
        ,JobNo	Varchar(10) 
                 ); 
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
DECLARE @CurrenyRate varchar(15);  
  
if @Market ='ALL'  
 BEGIN  
 set @Market = ''  
 END  

if ( @IncludeInterco = '0')
BEGIN
set @IsExcInterco ='T'
END
  
IF @DBNAME = 'ALL'  
 BEGIN  
   IF @version = '0'  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
  END  
  ELSE  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
  END  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);     
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
        
       IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
        Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
        Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
        Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
        Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
        Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End  
        Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End  
        -- Calculation of Age 
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,  
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)
      from ' + @DB + '_sahstn_rec 
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '
  print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt); 
        
      if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')     
                BEGIN  
         SET @query ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,ShptPfx,ShptNo,ShptItm,Age)       
          select stn_sld_cus_id as CustID, stn_is_slp,stn_os_slp,stn_tkn_slp,cus_cus_nm as CustName, cuc_desc30 as Market,  
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade, '  
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')     
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'  
             ELSE  
               SET @query = @query + ' SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,'  
          
        SET @query = @query + '  
  
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,  
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,   
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,  
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0 
         where itd_ref_no = ta.JobNo)  as Age 
         from ' + @DB + '_sahstn_rec   
         
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id   
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
          and STN_SHPT_BRH not in (''SFS'')
          group by  stn_sld_cus_id,stn_is_slp,stn_os_slp,stn_tkn_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,   
         stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo     
         order by stn_sld_cus_id '  
    END  
       Else  
      BEGIN  
        SET @query ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,ShptPfx,ShptNo,ShptItm,Age)       
          select stn_sld_cus_id as CustID, stn_is_slp,stn_os_slp,stn_tkn_slp,cus_cus_nm as CustName, cuc_desc30 as Market,  
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,  
        SUM(stn_blg_wgt) as TotWeight,   
        stn_ord_wgt_um as WeightUM,  
  
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,  
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,   
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,  
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm ,
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0 
         where itd_ref_no = ta.JobNo)  as Age
         from ' + @DB + '_sahstn_rec   
         
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm   
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
          and STN_SHPT_BRH not in (''SFS'')
          group by  stn_sld_cus_id,stn_is_slp,stn_os_slp,stn_tkn_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,   
         stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo   
         order by stn_sld_cus_id '  
      End  
        EXECUTE sp_executesql @query;  
          
          
        print(@query)  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN 
     IF @version = '0'  
     BEGIN    
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')  
     END
     ELSE
     BEGIN
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')  
     END
     
    IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End  
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End  
       
   if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')     
                BEGIN  
                
                -- Calculation of Age 
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,  
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)
      from ' + @DB + '_sahstn_rec 
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '
  print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt); 
   
         SET @sqltxt ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken , CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,ShptPfx,ShptNo,ShptItm,Age)       
          select stn_sld_cus_id as CustID,stn_is_slp,stn_os_slp,stn_tkn_slp, cus_cus_nm as CustName, cuc_desc30 as Market,  
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,'  
        if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')     
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'  
             ELSE  
               SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as TotWeight,stn_ord_wgt_um as WeightUM,'   
          
       SET @sqltxt = @sqltxt + '   
  
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,  
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,   
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,  
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm  ,
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0 
         where itd_ref_no = ta.JobNo)  as Age
         from ' + @DB + '_sahstn_rec  
         
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id   
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm   
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
          and STN_SHPT_BRH not in (''SFS'')
          group by  stn_sld_cus_id,stn_is_slp,stn_os_slp,stn_tkn_slp, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,   
         stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt ,ta.JobNo   
         order by stn_sld_cus_id '  
    END  
       Else  
      BEGIN  
      -- Calculation of Age 
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,  
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)
      from ' + @DB + '_sahstn_rec 
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '
  print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt); 
      
        SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,ShptPfx,ShptNo,ShptItm,Age)       
          select stn_sld_cus_id as CustID,stn_is_slp,stn_os_slp,stn_tkn_slp, cus_cus_nm as CustName, cuc_desc30 as Market,  
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,  
        SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,  
  
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,  
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,   
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,  
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size ,stn_inv_dt,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm ,
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0 
         where itd_ref_no = ta.JobNo)  as Age
         from ' + @DB + '_sahstn_rec 
         
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id  
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm 
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''') 
          and STN_SHPT_BRH not in (''SFS'')
          group by  stn_sld_cus_id, stn_is_slp,stn_os_slp, stn_tkn_slp,cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,
         stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo  
         order by stn_sld_cus_id '  
      End  
      print(@sqltxt)  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     END  
     
     if @IsExcInterco ='T'
 BEGIN
     
     print @GPpct;  
     if(@GPPct = '')
     begin
   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,  
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) as 'GP%',InvDate,InvNo,DateDiff(Day,Age,InvDate)  as AgeDays FROM #tmp  Where Market <> 'Interco' and DateDiff(Day,Age,InvDate) >= Convert(integer,@noOfDays) 
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,DateDiff(Day,Age,InvDate)  
   order by CustID   
   end
   else
   begin
   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,  
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) as 'GP%',InvDate,InvNo,DateDiff(Day,Age,InvDate) as AgeDays FROM #tmp   
     where   
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) <= @GPPct  and Market <> 'Interco' and DateDiff(Day,Age,InvDate) >=  Convert(integer,@noOfDays) 
     --CAST( MatGP AS Decimal) <= 100.25  
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size ,InvDate,InvNo,DateDiff(Day,Age,InvDate)
   order by CustID   
   end
   
   end
   
   else
   Begin
   
   print @GPpct;  
     if(@GPPct = '')
     begin
     
   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,  
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) as 'GP%',InvDate,InvNo,Age,DateDiff(Day,Age,InvDate) as AgeDays  FROM #tmp  Where DateDiff(Day,Age,InvDate) >=  Convert(integer,@noOfDays)   
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,InvNo,Age,DateDiff(Day,Age,InvDate)
   order by CustID   
   end
   else
   begin
   SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, TotWeight,'LBS' as WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,  
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) as 'GP%',InvDate,InvNo,Age,DateDiff(Day,Age,InvDate) as AgeDays FROM #tmp   
     where   
     (case TotalValue when 0 then 0 else (MatGP/TotalValue)* 100 end) <= @GPPct   and DateDiff(Day,Age,InvDate) >=  Convert(integer,@noOfDays)  
     --CAST( MatGP AS Decimal) <= 100.25  
   group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size ,InvDate,InvNo,Age,DateDiff(Day,Age,InvDate)
   order by CustID   
   end
   end
  print @GPpct; 
  
  
  drop table #tmp;
drop table #tmpAge;     
END  

-- exec [sp_itech_SalesAnalysisTIPReport_V1] '03/15/2016', '05/15/2016' , 'US','ALL','','1' ,'','100'
/*
date :2016-05-18
mail:option out PSM and especially SFS when all databases is selected.
I thought we discussed this and you were doing this progressively it appears the bulk of these reports do not have this toggle built in an this should now make sure we take out SFS if it was part of any report. Please get someone on this ASAP as we are once again looking like jack asses in the ops meeting!
*/  
 
GO
