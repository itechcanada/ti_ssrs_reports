USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Workflow_Messages]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <25 Apr 2018>    
-- Description: <Workflow Messages>   
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_Workflow_Messages]  @DBNAME varchar(50), @fromDate date, @toDate date   
AS    
BEGIN    
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @CurrenyRate varchar(15);      
declare @FD varchar(10)          
declare @TD varchar(10)    
    
CREATE TABLE #temp ( Dbname   VARCHAR(10)    
     --,LoginID VARCHAR(25)   
     ,MessageDT  VARCHAR(10)  
     ,RefPfx  VARCHAR(2)     
     ,RefNo   numeric    
     ,RefItem numeric    
    -- ,RefSubItem   numeric    
     ,SOPfx  VARCHAR(2)     
     ,SONo   numeric    
     ,SOItem numeric    
    -- ,SOSubItem   numeric   
     --,ProgramNM  VARCHAR(15)    
     --,MessageVal  Varchar(300)    
     
     );    
 set @FD = CONVERT(VARCHAR(10), @fromDate,120)      
 set @TD = CONVERT(VARCHAR(10), @toDate,120)    
    
IF @DBNAME = 'ALL'    
 BEGIN    
      
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
    OPEN ScopeCursor;    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   DECLARE @query NVARCHAR(MAX);    
   IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
        
     
    SET @query = 'INSERT INTO #temp (Dbname,MessageDT,RefPfx,RefNo,RefItem,SOPfx,SONo,SOItem)    
      SELECT '''+ @Prefix +''' as Country, lpr_lst_chg_dtts, lpr_po_pfx, lpr_po_no , lpr_po_itm,
      case when pod_ord_no>0 then pod_ord_pfx else res_ref_pfx end as ord_pfx, case when pod_ord_no>0 then pod_ord_no else res_ref_no end  as ord_no,
      case when pod_ord_no>0 then pod_ord_itm else res_ref_itm end as ord_itm
      from ' + @Prefix + '_potlpr_rec 
      join ' + @Prefix + '_potpod_rec on pod_cmpy_id = lpr_cmpy_id and pod_po_pfx = lpr_po_pfx and pod_po_no = lpr_po_no and pod_po_itm = lpr_po_itm 
	  and pod_po_intl_rls_no = lpr_po_intl_rls_no
	  join ' + @Prefix + '_rvtres_rec on res_cmpy_id = pod_cmpy_id and res_itm_ctl_no = pod_itm_ctl_no
	 where lpr_chg_fct = ''C'' and lpr_lst_chg_dtts >=  ''' + @FD + '''  And lpr_lst_chg_dtts <= ''' + @TD + '''
	 Union
	 SELECT '''+ @Prefix +''' as Country, lpr_lst_chg_dtts, lpr_po_pfx, lpr_po_no , lpr_po_itm,
      case when res_ref_no>0 then res_ref_pfx else pod_ord_pfx end as ord_pfx, case when res_ref_no>0 then res_ref_no else pod_ord_no end  as ord_no, 
      case when res_ref_no>0 then res_ref_itm else pod_ord_itm end as ord_itm
      from ' + @Prefix + '_potlpr_rec 
      join ' + @Prefix + '_potpod_rec on pod_cmpy_id = lpr_cmpy_id and pod_po_pfx = lpr_po_pfx and pod_po_no = lpr_po_no and pod_po_itm = lpr_po_itm 
	  and pod_po_intl_rls_no = lpr_po_intl_rls_no
	  join ' + @Prefix + '_rvtres_rec on res_cmpy_id = pod_cmpy_id and res_itm_ctl_no = pod_itm_ctl_no
	 where lpr_chg_fct = ''C'' and lpr_lst_chg_dtts >=  ''' + @FD + '''  And lpr_lst_chg_dtts <= ''' + @TD + ''''  
    
   print @query;    
   EXECUTE sp_executesql @query;    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  END       
 CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
ELSE    
BEGIN    
  
 --SET @CountryName = (select name from tbl_itech_DatabaseName where Prefix = @DBNAME)    
 IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
        
      
   SET @sqltxt ='INSERT INTO #temp (Dbname,MessageDT,RefPfx,RefNo,RefItem,SOPfx,SONo,SOItem)    
      SELECT '''+ @DBNAME +''' as Country, lpr_lst_chg_dtts, lpr_po_pfx, lpr_po_no , lpr_po_itm,
      case when pod_ord_no>0 then pod_ord_pfx else res_ref_pfx end as ord_pfx, case when pod_ord_no>0 then pod_ord_no else res_ref_no end  as ord_no,
      case when pod_ord_no>0 then pod_ord_itm else res_ref_itm end as ord_itm 
      from ' + @DBNAME + '_potlpr_rec 
      join ' + @DBNAME + '_potpod_rec on pod_cmpy_id = lpr_cmpy_id and pod_po_pfx = lpr_po_pfx and pod_po_no = lpr_po_no and pod_po_itm = lpr_po_itm 
	  and pod_po_intl_rls_no = lpr_po_intl_rls_no
	  join ' + @DBNAME + '_rvtres_rec on res_cmpy_id = pod_cmpy_id and res_itm_ctl_no = pod_itm_ctl_no
	 where lpr_chg_fct = ''C'' and lpr_lst_chg_dtts >=  ''' + @FD + '''  And lpr_lst_chg_dtts <= ''' + @TD + '''
	 Union
	 SELECT '''+ @DBNAME +''' as Country, lpr_lst_chg_dtts, lpr_po_pfx, lpr_po_no , lpr_po_itm,
      case when res_ref_no>0 then res_ref_pfx else pod_ord_pfx end as ord_pfx, case when res_ref_no>0 then res_ref_no else pod_ord_no end  as ord_no, 
      case when res_ref_no>0 then res_ref_itm else pod_ord_itm end as ord_itm 
      from ' + @DBNAME + '_potlpr_rec 
      join ' + @DBNAME + '_potpod_rec on pod_cmpy_id = lpr_cmpy_id and pod_po_pfx = lpr_po_pfx and pod_po_no = lpr_po_no and pod_po_itm = lpr_po_itm 
	  and pod_po_intl_rls_no = lpr_po_intl_rls_no
	  join ' + @DBNAME + '_rvtres_rec on res_cmpy_id = pod_cmpy_id and res_itm_ctl_no = pod_itm_ctl_no
	 where lpr_chg_fct = ''C'' and lpr_lst_chg_dtts >=  ''' + @FD + '''  And lpr_lst_chg_dtts <= ''' + @TD + '''
	 '    
     
 print(@sqltxt)    
 set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
END    
 SELECT distinct * FROM #temp where SOPfx != 'JS' order by MessageDT ;   
 DROP TABLE  #temp ;    
END    
    
--EXEC [sp_itech_Workflow_Messages] 'CN','2018-04-01', '2018-04-30'    
--EXEC [sp_itech_Workflow_Messages] 'ALL','2018-04-01', '2018-04-30'  
GO
