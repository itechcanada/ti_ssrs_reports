USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ProductRanking]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <16 Aug 2016>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_ProductRanking] @DBNAME varchar(50),@Branch  varchar(30)  
  
AS  
BEGIN  
  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(max)  
declare @execSQLtxt varchar(max)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
DECLARE @CurrenyRate varchar(15)    
  
DECLARE @YTDWgt Varchar(Max);     
DECLARE @YTDSale Varchar(Max);    
DECLARE @YTDMtlCst Varchar(Max);    
  
Set @YTDWgt = case Month(GETDATE())  
When 1 then 'msu_tot_wgt_val_37'  
When 2 then 'msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 3 then 'msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 4 then 'msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 5 then 'msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 6 then 'msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 7 then 'msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 8 then 'msu_tot_wgt_val_30 + msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 9 then 'msu_tot_wgt_val_29 + msu_tot_wgt_val_30 + msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 10 then 'msu_tot_wgt_val_28 + msu_tot_wgt_val_29 + msu_tot_wgt_val_30 + msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 11 then 'msu_tot_wgt_val_27 + msu_tot_wgt_val_28 + msu_tot_wgt_val_29 + msu_tot_wgt_val_30 + msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'  
When 12 then 'msu_tot_wgt_val_26 + msu_tot_wgt_val_27 + msu_tot_wgt_val_28 + msu_tot_wgt_val_29 + msu_tot_wgt_val_30 + msu_tot_wgt_val_31 + msu_tot_wgt_val_32 + msu_tot_wgt_val_33 + msu_tot_wgt_val_34 + msu_tot_wgt_val_35 + msu_tot_wgt_val_36 + msu_tot_wgt_val_37'    
End  
  
Set @YTDSale = case Month(GETDATE())   
When 1 then 'msu_tot_sls_val_37'  
When 2 then 'msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 3 then 'msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 4 then 'msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 5 then 'msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 6 then 'msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 7 then 'msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 8 then 'msu_tot_sls_val_30 + msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 9 then 'msu_tot_sls_val_29 + msu_tot_sls_val_30 + msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 10 then 'msu_tot_sls_val_28 + msu_tot_sls_val_29 + msu_tot_sls_val_30 + msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 11 then 'msu_tot_sls_val_27 + msu_tot_sls_val_28 + msu_tot_sls_val_29 + msu_tot_sls_val_30 + msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'  
When 12 then 'msu_tot_sls_val_26 + msu_tot_sls_val_27 + msu_tot_sls_val_28 + msu_tot_sls_val_29 + msu_tot_sls_val_30 + msu_tot_sls_val_31 + msu_tot_sls_val_32 + msu_tot_sls_val_33 + msu_tot_sls_val_34 + msu_tot_sls_val_35 + msu_tot_sls_val_36 + msu_tot_sls_val_37'    
End  
  
Set @YTDMtlCst = case Month(GETDATE())   
When 1 then 'msu_tot_mtl_cst_37'  
When 2 then 'msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 3 then 'msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 4 then 'msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 5 then 'msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 6 then 'msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 7 then 'msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 8 then 'msu_tot_mtl_cst_30 + msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 9 then 'msu_tot_mtl_cst_29 + msu_tot_mtl_cst_30 + msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 10 then 'msu_tot_mtl_cst_28 + msu_tot_mtl_cst_29 + msu_tot_mtl_cst_30 + msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 11 then 'msu_tot_mtl_cst_27 + msu_tot_mtl_cst_28 + msu_tot_mtl_cst_29 + msu_tot_mtl_cst_30 + msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'  
When 12 then 'msu_tot_mtl_cst_26 + msu_tot_mtl_cst_27 + msu_tot_mtl_cst_28 + msu_tot_mtl_cst_29 + msu_tot_mtl_cst_30 + msu_tot_mtl_cst_31 + msu_tot_mtl_cst_32 + msu_tot_mtl_cst_33 + msu_tot_mtl_cst_34 + msu_tot_mtl_cst_35 + msu_tot_mtl_cst_36 + msu_tot_mtl_cst_37'    
End  
  
SET @DB=@DBNAME;  
  
CREATE TABLE #tmp ( Databases varchar(3)    
     ,Branch varchar(3)   
        ,BranchName Varchar(15)  
        ,Form  varchar(35)  
        ,Grade  varchar(35)  
        ,Size  varchar(35)  
        ,Finish  varchar(35)  
        ,Wgt1 decimal(20,0)  
        ,Wgt2 decimal(20,0)  
        ,Sales1  Decimal(20,0)  
        ,Sales2  Decimal(20,0)  
        ,Profit1  Decimal(20,0)  
        ,Profit2  Decimal(20,0)  
        ,OnePct  Decimal(20,1)  
        ,ProductRank  numeric  
        ,CumPct  Decimal(20,0)  
                 );   
IF(@Branch = 'ALL')  
Begin  
Set @Branch = '';  
End  
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
DECLARE @Name VARCHAR(15);  
DECLARE @CusID varchar(max);  
Declare @Value as varchar(500);  
DECLARE @CustIDLength int;   ---SELECT DATALENGTH(yourtextfield)  
  
    
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);   
        IF (UPPER(@Prefix) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin              
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@Prefix) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End     
    Else if(UPPER(@Prefix) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End     
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')            
       BEGIN            
       SET @query = 'INSERT INTO #tmp (Databases, Branch,BranchName,Form,Grade,Size,Finish,Wgt1, Wgt2, Sales1, Sales2, Profit1, Profit2,OnePct,ProductRank,CumPct )  
       SElect '''+ @Prefix + ''' as Databases, msu_sls_brh, brh_brh_nm,msu_frm,msu_grd,msu_size,msu_fnsh,  
      SUM(' + @YTDWgt + ' )* 2.20462, SUM(msu_tot_wgt_val_37)* 2.20462, SUM(' + @YTDSale + ')* ' + @CurrenyRate + ', SUM(msu_tot_sls_val_37)* ' + @CurrenyRate + ',       
    SUM((' + @YTDSale + ') - (' + @YTDMtlCst + ')), SUM(msu_tot_sls_val_37 - msu_tot_mtl_cst_37)  , 0 as OnePct,1 as ProductRank,0 as CumPct    
FROM '+ @Prefix + '_sasmsu_rec, '+ @Prefix + '_scrbrh_rec  
WHERE     
             msu_cmpy_id = brh_cmpy_id AND   msu_sls_brh = brh_brh  
GROUP BY  
msu_sls_brh, brh_brh_nm, msu_frm, msu_grd, msu_size,msu_fnsh  '  
       
     --  * 2.20462           
       END            
       ELSE            
       BEGIN            
        SET @query = 'INSERT INTO #tmp (Databases, Branch,BranchName,Form,Grade,Size,Finish,Wgt1, Wgt2, Sales1, Sales2, Profit1, Profit2,OnePct,ProductRank,CumPct )  
       SElect '''+ @Prefix + ''' as Databases, msu_sls_brh, brh_brh_nm,msu_frm,msu_grd,msu_size,msu_fnsh,  
      SUM( ' + @YTDWgt + '), SUM(msu_tot_wgt_val_37), SUM( ' + @YTDSale + ')* ' + @CurrenyRate + ', SUM(msu_tot_sls_val_37)* ' + @CurrenyRate + ',       
    SUM((' + @YTDSale + ') - (' + @YTDMtlCst + ')), SUM(msu_tot_sls_val_37 - msu_tot_mtl_cst_37)  , 0 as OnePct,1 as ProductRank,0 as CumPct      
FROM '+ @Prefix + '_sasmsu_rec, '+ @Prefix + '_scrbrh_rec  
WHERE     
             msu_cmpy_id = brh_cmpy_id AND   msu_sls_brh = brh_brh  
GROUP BY  
msu_sls_brh, brh_brh_nm, msu_frm, msu_grd, msu_size,msu_fnsh '          
       END            
         
  
                  print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
      IF (UPPER(@DB) = 'TW')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DB) = 'NO')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DB) = 'CA')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DB) = 'CN')              
    begin              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )              
    begin              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DB) = 'UK')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End    
    Else if(UPPER(@DB) = 'DE')              
    begin              
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End    
      
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')            
       BEGIN        
       -- * 2.20462 as Weight      
       SET @sqltxt =  'INSERT INTO #tmp (Databases, Branch,BranchName,Form,Grade,Size,Finish,Wgt1, Wgt2, Sales1, Sales2, Profit1, Profit2,OnePct,ProductRank,CumPct )  
       SElect '''+ @DB + ''' as Databases, msu_sls_brh, brh_brh_nm,msu_frm,msu_grd,msu_size,msu_fnsh,  
      SUM(' + @YTDWgt + ' )* 2.20462, SUM(msu_tot_wgt_val_37)* 2.20462, SUM(' + @YTDSale + ')* ' + @CurrenyRate + ', SUM(msu_tot_sls_val_37)* ' + @CurrenyRate + ',       
    SUM((' + @YTDSale + ') - (' + @YTDMtlCst + ')), SUM(msu_tot_sls_val_37 - msu_tot_mtl_cst_37)  , 0 as OnePct,1 as ProductRank,0 as CumPct    
FROM '+ @DB + '_sasmsu_rec, '+ @DB + '_scrbrh_rec  
WHERE     
             msu_cmpy_id = brh_cmpy_id AND   msu_sls_brh = brh_brh  
GROUP BY  
msu_sls_brh, brh_brh_nm, msu_frm, msu_grd, msu_size,msu_fnsh  '           
       END            
       ELSE            
       BEGIN            
       SET @sqltxt =  'INSERT INTO #tmp (Databases, Branch,BranchName,Form,Grade,Size,Finish,Wgt1, Wgt2, Sales1, Sales2, Profit1, Profit2,OnePct,ProductRank,CumPct )  
        SElect '''+ @DB + ''' as Databases, msu_sls_brh, brh_brh_nm,msu_frm,msu_grd,msu_size,msu_fnsh,  
      SUM( ' + @YTDWgt + '), SUM(msu_tot_wgt_val_37), SUM( ' + @YTDSale + ')* ' + @CurrenyRate + ', SUM(msu_tot_sls_val_37)* ' + @CurrenyRate + ',       
    SUM((' + @YTDSale + ') - (' + @YTDMtlCst + ')), SUM(msu_tot_sls_val_37 - msu_tot_mtl_cst_37)  , 0 as OnePct,1 as ProductRank,0 as CumPct    
FROM '+ @DB + '_sasmsu_rec, '+ @DB + '_scrbrh_rec  
WHERE     
             msu_cmpy_id = brh_cmpy_id AND   msu_sls_brh = brh_brh  
GROUP BY  
msu_sls_brh, brh_brh_nm, msu_frm, msu_grd, msu_size,msu_fnsh '            
       END            
    
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     END  
   SELECT  Databases, Branch,BranchName,(RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(prm_size_desc)) + '/' + RTRIM(LTRIM(Finish))) as product ,Form,Grade,prm_size as Size,Finish,Wgt1, Wgt2, Sales1, Sales2, Profit1, Profit2,
   OnePct,ProductRank,CumPct, (Select isnull(SUM(prd_ohd_Wgt),0)   
   from US_intprd_rec where prd_frm = Form and prd_grd = Grade and prd_size= Size and prd_fnsh =  Finish )   
   as TotalInventory  FROM #tmp join US_inrprm_rec on prm_frm = Form and prm_grd = Grade and prm_size= Size and prm_fnsh =  Finish  Where  (Branch = @Branch OR @Branch = '')   
  and Sales1 <> 0 ;  
  DROP TABLE #tmp  
END  
  
-- exec [sp_itech_ProductRanking] 'US','ALL'  
  
-- select * from US_inrprm_rec  
  
/*  
SELECT  
    
    msu_sls_brh,  
    brh_brh_nm,  
    msu_frm,  
    msu_grd,  
    msu_size,  
    msu_fnsh,  
  
    SUM(msu_tot_wgt_val_36),    -- Sales 1  
    SUM(msu_tot_wgt_val_37),    -- Sales 2  
      
--Sales  
  
    SUM( msu_tot_sls_val_36),    -- Sales 1  
    SUM(msu_tot_sls_val_37),    -- Sales 2  
      
--Profit  
    SUM(msu_tot_sls_val_36 - msu_tot_cst_val_36),    -- Profit 1  
    SUM(msu_tot_sls_val_37 - msu_tot_cst_val_37),    -- Profit 2  
      
  
FROM  
      sasmsu_rec,  
    scrbrh_rec  
  
--217771  
  
WHERE     
             msu_cmpy_id = brh_cmpy_id  
AND        msu_sls_brh = brh_brh  
  
  
GROUP BY  
  
msu_sls_brh,  
brh_brh_nm,  
msu_frm,  
msu_grd,  
msu_size,  
msu_fnsh  
  
*/  
GO
