USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_KPI_OpenOrder_3W]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <24 Jan 2019>            
-- Description: <Getting Open order of customers for SSRS reports>            
         
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_KPI_OpenOrder_3W] @DBNAME varchar(50),@Branch varchar(10)  
            
AS            
BEGIN           
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(7000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)  
declare @FDW1 varchar(10)            
declare @TDW1 varchar(10)            
declare @FDW2 varchar(10)            
declare @TDW2 varchar(10)            
declare @FDW3 varchar(10)            
declare @TDW3 varchar(10)     
declare @FDW4 varchar(10)            
declare @TDW4 varchar(10)   
declare @FDW5 varchar(10)            
declare @TDW5 varchar(10)    
declare @FDW6 varchar(10)            
declare @TDW6 varchar(10)   
declare @FDW7 varchar(10)            
declare @TDW7 varchar(10)        
declare @NOOfCust varchar(15)            
DECLARE @ExchangeRate varchar(15)        
      
set @FDW5 = CONVERT(VARCHAR(10),DATEADD(wk, 1, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))), 120) --first day next week  
set @TDW5 = CONVERT(VARCHAR(10),DATEADD(wk, 2, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))), 120) --last day next week          
  
set @FDW1 = CONVERT(VARCHAR(10), DATEADD(d, -28, @FDW5) , 120) -- First day of previous + 2 week        
set @TDW1 = CONVERT(VARCHAR(10), DATEADD(d, -28, @TDW5) , 120) -- LAst day of previous + 2 week   
  
set @FDW2 = CONVERT(VARCHAR(10), DATEADD(d, -21, @FDW5) , 120) -- First day of previous + 1 week        
set @TDW2 = CONVERT(VARCHAR(10), DATEADD(d, -21, @TDW5) , 120) -- LAst day of previous + 1 week   
  
set @FDW3 = CONVERT(VARCHAR(10), DATEADD(d, -14, @FDW5) , 120) -- First day of previous week        
set @TDW3 = CONVERT(VARCHAR(10), DATEADD(d, -14, @TDW5) , 120) -- LAst day of previous week   
  
set @FDW4 = CONVERT(VARCHAR(10), DATEADD(d, -7, @FDW5) , 120) -- First day of current week        
set @TDW4 = CONVERT(VARCHAR(10), DATEADD(d, -7, @TDW5) , 120) -- LAst day of current week   
  
set @FDW6 = CONVERT(VARCHAR(10), DATEADD(d, 7, @FDW5) , 120) -- First day of next + 1 week         
set @TDW6 = CONVERT(VARCHAR(10), DATEADD(d, 7, @TDW5) , 120) -- LAst day of next + 1 week    
  
set @FDW7 = CONVERT(VARCHAR(10), DATEADD(d, 14, @FDW5) , 120) -- First day of next + 2 week         
set @TDW7 = CONVERT(VARCHAR(10), DATEADD(d, 14, @TDW5) , 120) -- LAst day of next + 2 week   
  
set @FD = CONVERT(VARCHAR(10), @FDW1 , 120)         
set @TD = CONVERT(VARCHAR(10), @TDW7 , 120)  
                 
set @DB= @DBNAME        
            
            
CREATE TABLE #tmp (    
  Databases varchar(10)   
  ,Branch varchar(10)    
   ,OrderNo varchar(10)            
        ,OrderItem   varchar(3)           
        , ChargeValue    DECIMAL(20, 0)    
        , DueDate    varchar(20)   
             
                 );             
    
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(5);            
DECLARE @Name VARCHAR(15);            
DECLARE @CurrenyRate varchar(15);             
    
             
 if @Branch ='ALL'            
 BEGIN            
 set @Branch = ''            
 END            
            
            
IF @DBNAME = 'ALL'            
 BEGIN            
             
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
   OPEN ScopeCursor;              
     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query NVARCHAR(Max);               
      SET @DB= @Prefix            
      IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End               
    Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End               
      SET @query =            
       'INSERT INTO #tmp (Databases,Branch ,ChargeValue,DueDate,OrderNo,OrderItem)        --, BalPcs    
       select distinct ''' + @DB + ''',ord_ord_brh as Branch,  chl_chrg_val * '+ @CurrenyRate +' as ChargeValue, orl_due_to_dt as DueDate  
       ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem --,orl_bal_pcs as BalPcs      
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_arrcuc_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
       ' + @DB + '_ortchl_rec ,' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec  where   
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and  cuc_cus_cat = cus_cus_cat and           
       ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
        and cds_cd=orh_ord_typ       AND         
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm      
        and cht_tot_typ = ''T''              
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and          
       (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
                  
        and  ord_ord_brh not in (''SFS'')          
       '            
            
     print(@query);         
        EXECUTE sp_executesql @query;            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN             
              
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
                 
     IF (UPPER(@DB) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DB) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End              
    Else if(UPPER(@DB) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End              
                
     SET @sqltxt ='INSERT INTO #tmp (Databases,Branch ,ChargeValue,DueDate,OrderNo,OrderItem)     --, BalPcs       
       select distinct ''' + @DB + ''', ord_ord_brh as Branch, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue, orl_due_to_dt as DueDate  
       ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem --,orl_bal_pcs as BalPcs     
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_arrcuc_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
       ' + @DB + '_ortchl_rec, ' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec  where   
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and  cuc_cus_cat = cus_cus_cat and           
       ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
        and cds_cd=orh_ord_typ AND    
        ord_cmpy_id = cht_cmpy_id   AND  ord_ord_pfx = cht_ref_pfx   AND  ord_ord_no  = cht_ref_no    AND  ord_ord_itm = cht_ref_itm      
        and cht_tot_typ = ''T''              
       and orl_bal_qty >= 0             
       and chl_chrg_cl= ''E''            
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
       and orh_sts_actn <> ''C''            
       and ord_ord_pfx <>''QT''             
       and          
       (         
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
       or        
       (orl_due_to_dt is null and orh_ord_typ =''J''))        
               
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
                  
        and  ord_ord_brh not in (''SFS'') order by ord_ord_no,ord_ord_itm,orl_due_to_dt'            
       print('test')  ;        
     print(@sqltxt);             
    set @execSQLtxt = @sqltxt;             
       EXEC (@execSQLtxt);            
     END  
      
    print @FDW1;  
    print @TDW1;  
      
    print @FDW2;  
    print @TDW2;  
      
    print @FDW3;  
    print @TDW3;  
      
    print @FDW4;  
    print @TDW4;  
      
    print @FDW5;  
    print @TDW5;  
      
    print @FDW6;  
    print @TDW6;  
      
    print @FDW7;  
    print @TDW7;  
      
    --select * from #tmp ;  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) -3 as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw1 where tw1.Databases = t1.Databases and tw1.Branch = t1.Branch and tw1.DueDate >= @FDW1 and tw1.DueDate<= @TDW1) as OpenOrderCount  
    , 1 as seq
    from #tmp t1  group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) - 2 as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw2 where tw2.Databases = t1.Databases and tw2.Branch = t1.Branch and tw2.DueDate >= @FDW2 and tw2.DueDate<= @TDW2) as OpenOrderCount  
    , 2 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) - 1  as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw3 where tw3.Databases = t1.Databases and tw3.Branch = t1.Branch and tw3.DueDate >= @FDW3 and tw3.DueDate<= @TDW3) as OpenOrderCount  
    , 3 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw4 where tw4.Databases = t1.Databases and tw4.Branch = t1.Branch and tw4.DueDate >= @FDW4 and tw4.DueDate<= @TDW4) as OpenOrderCount  
    , 4 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) + 1 as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw5 where tw5.Databases = t1.Databases and tw5.Branch = t1.Branch and tw5.DueDate >= @FDW5 and tw5.DueDate<= @TDW5) as OpenOrderCount  
    , 5 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) + 2 as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw6 where tw6.Databases = t1.Databases and tw6.Branch = t1.Branch and tw6.DueDate >= @FDW6 and tw6.DueDate<= @TDW6) as OpenOrderCount  
    , 6 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    union  
    select t1.Databases,t1.Branch ,'Week ' + cast(DATEPART(wk, GETDATE()) + 3 as varchar(2))  as 'Category' ,  
    (select SUM(ChargeValue) from #tmp tw7 where tw7.Databases = t1.Databases and tw7.Branch = t1.Branch and tw7.DueDate >= @FDW7 and tw7.DueDate<= @TDW7) as OpenOrderCount  
    , 7 as seq
    from #tmp t1 group by t1.Databases,t1.Branch  
    
    order by seq;
    
    -- below sql return open order count  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 -2 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw1 where tw1.Databases = t1.Databases and tw1.Branch = t1.Branch and tw1.DueDate >= @FDW1 and tw1.DueDate<= @TDW1) as OpenOrderCount  
    --from #tmp t1  group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 - 1 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw2 where tw2.Databases = t1.Databases and tw2.Branch = t1.Branch and tw2.DueDate >= @FDW2 and tw2.DueDate<= @TDW2) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7  as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw3 where tw3.Databases = t1.Databases and tw3.Branch = t1.Branch and tw3.DueDate >= @FDW3 and tw3.DueDate<= @TDW3) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 + 1 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw4 where tw4.Databases = t1.Databases and tw4.Branch = t1.Branch and tw4.DueDate >= @FDW4 and tw4.DueDate<= @TDW4) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 + 2 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw5 where tw5.Databases = t1.Databases and tw5.Branch = t1.Branch and tw5.DueDate >= @FDW5 and tw5.DueDate<= @TDW5) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 + 3 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw6 where tw6.Databases = t1.Databases and tw6.Branch = t1.Branch and tw6.DueDate >= @FDW6 and tw6.DueDate<= @TDW6) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
    --union  
    --select t1.Databases,t1.Branch ,'Week ' + cast(datepart(day, datediff(day, -1, GETDATE())/7 * 7)/7 + 4 as varchar(2))  as 'Category' ,  
    --(select COUNT(*) from #tmp tw7 where tw7.Databases = t1.Databases and tw7.Branch = t1.Branch and tw7.DueDate >= @FDW7 and tw7.DueDate<= @TDW7) as OpenOrderCount  
    --from #tmp t1 group by t1.Databases,t1.Branch  
      
   drop table #tmp;      
         
END            
            
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@CustomerID varchar(10) = ''            
            
-- exec [sp_itech_KPI_OpenOrder_3W]  'UK','ALL'          
-- exec [sp_itech_KPI_OpenOrder_3W]  'ALL','ALL'  
/*
Date:20190130
Mail sub:Pending Reports Status Update
Can you please change the output to be open order dollar value ($) rather than the open order line count?
*/
GO
