USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenOrderBranch_Date]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- Author:  <Sumit>                  
-- Create date: <30 Sept 2020>                  
-- Description: <Getting Total Open Order for branch with Date>                  
CREATE PROCEDURE [dbo].[sp_itech_OpenOrderBranch_Date] (@DBNAME varchar(50), @FromDate datetime, @ToDate datetime )  
as  
Begin  
  
SET NOCOUNT ON;                  
declare @execSQLtxt nvarchar(max)                  
declare @DB varchar(50)                  
DECLARE @DatabaseName VARCHAR(35);                  
DECLARE @Prefix VARCHAR(5);     
DECLARE @Name VARCHAR(15);                                                  
  
CREATE TABLE #tmp (    
  DataBases varchar(5)                   
        ,Branch Varchar(10)                  
        ,ActivityDate Date                  
        ,TotalOrder   int  
  );  
  
 set @DB=  @DBNAME                                                 
 IF @DBNAME = 'ALL'                                                        
 BEGIN                                                        
  DECLARE ScopeCursor CURSOR FOR                                            
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                             
  OPEN ScopeCursor;                                            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                  
  WHILE @@FETCH_STATUS = 0                                                        
  BEGIN                                                        
            SET @DB= @Prefix     
     
   set @execSQLtxt = 'insert into #tmp (Databases, Branch,ActivityDate,TotalOrder)  
   select '''+ @DB +''', bka_brh, bka_actvy_dt, count(*) from '+ @DB +'_ortbka_rec where  
   (CONVERT(varchar(10),bka_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and     
   (CONVERT(varchar(10),bka_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')    
   and bka_trs_md = ''A'' and bka_ord_pfx = ''SO'' and bka_ord_itm = 1  group by bka_brh, bka_actvy_dt'  
  
   print @execSQLtxt  
   EXECUTE sp_executesql @execSQLtxt;                                                        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                        
  END  
  CLOSE ScopeCursor;                                                        
  DEALLOCATE ScopeCursor;                                          
 END  
 Else  
 Begin  
  set @execSQLtxt = 'insert into #tmp (Databases, Branch,ActivityDate,TotalOrder)  
  select '''+ @DB +''', bka_brh, bka_actvy_dt, count(*) from '+ @DB +'_ortbka_rec where  
  (CONVERT(varchar(10),bka_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and     
  (CONVERT(varchar(10),bka_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')    
  and bka_trs_md = ''A'' and bka_ord_pfx = ''SO'' and bka_ord_itm = 1  group by bka_brh, bka_actvy_dt'  
  
  print @execSQLtxt  
  EXECUTE sp_executesql @execSQLtxt;   
 End  
  
 select databases, Branch, convert(varchar(10), ActivityDate, 103) as OrderDate, TotalOrder from #tmp order by ActivityDate
  
 Drop table #tmp  
End  
  
/*  
exec sp_itech_OpenOrderBranch_Date_Test 'US', '2020-07-01', '2020-09-29'  
exec sp_itech_OpenOrderBranch_Date 'ALL', '2020-09-01', '2020-09-29'  
*/
GO
