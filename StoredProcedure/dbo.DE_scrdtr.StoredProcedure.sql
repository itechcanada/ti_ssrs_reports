USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_scrdtr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Jan 27 2021    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_scrdtr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.DE_scrdtr_rec', 'U') IS NOT NULL    
  drop table dbo.DE_scrdtr_rec;    
        
            
SELECT *    
into  dbo.DE_scrdtr_rec    
FROM [LIVEDESTX].[livedestxdb].[informix].[scrdtr_rec];    
    
END    
--- exec DE_scrpyt    
-- select * from DE_scrdtr_rec
GO
