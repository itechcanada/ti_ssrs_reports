USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ncr_reporting]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================          
-- Author:  <Author,Mrnal Jha>          
-- Create date: <Create Date,18/9/2012,>         
-- Last updated By mukesh       
-- Last updated Date Oct 05, 2015       
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_ncr_reporting] @DBNAME varchar(10), @FromDate datetime, @ToDate datetime          
           
AS          
BEGIN        
  

          
declare @sqltxt varchar(7000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
          
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(35);            
DECLARE @Name VARCHAR(15);            
DECLARE @CurrenyRate varchar(15);           
          
set @DB=  @DBNAME            
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)           
          
          
CREATE TABLE #tmp (             
         Databases   VARCHAR(15)             
        , NitBranch   varchar(10)      
        , NitWhs   varchar(10)        
        ,Rsn Varchar(15)       
        ,RsnFlt Varchar(3)            
        ,NumberOfNcr int             
        ,AmountClaimed  DECIMAL(20, 2)            
        ,ApprovedCreditVal DECIMAL(20, 2)          
        ,MatWeight DECIMAL(20, 2)          
                  );            
          
          
IF @DBNAME = 'ALL'            
  BEGIN          
  DECLARE ScopeCursor CURSOR FOR              
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
  OPEN ScopeCursor;          
            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
        WHILE @@FETCH_STATUS = 0            
       BEGIN            
   DECLARE @query NVARCHAR(max);              
   SET @DB= @Prefix           
   IF (UPPER(@Prefix) = 'TW')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
   End            
   Else if (UPPER(@Prefix) = 'NO')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
   End            
   Else if (UPPER(@Prefix) = 'CA')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
   End            
   Else if (UPPER(@Prefix) = 'CN')            
    begin            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
   End            
   Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')            
    begin            
     SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
   End            
   Else if(UPPER(@Prefix) = 'UK')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
      End           
   Else if(UPPER(@Prefix) = 'DE')            
    begin            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
      End           
                
            
            
   SET @query =  'INSERT INTO #tmp (Databases, NitBranch, NitWhs, Rsn, RsnFlt, NumberOfNcr,AmountClaimed,ApprovedCreditVal,MatWeight)          
         
 SELECT t.db,t.nit_brh, t.nit_whs, t.Rsn, t.RsnFlt, sum(t.Number_Of_NCRs),sum(t.AmountClaimed),sum(t.ApprovedCreditVal),sum(t.MatWeight)        
 FROM         
 (         
   Select ''' + @Name + ''' as db, nit_brh, nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN as Rsn, NCF_FLT as RsnFlt, count(*) AS Number_Of_NCRs,          
 sum(nit_claim_val * '+ @CurrenyRate +') as AmountClaimed, sum(nit_apcr_val * '+ @CurrenyRate +') AS ApprovedCreditVal, 0 as MatWeight          
 FROM '+@DB+'_nctnit_rec          
 JOIN '+@DB+'_nctnhh_rec ON           
 nhh_cmpy_id = nit_cmpy_id           
 AND nhh_ncr_pfx = nit_ncr_pfx           
 AND nhh_ncr_no = nit_ncr_no        
 left Join '+@DB+'_nctncf_rec on      
 ncf_cmpy_id = nit_cmpy_id         
 AND ncf_ncr_pfx = nit_ncr_pfx         
 AND ncf_ncr_no = nit_ncr_no       
 AND ncf_ncr_itm = nit_ncr_itm          
 where nhh_crtd_dtts  >= '''+ @FD +'''          
 AND  nhh_crtd_dtts  <= '''+ @TD +'''          
 group by nit_brh,nit_whs,NCF_RSN_TYP + ''-''+ NCF_RSN ,  NCF_FLT        
          
 Union        
          
 Select ''' + @Name + ''' as db, nit_brh, nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN as Rsn, NCF_FLT as RsnFlt, 0 AS Number_Of_NCRs,        
 0 as AmountClaimed, 0 AS ApprovedCreditVal, SUM(nit_claim_wgt)        
 FROM '+@DB+'_nctnit_rec        
 JOIN '+@DB+'_nctnhh_rec ON         
 nhh_cmpy_id = nit_cmpy_id         
 AND nhh_ncr_pfx = nit_ncr_pfx         
 AND nhh_ncr_no = nit_ncr_no  and nit_mat_dispn = ''S''      
 left Join '+@DB+'_nctncf_rec on      
 ncf_cmpy_id = nit_cmpy_id         
 AND ncf_ncr_pfx = nit_ncr_pfx         
 AND ncf_ncr_no = nit_ncr_no       
 AND ncf_ncr_itm = nit_ncr_itm         
 where nhh_crtd_dtts  >= '''+ @FD +'''        
 AND  nhh_crtd_dtts  <= '''+ @TD +'''         
 group by nit_brh,nit_whs,NCF_RSN_TYP + ''-''+ NCF_RSN ,  NCF_FLT        
 ) t GROUP BY db,nit_brh,nit_whs,Rsn,RsnFlt        
         
  '          
          
          
                
  EXECUTE sp_executesql @query;            
        print(@query)              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                 
                
  END            
   CLOSE ScopeCursor;            
      DEALLOCATE ScopeCursor;          
                
                
END   -- All Database block end here          
          
          
ELSE  -- Single database query start here          
 BEGIN          
           
 Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')          
           
 IF (UPPER(@DBNAME) = 'TW')            
 begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))            
 End            
 Else if (UPPER(@DBNAME) = 'NO')            
 begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))            
 End            
 Else if (UPPER(@DBNAME) = 'CA')            
 begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))            
 End            
 Else if (UPPER(@DBNAME) = 'CN')            
 begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))            
 End            
 Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')            
 begin            
  SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))            
 End            
 Else if(UPPER(@DBNAME) = 'UK')     begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))            
 End            
 Else if(UPPER(@DBNAME) = 'DE')     begin            
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))            
 End            
           
 SET @sqltxt ='INSERT INTO #tmp (Databases, NitBranch, NitWhs, Rsn, RsnFlt, NumberOfNcr,AmountClaimed,ApprovedCreditVal,MatWeight)          
         
 SELECT t.db,t.nit_brh, t.nit_whs, t.Rsn, t.RsnFlt, sum(t.Number_Of_NCRs),sum(t.AmountClaimed),sum(t.ApprovedCreditVal),sum(t.MatWeight)        
 FROM         
 (         
   Select ''' + @Name + ''' as db, nit_brh, nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN  as Rsn, NCF_FLT as RsnFlt, count(*) AS Number_Of_NCRs,          
 sum(nit_claim_val * '+ @CurrenyRate +') as AmountClaimed, sum(nit_apcr_val * '+ @CurrenyRate +') AS ApprovedCreditVal, 0 as MatWeight          
 FROM '+@DBNAME+'_nctnit_rec          
 JOIN '+@DBNAME+'_nctnhh_rec ON           
 nhh_cmpy_id = nit_cmpy_id           
 AND nhh_ncr_pfx = nit_ncr_pfx           
 AND nhh_ncr_no = nit_ncr_no         
 left Join '+@DBNAME+'_nctncf_rec on      
 ncf_cmpy_id = nit_cmpy_id         
 AND ncf_ncr_pfx = nit_ncr_pfx         
 AND ncf_ncr_no = nit_ncr_no       
 AND ncf_ncr_itm = nit_ncr_itm        
 where nhh_crtd_dtts  >= '''+ @FD +'''          
 AND  nhh_crtd_dtts  <= '''+ @TD +'''          
 group by nit_brh,nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN ,  NCF_FLT        
          
  Union        
          
 Select ''' + @Name + ''' as db, nit_brh, nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN as Rsn, NCF_FLT as RsnFlt, 0 AS Number_Of_NCRs,        
 0 as AmountClaimed, 0 AS ApprovedCreditVal, SUM(nit_claim_wgt)        
 FROM '+@DBNAME+'_nctnit_rec        
 JOIN '+@DBNAME+'_nctnhh_rec ON         
 nhh_cmpy_id = nit_cmpy_id         
 AND nhh_ncr_pfx = nit_ncr_pfx         
 AND nhh_ncr_no = nit_ncr_no  and nit_mat_dispn = ''S''        
 left Join '+@DBNAME+'_nctncf_rec on      
 ncf_cmpy_id = nit_cmpy_id         
 AND ncf_ncr_pfx = nit_ncr_pfx         
 AND ncf_ncr_no = nit_ncr_no       
 AND ncf_ncr_itm = nit_ncr_itm      
 where nhh_crtd_dtts  >= '''+ @FD +'''        
 AND  nhh_crtd_dtts  <= '''+ @TD +'''         
 group by nit_brh,nit_whs, NCF_RSN_TYP + ''-''+ NCF_RSN , NCF_FLT        
 ) t GROUP BY db,nit_brh,nit_whs, Rsn,RsnFlt        
         
  '          
            
   print(@sqltxt)  ;            
   print @DBNAME + @Name          
    set @execSQLtxt = @sqltxt;             
   EXEC (@execSQLtxt);          
 END --   -- Single database query end here          
          
SELECT Databases, NitBranch,NitWhs, isnull(Rsn,'') as Rsn, isnull(RsnFlt,'') as RsnFlt, NumberOfNcr,AmountClaimed,ApprovedCreditVal,        
case Databases when 'UK' then (MatWeight * 2.20462) when 'Norway' then (MatWeight * 2.20462) else MatWeight end as MatWeight        
FROM #tmp    order by NitBranch      
          
DROP TABLE #tmp ;         
          
-- exec sp_itech_ncr_reporting  'US','01/09/2016', '02/08/2016';           
-- exec sp_itech_ncr_reporting  'ALL','8/22/2013', '09/21/2015';            
          
END 
GO
