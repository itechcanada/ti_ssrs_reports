USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_totalrevenue]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_itech_totalrevenue] @DBNAME varchar(50), @AcctPeriod varchar(6), @BranchName Varchar(30) 
        
AS        
BEGIN  
select id, sum(Amount) as TotalRevenue  from
(
select 0 as id, 1 as Ctr, subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION  
  select 0 as id, 2 as Ctr,subAccountDesc, gld_sacct, pl_cat as AccountType , 
  sum(gld_cr_amt - gld_dr_amt) as Amount from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  ) as x group by id
END
GO
