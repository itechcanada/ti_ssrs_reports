USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ProfitLoss]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                          
-- Author:  <Mukesh >                          
-- Create date: <12 Jul 2017>                          
-- Description: <Getting ProfitLoss>                          
-- =============================================                          
CREATE  PROCEDURE [dbo].[sp_itech_ProfitLoss] @DBNAME varchar(50), @GLSAcct Varchar(35),@MonthYear Date                     
                          
AS                          
BEGIN                     
                  
                  
                       
SET NOCOUNT ON;     
  
  
                          
--declare @sqltxt varchar(6000)                          
declare @execSQLtxt varchar(7000)                          
DECLARE @CurrenyRate varchar(15);                   
DECLARE @StartDate VARCHAR(10);                   
DECLARE @plCompanyID VARCHAR(3);                  
                    
CREATE TABLE #tmp (   CompanyName   VARCHAR(10)                     
  ,CompanyID   VARCHAR(10)                       
        , BscGLAcc     int                          
        , BscGLSubAcc     VARCHAR(35)                 
        , AccountDescription    Varchar(100)                   
        , AccountCategoryID Varchar(5)                         
        , GroupName  Varchar(50)                  
        , AccountAmt  Decimal(20,2)                      
        , BudgetAmt    Decimal(20,2)                     
        , MonthYTD    Varchar(5)             
        ,OrderInPlace int                         
                 );                          
                                       
  if @GLSAcct = 'ALL'                
  begin                
  set @GLSAcct = ''                
  End                
                                           
DECLARE @company VARCHAR(35);                          
DECLARE @prefix VARCHAR(15);                           
DECLARE @DatabaseName VARCHAR(35);                    
                  
--------  +++++  Created Table for last up to current months ++++++++                      
Create TABLE #Last12Months ( StartDate varchar(15) );                      
                      
if(Month(@MonthYear) = 1)            
begin             
declare @start1 DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));               
 with CTEE(Last12MonthDate)                      
AS                      
(                      
    SELECT @start1                      
                 
)                      
INSERT INTO #Last12Months(StartDate)                      
select Last12MonthDate as StartDate  from CTEE                                
            
End                   
 else if (Month(@MonthYear) = 2)            
 begin            
 declare @start2 DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));               
 with CTEE(Last12MonthDate)                      
AS                      
(                      
    SELECT @start2            
     UNION   all                      
                      
    SELECT DATEADD(month,-1,@start2)                      
                       
)                      
INSERT INTO #Last12Months(StartDate)                      
select Last12MonthDate as StartDate  from CTEE                                
            
 END              
 else            
 begin                   
declare @start DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));               
 with CTEE(Last12MonthDate)                      
AS                      
(                      
    SELECT @start                      
     UNION   all                      
                      
    SELECT DATEADD(month,-1,Last12MonthDate)                      
    from CTEE                      
    where DATEADD(month,-2,Last12MonthDate)>=DATEADD(month,-MONTH(@MonthYear)-1,@start)  -- to set no of month                    
)                      
INSERT INTO #Last12Months(StartDate)                      
select Last12MonthDate as StartDate  from CTEE                                
end                                 
                           
                                 
 IF @DBNAME = 'ALL'                          
 BEGIN                          
  DECLARE ScopeCursor CURSOR FOR                          
   select DatabaseName, company,prefix from tbl_itech_DatabaseName                          
    OPEN ScopeCursor;                          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                          
     WHILE @@FETCH_STATUS = 0                          
       BEGIN                          
        DECLARE @query NVARCHAR(4000);                      
         IF (UPPER(@Prefix) = 'TW')                      
    begin                      
    set @plCompanyID = 'TWS';                 
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                      
    End                      
    Else if (UPPER(@Prefix) = 'NO')                 
                         
    begin               
    set @plCompanyID = 'NOS';                      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                      
    End                      
    Else if (UPPER(@Prefix) = 'CA')                      
    begin                   
    set @plCompanyID = 'CAS';                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
    End                      
    Else if (UPPER(@Prefix) = 'CN')                      
    begin                
    set @plCompanyID = 'CNS';                     
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                      
    End                      
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                      
    begin               
    set @plCompanyID = 'USS';                      
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                      
    End                  
    Else if(UPPER(@Prefix) = 'UK')                      
    begin                  
    set @plCompanyID = 'UKS';                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                      
    End                      
	Else if(UPPER(@Prefix) = 'DE')                      
    begin                  
    set @plCompanyID = 'DES';                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                      
    End                      
	
       DECLARE ScopeCursorin CURSOR FOR                      
       select StartDate from  #Last12Months                      
        OPEN ScopeCursorin;                      
        FETCH NEXT FROM ScopeCursorin INTO @StartDate;                      
         WHILE @@FETCH_STATUS = 0                      
           BEGIN                      
            DECLARE @sqltxt NVARCHAR(max);                   
               print @StartDate                  
               print @prefix                  
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                    
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,gld_sacct,plCustomerGroup, plAccountCategoryID,                  
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,                  
 (sum(isnull(gld_cr_amt,0)) - sum(ISNULL(gld_dr_amt,0))) as Account,                
 (select sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0))     
 from '+ @prefix +'_glbbud_rec where bud_bsc_gl_acct = plBasicGLAccount and bud_sacct = gld_sacct    and              
  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '  ) as budget ,  ''' + convert(varchar(2),MONTH(@StartDate)) + ''' , OrderInPlace                 
      from '+ @prefix +'_glhgld_rec                      
join tbl_itech_PLAccount on plBasicGLAccount = gld_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''                 
       where gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '        
       AND (gld_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')                 
      group by plcompanyID,plBasicGLAccount,gld_sacct,plCustomerGroup,OrderInPlace,plAccountCategoryID                  
                     
    union              
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,bud_sacct,plCustomerGroup, plAccountCategoryID,                  
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,                  
 0 as Account,                
 sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) as budget ,                  
 ''' + convert(varchar(2),MONTH(@StartDate)) + '''  ,OrderInPlace                
      from '+ @prefix +'_glbbud_rec                     
      join tbl_itech_PLAccount on plBasicGLAccount = bud_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''                 
       where  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '                 
       AND (bud_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')                 
       and bud_bsc_gl_acct not in (select gld_bsc_gl_acct from '+ @prefix +'_glhgld_rec Where bud_bsc_gl_acct = gld_bsc_gl_acct and bud_sacct = gld_sacct              
 and gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '  )              
      group by plcompanyID,plBasicGLAccount,bud_sacct,plCustomerGroup,OrderInPlace,plAccountCategoryID                  
      order by plBasicGLAccount                 
      '                      
                              
                                
    print(@sqltxt)                          
   set @execSQLtxt = @sqltxt;                           
 EXEC (@execSQLtxt);             
             
                
INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                    
        select @prefix,plcompanyID, plBasicGLAccount,plBasicGLaccount,plCustomerGroup,plAccountCategoryID,            
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,             
        0,0,convert(varchar(2),MONTH(@StartDate)),OrderInPlace            
         from tbl_itech_PLAccount where plBasicGLaccount not in (SElect BscGLAcc from #tmp);        
               
               
INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                             
        select  @prefix,plcompanyID,0,0, plCustomerGroup , plAccountCategoryID ,       
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,      
         0 as AccountAmt, 0 as BudgetAmt, convert(varchar(2),MONTH(@StartDate)) ,OrderInPlace      
  from tbl_itech_PLAccount where plAccountCategoryID not in (SElect distinct AccountCategoryID from #tmp where convert(varchar(2),MONTH(@StartDate)) = MonthYTD)  ;         
                      
                   
   FETCH NEXT FROM ScopeCursorin INTO @StartDate;                      
        END                       
        CLOSE ScopeCursorin;                      
        DEALLOCATE ScopeCursorin;               
                                 
                                  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                          
       END          
    CLOSE ScopeCursor;                          
    DEALLOCATE ScopeCursor;                          
  END                          
  ELSE                          
     BEGIN                           
                               
     Set @prefix= @DBNAME                      
     IF (UPPER(@DBNAME) = 'TW')                      
    begin          
    set @plCompanyID = 'TWS';                 
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                      
    End                      
    Else if (UPPER(@DBNAME) = 'NO')                      
    begin                   
    set @plCompanyID = 'NOS';                   
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                      
    End          
    Else if (UPPER(@DBNAME) = 'CA')                      
    begin                  
    set @plCompanyID = 'CAS';                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                      
    End                      
    Else if (UPPER(@DBNAME) = 'CN')                      
    begin                   
    set @plCompanyID = 'CNS';                   
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                      
    End                      
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                      
    begin                   
    set @plCompanyID = 'USS';                   
    print 'Mukesh'                
    print @plCompanyID;                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                      
    End                      
    Else if(UPPER(@DBNAME) = 'UK')                      
    begin                   
    set @plCompanyID = 'UKS';                   
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                      
    End                      
    Else if(UPPER(@DBNAME) = 'TWCN')                      
    begin                      
       SET @prefix ='TW'                  
       set @plCompanyID = 'TWS';                    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                      
    End
	Else if(UPPER(@DBNAME) = 'DE')                      
    begin                   
    set @plCompanyID = 'DES';                   
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                      
    End
                      
         DECLARE ScopeCursor CURSOR FOR                      
       select StartDate from  #Last12Months                      
        OPEN ScopeCursor;                      
        FETCH NEXT FROM ScopeCursor INTO @StartDate;                      
         WHILE @@FETCH_STATUS = 0            
           BEGIN                      
               print @StartDate                  
               print @prefix                  
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                    
    select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,gld_sacct,plCustomerGroup, plAccountCategoryID,                  
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,                  
 (sum(isnull(gld_cr_amt,0)) - sum(ISNULL(gld_dr_amt,0)))*MultiplicationFactor as Account,                
 (select (sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)))*MultiplicationFactor from '+ @prefix +'_glbbud_rec where bud_bsc_gl_acct = plBasicGLAccount      
  
    
     
 and bud_sacct = gld_sacct             
  and              
  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '  ) as budget ,                  
 ''' + convert(varchar(2),MONTH(@StartDate)) + ''' ,OrderInPlace                 
      from '+ @prefix +'_glhgld_rec                      
      join tbl_itech_PLAccount on plBasicGLAccount = gld_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''                 
       where gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '                  
       AND (gld_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')                 
      group by plcompanyID,plBasicGLAccount,gld_sacct,plCustomerGroup,plAccountCategoryID,OrderInPlace,MultiplicationFactor                  
                     
    union              
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,bud_sacct,plCustomerGroup, plAccountCategoryID,                  
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,                  
 0 as Account,                
 (sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)))*MultiplicationFactor as budget ,                  
 ''' + convert(varchar(2),MONTH(@StartDate)) + ''' ,OrderInPlace                 
      from '+ @prefix +'_glbbud_rec                     
      join tbl_itech_PLAccount on plBasicGLAccount = bud_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''                 
       where  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '                 
       AND (bud_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')                 
       and bud_bsc_gl_acct not in (select gld_bsc_gl_acct from '+ @prefix +'_glhgld_rec Where bud_bsc_gl_acct = gld_bsc_gl_acct and bud_sacct = gld_sacct              
 and gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '  )              
      group by plcompanyID,plBasicGLAccount,bud_sacct,plCustomerGroup,plAccountCategoryID,OrderInPlace,MultiplicationFactor                  
      order by plBasicGLAccount                 
      '                      
                              
                                
    print(@sqltxt)                          
   set @execSQLtxt = @sqltxt;                           
 EXEC (@execSQLtxt);                
                  
INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                    
        select @prefix,plcompanyID, plBasicGLAccount,plBasicGLaccount,plCustomerGroup,plAccountCategoryID,            
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,             
        0,0,convert(varchar(2),MONTH(@StartDate)),OrderInPlace            
         from tbl_itech_PLAccount where plBasicGLaccount not in (SElect BscGLAcc from #tmp) ;      
                
INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD,OrderInPlace)                             
        select  @prefix,plcompanyID,0,0, plCustomerGroup , plAccountCategoryID ,       
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,      
         0 as AccountAmt, 0 as BudgetAmt, convert(varchar(2),MONTH(@StartDate)) ,OrderInPlace      
  from tbl_itech_PLAccount where plAccountCategoryID not in (SElect distinct AccountCategoryID from #tmp where convert(varchar(2),MONTH(@StartDate)) = MonthYTD)  ;         
                   
                            
   FETCH NEXT FROM ScopeCursor INTO @StartDate;                      
        END                       
        CLOSE ScopeCursor;                      
        DEALLOCATE ScopeCursor;             
                    
   END                    
select CompanyName, AccountDescription , AccountCategoryID , GroupName , Sum(AccountAmt) as AccountAmt , --CompanyID , BscGLAcc , BscGLSubAcc ,            
 SUM(ISNULL(BudgetAmt,0)) as BudgetAmt, MonthYTD ,OrderInPlace ,           
 (Select sum(ISNULL(t1.AccountAmt,0)) from #tmp as t1 where t1.MonthYTD = t2.MonthYTD and t1.AccountCategoryID = 1) as Cat1MY,          
 (Select sum(ISNULL(t1.AccountAmt,0)) from #tmp as t1 where t1.MonthYTD = t2.MonthYTD and t1.AccountCategoryID = 2) as Cat2MY,          
 (Select sum(ISNULL(t1.AccountAmt,0)) from #tmp as t1 where t1.MonthYTD = t2.MonthYTD and t1.AccountCategoryID = 3) as Cat3MY,          
 (Select sum(ISNULL(t1.AccountAmt,0)) from #tmp as t1 where t1.MonthYTD = t2.MonthYTD and t1.AccountCategoryID = 5) as Cat5MY,      
 (Select sum(ISNULL(t1.AccountAmt,0)) from #tmp as t1 where t1.MonthYTD = t2.MonthYTD and t1.AccountCategoryID = 6) as Cat6MY         
  from #tmp t2 group by  CompanyName, AccountDescription , AccountCategoryID , GroupName ,MonthYTD ,OrderInPlace           
         
            
--select CompanyName, AccountDescription , AccountCategoryID , GroupName , Sum(AccountAmt) as AccountAmt , --,CompanyID , BscGLAcc , BscGLSubAcc ,            
-- Sum(BudgetAmt) as BudgetAmt, MonthYTD             
                    
drop table #tmp  ;                  
                          
                          
END                          
-- exec [sp_itech_ProfitLoss]  'ALL' ,'810000000000000000000000000000','2017-02-15' 
/*
20210128	Sumit
Add Germany Database
*/
GO
