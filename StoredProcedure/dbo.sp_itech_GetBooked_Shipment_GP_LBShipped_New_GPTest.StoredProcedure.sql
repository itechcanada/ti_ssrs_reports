USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped_New_GPTest]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetBooked_Shipment_GP_LBShipped_New_GPTest] @DBNAME varchar(50),@Market varchar(50), @version char = '0'

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 1, 0) , 120)   --First day of previous 13 month
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month

CREATE TABLE #tmp (  Shipped		DECIMAL(20, 2)
   					, sDate 			 VARCHAR(15)
   					 ,LBShipped  DECIMAL(20, 2)
   					, Booked  DECIMAL(20, 2)
   					,Market  varchar(50)
   					,GP     DECIMAL(20, 2)
   					, TotalNetProfitValue  	 DECIMAL(20, 2)
   					--, npft_avg_val  	 DECIMAL(20, 2)
   					--, tot_val  	 DECIMAL(20, 2)
   					,Databases varchar(50)
   					);
   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CurrenyRate varchar(15);

set @DB= @DBNAME
  	   
set @DBNAME = 'ALL'  
set @Market = 'ALL'    	   

IF @Market = 'ALL'
 BEGIN
	 set @Market = ''
 END
  	   
	   
  	   
 IF @DBNAME = 'ALL'
	BEGIN
		  IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
		
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				 set @DB= @prefix 
  				--print(@DB)
				IF (UPPER(@Prefix) = 'TW')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
				End
				Else if (UPPER(@Prefix) = 'NO')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
				End
				Else if (UPPER(@Prefix) = 'CA')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))
				End
				Else if (UPPER(@Prefix) = 'CN')
				begin
					SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))
				End
				Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')
				begin
					SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))
				End
				Else if(UPPER(@Prefix) = 'UK')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
				End
				Else if(UPPER(@Prefix) = 'DE')
				begin
				   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
				End
							
							SET @query = '  INSERT INTO #tmp(sDate,Shipped,TotalNETProfitValue,GP, LBShipped,Market,Databases)
					                        select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate, '
											if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
											BEGIN
											 SET @query= @query +   'SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as Shipped,
											                         SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as TotalNETProfitValue,  '
											 
											 --Without exchange rate calculation
					                         --SET @query= @query +   'SUM(ISNULL(stn_tot_val,0) ) as Shipped,
					                         --SUM(ISNULL(stn_npft_avg_val,0)) as TotalNETProfitValue,  '
					                         
					                         
											END
											 Else
											BEGIN
											 SET @query= @query +   'SUM(stn_tot_val * '+ @CurrenyRate +') as Shipped,
											                         SUM(stn_npft_avg_val * '+ @CurrenyRate +') as TotalNETProfitValue, '
											END
											SET @query= @query +   'CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as ''GP'','
											--SET @query= @query +  'SUM(stn_npft_avg_val), SUM(stn_tot_val), '
											if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
											  SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as LBShipped,'
											  ELSE
												SET @query = @query + '	SUM(stn_blg_wgt) as LBShipped,'
											
											SET @query = @query + 'cuc_desc30 as Market,'''+ @DB +'''
											from '+ @DB +'_sahstn_rec join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
											where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')'
											if @Market =''
											BEGIN
												 SET @query= @query + ' and (cuc_desc30 <> ''Interco'' OR cuc_desc30 is null) '
											END
											SET @query = @query + 'group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', cuc_desc30,stn_cry,crx_xexrt
											order by sDate desc'
					 print @query; 	  			  
  	  			     EXECUTE sp_executesql @query;
  	  					
  	  					SET @query =    'INSERT INTO #tmp(sDate,Booked,Market,Databases)
  	  					                SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,' 
										if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
										BEGIN
										 SET @query= @query +   'SUM(a.bka_tot_val * '+ @CurrenyRate +') as Booked,'
										END
										 Else
										BEGIN
										 SET @query= @query +   'SUM(a.bka_tot_val * '+ @CurrenyRate +') as Booked,'
										END
										SET @query= @query +   ' 
										isnull(cuc_desc30, '''') as cuc_desc30,'''+ @DB +'''
										FROM ['+ @DB +'_ortbka_rec] a
										INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
										left JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
										Left Join  '+ @DB +'_scrcrx_rec on bka_cry= crx_orig_cry and crx_eqv_cry = ''USD''
										WHERE a.bka_ord_pfx=''SO'' -- AND a.bka_ord_itm<>999 AND a.bka_wgt > 0
										and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
										and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)'
										if @Market =''
										BEGIN
											 SET @query= @query + ' and (cuc_desc30 <> ''Interco'' OR c.cuc_desc30 is null) '
										END
										SET @query = @query + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',cuc_desc30,bka_cry,crx_xexrt'
  	  			  print @query;
  	  			  EXECUTE sp_executesql @query;
  	  			  
  	  			  ---- SQL for [ Interco ]  -------------
  	  			  
  	  			  SET @query = '  INSERT INTO #tmp(sDate,Shipped,TotalNETProfitValue,GP,LBShipped,Market,Databases)
					                        select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate, '
											if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
											BEGIN
											 SET @query= @query +   'SUM(stn_tot_val * '+ @CurrenyRate +') as Shipped,
											                         SUM(stn_npft_avg_val * '+ @CurrenyRate +') as TotalNETProfitValue,  '
											END
											 Else
											BEGIN
											 SET @query= @query +   'SUM(stn_tot_val * '+ @CurrenyRate +') as Shipped,
											                         SUM(stn_npft_avg_val * '+ @CurrenyRate +') as TotalNETProfitValue,  '
											END
											SET @query= @query +   'CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as ''GP'','
											--SET @query= @query +  'SUM(stn_npft_avg_val), SUM(stn_tot_val), '
											if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
											  SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as LBShipped,'
											  ELSE
												SET @query = @query + '	SUM(stn_blg_wgt) as LBShipped,'
											
											SET @query = @query + 'cuc_desc30 as Market,'''+ @DB +'''
											from '+ @DB +'_sahstn_rec join '+ @DB +'_arrcus_rec on cus_cus_id = stn_sld_cus_id
											left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''
											where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX''
											and (cuc_desc30 = ''Interco'' )'
											SET @query = @query + 'group by convert(varchar(7),stn_inv_Dt, 126) +''-01'', cuc_desc30,stn_cry,crx_xexrt
											order by sDate desc'
  	  			     print @query;
  	  			     EXECUTE sp_executesql @query;
  	  					
  	  					SET @query =    'INSERT INTO #tmp(sDate,Booked,Market,Databases)
  	  					                SELECT convert(varchar(7),a.bka_actvy_dt, 126) +''-01'' as sDate,' 
										if  (UPPER(@prefix) = 'TW' OR UPPER(@prefix)= 'NO' OR  UPPER(@prefix)= 'UK')			
										BEGIN
										 SET @query= @query +   'SUM(a.bka_tot_val * '+ @CurrenyRate +') as Booked, '
										END
										 Else
										BEGIN
										 SET @query= @query +   'SUM(a.bka_tot_val * '+ @CurrenyRate +') as Booked, '
										END
										SET @query= @query +   ' 
										cuc_desc30,'''+ @DB +'''
										FROM ['+ @DB +'_ortbka_rec] a
										INNER JOIN ['+ @DB +'_arrcus_rec] b ON a.bka_sld_cus_id=b.cus_cus_id
										INNER JOIN ['+ @DB +'_arrcuc_rec] c ON b.cus_cus_cat=c.cuc_cus_cat
										Left Join  '+ @DB +'_scrcrx_rec on bka_cry= crx_orig_cry and crx_eqv_cry = ''USD''
										WHERE a.bka_ord_pfx=''SO'' -- AND a.bka_ord_itm<>999 AND a.bka_wgt > 0
										and (cuc_desc30 = ''Interco'' )
										and CONVERT(VARCHAR(10), a.bka_actvy_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)'
										SET @query = @query + ' group by convert(varchar(7),a.bka_actvy_dt, 126) +''-01'',cuc_desc30,bka_cry,crx_xexrt'
  	  			  print @query;
  	  			  EXECUTE sp_executesql @query;
  	  			  
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
 
   	            CREATE TABLE #Main (item  Varchar(50)
   					, Category  Varchar(150)
   					, actvy_dt	  VARCHAR(15)
   					, Value   DECIMAL(20, 2) 
   					, TotalNetProfitValue  	 DECIMAL(20, 2)
   					--, npft_avg_val  	 DECIMAL(20, 2)
   					--, tot_val  	 DECIMAL(20, 2)
   					,Databases varchar(15) 
   	               );
   	               
    
   INSERT INTO #Main (item,Category,actvy_dt,Value,Databases)
    SELECT  '$ Invoiced',Market,sDate,Shipped,Databases  FROM #tmp where Shipped is not null
   order by sDate
   
   INSERT INTO #Main (item,Category,actvy_dt,Value,Databases)
    SELECT  '$ Booked',Market,sDate,Booked,Databases  FROM #tmp where Booked is not null
   order by sDate
   
   INSERT INTO #Main (item,Category,actvy_dt,Value,Databases)
    SELECT  'LB Shipped',Market,sDate,LBShipped,Databases  FROM #tmp where LBShipped is not null
   order by sDate
   
   INSERT INTO #Main (item,Category,actvy_dt,Value,TotalNetProfitValue,Databases)
    SELECT  'GP%',Market,sDate,  Shipped ,TotalNetProfitValue,Databases FROM #tmp where GP is not null
   order by sDate
   

if not exists (select * from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest where convert(varchar(7),UpdateDtTm, 126) +'-01' =convert(varchar(7),CURRENT_TIMESTAMP, 126) +'-01')
begin
	Insert into dbo.tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest	
	SELECT CURRENT_TIMESTAMP as UpdateDtTm, item,Category,actvy_dt, 
	case item when 'GP%' 
	then  SUM(TotalNetProfitValue)/nullif(SUM(Value), 0) * 100 
	else sum(Value) end as 'Value',
--	nullif(sum(TotalNetProfitValue),0) as 'npft_avg_val',
sum(TotalNetProfitValue) as 'npft_avg_val',
--	SUM(nullif(Value,0))  as 'tot_val',
	SUM(Value)  as 'tot_val', 
		Databases  from #Main
		--Where Databases = 'US'
    group by item,Category,actvy_dt,Databases
    
    --select * from #abc;
 End 
 
 --select * from #Main where category='Medical';
 -- 
 -- select count(*) from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
 
 drop table #Main
 drop Table #tmp 
 
END


-- exec sp_itech_GetBooked_Shipment_GP_LBShipped_New_GPTest  'ALL','ALL','1'
-- select convert(varchar(7),UpdateDtTm, 126) +'-01', convert(varchar(7),CURRENT_TIMESTAMP, 126) +'-01', * from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest order by 1 desc where category='Industrial' and actvy_dt = '2013-08-01'
--- truncate table tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest
-- select * from tbl_itech_GetBooked_Shipment_GP_LBShipped_History_GPTest  where actvy_dt = '2015-09-01' order by updateDtTm desc 
--Select sum((NULL * 1.1)) , sum((0 * 1.1))
GO
