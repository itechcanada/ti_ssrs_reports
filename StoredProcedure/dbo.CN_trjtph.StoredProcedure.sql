USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_trjtph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,10/1/2012,>  
-- Description: <Description,Warehouse Shipment On Time Performance,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_trjtph]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.CN_trjtph_rec', 'U') IS NOT NULL        
  drop table dbo.CN_trjtph_rec;        
            
                
SELECT *        
into  dbo.CN_trjtph_rec 
FROM [LIVECNSTX].[livecnstxdb].[informix].[trjtph_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
