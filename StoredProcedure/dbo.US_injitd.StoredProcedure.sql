USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_injitd]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Author,Clayton Daigle>                      
-- Create date: <Create Date,10/5/2012,>                      
-- Description: <Description,Open Orders,>                      
-- Last changes by mukesh                 
-- Last updated date: 04 Dec 2018                  
-- Last Desc: Change the select Query                    
-- =============================================                      
CREATE PROCEDURE [dbo].[US_injitd]                      
AS                      
BEGIN                      
 -- SET NOCOUNT ON added to prevent extra result sets from                      
 -- interfering with SELECT statements.                      
 SET NOCOUNT ON;                      
--IF OBJECT_ID('dbo.US_injitd_rec', 'U') IS NOT NULL                    
--  drop table dbo.US_injitd_rec; 
-- Insert statements for procedure here                     
-- issue in column itd_part so ommiting following record. By Mrinal Jha                  
-- Commented By mukesh 20150624                
/*SELECT *                      
FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec]                      
  WHERE (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '666844')                    
 AND (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '666843')                   
 AND (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '666845')                
 AND (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '674827' OR itd_ref_itm <> 2)                  
 AND (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '674826' OR itd_ref_itm <> 2)                 
 AND (itd_cmpy_id <> 'USS' OR itd_ref_pfx <> 'IP' OR itd_ref_no <> '674825')                
      
--Insert into dbo.US_injitd_rec                       
--  (itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,itd_itm_ctl_no,itd_brh,                
--itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_idia,itd_odia,itd_ga_size,itd_ga_typ,itd_wdth,itd_lgth,itd_dim_dsgn,itd_rdm_dim_1,itd_rdm_dim_2,itd_rdm_dim_3,                
--itd_rdm_dim_4,itd_rdm_dim_5,itd_rdm_dim_6,itd_rdm_dim_7,itd_rdm_dim_8,itd_tag_no,itd_lbl_idfr,itd_whs,itd_loc,itd_mill,itd_heat,itd_qds_ctl_no,itd_ownr,itd_ownr_ref_id,itd_ownr_tag_no,                
--itd_bgt_for,itd_bgt_for_id,itd_invt_typ,itd_invt_sts,itd_invt_qlty,itd_invt_cat,itd_orig_zn,itd_ord_ffm,itd_upd_dtts,itd_upd_dtms,itd_prod_for,itd_part_cus_id,                
--itd_part,itd_part_revno,itd_part_acs,itd_cst_qty_indc,itd_prod_indc,itd_prs_parm_indc,itd_instr_indc,itd_hld_indc,itd_cond_indc,itd_atchmt_indc,itd_src_tag_indc)                
        
-- 20181204 commented below         
--Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,itd_itm_ctl_no,itd_brh,                
--itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_idia,itd_odia,itd_ga_size,itd_ga_typ,itd_wdth,itd_lgth,itd_dim_dsgn,itd_rdm_dim_1,itd_rdm_dim_2,itd_rdm_dim_3,                
--itd_rdm_dim_4,itd_rdm_dim_5,itd_rdm_dim_6,itd_rdm_dim_7,itd_rdm_dim_8,itd_tag_no,itd_lbl_idfr,itd_whs,itd_loc,itd_mill,itd_heat,itd_qds_ctl_no,itd_ownr,itd_ownr_ref_id,itd_ownr_tag_no,                
--itd_bgt_for,itd_bgt_for_id,itd_invt_typ,itd_invt_sts,itd_invt_qlty,itd_invt_cat,itd_orig_zn,itd_ord_ffm,itd_upd_dtts,itd_upd_dtms,itd_prod_for,itd_part_cus_id,                
--itd_part_revno,itd_part_acs,itd_cst_qty_indc,itd_prod_indc,itd_prs_parm_indc,itd_instr_indc,itd_hld_indc,itd_cond_indc,itd_atchmt_indc,itd_src_tag_indc                
--into dbo.US_injitd_rec          
--FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec]          
-- commented end        
        
Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm        
,itd_itm_ctl_no,itd_mill,itd_heat,itd_ownr,itd_bgt_for           
into dbo.US_injitd_rec              
FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec]                     
      
*/      
      
--Select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm        
--,itd_itm_ctl_no,itd_brh,itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_whs,itd_mill,itd_heat,itd_ownr,itd_bgt_for, itd_invt_cat, itd_upd_dtts, itd_cst_qty_indc     
--into dbo.US_injitd_rec              
--FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec]      

Truncate table dbo.US_injitd_rec;
-- Insert statements for procedure here
insert into dbo.US_injitd_rec 
select itd_cmpy_id,itd_ref_pfx,itd_ref_no,itd_ref_itm,itd_ref_sbitm,itd_actvy_dt,itd_trs_seq_no,itd_prnt_pfx,itd_prnt_no,itd_prnt_itm,itd_prnt_sitm,  
itd_itm_ctl_no,itd_brh,itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_whs,itd_mill,itd_heat,itd_ownr,itd_bgt_for, itd_invt_cat, itd_upd_dtts, itd_cst_qty_indc 
FROM [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec];
                      
END                      
      
/*      
20171031              
change delete to truncate of table US_injitd_rec              
20181204         
solve synchronizaiton issue and allow insertion of those records which is used in reports                
20200629      
add columns for journal receiving summary report (itd_prnt_sitm,itd_brh,itd_frm,itd_grd,itd_size,itd_fnsh,itd_ef_svar,itd_whs,itd_upd_dtts,itd_cst_qty_indc)      
Modified By: Sumit      
20201009 Sumit  
add itd_invt_cat for journal receiving summary report 
20201105	Sumit
Comment drop statement, implement truncate and insert statement
 */
GO
