USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_mcrmss]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[UK_mcrmss]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_mcrmss_rec', 'U') IS NOT NULL
		drop table dbo.UK_mcrmss_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.UK_mcrmss_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[mcrmss_rec] ; 
  
END
-- select * from UK_mcrmss_rec 
GO
