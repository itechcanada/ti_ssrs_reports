USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_VoucherForReceipts]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <6 Jan 2013>
-- Description:	<Getting Receipt Details for a Voucher SSRS reports>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_VoucherForReceipts] @DBNAME varchar(50),  @ReceiptNo Varchar(10), @version char = '0'
As
Begin

declare @DB varchar(100)
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)


IF @ReceiptNo = 'ALL' OR @ReceiptNo = ''
BEGIN
SET @ReceiptNo = '0'
END

CREATE TABLE #tmp (   [Database]		 VARCHAR(10)
   					, VoucherPfx		 VARCHAR(2)
   					, VoucherNo			 int
   					, VchDate			 Date
   					, VenID				 int
   					, POPfx				 Varchar(2)
   					, PONo 				 int
   					, RCPfx 			 Varchar(2)
   					, RCNo				 int
   					)
   					
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		IF @version = '0'
		BEGIN
			DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
			DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB=  @Prefix
			    SET @query ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, VchDate, VenID, POPfx, PONo, RCPfx, RCNo)
			    SELECT ''' +  @Prefix + ''' as [Database], jci_vchr_pfx , jci_vchr_no , jvc_ent_dt ,jvc_ven_id , jvc_po_pfx , jvc_po_no, 
							jci_trs_pfx , jci_trs_no from ' + @DB + '_apjjci_rec 	
							left join ' + @DB + '_apjjvc_rec on jvc_vchr_no = jci_vchr_no and jvc_vchr_pfx = ''VR''							
							where jci_trs_pfx = ''RC'' and jci_trs_no = ''' + @ReceiptNo +''';'
				print @query;
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
			if @DBNAME = 'PS'
				BEGIN
				Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DBNAME +'')
				END
				ELSE
				BEGIN
				Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')
				END
				
				SET @DB= @DBNAME
			     SET @sqltxt ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, VchDate, VenID, POPfx, PONo, RCPfx, RCNo)
			     SELECT ''' +  @DBNAME + ''' as [Database], jci_vchr_pfx , jci_vchr_no , jvc_ent_dt ,jvc_ven_id , jvc_po_pfx , jvc_po_no, 
							jci_trs_pfx , jci_trs_no from ' + @DB + '_apjjci_rec 
							left join ' + @DB + '_apjjvc_rec on jvc_vchr_no = jci_vchr_no and jvc_vchr_pfx = ''VR''						
							where jci_trs_pfx = ''RC'' and jci_trs_no = ''' + @ReceiptNo +''';'
							
 
					print(@sqltxt)	
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
   END
   Select * from #tmp order by [Database],VchDate,VoucherNo
   Drop table #tmp
End 

							 
-- Exec [sp_itech_VoucherForReceipts] 'ALL', '2854'
-- Exec [sp_itech_VoucherForReceipts] 'CA',  '2854'

--select * from [LIVECASTX].[livecastxdb].[informix].apjjci_rec where jci_vchr_no = '53250'
--select top 100 jvc_vchr_pfx, * from [LIVECASTX].[livecastxdb].[informix].apjjvc_rec where jvc_vchr_no = 53250
--select distinct jvc_vchr_pfx from [LIVECASTX].[livecastxdb].[informix].apjjvc_rec 
GO
