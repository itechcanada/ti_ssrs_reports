USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Top40_Lost_Account]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mrinal >
-- Create date: <15 oct 2015>
-- Description:	<Getting OTP>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_Top40_Lost_Account]  @DBNAME varchar(50), @Brnch varchar(15)

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @NFD varchar(10)
declare @NTD varchar(10)
declare @LFD varchar(10)
declare @LTD varchar(10)
declare @FD varchar(10)
declare @TD varchar(10)
declare @AFD varchar(10)
declare @ATD varchar(10)

declare @Month datetime
Set @Month = GETDATE()

set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-19,0)) , 120)   -- Lost Account
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month)-18,0)), 120)  -- Lost Account

set @AFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@Month)-12,0)) , 120)  -- for 12 month 

set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Month),0)), 120)


CREATE TABLE #tmp (      Branch varchar(50)
                        ,CustID  VARCHAR(50)
						,CustName varchar(100)
						,ContactName varchar(100)
						,CustEmail Varchar(50)
						,Market Varchar(50)
						,LastSaleDate Varchar(50)
						,Prior12MthSale Varchar(50)
						,Prior12MthGp Varchar(50)
   					);
   					
   					CREATE TABLE #tmp1 (      
                        CustID  VARCHAR(50)
						,Prior12MthSale Varchar(50)
						,Prior12MthGp Varchar(50)
   					);
   					
   					   					   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CurrenyRate varchar(15);
DECLARE @Category varchar(50);

set @DB= @DBNAME

IF @Brnch = 'ALL'
BEGIN
SET @Brnch = ''
END
  	   

  	   
 IF @DBNAME = 'ALL'
	BEGIN
			DECLARE ScopeCursor CURSOR FOR
			select DatabaseName, company,prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000);  	
  				 set @DB= @prefix    
							
						   
  	  			         
  	  			        SET @query = '   INSERT INTO #tmp(Branch,CustID,CustName,CustEmail ,ContactName,Market,LastSaleDate,Prior12MthSale,Prior12MthGp)
SELECT   CUS_ADMIN_BRH, CUS_CUS_ID, CUS_CUS_LONG_NM, IsNull((Select TOP 1 cvt_email from '+@DB+'_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = CUS_CUS_ID and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail    
  	  										 										
,(select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else
case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else
case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else
rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end
from '+@DB+'_scrcvt_rec where  cvt_cus_ven_id = CUS_CUS_ID and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no)	  										
  	  										
,cuc_desc30 as category 
,max(stn_inv_dt) as LastSaleDate

, 0, 0

FROM '+@DB+'_sahstn_rec  
INNER JOIN '+@DB+'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID  
Left Join '+@DB+'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
where 
STN_INV_DT <= '''+@NTD+'''  
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH  
Having MAX(stn_inv_dt) <=  '''+@LTD+'''  and   (CUS_ADMIN_BRH = '''+@Brnch +''' OR  '''+@Brnch +''' = '' '')
  	  										
'
-- and MAX(stn_inv_dt) >= '''+@LFD+''' AND								
											
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			    
  	  			SET @query = ' INSERT INTO #tmp1(CustID,Prior12MthSale,Prior12MthGp)
  	  			    select stn_sld_cus_id,SUM(stn_tot_val * 1) as TotalValue, 
CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage
from US_sahstn_rec
where stn_inv_Dt >= '''+@NTD+''' and stn_inv_dt <= '''+@AFD+'''
group by stn_sld_cus_id '
  	  			    
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			    
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
		  
  END
  ELSE
     BEGIN 
								
							SET @sqltxt = '   INSERT INTO #tmp(Branch,CustID,CustName,CustEmail,ContactName,Market,LastSaleDate,Prior12MthSale,Prior12MthGp)
SELECT   CUS_ADMIN_BRH, CUS_CUS_ID, CUS_CUS_LONG_NM, IsNull((Select TOP 1 cvt_email from '+@DBNAME+'_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = CUS_CUS_ID and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail    
  	  										 										
,(select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else
case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else
case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else
rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end
from '+@DBNAME+'_scrcvt_rec where  cvt_cus_ven_id = CUS_CUS_ID and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no)	  										
  	  										
,cuc_desc30 as category 
,max(stn_inv_dt) as LastSaleDate

, 0, 0

FROM '+@DBNAME+'_sahstn_rec  
INNER JOIN '+@DBNAME+'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID  
Left Join '+@DBNAME+'_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
where 
STN_INV_DT <= '''+@NTD+'''  
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH  
Having MAX(stn_inv_dt) <=  '''+@LTD+''' AND (CUS_ADMIN_BRH = '''+@Brnch +''' OR  '''+@Brnch +''' = '' '')
  	  										
'
-- and MAX(stn_inv_dt) >= '''+@LFD+''' 					
							
--							select SUM(stn_tot_val * 1) as TotalValue, 
--CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage
--from US_sahstn_rec
--where stn_inv_Dt >= '2015-01-01' and stn_inv_dt <= '2015-10-15'
--group by stn_sld_cus_id				

							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt); 
							
	SET @sqltxt = ' INSERT INTO #tmp1(CustID,Prior12MthSale,Prior12MthGp)
  	  			    select stn_sld_cus_id,SUM(stn_tot_val * 1) as TotalValue, 
CASE WHEN sum(stn_tot_val) = 0 THEN 0 ELSE(sum(stn_npft_avg_val)/sum(stn_tot_val)) *100 END as NProfitPercentage
from US_sahstn_rec
where stn_inv_Dt >= '''+@AFD+''' and stn_inv_dt <= '''+@NTD+'''  
group by stn_sld_cus_id '
  	  			    
  	  			    print(@sqltxt)
  	  			    EXEC (@sqltxt); 
							
						
					--delete from #tmp
							
     END
  SELECT 
  --TOP 40 
  a.Branch,a.CustID,a.CustName,a.ContactName,a.CustEmail,a.Market,a.LastSaleDate,ISNULL(b.Prior12MthSale,0) as Prior12MthSale,
  ISNULL(b.Prior12MthGp,0) as Prior12MthGp FROM #tmp a
  LEFT JOIN #tmp1 b ON a.CustID = b.CustID 
        	  
  Drop table #tmp  ;
  Drop table #tmp1  ;
END

-- exec [sp_itech_Top40_Lost_Account] 'US','ALL'
-- exec [sp_itech_Top40_Lost_Account] 'ALL','ALL'
-- select * from tbl_itech_Account



GO
