USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_cctatv]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: April 4, 2013  
-- Description: <Description,,> 
-- Last changed by mukesh Date Jun 30,2015
 
-- =============================================  
CREATE PROCEDURE [dbo].[US_cctatv]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.US_cctatv_rec', 'U') IS NOT NULL  
  drop table dbo.US_cctatv_rec;  
      
          
SELECT 
atv_cmpy_id,atv_crm_actvy_pfx,atv_crm_actvy_no,atv_crm_actvy_typ,atv_actvy_tsk_purp,atv_actvy_strt_dt,atv_actvy_end_dt,
atv_actvy_strt_tm,atv_actvy_dur,atv_actvy_loc,atv_actvy_subj,atv_crtd_lgn_id,atv_crtd_dtts,atv_crtd_dtms,atv_upd_lgn_id,atv_upd_dtts,
atv_upd_dtms,atv_del_dtts,atv_del_dtms,atv_prnt_crm_pfx,atv_prnt_crm_no,atv_cus_ven_typ,atv_cus_ven_id,atv_cmptr_cus_id,atv_brh,'-' as atv_actvy_txt
into  dbo.US_cctatv_rec  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[cctatv_rec];  
  
END  
--  exec  US_cctatv  
-- select * from US_cctatv_rec
GO
