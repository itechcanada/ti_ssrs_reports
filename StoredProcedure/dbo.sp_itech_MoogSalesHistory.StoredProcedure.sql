USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MoogSalesHistory]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <08 Aug 2016>            
-- Description: <Getting top 50 customers for SSRS reports>              
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_MoogSalesHistory] @DBNAME varchar(50), @FromDate datetime, @ToDate datetime          
AS            
BEGIN     
  
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(max)            
declare @execSQLtxt varchar(max)            
declare @DB varchar(100)          
declare @FD varchar(10)            
declare @TD varchar(10)            
DECLARE @IsExcInterco char(1)  
            
            
CREATE TABLE #tmp (  InvWeight  DECIMAL(20, 2)           
        , ShpWeight  DECIMAL(20, 2)            
        ,MonthYear Varchar(10)  
                 );             
  
            
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) 
  
  DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(5);      
DECLARE @Name VARCHAR(15);            
 
 set @DB=  @DBNAME  ;
 
  
  IF @DBNAME = 'ALL'      
 BEGIN      
       
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName Where Prefix in ('US','TW')        
    OPEN ScopeCursor;      
        
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
     WHILE @@FETCH_STATUS = 0      
       BEGIN      
      SET @DB= @Prefix      
     -- print(@DB)      
        DECLARE @query NVARCHAR(4000);            
              
        SET @query ='INSERT INTO #tmp ( InvWeight,ShpWeight ,MonthYear)            
        Select Sum(InvWgt) as InvWgt,Sum(shpWgt) as shpWgt, MonthYear from (
select SUM(stn_blg_wgt) shpWgt,
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM '+ @DB +'_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh  and oquery.prd_invt_sts = ''S'' 
and oquery.UpdateDtTm between CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01''  And CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''28'') as InvWgt,
 CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01'' as MonthYear,
stn_frm,stn_grd, stn_size,stn_fnsh from '+ @DB +'_sahstn_rec Where stn_sld_cus_id in ( ''11782'',''1767'',''993'',''302'')
 and stn_inv_dt >= '''+ @FD +''' and stn_inv_dt<= '''+ @TD +'''  and stn_frm <> ''XXXX'' 
group by stn_frm,stn_grd, stn_size,stn_fnsh,CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01'', CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''28'')
as oquery group by MonthYear

      '  
       print(@query)        
        EXECUTE sp_executesql @query;  
        
         FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
       END       
    CLOSE ScopeCursor;      
    DEALLOCATE ScopeCursor;      
  END      
  ELSE      
     BEGIN  
           SET @sqltxt ='INSERT INTO #tmp ( InvWeight,ShpWeight ,MonthYear)            
        Select Sum(InvWgt) as InvWgt,Sum(shpWgt) as shpWgt, MonthYear from (
select SUM(stn_blg_wgt) shpWgt,
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM '+ @DB +'_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh  and oquery.prd_invt_sts = ''S'' 
and oquery.UpdateDtTm between CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01''  And CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''28'') as InvWgt,
 CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01'' as MonthYear,
stn_frm,stn_grd, stn_size,stn_fnsh from '+ @DB +'_sahstn_rec Where stn_sld_cus_id in ( ''11782'',''1767'',''993'',''302'')
 and stn_inv_dt >= '''+ @FD +''' and stn_inv_dt<= '''+ @TD +'''  and stn_frm <> ''XXXX'' 
group by stn_frm,stn_grd, stn_size,stn_fnsh,CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''01'', CONVERT(VARCHAR(8), stn_inv_dt , 120) + ''28'')
as oquery group by MonthYear

      '   
          print(@sqltxt);            
                
    set @execSQLtxt = @sqltxt;             
   EXEC (@execSQLtxt);            
  End 
  SELECT * FROM #tmp ;
       
  DROP TABLE #tmp     ;       
            
END            
            
 -- exec sp_itech_MoogSalesHistory 'UK','09/01/2013', '09/30/2014'   
 /*
  '11782',
'12326',
-- '1767',
'6748',
'748'-- ,'993'
 */
 
GO
