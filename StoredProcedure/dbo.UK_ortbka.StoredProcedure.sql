USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ortbka]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[UK_ortbka] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.UK_ortbka_rec', 'U') IS NOT NULL
		drop table dbo.UK_ortbka_rec;
    
        
SELECT *
into  dbo.UK_ortbka_rec
FROM [LIVEUKSTX].[liveukstxdb].[informix].[ortbka_rec];

END
--- exec UK_ortbka
-- select * from UK_ortbka_rec
GO
