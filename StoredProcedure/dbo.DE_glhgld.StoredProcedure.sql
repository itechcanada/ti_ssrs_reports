USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_glhgld]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,11/19/2020,>  
-- Description: <Description,Germany glhgld_rec,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_glhgld]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
SET NOCOUNT ON;  

Truncate table dbo.DE_glhgld_rec ;   
  
-- Insert statements for procedure here  
insert into DE_glhgld_rec
SELECT *
  from [LIVEDEGL].[livedegldb].[informix].[glhgld_rec] ;  
    
END  
/*

*/
GO
