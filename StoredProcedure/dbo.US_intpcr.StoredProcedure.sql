USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_intpcr]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,OCT 30, 2015,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_intpcr] 
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  --   IF OBJECT_ID('dbo.US_intpcr_rec', 'U') IS NOT NULL  
  --drop table dbo.US_intpcr_rec ;
-- Insert statements for procedure here  
--SELECT *  into  dbo.US_intpcr_rec  from [LIVEUSSTX].[liveusstxdb].[informix].[intpcr_rec] ;   
Truncate table dbo.US_intpcr_rec;
-- Insert statements for procedure here  
insert into dbo.US_intpcr_rec select * from [LIVEUSSTX].[liveusstxdb].[informix].[intpcr_rec] ;

END  
/*
20201105	Sumit
Comment drop statement, implement truncate and insert statement
*/
-- select * from US_intpcr_rec   
GO
