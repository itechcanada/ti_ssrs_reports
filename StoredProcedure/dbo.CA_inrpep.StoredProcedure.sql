USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_inrpep]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================      
-- Author:  <Sumit>      
-- Create date: <Create Date,29/6/2020>      
-- Description: <To Select inrpep_rec records from CA Linked Server>      
-- Requirement: <To get product description for journal receiving report>
-- =============================================      
CREATE PROCEDURE [dbo].[CA_inrpep]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
IF OBJECT_ID('dbo.CA_inrpep_rec', 'U') IS NOT NULL            
  drop table dbo.CA_inrpep_rec;            
                
                    
SELECT *            
into  dbo.CA_inrpep_rec     
FROM [LIVECASTX].[livecastxdb].[informix].[inrpep_rec]  
  
END  



GO
