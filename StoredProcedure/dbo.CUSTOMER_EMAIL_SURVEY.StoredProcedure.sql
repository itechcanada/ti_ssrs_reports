USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CUSTOMER_EMAIL_SURVEY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,7/18/2012>
-- Description:	<Description,This report is for Bruce to identify performance of sales if they enter all account information to Stratix>
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMER_EMAIL_SURVEY]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, f.cvt_cntc_typ,f.cvt_frst_nm, f.cvt_lst_nm, f.cvt_email,e.slp_lgn_id,b.coc_frst_sls_dt
FROM [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrcus_rec] a
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[arrshp_rec] d ON b.coc_cus_id=d.shp_cus_id
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[scrslp_rec] e ON d.shp_is_slp=e.slp_slp
INNER JOIN [LIVEUS_IW].[liveusstxdb_iw].[informix].[scrcvt_rec] f ON a.cus_cus_id=f.cvt_cus_ven_id
WHERE
a.cus_actv=1
AND f.cvt_cus_ven_typ='C'
AND CAST(b.coc_frst_sls_dt AS datetime) BETWEEN @FromDate AND @ToDate
AND f.cvt_addr_typ = 'L' 
AND f.cvt_cntc_typ = 'PU'
UNION
SELECT DISTINCT a.cus_cus_id, a.cus_cus_long_nm,a.cus_admin_brh, f.cvt_cntc_typ,f.cvt_frst_nm, f.cvt_lst_nm, f.cvt_email,e.slp_lgn_id,b.coc_frst_sls_dt
FROM [LIVETWSTX].[livetwstxdb].[informix].[arrcus_rec] a
INNER JOIN [LIVETWSTX].[livetwstxdb].[informix].[arbcoc_rec] b ON a.cus_cus_id=b.coc_cus_id
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[arrshp_rec] d ON b.coc_cus_id=d.shp_cus_id
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[scrslp_rec] e ON d.shp_is_slp=e.slp_slp
INNER JOIN [LIVETWSTX].[livetwstxdb].[informix].[scrcvt_rec] f ON a.cus_cus_id=f.cvt_cus_ven_id

WHERE
a.cus_actv=1
AND f.cvt_cus_ven_typ='C'
AND CAST(b.coc_frst_sls_dt AS datetime) BETWEEN @FromDate AND @ToDate
AND f.cvt_addr_typ = 'L' 
AND f.cvt_cntc_typ = 'PU'


END

GO
