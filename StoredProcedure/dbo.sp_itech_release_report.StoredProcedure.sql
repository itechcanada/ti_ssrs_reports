USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_release_report]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<mrinal >
-- Create date: <27-07-2017>
-- Description:	<Getting release report>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_release_report] @DBNAME varchar(3),@fromDate Date, @toDate Date 

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
DECLARE @CountryName VARCHAR(25);     
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15);  

set @FD = CONVERT(varchar(10),@fromDate,120);
set @TD = CONVERT(varchar(10),@toDate,120);

CREATE TABLE #temp ( Dbname   VARCHAR(3)  
     , VendorID			Varchar(10)  
     , VendorLongName	Varchar(40)
     , PONumber			Varchar(10)
     , POItem			Varchar(10)
     , PODueToDate		Datetime
     , ReceiveDate		Datetime
     , LateBy			int
     );  

IF @DBNAME = 'ALL'  
 BEGIN  
   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
    SET @query = 'INSERT INTO #temp (Dbname, VendorID, VendorLongName, PONumber, POItem, PODueToDate, ReceiveDate, LateBy)  
				 select '''+ @Prefix +''', ven_ven_id, ven_ven_long_nm, por_po_no, por_po_itm, MIN(por_due_to_dt) as DueDate, MIN(rhr_rcpt_upd_Dt) as FirstItemReceivedOn , 
				DATEDIFF(DAY, MIN(por_due_to_dt) , MIN(rhr_rcpt_upd_Dt)) as LateBy
				from ' + @Prefix + '_potpor_rec 
				join ' + @Prefix + '_pohrhr_rec on por_cmpy_id= rhr_cmpy_Id and por_po_pfx = rhr_po_pfx and por_po_no = rhr_po_no and por_po_itm = rhr_po_itm 
				join ' + @Prefix + '_potpod_Rec on por_cmpy_id= pod_cmpy_Id and por_po_pfx = pod_po_pfx and por_po_no = pod_po_no and por_po_itm = pod_po_itm  
				join ' + @Prefix + '_potpoi_rec on poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm
				join ' + @Prefix + '_aprven_rec on ven_cmpy_id = pod_cmpy_id and ven_ven_id = pod_ven_Id 
				where por_due_to_dt >= '''+ @FD +''' and  por_due_to_dt <= '''+ @TD +''' and poi_pur_cat != ''NI''
				group by ven_ven_id, ven_ven_long_nm, por_po_no, por_po_itm
				-- order by ven_ven_id, LateBy desc  
				'  
    
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  
  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)  
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
   
  SET @sqltxt ='INSERT INTO #temp (Dbname, VendorID, VendorLongName, PONumber, POItem, PODueToDate, ReceiveDate, LateBy)  
				 select '''+ @DBNAME +''', ven_ven_id, ven_ven_long_nm, por_po_no, por_po_itm, MIN(por_due_to_dt) as DueDate, MIN(rhr_rcpt_upd_Dt) as FirstItemReceivedOn , 
				DATEDIFF(DAY, MIN(por_due_to_dt) , MIN(rhr_rcpt_upd_Dt)) as LateBy
				from ' + @DBNAME + '_potpor_rec 
				join ' + @DBNAME + '_pohrhr_rec on por_cmpy_id= rhr_cmpy_Id and por_po_pfx = rhr_po_pfx and por_po_no = rhr_po_no and por_po_itm = rhr_po_itm 
				join ' + @DBNAME + '_potpod_Rec on por_cmpy_id= pod_cmpy_Id and por_po_pfx = pod_po_pfx and por_po_no = pod_po_no and por_po_itm = pod_po_itm  
				join ' + @DBNAME + '_potpoi_rec on poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm
				join ' + @DBNAME + '_aprven_rec on ven_cmpy_id = pod_cmpy_id and ven_ven_id = pod_ven_Id 
				where por_due_to_dt >= '''+ @FD +''' and  por_due_to_dt <= '''+ @TD +''' and poi_pur_cat != ''NI''
				group by ven_ven_id, ven_ven_long_nm, por_po_no, por_po_itm
				-- order by ven_ven_id, LateBy desc  
				'  

 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ou order by VendorID,LateBy desc ;
 DROP TABLE  #temp ;  
END  


-- exec sp_itech_release_report 'US','2017-09-01', '2017-09-30' -- 510



GO
