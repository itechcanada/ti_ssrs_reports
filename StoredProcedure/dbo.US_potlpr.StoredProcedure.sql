USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_potlpr]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: May 2, 2018    
-- Description: <Description,,>    
-- =============================================    
create PROCEDURE [dbo].[US_potlpr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.US_potlpr_rec', 'U') IS NOT NULL    
  drop table dbo.US_potlpr_rec;    
        
            
SELECT *    
into  dbo.US_potlpr_rec    
FROM [LIVEUSSTX].[liveusstxdb].[informix].[potlpr_rec];    
    
END    
-- select * from US_potlpr_rec
GO
