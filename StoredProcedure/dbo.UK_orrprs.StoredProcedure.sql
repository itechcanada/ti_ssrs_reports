USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_orrprs]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 6, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[UK_orrprs]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.UK_orrprs_rec', 'U') IS NOT NULL
		drop table dbo.UK_orrprs_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.UK_orrprs_rec
  from [LIVEUKSTX].[liveukstxdb].[informix].[orrprs_rec] ; 
  
END
-- select * from UK_orrprs_rec
GO
