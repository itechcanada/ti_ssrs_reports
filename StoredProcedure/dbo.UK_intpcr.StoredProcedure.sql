USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_intpcr]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Feb 11, 2016,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
create PROCEDURE [dbo].[UK_intpcr]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_intpcr_rec', 'U') IS NOT NULL  
  drop table dbo.UK_intpcr_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_intpcr_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[intpcr_rec] ;   
    
END  
-- select * from UK_intpcr_rec 
GO
