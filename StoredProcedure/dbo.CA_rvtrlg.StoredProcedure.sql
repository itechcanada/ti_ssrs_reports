USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_rvtrlg]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Sumit>
-- Create date: <Create Date,Oct 6, 2021,>
-- Description:	<Description,Reservation Update>

-- =============================================
create PROCEDURE [dbo].[CA_rvtrlg]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_rvtrlg_rec', 'U') IS NOT NULL
		drop table dbo.CA_rvtrlg_rec;	

    -- Insert statements for procedure here
SELECT * into  dbo.CA_rvtrlg_rec
  from [LIVECASTX].[livecastxdb].[informix].[rvtrlg_rec]; 
  
END
GO
