USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_trjiri]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                    
-- Author:  <Sumit>                    
-- Create date: <Create Date,2020/10/09>                    
-- Description: <To get purchase category for journal receiving>                   
-- =============================================                    
create PROCEDURE [dbo].[TW_trjiri]                    
AS                    
BEGIN                    
 -- SET NOCOUNT ON added to prevent extra result sets from                    
 -- interfering with SELECT statements.                    
 SET NOCOUNT ON;                    
IF OBJECT_ID('dbo.TW_trjiri_rec', 'U') IS NOT NULL                  
  drop table dbo.TW_trjiri_rec;                  

select iri_cmpy_id, iri_ref_pfx, iri_ref_no, iri_ref_itm, iri_actvy_dt, iri_pur_cat, iri_sls_cat, iri_prnt_pfx, iri_prnt_no, iri_prnt_itm, iri_prnt_sitm
into dbo.TW_trjiri_rec 
from [LIVETWSTX].[livetwstxdb].[informix].trjiri_rec
                    
END                    
    
/*    

 */
GO
