USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerByProduct_NP_Eaton]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Mukesh >                      
-- Create date: <30 Mar 2020>                      
-- Description: <Getting top 50 customers for SSRS reports>                
-- Last changed Description : <Add new column shping warehouse >             
-- Last changes Desc: Add includeInterco filter                     
-- =============================================                      
CREATE PROCEDURE [dbo].[sp_itech_CustomerByProduct_NP_Eaton] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID  varchar(Max),                
@DateRange int,@CustmomerNoTxt varchar(Max), @version char = '0' , @status char = 'I' , @IncludeInterco char = '0'                    
AS                      
BEGIN               
         
                       
 SET NOCOUNT ON;  
 

declare @sqltxt varchar(max)                      
declare @execSQLtxt varchar(max)                      
declare @DB varchar(100)                    
declare @FD varchar(10)                      
declare @TD varchar(10)                      
declare @NOOfCust varchar(15)                      
DECLARE @CurrenyRate varchar(15)                      
DECLARE @IsExcInterco char(1)            
                      
SET @DB=@DBNAME;                      
                      
CREATE TABLE #tmp (  CustID varchar(15)                       
        ,CustName Varchar(65)                      
        ,InvDt varchar(15)                      
        ,Product   varchar(300)                      
        ,NoOfPcs Varchar(10)                      
        , Inches    DECIMAL(20, 2)                      
        , Weight  DECIMAL(20, 2)                      
        , Part   VARCHAR(50)                       
        ,InvNo Varchar(50)                      
        , Date     varchar(65)                      
        ,Form  varchar(35)                      
        ,Grade  varchar(35)                      
        ,Size  varchar(35)                      
        ,Finish  varchar(35)                      
        ,Measure  DECIMAL(20, 2)                      
        ,TotalValue DECIMAL(20, 2)            
  ,MatGP DECIMAL(20, 2)                     
        ,GPPct DECIMAL(20, 2)                      
  ,NPAmt DECIMAL(20, 2)          
        ,NPPct DECIMAL(20, 2)                     
        ,PONumber Varchar(75)                  
        ,TransportNo Varchar(30)     ,                
        MktCatg varchar(30),                
        ShpWhs varchar(3),            
        Branch varchar(10)             
        ,SalesPersonIs Varchar(10)            
        ,SalesPersonOs Varchar(10)            
        ,sizeDesc Varchar(50)           
        ,millName Varchar(40)               
        ,MillSurCost Decimal(20,2)       
        ,MillDlyDate Varchar(10)      
        ,ConditionCode Varchar(3)    
                 );                       
                     
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @Prefix VARCHAR(5);                      
DECLARE @Name VARCHAR(15);                      
DECLARE @CusID varchar(max);                      
Declare @Value as varchar(500);                      
DECLARE @CustIDLength int;   ---SELECT DATALENGTH(yourtextfield)                      
             
if (@IncludeInterco = '0')            
BEGIN            
set @IsExcInterco ='T'            
END            
                      
--SET @CustomerID= '1111111111,1003'                      
if @CustmomerNoTxt <> ''                      
 BEGIN                      
  set @CustomerID = @CustmomerNoTxt                      
 END                      
                      
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));                      
                      
if @CustomerID = ''                      
 BEGIN                      
  set @CustomerID = '0'                      
 END                      
                      
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )     
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month     
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month                      
 End                   
else if @DateRange=2 -- Last 7 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End                      
else if @DateRange=3 -- Last 14 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End                      
else if @DateRange=4 --Last 12 months excluding all the days in the current month                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                      
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month                      
 End                      
else                      
 Begin                      
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                      
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                      
 End                   
                      
        --->> Input customer data                      
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)                      
  SELECT @pos=0                      
  SELECT @input =@CustomerID                     
  SELECT @input = @input + ','                      
  CREATE TABLE #tempTable (temp varchar(max) )                      
  WHILE CHARINDEX(',',@input) > 0                      
  BEGIN                      
   SELECT @pos=CHARINDEX(',',@input)                      
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))                      
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))                      
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)                      
  END                      
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable                      
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)                      
  DROP TABLE #tempTable                      
                        
IF @DBNAME = 'ALL'                      
 BEGIN                      
 IF @version = '0'                      
  BEGIN                      
  DECLARE ScopeCursor CURSOR FOR                      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                      
    OPEN ScopeCursor;                      
  END                      
  ELSE                      
  BEGIN                      
  DECLARE ScopeCursor CURSOR FOR                      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                       
    OPEN ScopeCursor;                      
  END                      
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
     WHILE @@FETCH_STATUS = 0                      
       BEGIN                      
        DECLARE @query NVARCHAR(4000);                       
                              
        IF (UPPER(@Prefix) = 'TW')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                        
    End                        
    Else if (UPPER(@Prefix) = 'NO')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                     
    End                        
    Else if (UPPER(@Prefix) = 'CA')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                        
    End                        
    Else if (UPPER(@Prefix) = 'CN')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                        
    End                        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                        
    begin                        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                        
    End                      Else if(UPPER(@Prefix) = 'UK')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                        
    End                      
	Else if(UPPER(@Prefix) = 'DE')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                        
    End                         
      if @status = 'I'                    
      BEGIN                    
      SET @query = 'INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,MatGP,GPPct,NoOfPcs,InvNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch )                      
       SELECT  stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, '                      
       if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                      
       BEGIN                      
       SET @query = @query + ' stn_blg_wgt * 2.20462 as Weight, '                      
       END                      
   ELSE                      
       BEGIN                      
       SET @query = @query + ' stn_blg_wgt as Weight, '                      
       END                      
        SET @query = @query + ' frm_Desc25 + '' '' + grd_desc25 as Product,                        
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  ivh_inv_due_dt as Date ,stn_blg_MSR as Measure,STN_TOT_VAL * '+ @CurrenyRate +',                      
    CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',           
    (Case stn_tot_val When 0 then 0 else (stn_npft_avg_val/stn_tot_val)*100 end ) as NPPct,                         
    (stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,                     
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_mpft_avg_val  * '+ @CurrenyRate +' END as ''GPPct'',                       
       stn_blg_pcs,stn_upd_ref,stn_cus_po,stn_transp_pfx + ''-'' + Convert(Varchar(15),stn_transp_no) + ''-'' + Convert(Varchar(5),stn_shpt_itm) , cuc_desc30, stn_shpg_whs, stn_shpt_brh                
       FROM  ' + @Prefix + '_sahstn_rec  left join ' + @Prefix + '_ivtivh_rec on stn_cmpy_id = ivh_cmpy_id and stn_upd_ref = ivh_upd_ref , ' + @Prefix + '_inrfrm_rec,                        
       ' + @Prefix + '_inrgrd_rec, ' + @Prefix + '_arrcus_rec    left  join ' + @DB + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat                
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm                       
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'                      
                             
     if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @query += ' and  (stn_sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                     
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @query += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                      
             Set @query += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @query += ' and stn_Frm <> ''XXXX'''                       
   END                    
   ELSE     
   BEGIN                    
    SET @query ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,MatGP,GPPct,NoOfPcs,InvNo,PONumber, TransportNo, MktCatg, ShpWhs, Branch )                      
        select  ivs_Sld_cus_id, cus_cus_nm as CustName, ivh_inv_dt , ipd_lgth,'               
        -- Changed by mrinal on 19-05              
        print('MR ' + @Prefix)              
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                      
     BEGIN                      
     SET @query = @query + ' sum(ivd_blg_wgt) * 2.20462 , '                      
     END                      
       ELSE                 
     BEGIN                      
     SET @query = @query + ' sum(ivd_blg_wgt),'                      
     END              
                      
        SET @query = @query + 'LTRIM(ipd_frm) + '' X '' + LTRIM(ipd_grd) + '' X '' + LTRIM(ipd_size) as Product,                     
     ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_part,ivh_inv_due_dt, sum(ivd_blg_msr), SUM(tot.chl_chrg_val) * '+ @CurrenyRate +' ,                     
     (SUM(cht_tot_val) - sum(oit_tot_avg_val)) * '+ @CurrenyRate +'  as NPAmt,                            
  case when sum(cht_tot_val) > 0                     
                              then                     
                                    (SUM(cht_tot_val) - sum(oit_tot_avg_val))/sum(cht_tot_val) * 100                    
                                    else                    
                                    0                    
                              end                     
                              as NetPftVal,                    
                              (SUM(cht_tot_val) - sum(oit_mtl_avg_val)) * '+ @CurrenyRate +' as matGP,                      
                              case when sum(cht_tot_val) > 0                     
                              then                     
                                    (SUM(cht_tot_val) - sum(oit_mtl_avg_val))/sum(cht_tot_val) * 100                    
                else                    
                                    0                    
                              end                     
                              as MPFTAvgVal,                     
       sum(ivd_blg_pcs),                    
     ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivd_cus_po,ivs_transp_pfx + ''-'' + Convert(Varchar(15),ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm)  ,                
     cuc_desc30, ivd_shpg_whs , ipd_brh               
       from ' + @Prefix + '_ivtivs_rec                     
       join  ' + @Prefix + '_ivtivh_rec on                     
       ivs_cmpy_id = ivh_cmpy_id and ivs_inv_pfx = ivh_inv_pfx and ivs_inv_no = ivh_inv_no                    
       join ' + @Prefix + '_ortoit_rec on oit_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = oit_ref_pfx and                    
                                oit_ref_itm = 0 and ivs_Shpt_no = oit_ref_no                    
             join ' + @Prefix + '_ortcht_rec on cht_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = cht_ref_pfx and                    
                                cht_ref_itm = 0 and cht_tot_typ = ''T''  and ivs_Shpt_no = cht_ref_no                    
     join ' + @Prefix + '_tctipd_rec on ivs_cmpy_id = ipd_cmpy_id                    
     and ivs_shpt_pfx = ipd_ref_pfx and ivs_shpt_no = ipd_ref_no                     
     join ' + @Prefix + '_ivtivd_rec                    
     on ivd_cmpy_id = ipd_cmpy_id                    
     and ivd_shpt_pfx = ipd_ref_pfx and ivd_shpt_no = ipd_ref_no and ivd_shpt_itm = ipd_ref_itm                     
     join ' + @Prefix + '_ortchl_rec tot                    
     on ivd_cmpy_id = tot.chl_cmpy_id                    
     and ivd_shpt_pfx = tot.chl_ref_pfx and ivd_shpt_no = tot.chl_ref_no and ivd_shpt_itm = tot.chl_ref_itm                     
     and tot.chl_chrg_cl = ''E''                    
     join ' + @Prefix + '_ortchl_rec mtl                    
     on ivd_cmpy_id = mtl.chl_cmpy_id                    
    and ivd_shpt_pfx = mtl.chl_ref_pfx and ivd_shpt_no = mtl.chl_ref_no and ivd_shpt_itm = mtl.chl_ref_itm                     
     and mtl.chl_chrg_cl = ''E'' and mtl.chl_chrg_no = 1                    
     join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = ivs_cmpy_id  and cus_cus_id = ivs_Sld_cus_id               
     left join ' + @Prefix + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat                
      where ivs_sts_actn = ''A'' '                    
                          
  if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @query += ' and  (ivs_Sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @query += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                   Set @query += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @query += ' and ipd_frm <> ''XXXX''                    
      and ivh_inv_dt >= '''+ @FD +''' and ivh_inv_dt <= '''+ @TD +'''                    
      group by ivs_Sld_cus_id,cus_cus_nm,ivh_inv_dt, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ivd_cus_po, ipd_part, ivh_inv_due_dt,ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no),ivs_transp_pfx + ''-'' + Convert(Varchar(15)      
  
    
      
        
      ,ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm),cuc_desc30,ivd_shpg_whs, ipd_brh'                    
   END                    
                  print(@query);                      
        EXECUTE sp_executesql @query;                      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
       END                       
    CLOSE ScopeCursor;                      
    DEALLOCATE ScopeCursor;                      
  END                      
  ELSE                      
     BEGIN                       
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')                    
                            
     IF (UPPER(@DB) = 'TW')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                        
    End                        
    Else if (UPPER(@DB) = 'NO')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                        
    End                        
    Else if (UPPER(@DB) = 'CA')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                        
    End                        
    Else if (UPPER(@DB) = 'CN')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                        
    End                        
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                        
 begin                        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                        
    End                        
    Else if(UPPER(@DB) = 'UK')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                        
    End                        
    Else if(UPPER(@DB) = 'DE')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                        
    End                        
                        
    if @status = 'I'                    
      BEGIN                    
     SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,MatGP,GPPct,NoOfPcs,InvNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch ,SalesPersonIs ,SalesPersonOs,     
  
   
     sizeDesc,millName,MillSurCost,MillDlyDate,ConditionCode )                    
            
       SELECT distinct stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, '                      
       if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                      
       BEGIN                      
       SET @sqltxt = @sqltxt + ' stn_blg_wgt * 2.20462 as Weight, '                      
       END                      
       ELSE                      
       BEGIN                      
       SET @sqltxt = @sqltxt + ' stn_blg_wgt as Weight, '                      
       END                      
        SET @sqltxt = @sqltxt + ' frm_Desc25 + '' '' + grd_desc25 as Product,                        
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  ivh_inv_due_dt as Date,stn_blg_MSR as Measure ,STN_TOT_VAL * '+ @CurrenyRate +',                      
                             
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',          
      (Case stn_tot_val When 0 then 0 else (stn_npft_avg_val/stn_tot_val)*100 end ) as NPPct,                                   
       (stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,                     
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_mpft_avg_val  * '+ @CurrenyRate +' END as ''GPPct'',                      
       stn_blg_pcs,stn_upd_ref,stn_cus_po,stn_transp_pfx + ''-'' + Convert(Varchar(15),stn_transp_no) + ''-'' + Convert(Varchar(5),stn_shpt_itm)  , cuc_desc30, stn_shpg_whs , stn_shpt_brh                   
       ,stn_is_slp, stn_os_slp ,          
       (select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_fnsh = stn_fnsh and prm_size = stn_size ) ,          
       (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = stn_sprc_pfx and itd_ref_no = stn_sprc_no ) ,        
       (select Sum(csi_cst) from ' + @DB + '_cttcsi_rec join ' + @DB + '_intpcr_rec on pcr_cmpy_id = csi_cmpy_id and pcr_po_pfx = csi_ref_pfx and pcr_po_no = csi_ref_no and pcr_po_itm = csi_ref_itm and pcr_po_sitm = csi_ref_sbitm         
  join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_cmpy_id = stn_cmpy_id and itd_ref_pfx = stn_sprc_pfx        
  and itd_ref_no = stn_sprc_no and csi_cst_no = 400)  ,      
   (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_cmpy_id = stn_cmpy_id and itd_ref_pfx = stn_sprc_pfx      
        and itd_ref_no = stn_sprc_no )  , pcd_cond       
       FROM  ' + @DB + '_sahstn_rec     
       left join ' + @DB + '_injitd_rec on itd_cmpy_id = stn_cmpy_id and itd_ref_pfx = stn_sprc_pfx and itd_ref_no = stn_sprc_no    
       left join ' + @DB + '_intpcr_rec on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no    
       left join ' + @DB + '_intpcd_rec on pcd_cmpy_id = pcr_cmpy_id and pcd_itm_ctl_no = pcr_itm_ctl_no    
       left join ' + @DB + '_ivtivh_rec on stn_cmpy_id = ivh_cmpy_id and stn_upd_ref = ivh_upd_ref ,  ' + @DB + '_inrfrm_rec,                       
       ' + @DB + '_inrgrd_rec, ' + @DB + '_arrcus_rec    left  join ' + @DB + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat             
                     
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm                       
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'                      
     if @CusID Like '%''0''%'                      
      BEGIN                      
      --Set @sqltxt += ' and  (stn_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')'                       
      Set @sqltxt += ' and  (stn_sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                      
             Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
Set @sqltxt += ' and stn_Frm <> ''XXXX'''                      
       END                    
       ELSE                    
       BEGIN --for Order information                    
SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,MatGP,GPPct,NoOfPcs,InvNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch ,SalesPersonIs ,SalesPersonOs,        
        sizeDesc,millName,MillSurCost,MillDlyDate )                 
               
        select  ivs_Sld_cus_id, cus_cus_nm as CustName, ivh_inv_dt , ipd_lgth, '              
          -- Changed by mrinal on 19-05              
        print('MR :' + @DB)              
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                      
     BEGIN                      
     SET @sqltxt = @sqltxt + ' sum(ivd_blg_wgt) * 2.20462, '                      
     END                      
       ELSE                      
     BEGIN                
                         
     SET @sqltxt = @sqltxt + ' sum(ivd_blg_wgt),'                      
     END              
                      
        SET @sqltxt = @sqltxt + 'LTRIM(ipd_frm) + '' X '' + LTRIM(ipd_grd) + '' X '' + LTRIM(ipd_size) as Product,                     
     ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_part,ivh_inv_due_dt, sum(ivd_blg_msr), SUM(tot.chl_chrg_val) * '+ @CurrenyRate +' ,                     
     (SUM(cht_tot_val) - sum(oit_tot_avg_val))* '+ @CurrenyRate +' as NPAmt ,                
  case when sum(cht_tot_val) > 0                     
                       then                     
                                    (SUM(cht_tot_val) - sum(oit_tot_avg_val))/sum(cht_tot_val) * 100                    
                                    else                    
                                    0                    
                              end                     
                              as NetPftVal,                    
                              (SUM(cht_tot_val) - sum(oit_mtl_avg_val)) * '+ @CurrenyRate +' as matGP,                   
                              case when sum(cht_tot_val) > 0                     
                              then                     
                                    (SUM(cht_tot_val) - sum(oit_mtl_avg_val))/sum(cht_tot_val) * 100                    
                                    else                    
                                    0                    
                              end                     
                    as MPFTAvgVal,                     
     sum(ivd_blg_pcs),                    
     ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivd_cus_po,ivs_transp_pfx + ''-'' + Convert(Varchar(15),ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm)                     
       , cuc_desc30, ivd_shpg_whs, ipd_brh ,ivd_is_slp, ivd_os_slp,          
       (select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = ipd_frm and prm_grd = ipd_grd and prm_fnsh = ipd_fnsh and prm_size = ipd_size ) ,          
         (select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = ivd_sprc_pfx and itd_ref_no = MAX(ivd_sprc_no) ) ,         
         (select Sum(csi_cst) from ' + @DB + '_cttcsi_rec where csi_cst_no = 400 and csi_ref_pfx = ivd_sprc_pfx and csi_ref_no = MAX(ivd_sprc_no) ),      
         (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = ivd_sprc_pfx      
        and itd_ref_no = MAX(ivd_sprc_no) )            
       from ' + @DB + '_ivtivs_rec                     
       join  ' + @DB + '_ivtivh_rec on                     
       ivs_cmpy_id = ivh_cmpy_id and ivs_inv_pfx = ivh_inv_pfx and ivs_inv_no = ivh_inv_no                    
       join ' + @DB + '_ortoit_rec on oit_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = oit_ref_pfx and                    
                                oit_ref_itm = 0 and ivs_Shpt_no = oit_ref_no                    
                                 join ' + @DB + '_ortcht_rec on cht_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = cht_ref_pfx and                    
    cht_ref_itm = 0 and cht_tot_typ = ''T''  and ivs_Shpt_no = cht_ref_no                    
     join ' + @DB + '_tctipd_rec on ivs_cmpy_id = ipd_cmpy_id                    
     and ivs_shpt_pfx = ipd_ref_pfx and ivs_shpt_no = ipd_ref_no                     
     join ' + @DB + '_ivtivd_rec                    
     on ivd_cmpy_id = ipd_cmpy_id                    
     and ivd_shpt_pfx = ipd_ref_pfx and ivd_shpt_no = ipd_ref_no and ivd_shpt_itm = ipd_ref_itm                     
     join ' + @DB + '_ortchl_rec tot                    
     on ivd_cmpy_id = tot.chl_cmpy_id                    
     and ivd_shpt_pfx = tot.chl_ref_pfx and ivd_shpt_no = tot.chl_ref_no and ivd_shpt_itm = tot.chl_ref_itm                     
     and tot.chl_chrg_cl = ''E''                    
     join ' + @DB + '_ortchl_rec mtl                    
     on ivd_cmpy_id = mtl.chl_cmpy_id                    
     and ivd_shpt_pfx = mtl.chl_ref_pfx and ivd_shpt_no = mtl.chl_ref_no and ivd_shpt_itm = mtl.chl_ref_itm                     
     and mtl.chl_chrg_cl = ''E'' and mtl.chl_chrg_no = 1                    
     join ' + @DB + '_arrcus_rec on cus_cmpy_id = ivs_cmpy_id  and cus_cus_id = ivs_Sld_cus_id                   
     left join ' + @DB + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat                
    where  ivs_sts_actn = ''A''            
       '                    
                          
  if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @sqltxt += ' and  (ivs_Sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
 BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @sqltxt += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                     
       Else                      
        BEGIN                      
             Set @sqltxt += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
    END                      
      Set @sqltxt += ' and  ipd_frm <> ''XXXX''                    
      and ivh_inv_dt >= '''+ @FD +''' and ivh_inv_dt <= '''+ @TD +'''                    
      group by ivs_Sld_cus_id,cus_cus_nm,ivh_inv_dt, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ivd_cus_po, ipd_part, ivh_inv_due_dt,ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no),ivs_transp_pfx + ''-'' + Convert(Varchar(15)       
  
    
      
       
      ,ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm), cuc_desc30, ivd_shpg_whs, ipd_brh,ivd_is_slp, ivd_os_slp,ivd_sprc_pfx '                    
                     
       END                     
          print(@sqltxt);                      
     --print(@DB)                      
     --print(@CustomerID)                      
     --print(@CustIDLength)                      
     --print( @CusID)                      
     --print(@sqltxt);                       
    set @execSQLtxt = @sqltxt;                       
   EXEC (@execSQLtxt);                      
     END                      
              
  if @IsExcInterco ='T'            
  BEGIN            
              
  SELECT Rtrim(Ltrim(CustID)) as CustID  ,Rtrim(Ltrim(CustName)) as CustName ,Rtrim(Ltrim(InvDt)) as InvDt, Rtrim(Ltrim(Product)) as Product,      
   Rtrim(Ltrim(NoOfPcs)) as NoOfPcs, Inches,  Weight ,Rtrim(Ltrim(Part)) as Part ,Rtrim(Ltrim(InvNo)) as InvNo           
        ,Rtrim(Ltrim(Date)) as Date, Rtrim(Ltrim(Form)) as Form, Rtrim(Ltrim(Grade)) as Grade,Rtrim(Ltrim(Size)) as Size, Rtrim(Ltrim(Finish)) as Finish           
        , Measure,TotalValue, MatGP, GPPct            
        , NPAmt, NPPct,Rtrim(Ltrim(PONumber)) as PONumber, Rtrim(Ltrim(TransportNo)) as TransportNo      
        , Rtrim(Ltrim(MktCatg)) as MktCatg, Rtrim(Ltrim(ShpWhs)) as ShpWhs, Rtrim(Ltrim(Branch)) as Branch, Rtrim(Ltrim(SalesPersonIs)) as SalesPersonIs      
        ,Rtrim(Ltrim(SalesPersonOs)) as SalesPersonOs,Rtrim(Ltrim(sizeDesc)) as sizeDesc,Rtrim(Ltrim(millName)) as millName, MillSurCost        
        , Rtrim(Ltrim(MillDlyDate)) as MillDlyDate,ConditionCode   FROM #tmp where MktCatg <> 'Interco' order by CustID                   
  END            
  Else            
  BEGIN            
        
  SELECT Rtrim(Ltrim(CustID)) as CustID  ,Rtrim(Ltrim(CustName)) as CustName ,Rtrim(Ltrim(InvDt)) as InvDt, Rtrim(Ltrim(Product)) as Product,      
   Rtrim(Ltrim(NoOfPcs)) as NoOfPcs, Inches,  Weight ,Rtrim(Ltrim(Part)) as Part ,Rtrim(Ltrim(InvNo)) as InvNo           
        ,Rtrim(Ltrim(Date)) as Date, Rtrim(Ltrim(Form)) as Form, Rtrim(Ltrim(Grade)) as Grade,Rtrim(Ltrim(Size)) as Size, Rtrim(Ltrim(Finish)) as Finish           
        , Measure,TotalValue, MatGP, GPPct            
        , NPAmt, NPPct,Rtrim(Ltrim(PONumber)) as PONumber, Rtrim(Ltrim(TransportNo)) as TransportNo      
        , Rtrim(Ltrim(MktCatg)) as MktCatg, Rtrim(Ltrim(ShpWhs)) as ShpWhs, Rtrim(Ltrim(Branch)) as Branch, Rtrim(Ltrim(SalesPersonIs)) as SalesPersonIs      
        ,Rtrim(Ltrim(SalesPersonOs)) as SalesPersonOs,Rtrim(Ltrim(sizeDesc)) as sizeDesc,Rtrim(Ltrim(millName)) as millName, MillSurCost        
        , Rtrim(Ltrim(MillDlyDate)) as MillDlyDate,ConditionCode  FROM #tmp order by CustID               
  END            
  DROP TABLE #tmp                      
                      
END                      
                      
 -- exec sp_itech_CustomerByProduct_NP_Eaton '04/17/2017', '06/16/2017' ,'US','0','5','','0','I'               
             
 -- exec sp_itech_CustomerByProduct '01/18/2016', '03/18/2016' ,'US','0','5','','0','I'                      
--11930,12681,141,614,654,855,5567,6010,9309,12217,12535,12645,13219                      
--'11930','12681','141','614','654','855','5567','6010','9309','12217','12535','12645','13219'                  
--select * US_ortcht_rec                
                
--[sp_itech_CustomerByProduct] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@CustomerID  varchar(Max),                
--@DateRange int,@CustmomerNoTxt varchar(Max), @version char = '0' , @status char = 'I'       
/*      
Date: 28 Jun 2017      
Mail sub:Customer By Product Report EATON 03-01 06-25.xls      
For eaton report added column Mill related      
      
*/ 
GO
