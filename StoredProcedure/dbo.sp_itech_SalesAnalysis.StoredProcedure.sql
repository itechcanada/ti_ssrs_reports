USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysis]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <11 Feb 2013>  
-- Last Updated By: Mukesh      
-- Last updated Description: Add cust Address column with City and postalcode  
-- Last Updated Date: 25 Jun 2015   
-- last updated by mukesh date 07 Jul, 2015 for the purpose of rounding amount field  
-- =============================================  
-- Description: <Getting top 50 customers for SSRS reports>  
  
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysis] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65),@version char = '0'  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
   
declare @sqltxt varchar(7000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
set @DB=  @DBNAME  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
CREATE TABLE #tmp (   CustID   VARCHAR(10)  
        , CustName     VARCHAR(65)  
        , Market   VARCHAR(65)   
        , ShipgWhs   VARCHAR(65)   
        , ShpBrh  Varchar(3)  
        , Form           Varchar(65)  
        , Grade           Varchar(65)  
        , TotWeight  DECIMAL(20, 2)  
        , WeightUM           Varchar(10)  
                    , TotalValue    DECIMAL(20, 0)  
        , NetGP               DECIMAL(20, 0)  
        , TotalMatValue    DECIMAL(20, 0)  
        , MatGP    DECIMAL(20, 0)  
        , Databases   VARCHAR(15)   
        , MarketID   integer  
        ,CustAdd VARCHAR(170)   
        ,City  VARCHAR(40)  
        ,StateProv Varchar(5)  
        ,Country Varchar(5)   
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
DECLARE @CurrenyRate varchar(15);  
  
if @Market ='ALL'  
 BEGIN  
 set @Market = ''  
 END  
  
IF @DBNAME = 'ALL'  
 BEGIN  
    IF @version = '0'    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
   OPEN ScopeCursor;    
    END    
    ELSE    
    BEGIN    
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    END    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);    
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
       IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End   
    Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End   
      
       
      SET @query =  'INSERT INTO #tmp (CustID, CustName,Market,ShipgWhs,ShpBrh,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases, CustAdd, City, StateProv, Country)       
        select stn_sld_cus_id , cus_cus_nm , cuc_desc30 ,  
      stn_shpg_whs , stn_shpt_brh, stn_frm , stn_grd ,  
      SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM, '  
        
      if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                        
       BEGIN  
              SET @query = @query + ' SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP, '  
      End  
      ELSE  
      Begin  
               SET @query = @query + ' SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP, '  
         
       end  
         
         
       SET @query = @query + ' cuc_cus_cat , '''+ @Name +''' as databases  
        ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)) + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))   
        ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty))               
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec  
       on cus_cus_id = stn_sld_cus_id left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0   
       left join ' + @DB + '_arrcuc_rec  
       on cuc_cus_cat = cus_cus_cat     
        Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, stn_shpg_whs,stn_shpt_brh, stn_frm, stn_grd,  
       stn_ord_wgt_um ,cuc_cus_cat, stn_cry,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)) + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))          
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty))   
       order by stn_sld_cus_id ;'  
         
         
        EXECUTE sp_executesql @query;  
        print(@query)    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
         
       /*  
       EXECUTE sp_executesql @query;  
          
          
        print(@query)  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
         
       */  
         
         
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
       IF @version = '0'    
     BEGIN      
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')    
     END  
     ELSE  
     BEGIN  
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')    
     END  
       
    print('2:' + @Name);  
    print('3:' + @DBNAME);  
     
    IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End  
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End  
       
       
     SET @sqltxt ='INSERT INTO #tmp (CustID, CustName,Market,ShipgWhs,ShpBrh,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases, CustAdd, City, StateProv, Country)       
        select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,  
      stn_shpg_whs as ShipgWhs,stn_shpt_brh, stn_frm as Form, stn_grd as Grade,  
      SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM, '  
        
      if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')     
             SET @sqltxt = @sqltxt + '        
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP, '  
        
      ELSE  
               SET @sqltxt = @sqltxt + '  
      SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,   
      SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,        
      SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,         
      SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP, '  
         
       SET @sqltxt = @sqltxt + '         
      cuc_cus_cat as MarketID, '''+ @Name +''' as databases  
       ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))               
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty))   
       from ' + @DB + '_sahstn_rec join ' + @DB + '_arrcus_rec          
       on cus_cus_id = stn_sld_cus_id left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CS'' and cva_addr_typ = ''S'' and cva_cus_ven_id = cus_cus_id and cva_addr_no = 0   
       left join ' + @DB + '_arrcuc_rec  
       on cuc_cus_cat = cus_cus_cat     
       Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''   
       where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''  
        and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')  
        group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, stn_shpg_whs,stn_shpt_brh, stn_frm, stn_grd,  
       stn_ord_wgt_um ,cuc_cus_cat, stn_cry,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3))  + '' '' + LTRIM(RTRIM(cva_city))  + '' '' + LTRIM(RTRIM(cva_cty)) + '' '' + LTRIM(RTRIM(cva_pcd))         
       ,LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov)),LTRIM(RTRIM(cva_cty))   
       order by stn_sld_cus_id ;'  
    print(@sqltxt)  ;  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     END  
      
      
        SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, CustName,Market,ShipgWhs,shpBrh,Form,Grade,  
      --  case Databases when 'UK' then REPLACE(STR(TotWeight * 2.20462, 20), ' ', '') when 'Norway' then REPLACE(STR(TotWeight * 2.20462, 20), ' ', '') else REPLACE(STR(TotWeight, 20), ' ', '') end as TotWeight,  
      case Databases when 'UK' then (TotWeight * 2.20462) when 'Norway' then (TotWeight * 2.20462) else TotWeight end as TotWeight,  
        'LBS' as WeightUM,  
        TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,CustAdd,City,StateProv,Country FROM #tmp   
          where ShipgWhs not in ('SFS') AND shpBrh not in ('SFS')
        order by CustID   
      
       
     drop table #tmp  
END  
  
-- exec sp_itech_SalesAnalysis '05/08/2014', '07/08/2014' , 'PS','ALL','0';  
--exec sp_itech_SalesAnalysis '4/19/2015', '06/19/2015' , 'TW','Medical';  
--exec #sp_itech_SalesAnalysis '4/19/2015', '06/19/2015' , 'ALL','ALL';  
  
  /*
CHANGES:-
20160525 :-
 option out PSM and especially SFS when all databases is selected
 
 SOLUTION:- 
 NitBranch not in ('SFS') and NitWhs not in ('SFS') 
*/  
  
  
GO
