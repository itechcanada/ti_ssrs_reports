USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_JobPickingList_ByTag]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <20 Dec 2013>  
-- Description: <Getting Picking List SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_JobPickingList_ByTag] @DBNAME varchar(50), @TagNo Varchar(50), @PrdWhs Varchar(3), @version char = '0'   
As  
Begin  
declare @DB varchar(100)  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
  
IF @TagNo = 'ALL' OR @TagNo = ''  
BEGIN  
SET @TagNo = ''  
END  
  
IF @PrdWhs = 'ALL' OR @PrdWhs = ''  
BEGIN  
SET @PrdWhs = ''  
END  
  
CREATE TABLE #tmp (   [Database]   VARCHAR(10)  
     , PrdWhs    VARCHAR(3)  
        , SalesOrderNo   VARCHAR(65)  
        , JSNo     VARCHAR(65)  
        , Dimensions   Varchar(100)  
        , TagNo     Varchar(65)  
        , OnHandPieces   int  
        , OnHandWeight   Varchar(50)  
        , ReservedPieces  int  
        , ReservedWeight  Varchar(50)  
        )  
          
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
    IF @version = '0'  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
  END  
  ELSE  
  BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
  END  
    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);     
      SET @DB= @Prefix -- UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
    SET @query ='INSERT INTO #tmp ([Database], PrdWhs, SalesOrderNo, JSNo, Dimensions, TagNo, OnHandPieces, OnHandWeight, ReservedPieces, ReservedWeight)  
        SELECT ''' +  @Prefix + ''' as [Database], prd_whs, jso_ord_pfx + ''' + '-' + ''' + CAST(jso_ord_no AS VARCHAR(50)) + ''' + '-' + ''' + CAST(jso_ord_itm AS VARCHAR(50)) + ''' + '-' + ''' + CAST(jso_ord_rls_no AS VARCHAR(50)), res_ref_pfx + ''' + '
-' + ''' + CAST(res_ref_no AS VARCHAR(50)),  
         Cast(prd_wdth as varchar(50)) + ''' + ' x ' + ''' + cast(prd_lgth as varchar(50)) as Dimension, prd_tag_no, prd_ohd_pcs,'
          
          -- Changed by mrinal on 19-05    
           print('mr : ' + @Prefix);
            
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')            
		 BEGIN            
			SET @query = @query + ' prd_ohd_wgt * 2.20462, '            
		 END            
       ELSE            
		 BEGIN                
			SET @query = @query + ' prd_ohd_wgt, '            
		 END   
         
          
          SET @query = @query + ' res_res_pcs, '  
          
          if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')            
		 BEGIN            
			SET @query = @query + ' res_res_wgt * 2.20462 '            
		 END            
       ELSE            
		 BEGIN                
			SET @query = @query + ' res_res_wgt '            
		 END   
          
         SET @query = @query + ' from ' + @DB + '_rvtres_rec   
         join ' + @DB + '_intprd_rec on prd_cmpy_id = res_cmpy_id and prd_itm_ctl_no = res_itm_ctl_no  
         join ' + @DB + '_iptjso_rec on jso_cmpy_id = res_cmpy_id and jso_jbs_pfx = res_ref_pfx and jso_jbs_no = res_ref_no  
         where prd_brh not in (''SFS'') and (prd_whs= ''' + @PrdWhs + ''' OR ''' + @PrdWhs + ''' = ''' + ''') and prd_invt_sts = ''S'' and res_ref_pfx = ''' + 'JS' + ''' and (prd_tag_no =  ''' + @TagNo + ''' OR ''' + @TagNo + ''' = ''' + ''');'  
  
     print @query;  
         EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
       IF @version = '0'  
       BEGIN  
     Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName where Prefix =''+ @DBNAME +'')  
     END  
     ELSE  
     BEGIN  
     Set @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DBNAME +'')  
     END  
    SET @DB= @DBNAME-- UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
    SET @sqltxt ='INSERT INTO #tmp ([Database], PrdWhs, SalesOrderNo, JSNo, Dimensions, TagNo, OnHandPieces, OnHandWeight, ReservedPieces, ReservedWeight)  
        SELECT ''' +  @DBNAME + ''' as [Database], prd_whs, jso_ord_pfx + ''' + '-' + ''' + CAST(jso_ord_no AS VARCHAR(50)) + ''' + '-' + ''' + CAST(jso_ord_itm AS VARCHAR(50)) + ''' + '-' + ''' + CAST(jso_ord_rls_no AS VARCHAR(50)), res_ref_pfx + ''' + '
-' + ''' + CAST(res_ref_no AS VARCHAR(50)),  
         Cast(prd_wdth as varchar(50)) + ''' + ' x ' + ''' + cast(prd_lgth as varchar(50)) as Dimension, prd_tag_no, prd_ohd_pcs,'
          
          -- Changed by mrinal on 19-05    
           print('mr : ' + @DBNAME);
            
        if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')            
		 BEGIN            
			SET @sqltxt = @sqltxt + ' prd_ohd_wgt * 2.20462, '            
		 END            
       ELSE            
		 BEGIN                
			SET @sqltxt = @sqltxt + ' prd_ohd_wgt, '            
		 END   
         
          
          SET @sqltxt = @sqltxt + ' res_res_pcs, '  
          
          if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')            
		 BEGIN            
			SET @sqltxt = @sqltxt + ' res_res_wgt * 2.20462 '            
		 END            
       ELSE            
		 BEGIN                
			SET @sqltxt = @sqltxt + ' res_res_wgt '            
		 END
          
             
          SET @sqltxt = @sqltxt + ' from ' + @DB + '_rvtres_rec   
         join ' + @DB + '_intprd_rec on prd_cmpy_id = res_cmpy_id and prd_itm_ctl_no = res_itm_ctl_no  
         join ' + @DB + '_iptjso_rec on jso_cmpy_id = res_cmpy_id and jso_jbs_pfx = res_ref_pfx and jso_jbs_no = res_ref_no  
         where prd_brh not in (''SFS'') and (prd_whs= ''' + @PrdWhs + ''' OR ''' + @PrdWhs + ''' = ''' + ''') and prd_invt_sts = ''S'' and res_ref_pfx = ''' + 'JS' + ''' and (prd_tag_no =  ''' + @TagNo + ''' OR ''' + @TagNo + ''' = ''' + ''') ;'  
     print(@sqltxt)   
     set @execSQLtxt = @sqltxt;   
     EXEC (@execSQLtxt);  
   END  
   Select * from #tmp order by TagNo DESC  
   Drop table #tmp   
End   
  
-- Exec [sp_itech_JobPickingList_ByTag] 'ALL', '34256B'  
-- Exec [sp_itech_JobPickingList_ByTag] 'ALL', 'ALL', 'ALL'  
-- Exec [sp_itech_JobPickingList_ByTag] 'US', '34256B'  
-- Exec [sp_itech_JobPickingList_ByTag] 'US', 'ALL', 'ROC'  
-- Exec [sp_itech_JobPickingList_ByTag] 'US', 'ALL', 'ALL'
GO
