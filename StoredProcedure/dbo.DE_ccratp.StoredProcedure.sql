USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ccratp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author, Sumit>  
-- Create date: <Create Date,Jan 27 2021,>  
-- Description: <Description,Open Orders,>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_ccratp]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
--     IF OBJECT_ID('dbo.DE_ccratp_rec', 'U') IS NOT NULL  
--  drop table dbo.DE_ccratp_rec ;   
--    -- Insert statements for procedure here  
--SELECT *  into  dbo.DE_ccratp_rec  
--  from [LIVEDESTX].[livedestxdb].[informix].[ccratp_rec] ;   
Truncate table dbo.DE_ccratp_rec ;

Insert into dbo.DE_ccratp_rec 
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[ccratp_rec] ;  
    
END  
-- select * from DE_ccratp_rec   
/*
20201014	Sumit
comment table drop and select statement, add truncate and select statement to keep table index 
*/

GO
