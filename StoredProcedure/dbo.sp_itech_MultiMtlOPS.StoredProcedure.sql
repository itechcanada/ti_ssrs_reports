USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MultiMtlOPS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                        
-- Author:  <Mukesh >                        
-- Create date: <25 Apr 2017>                        
                     
                      
-- =============================================                        
CREATE PROCEDURE [dbo].[sp_itech_MultiMtlOPS] @DBNAME varchar(50),  @Grade Varchar(10), @Size Varchar(20),@Finish Varchar(10), @SalesDateFrom  datetime, @SalesDateTo Datetime      
                        
AS                        
BEGIN                        
                         
 SET NOCOUNT ON;                        
declare @sqltxt Varchar(8000)         
declare @execSQLtxt Varchar(8000)        
declare @DB varchar(100)                        
declare @FD varchar(10)        
declare @TD varchar(10)                        
DECLARE @CurrenyRate varchar(15)                     
                        
set @DB=  @DBNAME                        
                        
set @FD = CONVERT(VARCHAR(10), @SalesDateFrom,120)            
 set @TD = CONVERT(VARCHAR(10),@SalesDateTo,120)        
       
 if Rtrim(Ltrim(@Size)) in ('*' , 'ALL','')      
 begin       
 set @Size = ''      
 End      
       
 if @Grade = 'ALL'      
 begin       
 set @Grade = ''      
 End      
       
if @Finish = 'ALL'    
 begin       
 set @Finish = ''      
 End      
                        
CREATE TABLE #tmp (   [Database]   VARCHAR(10)                        
        , Form     Varchar(10)                        
        , Grade     Varchar(10)                        
        , Size     Varchar(20)                        
        , Finish    Varchar(10)              
         , WeightSold   DECIMAL(20, 2)                       
        , AmountSold DECIMAL(20, 2)        
        ,OpenOrderWgt  DECIMAL(20, 2)                    
        , OpenOrderAmt DECIMAL(20, 2)                        
        ,CustID Varchar(Max)                       
        ,ISPerson Varchar(Max)        
        ,OSPerson Varchar(Max)        
        ,InvoiceAmt DECIMAL(20, 2)      
        ,GPPct DECIMAL(20, 2)      
        ,NPPct DECIMAL(20, 2)      
                 );                         
                 
DECLARE @DatabaseName VARCHAR(35);                        
DECLARE @Prefix VARCHAR(35);                        
DECLARE @Name VARCHAR(15);                  
             
          
                      
                        
IF @DBNAME = 'ALL'                        
 BEGIN                        
           
    DECLARE ScopeCursor CURSOR FOR            
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName            
   OPEN ScopeCursor;            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
     WHILE @@FETCH_STATUS = 0                        
       BEGIN                        
        DECLARE @query NVARCHAR(max);                           
      SET @DB= @Prefix                 
                  
                       
      IF (UPPER(@DB) = 'TW')                          
   begin                          
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
   End                          
   Else if (UPPER(@DB) = 'NO')                          
   begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
   End                          
   Else if (UPPER(@DB) = 'CA')                          
   begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
   End                          
   Else if (UPPER(@DB) = 'CN')                          
   begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                          
 End                          
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                          
   begin                    
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
   End                          
   Else if(UPPER(@DB) = 'UK')                          
   begin      
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
   End
   Else if(UPPER(@DB) = 'DE')                          
   begin      
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
   End
             
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                         
        Begin                  
                        
                              
           SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish, WeightSold, AmountSold,OpenOrderWgt, OpenOrderAmt,CustID,ISPerson,OSPerson ,InvoiceAmt,GPPct,NPPct )                        
                   select ''' +  @DB + '''  as [Database], stn_FRM as Form, stn_GRD as Grade, stn_size, stn_fnsh , Sum(stn_blg_wgt * 2.20462),      
           sum(stn_tot_val* '+ @CurrenyRate +'),                    
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh)  as OpenSOWgt,        
        (select SUM(oit_tot_avg_val * '+ @CurrenyRate +') from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx       
       join ' + @DB + '_ortoit_rec on  oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm        
        join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh) as openOrderAMT,         
      stn_sld_cus_id AS custIDs,       
(SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_is_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS isSalesperson,       
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_os_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS osSalesperson,       
        0 as invoiceAmt,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,      
        (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as NPPct      
        FROM ' + @DB + '_sahstn_rec ouu        
      Where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_frm != ''XXXX'' and (stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')           
       group by stn_sld_cus_id,stn_FRM, stn_GRD,stn_size, stn_fnsh       
      '                        
        End                        
        Else                        
        Begin                        
            SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish, WeightSold, AmountSold,OpenOrderWgt, OpenOrderAmt,CustID,ISPerson,OSPerson ,InvoiceAmt,GPPct,NPPct)      
                   select ''' +  @DB + '''  as [Database], stn_FRM as Form, stn_GRD as Grade, stn_size, stn_fnsh , Sum(stn_blg_wgt * 2.20462),      
           sum(stn_tot_val* '+ @CurrenyRate +'),                    
       (select SUM(ortord_rec.ord_bal_wgt ) from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh)  as OpenSOWgt,        
        (select SUM(oit_tot_avg_val * '+ @CurrenyRate +') from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx       
       join ' + @DB + '_ortoit_rec on  oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm        
        join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh) as openOrderAMT,         
       stn_sld_cus_id AS custIDs,       
(SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_is_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')    
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS isSalesperson,       
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_os_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS osSalesperson,       
        0 as invoiceAmt,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,      
        (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as NPPct      
        FROM ' + @DB + '_sahstn_rec ouu        
      Where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_frm != ''XXXX'' and (stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')           
       group by stn_sld_cus_id,stn_FRM, stn_GRD,stn_size, stn_fnsh       
       '                        
        End                        
                               
      print @query;                        
        EXECUTE sp_executesql @query;                        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                        
       END                         
    CLOSE ScopeCursor;             
    DEALLOCATE ScopeCursor;                        
  END                        
  ELSE                        
     BEGIN                    
                
   IF (UPPER(@DB) = 'TW')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
    End                          
    Else if (UPPER(@DB) = 'NO')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
    End                          
    Else if (UPPER(@DB) = 'CA')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                          
    End                          
    Else if (UPPER(@DB) = 'CN')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                          
    End                          
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                          
    begin                          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
    End                          
    Else if(UPPER(@DB) = 'UK')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                          
    End 
	Else if(UPPER(@DB) = 'DE')                          
    begin                          
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                          
    End
                       
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                         
   Begin                 
                          
       SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish, WeightSold, AmountSold,OpenOrderWgt, OpenOrderAmt,CustID,ISPerson,OSPerson ,InvoiceAmt,GPPct,NPPct )                        
   select ''' +  @DB + '''  as [Database], stn_FRM as Form, stn_GRD as Grade, stn_size, stn_fnsh , Sum(stn_blg_wgt * 2.20462),      
           sum(stn_tot_val* '+ @CurrenyRate +'),                    
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                 
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh)  as OpenSOWgt,        
        (select SUM(oit_tot_avg_val * '+ @CurrenyRate +') from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm          
       and ord_ord_pfx = ipd_ref_pfx       
       join ' + @DB + '_ortoit_rec on  oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm        
        join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh) as openOrderAMT,         
       stn_sld_cus_id AS custIDs,       
(SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_is_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS isSalesperson,       
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_os_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS osSalesperson,       
        0 as invoiceAmt,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,      
        (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as NPPct      
        FROM ' + @DB + '_sahstn_rec ouu        
      Where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_frm != ''XXXX'' and (stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')    
   and (stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')           
       group by stn_sld_cus_id,stn_FRM, stn_GRD,stn_size, stn_fnsh  '        
       print( @sqltxt)                         
    set @execSQLtxt = @sqltxt;                         
   EXEC (@execSQLtxt);         
                          
   End                        
   Else                        
   Begin                
                           
        SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish, WeightSold, AmountSold,OpenOrderWgt,OpenOrderAmt,CustID,ISPerson,OSPerson ,InvoiceAmt,GPPct,NPPct)                        
                   select ''' +  @DB + '''  as [Database], stn_FRM as Form, stn_GRD as Grade, stn_size, stn_fnsh , Sum(stn_blg_wgt * 2.20462),      
           sum(stn_tot_val* '+ @CurrenyRate +'),                    
       (select SUM(ortord_rec.ord_bal_wgt ) from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh)  as OpenSOWgt,        
        (select SUM(oit_tot_avg_val * '+ @CurrenyRate +') from ' + @DB + '_tctipd_rec                         
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm                         
       and ord_ord_pfx = ipd_ref_pfx       
       join ' + @DB + '_ortoit_rec on  oit_cmpy_id = ord_cmpy_id and  oit_ref_pfx = ord_ord_pfx and oit_ref_no = ord_ord_no and oit_ref_itm = ord_ord_itm        
        join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and                         
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''                         
       where  ipd_FRM=stn_FRM and ipd_GRD = stn_GRD and ipd_size = stn_size and ipd_fnsh = stn_fnsh) as openOrderAMT,         
       stn_sld_cus_id AS custIDs,       
(SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_is_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS isSalesperson,       
        (SELECT STUFF((SELECT Distinct '','' + RTRIM(inn.stn_os_slp )                       
    FROM ' + @DB + '_sahstn_rec inn where inn.stn_FRM = ouu.Stn_FRM and inn.stn_GRD = ouu.Stn_GRD and inn.stn_size = ouu.Stn_size and inn.stn_fnsh = ouu.Stn_fnsh                        
    and inn.STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(inn.stn_frm,1,1) not in  (''T'')   and inn.stn_frm != ''XXXX'' and (inn.stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')     
   and (inn.stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (inn.stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')      
        FOR XML PATH('''')),1,1,'''')) AS osSalesperson,       
        0 as invoiceAmt,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_mpft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,      
        (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as NPPct      
        FROM ' + @DB + '_sahstn_rec ouu        
      Where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''             
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_frm != ''XXXX'' and (stn_fnsh = ''' + @Finish + ''' OR ''' + @Finish + ''' = '''')    
   and (stn_GRD = ''' + @Grade + ''' OR ''' + @Grade + ''' = '''')           
   and  (stn_size = ''' + Rtrim(Ltrim(@Size)) + ''' OR ''' + Rtrim(Ltrim(@Size)) + ''' = '''')           
       group by stn_sld_cus_id,stn_FRM, stn_GRD,stn_size, stn_fnsh '        
               
print( @sqltxt)                         
    set @execSQLtxt = @sqltxt;                         
   EXEC (@execSQLtxt);          
           
                        
   End                        
                                  
                            
   END                        
                       
                           
select *  from #tmp order by Form,Grade,Size,Finish;                         
                     
                                         
   drop table #tmp  ;           
          
END                        
-- @DBNAME varchar(50),  @Grade Varchar(10), @Size Varchar(20),@Finish Varchar(10), @SalesDateFrom  datetime, @SalesDateTo Datetime      
--exec [sp_itech_MultiMtlOPS] 'ALL',  'A286'  ,'1.5','','2017-04-01','2017-04-25'         
/*
20210128	Sumit
Add Germany Database
*/ 
GO
