USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_injtdc]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,12/2/2013,>  
-- Description: <>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_injtdc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
Delete from dbo.CA_injtdc_rec ;   
   
Insert into dbo.CA_injtdc_rec   
  
    -- Insert statements for procedure here  
select 
tdc_cmpy_id, tdc_ref_pfx, tdc_ref_no, tdc_ref_itm, tdc_ref_sbitm, tdc_actvy_dt, tdc_ref_ln_no, tdc_cst_no, tdc_cst_desc20, tdc_cst_src_pfx, tdc_cst_cl, tdc_cc_typ, 
tdc_cst_orig, tdc_cst, tdc_cst_um, tdc_cst_qty, tdc_cst_qty_um, tdc_cst_qty_typ, tdc_cry, tdc_exrt, tdc_ex_rt_typ, tdc_bas_cry_val, tdc_ven_cry_val, tdc_ovrd_cst
FROM [LIVECASTX].[livecastxdb].[informix].[injtdc_rec]  
--SELECT *  
--FROM [LIVECASTX].[livecastxdb].[informix].[injtdc_rec]  

END
/*
2020/07/06	Sumit	
Stratix Upgrade 10.4 work
skip new addede 4 columns (tdc_aplc_pfx, tdc_aplc_no, tdc_aplc_itm, tdc_aplc_sbitm)
*/
GO
