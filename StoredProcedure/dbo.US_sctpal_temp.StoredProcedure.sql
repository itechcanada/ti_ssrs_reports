USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_sctpal_temp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: jan 6, 2021
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[US_sctpal_temp] 
AS
BEGIN
	Declare @palAudCtlNo nvarchar(25)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select @palAudCtlNo = max(pal_aud_ctl_no) from US_sctpal_temp_rec
	-- Insert statements for procedure here
	Print @palAudCtlNo;

	insert into  dbo.US_sctpal_temp_rec
	select * FROM [LIVEUSSTX].[liveusstxdb].[informix].[sctpal_rec] where pal_aud_ctl_no > @palAudCtlNo;

END
--  exec  US_sctpal_temp
-- select * from US_sctpal_temp_rec
GO
