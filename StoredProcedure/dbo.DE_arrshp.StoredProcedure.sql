USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_arrshp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,01/19/2021,>    
-- Description: <Description,Customer Ship-To,>    
    
-- =============================================    
create PROCEDURE [dbo].[DE_arrshp]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
Truncate table dbo.DE_arrshp_rec ;     
    
-- Insert statements for procedure here    
insert into dbo.DE_arrshp_rec  
SELECT *    
  from [LIVEDESTX].[livedestxdb].[informix].[arrshp_rec] ;     
      
END    
GO
