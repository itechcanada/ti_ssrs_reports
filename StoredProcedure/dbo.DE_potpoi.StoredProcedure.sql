USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_potpoi]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: Oct 29, 2020  
-- Description: <Germany Purchase Order Item>  
-- =============================================  
CREATE  PROCEDURE [dbo].[DE_potpoi]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.DE_potpoi_rec', 'U') IS NOT NULL  
  drop table dbo.DE_potpoi_rec;  
      
          
SELECT *  
into  dbo.DE_potpoi_rec  
FROM [LIVEDESTX].[livedestxdb].[informix].[potpoi_rec];  
  
END  
  
-- exec DE_potpoi  
-- select * from DE_potpoi_rec  
GO
