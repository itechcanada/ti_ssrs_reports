USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesBySate]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <1 Mar 2019>    
-- Description: <Invoice Summary CED>   
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_SalesBySate]  @DBNAME varchar(50), @fromDate date, @toDate date   
AS    
BEGIN    
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @CurrenyRate varchar(15);      
declare @FD varchar(10);          
declare @TD varchar(10);
    
    
CREATE TABLE #tmp ( Dbname   VARCHAR(10)    
     ,CusID   VARCHAR(10) 
     ,CustomerName varchar(35)
     ,ShpPfx varchar(2)
     ,ShpNo int
     ,ShpItem int
     ,ShpBranch Varchar(3) 
     ,InvPfx Varchar(2)
     ,InvNo int
     ,InvDate varchar(2)
     ,OrdPfx varchar(2)
     ,OrdNo int
     ,OrdItem int
     ,TotalInvoice Decimal(20,2)
     ,NPAmt Decimal(20,2)
     ,ShpAddr1 Varchar(35)
     ,ShpAddr2 Varchar(35)
     ,ShpAddr3 Varchar(35)
     ,ShpCity Varchar(35)
     ,ShpState Varchar(2)
     );  

set @FD = CONVERT(VARCHAR(10), @fromDate,120)      
set @TD = CONVERT(VARCHAR(10), @toDate,120)  
 
    
IF @DBNAME = 'ALL'    
 BEGIN    
     
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
    OPEN ScopeCursor;    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   DECLARE @query NVARCHAR(MAX);    
   IF (UPPER(@Prefix) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@Prefix) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@Prefix) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@Prefix) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@Prefix) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@Prefix) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
        
     SET @query = 'INSERT INTO #tmp (Dbname ,CusID,CustomerName,ShpPfx,ShpNo,ShpItem,ShpBranch,InvPfx,InvNo,InvDate,OrdPfx,OrdNo,OrdItem,TotalInvoice,NPAmt,
     ShpAddr1,ShpAddr2,ShpAddr3,ShpCity,ShpState)  
  select distinct '''+ @Prefix +''', sat_sld_cus_id  as CustID,sat_nm1, sat_shpt_pfx, sat_shpt_no, sat_shpt_itm,sat_shpt_brh,sat_inv_pfx, sat_upd_ref as invNo,
  sat_inv_dt, sat_ord_pfx, sat_ord_no, sat_ord_itm,sat_tot_val * '+ @CurrenyRate +', sat_npft_avg_val * '+ @CurrenyRate +' ,cva_addr1,cva_addr2, cva_addr3,cva_city, cva_st_prov
 from   ' + @Prefix + '_sahsat_rec  
 join     ' + @Prefix + '_scrcva_rec a on a.cva_cmpy_id = sat_cmpy_id and a.cva_cus_ven_id = sat_sld_cus_id and a.cva_ref_pfx = ''CS'' and a.cva_cus_ven_typ =''C'' and 
        a.cva_addr_no = sat_shp_to and a.cva_addr_typ = ''S'' 
       where sat_inv_Dt >=''' + @FD + '''  and sat_inv_dt <''' + @TD+ ''' and sat_frm != ''XXXX''  '    
   print @query;    
   EXECUTE sp_executesql @query; 
   
   
       
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;    
  END       
 CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
ELSE    
BEGIN    
 
 IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
        
  SET @sqltxt ='INSERT INTO #tmp (Dbname ,CusID,CustomerName,ShpPfx,ShpNo,ShpItem,ShpBranch,InvPfx,InvNo,InvDate,OrdPfx,OrdNo,OrdItem,TotalInvoice,NPAmt,
     ShpAddr1,ShpAddr2,ShpAddr3,ShpCity,ShpState)  
  select distinct '''+ @DBNAME +''', sat_sld_cus_id  as CustID,sat_nm1, sat_shpt_pfx, sat_shpt_no, sat_shpt_itm,sat_shpt_brh,sat_inv_pfx, sat_upd_ref as invNo,
  sat_inv_dt, sat_ord_pfx, sat_ord_no, sat_ord_itm,sat_tot_val * '+ @CurrenyRate +', sat_npft_avg_val * '+ @CurrenyRate +' ,cva_addr1,cva_addr2, cva_addr3,cva_city, cva_st_prov
 from   ' + @DBNAME + '_sahsat_rec  
 join     ' + @DBNAME + '_scrcva_rec a on a.cva_cmpy_id = sat_cmpy_id and a.cva_cus_ven_id = sat_sld_cus_id and a.cva_ref_pfx = ''CS'' and a.cva_cus_ven_typ =''C'' and 
        a.cva_addr_no = sat_shp_to and a.cva_addr_typ = ''S'' 
       where sat_inv_Dt >=''' + @FD + '''  and sat_inv_dt <''' + @TD+ ''' and sat_frm != ''XXXX'' '    
        
 print(@sqltxt)    
 set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);  
 

   
END    



select * from #tmp;
-- from #tmpCustList;   

DROP TABLE  #tmp;    

END    
    
--EXEC [sp_itech_SalesBySate] 'US','2019-01-01', '2019-01-31'    
--EXEC [sp_itech_InvoiceSummary_CED] 'ALL'    
--select * from tbl_itech_DatabaseName  
  
 
GO
