USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_ortipr]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: jun 25, 2019 
-- Description: <Description,,>  
-- =============================================  
create PROCEDURE [dbo].[CN_ortipr]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
    IF OBJECT_ID('dbo.CN_ortipr_rec', 'U') IS NOT NULL  
  drop table dbo.CN_ortipr_rec;  
          
SELECT *  
into  dbo.CN_ortipr_rec  
FROM [LIVECNSTX].[livecnstxdb].[informix].[ortipr_rec];  
  
END  
-- select * from CN_ortipr_rec
GO
