USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_arrcus]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,11/19/2020,>    
-- Description: <Description,Germany arrcus_rec,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_arrcus]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
truncate table dbo.DE_arrcus_rec ;     
  
-- Insert statements for procedure here       
Insert into dbo.DE_arrcus_rec     
SELECT *    
FROM [LIVEDESTX].[livedestxdb].[informix].[arrcus_rec] where cus_cus_id != '135' 

insert into dbo.DE_arrcus_rec
SELECT cus_cmpy_id, cus_cus_id, cus_cus_nm, '', cus_cus_acct_typ, 
cus_cry, cus_admin_brh, cus_sic, cus_web_site, cus_lng, 
cus_cus_cat, cus_prnt_org ,cus_rltnshp, cus_sls_area, cus_rqst_pttrm, 
cus_rqst_disc_trm, cus_upd_dtts, cus_upd_dtms, cus_actv 
FROM [LIVEDESTX].[livedestxdb].[informix].[arrcus_rec] where cus_cus_id = '135'


    
END    
/*  
20210716	Sumit
add condition for cus_id 135  
Error: mismatch and overflow

*/
GO
