USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerCreditCard]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <01 Dec 2015>  
-- Description: <Booking Daily Reports> 
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_CustomerCreditCard]  @DBNAME varchar(50) 
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 
 SET NOCOUNT ON;  
declare @DB varchar(100);  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000);  
DECLARE @CountryName VARCHAR(25);     
DECLARE @prefix VARCHAR(15);     
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @CurrenyRate varchar(15);    
  
Set @DB =@DBNAME;
  
CREATE TABLE #temp ( Dbname   VARCHAR(10)  
     ,CusID   VARCHAR(10)  
     ,CusLongNm  VARCHAR(40)  
     ,Branch   VARCHAR(3)  
     ,MthdPmt  VARCHAR(10)  
     );  

  
IF @DBNAME = 'ALL'  
 BEGIN  
   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
   DECLARE @query NVARCHAR(MAX);  
   IF (UPPER(@Prefix) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@Prefix) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@Prefix) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@Prefix) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@Prefix) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@Prefix) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
      
   
    SET @query = 'INSERT INTO #temp (Dbname, CusID, CusLongNm, Branch, MthdPmt)  
      select cus_cmpy_id as Company, cus_cus_id as CustomerID, cus_cus_long_nm as CustomerName, Replace(cus_admin_brh,''PSM'',''LAX'') as AdminBranch, crd_mthd_pmt as MethodOfPayment  
      from ' + @Prefix + '_arrcus_rec 
	join ' + @Prefix + '_arrcrd_rec  on cus_cmpy_id = crd_cmpy_id and  cus_cus_id = crd_cus_id 
	where crd_mthd_Pmt = ''CC'' 
	'  
    
   print @query;  
   EXECUTE sp_executesql @query;  
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;  
  END     
  CLOSE ScopeCursor;  
  DEALLOCATE ScopeCursor;  
 END  
ELSE  
BEGIN  

  SET @CountryName = (select name from tbl_itech_DatabaseName_PS where Prefix = @DBNAME)  
 
 IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
      
  
   SET @sqltxt =' INSERT INTO #temp (Dbname, CusID, CusLongNm, Branch, MthdPmt)  
    select cus_cmpy_id as Company, cus_cus_id as CustomerID, cus_cus_long_nm as CustomerName, Replace(cus_admin_brh,''PSM'',''LAX'') as AdminBranch, crd_mthd_pmt as MethodOfPayment  
      from ' + @DB + '_arrcus_rec 
	join ' + @DB + '_arrcrd_rec  on cus_cmpy_id = crd_cmpy_id and  cus_cus_id = crd_cus_id 
	where crd_mthd_Pmt = ''CC''
	'  
 
 print(@sqltxt)  
 set @execSQLtxt = @sqltxt;   
 EXEC (@execSQLtxt);  
END  
 SELECT * FROM #temp ;  
 DROP TABLE  #temp;  
END  
  
--EXEC [sp_itech_CustomerCreditCard] 'ALL'  
--EXEC [sp_itech_CustomerCreditCard] 'PS'
GO
