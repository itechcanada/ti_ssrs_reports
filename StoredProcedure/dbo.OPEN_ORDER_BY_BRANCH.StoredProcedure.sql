USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[OPEN_ORDER_BY_BRANCH]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[OPEN_ORDER_BY_BRANCH]
	-- Add the parameters for the stored procedure here
	@Branch varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
CREATE TABLE #OPEN_ORDER
(
country varchar(3),
ord_ord_no varchar(10),
ord_ord_pfx varchar(10),
cus_cus_id varchar(10),
ord_ord_itm varchar(10),
ord_ord_brh varchar(3),
cus_cus_long_nm varchar(50),
orl_bal_pcs int,
orl_bal_msr float,
ord_bal_wgt float,
chl_chrg_val float,
orl_atnbl_rdy_dt date,
orl_due_dt_ent int,
product varchar(50),
sales varchar(35)
)
/*US*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #US1
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #US2 
 FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortord_rec] b 
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'
AND b.ord_ord_brh=@Branch


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #US1 a
INNER JOIN #US2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm
WHERE
b.ord_ord_brh=@Branch

/*
/*UK*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #UK1
FROM [LIVEUK_IW].[liveukstxdb_iw].[informix].[ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #UK2 
 FROM [LIVEUK_IW].[liveukstxdb_iw].[informix].[ortord_rec] b 
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'
AND b.ord_ord_brh=@Branch


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm,f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #UK1 a
INNER JOIN #UK2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [LIVEUK_IW].[liveukstxdb_iw].[informix].[tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm
WHERE
b.ord_ord_brh=@Branch

/*CANADA*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #CA1
FROM [LIVECA_IW].[livecastxdb_iw].[informix].[ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #CA2 
 FROM [LIVECA_IW].[livecastxdb_iw].[informix].[ortord_rec] b 
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'
AND b.ord_ord_brh=@Branch


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #CA1 a
INNER JOIN #CA2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm
WHERE
b.ord_ord_brh=@Branch
/*TAIWAN*/
SELECT a.chl_chrg_val,a.chl_ref_pfx,a.chl_ref_no,a.chl_ref_itm
INTO #TW1
FROM [LIVETW_IW].[livetwstxdb_iw].[informix].[ortchl_rec] a
WHERE
a.chl_ref_pfx='SO' AND
a.chl_chrg_cl= 'E' AND
a.chl_chrg_val<>0

SELECT c.cus_cmpy_id,(b.ord_bal_wgt),b.ord_ord_brh,b.ord_ord_pcs,b.ord_bal_msr,b.ord_ord_pfx,b.ord_ord_no,b.ord_ord_itm,c.cus_cus_id,c.cus_cus_long_nm
INTO #TW2 
 FROM [LIVETW_IW].[livetwstxdb_iw].[informix].[ortord_rec] b 
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[arrcus_rec] c ON b.ord_sld_cus_id=c.cus_cus_id 
WHERE 
b.ord_sts_actn= 'A' 
AND b.ord_ord_pfx='SO'
AND b.ord_ord_brh=@Branch


INSERT INTO #OPEN_ORDER
SELECT b.cus_cmpy_id, b.ord_ord_no, b.ord_ord_pfx, b.cus_cus_id, b.ord_ord_itm, b.ord_ord_brh, b.cus_cus_long_nm, f.orl_bal_pcs,f.orl_bal_msr,b.ord_bal_wgt,a.chl_chrg_val,
 f.orl_atnbl_rdy_dt, f.orl_due_dt_ent,g.ipd_frm+g.ipd_grd+g.ipd_size+g.ipd_fnsh, e.usr_nm
FROM #TW1 a
INNER JOIN #TW2 b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[ortorh_rec] c ON b.ord_ord_pfx=c.orh_ord_pfx AND b.ord_ord_no=c.orh_ord_no 
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[scrslp_rec] d ON c.orh_tkn_slp=d.slp_slp
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[mxrusr_rec] e ON d.slp_lgn_id=e.usr_lgn_id
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[ortorl_rec] f ON a.chl_ref_pfx=f.orl_ord_pfx AND a.chl_ref_no=f.orl_ord_no AND a.chl_ref_itm=f.orl_ord_itm
INNER JOIN [LIVETW_IW].[livetwstxdb_iw].[informix].[tctipd_rec] g ON f.orl_ord_pfx=g.ipd_ref_pfx AND f.orl_ord_no=g.ipd_ref_no AND f.orl_ord_itm=g.ipd_ref_itm
WHERE
b.ord_ord_brh=@Branch

DROP TABLE #UK1
DROP TABLE #UK2
DROP TABLE #CA1
DROP TABLE #CA2
DROP TABLE #TW1
DROP TABLE #TW2

*/
SELECT * FROM #OPEN_ORDER WHERE ord_ord_brh=@Branch

DROP TABLE #US1
DROP TABLE #US2

DROP TABLE #OPEN_ORDER
END

GO
