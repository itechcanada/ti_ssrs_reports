USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_scrpyt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CN_scrpyt] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_scrpyt_rec', 'U') IS NOT NULL
		drop table dbo.CN_scrpyt_rec;
    
        
SELECT *
into  dbo.CN_scrpyt_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[scrpyt_rec];

END
--- exec CN_scrpyt
-- select * from CN_scrpyt_rec
GO
