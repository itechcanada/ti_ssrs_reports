USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_aptvch]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: Dec 12, 2015
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CA_aptvch] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CA_aptvch_rec', 'U') IS NOT NULL
		drop table dbo.CA_aptvch_rec;
    
        
SELECT *
into  dbo.CA_aptvch_rec
FROM [LIVECASTX].[livecastxdb].[informix].[aptvch_rec];

END
--  exec  CA_aptvch
-- select * from CA_aptvch_rec
GO
