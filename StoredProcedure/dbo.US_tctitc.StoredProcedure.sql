USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_tctitc]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mukesh>  
-- Create date: <Create Date,Oct 15, 2015,>  
-- Description: <Description,Due Date,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_tctitc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_tctitc_rec', 'U') IS NOT NULL  
  drop table dbo.US_tctitc_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_tctitc_rec  
  from [LIVEUSSTX].[liveusstxdb].[informix].[tctitc_rec];   
    
END  
GO
