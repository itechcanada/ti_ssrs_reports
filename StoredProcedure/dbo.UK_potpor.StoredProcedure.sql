USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_potpor]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Mukesh       
-- Create date: OCt 09, 2017      
-- Description: <Description,,>      
-- =============================================      
create PROCEDURE [dbo].[UK_potpor]       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
    -- Insert statements for procedure here      
          
          
    IF OBJECT_ID('dbo.UK_potpor_rec', 'U') IS NOT NULL      
  drop table dbo.UK_potpor_rec;      
          
              
SELECT *      
into  dbo.UK_potpor_rec      
FROM [LIVEUKSTX].[liveukstxdb].[informix].[potpor_rec];      
      
END      
--  exec  US_potpor      
-- select * from US_potpor_rec
GO
