USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_potpoh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: Feb 05, 2016    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[UK_potpoh]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.UK_potpoh_rec', 'U') IS NOT NULL    
  drop table dbo.UK_potpoh_rec;    
        
            
SELECT *    
into  dbo.UK_potpoh_rec    
FROM [LIVEUKSTX].[liveukstxdb].[informix].[potpoh_rec];    
    
END    
--  exec  UK_potpoh    
-- select * from UK_potpoh_rec
GO
