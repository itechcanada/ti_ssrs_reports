USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetGLSubAccount]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <04 Jun 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetGLSubAccount]  @DBNAME varchar(50)  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  Value varchar(100)   
        ,text Varchar(100)  
        ,temp varchar(3)  
        )  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( Value,text,temp)  
      SElect distinct glj_sacct as text, substring(glj_sacct,0,3) As value ,''A'' as temp from '+ @Prefix +'_apjglj_rec '
                  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( Value,text,temp)  
                    SElect distinct glj_sacct as text, substring(glj_sacct,0,3) As value ,''A'' as temp from '+ @DBNAME +'_apjglj_rec
                    
       Union   
       Select ''ALL'' as ''Value'',''All'' as ''text'',''B'' as temp  
       Order by temp desc, text  
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
        
      IF @DBNAME = 'ALL'  
 BEGIN  
      select Value, case when text = '01' then '01 – Corporate' 
                   when text = '02' then '02 – Purchase /Accounting' 
                   when text = '04' then '04 – IT' 
                   when text = '05' then '05 – Warehouse' 
                   when text = '10' then '10 – Sales' 
                   when text = '40' then '40 – Jacksonville' 
                   when text = '55' then '55 – Hillsboro' 
                   when text = '60' then '60 – Aurora'
                   when text = '70' then '70 – Exports'
                   when text = '85' then '85 – Seattle'
                   when text = '81' then '81 – Garden Grove'
                   when text = '95' then '95 - India' else 
                   text end as text,temp from #tmp  
      Union   
   Select 'ALL' as 'Value','ALL' as 'text','B' as temp  
   Order by temp,text  
      End  
      ELSE  
      BEGIN  
                 
                   select Value, case when text = '01' then '01 – Corporate' 
                   when text = '02' then '02 – Purchase /Accounting' 
                   when text = '04' then '04 – IT' 
                   when text = '05' then '05 – Warehouse' 
                   when text = '10' then '10 – Sales' 
                   when text = '40' then '40 – Jacksonville' 
                   when text = '55' then '55 – Hillsboro' 
                   when text = '60' then '60 – Aurora'
                   when text = '70' then '70 – Exports'
                   when text = '85' then '85 – Seattle'
                   when text = '81' then '81 – Garden Grove'
                   when text = '95' then '95 - India' else 
                   text end as text,temp from #tmp ; 
      END  
      drop table  #tmp  
END  
  
-- exec [sp_itech_GetGLSubAccount]  'CA'  
  
  /*
  2016-05-19
  Changed by mukesh
  01 – Corporate
02 – Purchase /Accounting
04 – IT
05 – Warehouse
10 – Sales 
40 – Jacksonville
55 – Hillsboro     
60 – Aurora
70 – Exports       
85 – Seattle
81 – Garden Grove
95 - India
  */
  
  
  
GO
