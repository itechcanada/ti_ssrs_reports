USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ivtivd]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Jul 30, 2014,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[US_ivtivd]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_ivtivd_rec', 'U') IS NOT NULL
		drop table dbo.US_ivtivd_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_ivtivd_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[ivtivd_rec] ; 
  
END
-- select * from US_ivtivd_rec 
GO
