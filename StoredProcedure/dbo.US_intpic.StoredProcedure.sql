USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_intpic]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,OCT 30, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[US_intpic]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_intpic_rec', 'U') IS NOT NULL
		drop table dbo.US_intpic_rec ;	

	
    -- Insert statements for procedure here
select  pic_cmpy_id,pic_itm_ctl_no,pic_brh,pic_extd_prd,pic_whs,pic_sk_ctl,	pic_tag_no
into  dbo.US_intpic_rec
    from [LIVEUSSTX].[liveusstxdb].[informix].intpic_rec
  
END
-- select * from US_intpic_rec 
GO
