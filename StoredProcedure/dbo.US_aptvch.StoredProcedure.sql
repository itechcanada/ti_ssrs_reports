USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_aptvch]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: Dec 12, 2015
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[US_aptvch] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_aptvch_rec', 'U') IS NOT NULL
		drop table dbo.US_aptvch_rec;
    
        
SELECT *
into  dbo.US_aptvch_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[aptvch_rec];

END
--  exec  US_aptvch
-- select * from US_aptvch_rec
GO
