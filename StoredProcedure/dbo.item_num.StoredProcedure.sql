USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[item_num]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		chris riggi
-- Create date: 06/12/2014
-- Description:	create itemnum
-- =============================================
CREATE PROCEDURE [dbo].[item_num] 
	-- Add the parameters for the stored procedure here
	@num varchar(50) = NULL, 
	@item varchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @num, @item
END
GO
