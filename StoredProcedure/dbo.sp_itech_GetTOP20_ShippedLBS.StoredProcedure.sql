USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTOP20_ShippedLBS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetTOP20_ShippedLBS] @DBNAME varchar(50),@NoOfRecords int = 20

AS
BEGIN
SET NOCOUNT ON;


declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)

set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)), 120)  --Last Day of current month

--set @FD='2013-03-01'
--set @TD='2013-03-31'

CREATE TABLE #tmp (  sDate 			 VARCHAR(15)
   					 ,LBShipped   DECIMAL(20, 2)
   					,Product  varchar(50)
   					,Shippled DECIMAL(20, 2)
   					,stn_ord_wgt_um varchar(10)
   			
   					);
   					
CREATE TABLE #tmpMain (  sDate 			 VARCHAR(15)
   					 ,LBShipped   DECIMAL(20, 2)
   					,Product  varchar(50)
   					,Shippled DECIMAL(20, 2)
   					,stn_ord_wgt_um varchar(10)
   					);
   					
   					CREATE TABLE #tmpSpShpInvWgt (  sDate 			 VARCHAR(15)
   					 ,LBShipped   DECIMAL(20, 2)
   					,Product  varchar(50)
   					,Shippled DECIMAL(20, 2)
   					,stn_ord_wgt_um varchar(10),
   					LBSInventory DECIMAL(20, 2)
   					);
   					   	CREATE TABLE #tmpShpInvWgt (  sDate 			 VARCHAR(15)
   					 ,LBShipped   DECIMAL(20, 2)
   					,Product  varchar(50)
   					,Shippled DECIMAL(20, 2)
   					,stn_ord_wgt_um varchar(10)
   					);				
   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CurrenyRate varchar(15);

set @DB= @DBNAME

if @DB = ''
 set @DB = 'US'
  	   
 IF @DBNAME = 'ALL'
	BEGIN
		  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,company,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				 set @DB= @prefix    --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
  				print(@DB)
							
							SET @query = '  INSERT INTO #tmp(sDate,LBShipped,Product,Shippled,stn_ord_wgt_um)
					                        select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate, '
											
											if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')			
											    SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as LBShipped,'
											ELSE
												SET @query = @query + '	SUM(stn_blg_wgt) as LBShipped,' 
												
							SET @query = @query + 'RTRIM(LTrim(STN_FRM)) + ''/'' + RTRIM(LTrim(STN_GRD)) as Product
											,0 as ''Shippled'',stn_ord_wgt_um
											from '+ @DB +'_sahstn_rec 
											where STN_FRM <>''XXXX'' and STN_GRD <>''XXXX'' --and  STN_FRM =''TIRD'' and STN_GRD =''64''
											and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''
											group by STN_FRM , STN_GRD,convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_ord_wgt_um
											order by sDate desc'
  	  			     EXECUTE sp_executesql @query;
  	  					print(@query)
  	  					
  	  					insert into #tmpMain (sDate,LBShipped, Product,Shippled,stn_ord_wgt_um)
						 Select sDate,sum(LBShipped), Product,Sum(Shippled),stn_ord_wgt_um from #tmp
						 group by sDate,Product,stn_ord_wgt_um order by 1 desc , 2 desc 
					   	    
					   insert into #tmpShpInvWgt
					   SELECT  sDate,LBShipped as LBShipped , Product,Shippled as Shippled,stn_ord_wgt_um
						  FROM (SELECT
					   ROW_NUMBER() OVER ( PARTITION BY sDate ORDER BY sDate desc, LBShipped DESC ) AS 'RowNumber',
					   sDate, LBShipped ,Product,Shippled,stn_ord_wgt_um
					   FROM #tmpMain 
					   ) dt
					   WHERE RowNumber <= 20
					   order by sDate desc , LBShipped desc
					  
					  SET @sqltxt = '  INSERT INTO #tmpSpShpInvWgt(sDate,LBShipped, LBSInventory, Product,Shippled,stn_ord_wgt_um) select  sDate,LBShipped as LBShipped , '
					  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')		
							SET @sqltxt = @sqltxt + '	SUM(prd_ohd_wgt * 2.20462) as LBSInventory, '
						ELSE
							SET @sqltxt = @sqltxt + '	SUM(prd_ohd_wgt) as LBSInventory, '
						
					  SET @sqltxt = @sqltxt + ' Product,Shippled as Shippled,stn_ord_wgt_um  from #tmpShpInvWgt
					  join  '+ @DB +'_intprd_rec_history on RTRIM(LTrim(prd_FRM)) + ''/'' + RTRIM(LTrim(prd_GRD)) = Product  
																and UpdateDtTm >= sDate
																and  UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,sDate)+1,0))
					group by  sDate, LBShipped , Product,Shippled,stn_ord_wgt_um  order by sDate desc , LBShipped desc';
						        
												print(@sqltxt)
												set @execSQLtxt = @sqltxt; 
												EXEC (@execSQLtxt);
												
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
					SET @sqltxt = '    INSERT INTO #tmp(sDate,LBShipped, Product,Shippled,stn_ord_wgt_um)
					                        select  convert(varchar(7),stn_inv_Dt, 126) +''-01'' as sDate,'
											if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')		
											BEGIN	
											    SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt * 2.20462) as LBShipped, '
											     END
											ELSE
											BEGIN
												SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt) as LBShipped, ' 
												 END
												
							SET @sqltxt = @sqltxt + 'RTRIM(LTrim(STN_FRM)) + ''/'' + RTRIM(LTrim(STN_GRD)) as Product
											,0 as ''Shippled'',stn_ord_wgt_um
											from '+ @DB +'_sahstn_rec 
											
											where STN_FRM <>''XXXX'' and STN_GRD <>''XXXX'' 
											and stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''
											group by STN_FRM , STN_GRD,convert(varchar(7),stn_inv_Dt, 126) +''-01'',stn_ord_wgt_um
											order by sDate desc'
	        
							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt);
							
						insert into #tmpMain (sDate,LBShipped, Product,Shippled,stn_ord_wgt_um)
						 Select sDate,sum(LBShipped), Product,Sum(Shippled),stn_ord_wgt_um from #tmp
						 group by sDate,Product,stn_ord_wgt_um order by 1 desc , 2 desc 
					   	    
					   insert into #tmpShpInvWgt
					   SELECT  sDate,LBShipped as LBShipped , Product,Shippled as Shippled,stn_ord_wgt_um
						  FROM (SELECT
					   ROW_NUMBER() OVER ( PARTITION BY sDate ORDER BY sDate desc, LBShipped DESC ) AS 'RowNumber',
					   sDate, LBShipped ,Product,Shippled,stn_ord_wgt_um
					   FROM #tmpMain 
					   ) dt
					   WHERE RowNumber <= @NoOfRecords
					   order by sDate desc , LBShipped desc
					  
					  SET @sqltxt = '  INSERT INTO #tmpSpShpInvWgt(sDate,LBShipped, LBSInventory, Product,Shippled,stn_ord_wgt_um) select  sDate,LBShipped as LBShipped , '
					  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')		
							SET @sqltxt = @sqltxt + '	SUM(prd_ohd_wgt * 2.20462) as LBSInventory, '
						ELSE
							SET @sqltxt = @sqltxt + '	SUM(prd_ohd_wgt) as LBSInventory, '
						
					  SET @sqltxt = @sqltxt + ' Product,Shippled as Shippled,stn_ord_wgt_um  from #tmpShpInvWgt
					  join  '+ @DB +'_intprd_rec_history on RTRIM(LTrim(prd_FRM)) + ''/'' + RTRIM(LTrim(prd_GRD)) = Product  
																and UpdateDtTm >= sDate
																and  UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,sDate)+1,0))
					group by  sDate, LBShipped , Product,Shippled,stn_ord_wgt_um  order by sDate desc , LBShipped desc';
						        
												print(@sqltxt)
												set @execSQLtxt = @sqltxt; 
												EXEC (@execSQLtxt);
					  
					    
					  
     END
     
   --select  sDate, LBShipped ,  LBSInventory, Product,Shippled,stn_ord_wgt_um from #tmpSpShpInvWgt; 
   select  sDate, LBShipped ,  LBSInventory, Product,Shippled,stn_ord_wgt_um 
   FROM (SELECT	   ROW_NUMBER() OVER ( PARTITION BY sDate ORDER BY sDate desc, LBShipped DESC ) AS 'RowNumber',
					   sDate, LBShipped ,LBSInventory,Product,Shippled,stn_ord_wgt_um
					   from #tmpSpShpInvWgt
					   ) dt
					   WHERE RowNumber <= @NoOfRecords
					   order by sDate desc , LBShipped desc
  
  
  drop table #tmp;
  drop table #tmpMain;
  drop table #tmpShpInvWgt;
  drop table #tmpSpShpInvWgt;
  
 
END

-- exec sp_itech_GetTOP20_ShippedLBS  'US' exec sp_itech_GetTOP20_ShippedLBS  'ALL'
-- exec sp_itech_GetTOP20_ShippedLBS  'ALL','1'
/*
join  '+ @DB +'_intprd_rec_history on prd_frm = stn_frm and prd_grd = stn_grd 
											and UpdateDtTm >= dateadd(month, datediff(month, 0, convert(varchar(7),stn_inv_Dt, 126) +''-01'') - 12, 0)
											and  UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,convert(varchar(7),stn_inv_Dt, 126) +''-01'')+1,0))
*/


GO
