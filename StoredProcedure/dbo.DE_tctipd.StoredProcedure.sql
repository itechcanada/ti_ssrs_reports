USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_tctipd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Author,Sumit>    
-- Create date: <Create Date,10/29/2020>    
-- Description: <Germany Item Product>    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_tctipd]    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  IF OBJECT_ID('dbo.DE_tctipd_rec', 'U') IS NOT NULL                          
  drop table dbo.DE_tctipd_rec;                          
                                                                    
SELECT   *   into dbo.DE_tctipd_rec                                                                  
FROM [LIVEDESTX].[livedestxdb].[informix].[tctipd_rec]   
    
END    
GO
