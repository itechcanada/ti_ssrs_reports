USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisTIPReport_Excess_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <09 July 2018>          
-- Description: <Getting top 50 customers for SSRS reports>          
      
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisTIPReport_Excess_V1] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65),      
@GPPct as varchar(20),@version char = '0', @IncludeInterco char = '0' , @noOfDays varchar(10), @IncludeLTA char(1) = '1', @FilterOnGPPct char = '0'        
          
AS          
BEGIN          
 SET NOCOUNT ON;  
 
         
declare @sqltxt varchar(6000)          
declare @execSQLtxt varchar(7000)          
declare @DB varchar(100)          
declare @FD varchar(10)          
declare @TD varchar(10)          
declare @NOOfCust varchar(15)          
DECLARE @ExchangeRate varchar(15)         
DECLARE @IsExcInterco char(1)         
declare @FD1 varchar(10)            
declare @TD1 varchar(10)        
declare @FD2 varchar(10)            
declare @TD2 varchar(10)        
          
set @DB=  @DBNAME          
        
if (@FromDate = '' OR @FromDate = '1900-01-01')        
BEGIN          
 set @FromDate = CONVERT(VARCHAR(10),DATEADD(day, -8, GETDATE()), 120)         
 END         
        
if @ToDate = '' OR @ToDate = '1900-01-01'        
BEGIN          
 set @ToDate = CONVERT(VARCHAR(10),DATEADD(day, -1, GETDATE()), 120)         
 END         
        
if(@noOfDays = '')        
Begin        
set @noOfDays  = 180;        
end        
        
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)          
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)          
set @FD1 = CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)    -- current month from date        
set @TD1 = @FD  --current month To date        
        
set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date        
set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date        
          
CREATE TABLE #tmp (   CustID   VARCHAR(10)          
        , CustName     VARCHAR(65)          
        ,SalesPersonInside VARCHAR(10)          
        ,SalesPersonOutside VARCHAR(10)        
        ,SalesPersonTaken VARCHAR(10)        
        , Market   VARCHAR(65)           
        , Branch   VARCHAR(65)           
        , Form           Varchar(65)          
        , Grade           Varchar(65)          
        , TotWeight  DECIMAL(20, 2)          
        , WeightUM           Varchar(10)          
                    , TotalValue    DECIMAL(20, 2)          
        , NetGP               DECIMAL(20, 2)          
        , TotalMatValue    DECIMAL(20, 2)          
        , MatGP    DECIMAL(20, 2)          
        , Databases   VARCHAR(15)           
        , MarketID   integer,          
        Finish varchar(35),          
        Size varchar(35),stn_cry   varchar(5)        
        ,InvDate Varchar(10)        
        ,OrdNo int       
        ,InvNo int        
        ,ShptPfx Varchar(2)        
        ,ShptNo  Varchar(10)        
        ,ShptItm  Varchar(10)        
        ,Age DATeTime         
        ,InvoiceMonthTotal DECIMAL(20, 2)        
        ,prd_ohd_wg DECIMAL(20, 2)        
        ,orgTotalWgt Decimal(20,2)      
        ,sizeDesc Varchar(30)      
        ,millName Varchar(40)       
        ,MillDlyDate Varchar(10) 
        ,ShpgWhs Varchar(3)  
        ,ReplCost decimal(20, 2)
                 );        
                     
CREATE TABLE #tmpAge (   CustID   VARCHAR(10)          
        , Branch   VARCHAR(65)           
        , Databases   VARCHAR(15)           
        ,InvNo Varchar(25)        
        ,ShptPfx Varchar(2)        
        ,ShptNo  Varchar(10)        
        ,ShptItm  Varchar(10)        
        ,JobNo Varchar(10)         
                 );         
          
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(35);          
DECLARE @Name VARCHAR(15);          
DECLARE @CurrenyRate varchar(15);          
          
if @Market ='ALL'          
 BEGIN          
 set @Market = ''          
 END          
        
if ( @IncludeInterco = '0')        
BEGIN        
set @IsExcInterco ='T'        
END        
         
if( @FilterOnGPPct = '0')        
BEGIN        
set @GPPct = ''         
End        
          
IF @DBNAME = 'ALL'          
 BEGIN          
   IF @version = '0'          
  BEGIN          
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName          
    OPEN ScopeCursor;          
  END          
  ELSE          
  BEGIN          
  DECLARE ScopeCursor CURSOR FOR          
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS           
    OPEN ScopeCursor;          
  END          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
        DECLARE @query NVARCHAR(max);             
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'          
                
       IF (UPPER(@Prefix) = 'TW')          
        begin          
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
        End          
        Else if (UPPER(@Prefix) = 'NO')          
        begin          
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
        End          
        Else if (UPPER(@Prefix) = 'CA')          
        begin          
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
        End          
        Else if (UPPER(@Prefix) = 'CN')          
        begin          
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
        End          
        Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')          
        begin          
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
        End          
        Else if(UPPER(@Prefix) = 'UK')          
        begin          
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
        End          
        Else if(UPPER(@Prefix) = 'DE')          
        begin          
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
        End          
        -- Calculation of Age         
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)        
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,          
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)        
      from ' + @DB + '_sahstn_rec         
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no        
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no        
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '        
  print(@sqltxt)          
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);         
                
      if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')    -- UPPER(@Prefix) = 'TW' OR          
                BEGIN          
         SET @query ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,  
         ShptPfx   ,ShptNo,ShptItm,Age,InvoiceMonthTotal,prd_ohd_wg,orgTotalWgt,sizeDesc,millName ,MillDlyDate,ShpgWhs,ReplCost)               
          select stn_sld_cus_id as CustID, ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,cus_cus_long_nm as CustName, cuc_desc30 as Market,          
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade, '          
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')             
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'          
             ELSE          
               SET @query = @query + ' SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,'          
                  
        SET @query = @query + '          
          
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,           
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,          
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,           
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,          
cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,        
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''        
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm        
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0         
         where itd_ref_no = ta.JobNo)  as Age,'        
          if(@IncludeLTA = '0')        
         begin        
         SET @query = @query + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''         
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = stn_shpt_brh)) as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt* 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd        
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh and oquery.prd_invt_sts = ''S''         
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''        
and oquery.prd_ord_ffm not in (        
select inq.prd_ord_ffm        
FROM ' + @DB + '_intprd_rec_history inq        
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''        
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = stn_shpt_brh        
 where inq.prd_invt_sts = ''S''         
          
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg, '        
         end        
         else        
         begin        
        SET @query = @query + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = stn_FRM and prd_grd = stn_GRD         
and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg, '        
        
end        
SET @query = @query + '        
(SELECT ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT* 2.20462,0)),0) as periodSelRange        
FROM ' + @DB + '_sahsat_rec currentPrdData        
WHERE currentPrdData.SAT_INV_DT  between '''+ @FD +'''  And '''+ @TD +'''        
    AND (currentPrdData.sat_shpt_brh = stn_shpt_brh ) and sat_frm = stn_FRM and  sat_grd = stn_GRD and sat_size = stn_size and sat_fnsh = stn_fnsh),      
(select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_fnsh = stn_fnsh and prm_size = stn_size ),    
(select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,      
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,
 (case when ou.stn_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and 
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else ou.stn_shpg_whs end ) as ''ShpgWhs''
 ,(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)* '+ @CurrenyRate +'  )/2.20462 else ((ppb_repl_cst + 
			ppb_frt_in_cst) * '+ @CurrenyRate +' ) end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  stn_frm and ppb_GRD = stn_grd  and ppb_size = 
			stn_size and ppb_fnsh = stn_fnsh order by  ppb_rct_expy_dt desc ) as MonthlyAvgReplCost
         from ' + @DB + '_sahstn_rec  ou         
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id  
         join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no           
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''        
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm         
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''    
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = stn_is_slp       
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = stn_os_slp  
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp          
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''          
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
          and STN_SHPT_BRH not in (''SFS'')        
          group by  stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id,tks.slp_lgn_id,stn_sls_cat, cus_cus_long_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,           
          stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo,stn_shpg_whs            
         order by stn_sld_cus_id '          
    END          
       Else          
      BEGIN          
        SET @query ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,      
        TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,ShptPfx,ShptNo,ShptItm,Age,InvoiceMonthTotal,prd_ohd_wg,orgTotalWgt,sizeDesc,millName ,MillDlyDate,ShpgWhs,ReplCost )
          select stn_sld_cus_id as CustID, ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id,cus_cus_long_nm as CustName, cuc_desc30 as Market,          
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,          
        SUM(stn_blg_wgt) as TotWeight,           
        stn_ord_wgt_um as WeightUM,          
          
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,           
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,          
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,           
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,          
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm ,        
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''        
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm        
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0         
         where itd_ref_no = ta.JobNo)  as Age, '        
         if(@IncludeLTA = '0')        
         begin        
         SET @query = @query + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''         
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = stn_shpt_brh)) as InvoiceMonthTotal,        
     
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd        
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh  and oquery.prd_invt_sts = ''S''         
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''        
and oquery.prd_ord_ffm not in (        
select inq.prd_ord_ffm        
FROM ' + @DB + '_intprd_rec_history inq        
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''        
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = stn_shpt_brh        
 where inq.prd_invt_sts = ''S''         
          
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg, '        
         end        
         else        
         begin        
        SET @query = @query + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = stn_FRM and prd_grd = stn_GRD         
and prd_size = stn_size and prd_fnsh = stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg,'        
end        
SET @query = @query + '        
        
(SELECT ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange        
FROM ' + @DB + '_sahsat_rec currentPrdData        
WHERE currentPrdData.SAT_INV_DT  between '''+ @FD +'''  And '''+ @TD +'''        
    AND (currentPrdData.sat_shpt_brh = stn_shpt_brh ) and sat_frm = stn_FRM and  sat_grd = stn_GRD and sat_size = stn_size and sat_fnsh = stn_fnsh) ,      
(select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_fnsh = stn_fnsh and prm_size = stn_size ),    
(select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,      
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ), '
if  (UPPER(@Prefix)= 'US') 
SET @query = @query + ' stn_shpg_whs  '
else
SET @query = @query + '
  (case when ou.stn_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and 
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else ou.stn_shpg_whs end ) as ''ShpgWhs'' '
 SET @query = @query + ' ,(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)* '+ @CurrenyRate +'  )/2.20462 else ((ppb_repl_cst + 
			ppb_frt_in_cst) * '+ @CurrenyRate +' ) end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  stn_frm and ppb_GRD = stn_grd  and ppb_size = 
			stn_size and ppb_fnsh = stn_fnsh order by  ppb_rct_expy_dt desc ) as MonthlyAvgReplCost
         from ' + @DB + '_sahstn_rec ou          
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id   
         join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no        
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''        
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm           
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''    
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = stn_is_slp       
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = stn_os_slp  
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp              
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''          
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')        
          and STN_SHPT_BRH not in (''SFS'')        
          group by  stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id, stn_sls_cat,cus_cus_long_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,           
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo,stn_shpg_whs          
         order by stn_sld_cus_id '          
      End          
        EXECUTE sp_executesql @query;          
                  
                  
        print(@query)          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN         
     IF @version = '0'          
     BEGIN            
     Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')          
     END        
     ELSE        
     BEGIN        
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')          
     END        
             
    IF (UPPER(@DBNAME) = 'TW')          
   begin          
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
   End          
   Else if (UPPER(@DBNAME) = 'NO')          
   begin          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
   End          
   Else if (UPPER(@DBNAME) = 'CA')          
   begin          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
   End          
   Else if (UPPER(@DBNAME) = 'CN')          
   begin          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
   End          
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')          
   begin          
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
   End          
   Else if(UPPER(@DBNAME) = 'UK')          
   begin          
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
   End          
   Else if(UPPER(@DBNAME) = 'DE')          
   begin          
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
   End          
               
   if  ( UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')    --UPPER(@DBNAME) = 'TW' OR         
                BEGIN          
                        
                -- Calculation of Age         
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)        
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,          
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)        
      from ' + @DB + '_sahstn_rec         
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no        
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no        
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '       
  print(@sqltxt)          
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);         
           
         SET @sqltxt ='INSERT INTO #tmp (CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken , CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,      
         TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,ShptPfx,ShptNo,ShptItm,Age,InvoiceMonthTotal,prd_ohd_wg,orgTotalWgt,sizeDesc,millName ,MillDlyDate ,ShpgWhs,ReplCost)               
          select stn_sld_cus_id as CustID,ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id, cus_cus_long_nm as CustName, cuc_desc30 as Market,          
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,'          
        if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')             
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as TotWeight,Case stn_ord_wgt_um when ''KGS'' then ''LBS'' else stn_ord_wgt_um end as WeightUM,'          
             ELSE          
               SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as TotWeight,stn_ord_wgt_um as WeightUM,'           
                  
       SET @sqltxt = @sqltxt + '           
          
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,           
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,          
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,           
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,          
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm  ,        
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''        
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm        
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0         
         where itd_ref_no = ta.JobNo)  as Age, '        
          if(@IncludeLTA = '0')        
         begin        
         SET @sqltxt = @sqltxt + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''         
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = stn_shpt_brh)) as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt* 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd        
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh  and oquery.prd_invt_sts = ''S''         
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''        
and oquery.prd_ord_ffm not in (        
select inq.prd_ord_ffm        
FROM ' + @DB + '_intprd_rec_history inq        
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''        
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = stn_shpt_brh        
 where inq.prd_invt_sts = ''S''         
          
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg, '        
         end        
         else        
         begin        
        SET @sqltxt = @sqltxt + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = stn_FRM and prd_grd = stn_GRD         
and prd_size = stn_size and prd_fnsh = stn_fnsh  and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg,'        
End        
 SET @sqltxt = @sqltxt + '        
(SELECT ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT* 2.20462,0)),0) as periodSelRange        
FROM ' + @DB + '_sahsat_rec currentPrdData        
WHERE currentPrdData.SAT_INV_DT  between '''+ @FD +'''  And '''+ @TD +'''        
    AND (currentPrdData.sat_shpt_brh = stn_shpt_brh ) and sat_frm = stn_FRM and  sat_grd = stn_GRD and sat_size = stn_size and sat_fnsh = stn_fnsh) ,      
(select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_fnsh = stn_fnsh and prm_size = stn_size ),    
(select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,      
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ),
  (case when ou.stn_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and 
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else ou.stn_shpg_whs end ) as ''ShpgWhs''
 ,(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)* '+ @CurrenyRate +'  )/2.20462 else ((ppb_repl_cst + 
			ppb_frt_in_cst)* '+ @CurrenyRate +'  ) end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  stn_frm and ppb_GRD = stn_grd  and ppb_size = 
			stn_size and ppb_fnsh = stn_fnsh order by  ppb_rct_expy_dt desc ) as MonthlyAvgReplCost
 from ' + @DB + '_sahstn_rec  ou        
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id   
         join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no          
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat          
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''        
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm           
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''    
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = stn_is_slp       
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = stn_os_slp  
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp              
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''          
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')          
          and STN_SHPT_BRH not in (''SFS'')        
          group by  stn_sld_cus_id,ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id,stn_sls_cat, cus_cus_long_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,           
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt ,ta.JobNo ,stn_shpg_whs          
         order by stn_sld_cus_id '          
    END          
       Else          
      BEGIN          
      -- Calculation of Age         
       SET @sqltxt ='INSERT INTO #tmpAge (CustID, Branch, Databases,InvNo,ShptPfx,ShptNo,ShptItm,JobNo)        
        select stn_sld_cus_id,STN_SHPT_BRH, '''+ @Name +''' as databases, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,          
 (case When Min(ISNULL(rps_job_no_1,0))>0 then  Min(rps_job_no_1) else   Min(ssh_job_no_1) end)        
      from ' + @DB + '_sahstn_rec         
      left join  ' + @DB + '_iptrps_rec on rps_cmpy_id = stn_cmpy_id and  rps_ord_pfx = stn_ord_pfx and rps_ord_no =stn_ord_no and rps_ord_itm = stn_ord_itm and rps_ord_rls_no = stn_ord_rls_no        
left join  ' + @DB + '_ipjssh_rec   on ssh_ord_pfx = stn_ord_pfx and ssh_ord_no =stn_ord_no and ssh_ord_itm = stn_ord_itm and ssh_ord_sitm = stn_ord_rls_no        
  group by stn_sld_cus_id,STN_SHPT_BRH, stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm '        
  print(@sqltxt)          
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);         
              
        SET @sqltxt ='INSERT INTO #tmp (CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,  
        ShptPfx  ,  ShptNo,ShptItm,Age,InvoiceMonthTotal,prd_ohd_wg,orgTotalWgt,sizeDesc,millName ,MillDlyDate,ShpgWhs,ReplCost )         
          select stn_sld_cus_id as CustID,ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id, cus_cus_long_nm as CustName, cuc_desc30 as Market,          
        STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,          
        SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,          
          
        SUM(stn_tot_val * '+ @CurrenyRate +') as TotalValue,           
        SUM(stn_npft_avg_val * '+ @CurrenyRate +') as NetGP,          
        SUM(stn_tot_mtl_val * '+ @CurrenyRate +') as TotalMatValue ,           
        SUM(stn_mpft_avg_val * '+ @CurrenyRate +') as MatGP,          
          cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry, stn_fnsh, stn_size ,stn_inv_dt,stn_ord_no,stn_upd_ref,stn_shpt_pfx, stn_shpt_no, stn_shpt_itm ,        
         (Select MAX(pcr_agng_dtts) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec on itd_cmpy_id = pcr_cmpy_id and pcr_itm_ctl_no = itd_itm_ctl_no and itd_ref_pfx = ''IP''        
join ' + @DB + '_injitv_rec on itd_cmpy_id = itv_cmpy_id and itd_ref_pfx = itv_ref_pfx and itd_ref_no = itv_ref_no and itd_ref_itm = itv_ref_itm        
and itd_ref_sbitm = itv_ref_sbitm and itd_actvy_dt = itv_actvy_dt and itd_trs_seq_no = itv_trs_seq_no and itv_trs_pcs <0         
         where itd_ref_no = ta.JobNo)  as Age, '        
          if(@IncludeLTA = '0')        
         begin        
         SET @sqltxt = @sqltxt + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + '''         
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = stn_shpt_brh)) as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = stn_frm and oquery.prd_grd = stn_grd        
and oquery.prd_size = stn_size and oquery.prd_fnsh = stn_fnsh  and oquery.prd_invt_sts = ''S''         
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''        
and oquery.prd_ord_ffm not in (        
select inq.prd_ord_ffm        
FROM ' + @DB + '_intprd_rec_history inq        
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8) and orh_ord_pfx = ''SO''        
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = stn_shpt_brh        
 where inq.prd_invt_sts = ''S''         
          
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg, '        
         end        
         else        
         begin        
        SET @sqltxt = @sqltxt + '        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = stn_frm and sixMonthWgt.sat_grd = stn_grd and sixMonthWgt.sat_size = stn_size        
and sixMonthWgt.sat_fnsh = stn_fnsh and sixMonthWgt.sat_shpt_brh = stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @FD1 + '''  And ''' + @TD1 + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = stn_FRM and prd_grd = stn_GRD         
and prd_size = stn_size and prd_fnsh = stn_fnsh  and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg, '        
End        
SET @sqltxt = @sqltxt + '        
(SELECT ISNULL(SUM(ISNULL(currentPrdData.SAT_BLG_WGT,0)),0) as periodSelRange        
FROM ' + @DB + '_sahsat_rec currentPrdData        
WHERE currentPrdData.SAT_INV_DT  between '''+ @FD +'''  And '''+ @TD +'''        
    AND (sat_shpt_brh = stn_shpt_brh ) and sat_frm = stn_FRM and  sat_grd = stn_GRD and sat_size = stn_size and sat_fnsh = stn_fnsh) ,      
(select top 1 prm_size_desc from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_fnsh = stn_fnsh and prm_size = stn_size ),        
(select top 1 mil_nm from ' + @DB + '_inrmil_rec join ' + @DB + '_injitd_rec on itd_mill = mil_mill where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ) ,      
  (select Convert(varchar(10),Min(pcr_agng_dtts),120) from ' + @DB + '_intpcr_rec join ' + @DB + '_injitd_rec  on itd_cmpy_id = pcr_cmpy_id and itd_itm_ctl_no = pcr_itm_ctl_no where itd_ref_pfx = Max(stn_sprc_pfx) and itd_ref_no = Max(stn_sprc_no) ), '
  if  (UPPER(@DBNAME)= 'US') 
SET @sqltxt = @sqltxt + ' stn_shpg_whs  '
else
SET @sqltxt = @sqltxt + '
  (case when ou.stn_sls_cat = ''DS'' then 
 (select top 1 us.stn_shpg_whs from US_SAHSTN_REC us Where ou.stn_cus_po = us.stn_end_usr_po and 
 cast(ou.stn_ord_itm as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',1)), 6) 
 and cast(ou.stn_ord_rls_no as varchar(2)) = substring([dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2),patindex(''%[^0]%'',[dbo].[fun_itech_GetColumnValue](RTRIM(LTRIM(us.stn_cus_rls)),''-'',2)), 6)  )  
 else ou.stn_shpg_whs end ) as ''ShpgWhs'''
 SET @sqltxt = @sqltxt + ' ,(Select top 1 (case when ppb_repl_cst_um = ''KGS'' then ((ppb_repl_cst + ppb_frt_in_cst)* '+ @CurrenyRate +'  )/2.20462 else ((ppb_repl_cst + 
			ppb_frt_in_cst) * '+ @CurrenyRate +' ) end) as replacementCost   from ' + @DB + '_perppb_rec  where ppb_FRM =  stn_frm and ppb_GRD = stn_grd  and ppb_size = 
			stn_size and ppb_fnsh = stn_fnsh order by  ppb_rct_expy_dt desc ) as MonthlyAvgReplCost
         from ' + @DB + '_sahstn_rec  ou       
         join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id  
         join ' + @DB + '_ortorh_rec on orh_cmpy_id = stn_cmpy_id and  orh_ord_pfx = stn_ord_pfx and orh_ord_no = stn_ord_no         
         left join #tmpAge ta on  CustID = stn_sld_cus_id and Branch = STN_SHPT_BRH and Databases = '''+ @Name +'''        
         and InvNo = stn_upd_ref and ShptPfx = stn_shpt_pfx and ShptNo = stn_shpt_no and ShptItm = stn_shpt_itm         
         left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat            
         Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD''    
         left join '+ @DB +'_scrslp_rec ins on ins.slp_cmpy_id = stn_cmpy_id and ins.slp_slp = stn_is_slp       
         left join '+ @DB +'_scrslp_rec ous on ous.slp_cmpy_id = stn_cmpy_id and ous.slp_slp = stn_os_slp  
         left join '+ @DB +'_scrslp_rec tks on tks.slp_cmpy_id = stn_cmpy_id and tks.slp_slp = stn_tkn_slp              
         where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''          
          and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')         
          and STN_SHPT_BRH not in (''SFS'')        
          group by  stn_sld_cus_id, ins.slp_lgn_id,ous.slp_lgn_id, tks.slp_lgn_id,stn_sls_cat,cus_cus_long_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd, stn_fnsh, stn_size,stn_inv_dt,stn_ord_no,stn_upd_ref, stn_shpt_pfx, stn_shpt_no, stn_shpt_itm,        
         stn_cus_po,stn_ord_itm,stn_ord_rls_no,stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt,ta.JobNo,stn_shpg_whs          
         order by stn_sld_cus_id '          
      End          
      print(@sqltxt)          
    set @execSQLtxt = @sqltxt;           
   EXEC (@execSQLtxt);          
     END          
            
 if(@IncludeLTA = '0')        
begin        
             
   if @IsExcInterco ='T'        
  BEGIN        
              
   print @GPpct;          
   if(@GPPct = '')        
   begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)  as AgeDays ,  
  
      
   (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),Branch))>0 then 0 else         
   (case when SUM(TotWeight) < (Sum(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end) end) as excess ,      
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product ,millName ,Min(MillDlyDate) as MillDlyDate ,ShpgWhs 
   ,ReplCost
   FROM #tmp  Where (Market <> 'Interco' or Market is null)          
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,      
    MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc ,millName,ShpgWhs ,ReplCost      
    order by CustID           
    end        
    else        
    begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)  as AgeDays , 
   
      
   (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),Branch))>0 then 0 else         
   (case when SUM(TotWeight) < (Sum(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end) end) as excess ,      
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product ,millName ,Min(MillDlyDate) as MillDlyDate ,ShpgWhs,ReplCost      
   FROM #tmp           
   where (case SUM(TotalValue) when 0 then 0 else (SUM(NetGP)/SUM(TotalValue))* 100 end) <= @GPPct  and (Market <> 'Interco' or Market is null)   
            
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,      
    MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc ,millName,ShpgWhs,ReplCost        
    order by CustID           
    end        
            
    end        
            
    else        
    Begin        
            
    print @GPpct;          
   if(@GPPct = '')        
   begin        
              
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0) as AgeDays,
  
        
   (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),Branch))>0 then 0 else         
   (case when SUM(TotWeight) < (Sum(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end) end) as excess ,       
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product,millName ,Min(MillDlyDate) as MillDlyDate,ShpgWhs ,ReplCost
     FROM #tmp             
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc,millName,ShpgWhs ,ReplCost     
    order by CustID           
    end        
    else        
    begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,  SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0) as AgeDays,
  
        
   (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),Branch))>0 then 0 else         
   (case when SUM(TotWeight) < (Sum(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end) end) as excess ,         
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product,millName ,Min(MillDlyDate) as MillDlyDate,ShpgWhs ,ReplCost      
    FROM #tmp           
   where (case SUM(TotalValue) when 0 then 0 else (SUM(NetGP)/SUM(TotalValue))* 100 end) <= @GPPct   
          
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size ,InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt      
    , sizeDesc ,millName,ShpgWhs,ReplCost      
    order by CustID           
    end        
    end        
End        
Else        
Begin        
  if @IsExcInterco ='T'        
  BEGIN        
              
   print @GPpct;          
   if(@GPPct = '')        
   begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,  SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)  as AgeDays ,  
  
      
   (case when SUM(TotWeight) < (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end)  as excess  ,      
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product ,millName ,Min(MillDlyDate) as MillDlyDate,ShpgWhs,ReplCost
   FROM #tmp  Where (Market <> 'Interco' or Market is null)         
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc,millName,ShpgWhs,ReplCost          
    order by CustID           
    end        
    else        
    begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade,  SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0) as AgeDays,    
  
    
   (case when SUM(TotWeight) < (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end)  as excess  ,      
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product,millName ,Min(MillDlyDate) as MillDlyDate,ShpgWhs ,ReplCost
   FROM #tmp           
   where (case SUM(TotalValue) when 0 then 0 else (SUM(NetGP)/SUM(TotalValue))* 100 end) <= @GPPct  and (Market <> 'Interco' or Market is null) 
   
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size ,InvDate,OrdNo,InvNo,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc,millName,ShpgWhs,ReplCost
    order by CustID           
    end        
            
    end        
            
    else        
    Begin        
            
    print @GPpct;          
   if(@GPPct = '')        
   begin        
              
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0) as AgeDays
,  
        
   (case when SUM(TotWeight) < (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end)  as excess  ,       
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product ,millName ,Min(MillDlyDate) as MillDlyDate,ShpgWhs,ReplCost
     FROM #tmp            
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size,InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
   , sizeDesc,millName,ShpgWhs,ReplCost      
    order by CustID           
    end        
    else        
    begin        
    SELECT RTRIM(LTrim(CustID))+'-'+Databases as CustID, SalesPersonInside,SalesPersonOutside,SalesPersonTaken,CustName,Market,Branch,Form,Grade, SUM(TotWeight) as TotWeight,'LBS' as WeightUM,    
    SUM(TotalValue) as TotalValue,SUM(NetGP) as NetGP,SUM(TotalMatValue) as TotalMatValue,SUM(MatGP) as MatGP,MarketID,Databases,stn_cry, Finish, Size,      
   (case SUM(TotalValue) when 0 then 0 else (Sum(MatGP)/Sum(TotalValue))* 100 end) as 'GP%',(case Sum(TotalValue) when 0 then 0 else (Sum(NetGP)/SUM(TotalValue))* 100 end) as 'NetGP%',InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0) as AgeDays,
  
        
   (case when SUM(TotWeight) < (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) then SUM(TotWeight) else (SUM(prd_ohd_wg) - SUM(InvoiceMonthTotal)) end)  as excess  ,        
   (RTRIM(LTRIM(Form)) + '/' + RTRIM(LTRIM(Grade)) + '/' + RTRIM(LTRIM(sizeDesc)) + '/' + RTRIM(LTRIM(Finish))) as product ,millName ,Min(MillDlyDate) as MillDlyDate ,ShpgWhs,ReplCost
    FROM #tmp           
   where (case SUM(TotalValue) when 0 then 0 else (SUM(NetGP)/SUM(TotalValue))* 100 end) <= @GPPct  
   
    group by CustID,SalesPersonInside,SalesPersonOutside,SalesPersonTaken, CustName,Market,Branch,Form,Grade,WeightUM,MarketID,Databases,stn_cry, Finish, Size ,InvDate,OrdNo,InvNo,Age,ISNULL(DateDiff(Day,Age,InvDate),0)--,orgTotalWgt       
    , sizeDesc ,millName ,ShpgWhs,ReplCost    
    order by CustID           
    end        
    end        
End        
  print @GPpct;         
          
          
  drop table #tmp;        
drop table #tmpAge;             
END          
-- @GPPct as varchar(20),@version char = '0', @IncludeInterco char = '0' , @noOfDays varchar(10), @IncludeLTA char(1) = '1', @FilterOnGPPct char = '0'        
        
-- exec [sp_itech_SalesAnalysisTIPReport_Excess] '08/01/2017', '08/31/2017' , 'ALL','ALL','','0' ,'1','0','1'   
-- exec [sp_itech_SalesAnalysisTIPReport_Excess_V1] '08/01/2017', '08/31/2017' , 'ALL','ALL','','0' ,'1','0','1'        
/*        
date : 20161025        
mail sub: report fix        
-- @GPPct act as net profit %        
date:  20160630        
Mail sub:Aged Inventory Sales        
Exclude LTA Sales of Excess lbs. option selected        
         
        
DO NOT INCLUDE WEIGHT SHIPPED to LTA Customers for excess weight  calculation        
         
        
+        
         
        
Set EXCESS Inventory for LTA Customer to 0 AND negative excess inventory is set to 0 for all customers. For rest of the customers keep showing the EXCESS inventory        
        
Include LTA Sales of Excess lbs. Option selected        
         
        
INCLUDE WEIGHT SHIPPED to LTA Customers for excess weight  calculation        
         
        
+        
         
        
Show EXCESS Inventory for All customers including LTA AND negative excess inventory is set to 0 for all customers        
        
date:  20160630        
Fwd: FW: Aged Inventory Report ver 2 Observations and Requests        
1.  Please change the "Include LTA" to "Exclude LTA's"        
2.  Please create a toggle on the GP% less than or equal to as I don't usually use this feature.        
3.  Please set the default Age of Inventory to 1 (or zero if allowed).        
        
        
date :2016-05-18        
mail:option out PSM and especially SFS when all databases is selected.        
I thought we discussed this and you were doing this progressively it appears the bulk of these reports do not have this toggle built in an this should now make sure we take out SFS if it was part of any report. Please get someone on this ASAP as we are o
n  
    
      
ce again looking like jack asses in the ops meeting!        
        
date : 2016-05-24        
sub: Aged Inventory Report        
In addition to the LTA toggle on the Aged Inventory report can you also please have Prabh convert the Invoice Date field to an actual date field.           
        
Excel does not recognize the text format "2016-12-23" it will recognize 12/23/2016 though      
    
Date 20170821    
sub:Two Report Modifications Request      
    
Date:20170908    
Sub:Another Data Validation Request    
   
Date 20170915
Sub: Additional Request - Add Shipping Warehouse 

date 20180420/20180507
sub: Please check this report   

date 20180709
sub:Report Modification   

Date 20181213
Sub:Report Mod Request 
*/
GO
