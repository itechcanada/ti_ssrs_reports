USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[RACK_CAPACITY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,2/14/2012>
-- Description:	<Description,Rack Capacity>
-- =============================================
CREATE PROCEDURE [dbo].[RACK_CAPACITY]
	-- Add the parameters for the stored procedure here
	@Warehouse varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT a.prd_whs,a.prd_loc, a.prd_tag_no, a.prd_ohd_wgt, b.loc_capy_wgt, b.loc_capy_wgt_um
FROM [LIVEUSSTX].[liveusstxdb].[informix].[intprd_rec]  a
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[inrloc_rec] b ON a.prd_whs=b.loc_whs AND a.prd_loc=b.loc_loc
WHERE
b.loc_capy_wgt NOT IN(9999999999.00, 999999999.00, 9999999.00, 999999.00, 99999.00)
AND a.prd_whs=@Warehouse
UNION
SELECT a.prd_whs,a.prd_loc, a.prd_tag_no, a.prd_ohd_wgt, b.loc_capy_wgt, b.loc_capy_wgt_um
FROM [LIVEUKSTX].[liveukstxdb].[informix].[intprd_rec]  a
INNER JOIN [LIVEUKSTX].[liveukstxdb].[informix].[inrloc_rec] b ON a.prd_whs=b.loc_whs AND a.prd_loc=b.loc_loc
WHERE
b.loc_capy_wgt NOT IN(9999999999.00, 999999999.00, 9999999.00, 999999.00, 99999.00)
AND a.prd_whs=@Warehouse
UNION
SELECT a.prd_whs,a.prd_loc, a.prd_tag_no, a.prd_ohd_wgt, b.loc_capy_wgt, b.loc_capy_wgt_um
FROM [LIVECASTX].[livecastxdb].[informix].[intprd_rec]  a
INNER JOIN [LIVECASTX].[livecastxdb].[informix].[inrloc_rec] b ON a.prd_whs=b.loc_whs AND a.prd_loc=b.loc_loc
WHERE
b.loc_capy_wgt NOT IN(9999999999.00, 999999999.00, 9999999.00, 999999.00, 99999.00)
AND a.prd_whs=@Warehouse
UNION
SELECT a.prd_whs,a.prd_loc, a.prd_tag_no, a.prd_ohd_wgt, b.loc_capy_wgt, b.loc_capy_wgt_um
FROM [LIVETWSTX].[livetwstxdb].[informix].[intprd_rec]  a
INNER JOIN [LIVETWSTX].[livetwstxdb].[informix].[inrloc_rec] b ON a.prd_whs=b.loc_whs AND a.prd_loc=b.loc_loc
WHERE
b.loc_capy_wgt NOT IN(9999999999.00, 999999999.00, 9999999.00, 999999.00, 99999.00)
AND a.prd_whs=@Warehouse

END

GO
