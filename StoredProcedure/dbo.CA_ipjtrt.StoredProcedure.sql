USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_ipjtrt]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/6/2012,>
-- Description:	<Description,Open Orders,>

-- =============================================
CREATE PROCEDURE [dbo].[CA_ipjtrt]
	
AS
BEGIN
	-- SET USCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Delete from dbo.CA_ipjtrt_rec ;	
	
Insert into dbo.CA_ipjtrt_rec	

    -- Insert statements for procedure here
SELECT *
FROM [LIVECASTX].[livecastxdb].[informix].[ipjtrt_rec]


END





















GO
