USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[AGED_AP]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Wendy Wang>  
-- Create date: <Create Date,3/5/2012>  
-- Description: <Description,Aged Accounts Payable (no balance total on Stratix) for Canada>  
-- =============================================  
CREATE PROCEDURE [dbo].[AGED_AP]   
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
SELECT a.pyh_ven_id,b.ven_ven_long_nm,a.pyh_due_dt,a.pyh_opa_pfx, a.pyh_opa_no,a.pyh_ap_brh,a.pyh_ven_inv_no, a.pyh_balamt,a.pyh_desc30  
FROM [LIVECASTX].[livecastxdb].[informix].[aptpyh_rec] a  
INNER JOIN [LIVECA_IW].[livecastxdb_iw].[informix].[aprven_rec] b ON a.pyh_ven_id=b.ven_ven_id  
WHERE   
a.pyh_cry='USD'  
AND a.pyh_balamt<>0                                                                                                                          
END  
GO
