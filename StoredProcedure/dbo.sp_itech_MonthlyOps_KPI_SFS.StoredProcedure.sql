USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyOps_KPI_SFS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  <Mukesh >        
-- Create date: <14 Dec 2016>        
    
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_MonthlyOps_KPI_SFS] @DBNAME varchar(50),@Branch varchar(10),@FromDate datetime, @ToDate datetime, @IncludeLTA char(1) = '1' , @version char = '0'      
        
AS        
BEGIN        
         
         
 SET NOCOUNT ON;        
declare @sqltxt varchar(6000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
declare @FD varchar(10)        
declare @TD varchar(10)         
declare @NFD varchar(10)      
declare @NTD varchar(10)      
declare @D3MFD varchar(10)      
declare @D3MTD varchar(10)      
declare @D6MFD varchar(10)      
declare @D6MTD varchar(10)      
declare @LFD varchar(10)      
declare @LTD varchar(10)     
declare @AFD varchar(10)      
declare @ATD varchar(10)      
declare @FD2 varchar(10)        
declare @TD2 varchar(10)    
declare @RFD varchar(10)    
declare @RTD varchar(10)    
    
declare @ExcessFD varchar(10)    
declare @ExcessTD varchar(10)    
     
set @DB=  @DBNAME         
 set @FD = CONVERT(VARCHAR(10), @FromDate,120)    
 set @TD = CONVERT(VARCHAR(10), @ToDate,120)     
     
 set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @RTD = CONVERT(VARCHAR(10), @ToDate,120)   -- New Account      
     
 set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate),0)) , 120)   -- New Account      
       set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)  -- New Account      
      
       set @D3MFD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate)-3, 0),120)   -- Dormant 3 Month  correct    
       set @D3MTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-3, -1),120)  -- Dormant 3 Month correct     
      
       set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-6,0)) , 120)   --Dormant 6 Month      
       set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-5,0)), 120)  -- Dormant 6 Month      
           
    
  set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date    
  set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date    
    
       set @ExcessFD =  CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)    
       set @ExcessTD =   @FD    
       print 'ExFD ' + @ExcessTD    
           
       set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)   -- Lost Account      
       set @LTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-18, -1),120)  -- Lost Account  correct    
                 
       set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-18,0)) , 120)    -- Active Accounts       
       set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)), 120)      
      
    
CREATE TABLE #tmp1 (    Databases   VARCHAR(15)        
        , Branch   VARCHAR(3)         
         ,ShippedWGT    DECIMAL(20, 0)        
        , ShippedValue        DECIMAL(20, 0)        
        , BookedValue    DECIMAL(20, 0)     
        , GrossProfit Decimal(20,0)       
        , GProfitPercentage    DECIMAL(20, 1)        
        , OperExp   DECIMAL(20, 0)    
        ,OperProfit           DECIMAL(20, 0)        
        , ExcessInvLBSSold    DECIMAL(20, 0)        
        , MultiMetalLBSSold    DECIMAL(20, 0)    
        ,TotalAccount  Numeric      
        ,NewAccount  Numeric       
        ,ReclaimedAccount  Numeric     
        ,LostAccount  Numeric     
        ,DormantAccount3M  Numeric       
        ,DormantAccount6M  Numeric     
        , NoOfQuotes Numeric     
        , NoOfOrders Numeric     
        , NoOfLostOrder Numeric     
        ,PhoneCall Numeric     
  ,SalesVisit Numeric     
        ,LBS Varchar(10)    
        ,Pieces  Numeric     
        ,BOL Numeric     
        ,WhsHours numeric    
        , LBSPerHour numeric    
        , PiecesPerHour numeric    
        , BOLPerHour numeric   
        ,DeliveryOTP Decimal(20,1)     
                   );      
                       
 CREATE TABLE #NLDAccount (  TotalActive  numeric    
 ,ActiveAccount  numeric    
 ,D3MAccount numeric    
 ,D6MAccount numeric    
 ,LostAccount numeric    
 ,ReactAccount numeric    
 ,PhoneCall numeric    
 ,Visit numeric    
 ,booked numeric    
 ,Excess numeric    
 ,MultiMetal numeric    
 ,NoOfQuot numeric    
 ,NoOfOrder numeric    
 ,LostOrd numeric    
 ,Bol numeric    
 ,Otp Decimal(20,1)     
 ,OperProfit           DECIMAL(20, 2)   
 ,Branch Varchar(3)      
 );        
        
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @Prefix VARCHAR(5);        
DECLARE @Name VARCHAR(15);        
        
       
        
if @Branch ='ALL'        
 BEGIN        
 set @Branch = ''        
 END        
        
DECLARE @CurrenyRate varchar(15);        
        
IF @DBNAME = 'ALL'        
 BEGIN        
  
   IF @version = '0'    
  BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
    OPEN ScopeCursor;    
  END    
  ELSE    
  BEGIN    
         
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
End          
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
     WHILE @@FETCH_STATUS = 0        
       BEGIN        
      SET @DB= @Prefix        
     -- print(@DB)        
        DECLARE @query NVARCHAR(4000);        
         IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
         SET @query = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
              select  count(crd_cus_id),0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH      
              FROM (select  stn_sld_cus_id as CustomerID    
              from '+ @DB +'_sahstn_rec      
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @TD +'''     
              group by stn_sld_cus_id ) as t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id       
              where t.CustomerID = crd_cus_id    
              Group by CUS_ADMIN_BRH'    
               print @query;      
   EXECUTE sp_executesql @query;     
               
            SET @query = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
              select 0,COUNT(STN_SLD_CUS_ID) ,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
    select STN_SLD_CUS_ID, CUS_ADMIN_BRH      
               from '+ @DB +'_sahstn_rec INNER JOIN '+@DB+'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID       
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat       
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID      
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id       
               where dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @FD + '''  And ''' + @TD + '''     
               AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
               Group by  STN_SLD_CUS_ID,CUS_ADMIN_BRH ) as outerq group by CUS_ADMIN_BRH     
               '    
               print @query;      
   EXECUTE sp_executesql @query;     
               
           SET @query = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
                  
              select 0,0,COUNT(CUS_CUS_ID),0, 0,0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
     SELECT  CUS_CUS_ID,  CUS_ADMIN_BRH    
           FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id      
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between   ''' + @D3MFD + '''  And ''' + @D3MTD + '''     
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
           group by CUS_CUS_ID,  CUS_ADMIN_BRH  ) as outerq group by CUS_ADMIN_BRH     
               '    
               print @query;      
           EXECUTE sp_executesql @query;     
               
           -- D6MAccount added by mrinal.    
           SET @query = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
                  
              select 0,0,0, COUNT(CUS_CUS_ID), 0,0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
     SELECT  CUS_CUS_ID,  CUS_ADMIN_BRH    
           FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id      
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between   ''' + @D6MFD + '''  And ''' + @D6MTD + '''     
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
           group by CUS_CUS_ID,  CUS_ADMIN_BRH  ) as outerq group by CUS_ADMIN_BRH     
               '    
               print @query;      
           EXECUTE sp_executesql @query;     
                
            -- Lost added by mrinal.    
          SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
           SELECT 0,0,0,0,COUNT(t.STN_SLD_CUS_ID),0, 0,0,0,0,0,0,0,0,0,0,0,Branch FROM                
(                
SELECT   CUS_ADMIN_BRH as Branch , CUS_CUS_ID as STN_SLD_CUS_ID                  
    FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
               
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id     
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between  '''+@LFD+''' and '''+@LTD+'''      
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
           group by CUS_ADMIN_BRH,CUS_CUS_ID     
) as t GROUP BY Branch'    
print @query;      
     EXECUTE sp_executesql @query;     
               
               
               
           -- React by mrinal    
            SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
           Select 0,0,0,0,0,COUNT(crd_cus_id),0, 0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from (                  
 SELECT  crd_cus_id, CUS_ADMIN_BRH    
              FROM (    
              select  stn_sld_cus_id as CustomerID from '+ @DB +'_sahstn_rec      
              where STN_INV_DT >= '''+@RFD+''' and STN_INV_DT <= '''+@RTD+'''      
               group by stn_sld_cus_id ) t,    
                '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id       
              where       
               (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
               and  t.CustomerID Not IN       
              ( select distinct t1.CustomerID  from (select  stn_sld_cus_id as CustomerID from '+ @DB +'_sahstn_rec      
              where STN_INV_DT >= '''+@RFD+''' and STN_INV_DT <= '''+@RTD+'''      
               group by stn_sld_cus_id ) t1,      
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID       
              and u.STN_INV_DT > '''+@AFD+''' AND u.STN_INV_DT < '''+@RFD+''')      
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec       
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  '''+@RFD+''' And '''+@RTD+'''     
              )      
              and  t.CustomerID = crd_cus_id ) as outerqery group by CUS_ADMIN_BRH'    
                  
           print @query;      
           EXECUTE sp_executesql @query;     
               
               
             -- phone call by mrinal    
            SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
             Select 0,0,0,0,0,0,SUM( t.TotalPhoneCall) as TotalPhoneCall,0,0, 0,0,0,0,0,0,0,0,cus_admin_brh  from (          
                      
   select  cus_cus_id as CustomerId, COUNT(*) as TotalPhoneCall  , cus_admin_brh                       
       from '+ @DB +'_cctcta_rec join                         
       '+ @DB +'_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join                        
       '+ @DB +'_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id                                                      
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                                    
       and rtrim(atp_desc30) in (''Phone Call - Lost/Dormant'',''Phone Call - Existing Account'',''Phone Call - Prospect Account'')   
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, cus_cus_id    
       ) as t    
       GROUP  BY cus_admin_brh'    
                  
           print @query;      
           EXECUTE sp_executesql @query;    
               
               
        -- visit by mrinal    
            SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
             Select 0,0,0,0,0,0,0,SUM( t.TotalPhoneCall) as TotalPhoneCall, 0,0,0,0,0,0,0,0,0,cus_admin_brh  from (          
                      
   select  cus_cus_id as CustomerId, COUNT(*) as TotalPhoneCall  , cus_admin_brh                       
       from '+ @DB +'_cctcta_rec join                         
       '+ @DB +'_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join                        
       '+ @DB +'_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id                                                      
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                                    
       and rtrim(atp_desc30) in (''Visit - Prospect Account'',''Visit - Existing Account'',''Visit - Lost/Dormant Account'')   
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, cus_cus_id    
       ) as t    
       GROUP  BY cus_admin_brh'    
                  
         print @query;      
           EXECUTE sp_executesql @query;    
               
               
                    -- booked val by mrinal     
        SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)     
         Select 0,0,0,0,0,0,0,0,SUM( t.mbk_tot_val) as mbk_tot_val, 0,0,0,0,0,0,0,0,mbk_brh  from (    
SELECT a.mbk_brh, (a.mbk_tot_val* '+ @CurrenyRate +') as mbk_tot_val    
      FROM '+ @DB +'_ortmbk_rec a    
      join '+ @DB +'_ortorh_rec on orh_ord_pfx = a.mbk_ord_pfx and orh_ord_no = a.mbk_ord_no     
      INNER JOIN '+ @DB +'_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id      
      left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat     
      WHERE a.mbk_ord_pfx=''SO''       
        AND a.mbk_ord_itm<>999       
         and a.mbk_trs_md = ''A''    
         and orh_sts_actn <> ''C''      
         AND a.mbk_actvy_dt between '''+@FD+''' And '''+@TD+'''     
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )    
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
         ) as t    
         group by mbk_brh'     
             
         print @query;      
           EXECUTE sp_executesql @query;     
               
           
       -- Excess by mrinal    
     
 SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
   SELECT 0,0,0,0,0,0,0,0,0, '  
   if(@IncludeLTA = '0')  
begin  
SET @query = @query + ' (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),stn_shpt_brh))>0 then 0 else   
   (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) end)  as excess, '  
   End  
   Else   
   Begin  
   SET @query = @query + ' (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end)  as excess, '  
   End  
     
  SET @query = @query + '  0,0,0,0,0,0,0,stn_shpt_brh    
  FROM     
  (   
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh, currentPrdData.stn_shpt_brh , '  
    
  if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')    
  Begin  
   if(@IncludeLTA = '0')  
         begin  
         SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(sixMonthWgt.SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.stn_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt* 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.stn_frm   
and oquery.prd_grd = currentPrdData.stn_grd  
and oquery.prd_size = currentPrdData.stn_size and oquery.prd_fnsh = currentPrdData.stn_fnsh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DB + '_intprd_rec_history inq  
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8)   
  and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.stn_shpt_brh  
 where inq.prd_invt_sts = ''S''   
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg '  
         end  
         else  
         begin  
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM   
and prd_grd = currentPrdData.stn_GRD   
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '  
end  
End  
Else   
Begin  
if(@IncludeLTA = '0')  
         begin  
         SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(sixMonthWgt.SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.stn_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.stn_frm   
and oquery.prd_grd = currentPrdData.stn_grd  
and oquery.prd_size = currentPrdData.stn_size and oquery.prd_fnsh = currentPrdData.stn_fnsh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DB + '_intprd_rec_history inq  
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8)   
  and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.stn_shpt_brh  
 where inq.prd_invt_sts = ''S''   
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg '  
         end  
         else  
         begin  
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM   
and prd_grd = currentPrdData.stn_GRD   
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '  
end  
End  
SET @query = @query + '  
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''  
    AND (currentPrdData.stn_shpt_brh = '''+ @Branch +''' or '''+  @Branch +'''= '''')    
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh    
       
) as oquery     
'    
      
  print @query;      
           EXECUTE sp_executesql @query;    
               
               
          -- MultiMetal by mrinal    
     
  SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
      Select 0,0,0,0,0,0,0,0,0,0, sum(wgt) as form ,0,0,0,0,0,0,t.stn_shpt_brh                           
   from (                           
   select stn_shpt_brh , stn_frm as form ,sum(stn_blg_wgt) as wgt                         
    from '+ @DB +'_sahstn_rec                                                
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+''' and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''' )     
   and SUBSTRING(stn_frm,1,1) not in  (''T'')             
   group by stn_shpt_brh, stn_frm              
   ) as t      
   group by t.stn_shpt_brh'    
       
   print @query;      
   EXECUTE sp_executesql @query;    
     
     
 -- NoOfQuot by mrinal    
 SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,COUNT(*), 0,0,0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''QT''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
GROUP BY orh_ord_brh'    
    print @query;      
 EXECUTE sp_executesql @query;    
     
     
  --NoOfOrder by mrinal          
   SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0,COUNT(*),0, 0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''SO''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
GROUP BY orh_ord_brh'    
     print @query;      
 EXECUTE sp_executesql @query;    
     
  -- LostOrder by mrinal    
 SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,COUNT(*),0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''QT''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
AND (select count(*) from '+@DB+'_ortxro_rec where xro_cmpy_id = orh_cmpy_id AND xro_src_pfx = orh_ord_pfx AND xro_src_no = orh_ord_no AND xro_ord_pfx = ''SO'') = 0     
 AND orh_sts_Actn = ''C''    
GROUP BY orh_ord_brh '    
    print @query;      
 EXECUTE sp_executesql @query;    
     
     
   -- Bol by mrinal    
       
 SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,0,COUNT(*),  0,0,whs_mng_brh as Branch    
FROM     
'+ @DB +'_mxtarh_Rec     
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no    
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''    
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs    
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1    
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
AND (whs_mng_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
GROUP BY whs_mng_brh '    
   print @query;      
 EXECUTE sp_executesql @query;    
     
     
  -- OTP By mrinal    
SET @query = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)     
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0, case when SUM(ShipmentReceiptNo) = 0 then 0 else  (SUM(ShipmentReceiptNo)-sum(Late)) /SUM(ShipmentReceiptNo)*100 end as cnt,0, ShpgWhs FROM    
(    
            select  dpf_ord_brh as ''ShpgWhs'',Max(dpf_lte_shpt) as ''Late'', count(dpf_sprc_no) as ''ShipmentReceiptNo''     
       from '+ @DB +'_pfhdpf_rec    
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+@FD+''', 120)  and   
       CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+@TD+''', 120)      
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') and (dpf_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
       group by dpf_ord_brh  , dpf_transp_no,dpf_ord_no , dpf_ord_rls_no       
   Union ALL  
    SELECT ord_ord_brh as ''ShpgWhs'', case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',  
    count(b.tud_sprc_no) as ''ShipmentReceiptNo''    
    FROM ['+ @DB +'_trjtph_rec] a      
    left JOIN ['+ @DB +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no      
    left JOIN ['+ @DB +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm      
     left Join ['+ @DB +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no       
    WHERE a.tph_sch_dtts BETWEEN '''+@FD+''' AND '''+@TD+'''  and ord_ord_brh not in  (''SFS'')    
    and  (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
     AND(      
      (b.tud_prnt_no<>0       
      and TPH_NBR_STP =1       
      and b.tud_sprc_pfx<>''SH''       
      and b.tud_sprc_pfx <>''IT'')        
      OR       
      (b.tud_sprc_pfx=''IT'')      
      )  group by ord_ord_brh , a.tph_sch_dtts , a.tph_transp_no  
 ) as t group by t.ShpgWhs '    
    print @query;      
 EXECUTE sp_executesql @query;    
   
               
        SET @query = 'INSERT INTO #tmp1 (Databases, Branch,ShippedWGT, ShippedValue, BookedValue, GrossProfit, GProfitPercentage, OperExp,OperProfit, ExcessInvLBSSold, MultiMetalLBSSold,TotalAccount ,NewAccount,ReclaimedAccount,  
        LostAccount,DormantAccount3M,DormantAccount6M, NoOfQuotes, NoOfOrders, NoOfLostOrder,PhoneCall ,SalesVisit ,LBS,Pieces,BOL  ,WhsHours, LBSPerHour, PiecesPerHour , BOLPerHour,DeliveryOTP)        
      select   '''+ @DB +''' as Databases, stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as LBShipped,'                
           ELSE                
            SET @query = @query + ' SUM(stn_blg_wgt) as LBShipped,'                
                           
           SET @query = @query + 'SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as Shipments,(select sum(booked) from #NLDAccount Where Branch = stn_shpt_brh) as booked,SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as GP$MTD,    
           (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,    
   SUM(ISNULL(stn_tot_repl_val,0) * '+ @CurrenyRate +') as openExp,0,    
   (select sum(Excess) from #NLDAccount Where Branch = stn_shpt_brh and Excess > 0) as Excess,    
   (select sum(MultiMetal) from #NLDAccount Where Branch = stn_shpt_brh) as MultiMetal,    
   (select sum(TotalActive) from #NLDAccount Where Branch = stn_shpt_brh) as totalActive,    
   (select sum(ActiveAccount) from #NLDAccount Where Branch = stn_shpt_brh) as NewActive,    
   (select sum(ReactAccount) from #NLDAccount Where Branch = stn_shpt_brh) as ReactAccount,    
   (select sum(LostAccount) from #NLDAccount Where Branch = stn_shpt_brh) as LostAccount,    
   (select sum(D3MAccount) from #NLDAccount Where Branch = stn_shpt_brh) as Lost3m,    
   (select sum(D6MAccount) from #NLDAccount Where Branch = stn_shpt_brh) as Lost6m,    
   (select sum(NoOfQuot) from #NLDAccount Where Branch = stn_shpt_brh) as NoOfQuot,    
   (select sum(NoOfOrder) from #NLDAccount Where Branch = stn_shpt_brh) as NoOfOrder,    
   (select sum(LostOrd) from #NLDAccount Where Branch = stn_shpt_brh) as LostOrd,    
   (select sum(PhoneCall) from #NLDAccount Where Branch = stn_shpt_brh) as PhoneCall,    
   (select sum(Visit) from #NLDAccount Where Branch = stn_shpt_brh) as Visit,    
   '''',    
   SUM(stn_blg_pcs),    
   (select sum(Bol) from #NLDAccount Where Branch = stn_shpt_brh) as Bol,   
   (select sum(KPIHour)/(COUNT(*)*5) as oneDayh from tbl_itech_MonthlyKPI where BranchPrefix = stn_shpt_brh and KPIHour != 0 and KPIHour is not null and KPIDate between '''+ @FD +''' and '''+ @TD +'''),  
   0,0,0,     
   (select sum(Otp) from #NLDAccount Where Branch = stn_shpt_brh) as Otp                         
           from '+ @DB +'_sahstn_rec           
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id     
                    
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
           where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''           
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null ) and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
           AND stn_shpt_brh NOT IN(''SFS'' )          
           group by stn_shpt_brh       
            '        
                 
          
            
               
     print(@query)          
        EXECUTE sp_executesql @query;    
          
        truncate table #NLDAccount    ;  
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;        
       END         
    CLOSE ScopeCursor;        
    DEALLOCATE ScopeCursor;        
  END        
  ELSE        
     BEGIN         
       print 'starting' ;        
      IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DB ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
     print 'Ending';        
     print @CurrenyRate ;        
        -- Total Active      
              
          SET @sqltxt = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
              select  count(crd_cus_id),0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH      
              FROM (select  stn_sld_cus_id as CustomerID    
              from '+ @DB +'_sahstn_rec      
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @TD +'''     
              group by stn_sld_cus_id ) as t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id       
              where t.CustomerID = crd_cus_id    
              Group by CUS_ADMIN_BRH'    
               print @sqltxt;      
  set @execSQLtxt = @sqltxt;       
           EXEC (@execSQLtxt);    
               
            SET @sqltxt = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
              select 0,COUNT(STN_SLD_CUS_ID) ,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
    select STN_SLD_CUS_ID, CUS_ADMIN_BRH      
               from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID       
               left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat       
               left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= STN_SLD_CUS_ID and CRD_CMPY_ID= STN_CMPY_ID      
               left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id       
               where dbo.fun_itech_GetFirstSalesDate(STN_SLD_CUS_ID,coc_frst_sls_dt) Between  ''' + @FD + '''  And ''' + @TD + '''    
               AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
               Group by  STN_SLD_CUS_ID,CUS_ADMIN_BRH ) as outerq group by CUS_ADMIN_BRH     
               '    
               print @sqltxt;      
  set @execSQLtxt = @sqltxt;       
           EXEC (@execSQLtxt);    
               
           SET @sqltxt = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
                  
              select 0,0,COUNT(CUS_CUS_ID),0, 0,0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
     SELECT  CUS_CUS_ID,  CUS_ADMIN_BRH    
           FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id      
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between   ''' + @D3MFD + '''  And ''' + @D3MTD + '''     
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
           group by CUS_CUS_ID,  CUS_ADMIN_BRH  ) as outerq group by CUS_ADMIN_BRH     
   '    
               print @sqltxt;      
  set @execSQLtxt = @sqltxt;       
           EXEC (@execSQLtxt);    
               
           --mrinal D6MAccount    
               
            SET @sqltxt = ' INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)      
                  
              select 0,0,0,COUNT(CUS_CUS_ID), 0,0,0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from(           
     SELECT  CUS_CUS_ID,  CUS_ADMIN_BRH    
           FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
           left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat      
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id      
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between   ''' + @D6MFD + '''  And ''' + @D6MTD + '''     
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
           group by CUS_CUS_ID,  CUS_ADMIN_BRH  ) as outerq group by CUS_ADMIN_BRH     
               '    
               print @sqltxt;      
  set @execSQLtxt = @sqltxt;       
           EXEC (@execSQLtxt);    
               
             -- Lost added by mrinal.    
          SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
           SELECT 0,0,0,0,COUNT(t.STN_SLD_CUS_ID), 0,0,0,0,0,0,0,0,0,0,0,0,Branch FROM                
(                
SELECT   CUS_ADMIN_BRH as Branch , CUS_CUS_ID as STN_SLD_CUS_ID                  
    FROM '+ @DB +'_sahstn_rec      
           INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID      
               
           left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id     
           where dbo.fun_itech_GetLastSalesDate(STN_SLD_CUS_ID,coc_lst_sls_dt) Between  '''+@LFD+''' and '''+@LTD+'''       
           AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
           group by CUS_ADMIN_BRH,CUS_CUS_ID     
) as t GROUP BY Branch'    
print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
     
      
           -- React by mrinal    
            SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
           Select 0,0,0,0,0,COUNT(crd_cus_id), 0,0,0,0,0,0,0,0,0,0,0,CUS_ADMIN_BRH from (                  
 SELECT  crd_cus_id, CUS_ADMIN_BRH    
              FROM (    
              select  stn_sld_cus_id as CustomerID from '+ @DB +'_sahstn_rec      
              where STN_INV_DT >= '''+@RFD+''' and STN_INV_DT <= '''+@RTD+'''      
               group by stn_sld_cus_id ) t,    
                '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id       
              where       
              (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')      
               and  t.CustomerID Not IN       
              ( select distinct t1.CustomerID  from (select  stn_sld_cus_id as CustomerID from '+ @DB +'_sahstn_rec      
              where STN_INV_DT >= '''+@RFD+''' and STN_INV_DT <= '''+@RTD+'''      
               group by stn_sld_cus_id ) t1,      
              '+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID       
              and u.STN_INV_DT > '''+@AFD+''' AND u.STN_INV_DT < '''+@RFD+''')      
              and t.CustomerID not in  (select coc_cus_id from '+ @DB +'_arbcoc_rec       
              where dbo.fun_itech_GetFirstSalesDate(coc_cus_id,coc_frst_sls_dt) Between  '''+@RFD+''' And '''+@RTD+'''     
                  
              )      
              and  t.CustomerID = crd_cus_id ) as outerqery group by CUS_ADMIN_BRH'    
                  
          print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
     
    -- phone call by mrinal    
            SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
             Select 0,0,0,0,0,0,SUM( t.TotalPhoneCall) as TotalPhoneCall, 0,0,0,0,0,0,0,0,0,0,cus_admin_brh  from (          
                      
   select  cus_cus_id as CustomerId, COUNT(*) as TotalPhoneCall  , cus_admin_brh                       
       from '+ @DB +'_cctcta_rec join                         
       '+ @DB +'_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join                        
       '+ @DB +'_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id                                                      
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                                    
       and rtrim(atp_desc30) in (''Phone Call - Lost/Dormant'',''Phone Call - Existing Account'',''Phone Call - Prospect Account'')   
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, cus_cus_id    
       ) as t    
       GROUP  BY cus_admin_brh'    
                  
         print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
  -- visit by mrinal    
            SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
             Select 0,0,0,0,0,0,0,SUM( t.TotalPhoneCall) as TotalPhoneCall, 0,0,0,0,0,0,0,0,0,cus_admin_brh  from (          
                      
   select  cus_cus_id as CustomerId, COUNT(*) as TotalPhoneCall  , cus_admin_brh                       
       from '+ @DB +'_cctcta_rec join                         
       '+ @DB +'_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join                        
       '+ @DB +'_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id                                                      
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                                    
       and rtrim(atp_desc30) in (''Visit - Prospect Account'',''Visit - Existing Account'',''Visit - Lost/Dormant Account'')   
          
       AND (cus_admin_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
       group by cus_admin_brh, cus_cus_id    
       ) as t    
       GROUP  BY cus_admin_brh'    
                  
         print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
               
         -- booked val by mrinal     
        SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)     
         Select 0,0,0,0,0,0,0,0,SUM( t.mbk_tot_val) as mbk_tot_val, 0,0,0,0,0,0,0,0,mbk_brh  from (    
SELECT a.mbk_brh, (a.mbk_tot_val* '+ @CurrenyRate +') as mbk_tot_val    
      FROM '+ @DB +'_ortmbk_rec a   
      join '+ @DB +'_ortorh_rec on orh_ord_pfx = a.mbk_ord_pfx and orh_ord_no = a.mbk_ord_no      
      INNER JOIN '+ @DB +'_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id      
      left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat     
      WHERE a.mbk_ord_pfx=''SO''       
        AND a.mbk_ord_itm<>999       
         and a.mbk_trs_md = ''A''   
         and orh_sts_actn <> ''C''       
         AND a.mbk_actvy_dt between '''+@FD+''' And '''+@TD+'''    
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )    
         AND (a.mbk_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
         ) as t    
         group by mbk_brh'     
             
          print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);      
     
 -- Excess by mrinal    
      
 SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
   SELECT 0,0,0,0,0,0,0,0,0, '  
   if(@IncludeLTA = '0')  
begin  
SET @sqltxt = @sqltxt + ' (case when (select [dbo].[IsLTACustomer](RTRIM(LTrim(CustID)),stn_shpt_brh))>0 then 0 else   
   (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end) end)  as excess, '  
   End  
   Else   
   Begin  
   SET @sqltxt = @sqltxt + ' (case when periodSelRange < (prd_ohd_wg - InvoiceMonthTotal) then periodSElRange else (prd_ohd_wg - InvoiceMonthTotal) end)  as excess, '  
   End  
     
  SET @sqltxt = @sqltxt + '  0,0,0,0,0,0,0,stn_shpt_brh    
  FROM     
  (   
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh, currentPrdData.stn_shpt_brh , '  
    
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')    
  Begin  
   if(@IncludeLTA = '0')  
         begin  
         SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(sixMonthWgt.SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.stn_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt* 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.stn_frm   
and oquery.prd_grd = currentPrdData.stn_grd  
and oquery.prd_size = currentPrdData.stn_size and oquery.prd_fnsh = currentPrdData.stn_fnsh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DB + '_intprd_rec_history inq  
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8)   
  and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.stn_shpt_brh  
 where inq.prd_invt_sts = ''S''   
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg '  
         end  
         else  
         begin  
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM   
and prd_grd = currentPrdData.stn_GRD   
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '  
end  
End  
Else   
Begin  
if(@IncludeLTA = '0')  
         begin  
         SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(sixMonthWgt.SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + '''   
and sixMonthWgt.sat_sld_cus_id not in (select customer_id from US_LTA_Customer where branch = currentPrdData.stn_shpt_brh)) as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(oquery.prd_ohd_wgt,0)),0) FROM ' + @DB + '_intprd_rec_history oquery where oquery.prd_frm = currentPrdData.stn_frm   
and oquery.prd_grd = currentPrdData.stn_grd  
and oquery.prd_size = currentPrdData.stn_size and oquery.prd_fnsh = currentPrdData.stn_fnsh and oquery.prd_invt_sts = ''S''   
and oquery.UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  
and oquery.prd_ord_ffm not in (  
select inq.prd_ord_ffm  
FROM ' + @DB + '_intprd_rec_history inq  
  join ' + @DB + '_ortorh_rec on orh_ord_no = SUBSTRING(SUBSTRING(inq.prd_ord_ffm,3,8), PATINDEX(''%[^0]%'', SUBSTRING(inq.prd_ord_ffm,3,8)), 8)   
  and orh_ord_pfx = ''SO''  
  join US_LTA_Customer on orh_sld_cus_id = customer_id and branch = currentPrdData.stn_shpt_brh  
 where inq.prd_invt_sts = ''S''   
and inq.UpdateDtTm between  ''' + @FD2 + '''  And ''' + @TD2 + ''' and SUBSTRING(inq.prd_ord_ffm,3,13) != ''0000000000000'')  ) as prd_ohd_wg '  
         end  
         else  
         begin  
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,  
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where   
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size  
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and   
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,  
  
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM   
and prd_grd = currentPrdData.stn_GRD   
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''   
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '  
end  
End  
SET @sqltxt = @sqltxt + '  
FROM '+ @DB +'_sahstn_rec currentPrdData    
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''  
    AND (currentPrdData.stn_shpt_brh = '''+ @Branch +''' or '''+  @Branch +'''= '''')    
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh    
       
) as oquery     
'   
        print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);     
    
/*    
 INSERT INTO #NLDAccount(Branch,Excess)    
 exec sp_itech_Excess6MonthInventory @DB,@TD,@Branch */    
     
  -- MultiMetal by mrinal    
     
 SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
      Select 0,0,0,0,0,0,0,0,0,0, sum(wgt) as form ,0,0,0,0,0,0,t.stn_shpt_brh                           
   from (                           
   select stn_shpt_brh , stn_frm as form ,sum(stn_blg_wgt) as wgt                         
    from '+ @DB +'_sahstn_rec                                                
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+''' and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''' )      
   and SUBSTRING(stn_frm,1,1) not in  (''T'')          
   group by stn_shpt_brh, stn_frm              
   ) as t      
   group by t.stn_shpt_brh'    
    print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);     
     
     
 -- NoOfQuot by mrinal    
 SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,COUNT(*), 0,0,0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''QT''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
GROUP BY orh_ord_brh'    
 print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
             
             
  --NoOfOrder by mrinal          
   SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0,COUNT(*), 0,0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''SO''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
GROUP BY orh_ord_brh'    
 print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
 -- LostOrder by mrinal    
 SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,COUNT(*),0,0,0,orh_ord_brh FROM '+ @DB +'_ortorh_rec    
INNER JOIN '+ @DB +'_ORTXRE_REC ON orh_cmpy_id = xre_cmpy_id     
AND orh_ord_pfx = xre_ord_pfx    
AND orh_ord_no = xre_ord_no    
WHERE orh_ord_pfx = ''QT''     
AND (orh_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
AND xre_crtd_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
AND (select count(*) from '+ @DB +'_ortxro_rec where xro_cmpy_id = orh_cmpy_id AND xro_src_pfx = orh_ord_pfx AND xro_src_no = orh_ord_no AND xro_ord_pfx = ''SO'') = 0     
 AND orh_sts_Actn = ''C''    
GROUP BY orh_ord_brh '    
   print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
  -- Bol  by mrinal    
SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)    
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,0,COUNT(*),  0,0,whs_mng_brh as Branch    
FROM     
'+ @DB +'_mxtarh_Rec     
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no    
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''    
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs    
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1    
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''    
AND (whs_mng_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
GROUP BY whs_mng_brh '    
   print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
     
 -- OTP By mrinal    
SET @sqltxt = 'INSERT INTO #NLDAccount(TotalActive,ActiveAccount,D3MAccount,D6MAccount,LostAccount,ReactAccount,PhoneCall,Visit,booked,Excess,MultiMetal,NoOfQuot,NoOfOrder,LostOrd,Bol,Otp,OperProfit,Branch)     
 SELECT  0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0, case when SUM(ShipmentReceiptNo) = 0 then 0 else  (SUM(ShipmentReceiptNo)-sum(Late)) /SUM(ShipmentReceiptNo)*100 end as cnt,0, ShpgWhs FROM    
(    
            select  dpf_ord_brh as ''ShpgWhs'',Max(dpf_lte_shpt) as ''Late'', count(dpf_sprc_no) as ''ShipmentReceiptNo''     
       from '+ @DB +'_pfhdpf_rec      
       where  CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) >=CONVERT(VARCHAR(10), '''+@FD+''', 120)  and   
       CONVERT(VARCHAR(10), dpf_shpg_dtts , 120) <= CONVERT(VARCHAR(10), '''+@TD+''', 120)      
       and dpf_sprc_pfx = ''SH'' and dpf_ord_brh not in (''SFS'') and (dpf_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
       group by dpf_ord_brh  , dpf_transp_no,dpf_ord_no , dpf_ord_rls_no       
   Union ALL  
    SELECT ord_ord_brh as ''ShpgWhs'', case when  (a.tph_sch_dtts > Max(c.orl_due_to_dt)) then 1 else 0 end as ''Late'',  
    count(b.tud_sprc_no) as ''ShipmentReceiptNo''    
    FROM ['+ @DB +'_trjtph_rec] a      
    left JOIN ['+ @DB +'_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no      
    left JOIN ['+ @DB +'_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm      
     left Join ['+ @DB +'_ortord_rec] on orl_ord_pfx= ord_ord_pfx and orl_ord_itm = ord_ord_itm  and orl_cmpy_id= ord_cmpy_id and orl_ord_no = ord_ord_no       
    WHERE a.tph_sch_dtts BETWEEN '''+@FD+''' AND '''+@TD+'''  and ord_ord_brh not in  (''SFS'')    
    and  (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
     AND(      
      (b.tud_prnt_no<>0       
      and TPH_NBR_STP =1       
      and b.tud_sprc_pfx<>''SH''       
      and b.tud_sprc_pfx <>''IT'')        
      OR       
      (b.tud_sprc_pfx=''IT'')      
      )  group by ord_ord_brh , a.tph_sch_dtts , a.tph_transp_no  
 ) as t group by t.ShpgWhs '    
      print @sqltxt;      
set @execSQLtxt = @sqltxt;     
 EXEC (@execSQLtxt);    
   
              
     SET @sqltxt = 'INSERT INTO #tmp1 (Databases, Branch,ShippedWGT, ShippedValue, BookedValue, GrossProfit, GProfitPercentage, OperExp,OperProfit, ExcessInvLBSSold, MultiMetalLBSSold,TotalAccount ,NewAccount,ReclaimedAccount,LostAccount,DormantAccount3M
,  
DormantAccount6M, NoOfQuotes, NoOfOrders, NoOfLostOrder,PhoneCall ,SalesVisit ,LBS,Pieces,BOL  ,WhsHours, LBSPerHour, PiecesPerHour , BOLPerHour,DeliveryOTP)        
       select   '''+ @DB +''' as Databases, stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                   
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as LBShipped,'                
           ELSE                
            SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as LBShipped,'                
                           
           SET @sqltxt = @sqltxt + 'SUM(ISNULL(stn_tot_val,0) * '+ @CurrenyRate +') as Shipments,(select sum(booked) from #NLDAccount Where Branch = stn_shpt_brh) as booked,SUM(ISNULL(stn_npft_avg_val,0) * '+ @CurrenyRate +') as GP$MTD,    
           (Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct,    
   SUM(ISNULL(stn_tot_repl_val,0) * '+ @CurrenyRate +') as openExp,0,    
   (select sum(Excess) from #NLDAccount Where Branch = stn_shpt_brh and Excess > 0) as Excess,    
   (select sum(MultiMetal) from #NLDAccount Where Branch = stn_shpt_brh) as MultiMetal,    
   (select sum(TotalActive) from #NLDAccount Where Branch = stn_shpt_brh) as totalActive,    
   (select sum(ActiveAccount) from #NLDAccount Where Branch = stn_shpt_brh) as NewActive,    
   (select sum(ReactAccount) from #NLDAccount Where Branch = stn_shpt_brh) as ReactAccount,    
   (select sum(LostAccount) from #NLDAccount Where Branch = stn_shpt_brh) as LostAccount,    
   (select sum(D3MAccount) from #NLDAccount Where Branch = stn_shpt_brh) as Lost3m,    
   (select sum(D6MAccount) from #NLDAccount Where Branch = stn_shpt_brh) as Lost6m,    
   (select sum(NoOfQuot) from #NLDAccount Where Branch = stn_shpt_brh) as NoOfQuot,    
   (select sum(NoOfOrder) from #NLDAccount Where Branch = stn_shpt_brh) as NoOfOrder,    
   (select sum(LostOrd) from #NLDAccount Where Branch = stn_shpt_brh) as LostOrd,    
   (select sum(PhoneCall) from #NLDAccount Where Branch = stn_shpt_brh) as PhoneCall,    
   (select sum(Visit) from #NLDAccount Where Branch = stn_shpt_brh) as Visit,    
   '''',    
   SUM(stn_blg_pcs),    
   (select sum(Bol) from #NLDAccount Where Branch = stn_shpt_brh) as Bol,    
   (select sum(KPIHour)/(COUNT(*)*5) as oneDayh from tbl_itech_MonthlyKPI where BranchPrefix = stn_shpt_brh and KPIHour != 0 and KPIHour is not null and KPIDate between '''+ @FD +''' and '''+ @TD +'''),  
   0,0,0,    
   (select sum(Otp) from #NLDAccount Where Branch = stn_shpt_brh) as Otp                         
           from '+ @DB +'_sahstn_rec           
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id     
                    
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                
           where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +'''           
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null ) and (stn_shpt_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')       
           AND stn_shpt_brh NOT IN(''SFS'' )    
           group by stn_shpt_brh '        
                  
       END         
            
     
             
  print(@sqltxt)        
  set @execSQLtxt = @sqltxt;         
  EXEC (@execSQLtxt);        
             
      
select Databases, Branch,ShippedWGT, ShippedValue, BookedValue, GrossProfit, GProfitPercentage, OperExp,OperProfit, ExcessInvLBSSold,   
MultiMetalLBSSold,ShippedWGT - MultiMetalLBSSold as TitaniumLBSSold,TotalAccount ,NewAccount,ReclaimedAccount,LostAccount,DormantAccount3M,  
DormantAccount6M, NoOfQuotes, NoOfOrders, NoOfLostOrder,PhoneCall ,SalesVisit ,LBS,Pieces,BOL  ,WhsHours,   
 (case When WhsHours > 0 then  ShippedWGT/WhsHours else 0 end) as LBSPerHour,  
 (case When WhsHours > 0 then  Pieces/WhsHours else 0 end) as PiecesPerHour ,   
 (case When WhsHours > 0 then  BOL/WhsHours else 0 end) as BOLPerHour,  
  DeliveryOTP  
 into #tmpresult1  
      from   #tmp1    
 where ([databaseS]!='US' or BRANCH!='TAI') ;   
    
    
  -- To get records for Market such as aerospace , Industrial, medical, Oil and gas  
    CREATE TABLE #tmpresult2 (    Databases   VARCHAR(15)        
        , Branch   VARCHAR(3)         
        ,AerospaceSales$ DECIMAL(20, 0)   
  ,AerospaceLBSSold DECIMAL(20, 0)  
  ,AerospaceNetProfit$ Decimal(20,0)   
  ,AerospaceNetProfitMargin  DECIMAL(20, 1)   
  ,IndustrialSales$ DECIMAL(20, 0)   
  ,IndustrialLBSSold DECIMAL(20, 0)  
  ,IndustrialNetProfit$ Decimal(20,0)   
  ,IndustrialNetProfitMargin  DECIMAL(20, 1)   
  ,MedicalSales$ DECIMAL(20, 0)   
  ,MedicalLBSSold DECIMAL(20, 0)  
  ,MedicalNetProfit$ Decimal(20,0)   
  ,MedicalNetProfitMargin  DECIMAL(20, 1)   
  ,OilandGasSales$ DECIMAL(20, 0)   
  ,OilandGasLBSSold DECIMAL(20, 0)  
  ,OilandGasNetProfit$ Decimal(20,0)   
  ,OilandGasNetProfitMargin  DECIMAL(20, 1)    
  ,ComptitorandMillSales$ DECIMAL(20, 0)   
  ,ComptitorandMillLBSSold DECIMAL(20, 0)  
  ,ComptitorandMillNetProfit$ Decimal(20,0)   
  ,ComptitorandMillProfitMargin  DECIMAL(20, 1)  
  );    
    
  insert into #tmpresult2 exec [sp_itech_MonthlyKPI_Market_SFS] @DBNAME,@Branch,@FromDate,@ToDate,@version;  
   
-- Select Table  
select t1.* ,t2.AerospaceSales$,t2.AerospaceLBSSold,t2.AerospaceNetProfit$,t2.AerospaceNetProfitMargin,t2.IndustrialSales$,  
t2.IndustrialLBSSold,t2.IndustrialNetProfit$,  
t2.IndustrialNetProfitMargin,t2.MedicalSales$,t2.MedicalLBSSold,t2.MedicalNetProfit$,t2.MedicalNetProfitMargin,t2.OilandGasSales$  
,t2.OilandGasLBSSold,t2.OilandGasNetProfit$,t2.OilandGasNetProfitMargin, t2.ComptitorandMillSales$,t2.ComptitorandMillLBSSold  
,t2.ComptitorandMillNetProfit$,t2.ComptitorandMillProfitMargin, @FD as fromDate from #tmpresult1 t1 join #tmpresult2 t2   
on t1.Databases = t2.Databases and t1.Branch = t2.Branch   
    
    
 -- where  (Branch not in ('TAI') or [Databases] = 'US') ;    
    
 -- select * from #NLDAccount;        
   drop table #tmp1    ;    
   drop table #NLDAccount;    
   drop table #tmpresult1;    
   drop table #tmpresult2;    
END        
        
-- exec [sp_itech_MonthlyOps_KPI_SFS] 'ALL','ALL','2016-05-01', '2016-05-31'     
-- exec [sp_itech_MonthlyOps_KPI_SFS] 'US','ALL', '2016-05-01', '2016-05-31','0','1'    
/*
2020/07/10
Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account
*/
GO
