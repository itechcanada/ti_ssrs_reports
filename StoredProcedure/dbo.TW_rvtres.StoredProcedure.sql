USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_rvtres]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,11/3/2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[TW_rvtres]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 IF OBJECT_ID('dbo.TW_rvtres_rec', 'U') IS NOT NULL
		drop table dbo.TW_rvtres_rec;
    
        
SELECT *
into  dbo.TW_rvtres_rec
FROM [LIVETWSTX].[livetwstxdb].[informix].[rvtres_rec];
	
	

END

-- Select * from TW_rvtres_rec













GO
