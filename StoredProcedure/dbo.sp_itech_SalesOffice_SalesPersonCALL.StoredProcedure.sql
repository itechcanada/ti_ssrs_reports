USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesOffice_SalesPersonCALL]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <27 Nov 2017>            
-- Modified by: <Mukesh>           
          
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_SalesOffice_SalesPersonCALL] @DBNAME varchar(50),@Branch varchar(50)  
            
AS            
BEGIN            
             
 SET NOCOUNT ON;            
declare @sqltxtAct varchar(6000)            
declare @execSQLtxtAct varchar(7000)            
declare @sqltxtTsk varchar(6000)            
declare @execSQLtxtTsk varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
            
set @DB=  @DBNAME            
  
IF @Branch = 'ALL'            
 BEGIN            
  set @Branch = ''            
 END              
             
             
--set @FD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0),120)   -- First date of Previou Month                   
  --set @TD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1),120) -- Last date of previous Month         
   set @FD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0),120)   -- First date of current Month                   
  set @TD = Convert(Varchar(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1),120) -- Last date of current Month    
            
--CREATE TABLE #tmpAct ([Database]   VARCHAR(10)  
--  ,Branch Varchar(10)          
--     , LoginID   VARCHAR(50)            
          
--    -- , MonthYear   VARCHAR(10)            
--        , ActDormant  integer            
--        , ActExisting  integer            
--        , ActProspect  integer            
--        , TskDormant  integer            
--        , TskExisting  integer            
--        , TskProspect  integer            
--        , ActCallDormant  integer            
--        , ActCallExisting  integer            
--        , ActCallProspect  integer            
--        --, TskCallDormant  integer            
--        --, TskCallExisting  integer            
--        --, TskCallProspect  integer            
--                 );             
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)    
     ,Branch Varchar(10)                 
     , LoginID   VARCHAR(50)       
      , SalesPersonInitial   VARCHAR(50)            
    -- , MonthYear   VARCHAR(10)            
        --, ActDormant  integer            
        --, ActExisting  integer            
        --, ActProspect  integer            
        --, TskDormant  integer            
        --, TskExisting  integer            
        --, TskProspect  integer           
        --, ActCallDormant  integer            
        --, ActCallExisting  integer            
        --, ActCallProspect  integer            
        , TskCallDormant  integer            
        , TskCallExisting  integer            
        , TskCallProspect  integer            
                 );             
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(35);            
DECLARE @Name VARCHAR(15);            
            
IF @DBNAME = 'ALL'            
 BEGIN            
                
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
   OPEN ScopeCursor;              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @queryAct NVARCHAR(1500);               
        DECLARE @queryTsk NVARCHAR(1500);               
      SET @DB= @Prefix              
                    
        SET @queryTsk =            
              'INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting, TskCallProspect)          
      Select * from (           
      select ''' +  @Prefix + ''' as [Database], cus_admin_brh as branch , usr_nm ,  rtrim(atp_desc30) As att_desc30, COUNT(*) as counts   
       from ' + @DB + '_cctcta_rec join           
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join          
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id       
       join ' + @DB + '_mxrusr_rec on usr_lgn_id = cta_crtd_lgn_id  
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))             
       And (cus_admin_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')            
       group by  usr_nm, cus_admin_brh ,  atp_desc30  
       ) AS Query1            
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account])) As po'            
        EXECUTE sp_executesql @queryTsk;            
        print(@queryTsk);            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN                
 -- task              
                
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting, TskCallProspect)          
      Select * from (           
      select ''' +  @DB + ''' as [Database], cus_admin_brh as branch,  usr_nm, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts           
       from ' + @DB + '_cctcta_rec join           
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join          
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id      
        join ' + @DB + '_mxrusr_rec on usr_lgn_id = cta_crtd_lgn_id  
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))             
       And (cus_admin_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')       
       group by  usr_nm,cus_admin_brh, atp_desc30        
       ) AS Query1            
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account])) As po'                 
                
    print(@sqltxtTsk)            
    set @execSQLtxtTsk = @sqltxtTsk;             
   EXEC (@execSQLtxtTsk);            
     END            
   -- SELECT * FROM #tmpAct Order by MonthYear desc, LoginID            
   -- SELECT * FROM #tmpTsk Order by MonthYear desc, LoginID            
   SELECT   [Database],  Branch,       
    tsk.LoginID  AS LoginID,        
    SUM(ISNULL(tsk.TskCallDormant,0)) AS TskCallDormant,             
    Sum(ISNULL(tsk.TskCallExisting,0)) AS TskCallExisting,            
    Sum(ISNULL(tsk.TskCallProspect,0)) AS TskCallProspect,  
    SUM(ISNULL(tsk.TskCallDormant,0)) + Sum(ISNULL(tsk.TskCallExisting,0)) + Sum(ISNULL(tsk.TskCallProspect,0)) as total          
    --Sum(ISNULL(tsk.TskDormant,0)) AS TskDormant,             
    --Sum(ISNULL(tsk.TskExisting,0)) AS TskExisting,            
    --Sum(ISNULL(tsk.TskProspect,0)) AS TskProspect             
    FROM #tmpTsk as tsk group by [Database],  Branch,  LoginID          
    order by  LoginID            
                
    --Drop table #tmpAct   
    Drop table #tmpTsk            
END            
            
-- exec sp_itech_SalesOffice_SalesPersonCALL  'US','LAX'             
-- exec sp_itech_SalesOffice_SalesPersonCALL 'ALL', 'ALL'
/*
2020/07/13	Sumit
Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account
*/
GO
