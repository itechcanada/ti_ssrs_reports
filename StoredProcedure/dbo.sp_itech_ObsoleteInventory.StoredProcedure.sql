USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ObsoleteInventory]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
---Pierce Database added
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_ObsoleteInventory] @DBNAME varchar(50), @version char = '0'

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)
declare @FD12 varchar(10)

set @DB=  @DBNAME
set @FD = CONVERT(VARCHAR(10), DateAdd(mm, -6, GetDate()) , 120)
set @FD12 = CONVERT(VARCHAR(10), DATEADD(month, -12 ,GetDate()) ,120)  
set @TD = CONVERT(VARCHAR(10), GetDate() , 120)
print @FD12;
print @FD;

CREATE TABLE #tmp (   [Database]		 VARCHAR(10)
   					, Form				 Varchar(65)
   					, Grade				 Varchar(65)
   					, Size				 Varchar(65)
   					, Finish			 Varchar(65)
   					, OnHandWeightLBS	 DECIMAL(20, 2)
                    , IncomingLBS  		 DECIMAL(20, 2)
   					, ReservedWeightLBS   DECIMAL(20, 2)
   					, InvoicedWeightLast6MonthsLBS			 DECIMAL(20, 2)
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(1500);  	
  				SET @DB= @Prefix  
  				SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,OnHandWeightLBS,ReservedWeightLBS,InvoicedWeightLast6MonthsLBS,IncomingLBS)
  							select prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH, SUM(prd_ohd_wgt) as StockLbs, SUM(PRD_ORD_RES_WGT) as StockReserved, 
  							(Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD aND PRD_SIZE = SAT_SIZE and  PRD_FNSH = SAT_FNSH
							and SAT_INV_DT >'''+ @FD +''' ) as SalesHist6M,
							(select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec
							on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''
							where IPD_CMPY_ID = prd_cmpy_id and ipd_FRM=PRD_FRM 
							 and ipd_GRD = PRD_GRD and ipd_SIZE = PRD_SIZE and ipd_fnsh = PRD_FNSH and pod_ven_id not in 
							 (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as IncomingLbs
  							from ' + @DB + '_intprd_rec 
  							where (PRD_INVT_STS = ''S''  OR PRD_INVT_STS = ''N'' ) and
  							(Select ISNULL(SUM(SAT_BLG_WGT),0) from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD  and PRD_SIZE = sat_size and 
  							PRD_FNSH = sat_fnsh and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''') = 0
							group by  prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH '
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			  SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,OnHandWeightLBS,ReservedWeightLBS,InvoicedWeightLast6MonthsLBS,IncomingLBS)
  							select prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH, SUM(prd_ohd_wgt) as StockLbs, SUM(PRD_ORD_RES_WGT) as StockReserved, 
  							(Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD aND PRD_SIZE = SAT_SIZE and  PRD_FNSH = SAT_FNSH
							and SAT_INV_DT >'''+ @FD +''' ) as SalesHist6M,
							(select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec
							on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''
							where IPD_CMPY_ID = prd_cmpy_id and ipd_FRM=PRD_FRM 
							 and ipd_GRD = PRD_GRD and ipd_SIZE = PRD_SIZE and ipd_fnsh = PRD_FNSH and pod_ven_id not in 
							 (Select ixv_ven_id from ' + @DB + '_xcrixv_rec where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as IncomingLbs
  							from ' + @DB + '_intprd_rec 
  							where (PRD_INVT_STS = ''S''  OR PRD_INVT_STS = ''N'' ) and
  							(Select ISNULL(SUM(SAT_BLG_WGT),0) from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD  and PRD_SIZE = sat_size and 
  							PRD_FNSH = sat_fnsh and SAT_INV_DT  between ''' + @FD12 + ''' and ''' + @TD + ''') = 0
							group by  prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH '
					print(@sqltxt)	
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
     
     if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')
        Begin			
					SELECT [Database],Form,Grade,Size,Finish,
					(OnHandWeightLBS * 2.20462) as OnHandWeightLBS ,(ReservedWeightLBS * 2.20462) as ReservedWeightLBS ,
					(InvoicedWeightLast6MonthsLBS * 2.20462) as InvoicedWeightLast6MonthsLBS,(IncomingLBS * 2.20462) as IncomingLBS 
					FROM #tmp order by Form, Grade, Size, Finish
	    End
	 ELSE
	    Begin
	    SELECT * FROM #tmp order by Form, Grade, Size, Finish
        End
   
   drop table #tmp
END

-- exec sp_itech_ObsoleteInventory 'TW'




GO
