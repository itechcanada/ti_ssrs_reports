USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_potpoh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: Feb 05, 2016    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[CA_potpoh]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.CA_potpoh_rec', 'U') IS NOT NULL    
  drop table dbo.CA_potpoh_rec;    
        
            
SELECT *    
into  dbo.CA_potpoh_rec    
FROM [LIVECASTX].[livecastxdb].[informix].[potpoh_rec];    
    
END    
--  exec  CA_potpoh    
-- select * from CA_potpoh_rec
GO
