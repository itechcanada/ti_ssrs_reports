USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ortbka]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[US_ortbka] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_ortbka_rec', 'U') IS NOT NULL
		drop table dbo.US_ortbka_rec;
    
        
SELECT *
into  dbo.US_ortbka_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortbka_rec];

END
--- exec US_ortbka
-- select * from US_ortbka_rec
GO
