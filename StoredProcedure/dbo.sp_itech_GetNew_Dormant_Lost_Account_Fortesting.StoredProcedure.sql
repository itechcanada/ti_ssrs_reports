USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Fortesting]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetNew_Dormant_Lost_Account_Fortesting] @AMonth Datetime, @DBNAME varchar(50),@Branch varchar(50),@ISCombineData int,@Account varchar(50),@Market varchar(50)

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @NFD varchar(10)
declare @NTD varchar(10)
declare @D3MFD varchar(10)
declare @D3MTD varchar(10)
declare @D6MFD varchar(10)
declare @D6MTD varchar(10)
declare @LFD varchar(10)
declare @LTD varchar(10)
declare @FD varchar(10)
declare @TD varchar(10)
declare @RFD varchar(10)
declare @RTD varchar(10)
declare @AFD varchar(10)
declare @ATD varchar(10)


set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @AMonth) - 12, 0) , 120)   --First day of previous 12 month
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth)+1,0)), 120)  --Last Day of current month

set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-1,0)) , 120)   -- New Account
set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth),0)), 120)  -- New Account

set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-4,0)) , 120)   -- Dormant 3 Month
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-3,0)), 120)  -- Dormant 3 Month

set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-7,0)) , 120)   --Dormant 6 Month
set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-6,0)), 120)  -- Dormant 6 Month

set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-19,0)) , 120)   -- Lost Account
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-18,0)), 120)  -- Lost Account


set @RFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-19,0)) , 120)   -- Reactivated Account
set @RTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-19,0)), 120)  -- Reactivated Account

set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@AMonth)-19,0)) , 120)    -- Active Accounts 
set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@AMonth),0)), 120)

 -- IF @ISCombineData =1
	--BEGIN
	--		set @NFD =@FD 
	--		set @NTD = @TD
	--		set @D3MFD = @FD
	--		set @D3MTD = @TD
	--		set @D6MFD = @FD
	--		set @D6MTD = @TD
	--		set @LFD = @FD
	--		set @LTD = @TD
 --  END
 
--1	New Accounts
--2	Dormant Accounts 3M
--3	Dormant Accounts 6M
--4	Lost Accounts
--5	Total Reactivated Accounts
--6	Current Month Total Active Customers
--0	ALL

CREATE TABLE #tmp (      AccountType varchar(50)
                        ,CustomerID  VARCHAR(15)
						,AccountName VARCHAR(150)
						,AccountDate varchar(15)
						,FirstSaleDate Varchar(15)
						,Branch  VARCHAR(15)
						,Category  VARCHAR(100)
						,SalePersonName  VARCHAR(50)
						,DatabaseName Varchar(3)
   					);
   					   					
DECLARE @company VARCHAR(15); 
DECLARE @prefix VARCHAR(15); 
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @CurrenyRate varchar(15);
DECLARE @Category varchar(50);

set @DB= @DBNAME
  	   
IF @Branch = 'ALL'
 BEGIN
	 set @Branch = ''
 END
 
 IF @Market = 'ALL'  
 	 set @Market = ''
 ELSE IF @Market ='Unknown'
	 Begin
		 set @Category='Unknown'
		 set @Market = ''
	 END
 
  	   
 IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName, company,prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(4000);  	
  				 set @DB= @prefix    
							SET @query = '  INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName)
					                        SELECT ''1'', crd_cus_id, CUS_CUS_LONG_NM, crd_acct_opn_dt,COC_LST_SLS_DT, CUS_ADMIN_BRH,cuc_desc30 as category,
											Min(STN_OS_SLP) as SalePersonName,'''+ @DB +'''
											FROM '+ @DB +'_arrcrd_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON CRD_CUS_ID = CUS_CUS_ID
											LEFT JOIN '+ @DB +'_arbcoc_rec ON CRD_CUS_ID = COC_CUS_ID
											LEFT Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											Left JOIN 	'+ @DB +'_sahstn_rec c ON  STN_SLD_CUS_ID= coc_cus_id
											WHERE CUS_CUS_ACCT_TYP=''R''
											And crd_acct_opn_dt Between '''+ @NFD +''' And '''+ @NTD +'''
											and (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and COC_LST_SLS_DT is not null
											group by crd_cus_id , CUS_CUS_LONG_NM,cuc_desc30 , crd_acct_opn_dt,CUS_ADMIN_BRH, 
											CUS_CUS_ACCT_TYP,COC_LST_SLS_DT
											ORDER BY crd_acct_opn_dt DESC '
  	  			  EXECUTE sp_executesql @query;
  	  			  
						   SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT  ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category, Min(STN_OS_SLP),'''+ @DB +'''
											FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH 
											Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''
											
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			              SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT  ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,Min(STN_OS_SLP),'''+ @DB +'''
											FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''
											
  	  			    EXECUTE sp_executesql @query;     
  	  			         
  	  			         SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT  ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,
  	  										Min(STN_OS_SLP),'''+ @DB +'''
  	  										FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''
											
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			     SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,FirstSaleDate,DatabaseName)'
  	  						--				SELECT  ''5'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,
  	  						--				STN_OS_SLP
  	  						--				FROM '+ @DB +'_sahstn_rec
											--INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											--Inner Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											--where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											--and stn_inv_dt <= '''+ @RTD +''' AND stn_inv_dt >= '''+ @NFD +''' AND stn_inv_dt <= '''+ @NTD +'''
											--group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,STN_OS_SLP '
				   SET @query =  @query + ' SELECT  ''5'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as ''PreviousSaleDate'' , 
											CUS_ADMIN_BRH,cuc_desc30 as category,
											Min(STN_OS_SLP) ,(select MAX(stn_inv_dt) from '+ @DB +'_sahstn_rec  where STN_SLD_CUS_ID=CUS_CUS_ID and 
											stn_inv_dt >= '''+ @NFD +''' AND stn_inv_dt <= '''+ @NTD +''' group by STN_SLD_CUS_ID ) as ''LastSaleDate'','''+ @DB +'''
											FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and CUS_CUS_ID in (
											select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec group by STN_SLD_CUS_ID  
											having Max(stn_inv_dt) >= '''+ @NFD +''' AND Max(stn_inv_dt) <= '''+ @NTD +'''
											)
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											having MAX(stn_inv_dt) <= '''+ @RTD +''''
											
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			      SET @query = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT  ''6'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,
  	  										Min(STN_OS_SLP),'''+ @DB +'''
  	  										FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and stn_inv_dt <=  '''+ @ATD +''' and stn_inv_dt >= '''+ @AFD +'''
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH'
											--Having MAX(stn_inv_dt) <=  '''+ @ATD +''' and MAX(stn_inv_dt) >= '''+ @AFD +''''
											--and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
  	  			    EXECUTE sp_executesql @query;
  	  			    
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
					SET @sqltxt = '    INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,DatabaseName)
					                        SELECT ''1'', crd_cus_id, CUS_CUS_LONG_NM, crd_acct_opn_dt,COC_LST_SLS_DT, CUS_ADMIN_BRH,cuc_desc30 as category,
											Min(STN_OS_SLP) as SalePersonName,'''+ @DB +'''
											FROM '+ @DB +'_arrcrd_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON CRD_CUS_ID = CUS_CUS_ID
											LEFT JOIN '+ @DB +'_arbcoc_rec ON CRD_CUS_ID = COC_CUS_ID
											LEFT Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											Left JOIN 	'+ @DB +'_sahstn_rec c ON  STN_SLD_CUS_ID= coc_cus_id
											WHERE CUS_CUS_ACCT_TYP=''R''
											And crd_acct_opn_dt Between '''+ @NFD +''' And '''+ @NTD +'''
											and (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and COC_LST_SLS_DT is not null
											group by crd_cus_id , CUS_CUS_LONG_NM,cuc_desc30 , crd_acct_opn_dt,CUS_ADMIN_BRH, 
											CUS_CUS_ACCT_TYP,COC_LST_SLS_DT
											ORDER BY crd_acct_opn_dt DESC '
	        
						--	print(@sqltxt)
						   set @execSQLtxt = @sqltxt; 
						   EXEC (@execSQLtxt);
							
							SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT   ''2'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category
  	  										,Min(STN_OS_SLP),'''+ @DB +'''
  	  										FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +''''

							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
						   EXEC (@execSQLtxt);
							
							SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT   ''3'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category
  	  										,Min(STN_OS_SLP),'''+ @DB +'''
  	  										 FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +''''

							--print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt); 
							
							SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT   ''4'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category
  	  										,Min(STN_OS_SLP),'''+ @DB +'''
  	  										 FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
											Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''''

							--print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt); 
							
							SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,FirstSaleDate,DatabaseName)'
  	  						--				SELECT  ''5'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,
  	  						--				STN_OS_SLP
  	  						--				FROM '+ @DB +'_sahstn_rec
											--INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											--Inner Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											--where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											--and stn_inv_dt <= '''+ @RTD +''' AND stn_inv_dt >= '''+ @NFD +''' AND stn_inv_dt <= '''+ @NTD +'''
											--group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,STN_OS_SLP '
				          SET @sqltxt =  @sqltxt + ' SELECT  ''5'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as ''PreviousSaleDate'' , 
											CUS_ADMIN_BRH,cuc_desc30 as category,
											Min(STN_OS_SLP) ,(select MAX(stn_inv_dt) from '+ @DB +'_sahstn_rec  where STN_SLD_CUS_ID=CUS_CUS_ID and 
											stn_inv_dt >= '''+ @NFD +''' AND stn_inv_dt <= '''+ @NTD +''' group by STN_SLD_CUS_ID ) as ''LastSaleDate'','''+ @DB +'''
											FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and CUS_CUS_ID in (
											select distinct STN_SLD_CUS_ID from '+ @DB +'_sahstn_rec group by STN_SLD_CUS_ID  
											having Max(stn_inv_dt) >= '''+ @NFD +''' AND Max(stn_inv_dt) <= '''+ @NTD +'''
											)
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,STN_OS_SLP 
											having MAX(stn_inv_dt) <= '''+ @RTD +''''
							print(@sqltxt)
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt); 
							
							  SET @sqltxt = '   INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,Branch,Category,SalePersonName,DatabaseName)
  	  										SELECT  ''6'',CUS_CUS_ID,  CUS_CUS_LONG_NM,max(stn_inv_dt) as InvDate, CUS_ADMIN_BRH,cuc_desc30 as category,
  	  										Min(STN_OS_SLP),'''+ @DB +'''
  	  										FROM '+ @DB +'_sahstn_rec
											INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID
											Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat
											where (CUS_ADMIN_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
											and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
											and stn_inv_dt <=  '''+ @ATD +''' and stn_inv_dt >= '''+ @AFD +'''
											group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH'
											--Having MAX(stn_inv_dt) <=  '''+ @ATD +''' and MAX(stn_inv_dt) >= '''+ @AFD +''''
											--and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
										print(@sqltxt)	
							set @execSQLtxt = @sqltxt; 
							EXEC (@execSQLtxt);
     END
  
        IF @Category='Unknown'
           BEGIN
                IF @ISCombineData =1
					BEGIN
					   select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName,DatabaseName
					   from #tmp inner Join  tbl_itech_Account on id=AccountType
					   where AccountType=@Account and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					    and Category is null
					   group by Name,Category,DatabaseName
					END 
					Else IF @ISCombineData =2
					BEGIN
					   select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,DatabaseName
					   from #tmp inner Join  tbl_itech_Account on id=AccountType
					   where AccountType=@Account and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					   and Category is null
					   group by Name,Branch,DatabaseName
					END 
				  Else
					BEGIN
					  select *, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category
					  from #tmp where AccountType=@Account  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					  and Category is null
					  order by CustomerID --and AccountDate <= '2013-03-31' and AccountDate >='2013-03-01'
					END
           END
           ELSE
           BEGIN
                 IF @ISCombineData =1
					BEGIN
					   select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Category)),'Unknown') as Category,'' as AccountName,null as AccountDate,'' as Branch,'' as SalePersonName,DatabaseName
					   from #tmp inner Join  tbl_itech_Account on id=AccountType
					   where AccountType=@Account and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					   group by Name,Category,DatabaseName
					END 
					Else IF @ISCombineData =2
					BEGIN
					   select  Name as AccountType ,count(CustomerID) as CustomerID,ISNULL(Rtrim(LTrim(Branch)),'Unknown') as Branch,'' as AccountName,null as AccountDate,'' as Category,'' as SalePersonName,DatabaseName
					   from #tmp inner Join  tbl_itech_Account on id=AccountType
					   where AccountType=@Account and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					   group by Name,Branch,DatabaseName
					END 
				  Else
					BEGIN
					  select *, ISNULL(Rtrim(LTrim(Category)),'Unknown') as New_Category
					  from #tmp where AccountType=@Account  and CustomerID not IN (select CustomerID from #tmp where DatabaseName ='US'and Branch in ('BHM','MTL','TAI'))
					  order by CustomerID --and AccountDate <= '2013-03-31' and AccountDate >='2013-03-01'
					END
           END
           
           
				  
  Drop table #tmp  
  
END

-- exec sp_itech_GetNew_Dormant_Lost_Account_Fortesting 'US','ALL',2,'6','2013-07-02','Unknown'
-- select * from tbl_itech_Account



GO
