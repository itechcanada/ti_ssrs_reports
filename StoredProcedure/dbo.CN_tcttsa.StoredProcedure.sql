USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_tcttsa]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <Create Date,August,30 2015,>


-- =============================================
CREATE PROCEDURE [dbo].[CN_tcttsa]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

insert
into  [Stratix_US].[dbo].[CN_tcttsa_rec]
  select *
  from [LIVECNSTX].[livecnstxdb].[informix].[tcttsa_rec] ; 
  
END
-- select * from CN_tcttsa_rec 

GO
