USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[SPEC]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Wendy Wang>
-- Create date: <12/15/2011>
-- Description:	<CREATE TABLE TO LET USER ENTER DIAMETER AND TOLERANCE THRU FRONT END APPLICATION>
-- =============================================
create PROCEDURE [dbo].[SPEC] 
	-- Add the parameters for the stored procedure here
	
AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	/*CREATE TABLE TO LET USER ENTER DIAMETER THRU FRONT END APPLICATION*/ 
--DROP TABLE SPEC_USA
--DROP TABLE SPEC_EUR
--DROP TABLE SPEC_CAN
--DROP TABLE SPEC_TW
/*
	CREATE TABLE SPEC_USA
	(
	spec_no varchar(46) not null,
	diameter varchar(15),
	tolerance varchar(10),
	remark varchar(250)
	)
	
	CREATE TABLE SPEC_EUR
	(
	spec_no varchar(46) not null,
	diameter varchar(15),
	tolerance varchar(10),
	remark varchar(250)
	)
	
	CREATE TABLE SPEC_CAN
	(
	spec_no varchar(46) not null,
	diameter varchar(15),
	tolerance varchar(10),
	remark varchar(250)
	)
	
	CREATE TABLE SPEC_TW
	(
	spec_no varchar(46) not null,
	diameter varchar(15),
	tolerance varchar(10),
	remark varchar(250)
	)

INSERT INTO Stratix_US.dbo.SPEC_USA(spec_no)
SELECT mss_sdo+mss_std_id+mss_addnl_id
FROM Stratix_US.dbo.MCRMSS_USA
WHERE mss_sdo+mss_std_id NOT IN (SELECT spec_no FROM Stratix_US.dbo.SPEC_USA)

INSERT INTO Stratix_US.dbo.SPEC_EUR(spec_no)
SELECT mss_sdo+mss_std_id+mss_addnl_id
FROM Stratix_US.dbo.MCRMSS_EUR
WHERE mss_sdo+mss_std_id NOT IN (SELECT spec_no FROM Stratix_US.dbo.SPEC_EUR)

INSERT INTO Stratix_US.dbo.SPEC_CAN(spec_no)
SELECT mss_sdo+mss_std_id+mss_addnl_id
FROM Stratix_US.dbo.MCRMSS_CAN
WHERE mss_sdo+mss_std_id NOT IN (SELECT spec_no FROM Stratix_US.dbo.SPEC_CAN)

INSERT INTO Stratix_US.dbo.SPEC_TW(spec_no)
SELECT mss_sdo+mss_std_id+mss_addnl_id
FROM Stratix_US.dbo.MCRMSS_TW
WHERE mss_sdo+mss_std_id NOT IN (SELECT spec_no FROM Stratix_US.dbo.SPEC_TW)


select * from spec_usa
    

SELECT * from Stratix_US.dbo.MCRCMS_USA
	
SELECT * from Stratix_US.dbo.MCRMSS_USA
	
SELECT * from Stratix_US.dbo.MCRTSF_USA



GO
*/
GO
