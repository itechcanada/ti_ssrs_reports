USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_TemporaryVendorPaymentDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <6 Jan 2013>  
-- Description: <Getting Receipt Details for a Voucher SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_TemporaryVendorPaymentDetails] @DBNAME varchar(50),  @VendorNo Varchar(20),@FromDate datetime, @ToDate datetime  
As  
Begin  
  
  
declare @DB varchar(100)  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @FD varchar(10)  
declare @TD varchar(10)  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
IF @VendorNo = 'ALL' OR @VendorNo = ''  
BEGIN  
--SET @VendorNo = '8888'  
SET @VendorNo = ''  
END  
  
Set @VendorNo = RTRIM(LTRIM(@VendorNo));  
  
CREATE TABLE #tmp (   [Database]   VARCHAR(10)  
        , VoucherPfx   VARCHAR(4)  
        , VoucherNo    int  
        , RefDate    date  
        --, VenID     varchar(20)  
        --, InvPfx       Varchar(4)  
        , InvNo     varchar(50)  
        , InvBrh    varchar(3)  
        , OrigAmt     decimal(20,2)  
        , DueDate    date  
        , DiscAmt    decimal(20,2)  
        )  
          
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);     
       SET @query ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, RefDate, InvNo, InvBrh, OrigAmt, DueDate, DiscAmt)  
       SELECT ''' +  @Prefix + ''' as [Database],  jvc_vchr_pfx, jvc_vchr_no, jvc_ent_dt, jvc_ven_inv_no, jvc_vchr_brh,  
         jvc_vchr_amt, jvc_due_dt, jvc_disc_amt from ' + @Prefix + '_apjjvc_rec where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''' ) ;'         
    print @query;  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
        SET @sqltxt ='INSERT INTO #tmp ([Database], VoucherPfx, VoucherNo, RefDate, InvNo, InvBrh, OrigAmt, DueDate, DiscAmt)  
        SELECT ''' +  @DBNAME + ''' as [Database],  jvc_vchr_pfx, jvc_vchr_no, jvc_ent_dt, jvc_ven_inv_no, jvc_vchr_brh,  
         jvc_vchr_amt, jvc_due_dt, jvc_disc_amt from ' + @DBNAME + '_apjjvc_rec where jvc_ent_dt >= ''' + @FD + ''' and jvc_ent_dt <= ''' + @TD + ''' and (RTRIM(LTRIM(jvc_ven_id)) = ''' + @VendorNo + ''' OR ''' + ''' = ''' + @VendorNo + ''') ;'  
     print(@sqltxt)   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
   END  
   Select * from #tmp order by [Database],RefDate,VoucherNo  
   Drop table #tmp  
End   
  
          
-- Exec [sp_itech_TemporaryVendorPaymentDetails] 'US', '  8888  ', '2015-01-01', '2015-12-31'  
-- Exec [sp_itech_VoucherForReceipts] 'CA',  '2854'  
  
--select * from [LIVECASTX].[livecastxdb].[informix].apjjci_rec where jci_vchr_no = '53250'  
--select top 100 jvc_vchr_pfx, * from [LIVECASTX].[livecastxdb].[informix].apjjvc_rec where jvc_vchr_no = 53250  
--select distinct jvc_vchr_pfx from [LIVECASTX].[livecastxdb].[informix].apjjvc_rec  
GO
