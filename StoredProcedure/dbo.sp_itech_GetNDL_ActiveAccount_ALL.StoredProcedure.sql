USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNDL_ActiveAccount_ALL]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Mukesh >                  
-- Create date: <20 Feb 2017>                  
-- Description: <Getting OTP>                  
-- =============================================                  
CREATE PROCEDURE [dbo].[sp_itech_GetNDL_ActiveAccount_ALL] @DBNAME varchar(50),@Branch varchar(50),@Month as Datetime ,@Market varchar(50), @version char = '0'--,@ISCategory int--,@Account varchar(50)                  
                  
AS                  
BEGIN                  
SET NOCOUNT ON;                  
                  
                  
	declare @sqltxt varchar(6000)                  
	declare @execSQLtxt varchar(7000)                  
	declare @SQL varchar(max)                  
	declare @DB varchar(100)                  
	declare @NFD varchar(10)                  
	declare @NTD varchar(10)                  
	declare @D3MFD varchar(10)                  
	declare @D3MTD varchar(10)                  
	declare @D6MFD varchar(10)                  
	declare @D6MTD varchar(10)                  
	declare @LFD varchar(10)                  
	declare @LTD varchar(10)                  
	DECLARE @StartDate VARCHAR(15);                    
	DECLARE @EndDate varchar(15);                  
	declare @AFD varchar(10)                  
	declare @ATD varchar(10)                  
	declare @RFD varchar(10)                  
	declare @RTD varchar(10)                  
                  
	CREATE TABLE #tmp (      AccountType varchar(50)                  
		,CustomerID  VARCHAR(15)                  
		,AccountName VARCHAR(150)                  
		,AccountDate varchar(15)                  
		,FirstSaleDate Varchar(15)                  
		,Branch  VARCHAR(15)                  
		,Category  VARCHAR(100)                  
		,SalePersonName  VARCHAR(50)                  
		,Months varchar(15)                  
        );                  
                          
	CREATE TABLE #tmpFinal (      AccountType varchar(50)                  
		,CustomerID  VARCHAR(15)                  
		,AccountName VARCHAR(150)                  
		,AccountDate varchar(15)                  
		,FirstSaleDate Varchar(15)                  
		,Branch  VARCHAR(15)                  
		,Category  VARCHAR(100)                  
		,SalePersonName  VARCHAR(50)                  
		,Months varchar(15)                  
        );                          
                          
	CREATE TABLE #Main (   AccountType varchar(50)                  
		,CustomerID  int                  
		,AccountName VARCHAR(150)                  
		,AccountDate varchar(15)                  
		,FirstSaleDate Varchar(15)                  
		,Branch  VARCHAR(15)                  
		,Category  VARCHAR(100)                  
		,SalePersonName  VARCHAR(50)                  
		,Months varchar(15)                  
        );                  
                          
	CREATE TABLE #AVG (   avgID varchar(50)                  
		, average Decimal(20,2)                
        ,avgMonths varchar(15)                
        );                 
	CREATE TABLE #tactive (  CustomerID  VARCHAR(15)                  
		,FirstInvOfMonth Date                  
        );                  
                                   
	CREATE TABLE #tTtlactive (  CustomerID  VARCHAR(15)                  
		,FirstInvOfMonth Date                  
        );                          
                          
	set @DB= @DBNAME                  
	IF @Branch = 'ALL'                  
	BEGIN                  
		set @Branch = ''                  
	END                  
                   
	IF @Market = 'ALL'                  
	BEGIN                  
		set @Market = ''                  
	END                  
                   
 --------  +++++  Created Table for last 12 months ++++++++    
	Create TABLE #Last12Months (   StartDate varchar(15)                  
		,EndDate  VARCHAR(15)                  
        );            
	print '1';                  
	declare @start DATE = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month)-1, 0) , 120);                  
	with CTEE(Last12MonthDate)                  
	AS               
	(                  
		SELECT @start                  
		UNION   all                  
	    SELECT DATEADD(month,-1,Last12MonthDate)                  
		from CTEE                  
		where DATEADD(month,-1,Last12MonthDate)>=DATEADD(month,-DATEDIFF(Month,'2013-02-01',getdate()),@start)  -- change 2013 to 2017 to set no of month                
   --   where DATEADD(month,-1,Last12MonthDate)>=DATEADD(month,-1,@start)  -- to set no of month                  
	)                  
	INSERT INTO #Last12Months(StartDate,EndDate)                  
	select Last12MonthDate as StartDate,CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Last12MonthDate)+1,0)), 120) as EndDate  from CTEE                  
	option (maxrecursion 365); -- add max recursion for error maximum recursion 100 has been exhausted before statement completion
	print '2';

	DECLARE @DatabaseName VARCHAR(35);                  
	DECLARE @Prefix VARCHAR(5);                  
	DECLARE @Exists VARCHAR(max);                  
	DECLARE @E VARCHAR(max);                  
	DECLARE @val varchar(5);                  
                        
	IF @DBNAME = 'ALL'                  
	BEGIN                  
		IF @version = '0'                
		BEGIN                
			DECLARE DBAcct CURSOR FOR                
			select DatabaseName,Prefix from tbl_itech_DatabaseName -- where  Prefix = 'CA';                
			OPEN DBAcct;                
		END                
		ELSE                
		BEGIN                
			DECLARE DBAcct CURSOR FOR                
			select DatabaseName,Prefix from tbl_itech_DatabaseName_ALL -- where  Prefix = 'US';                
			OPEN DBAcct;                
		END                
		FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;                  
		WHILE @@FETCH_STATUS = 0                  
		BEGIN                  
			SET @DB= @Prefix                 
			declare @MinSalesDate Varchar(10);                
            Print 'DB Name: '+ @DB;  
			IF (UPPER(@Prefix) = 'TW')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from TW_sahstn_rec)              
			End              
			Else if (UPPER(@Prefix) = 'NO')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from NO_sahstn_rec)              
			End              
			Else if (UPPER(@Prefix) = 'CA')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from CA_sahstn_rec)              
			End              
			Else if (UPPER(@Prefix) = 'CN')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from CN_sahstn_rec)              
			End              
			Else if (UPPER(@Prefix) = 'US')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from US_sahstn_rec)              
			End              
			Else if(UPPER(@Prefix) = 'UK')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from UK_sahstn_rec)              
			End  
			Else if(UPPER(@Prefix) = 'DE')              
			begin              
				SET @MinSalesDate = (select MIN(STN_INV_DT) from DE_sahstn_rec)              
			End  
                  
			DECLARE ScopeCursor CURSOR FOR                  
			select StartDate,EndDate from  #Last12Months                  
			OPEN ScopeCursor;                  
			FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;                  
			WHILE @@FETCH_STATUS = 0                  
			BEGIN                  
				DECLARE @query NVARCHAR(max);                   
				print 'StartDate: '+ @StartDate +'	EndDate: '+ @EndDate ;
				set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate),0)) , 120)   -- New Account                  
				set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)  -- New Account                  
                  
				set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-3,0)) , 120)   -- Dormant 3 Month                  
				set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-2,0)), 120)  -- Dormant 3 Month                  
                  
				set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-6,0)) , 120)   --Dormant 6 Month                  
				set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-5,0)), 120)  -- Dormant 6 Month                  
                  
				set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)   -- Lost Account                  
				set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-17,0)), 120)  -- Lost Account                  
                               
				set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)    -- Active Accounts                   
				set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)                  
                           
				SET @query = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth)                  
				select distinct stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth                   
				from '+ @DB +'_sahstn_rec                  
				where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''                  
				group by stn_sld_cus_id'                  
                  
				EXECUTE sp_executesql @query;                  
				-- Total Active                  
				SET @query = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth)                  
				select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth                   
				from '+ @DB +'_sahstn_rec                  
				where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''                  
				group by stn_sld_cus_id '                  
				print @query;                
				EXECUTE sp_executesql @query;                    
                          
                          
				if(@DB = 'US' and @StartDate >= ('2015-11-01'))                    
				begin                
				SET @SQL = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
				select ''1''+'''+@DB+''', CUS_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +'''             
				from '+ @DB +'_arrcus_rec             
				left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
				left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= CUS_CUS_ID and CRD_CMPY_ID= cus_cmpy_id                 
				left join tbl_itech_US_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                 
				where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +''' Group by  CUS_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30                  
				Union                  
				SELECT  '''+@DB+'''+''3'',CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,'''',CUS_ADMIN_BRH,cuc_desc30,              
				Min(STN_OS_SLP),'''+ @StartDate +''' as M             
				FROM '+ @DB +'_sahstn_rec             
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID                 
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat                 
				left join tbl_itech_US_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                 
				where   coc_lst_sls_dt Between '''+ @D3MFD +''' and  '''+ @D3MTD +'''              
				group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,coc_lst_sls_dt               
				Union                   
				SELECT  '''+@DB+'''+''2'',CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,'''',CUS_ADMIN_BRH,cuc_desc30,              
				Min(STN_OS_SLP),'''+ @StartDate +''' as M             
				FROM '+ @DB +'_sahstn_rec             
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID                
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat             
				left join tbl_itech_US_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id               
				where coc_lst_sls_dt Between '''+ @D6MFD +''' and '''+ @D6MTD +'''             
				group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH,coc_lst_sls_dt                
				Union                   
				SELECT  '''+@DB+'''+''4'',CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,'''',CUS_ADMIN_BRH,cuc_desc30,              
				Min(STN_OS_SLP),'''+ @StartDate +''' as M             
				FROM '+ @DB +'_sahstn_rec         
				INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID                 
				Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat             
				left join tbl_itech_US_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id               
				where  coc_lst_sls_dt Between '''+ @LFD +''' and  '''+ @LTD +'''              
				group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,coc_lst_sls_dt            
				'              
				if ( @MinSalesDate <= @AFD)                
					SET @SQL = @SQL +  '                
					Union                   
					SELECT '''+@DB+'''+''1'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +'''             
					FROM #tactive t, '+ @DB +'_arrcrd_rec             
					join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id                 
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
					left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id             
					where t.CustomerID Not IN (select distinct t1.CustomerID  from #tactive t1,'+ @DB +'_sahstn_rec u             
					where t1.CustomerID = u.STN_SLD_CUS_ID                 
					and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''') and t.CustomerID not in  (select coc_cus_id from tbl_itech_US_arbcoc_rec               
					where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +''') and  t.CustomerID = crd_cus_id               
					and t.FirstInvOfMonth = ''' +    @NFD + '''  '              
-- Else               
              
					SET @SQL = @SQL +  '                 
					Union                   
					SELECT '''+@DB+'''+''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,'''','''', CUS_ADMIN_BRH,cuc_desc30 as category,'''','''+ @StartDate +''' as M             
					FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id                 
					left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat             
					where t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + ''' group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH'                  
				end                
				else                
				begin                
					SET @SQL = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
            select ''1''+'''+@DB+''', CUS_CUS_ID, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +''' from '+ @DB +'_arrcus_rec                  
            left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat left join '+ @DB +'_arrcrd_rec  on CRD_CUS_ID= CUS_CUS_ID and CRD_CMPY_ID= cus_cmpy_id              
             left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                 
            where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' +   @NTD +''' Group by  CUS_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30                  
         Union                  
         SELECT  '''+@DB+'''+''3'',CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,'''',CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +''' as M FROM '+ @DB +'_arrcus_rec                 
          Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id               
          where coc_lst_sls_dt <=  '''+ @D3MTD +''' and coc_lst_sls_dt >= '''+ @D3MFD +'''   group by CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,cuc_desc30, CUS_ADMIN_BRH                 
         Union                   
         SELECT  '''+@DB+'''+''2'',CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,'''',CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +''' as M FROM '+ @DB +'_arrcus_rec                 
         Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id                
         where coc_lst_sls_dt <=  '''+ @D6MTD +''' and coc_lst_sls_dt >= '''+ @D6MFD +'''   group by CUS_CUS_ID,CUS_CUS_LONG_NM,coc_lst_sls_dt,cuc_desc30, CUS_ADMIN_BRH                 
         Union                   
         SELECT  '''+@DB+'''+''4'',CUS_CUS_ID,CUS_CUS_LONG_NM,max(stn_inv_dt),'''',CUS_ADMIN_BRH,cuc_desc30,Min(STN_OS_SLP),'''+ @StartDate +''' as M FROM '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID                 
         Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat where  STN_INV_DT <= ''' + @NTD +''' group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30,       
         CUS_ADMIN_BRH Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''' '              
          if ( @MinSalesDate <= @AFD)                
         SET @SQL = @SQL +  '               
          Union                
         SELECT '''+@DB+'''+''1'', crd_cus_id, '''', '''','''', CUS_ADMIN_BRH,cuc_desc30,'''','''+ @StartDate +''' FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id                 
         left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat left join '+ @DB +'_arbcoc_rec on coc_cmpy_id = cus_cmpy_id       
         and coc_cus_id = cus_cus_id where t.CustomerID Not IN (select distinct t1.CustomerID  from #tactive t1,'+ @DB +'_sahstn_rec u       
  where t1.CustomerID = u.STN_SLD_CUS_ID and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''') and t.CustomerID not in               
 (select coc_cus_id from '+ @DB +'_arbcoc_rec where coc_frst_sls_dt Between  ''' + @NFD + ''' And ''' + @NTD +''') and  t.CustomerID = crd_cus_id               
 and t.FirstInvOfMonth = ''' +  @NFD + ''' '              
-- Else               
  end               
         SET @SQL = @SQL +  '               
         Union                   
         SELECT '''+@DB+'''+''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,'''','''', CUS_ADMIN_BRH,cuc_desc30 as category,'''','''+ @StartDate +''' as M FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id                 
  left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat where t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' +  @NFD + ''' group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH'                  
                   
                        
         set @query = @SQL                  
         print(@query)                  
         EXECUTE sp_executesql @query;                  
                           
         set @val='1'+''+@DB+'';                  
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')                  
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT ''1''+'''+@DB+''' as A,'''+ @StartDate +''' as M '                  
   EXECUTE sp_executesql @query;                  
          End                  
                            
         set @val=''+@DB+''+'1';                  
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')                  
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT '''+@DB+'''+''1'' as A,'''+ @StartDate +''' as M '                  
           EXECUTE sp_executesql @query;                  
          End                   
                            
         set @val=''+@DB+''+'2';                  
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')           
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT '''+@DB+'''+''2'' as A,'''+ @StartDate +''' as M '                  
           EXECUTE sp_executesql @query;                  
          End                   
                            
         set @val=''+@DB+''+'3';                  
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')                  
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT '''+@DB+'''+''3'' as A,'''+ @StartDate +''' as M '                  
           EXECUTE sp_executesql @query;                  
          End                   
                             
         set @val=''+@DB+''+'4';                  
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')                  
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT '''+@DB+'''+''4'' as A,'''+ @StartDate +''' as M '                  
           EXECUTE sp_executesql @query;                  
          End                   
                            
         set @val=''+@DB+''+'5';           
          if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')                  
             begin                   
           SET @query = 'INSERT INTO #tmp(AccountType,Months)                  
              SELECT '''+@DB+'''+''5'' as A,'''+ @StartDate +''' as M '                  
           EXECUTE sp_executesql @query;                  
          End                      
                      
          Truncate table #tactive;                  
          Truncate Table #tTtlactive;                  
                      
                         
                            
          set @val=''                  
                            
          if @DB='US'                  
          begin                
            Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')  --and  CustomerID not like  'L%' and AccountType like '1%'                
            -- Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')  and   AccountType not like '1%'                
            end                
         else                  
              Insert into #tmpFinal select * from #tmp                   
                           
                           
         truncate table #tmp                  
                           
        FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;                  
        END                   
        CLOSE ScopeCursor;                  
        DEALLOCATE ScopeCursor;                  
                          
                          
                       
        FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;                  
       END                   
    CLOSE DBAcct;                  
    DEALLOCATE DBAcct;                  
                          
   End                  
                   
    INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
  select AccountType ,                
  (case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end) as CustomerID,                
  '' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal                   
 group by AccountType,Months                  
                 
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
 select 'TActAcct' ,(case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end)  as CustomerID,      
 '' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName        
    ,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where [order] in (1,4,8,11,14,17,60) -- and  CustomerID not like  'L%' --name like '%Total New Acct %'                    
 group by AccountType,Months                  
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
 select 'TRA' ,(case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end)  as CustomerID,'' as              
  AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total Reactivated Acct%'                  
 group by AccountType,Months                  
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
 select 'T6MDA' ,(case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end)  as CustomerID,              
 '' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total 6M Dormant Acct%'                    
 group by AccountType,Months         
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
 select 'T3MDA' ,(case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end)  as CustomerID,              
 '' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total 3M Dormant Acct%'                  
 group by AccountType,Months                  
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
 select 'TLA' ,(case when (AccountType IN ('PS3','PS2','PS4','PS5') and Months >= ('2015-11-01')) then 0 else count(CustomerID) end)  as CustomerID,              
 '' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total Lost Acct%'                    
 group by AccountType,Months                  
                   
  INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months)                   
              
-- eDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 select 'TCMAA' ,(case when (AccountType like '%PS%') then 0 else count(CustomerID) end)  as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months                  
 from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Current Month active customers%'                  
 group by AccountType,Months                  
                
insert into #AVG(avgID,average,avgMonths)                
Select AccountType,SUM(CustomerID),Months from #Main group by AccountType,Months;                
                   
   select [order],databasename, id, Name as AccountType ,CustomerID as CustomerID,'' as Category,'' as AccountName,'' as AccountDate,                 
   Months,'' as Branch,'' as SalePersonName ,(select Avg(average) from #AVG where avgID = AccountType  ) as avgChart                
    from #Main Inner Join  tbl_itech_management_account on id=AccountType                 
 order by [order]                  
                
                   
   Drop table #tactive;                  
    Drop Table #tTtlactive;                           
  drop table #tmpFinal                    
  Drop table #tmp                    
  Drop table #Last12Months                  
  drop table #Main                 
  drop table #AVG                 
                   
                    
END                  
                  
-- exec sp_itech_GetNDL_ActiveAccount_ALL 'ALL','ALL','2020-03-04','ALL' ,'1'                 
                  
                  
--select * from tbl_itech_management_account   it is taking 21 Min to execute          
/*          
CA: 00:00:26          
CN: 00:00:08          
UK: 00:00:21          
TW: 00:00:14          
US: 00:18:00   
  
20210128 Sumit  
Add Germany Database  
*/ 
GO
