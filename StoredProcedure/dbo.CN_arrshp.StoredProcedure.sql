USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_arrshp]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CN_arrshp]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.CN_arrshp_rec', 'U') IS NOT NULL          
  drop table dbo.CN_arrshp_rec;          
              
                  
SELECT *          
into  dbo.CN_arrshp_rec   
FROM [LIVECNSTX].[livecnstxdb].[informix].[arrshp_rec]     
    
END    
    
GO
