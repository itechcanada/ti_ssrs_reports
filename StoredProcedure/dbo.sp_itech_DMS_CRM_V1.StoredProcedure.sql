USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_DMS_CRM_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                              
-- Author:  <Mukesh >                              
-- Create date: <29 Mar 2019>     sp_itech_IS_Performance_LinesBooked                         
-- Modified by: <Mukesh>      
-- =============================================                              
CREATE PROCEDURE [dbo].[sp_itech_DMS_CRM_V1] @DBNAME varchar(50),@Branch varchar(50)                            
                              
AS                              
BEGIN                              
                               
 SET NOCOUNT ON;                     
                   
                     
declare @sqltxtAct varchar(6000)                              
declare @execSQLtxtAct varchar(7000)                              
declare @sqltxtTsk varchar(6000)                              
declare @execSQLtxtTsk varchar(7000)                              
declare @DB varchar(100)                              
declare @FD varchar(10)                              
declare @TD varchar(10)                              
                              
set @DB=  @DBNAME                              
                    
IF @Branch = 'ALL'                              
 BEGIN                              
  set @Branch = ''                              
 END                                
                                  
                               
set @FD = CONVERT(VARCHAR(10), DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) , 120)                              
set @TD = CONVERT(VARCHAR(10), GETDATE(),120)                               
                              
                             
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)                      
     ,Branch Varchar(10)                  
     ,LoginID varchar(35)                                 
        , TskCallDormant  integer                              
        , TskCallExisting  integer            
        ,TskCallProspect integer          
  ,TskCallUnqualified integer     
         , TskVisitDormant  integer                              
        , TskVisitExisting  integer            
        ,TskVisitProspect integer                             
  ,TskVisitUnqualified integer    
                 );                               
DECLARE @DatabaseName VARCHAR(35);                              
DECLARE @Prefix VARCHAR(35);                              
DECLARE @Name VARCHAR(15);                              
                          
IF @DBNAME = 'ALL'                              
 BEGIN                              
                                  
    DECLARE ScopeCursor CURSOR FOR                                
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                 
   OPEN ScopeCursor;                                
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                              
     WHILE @@FETCH_STATUS = 0                              
       BEGIN                              
        DECLARE @queryAct NVARCHAR(4000);                                 
        DECLARE @queryTsk NVARCHAR(4000);                                 
      SET @DB= @Prefix                                
                                      
        SET @queryTsk =                              
              'INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting,TskCallProspect, TskCallUnqualified, TskVisitDormant , TskVisitExisting  ,TskVisitProspect, TskVisitUnqualified)                            
      Select * from (                             
      select ''' +  @DB + ''' as [Database], usr_usr_brh as branch,  usr_lgn_id, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                             
       from ' + @DB + '_cctcta_rec join                             
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1               
       join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to                 
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                    
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR                    
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                              
                                
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                         
       group by  usr_lgn_id,usr_usr_brh, atp_desc30                          
       ) AS Query1                              
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Phone Call Unqualified Lead],
	  [Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account], [Visit Unqualified Lead])) As po '               
     
        EXECUTE sp_executesql @queryTsk;                              
        print(@queryTsk);                              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;               
       END                               
    CLOSE ScopeCursor;                              
    DEALLOCATE ScopeCursor;                              
  END                              
  ELSE                  
     BEGIN                                  
 -- task                                
                                  
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database],Branch, LoginID,  TskCallDormant, TskCallExisting,TskCallProspect, TskCallUnqualified, TskVisitDormant , TskVisitExisting  ,TskVisitProspect, TskVisitUnqualified)                            
      Select * from (                             
      select ''' +  @DB + ''' as [Database], usr_usr_brh as branch,  usr_lgn_id, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                             
       from ' + @DB + '_cctcta_rec join                             
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1                      
       join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to                 
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                    
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR                    
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                              
                                
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                         
       group by  usr_lgn_id,usr_usr_brh, atp_desc30                          
       ) AS Query1                              
      pivot (Max(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Phone Call Unqualified Lead],
	  [Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account],[Visit Unqualified Lead])) As po'                             
      
    print(@sqltxtTsk)                              
    set @execSQLtxtTsk = @sqltxtTsk;                               
   EXEC (@execSQLtxtTsk);                              
     END                   
                
select [Database],Branch,   LoginID,    
ISNULL(TskCallDormant,0) as TskCallDormant,   
ISNULL(TskCallExisting,0) as TskCallExisting,            
ISNULL(TskCallProspect,0) as TskCallProspect,            
ISNULL(TskCallUnqualified,0) as TskCallUnqualified,  
ISNULL(TskVisitDormant,0) as TskVisitDormant,   
ISNULL(TskVisitExisting,0) as TskVisitExisting,            
ISNULL(TskVisitProspect,0) as TskVisitProspect,         
ISNULL(TskVisitUnqualified,0) as TskVisitUnqualified,  
 -- SUBSTRING(LoginID, 1, CHARINDEX(' ', LoginID) - 1)              
ISNULL(TskCallDormant,0) + ISNULL(TskCallExisting,0)  + ISNULL(TskCallProspect,0) + ISNULL(TskCallUnqualified,0) + ISNULL(TskVisitDormant,0) + ISNULL(TskVisitExisting,0)          
+ ISNULL(TskVisitProspect,0) + ISNULL(TskVisitUnqualified,0) as Total from #tmpTsk order by Total desc;                
                                  
    Drop table #tmpTsk                              
END                              
                
-- exec [sp_itech_DMS_CRM_V1] 'ALL', 'ALL'                
/*                    
(select COUNT(ISlp)/count(distinct ISlp) FROM #temp where ActvyMonth = Month(GETDATE()) )                
              
Mail Sub: DMS Report - LAX Branch Update Request              
Date:20190904              
    
20200710 Sumit    
Mail : RE: DMS File and Dormant Calls    
Replace Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account  
20200828  
Add Phone Call Unqualified Lead   ,Visit Unqualified Lead    
Mail: Re: CRM  
*/ 
GO
