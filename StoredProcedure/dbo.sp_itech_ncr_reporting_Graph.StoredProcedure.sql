USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ncr_reporting_Graph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,10/05/2015,>   

-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_ncr_reporting_Graph] @DBNAME varchar(10), @FromDate datetime, @ToDate datetime    
     
AS    
BEGIN    
    
declare @sqltxt varchar(7000)      
declare @execSQLtxt varchar(7000)      
declare @DB varchar(100)      
declare @FD varchar(10)      
declare @TD varchar(10)      
    
DECLARE @DatabaseName VARCHAR(35);      
DECLARE @Prefix VARCHAR(35);      
DECLARE @Name VARCHAR(15);      
DECLARE @CurrenyRate varchar(15);     
    
set @DB=  @DBNAME      
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)      
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)     
    
    
CREATE TABLE #tmp (       
         Databases   VARCHAR(15)       
        , NitBranch   varchar(10)  
        --,Rsn Varchar(15)      
        ,NumberOfNcr int       
        --,AmountClaimed  DECIMAL(20, 2)      
        --,ApprovedCreditVal DECIMAL(20, 2)    
        --,MatWeight DECIMAL(20, 2)    
                  );      
    
    
IF @DBNAME = 'ALL'      
  BEGIN    
  DECLARE ScopeCursor CURSOR FOR        
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
  OPEN ScopeCursor;    
      
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;      
        WHILE @@FETCH_STATUS = 0      
       BEGIN      
   DECLARE @query NVARCHAR(max);        
   SET @DB= @Prefix     
   IF (UPPER(@Prefix) = 'TW')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
   End      
   Else if (UPPER(@Prefix) = 'NO')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
   End      
   Else if (UPPER(@Prefix) = 'CA')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
   End      
   Else if (UPPER(@Prefix) = 'CN')      
    begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
   End      
   Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')      
    begin      
     SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
   End      
   Else if(UPPER(@Prefix) = 'UK')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
      End     
   Else if(UPPER(@Prefix) = 'DE')      
    begin      
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
      End     
          
      
      
   SET @query =  'INSERT INTO #tmp (Databases, NitBranch, NumberOfNcr)    
   
 SELECT t.db,t.nit_brh, sum(t.Number_Of_NCRs)  
 FROM   
 (   
   Select ''' + @Name + ''' as db, nit_brh,   count(*) AS Number_Of_NCRs,    
 sum(nit_claim_val * '+ @CurrenyRate +') as AmountClaimed, sum(nit_apcr_val * '+ @CurrenyRate +') AS ApprovedCreditVal, 0 as MatWeight    
 FROM '+@DB+'_nctnit_rec    
 JOIN '+@DB+'_nctnhh_rec ON     
 nhh_cmpy_id = nit_cmpy_id     
 AND nhh_ncr_pfx = nit_ncr_pfx     
 AND nhh_ncr_no = nit_ncr_no  
 where nhh_crtd_dtts  >= '''+ @FD +'''    
 AND  nhh_crtd_dtts  <= '''+ @TD +'''    
 group by nit_brh  
    
 
 ) t GROUP BY db,nit_brh
   
  '    
    
    
          
  EXECUTE sp_executesql @query;      
        print(@query)        
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;           
          
  END      
   CLOSE ScopeCursor;      
      DEALLOCATE ScopeCursor;    
          
          
END   -- All Database block end here    
    
    
ELSE  -- Single database query start here    
 BEGIN    
     
 Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')    
     
 IF (UPPER(@DBNAME) = 'TW')      
 begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
 End      
 Else if (UPPER(@DBNAME) = 'NO')      
 begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
 End      
 Else if (UPPER(@DBNAME) = 'CA')      
 begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
 End      
 Else if (UPPER(@DBNAME) = 'CN')      
 begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
 End      
 Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')      
 begin      
  SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
 End      
 Else if(UPPER(@DBNAME) = 'UK')     begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
 End      
 Else if(UPPER(@DBNAME) = 'DE')     begin      
  SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
 End      
     
 SET @sqltxt ='INSERT INTO #tmp (Databases, NitBranch, NumberOfNcr)    
   
 SELECT t.db,t.nit_brh,sum(t.Number_Of_NCRs)  
 FROM   
 (   
   Select ''' + @Name + ''' as db, nit_brh, count(*) AS Number_Of_NCRs
 FROM '+@DBNAME+'_nctnit_rec    
 JOIN '+@DBNAME+'_nctnhh_rec ON     
 nhh_cmpy_id = nit_cmpy_id     
 AND nhh_ncr_pfx = nit_ncr_pfx     
 AND nhh_ncr_no = nit_ncr_no   
 where nhh_crtd_dtts  >= '''+ @FD +'''    
 AND  nhh_crtd_dtts  <= '''+ @TD +'''    
 group by nit_brh  
    
  
 ) t GROUP BY db,nit_brh
   
  '    
      
   print(@sqltxt)  ;      
   print @DBNAME + @Name    
    set @execSQLtxt = @sqltxt;       
   EXEC (@execSQLtxt);    
 END --   -- Single database query end here    
    
SELECT Databases, NitBranch, NumberOfNcr 
FROM #tmp    
    
DROP TABLE #tmp ;   
    
-- exec sp_itech_ncr_reporting_Graph  'US','01/09/2016', '02/08/2016';      
-- exec sp_itech_ncr_reporting_Graph  'ALL','09/05/2015', '10/05/2015';      
    
END    
  
GO
