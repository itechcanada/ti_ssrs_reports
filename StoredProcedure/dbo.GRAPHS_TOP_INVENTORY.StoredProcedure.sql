USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[GRAPHS_TOP_INVENTORY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GRAPHS_TOP_INVENTORY]
	-- Add the parameters for the stored procedure here
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/*EXECUTE GRAPHS_PRODUCTS_INVENTORY created this two tables.  This Crystal Report is located in Monthly folder.
It will run monthly and store data to the tables*/

EXECUTE GRAPHS_PRODUCTS_INVENTORY @EndDate 

SELECT * FROM top_inventory
WHERE product IN (SELECT product FROM top_shipments)
ORDER BY period, product ASC

/*DELETE FROM top_inventory
  DELETE FROM top_shipments*/
END
GO
