USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesOffice_SalesPerson]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  <Mukesh >          
-- Create date: <22 Nov 2017>          
-- Description: <Getting Open order of customers for SSRS reports>          
       
-- =============================================          
CREATE PROCEDURE [dbo].[sp_itech_SalesOffice_SalesPerson] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10)
          
AS          
BEGIN         
           
 SET NOCOUNT ON;          
declare @sqltxt varchar(7000)          
declare @execSQLtxt varchar(7000)          
declare @DB varchar(100)          
declare @FD varchar(10)          
declare @TD varchar(10)          
declare @NOOfCust varchar(15)          
DECLARE @ExchangeRate varchar(15)      
    
        
     if(@FromDate = '' OR @FromDate = null OR @FromDate = '1900-01-01' )    
     begin    
     set @FromDate = '2009-01-01'    
      End    
          
      if (@ToDate = '' OR @ToDate = null OR @ToDate = '1900-01-01')    
      begin    
      set @ToDate = CONVERT(VARCHAR(10), GETDATE() , 120)    
      end    
          
set @DB= @DBNAME      
  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)          
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)          
          
          
CREATE TABLE #tmp (  
		Databases varchar(10) 
		,Branch varchar(10)  
		 ,OrderNo varchar(10)          
        ,OrderItem   varchar(3)         
        , ChargeValue    DECIMAL(20, 0)  
        , DueDate    varchar(20)
        , OrderType     varchar(65) 
        , Salesperson     varchar(65) 
        --, BalPcs  DECIMAL(20, 2)          
        -- ,CustID   VARCHAR(10) 
		--CompanyId varchar(5)           
  --      ,OrderPrefix Varchar(2)          
        --, CustName     VARCHAR(65)          
        --, CustomerCat   VARCHAR(65)           
        --, BalWgt           DECIMAL(20, 2)          
        --, BalMsr    DECIMAL(20, 2)          
        --, StatusAction               varchar(2)          
        --, ProductName   VARCHAR(100)           
        --,Form  varchar(35)          
        --,Grade  varchar(35)          
        --,Size  varchar(35)          
        --,Finish  varchar(35)         
        --,CustPO   VARCHAR(30)       
        --,CustAdd VARCHAR(170)       
        -- ,SalesPersonIs Varchar(10)        
        --,SalesPersonOs Varchar(10)       
        --,NetAmt Decimal(20,2)      
        --,NetPct Decimal(20,2)      
        --,ShipWhs Varchar(3)     
        --,PrdFull varchar(70)      
                 );           
CREATE TABLE #tmp1 (  
		Salesperson varchar(65)          
        ,OrderCount  Varchar(10)
       
     )     
DECLARE @DatabaseName VARCHAR(35);          
DECLARE @Prefix VARCHAR(5);          
DECLARE @Name VARCHAR(15);          
DECLARE @CurrenyRate varchar(15);           
  
           
 if @Branch ='ALL'          
 BEGIN          
 set @Branch = ''          
 END          
          
          
IF @DBNAME = 'ALL'          
 BEGIN          
           
    DECLARE ScopeCursor CURSOR FOR            
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS             
   OPEN ScopeCursor;            
   
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
        DECLARE @query NVARCHAR(Max);             
      SET @DB= @Prefix          
      IF (UPPER(@Prefix) = 'TW')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@Prefix) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@Prefix) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@Prefix) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                  
    begin                  
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@Prefix) = 'UK')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End             
    Else if(UPPER(@Prefix) = 'DE')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End             
      SET @query =          
       'INSERT INTO #tmp (Databases,Branch ,ChargeValue,DueDate,OrderNo,OrderItem,OrderType ,Salesperson)
       select distinct ''' + @DB + ''',ord_ord_brh as Branch,  chl_chrg_val * '+ @CurrenyRate +' as ChargeValue, orl_due_to_dt as DueDate
       ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem,cds_desc  ,usr_nm as Salesperson    
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_arrcuc_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,          
       ' + @DB + '_ortchl_rec, ' + @DB + '_scrslp_rec ou ,' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec,' + @DB + '_mxrusr_rec  where 
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and  cuc_cus_cat = cus_cus_cat and         
       ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id          
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no          
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
        and cds_cd=orh_ord_typ       AND       
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm
        and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id     
        and cht_tot_typ = ''T''            
       and orl_bal_qty >= 0           
       and chl_chrg_cl= ''E''          
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''          
       and orh_sts_actn <> ''C''          
       and ord_ord_pfx <>''QT''           
       and        
       (       
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )      
       or      
       (orl_due_to_dt is null and orh_ord_typ =''J''))      
             
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
                
        and  ord_ord_brh not in (''SFS'')        
       '          
          
     print(@query);       
        EXECUTE sp_executesql @query;          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN           
            
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')            
               
     IF (UPPER(@DB) = 'TW')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@DB) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@DB) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@DB) = 'CN')                  
    begin                  
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@DB) = 'UK')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End            
    Else if(UPPER(@DB) = 'DE')                  
    begin                  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End            
              
     SET @sqltxt ='INSERT INTO #tmp (Databases,Branch ,ChargeValue,DueDate,OrderNo,OrderItem,OrderType ,Salesperson)
       select distinct ''' + @DB + ''', ord_ord_brh as Branch, chl_chrg_val * '+ @CurrenyRate +' as ChargeValue, orl_due_to_dt as DueDate
       ,ord_ord_no as OrderNo,ord_ord_itm as OrderItem ,cds_desc  ,usr_nm as Salesperson    
       from ' + @DB + '_ortord_rec,' + @DB + '_arrcus_rec,' + @DB + '_arrcuc_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,          
       ' + @DB + '_ortchl_rec, ' + @DB + '_scrslp_rec ou , ' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec ,' + @DB + '_mxrusr_rec where 
       cus_cmpy_id=ord_cmpy_id and cus_cus_id=ord_sld_cus_id and  cuc_cus_cat = cus_cus_cat and         
       ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id          
       and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no          
       and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id          
        and cds_cd=orh_ord_typ AND  
        ord_cmpy_id = cht_cmpy_id   AND  ord_ord_pfx = cht_ref_pfx   AND  ord_ord_no  = cht_ref_no    AND  ord_ord_itm = cht_ref_itm  
        and ou.slp_slp=orh_tkn_slp and usr_lgn_id = ou.slp_lgn_id   
        and cht_tot_typ = ''T''            
       and orl_bal_qty >= 0           
       and chl_chrg_cl= ''E''          
       and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''          
       and orh_sts_actn <> ''C''          
       and ord_ord_pfx <>''QT''           
       and        
       (       
       (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )      
       or      
       (orl_due_to_dt is null and orh_ord_typ =''J''))      
             
       and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')          
                
        and  ord_ord_brh not in (''SFS'') order by ord_ord_no,ord_ord_itm,orl_due_to_dt'          
       print('test')  ;      
     print(@sqltxt);           
    set @execSQLtxt = @sqltxt;           
       EXEC (@execSQLtxt);          
     END
     
     insert into #tmp1   
     select distinct Salesperson,OrderNo from #tmp       
    where DueDate <= CONVERT(VARCHAR(10), GETDATE()-30, 120) and OrderType = 'Normal Order'
   
   select Salesperson, COUNT(*) as OrderCount from #tmp1 group by Salesperson order by COUNT(*) desc;
   
   drop table #tmp;    
   drop table #tmp1;    
       
END          
          
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@CustomerID varchar(10) = ''          
          
-- exec [sp_itech_SalesOffice_OpenOrder] '11/22/2016', '11/22/2018' , 'UK','ALL'        
-- exec [sp_itech_SalesOffice_OpenOrder] '05/01/2015', '05/31/2015' , 'ALL','ALL'
GO
