USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_potpor]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Mukesh     
-- Create date: Aug 03, 2017    
-- Description: <Description,,>    
-- =============================================    
create PROCEDURE [dbo].[US_potpor]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.US_potpor_rec', 'U') IS NOT NULL    
  drop table dbo.US_potpor_rec;    
        
            
SELECT *    
into  dbo.US_potpor_rec    
FROM [LIVEUSSTX].[liveusstxdb].[informix].[potpor_rec];    
    
END    
--  exec  US_potpor    
-- select * from US_potpor_rec
GO
