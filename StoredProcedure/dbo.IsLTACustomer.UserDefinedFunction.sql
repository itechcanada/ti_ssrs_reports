USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[IsLTACustomer]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsLTACustomer] (@custID Varchar(50), @branch Varchar(3)) Returns  int

AS
BEGIN
	Return (Select COUNT(*) from US_LTA_Customer where Rtrim(Ltrim(customer_id)) = @custID and branch = @branch)   

	-- Return the result of the function
	

END
GO
