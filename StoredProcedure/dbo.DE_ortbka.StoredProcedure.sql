USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_ortbka]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  iTECH     
-- Create date: Nov 18, 2020    
-- Description: <Description, Germany Booking order>    
-- =============================================    
CREATE PROCEDURE [dbo].[DE_ortbka]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
--IF OBJECT_ID('dbo.DE_ortbka_rec', 'U') IS NOT NULL    
--  drop table dbo.DE_ortbka_rec;    

--SELECT *    
--into  dbo.DE_ortbka_rec    
--FROM [LIVEDESTX].[livedestxdb].[informix].[ortbka_rec];  

truncate table DE_ortbka_rec;
-- Insert statements for procedure here    
insert into DE_ortbka_rec
select * from [LIVEDESTX].[livedestxdb].[informix].[ortbka_rec];  
	
END    
--- exec DE_ortbka    
-- select * from DE_ortbka_rec    
GO
