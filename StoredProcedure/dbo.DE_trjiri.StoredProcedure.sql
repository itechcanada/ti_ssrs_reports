USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_trjiri]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Sumit>                      
-- Create date: <10/27/2020>                      
-- Description: <Germany Inventory Transaction History Journal – Receipt>                     
-- =============================================                      
create PROCEDURE [dbo].[DE_trjiri]                      
AS                      
BEGIN                      
 -- SET NOCOUNT ON added to prevent extra result sets from                      
 -- interfering with SELECT statements.                      
 SET NOCOUNT ON;                      
IF OBJECT_ID('dbo.DE_trjiri_rec', 'U') IS NOT NULL                    
  drop table dbo.DE_trjiri_rec;                    
  
select iri_cmpy_id, iri_ref_pfx, iri_ref_no, iri_ref_itm, iri_actvy_dt, iri_pur_cat, iri_sls_cat, iri_prnt_pfx, iri_prnt_no, iri_prnt_itm, iri_prnt_sitm  
into dbo.DE_trjiri_rec   
from [LIVEDESTX].[livedestxdb].[informix].trjiri_rec  
                      
END                      
      
/*      
  
 */  
GO
