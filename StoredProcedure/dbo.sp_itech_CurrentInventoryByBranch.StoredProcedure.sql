USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CurrentInventoryByBranch]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  <Mukesh >                
-- Create date: <29 Mar 2017>                
-- Description: <Get Current inventory results >               
            
              
-- =============================================                
CREATE PROCEDURE [dbo].[sp_itech_CurrentInventoryByBranch] @DBNAME varchar(50) ,@Branch Varchar(3) , @InventoryStatus Varchar(3) = 'S'                   
                
AS                
BEGIN                
 SET NOCOUNT ON;  
               
declare @sqltxt varchar(6000)                
declare @execSQLtxt varchar(7000)                
declare @DB varchar(100)                
DECLARE @ExchangeRate varchar(15)           
DECLARE @CurrenyRate varchar(15)             
                
set @DB=  @DBNAME    


IF @Branch = 'ALL'
BEGIN
SET @Branch = ''
END     

if @InventoryStatus = 'ALL'
Begin
set @InventoryStatus = ''
End
              
CREATE TABLE #tmp (   [Database]   VARCHAR(10)      
  , Branch     Varchar(3)            
        , OhdWgt DECIMAL(20, 0)    
        , OpenPOWgt DECIMAL(20, 0)                
         , AvailableWeight Decimal (20,0)                                
                 );                 
                 
DECLARE @DatabaseName VARCHAR(35);                
DECLARE @Prefix VARCHAR(35);                
DECLARE @Name VARCHAR(15);          
     
  
              
                
IF @DBNAME = 'ALL'                
 BEGIN                
     DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
     WHILE @@FETCH_STATUS = 0                
       BEGIN                
        DECLARE @query NVARCHAR(max);                   
      SET @DB= @Prefix         
    
               
      IF (UPPER(@DB) = 'TW')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
   End                  
   Else if (UPPER(@DB) = 'NO')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
   End                  
   Else if (UPPER(@DB) = 'CA')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
   End                  
   Else if (UPPER(@DB) = 'CN')                  
   begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
 End                  
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
   begin            
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
   End                  
   Else if(UPPER(@DB) = 'UK')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
   End       
   Else if(UPPER(@DB) = 'DE')                  
   begin                  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
   End       
       
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
   Begin         
                  
       SET @query ='  
     INSERT INTO #tmp ([Database], Branch, OhdWgt, AvailableWeight, OpenPOWgt)                
   select ''' +  @DB + '''  as [Database], replace(prd_brh, ''SFS'',''LAX''), sum(prd_ohd_wgt * 2.20462 ) , 
   (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )* 2.20462 ) as availableWgt ,
       (select sum(potpod_rec.pod_bal_wgt * 2.20462 ) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' 
        left join ' + @DB + '_arrcus_rec on cus_cmpy_id = pod_cmpy_id and cus_cus_id = pod_bgt_for_cus_id
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat where ipd_FRM=PRd_FRM   and (cuc_desc30<>''Interco'' or cuc_desc30 is null )               
       and ipd_GRD = PRd_GRD and ipd_size = prd_size and ipd_fnsh = prd_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec 
       where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  
                      
       from '+ @DB + '_intprd_rec   where (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  and (prd_Invt_Sts = '''+ @InventoryStatus +''' or '''+ @InventoryStatus +'''= '''')
       group by prd_brh,PRd_FRM, Prd_GRD,PRd_size,PRd_fnsh'                                         
       
   End  
   Else   
   Begin  
  set @query='  
     INSERT INTO #tmp ([Database], Branch, OhdWgt, AvailableWeight, OpenPOWgt)                
   select ''' +  @DB + '''  as [Database], replace(prd_brh, ''SFS'',''LAX''), sum(prd_ohd_wgt ) , 
   (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as availableWgt ,
       (select sum(potpod_rec.pod_bal_wgt ) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C''  
        left join ' + @DB + '_arrcus_rec on cus_cmpy_id = pod_cmpy_id and cus_cus_id = pod_bgt_for_cus_id
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat where ipd_FRM=PRd_FRM   and (cuc_desc30<>''Interco'' or cuc_desc30 is null )               
       and ipd_GRD = PRd_GRD and ipd_size = prd_size and ipd_fnsh = prd_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec 
       where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  
                      
       from '+ @DB + '_intprd_rec   where (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  and (prd_Invt_Sts = '''+ @InventoryStatus +''' or '''+ @InventoryStatus +'''= '''')
       group by prd_brh,PRd_FRM, Prd_GRD,PRd_size,PRd_fnsh'                
   End    
      print @query;                
        EXECUTE sp_executesql @query;                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                
       END                 
    CLOSE ScopeCursor;                
    DEALLOCATE ScopeCursor;                
  END    
  ELSE                
     BEGIN            
        
   IF (UPPER(@DB) = 'TW')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                  
    End                  
    Else if (UPPER(@DB) = 'NO')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                  
    End                  
    Else if (UPPER(@DB) = 'CA')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                  
    End                  
    Else if (UPPER(@DB) = 'CN')                  
    begin                  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                  
    End                  
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                  
    begin                  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                  
    End                  
    Else if(UPPER(@DB) = 'UK')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                  
    End        
    Else if(UPPER(@DB) = 'DE')                  
    begin                  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                  
    End        
               
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                 
   Begin         
                  
       SET @sqltxt ='INSERT INTO #tmp ([Database], Branch, OhdWgt, AvailableWeight, OpenPOWgt)                
       select ''' +  @DB + '''  as [Database], replace(prd_brh, ''SFS'',''LAX''), sum(prd_ohd_wgt * 2.20462 ) , 
   (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )* 2.20462 ) as availableWgt ,
       (select sum(potpod_rec.pod_bal_wgt * 2.20462 ) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' 
       left join ' + @DB + '_arrcus_rec on cus_cmpy_id = pod_cmpy_id and cus_cus_id = pod_bgt_for_cus_id
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat where ipd_FRM=PRd_FRM   and (cuc_desc30<>''Interco'' or cuc_desc30 is null ) 
       and ipd_GRD = PRd_GRD and ipd_size = prd_size and ipd_fnsh = prd_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec 
       where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  
                      
       from '+ @DB + '_intprd_rec   where (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  and (prd_Invt_Sts = '''+ @InventoryStatus +''' or '''+ @InventoryStatus +'''= '''')
       group by prd_brh,PRd_FRM, Prd_GRD,PRd_size,PRd_fnsh'                              
   End                
   Else                
   Begin        
      SET @sqltxt ='INSERT INTO #tmp ([Database], Branch, OhdWgt, AvailableWeight, OpenPOWgt)                
   select ''' +  @DB + '''  as [Database], replace(prd_brh, ''SFS'',''LAX''), sum(prd_ohd_wgt ) , 
   (Sum(prd_ohd_wgt - (prd_qte_res_wgt + prd_ord_res_wgt + prd_prod_res_wgt + prd_shp_res_wgt) )) as availableWgt ,
       (select sum(potpod_rec.pod_bal_wgt ) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec                
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx                
       and pod_trcomp_sts <> ''C'' 
       left join ' + @DB + '_arrcus_rec on cus_cmpy_id = pod_cmpy_id and cus_cus_id = pod_bgt_for_cus_id
       left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat where ipd_FRM=PRd_FRM   and (cuc_desc30<>''Interco'' or cuc_desc30 is null )              
       and ipd_GRD = PRd_GRD and ipd_size = prd_size and ipd_fnsh = prd_fnsh and pod_ven_id not in (Select ixv_ven_id from ' + @DB + '_xcrixv_rec 
       where ixv_cmpy_id = pod_cmpy_id and ixv_actv = 1)) as OpenPOWgt  
                      
       from '+ @DB + '_intprd_rec  where (prd_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''') and (prd_Invt_Sts = '''+ @InventoryStatus +''' or '''+ @InventoryStatus +'''= '''')
       group by prd_brh,PRd_FRM, Prd_GRD,PRd_size,PRd_fnsh'            
   End                
                   
                    
                          
     print( @sqltxt)                 
    set @execSQLtxt = @sqltxt;                 
   EXEC (@execSQLtxt);                
   END                
    
select [Database], Branch, SUM(OhdWgt) as OhdWgt, SUM(AvailableWeight) as AvailableWeight, SUM(OpenPOWgt) as OpenPOWgt from #tmp 
where ([Database]!='US' or BRANCH not in ('TAI','MTL') )
group by  [Database],Branch ;             
                                 
   drop table #tmp  ;     
END                
-- exec [sp_itech_CurrentInventoryByBranch] 'US', 'ALL' ;    
-- exec [sp_itech_CurrentInventoryByBranch] 'US' , 'DET' ;
GO
