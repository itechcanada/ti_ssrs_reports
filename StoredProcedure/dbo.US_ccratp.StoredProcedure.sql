USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ccratp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,June 28, 2015,>  
-- Description: <Description,Open Orders,>  
-- =============================================  
CREATE PROCEDURE [dbo].[US_ccratp]  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
--     IF OBJECT_ID('dbo.US_ccratp_rec', 'U') IS NOT NULL  
--  drop table dbo.US_ccratp_rec ;   
--    -- Insert statements for procedure here  
--SELECT *  into  dbo.US_ccratp_rec  
--  from [LIVEUSSTX].[liveusstxdb].[informix].[ccratp_rec] ;   
Truncate table dbo.US_ccratp_rec ;

Insert into dbo.US_ccratp_rec 
SELECT *  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ccratp_rec] ;  
    
END  
-- select * from US_ccratp_rec   
/*
20201014	Sumit
comment table drop and select statement, add truncate and select statement to keep table index 
*/

GO
