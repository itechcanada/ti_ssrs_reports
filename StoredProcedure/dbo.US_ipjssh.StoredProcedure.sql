USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ipjssh]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Aug 6, 2015,>    
-- Description: <Description,gl account,>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[US_ipjssh]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  --   IF OBJECT_ID('dbo.US_ipjssh_rec', 'U') IS NOT NULL    
  --drop table dbo.US_ipjssh_rec;     
-- Insert statements for procedure here    
--SELECT * into  dbo.US_ipjssh_rec from [LIVEUSSTX].[liveusstxdb].[informix].[ipjssh_rec];     
Truncate table dbo.US_ipjssh_rec;
-- Insert statements for procedure here    
Insert into dbo.US_ipjssh_rec select * from [LIVEUSSTX].[liveusstxdb].[informix].[ipjssh_rec];

END    
/*
20201105	Sumit
Comment drop statement, implement truncate and insert statement
*/
-- select * from US_ipjssh_rec  
GO
