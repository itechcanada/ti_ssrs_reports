USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReportSUMMARY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <11 Feb 2013>  
-- Description: <Getting top 50 customers for SSRS reports> 
-- Last Changes By: Mukesh
--Last changes date 10 Nov 2014
-- To use this report in Technical Name ([sp_itech_MonthlyInventoryReportSUMMARY].rdl) 
-- Last changes : Add new column MatGroup for the purpose of to differenciate "HPM" an "Titanium". 
-- Last changes Date 2014-12-08 Modified the Quer for Stock value sum

-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReportSUMMARY] @DBNAME varchar(50), @InventoryStatus varchar(10)   
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @12FD varchar(10)  
declare @3FD varchar(10)  
declare @6FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15) 
DECLARE @CurrenyRate varchar(15) 
  
set @DB=  @DBNAME  
--set @FD = select CONVERT(VARCHAR(10), DateAdd(mm, -12, GetDate()) , 120)  
  
  
set @3FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 3, 0) , 120)   --First day of previous 3 month  
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)   --First day of previous 6 month  
set @12FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month  
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)    ---Last day of previous month  
  
CREATE TABLE #tmp (   [Database]   VARCHAR(10)  
        , Form     Varchar(65)  
        , Grade     Varchar(65)  
        , Size     Varchar(65)  
        , Finish    Varchar(65)  
        , Months3InvoicedWeight   DECIMAL(20, 2)  
        , Months6InvoicedWeight   DECIMAL(20, 2)  
        , Months12InvoicedWeight   DECIMAL(20, 2)  
        , OpenSOWgt DECIMAL(20, 2)  
        , OpenPOWgt DECIMAL(20, 2)  
                     , Avg3Month     DECIMAL(20, 2)  
                     ,OhdStock DECIMAL(20, 2)  
                     ,OhdStockCost DECIMAL(20, 2)  
                       
                      
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
    OPEN ScopeCursor;  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);     
      SET @DB= @Prefix   
      
      IF (UPPER(@DB) = 'TW')                
   begin                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
   End                
   Else if (UPPER(@DB) = 'NO')                
   begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
   End                
   Else if (UPPER(@DB) = 'CA')                
   begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
   End                
   Else if (UPPER(@DB) = 'CN')                
   begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
 End                
   Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                
   begin          
    SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
   End                
   Else if(UPPER(@DB) = 'UK')                
   begin                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
   End  
   Else if(UPPER(@DB) = 'DE')                
   begin                
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
   End  
   
      if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')   
        Begin  
           SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost )  
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, '''', '''',  
         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD   
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,  
         
       (select sum(potpod_rec.pod_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx   
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM   
       and ipd_GRD = PRm_GRD) as OpenPOWgt,  
         
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec   
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm   
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and   
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''   
       where ipd_FRM=PRm_FRM 
       and ipd_GRD = PRm_GRD )  as OpenSOWgt,  
         
       (Select sum(SAT_BLG_WGT * 2.20462)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,  
       sum(prd_ohd_wgt * 2.20462)   ,  
       
        isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))
         
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and  prd_fnsh = prm_fnsh  
       left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool  
       Where  prd_invt_sts = ''S'' 
       group by PRm_FRM, PRm_GRD;'  
        End  
        Else  
        Begin  
            SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost )  
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, '''', '''' ,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,  
         
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx   
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM  
       and ipd_GRD = PRm_GRD) as OpenPOWgt,  
         
       (select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec   
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm   
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and   
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''   
       where ipd_FRM=PRm_FRM 
       and ipd_GRD = PRm_GRD )  as OpenSOWgt,  
         
       (Select sum(SAT_BLG_WGT)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,  
       sum(prd_ohd_wgt)   ,   
       
      isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))
         
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size    and prd_fnsh = prm_fnsh  
       left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool  
       Where  prd_invt_sts = ''S'' 
       group by PRm_FRM, PRm_GRD;'  
        End  
         
      print @query;  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN  
     IF (UPPER(@DB) = 'TW')                
    begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@DB) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@DB) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@DB) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@DB) = 'UK')                
    begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End    
    Else if(UPPER(@DB) = 'DE')                
    begin                
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End    
    
   if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')   
   Begin  
       SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size, Finish, Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost )  
                   select ''' +  @DB + '''  as [Database], PRm_FRM as Form, PRm_GRD as Grade, '''' as Size,'''' as Finish,    
         
       (Select  SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT * 2.20462) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,  
         
       (select sum(potpod_rec.pod_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx   
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM  
       and ipd_GRD = PRm_GRD ) as OpenPOWgt,  
         
       (select SUM(ortord_rec.ord_bal_wgt * 2.20462) from ' + @DB + '_tctipd_rec   
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm   
       and ord_ord_pfx = ipd_ref_pfx  join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and   
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''   
       where  ipd_FRM=PRm_FRM 
       and ipd_GRD = PRm_GRD )  as OpenSOWgt,  
         
       (Select sum(SAT_BLG_WGT * 2.20462)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD 
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,  
       sum(prd_ohd_wgt * 2.20462)   ,  
       
       isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))
         
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size   and prd_fnsh = prm_fnsh         
       left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool  
       Where  prd_invt_sts = ''S'' 
       group by PRm_FRM, Prm_GRD'  
   End  
   Else  
   Begin  
        SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,OpenPOWgt,OpenSOWgt,Avg3Month, OhdStock, OhdStockCost )  
                   select ''' +  @DB + ''' as [Database], PRm_FRM as Form, PRm_GRD as Grade, '''', '''' ,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD   
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Months3InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @6FD +''' and '''+ @TD +''' ) as Months6InvoicedWeight,  
         
       (Select SUM(SAT_BLG_WGT) from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @12FD +''' and '''+ @TD +''' ) as Months12InvoicedWeight,  
         
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx   
       and pod_trcomp_sts <> ''C'' where ipd_FRM=PRm_FRM 
       and ipd_GRD = PRm_GRD) as OpenPOWgt,  
         
       (select SUM(ortord_rec.ord_bal_wgt) from ' + @DB + '_tctipd_rec   
       join ' + @DB + '_ortord_rec as ortord_rec on ord_cmpy_id =ipd_cmpy_id and ord_ord_no = ipd_ref_no and ipd_ref_itm = ord_ord_itm   
       and ord_ord_pfx = ipd_ref_pfx join  ' + @DB + '_ortorh_rec as ortorh_rec on orh_cmpy_id =ipd_cmpy_id and orh_ord_no = ipd_ref_no and   
       orh_ord_pfx = ipd_ref_pfx and orh_ord_typ = ''N''   
       where ipd_FRM=PRm_FRM
       and ipd_GRD = PRm_GRD )  as OpenSOWgt,  
         
       (Select sum(SAT_BLG_WGT)/3 from ' + @DB + '_sahsat_rec where PRm_FRM = SAT_FRM and PRm_GRD = SAT_GRD  
       and SAT_INV_DT  between '''+ @3FD +''' and '''+ @TD +''' ) as Avg3Month,  
       sum(prd_ohd_wgt)   ,  
        
       isnull (SUM(prd_ohd_mat_val * '+ @CurrenyRate +') + sum(prd_ohd_mat_val + (case when acp_tot_qty = 0 then 0 else  ((prd_ohd_qty * isnull(acp_tot_mat_val,0)) / acp_tot_qty) end) * '+ @CurrenyRate +' ), SUM(prd_ohd_mat_val * '+ @CurrenyRate +'))
       
                
       from ' + @DB + '_inrprm_rec left join '+ @DB + '_intprd_rec  on prd_frm = prm_frm and prd_Grd = prm_grd and prd_size = prm_size and prd_fnsh = prm_fnsh  
       left join '+ @DB + '_intacp_rec on acp_cmpy_id = prd_cmpy_id and acp_avg_cst_pool = prd_avg_cst_pool   
       Where  prd_invt_sts = ''S'' 
       group by PRm_FRM, PRm_GRD;'  
   End  
     
      
            
     print(@sqltxt)   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
   END  
       
     
select [Database],Ltrim(Form) + '/'+ Ltrim(Grade)   as 'Product', Size, Finish, 
--select [Database],Ltrim(Form) + '/'+ Ltrim(Grade)   as 'Product', Size, Finish, 
CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,  
 Months3InvoicedWeight,  
 Months6InvoicedWeight,  
 Months12InvoicedWeight,  
 OpenPOWgt,  
 OpenSOWgt,  
 Avg3Month,   
 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',  
 cast(OpenSOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',   
 OhdStock,   
 OhdStockCost,   
 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt'  
 from #tmp   
 WHERE  (  
(@InventoryStatus = 'False')OR  
(@InventoryStatus = 'True'  and OhdStock>0)  
)   
-- and Form = 'NIRD' and Grade = '625 PLUS' and Size = '5.250' 
 order by Product  
                   
   drop table #tmp  
END  
  
--exec [sp_itech_MonthlyInventoryReportSUMMARY] 'ALL',  'True'  
  
  
  
  
  
  
GO
