USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DELIVERY_OTP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Wendy Wang>
-- Create date: <Create Date,12/30/2011,>
-- Description:	<Description,Delivery to customer performance>
-- =============================================
create PROCEDURE [dbo].[DELIVERY_OTP]
	-- Add the parameters for the stored procedure here
	
-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 'USA' as Country, dpf_sprc_pfx,dpf_sprc_no, dpf_ord_pfx,dpf_ord_no, dpf_ord_itm, dpf_ord_brh, dpf_shp_pcs, dpf_shp_wgt,
	       dpf_transp_pfx, dpf_transp_no, dpf_shpg_whs, dpf_shpg_dtts, dpf_rdy_by_dt, dpf_shpg_dtms, dpf_lte_shpt, dpf_rsn_typ, dpf_rsn, dpf_rsn_rmk, 
	       dpf_lgn_id, dpf_crtd_dtts, dpf_crtd_dtms, dpf_smry_upd_flg
	  FROM [US_pfhdpf_rec]
	  WHERE CAST(dpf_shpg_dtts AS datetime) BETWEEN @FromDate AND @ToDate
UNION
SELECT 'UK' as Country, dpf_sprc_pfx,dpf_sprc_no, dpf_ord_pfx, dpf_ord_no,dpf_ord_itm, dpf_ord_brh, dpf_shp_pcs, dpf_shp_wgt,
	       dpf_transp_pfx, dpf_transp_no, dpf_shpg_whs, dpf_shpg_dtts, dpf_rdy_by_dt, dpf_shpg_dtms, dpf_lte_shpt, dpf_rsn_typ, dpf_rsn, dpf_rsn_rmk, 
	       dpf_lgn_id, dpf_crtd_dtts, dpf_crtd_dtms, dpf_smry_upd_flg
	  FROM [UK_pfhdpf_rec]
	  WHERE CAST(dpf_shpg_dtts AS datetime) BETWEEN @FromDate AND @ToDate
UNION
SELECT 'CANADA' as Country, dpf_sprc_pfx,dpf_sprc_no, dpf_ord_pfx,dpf_ord_no, dpf_ord_itm, dpf_ord_brh, dpf_shp_pcs, dpf_shp_wgt,
	       dpf_transp_pfx, dpf_transp_no, dpf_shpg_whs, dpf_shpg_dtts, dpf_rdy_by_dt, dpf_shpg_dtms, dpf_lte_shpt, dpf_rsn_typ, dpf_rsn, dpf_rsn_rmk, 
	       dpf_lgn_id, dpf_crtd_dtts, dpf_crtd_dtms, dpf_smry_upd_flg
	  FROM [CA_pfhdpf_rec]
	  WHERE CAST(dpf_shpg_dtts AS datetime) BETWEEN @FromDate AND @ToDate
UNION
SELECT 'TAIWAN' as Country, dpf_sprc_pfx,dpf_sprc_no, dpf_ord_pfx, dpf_ord_no,dpf_ord_itm, dpf_ord_brh, dpf_shp_pcs, dpf_shp_wgt,
	       dpf_transp_pfx, dpf_transp_no, dpf_shpg_whs, dpf_shpg_dtts, dpf_rdy_by_dt, dpf_shpg_dtms, dpf_lte_shpt, dpf_rsn_typ, dpf_rsn, dpf_rsn_rmk, 
	       dpf_lgn_id, dpf_crtd_dtts, dpf_crtd_dtms, dpf_smry_upd_flg
 	  FROM [TW_pfhdpf_rec]
      WHERE CAST(dpf_shpg_dtts AS datetime) BETWEEN @FromDate AND @ToDate
UNION
SELECT 'NORWAY' as Country, dpf_sprc_pfx,dpf_sprc_no, dpf_ord_pfx, dpf_ord_no,dpf_ord_itm, dpf_ord_brh, dpf_shp_pcs, dpf_shp_wgt,
	       dpf_transp_pfx, dpf_transp_no, dpf_shpg_whs, dpf_shpg_dtts, dpf_rdy_by_dt, dpf_shpg_dtms, dpf_lte_shpt, dpf_rsn_typ, dpf_rsn, dpf_rsn_rmk, 
	       dpf_lgn_id, dpf_crtd_dtts, dpf_crtd_dtms, dpf_smry_upd_flg
 	  FROM [NO_pfhdpf_rec]
      WHERE CAST(dpf_shpg_dtts AS datetime) BETWEEN @FromDate AND @ToDate

END

GO
