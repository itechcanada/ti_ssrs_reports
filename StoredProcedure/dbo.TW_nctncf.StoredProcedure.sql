USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_nctncf]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mukesh>  
-- Create date: <Create Date,Oct 05, 2015,>  
-- Description: <Description,NCR,>  
  
-- =============================================  
create PROCEDURE [dbo].[TW_nctncf]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_nctncf_rec', 'U') IS NOT NULL  
  drop table dbo.TW_nctncf_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_nctncf_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[nctncf_rec];   
    
END  
-- select * from TW_nctncf_rec
GO
