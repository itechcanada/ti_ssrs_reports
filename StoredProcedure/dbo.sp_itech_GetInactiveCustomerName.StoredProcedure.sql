USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetInactiveCustomerName]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
  
  
-- =============================================    
-- Author:  <Mayank >    
-- Create date: <11 Feb 2013>    
-- Description: <Getting top 50 customers for SSRS reports>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_GetInactiveCustomerName]  @DBNAME varchar(50) , @FromDate datetime, @ToDate datetime    
AS    
BEGIN    
     
SET NOCOUNT ON;    
declare @sqltxt varchar(8000)    
--declare @execSQLtxt varchar(7000)    
declare @DB varchar(100)    
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
declare @FDate varchar(10)  
declare @TDate varchar(10)  
    
CREATE TABLE #tmp (    
 DBPrefix varchar(8),  
 CustID varchar(15) ,   
 CustNm varchar(50),    
    UpdateStatus Varchar(3),    
    StatusVal varchar(1),    
    StatusUpdatedOn Date,    
    StatusUpdatedBy varchar(10)    
    )    
  
 
 set @FDate = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @FromDate), 0) , 120)           
 set @TDate = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ToDate)+1,0)),120);  
 print 'DB: '+ @DBNAME +' FromDate: '+ @FDate +' ToDate: '+ @TDate;  
    
 IF @DBNAME = 'ALL'    
 BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
  select DatabaseName,Prefix from tbl_itech_DatabaseName    
  OPEN ScopeCursor;    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;    
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
   --DECLARE @query NVARCHAR(4000);       
         
   SET @sqltxt ='INSERT INTO #tmp ( DBPrefix, CustID,CustNm, UpdateStatus,StatusVal,StatusUpdatedOn,StatusUpdatedBy)    
   select '''+ @Prefix +''', SUBSTRING(CONVERT(VARCHAR(MAX), pal_rec_txt),4,8) , cus_cus_long_nm,    
   pal_fct, right(CONVERT(VARCHAR(MAX), pal_rec_txt),1), pal_upd_dtts, pal_lgn_id from '+ @Prefix +'_sctpal_rec     
   join '+ @Prefix +'_arrcus_rec on cus_cmpy_id=pal_cmpy_id and cus_cus_id= SUBSTRING(CONVERT(VARCHAR(MAX), pal_rec_txt),4,8)    
   and cus_actv=0    
   where pal_tbl_nm=''arrcus''     
   and right(CONVERT(VARCHAR(MAX), pal_rec_txt),1) =''0''  and pal_fct=''UPD''  
   and convert(varchar(10), pal_upd_dtts,120) between '''+ @FDate +''' and '''+ @TDate +''';'    
   print(@sqltxt);    
   --EXECUTE sp_executesql @query;    
   EXEC (@sqltxt);    
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;    
  END     
  CLOSE ScopeCursor;    
  DEALLOCATE ScopeCursor;    
 END    
 ELSE    
 BEGIN        
  SET @sqltxt ='INSERT INTO #tmp ( DBPrefix, CustID,CustNm,UpdateStatus,StatusVal,StatusUpdatedOn,StatusUpdatedBy)    
  select '''+ @DBNAME +''', SUBSTRING(CONVERT(VARCHAR(MAX), pal_rec_txt),4,8) , cus_cus_long_nm,    
  pal_fct, right(CONVERT(VARCHAR(MAX), pal_rec_txt),1), pal_upd_dtts, pal_lgn_id from '+ @DBNAME +'_sctpal_rec     
  join '+ @DBNAME +'_arrcus_rec on cus_cmpy_id=pal_cmpy_id and cus_cus_id= SUBSTRING(CONVERT(VARCHAR(MAX), pal_rec_txt),4,8)    
  and cus_actv=0    
  where pal_tbl_nm=''arrcus''     
  and right(CONVERT(VARCHAR(MAX), pal_rec_txt),1) =''0'' and pal_fct=''UPD''   
  and convert(varchar(10), pal_upd_dtts,120) between '''+ @FDate +''' and '''+ @TDate +''';'    
  print(@sqltxt);     
  --set @execSQLtxt = @sqltxt;     
  EXEC (@sqltxt);    
 End    
--BEGIN    
 
 -- select distinct * from #tmp order by DBPrefix, StatusUpdatedOn desc;    
 select distinct DBPrefix, CustID,CustNm,UpdateStatus,StatusVal,convert(varchar(10), StatusUpdatedOn,103) as StatusUpdatedOn,StatusUpdatedBy   
 from #tmp order by DBPrefix, StatusUpdatedOn desc;  
 
  --print(@sqltxt);     
  --  set @execSQLtxt = @sqltxt;     
  -- EXEC (@execSQLtxt);    
    
    drop table  #tmp    
--END    
END    
/*  
exec sp_iTECH_GetInactiveCustomerName 'US','09-01-2021','09-30-2021'  
  
*/    
-- exec [sp_itech_GetInactiveCustomerName]  'ALL'    
--select TOP 10 * from US_arrcus_rec    
--select cus_cus_id as 'Value',cus_cus_nm as 'text', 'C' as temp  from  CA_arrcus_rec    
--       Union    
--       Select CAST(id as varchar(500)) as 'Value',UPPER(contractor_cust_nm) as 'text',    
--       'B' as temp from tbl_itech_CA_Sub_Contactor    
--       Union     
--       Select 'ALL' as 'Value','All Customer' as 'text','A' as temp    
--       Order by temp,text    
    
-- select * from us_arrcus_rec where cus_cus_id='10567'     
    
    
    
GO
