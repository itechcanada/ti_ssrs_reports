USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ProfitLoss_DetailedExpenses]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Mukesh >                  
-- Create date: <12 Jul 2017>                  
-- Description: <Getting ProfitLoss>                  
-- =============================================                  
CREATE  PROCEDURE [dbo].[sp_itech_ProfitLoss_DetailedExpenses] @DBNAME varchar(50), @GLSAcct Varchar(35),@MonthYear Date             
AS                  
BEGIN             
               
SET NOCOUNT ON;                  
                  
--declare @sqltxt varchar(6000)                  
declare @execSQLtxt varchar(7000)                  
DECLARE @CurrenyRate varchar(15);           
DECLARE @StartDate VARCHAR(10);           
DECLARE @plCompanyID VARCHAR(3);          
            
CREATE TABLE #tmp (   CompanyName   VARCHAR(10)             
  ,CompanyID   VARCHAR(10)               
        , BscGLAcc     VARCHAR(10)                  
        , BscGLSubAcc     VARCHAR(35)         
        , AccountDescription    Varchar(100)           
        , AccountCategoryID Varchar(5)                 
        , GroupName  Varchar(50)          
        , AccountAmt  Decimal(20,2)              
        , BudgetAmt    Decimal(20,2)             
        , MonthYTD    Varchar(5)                  
                 );                  
                               
  if @GLSAcct = 'ALL'        
  begin        
  set @GLSAcct = ''        
  End        
                                   
DECLARE @company VARCHAR(35);                  
DECLARE @prefix VARCHAR(15);                   
DECLARE @DatabaseName VARCHAR(35);            
          
--------  +++++  Created Table for last up to current months ++++++++              
Create TABLE #Last12Months ( StartDate varchar(15) );       
    
if(Month(@MonthYear) = 1)    
begin     
declare @start1 DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));       
 with CTEE(Last12MonthDate)              
AS              
(              
    SELECT @start1              
         
)              
INSERT INTO #Last12Months(StartDate)              
select Last12MonthDate as StartDate  from CTEE                        
    
End           
 else if (Month(@MonthYear) = 2)    
 begin    
 declare @start2 DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));       
 with CTEE(Last12MonthDate)              
AS              
(              
    SELECT @start2    
     UNION   all              
              
    SELECT DATEADD(month,-1,@start2)              
               
)              
INSERT INTO #Last12Months(StartDate)              
select Last12MonthDate as StartDate  from CTEE                        
    
 END      
 else    
 begin           
declare @start DATE = CONVERT(VARCHAR(10), CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, @MonthYear), -1) , 120));       
 with CTEE(Last12MonthDate)              
AS              
(              
    SELECT @start              
     UNION   all              
              
    SELECT DATEADD(month,-1,Last12MonthDate)              
    from CTEE              
    where DATEADD(month,-2,Last12MonthDate)>=DATEADD(month,-MONTH(@MonthYear)-1,@start)  -- to set no of month            
)              
INSERT INTO #Last12Months(StartDate)              
select Last12MonthDate as StartDate  from CTEE                        
end                         
                         
 IF @DBNAME = 'ALL'                  
 BEGIN                  
  DECLARE ScopeCursor CURSOR FOR                  
   select DatabaseName, company,prefix from tbl_itech_DatabaseName                  
    OPEN ScopeCursor;                  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                  
     WHILE @@FETCH_STATUS = 0                  
       BEGIN                  
        DECLARE @query NVARCHAR(4000);              
         IF (UPPER(@Prefix) = 'TW')              
    begin              
    set @plCompanyID = 'TWS';         
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@Prefix) = 'NO')         
                 
    begin       
    set @plCompanyID = 'NOS';              
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@Prefix) = 'CA')              
    begin           
    set @plCompanyID = 'CAS';          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@Prefix) = 'CN')              
    begin        
    set @plCompanyID = 'CNS';             
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')              
    begin       
    set @plCompanyID = 'USS';              
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End          
    Else if(UPPER(@Prefix) = 'UK')              
    begin          
    set @plCompanyID = 'UKS';            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End
	Else if(UPPER(@Prefix) = 'DE')              
    begin          
    set @plCompanyID = 'DES';            
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End 
                       
       DECLARE ScopeCursorin CURSOR FOR              
       select StartDate from  #Last12Months              
        OPEN ScopeCursorin;              
        FETCH NEXT FROM ScopeCursorin INTO @StartDate;              
         WHILE @@FETCH_STATUS = 0              
           BEGIN              
            DECLARE @sqltxt NVARCHAR(max);           
               print @StartDate          
               print @prefix          
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD)            
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,gld_sacct,plAccountDescription, plAccountCategoryID,          
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,          
 (sum(isnull(gld_cr_amt,0)) - sum(ISNULL(gld_dr_amt,0))) as Account,        
 (select sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) from '+ @prefix +'_glbbud_rec where bud_bsc_gl_acct = plBasicGLAccount and bud_sacct = gld_sacct    
 
  and      
  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '  ) as budget ,          
 ''' + convert(varchar(2),MONTH(@StartDate)) + '''          
      from '+ @prefix +'_glhgld_rec              
      join tbl_itech_PLAccount on plBasicGLAccount = gld_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''         
       where gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '          
       AND (gld_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')         
      group by plcompanyID,plBasicGLAccount,gld_sacct,plAccountDescription,plAccountCategoryID          
             
    union      
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,bud_sacct,plAccountDescription, plAccountCategoryID,          
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,          
 0 as Account,        
 sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) as budget ,          
 ''' + convert(varchar(2),MONTH(@StartDate)) + '''          
      from tbl_itech_PLAccount             
      left join '+ @prefix +'_glbbud_rec on plBasicGLAccount = bud_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''         
       where  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '         
       AND (bud_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')         
       and bud_bsc_gl_acct not in (select gld_bsc_gl_acct from '+ @prefix +'_glhgld_rec Where bud_bsc_gl_acct = gld_bsc_gl_acct and bud_sacct = gld_sacct      
 and gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '  )      
      group by plcompanyID,plBasicGLAccount,bud_sacct,plAccountDescription,plAccountCategoryID          
      order by plBasicGLAccount         
      '              
                      
                        
    print(@sqltxt)                  
   set @execSQLtxt = @sqltxt;                   
 EXEC (@execSQLtxt);             
           
INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD)            
        select @prefix,plcompanyID, plBasicGLAccount,plBasicGLaccount,plAccountDescription,plAccountCategoryID,    
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,     
        0,0,convert(varchar(2),MONTH(@StartDate))    
         from tbl_itech_PLAccount where plBasicGLaccount not in (SElect BscGLAcc from #tmp);    
           
   FETCH NEXT FROM ScopeCursorin INTO @StartDate;              
        END               
        CLOSE ScopeCursorin;              
        DEALLOCATE ScopeCursorin;       
                         
                          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@company,@prefix;                  
       END                   
    CLOSE ScopeCursor;                  
    DEALLOCATE ScopeCursor;                  
  END                  
  ELSE                  
     BEGIN                   
                       
     Set @prefix= @DBNAME              
     IF (UPPER(@DBNAME) = 'TW')              
    begin             
    set @plCompanyID = 'TWS';         
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End              
    Else if (UPPER(@DBNAME) = 'NO')              
    begin           
    set @plCompanyID = 'NOS';           
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))              
    End              
    Else if (UPPER(@DBNAME) = 'CA')              
    begin          
    set @plCompanyID = 'CAS';            
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))              
    End              
    Else if (UPPER(@DBNAME) = 'CN')              
    begin           
    set @plCompanyID = 'CNS';           
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))              
    End              
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')              
    begin           
    set @plCompanyID = 'USS';           
    print 'Mukesh'        
    print @plCompanyID;        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))              
    End              
    Else if(UPPER(@DBNAME) = 'UK')              
    begin           
    set @plCompanyID = 'UKS';           
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))              
    End              
    Else if(UPPER(@DBNAME) = 'TWCN')              
    begin              
       SET @prefix ='TW'          
       set @plCompanyID = 'TWS';            
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))              
    End
	Else if(UPPER(@DBNAME) = 'DE')              
    begin           
		set @plCompanyID = 'DES';           
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))              
    End 
              
         DECLARE ScopeCursor CURSOR FOR              
       select StartDate from  #Last12Months              
        OPEN ScopeCursor;              
        FETCH NEXT FROM ScopeCursor INTO @StartDate;              
         WHILE @@FETCH_STATUS = 0              
           BEGIN              
               print @StartDate          
               print @prefix          
     SET @sqltxt = 'INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD)            
    select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,gld_sacct,plAccountDescription, plAccountCategoryID,          
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,          
 (sum(isnull(gld_cr_amt,0)) - sum(ISNULL(gld_dr_amt,0))) as Account,        
 (select sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) from '+ @prefix +'_glbbud_rec where bud_bsc_gl_acct = plBasicGLAccount and bud_sacct = gld_sacct   
  
  and      
  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '  ) as budget ,          
 ''' + convert(varchar(2),MONTH(@StartDate)) + '''          
      from '+ @prefix +'_glhgld_rec              
      join tbl_itech_PLAccount on plBasicGLAccount = gld_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''         
       where gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '          
       AND (gld_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')         
      group by plcompanyID,plBasicGLAccount,gld_sacct,plAccountDescription,plAccountCategoryID          
             
    union      
     select ''' + @prefix + ''' , plcompanyID, plBasicGLAccount,bud_sacct,plAccountDescription, plAccountCategoryID,          
(select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,          
 0 as Account,        
 sum(ISNULL(bud_cr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) - sum(ISNULL(bud_dr_amt_' + convert(varchar(2),MONTH(@StartDate)) + ',0)) as budget ,          
 ''' + convert(varchar(2),MONTH(@StartDate)) + '''          
      from tbl_itech_PLAccount             
      left join '+ @prefix +'_glbbud_rec on plBasicGLAccount = bud_bsc_gl_acct and plcompanyID = '''+ @plCompanyID + '''         
       where  bud_fis_yr = ' + convert(varchar(4),YEAR(@StartDate)) + '         
       AND (bud_sacct = '''+ @GLSAcct +''' or '''+ @GLSAcct +'''= '''')         
       and bud_bsc_gl_acct not in (select gld_bsc_gl_acct from '+ @prefix +'_glhgld_rec Where bud_bsc_gl_acct = gld_bsc_gl_acct and bud_sacct = gld_sacct      
 and gld_acctg_per = ' + convert(varchar(4),YEAR(getdate())) + right ('0'+ convert(varchar(2),MONTH(@StartDate)),2 ) + '  )      
      group by plcompanyID,plBasicGLAccount,bud_sacct,plAccountDescription,plAccountCategoryID          
      order by plBasicGLAccount         
      '              
                      
                        
    print(@sqltxt)                  
   set @execSQLtxt = @sqltxt;                   
 EXEC (@execSQLtxt);     
     
 INSERT INTO #tmp (CompanyName,CompanyID, BscGLAcc,BscGLSubAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt, MonthYTD)            
        select @prefix,plcompanyID, plBasicGLAccount,plBasicGLaccount,plAccountDescription,plAccountCategoryID,    
        (select accountCategoryShortName from tbl_itech_accountCategory where companyID = plcompanyID and idAccountCategory = plAccountCategoryID) as groupName,     
        0,0,convert(varchar(2),MONTH(@StartDate))    
         from tbl_itech_PLAccount where plBasicGLaccount not in (SElect BscGLAcc from #tmp);            
           
   FETCH NEXT FROM ScopeCursor INTO @StartDate;              
        END               
        CLOSE ScopeCursor;              
        DEALLOCATE ScopeCursor;     
                           
   END            
                   
select CompanyName ,CompanyID , BscGLAcc , BscGLSubAcc , AccountDescription , AccountCategoryID , GroupName , (-1)*AccountAmt as AccountAmt , (-1)*BudgetAmt as BudgetAmt, MonthYTD  from #tmp     
where BscGLAcc --= --6250;    
  in(6110,6111,6115,6120,6121,6125,6200,6201,6202,6204,6206,6210,6220,6230,6240,6242,6244,6245,6248,6249,6250,6256,6260,6265,6268,6270,6272,6275,6280,6290,6292,6294,6297,6299,6300,6310,6315,6318,6320,6321,6322,6325,6330,6350,6360,6365,6385,6387,6390,6391
	, 6395,6420,6430,6440,6450,6460,6470,6475,6476,6478,6480,6520,6521,6524,6526,6530,6535,6538,6550,6560,6565,6570,6572,6580,6585,6590,6595,6599,6615,6620,6625,6640,6650,6660,6725,6730,6735,6740,6745,6750,6790,6795,6796,6798,6810,6820,6830,6990,8010,8100,8105,
	8106,8200,8250,8300,8400,8500,8600,8700,8900,8910,8920,8930,9110,9115,9125,9210,9400);        
    
--select CompanyName,CompanyID, BscGLAcc, AccountDescription, AccountCategoryID, GroupName, AccountAmt, BudgetAmt from (                      
-- SELECT  MonthYTD, Sum(BudgetAmt)  as val ,CompanyName,CompanyID, BscGLAcc, AccountDescription, AccountCategoryID, GroupName from #tmp                      
--     group by MonthYTD,CompanyName,CompanyID, BscGLAcc, AccountDescription, AccountCategoryID, GroupName) as s                      
--     PIVOT                      
--(                      
--    Sum(val)                      
--    FOR MonthYTD IN ( AccountAmt, BudgetAmt)                      
--)AS p --order by MonthYTD            
drop table #tmp  ;          
                  
                  
END                  
                  
-- exec [sp_itech_ProfitLoss_DetailedExpenses]  'US','ALL'              
          
-- exec [sp_itech_ProfitLoss_DetailedExpenses]  'US' ,'810000000000000000000000000000','2017-02-15'    
/*
20210128	Sumit
Add germany Database
*/
GO
