USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetMetalStdSpec]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <30 Oct 2015>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetMetalStdSpec]  

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(5);

CREATE TABLE #tmpPS (  pcrItmCtlNo varchar(15) 
   					,frm Varchar(6)
   					,grd Varchar(8)
   					,tag varchar(12)
   					--,mcrmssCtlNo Varchar(10)
   					--,qmsmssCtlNo Varchar(10)
   					,msssdo		Varchar(6)
   					, mssstdid Varchar(20)
   					, mssaddnlid	Varchar(20) 
   					,qmsmtchgchm	Varchar(2)
   					, qmsmtchgtst	Varchar(2)
   					, qmsmtchgjmy	Varchar(2)
   					)

CREATE TABLE #tmpUS (  pcrItmCtlNo varchar(15) 
   					,frm Varchar(6)
   					,grd Varchar(8)
   					,tag varchar(12)
   					--,mcrmssCtlNo Varchar(10)
   					--,qmsmssCtlNo Varchar(10)
   					,msssdo		Varchar(6)
   					, mssstdid Varchar(20)
   					, mssaddnlid	Varchar(20) 
   					,qmsmtchgchm	Varchar(2)
   					, qmsmtchgtst	Varchar(2)
   					, qmsmtchgjmy	Varchar(2)
   					
   					)
insert into #tmpPS (pcrItmCtlNo,frm,grd,tag,msssdo,mssstdid,mssaddnlid,qmsmtchgchm,qmsmtchgtst,qmsmtchgjmy)
SELECT  prd_itm_ctl_no, prd_frm, prd_grd, prd_tag_no, mss_sdo,mss_std_id,mss_addnl_id, mchqms_rec.qms_mtchg_chm,
mchqms_rec.qms_mtchg_tst, mchqms_rec.qms_mtchg_jmy
FROM PS_intpcr_rec intpcr_rec, PS_intprd_rec intprd_rec, PS_mchqds_rec mchqds_rec, 
PS_mchqms_rec mchqms_rec, 
PS_mcrmss_rec mcrmss_rec
WHERE mchqds_rec.qds_qds_ctl_no = intpcr_rec.pcr_qds_ctl_no AND mchqds_rec.qds_cmpy_id = intpcr_rec.pcr_cmpy_id 
AND intprd_rec.prd_cmpy_id = intpcr_rec.pcr_cmpy_id AND intprd_rec.prd_itm_ctl_no = intpcr_rec.pcr_itm_ctl_no 
AND mchqds_rec.qds_qds_ctl_no = mchqms_rec.qms_qds_ctl_no AND mchqms_rec.qms_cmpy_id = mchqds_rec.qds_cmpy_id 
AND mchqms_rec.qms_mss_ctl_no = mcrmss_rec.mss_mss_ctl_no
and prd_tag_no <> '' and prd_invt_sts <> 'R' and mss_actv = 1;


insert into #tmpUS (pcrItmCtlNo,frm,grd,tag,msssdo,mssstdid,mssaddnlid,qmsmtchgchm,qmsmtchgtst,qmsmtchgjmy)
--SELECT  prd_itm_ctl_no, prd_frm, prd_grd, prd_tag_no, mss_sdo,mss_std_id,mss_addnl_id, mchqms_rec.qms_mtchg_chm,
--mchqms_rec.qms_mtchg_tst, mchqms_rec.qms_mtchg_jmy
--FROM US_intpcr_rec intpcr_rec, US_intprd_rec intprd_rec, US_mchqds_rec mchqds_rec, 
--US_mchqms_rec mchqms_rec, 
--US_mcrmss_rec mcrmss_rec
--WHERE mchqds_rec.qds_qds_ctl_no = intpcr_rec.pcr_qds_ctl_no AND mchqds_rec.qds_cmpy_id = intpcr_rec.pcr_cmpy_id 
--AND intprd_rec.prd_cmpy_id = intpcr_rec.pcr_cmpy_id AND intprd_rec.prd_itm_ctl_no = intpcr_rec.pcr_itm_ctl_no 
--AND mchqds_rec.qds_qds_ctl_no = mchqms_rec.qms_qds_ctl_no AND mchqms_rec.qms_cmpy_id = mchqds_rec.qds_cmpy_id 
--AND mchqms_rec.qms_mss_ctl_no = mcrmss_rec.mss_mss_ctl_no;
select '','','','', mss_sdo,mss_std_id,mss_addnl_id, qms_mtchg_chm,
qms_mtchg_tst, qms_mtchg_jmy
from US_mcrmss_rec mcrmss_rec left join US_mchqms_rec on qms_mss_ctl_no = mss_mss_ctl_no and mss_actv = 1 ;

update  #tmpUS set qmsmtchgchm = 'NA'  where qmsmtchgchm is null;
update  #tmpUS set qmsmtchgtst = 'NA'  where qmsmtchgtst is null;
update  #tmpUS set qmsmtchgjmy = 'NA'  where qmsmtchgjmy is null;


      
   Select distinct 
    ps.tag,
   -- ps.frm, ps.grd, 
   ps.msssdo, ps.mssstdid, ps.mssaddnlid, ps.qmsmtchgchm, ps.qmsmtchgjmy, ps.qmsmtchgtst 
   from   #tmpPS  as ps left join #tmpUS us on 
   -- ltrim(ps.frm) = ltrim(us.frm) and ltrim(ps.grd) = ltrim(us.grd) and 
   rtrim(ltrim(ps.msssdo)) = rtrim(ltrim(us.msssdo)) and rtrim(ltrim(ps.mssstdid)) = rtrim(ltrim(us.mssstdid)) and rtrim(ltrim(ps.mssaddnlid ))=
    rtrim(ltrim(us.mssaddnlid) )
    and rtrim(ltrim(ps.qmsmtchgchm)) = rtrim(ltrim(us.qmsmtchgchm))
    and rtrim(ltrim(ps.qmsmtchgtst)) = rtrim(ltrim(us.qmsmtchgtst)) and rtrim(ltrim(ps.qmsmtchgjmy)) = rtrim(ltrim(us.qmsmtchgjmy) )
   where us.msssdo is  null order by 2,3;
   
   drop table #tmpPS;
   drop table #tmpUS;
     --sdo, std-id , addnl-id, mtchg-chm, mtchg-tst, mtchg-jmy
END

-- exec [sp_itech_GetMetalStdSpec] ;            





GO
