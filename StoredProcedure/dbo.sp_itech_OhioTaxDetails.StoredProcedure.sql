USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OhioTaxDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Mukesh>  
-- Create date: <08 July 2014>  
-- Description: <Ohio Tax details>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_OhioTaxDetails] @DBNAME varchar(50), @InvFromDate datetime, @InvToDate datetime, @TaxRegion varchar(10)   
   
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000);  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
DECLARE @CurrenyRate varchar(15)  
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
DECLARE @Name VARCHAR(15);    
  
set @DB= @DBNAME   
set @FD = CONVERT(VARCHAR(10), @InvFromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @InvToDate , 120)  
  
CREATE TABLE #tmp ( DBName   VARCHAR(10)   
     ,CityState  Varchar(100)    
     ,ShpToCity  Varchar(100)    
     ,TotalSales   DECIMAL(20, 2)    
     ,TaxRegion  Varchar(10)  
        );    
                   
IF @DBNAME = 'ALL'    
 BEGIN    
 DECLARE ScopeCursor CURSOR FOR  
 select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS    
    OPEN ScopeCursor;    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
  WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(4000);     
        IF (UPPER(@Prefix) = 'TW')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
  End      
  Else if (UPPER(@Prefix) = 'NO')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
  End      
  Else if (UPPER(@Prefix) = 'CA')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
  End      
  Else if (UPPER(@Prefix) = 'CN')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
  End      
  Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')      
  begin      
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
  End      
  Else if(UPPER(@Prefix) = 'UK')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
  End
  Else if(UPPER(@Prefix) = 'DE')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
  End

  SET @query =    
       'INSERT INTO #tmp (DBName,TaxRegion, TotalSales,ShpToCity, CityState)    
       SELECT  '''+ @Prefix +''','''+ @TaxRegion +''', Sum(stn_tot_val* '+ @CurrenyRate +'),rtrim(a.cva_city) + '', '' + rtrim(a.cva_st_prov),rtrim(b.cva_city) + '', '' + rtrim(b.cva_st_prov)  
       from ' + @Prefix + '_sahstn_rec join ' + @Prefix + '_scrcva_rec a on a.cva_cus_ven_id = stn_sld_cus_id and a.cva_ref_pfx = ''CS'' and a.cva_cus_ven_typ =''C'' and   
        a.cva_addr_no = stn_shp_to and a.cva_addr_typ = ''S'' and a.cva_st_prov = ''' + @TaxRegion + '''  
  join ' + @Prefix + '_scrcva_rec b on b.cva_cus_ven_id = stn_sld_cus_id and b.cva_ref_pfx = ''CU'' and b.cva_cus_ven_typ =''C'' and   
        b.cva_addr_no = 0 and b.cva_addr_typ = ''L''  
  -- join ' + @Prefix + '_arrstx_rec on stx_cmpy_id = stn_cmpy_id and stx_cus_id = stn_sld_cus_id and stx_shp_to = stn_shp_to and stx_tx_rgn = ''' + @TaxRegion + '''  
  where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' group by rtrim(a.cva_city) + '', '' + rtrim(a.cva_st_prov),rtrim(b.cva_city) + '', '' + rtrim(b.cva_st_prov)'  
        print(@query);    
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
         
   IF (UPPER(@DB) = 'TW')      
  begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))      
  End      
  Else if (UPPER(@DB) = 'NO')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))      
  End      
  Else if (UPPER(@DB) = 'CA')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))      
  End      
  Else if (UPPER(@DB) = 'CN')      
  begin      
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))      
  End      
  Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )      
  begin      
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))      
  End      
  Else if(UPPER(@DB) = 'UK')      
  begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))      
  End
  Else if(UPPER(@DB) = 'DE')      
  begin      
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))      
  End
        
  SET @sqltxt ='INSERT INTO #tmp (DBName,TaxRegion, TotalSales,ShpToCity, CityState)    
       SELECT  '''+ @DB +''','''+ @TaxRegion +''', Sum(stn_tot_val* '+ @CurrenyRate +'),rtrim(a.cva_city) + '', '' + rtrim(a.cva_st_prov), rtrim(b.cva_city) + '', '' + rtrim(b.cva_st_prov)  
       from ' + @DB + '_sahstn_rec join ' + @DB + '_scrcva_rec a on a.cva_cus_ven_id = stn_sld_cus_id and a.cva_ref_pfx = ''CS'' and a.cva_cus_ven_typ =''C'' and   
        a.cva_addr_no = stn_shp_to and a.cva_addr_typ = ''S'' and a.cva_st_prov = ''' + @TaxRegion + '''  
    join ' + @DB + '_scrcva_rec b on b.cva_cus_ven_id = stn_sld_cus_id and b.cva_ref_pfx = ''CU'' and b.cva_cus_ven_typ =''C'' and   
        b.cva_addr_no = 0 and b.cva_addr_typ = ''L''  
  -- join ' + @DB + '_arrstx_rec on stx_cmpy_id = stn_cmpy_id and stx_cus_id = stn_sld_cus_id and stx_shp_to = stn_shp_to and stx_tx_rgn = ''' + @TaxRegion + '''  
  where stn_inv_Dt >= '''+ @FD +''' and stn_inv_dt <= '''+ @TD +''' and stn_frm <> ''XXXX'' group by rtrim(a.cva_city) + '', '' + rtrim(a.cva_st_prov),rtrim(b.cva_city) + '', '' + rtrim(b.cva_st_prov)'  
    
        print(@sqltxt);     
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
     END  
       
     Select * from #tmp order by ShpToCity;  
     drop table #tmp;    
END  
  
--select * from   
--Exec [sp_itech_OhioTaxDetails] 'US','2013-01-01','2013-12-31','OH'  
  
--select nad_ref_pfx, nad_ref_no,*  from  [LIVEUSSTX].[liveusstxdb].[informix].[tctnad_rec] where  (nad_ref_pfx = 'TR') and nad_ref_no <> '23863') order by 1, 2  
--where  nad_ref_pfx = 'QT' and nad_ref_no = '20768'  
-- select * from US_scrcva_rec b where b.cva_ref_pfx = 'CU' and b.cva_cus_ven_typ ='C' and cva_addr_typ = 'L'  
--select top 31307 nad_ref_pfx, n ad_ref_no from US_tctnad_rec  
/*
20210128	Sumit
Add germany database
*/
GO
