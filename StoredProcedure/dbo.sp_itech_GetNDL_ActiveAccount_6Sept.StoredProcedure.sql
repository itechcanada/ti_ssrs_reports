USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetNDL_ActiveAccount_6Sept]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <18 Feb 2013>
-- Description:	<Getting OTP>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetNDL_ActiveAccount_6Sept] @DBNAME varchar(50),@Branch varchar(50),@Month as Datetime ,@Market varchar(50)--,@ISCategory int--,@Account varchar(50)

AS
BEGIN
SET NOCOUNT ON;

declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @SQL varchar(max)
declare @DB varchar(100)
declare @NFD varchar(10)
declare @NTD varchar(10)
declare @D3MFD varchar(10)
declare @D3MTD varchar(10)
declare @D6MFD varchar(10)
declare @D6MTD varchar(10)
declare @LFD varchar(10)
declare @LTD varchar(10)
DECLARE @StartDate VARCHAR(15);  
DECLARE @EndDate varchar(15);
declare @AFD varchar(10)
declare @ATD varchar(10)
declare @RFD varchar(10)
declare @RTD varchar(10)

CREATE TABLE #tmp (      AccountType varchar(50)
                        ,CustomerID  VARCHAR(15)
						,AccountName VARCHAR(150)
						,AccountDate varchar(15)
						,FirstSaleDate Varchar(15)
						,Branch  VARCHAR(15)
						,Category  VARCHAR(100)
						,SalePersonName  VARCHAR(50)
						,Months varchar(15)
   					);
   					
CREATE TABLE #tmpFinal (      AccountType varchar(50)
                        ,CustomerID  VARCHAR(15)
						,AccountName VARCHAR(150)
						,AccountDate varchar(15)
						,FirstSaleDate Varchar(15)
						,Branch  VARCHAR(15)
						,Category  VARCHAR(100)
						,SalePersonName  VARCHAR(50)
						,Months varchar(15)
   					);   					
   					
 CREATE TABLE #Main (   AccountType varchar(50)
                        ,CustomerID  int
						,AccountName VARCHAR(150)
						,AccountDate varchar(15)
						,FirstSaleDate Varchar(15)
						,Branch  VARCHAR(15)
						,Category  VARCHAR(100)
						,SalePersonName  VARCHAR(50)
						,Months varchar(15)
   					);
   					

   					
   					
set @DB= @DBNAME
IF @Branch = 'ALL'
 BEGIN
	 set @Branch = ''
 END
 
 IF @Market = 'ALL'
 BEGIN
	 set @Market = ''
 END
 
 --------  +++++  Created Table for last 12 months ++++++++
Create TABLE #Last12Months (   StartDate varchar(15)
                               ,EndDate  VARCHAR(15)
                            );

declare @start DATE = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, @Month)-1, 0) , 120);
 with CTEE(Last12MonthDate)
AS
(
    SELECT @start
     UNION   all

    SELECT DATEADD(month,-1,Last12MonthDate)
    from CTEE
    where DATEADD(month,-1,Last12MonthDate)>=DATEADD(month,-12,@start)
)
INSERT INTO #Last12Months(StartDate,EndDate)
select Last12MonthDate as StartDate,CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,Last12MonthDate)+1,0)), 120) as EndDate  from CTEE
  	   
DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(5);
DECLARE @Exists VARCHAR(max);
  	DECLARE @E VARCHAR(max);
  	DECLARE @val varchar(5);
  	   
  IF @DBNAME = 'ALL'
	BEGIN
		DECLARE DBAcct CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName
		  OPEN DBAcct;
				FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  				SET @DB= @Prefix

						DECLARE ScopeCursor CURSOR FOR
							select StartDate,EndDate from  #Last12Months
						  OPEN ScopeCursor;
								FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;
  							WHILE @@FETCH_STATUS = 0
  							  BEGIN
  	  							DECLARE @query NVARCHAR(max); 
				  	  			
  	  								set @NFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate),0)) , 120)   -- New Account
									set @NTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)  -- New Account

									set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-3,0)) , 120)   -- Dormant 3 Month
									set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-2,0)), 120)  -- Dormant 3 Month

									set @D6MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-6,0)) , 120)   --Dormant 6 Month
									set @D6MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-5,0)), 120)  -- Dormant 6 Month

									set @LFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)   -- Lost Account
									set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)-17,0)), 120)  -- Lost Account
					  	  			
  	  								set @AFD=  CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@StartDate)-18,0)) , 120)    -- Active Accounts 
									set @ATD=  CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0)), 120)
				  	  			 	
									CREATE TABLE #tactive (  CustomerID  VARCHAR(15)
															,FirstInvOfMonth Date
   														);
   														
                                    CREATE TABLE #tTtlactive (  CustomerID  VARCHAR(15)
																,FirstInvOfMonth Date
															);          														
									
									SET @query = ' INSERT INTO #tactive(CustomerID,FirstInvOfMonth)
														select distinct stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth 
														from '+ @DB +'_sahstn_rec
														where STN_INV_DT >= '''+  @NFD + ''' and STN_INV_DT <= ''' + @NTD +'''
														 group by stn_sld_cus_id'

										 EXECUTE sp_executesql @query;
									 -- Total Active
										 SET @query = ' INSERT INTO #tTtlactive(CustomerID,FirstInvOfMonth)
														select  stn_sld_cus_id, '''+  @NFD + ''' as FirstInvOfMonth 
														from '+ @DB +'_sahstn_rec
														where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @NTD +'''
														 group by stn_sld_cus_id '
										  EXECUTE sp_executesql @query;	 
								
									SET @SQL = 'INSERT INTO #tmp(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
								    select ''1''+'''+@DB+''', STN_SLD_CUS_ID, CUS_CUS_LONG_NM, min(STN_INV_DT),'''', CUS_ADMIN_BRH,cuc_desc30 as category,'''','''+ @StartDate +''' from '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat Group by  STN_SLD_CUS_ID,CUS_CUS_LONG_NM,CUS_ADMIN_BRH,cuc_desc30 having min(STN_INV_DT)  Between  ''' + @NFD + ''' And ''' + @NTD +'''
									Union
									SELECT  '''+@DB+'''+''3'',CUS_CUS_ID,CUS_CUS_LONG_NM,max(stn_inv_dt),'''',CUS_ADMIN_BRH,cuc_desc30,Min(STN_OS_SLP),'''+ @StartDate +''' as M FROM '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat where  STN_INV_DT <= ''' + @NTD +''' group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH Having MAX(stn_inv_dt) <=  '''+ @D3MTD +''' and MAX(stn_inv_dt) >= '''+ @D3MFD +'''
									Union 
									SELECT  '''+@DB+'''+''2'',CUS_CUS_ID,CUS_CUS_LONG_NM,max(stn_inv_dt),'''',CUS_ADMIN_BRH,cuc_desc30,Min(STN_OS_SLP),'''+ @StartDate +''' as M FROM '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat where STN_INV_DT <= ''' + @NTD +''' group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH Having MAX(stn_inv_dt) <=  '''+ @D6MTD +''' and MAX(stn_inv_dt) >= '''+ @D6MFD +'''
									Union 
									SELECT  '''+@DB+'''+''4'',CUS_CUS_ID,CUS_CUS_LONG_NM,max(stn_inv_dt),'''',CUS_ADMIN_BRH,cuc_desc30,Min(STN_OS_SLP),'''+ @StartDate +''' as M FROM '+ @DB +'_sahstn_rec INNER JOIN '+ @DB +'_arrcus_rec ON STN_SLD_CUS_ID=CUS_CUS_ID Left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat=cus_cus_cat where  STN_INV_DT <= ''' + @NTD +''' group by CUS_CUS_ID,CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH Having MAX(stn_inv_dt) <=  '''+ @LTD +''' and MAX(stn_inv_dt) >= '''+ @LFD +''' 
									Union 
									SELECT '''+@DB+'''+''1'' as A, CUS_CUS_ID,  CUS_CUS_LONG_NM,'''','''', CUS_ADMIN_BRH,cuc_desc30 as category,'''','''+ @StartDate +''' as M FROM #tactive t, '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat where  t.CustomerID Not IN ( select distinct t1.CustomerID  from #tactive t1,'+ @DB +'_sahstn_rec u where t1.CustomerID = u.STN_SLD_CUS_ID and u.STN_INV_DT > ''' + @AFD + ''' AND u.STN_INV_DT < ''' +@NFD + ''') and t.CustomerID not in (select stn_sld_cus_id from '+ @DB +'_sahstn_rec group by stn_sld_cus_id having min(STN_INV_DT)  Between  ''' + @NFD + ''' And ''' + @NTD +''') and  t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' + @NFD + ''' group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH
									Union 
									SELECT '''+@DB+'''+''5'', CUS_CUS_ID,  CUS_CUS_LONG_NM,'''','''', CUS_ADMIN_BRH,cuc_desc30 as category,'''','''+ @StartDate +''' as M FROM #tTtlactive t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id left Join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat where t.CustomerID = crd_cus_id and t.FirstInvOfMonth = ''' + 	@NFD + ''' group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH'
								
									set @query = @SQL
									print(@query)
									EXECUTE sp_executesql @query;
									
									set @val='1'+''+@DB+'';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT ''1''+'''+@DB+''' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End
										
									set @val=''+@DB+''+'1';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT '''+@DB+'''+''1'' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End	
										
									set @val=''+@DB+''+'2';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT '''+@DB+'''+''2'' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End	
										
									set @val=''+@DB+''+'3';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT '''+@DB+'''+''3'' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End	
											
									set @val=''+@DB+''+'4';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT '''+@DB+'''+''4'' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End	
										
									set @val=''+@DB+''+'5';
									 if not exists (select * from #tmp where AccountType =''+@val+'' and Months=''+ @StartDate +'')
									    begin 
											SET @query = 'INSERT INTO #tmp(AccountType,Months)
														SELECT '''+@DB+'''+''5'' as A,'''+ @StartDate +''' as M '
											EXECUTE sp_executesql @query;
										End				
									
									 Drop table #tactive;
									 Drop Table #tTtlactive;
									 
									 set @val=''
									 
									 if @DB='US'
									   Insert into #tmpFinal select * from #tmp where Branch  not in ('BHM','MTL','TAI')
									else
									   Insert into #tmpFinal select * from #tmp 
									
									delete from #tmp
									
								FETCH NEXT FROM ScopeCursor INTO @StartDate,@EndDate;
								END 
						  CLOSE ScopeCursor;
						  DEALLOCATE ScopeCursor;
						  
						  
					
  	  			FETCH NEXT FROM DBAcct INTO @DatabaseName,@Prefix;
  			  END 
		  CLOSE DBAcct;
		  DEALLOCATE DBAcct;
						  
   End
 
    INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
 	select AccountType ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal 
	group by AccountType,Months
	
 --   INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
 --	select 'TActAcct' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	--from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where  [order] in (1,4,8,11,14,17) --name like '%New Active Acct %' 
	--group by AccountType,Months
	
	-- INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	--select 'TNonActAcct' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	--from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where [order] in (2,5,9,12,15,18)--name like '%New Non Active Acct %'
	--group by AccountType,Months
	 
	-- INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	--select 'TZZAccount' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	--from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where [order] in (3,6,10,13,16,19)--name like '%Total New Acct %'  
	--group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'TActAcct' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where [order] in (1,4,8,11,14,17)--name like '%Total New Acct %'  
	group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'TRA' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total Reactivated Acct%'
	group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'T6MDA' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total 6M Dormant Acct%'  
	group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'T3MDA' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total 3M Dormant Acct%'
	group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'TLA' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Total Lost Acct%'  
	group by AccountType,Months
	
	 INSERT INTO #Main(AccountType,CustomerID,AccountName,AccountDate,FirstSaleDate,Branch,Category,SalePersonName,Months) 
	select 'TCMAA' ,count(CustomerID) as CustomerID,'' as AccountName,'' as AccountDate,'' as FirstSaleDate, '' as Branch,'' Category,'' as SalePersonName,Months
	from #tmpFinal inner Join  tbl_itech_management_account on id=AccountType where name like '%Current Month active customers%'
	group by AccountType,Months
	
   select [order],databasename, id, Name as AccountType ,CustomerID as CustomerID,'' as Category,'' as AccountName,'' as AccountDate, Months,'' as Branch,'' as SalePersonName
	from #Main Inner Join  tbl_itech_management_account on id=AccountType
	order by [order]
   
  
  drop table #tmpFinal  
  Drop table #tmp  
  Drop table #Last12Months
  drop table #Main
 
  
END

-- exec sp_itech_GetNDL_ActiveAccount 'ALL','ALL','2013-09-09','ALL'


--select * from tbl_itech_management_account
GO
