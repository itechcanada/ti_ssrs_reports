USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_IRM_40Week]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Mukesh>        
-- Create date: <26 Sep 2014>        
-- Description: <IRM_Report>
-- Last change Date: 29 Jun 2015
-- Last changes By: Mukesh
-- Last changes Desc: Remove the live connection of database        
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_IRM_40Week]  @DBNAME varchar(50),@FromDate datetime,@Whs Varchar(10),@CustType Varchar(2)  --,@Frm Varchar(6), @Grd Varchar(8), @Size Varchar(15), @Fnsh Varchar(8)      
AS        
BEGIN   

Return;
  
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
declare @DB varchar(100);        
declare @sqltxt varchar(6000);        
declare @execSQLtxt varchar(7000);        
DECLARE @prefix VARCHAR(15);          
DECLARE @CountryName VARCHAR(25);          
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @CurrenyRate varchar(15);          
declare @FD varchar(10)        
        
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)      
        
CREATE TABLE #temp ( Dbname    VARCHAR(10)        
     --,CusID    VARCHAR(10)        
     ,Branch    VARCHAR(3)        
     ,ActvyDT   VARCHAR(10)        
     ,Product  VARCHAR(100)        
     --,OrderPrefix  VARCHAR(100)        
     ,OrderNo   VARCHAR(20)     
     ,PONumber  VARCHAR(75)      
     ,Part   Varchar(100)  
      ,StkSDFQty1   DECIMAL(20, 2)        
      ,StkSDFQty2   DECIMAL(20, 2)        
      ,StkSDFQty3   DECIMAL(20, 2)        
      ,StkSDFQty4   DECIMAL(20, 2)        
      ,StkSDFQty5   DECIMAL(20, 2)        
      ,StkSDFQty6   DECIMAL(20, 2)        
      ,StkSDFQty7   DECIMAL(20, 2)        
      ,StkSDFQty8   DECIMAL(20, 2)        
      ,StkSDFQty9   DECIMAL(20, 2)        
      ,StkSDFQty10   DECIMAL(20, 2)        
      ,StkSDFQty11   DECIMAL(20, 2)        
      ,StkSDFQty12   DECIMAL(20, 2)        
      ,StkSDFQty13   DECIMAL(20, 2)        
      ,StkSDFQty14   DECIMAL(20, 2)        
      ,StkSDFQty15   DECIMAL(20, 2)
      ,MimStkQty1   DECIMAL(20, 2)        
      ,MimStkQty2   DECIMAL(20, 2)        
      ,MimStkQty3   DECIMAL(20, 2)        
      ,MimStkQty4   DECIMAL(20, 2)        
      ,MimStkQty5   DECIMAL(20, 2)        
      ,MimStkQty6   DECIMAL(20, 2)        
      ,MimStkQty7   DECIMAL(20, 2)        
      ,MimStkQty8   DECIMAL(20, 2)        
      ,MimStkQty9   DECIMAL(20, 2)        
      ,MimStkQty10   DECIMAL(20, 2)        
      ,MimStkQty11   DECIMAL(20, 2)        
      ,MimStkQty12   DECIMAL(20, 2)        
      ,MimStkQty13   DECIMAL(20, 2)        
      ,MimStkQty14   DECIMAL(20, 2)        
      ,MimStkQty15   DECIMAL(20, 2)   
   --,OnHandStockQty1  DECIMAL(20,2)        
   --,StockInQty1   DECIMAL(20, 2)        
   --     ,StockOutQty1        DECIMAL(20, 2)        
   --     ,NoOfIncomingItm1 DECIMAL(20, 2)        
   --     ,IncomingStockQty1  DECIMAL(20, 2)        
   --     ,IncomingStockVQty1 DECIMAL(20, 2)        
   --     ,OpenOrdBalanceQty1 DECIMAL(20, 2)        
   --     ,ForecastSaleQty1 DECIMAL(20, 2)        
   --     ,WhsSlsOverrideQty1 DECIMAL(20, 2)        
   --     ,SeasonalPct1  DECIMAL(20, 2)        
   --     ,StockCoverage1  DECIMAL(20, 2)        
   --     ,DerivedFSlsQty1  DECIMAL(20, 2)        
   --     ,StockPosQty1  DECIMAL(20, 2)        
   --     ,NTPosQty1   DECIMAL(20, 2)        
   --     ,TotalConsQty1  DECIMAL(20, 2)        
   --     ,NPosQty1   DECIMAL(20, 2)        
   --     ,MinWksSlsQty1  DECIMAL(20, 2)        
   --     ,MinStkAprnt1  DECIMAL(20, 2)        
   --     ,TotalMinStkQty1  DECIMAL(20, 2)        
   --     ,WkRapQty1   DECIMAL(20, 2)        
   --     ,StkSDFQty1   DECIMAL(20, 2)        
   --     ,NSDFQty1   DECIMAL(20, 2)     
   --     ,OnHandStockQty2  DECIMAL(20,2)        
   --,StockInQty2   DECIMAL(20, 2)        
   --     ,StockOutQty2        DECIMAL(20, 2)        
   --     ,NoOfIncomingItm2 DECIMAL(20, 2)        
   --     ,IncomingStockQty2  DECIMAL(20, 2)        
   --     ,IncomingStockVQty2 DECIMAL(20, 2)        
   --     ,OpenOrdBalanceQty2 DECIMAL(20, 2)        
   --     ,ForecastSaleQty2 DECIMAL(20, 2)        
   --     ,WhsSlsOverrideQty2 DECIMAL(20, 2)        
   --     ,SeasonalPct2  DECIMAL(20, 2)        
   --     ,StockCoverage2  DECIMAL(20, 2)        
   --     ,DerivedFSlsQty2  DECIMAL(20, 2)        
   --     ,StockPosQty2  DECIMAL(20, 2)        
   --     ,NTPosQty2   DECIMAL(20, 2)        
   --     ,TotalConsQty2  DECIMAL(20, 2)        
   --     ,NPosQty2   DECIMAL(20, 2)        
   --     ,MinWksSlsQty2  DECIMAL(20, 2)        
   --     ,MinStkQty2   DECIMAL(20, 2)        
   --     ,MinStkAprnt2  DECIMAL(20, 2)        
   --     ,TotalMinStkQty2  DECIMAL(20, 2)        
   --     ,WkRapQty2   DECIMAL(20, 2)        
   --     ,StkSDFQty2   DECIMAL(20, 2)        
   --     ,NSDFQty2   DECIMAL(20, 2)     
     );        
        
IF @DBNAME = 'ALL'        
 BEGIN        
  DECLARE ScopeCursor CURSOR FOR        
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS         
    OPEN ScopeCursor;        
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  WHILE @@FETCH_STATUS = 0        
  BEGIN        
   DECLARE @query NVARCHAR(MAX);        
   IF (UPPER(@Prefix) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@Prefix) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@Prefix) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@Prefix) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@Prefix) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@Prefix) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
          
    if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')        
    BEGIN        
    SET @query = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @Prefix +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,  wrw_
  
wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1      
  FROM ' + @Prefix + '_ortmbk_rec a        
      join ' + @Prefix + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @Prefix + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @Prefix + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  --where orl_bal_wgt > 0 '    
                 
    END        
    ELSE        
    BEGIN        
      SET @query = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @Prefix +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,  wrw_
  
wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1      
  FROM ' + @Prefix + '_ortmbk_rec a        
      join ' + @Prefix + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @Prefix + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @Prefix + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''')  '  
  --where orl_bal_wgt > 0 '       
    END        
   print @query;        
   EXECUTE sp_executesql @query;        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName, @CountryName, @prefix;        
  END           
  CLOSE ScopeCursor;        
  DEALLOCATE ScopeCursor;        
 END        
ELSE        
BEGIN      
 IF (UPPER(@DBNAME) = 'TW')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
    Else if (UPPER(@DBNAME) = 'NO')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))        
    End        
    Else if (UPPER(@DBNAME) = 'CA')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))        
    End        
    Else if (UPPER(@DBNAME) = 'CN')        
    begin        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))        
    End        
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')        
    begin        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))        
    End        
    Else if(UPPER(@DBNAME) = 'UK')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))        
    End        
    Else if(UPPER(@DBNAME) = 'DE')        
    begin        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))        
    End        
    Else if(UPPER(@DBNAME) = 'TWCN')        
    begin        
       SET @DBNAME ='TW'        
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))        
    End        
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix = + ''+ @DBNAME + '')     
     print @DatabaseName;     
     
     --Select * from tbl_itech_DatabaseName_PS;    
   if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK' )--UPPER(@DBNAME) = 'TW' OR         
   BEGIN        
   SET @sqltxt = 'INSERT INTO #temp (Dbname, CusID, Branch, ActvyDT, OrderPrefix, OrderNo, OrderItm, OnHandStockQty ,StockInQty,StockOutQty,NoOfIncomingItm ,IncomingStockQty,IncomingStockVQty ,OpenOrdBalanceQty ,ForecastSaleQty,WhsSlsOverrideQty ,    
    SeasonalPct ,StockCoverage    ,DerivedFSlsQty ,StockPosQty ,NTPosQty ,TotalConsQty ,NPosQty ,MinWksSlsQty ,MinStkQty ,MinStkAprnt ,TotalMinStkQty ,WkRapQty ,StkSDFQty ,NSDFQty )        
      SELECT '''+ @DBNAME +''' , mbk_sld_cus_id,wrw_whs,wrw_run_dt,mbk_ord_pfx, mbk_ord_no, mbk_ord_itm, wrw_ohd_stk_qty_1, wrw_stk_in_qty_1, wrw_stk_out_qty_1, wrw_nbr_ics_itm_1,  wrw_ics_qty_1, wrw_ics_vrs_qty_1,  wrw_oobal_qty_1,  wrw_fsls_qty_1,
        wrw_wso_qty_1,  wrw_ssnl_pct_1,  wrw_stk_covg_1,  wrw_dfs_qty_1,  wrw_stk_pos_qty_1,wrw_ntfrp_qty_1,  wrw_tcons_qty_1,  wrw_net_pos_qty_1,  wrw_wks_sls_qty_1,     
  wrw_mstk_qty_1,  wrw_mstkp_qty_1,  wrw_tmstkc_qty_1,  wrw_wkrapd_qty_1,  wrw_stk_sdf_qty_1,  wrw_net_sdf_qty_1     
  FROM ' + @DBNAME + '_ortmbk_rec a        
      join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @DBNAME + '_plswrw_rec on wrw_cmpy_id = rpd_cmpy_id and wrw_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), wrw_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (wrw_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''')  '  
  --where orl_bal_wgt > 0 '       
  END        
  ELSE        
  BEGIN        
      
   SET @sqltxt = 'INSERT INTO #temp (Dbname, OrderNo,PONumber, Part,Branch, ActvyDT, Product, StkSDFQty1,StkSDFQty2,StkSDFQty3,StkSDFQty4,StkSDFQty5,StkSDFQty6,StkSDFQty7,StkSDFQty8,StkSDFQty9,StkSDFQty10,StkSDFQty11,StkSDFQty12,StkSDFQty13,StkSDFQty14,
   StkSDFQty15,MimStkQty1,MimStkQty2,MimStkQty3,MimStkQty4,MimStkQty5,MimStkQty6,MimStkQty7,MimStkQty8,MimStkQty9,MimStkQty10,MimStkQty11,MimStkQty12,MimStkQty13,MimStkQty14,
   MimStkQty15)        
      SELECT  distinct '''+ @DBNAME +''',mbk_ord_no,ord_cus_po,clg_part, orl_shpg_whs,pwp_run_dt,RTRIM(mbk_frm) + ''-'' + RTRIM(mbk_grd) + ''-'' + RTRIM(mbk_size) + ''-'' + RTRIM(mbk_fnsh),pwp_stk_sdf_qty_1,pwp_stk_sdf_qty_2,pwp_stk_sdf_qty_3,pwp_stk_sdf_qty_4
,pwp_stk_sdf_qty_5,pwp_stk_sdf_qty_6,pwp_stk_sdf_qty_7,pwp_stk_sdf_qty_8,pwp_stk_sdf_qty_9,pwp_stk_sdf_qty_10,pwp_stk_sdf_qty_11,pwp_stk_sdf_qty_12,pwp_stk_sdf_qty_13,pwp_stk_sdf_qty_14,pwp_stk_sdf_qty_15,
pwp_mstk_qty_1,pwp_mstk_qty_2,pwp_mstk_qty_3,pwp_mstk_qty_4
,pwp_mstk_qty_5,pwp_mstk_qty_6,pwp_mstk_qty_7,pwp_mstk_qty_8,pwp_mstk_qty_9,pwp_mstk_qty_10,pwp_mstk_qty_11,pwp_mstk_qty_12,pwp_mstk_qty_13,pwp_mstk_qty_14,pwp_mstk_qty_15      
  FROM ' + @DBNAME + '_ortmbk_rec a        
      left join ' + @DBNAME + '_ortorl_rec d on d.orl_cmpy_id = a.mbk_cmpy_id and d.orl_ord_pfx = a.mbk_ord_pfx and d.orl_ord_no = a.mbk_ord_no and d.orl_ord_itm = a.mbk_ord_itm        
      join ' + @DBNAME + '_ortord_rec on ord_cmpy_id = mbk_cmpy_id and ord_ord_pfx = mbk_ord_pfx and ord_ord_no = mbk_ord_no and ord_ord_itm = mbk_ord_itm    
  join ' + @DBNAME + '_plrrpd_rec on rpd_cmpy_id = mbk_cmpy_id and rpd_frm = mbk_frm and rpd_grd = mbk_grd and rpd_size = mbk_size and rpd_fnsh =mbk_fnsh    
  join ' + @DBNAME + '_cprcpc_rec on cpc_cmpy_id = mbk_cmpy_id and cpc_frm = mbk_frm and cpc_grd = mbk_grd and cpc_size = mbk_size and cpc_fnsh =mbk_fnsh  
  join ' + @DBNAME + '_cprclg_rec on clg_part_ctl_no = cpc_part_ctl_no  
  join ' + @DBNAME + '_plspwp_rec on pwp_cmpy_id = rpd_cmpy_id and pwp_rpd_ctl_no = rpd_rpd_ctl_no and     
  CONVERT(VARCHAR(10), pwp_run_dt, 120) = CONVERT(VARCHAR(10), '''+ @FD +''', 120) and (orl_shpg_whs = ''' + @Whs + ''' OR '''' = ''' + @Whs + ''') '    
  if  (UPPER(@DBNAME)= 'US' )  
   BEGIN    
	   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''11782'' or mbk_sld_cus_id = ''1767'' or mbk_sld_cus_id = ''993'')  '  
		END
  END  
  ELSE  
  BEGIN  
   if (UPPER(@CustType) = 'M')
	   BEGIN
			SET @sqltxt = @sqltxt + ' and (mbk_sld_cus_id = ''302'')  '  
	END
  END  
  SET @sqltxt = @sqltxt + ' where  pwp_rec_nbr =  1 order by mbk_ord_no'      
 -- SET @sqltxt = @sqltxt + ' where orl_bal_wgt > 0 and wrw_rec_nbr =  1 order by mbk_ord_no'      
     
  print 'Mukesh';      
  END        
 print(@sqltxt)        
 set @execSQLtxt = @sqltxt;         
 EXEC (@execSQLtxt);        
END        
 SELECT * FROM #temp ;        
 DROP TABLE  #temp;        
END        
        
     
--EXEC [sp_itech_IRM_40Week] 'US','2014-09-26','ROC','M'  
GO
