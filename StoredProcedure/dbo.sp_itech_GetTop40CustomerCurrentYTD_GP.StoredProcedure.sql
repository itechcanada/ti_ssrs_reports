USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTop40CustomerCurrentYTD_GP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <27 Feb 2013>
-- Description:	<Getting top 40 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_GetTop40CustomerCurrentYTD_GP] @DBNAME varchar(50),@Location int,@NOOFCustomer integer,@Market varchar(65),@CompareYTD as integer,@Branch varchar(10),@version char = '0'

AS
BEGIN
	
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)

Set @CompareYTD=0   -- Current yesr


                   CREATE TABLE #tmpPrevious (    CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, CustEmail VARCHAR(150)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
                    , TotalValue  	 DECIMAL(20, 2)
   					, NProfitPercentage               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, GProfitPercentage			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15)
   					,Cry           Varchar(5) 
   					,ContactAdd		Varchar(100)
   	               );	
   	               
   	               CREATE TABLE #tmpCurrent (    CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, CustEmail VARCHAR(150)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
                    , TotalValue  	 DECIMAL(20, 2)
   					, NProfitPercentage               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, GProfitPercentage			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15)
   					,Cry           Varchar(5) 
   					,ContactAdd		Varchar(100)
   	               );	
   	               
   	                 CREATE TABLE #tmp (   
   					 Branch		 VARCHAR(65)
   					, CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, CustEmail VARCHAR(150)
   					, Market	  VARCHAR(65) 
                    , TotalValue_Pre  	 DECIMAL(20, 2)
   					, NProfitPercentage_pre               DECIMAL(20, 2)
   					, TotalMatValue_pre			 DECIMAL(20, 2)
   					, GProfitPercentage_Pre			 DECIMAL(20, 2)
   					, TotalValue_Cu 	 DECIMAL(20, 2)
   					, NProfitPercentage_Cu               DECIMAL(20, 2)
   					, TotalMatValue_Cu			 DECIMAL(20, 2)
   					, GProfitPercentage_Cu			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15)
   					,Cry           Varchar(5) 
   					,ContactAdd		Varchar(100)
   	               );
   	               
 -- Previous Year
IF @version = '0'
BEGIN
  insert into #tmpPrevious (CustID, CustName,CustEmail, Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry,ContactAdd)
   exec sp_itech_GetTop40Customer_GP @DBNAME,@Location,1,@Market,@Branch
END
ELSE
BEGIN
insert into #tmpPrevious (CustID, CustName,CustEmail, Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry,ContactAdd)
   exec sp_itech_GetTop40Customer_GP @DBNAME,@Location,1,@Market,@Branch,'1'
END
   
   -- Current Year
IF @version = '0'
BEGIN
   insert into #tmpCurrent (CustID, CustName,CustEmail,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry,ContactAdd)
   exec sp_itech_GetTop40Customer_GP @DBNAME,@Location,0,@Market,@Branch
END
ELSE
BEGIN
insert into #tmpCurrent (CustID, CustName,CustEmail,Market,Branch,TotalValue,NProfitPercentage,TotalMatValue,GProfitPercentage,Databases,Cry,ContactAdd)
   exec sp_itech_GetTop40Customer_GP @DBNAME,@Location,0,@Market,@Branch,'1'
END   
   
   if (@CompareYTD=1) -- Previous Year
     BEGIN
		   insert into #tmp (Branch,CustID, CustName,CustEmail,Market,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_pre,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,Databases,Cry,ContactAdd)
		   Select p.Branch, p.CustID, p.CustName, p.CustEmail, p.Market ,p.TotalValue,p.NProfitPercentage,p.TotalMatValue,p.GProfitPercentage,c.TotalValue,c.NProfitPercentage,c.TotalMatValue,c.GProfitPercentage,p.Databases,p.Cry, p.ContactAdd 
		   from #tmpPrevious as p
		   Left join #tmpCurrent  as c on p.CustID=c.CustID  and  p.Branch=c.Branch --and  p.cry=c.cry and p.ContactAdd = c.ContactAdd
      END
      ELSE
       BEGIN -- Current Year
           insert into #tmp (Branch,CustID, CustName,CustEmail,Market,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_Pre,Databases,Cry,ContactAdd)
		   Select c.Branch, c.CustID, c.CustName, c.CustEmail, c.Market ,c.TotalValue,c.NProfitPercentage,c.TotalMatValue,c.GProfitPercentage,p.TotalValue,p.NProfitPercentage,p.TotalMatValue,p.GProfitPercentage,c.Databases,c.Cry , c.ContactAdd
		   from #tmpCurrent   as c
		   Left join #tmpPrevious as p on c.CustID=p.CustID  and c.Branch= p.Branch --and  c.cry=p.cry and c.ContactAdd = p.ContactAdd
       END
    
    declare @I integer;
    SET  @I= @NOOFCustomer;
      
     IF (@Location=1)
			 BEGIN
				  if (@CompareYTD=1) -- Previous Year
					 BEGIN
						   SELECT  Branch,CustID, CustName,CustEmail,Market,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_Pre,Databases,Cry,ContactAdd
						 FROM (SELECT
							 ROW_NUMBER() OVER ( PARTITION BY Branch ORDER BY TotalValue_pre DESC ) AS 'RowNumber',
							Branch,CustID, CustName,CustEmail,Market,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_Pre,Databases,Cry,ContactAdd
						  FROM #tmp
						  ) dt
						  WHERE RowNumber <= @I
					  END
			  ELSE
					   BEGIN -- Current Year
						   SELECT  Branch,CustID, CustName,CustEmail,Market,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_Pre,Databases,Cry,ContactAdd
						 FROM (SELECT
							 ROW_NUMBER() OVER ( PARTITION BY Branch ORDER BY TotalValue_CU DESC ) AS 'RowNumber',
							Branch,CustID, CustName,CustEmail,Market,TotalValue_Cu,NProfitPercentage_Cu,TotalMatValue_Cu,GProfitPercentage_Cu,TotalValue_pre,NProfitPercentage_pre,TotalMatValue_pre,GProfitPercentage_Pre,Databases,Cry,ContactAdd
						  FROM #tmp
						  ) dt
						  WHERE RowNumber <= @I
					   END
			 END
	 ELSE
			 BEGIN
				  if (@CompareYTD=1) -- Previous Year
					 BEGIN
							IF(@NOOFCustomer = 0)
							BEGIN
								SELECT  * FROM #tmp --where ( Databases is not null and Databases <>'') 
								order by TotalValue_pre desc;
							END
							ELSE
							BEGIN
							
								SELECT TOP(@NOOFCustomer) * FROM #tmp --where ( Databases is not null and Databases <>'') 
								order by TotalValue_pre desc;
							END
					  END
			 ELSE
					 BEGIN
							if(@NOOFCustomer = 0)
							BEGIN
								SELECT  * FROM #tmp --where ( Databases is not null and Databases <>'') 
							  order by TotalValue_cu desc;
							END
							ELSE
							BEGIN
									
							  SELECT TOP(@NOOFCustomer) * FROM #tmp --where ( Databases is not null and Databases <>'') 
							  order by TotalValue_cu desc;
						 END
					 End
			 END
   
   drop table #tmpPrevious
   drop table #tmpCurrent
   drop table #tmp
END

-- exec sp_itech_GetTop40Customer_NewFormat 'ALL',1
-- exec sp_itech_GetTop40CustomerCurrentYTD_GP 'US',0,0,'ALL',1,'ALL'




GO
