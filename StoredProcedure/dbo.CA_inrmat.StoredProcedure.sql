USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_inrmat]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,Aug 8, 2015,>
-- Description:	<Description,Material,>

-- =============================================
create PROCEDURE [dbo].[CA_inrmat]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_inrmat_rec', 'U') IS NOT NULL
		drop table dbo.CA_inrmat_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CA_inrmat_rec
  from [LIVECASTX].[livecastxdb].[informix].[inrmat_rec] ; 
  
END
-- select * from CA_inrmat_rec
GO
