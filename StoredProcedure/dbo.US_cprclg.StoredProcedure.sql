USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_cprclg]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,Mrinal>    
-- Create date: <Create Date,Sep 29, 2014,>    
--   
    
-- =============================================    
CREATE PROCEDURE [dbo].[US_cprclg]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.US_cprclg_rec', 'U') IS NOT NULL    
  drop table dbo.US_cprclg_rec;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.US_cprclg_rec    
  from [LIVEUSSTX].[liveusstxdb].[informix].cprclg_rec ;     
      
END    
-- select * from US_cprclg_rec
GO
