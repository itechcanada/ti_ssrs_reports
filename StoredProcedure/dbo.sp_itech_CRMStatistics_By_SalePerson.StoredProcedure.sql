USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CRMStatistics_By_SalePerson]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <13 Nov 2013>    
-- Modified date: <14 -05-2015>    
-- Modified by: <Mukesh>   
-- Description: <Changed query as per new document, since we lost some table in upgrade>   
-- Last change Date: 29 Jun 2015  
-- Last changes By: Mukesh  
-- Last changes Desc: Remove the live connection of database   
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_CRMStatistics_By_SalePerson] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch as varchar(65), @SalePerson as Varchar(65),@version char = '0'     
    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
declare @sqltxtAct varchar(6000)    
declare @execSQLtxtAct varchar(7000)    
declare @sqltxtTsk varchar(6000)    
declare @execSQLtxtTsk varchar(7000)    
declare @DB varchar(100)    
declare @FD varchar(10)    
declare @TD varchar(10)    
    
set @DB=  @DBNAME    
    
IF @Branch = 'ALL'    
 BEGIN    
  set @Branch = ''    
 END    
     
 IF @SalePerson = 'ALL'    
 BEGIN    
  set @SalePerson = ''    
 END    
     
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)    
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)    
    
CREATE TABLE #tmpAct ([Database]   VARCHAR(10)       
     , LoginID   VARCHAR(50)    
     , MonthYear   VARCHAR(10)    
        , ActDormant  integer    
        , ActExisting  integer    
        , ActProspect  integer    
        , TskDormant  integer    
        , TskExisting  integer    
        , TskProspect  integer    
                 );     
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)       
     , LoginID   VARCHAR(50)    
     , MonthYear   VARCHAR(10)    
        , ActDormant  integer    
        , ActExisting  integer    
        , ActProspect  integer    
        , TskDormant  integer    
        , TskExisting  integer    
        , TskProspect  integer    
                 );     
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(35);    
DECLARE @Name VARCHAR(15);    
    
IF @DBNAME = 'ALL'    
 BEGIN    
 IF @version = '0'      
    BEGIN      
    DECLARE ScopeCursor CURSOR FOR      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName      
   OPEN ScopeCursor;      
    END      
    ELSE      
    BEGIN      
    DECLARE ScopeCursor CURSOR FOR      
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS       
   OPEN ScopeCursor;      
    END     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @queryAct NVARCHAR(1500);       
        DECLARE @queryTsk NVARCHAR(1500);       
      SET @DB= @Prefix     
      SET @queryAct =    
              'INSERT INTO #tmpAct ([Database], LoginID, MonthYear, ActDormant, ActExisting, ActProspect)    
        
       Select * from (  
        select ''' +  @Prefix + ''' as [Database],cay_prfd_by, REPLACE(RIGHT(CONVERT(VARCHAR(9), cay_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts   
        from  
         ' + @DB + '_cctcay_rec join   
        ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cay_crmact_purp  and cay_crmacct_typ = 1 join  
        ' + @DB + '_arrcus_rec on cus_cmpy_id = cay_cmpy_id  and cus_cus_id = cay_crmacct_id  
  where CONVERT(VARCHAR(10), cay_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cay_crtd_dtts , 120) <= '''+ @TD +'''    
  And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')    
      And (cay_prfd_by = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
    
  group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cay_crtd_dtts, 6), 6), '' '', ''-''), cay_prfd_by, atp_desc30  
    
  ) AS Query1  
  pivot (Max(counts) for att_desc30 in([Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po '  
      
      print(@queryAct);    
        EXECUTE sp_executesql @queryAct;    
            
        SET @queryTsk =    
              'INSERT INTO #tmpTsk ([Database], LoginID, MonthYear, TskDormant, TskExisting, TskProspect)  
      Select * from (   
      select ''' +  @Prefix + ''' as [Database],cta_tsk_asgn_to, REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts   
       from ' + @DB + '_cctcta_rec join   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join  
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id   
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))     
       And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')   
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
       group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-''), cta_tsk_asgn_to, atp_desc30  
       ) AS Query1    
      pivot (Max(counts) for att_desc30 in([Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'    
        EXECUTE sp_executesql @queryTsk;    
        print(@queryTsk);    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
     
      
        
      SET @sqltxtAct ='INSERT INTO #tmpAct ([Database], LoginID, MonthYear, ActDormant, ActExisting, ActProspect)    
        
       Select * from (  
        select ''' +  @DB + ''' as [Database],cay_prfd_by, REPLACE(RIGHT(CONVERT(VARCHAR(9), cay_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts   
        from  
         ' + @DB + '_cctcay_rec join   
        ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cay_crmact_purp  and cay_crmacct_typ = 1 join  
        ' + @DB + '_arrcus_rec on cus_cmpy_id = cay_cmpy_id  and cus_cus_id = cay_crmacct_id  
  where CONVERT(VARCHAR(10), cay_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cay_crtd_dtts , 120) <= '''+ @TD +'''    
  And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')    
      And (cay_prfd_by = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
    
  group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cay_crtd_dtts, 6), 6), '' '', ''-''), cay_prfd_by, atp_desc30  
    
  ) AS Query1  
  pivot (Max(counts) for att_desc30 in([Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po '  
        
        
        
    print(@sqltxtAct)    
    set @execSQLtxtAct = @sqltxtAct;     
   EXEC (@execSQLtxtAct);    
    
 -- task      
        
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database], LoginID, MonthYear, TskDormant, TskExisting, TskProspect)  
      Select * from (   
      select ''' +  @DB + ''' as [Database],cta_tsk_asgn_to, REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-'') AS MonthYeare, rtrim(atp_desc30) As att_desc30, COUNT(*) as counts   
       from ' + @DB + '_cctcta_rec join   
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1 join  
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id   
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR  
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))     
       And (cus_admin_brh = '''+ @Branch +'''or '''+ @Branch +'''= '''')   
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')  
       group by REPLACE(RIGHT(CONVERT(VARCHAR(9), cta_crtd_dtts, 6), 6), '' '', ''-''), cta_tsk_asgn_to, atp_desc30  
       ) AS Query1    
      pivot (Max(counts) for att_desc30 in([Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account])) As po'  
        
     
        
        
    print(@sqltxtTsk)    
    set @execSQLtxtTsk = @sqltxtTsk;     
   EXEC (@execSQLtxtTsk);    
     END    
   -- SELECT * FROM #tmpAct Order by MonthYear desc, LoginID    
   -- SELECT * FROM #tmpTsk Order by MonthYear desc, LoginID    
   SELECT CASE WHEN act.[Database] IS NULL THEN tsk.[Database] ELSE act.[Database] END AS [Database],    
    CASE WHEN act.LoginID Is NULL then tsk.LoginID ELSE act.LoginID END AS LoginID,     
    CASE WHEN act.MonthYear IS NULL then tsk.MonthYear ELSE act.MonthYear  END AS MonthYear,     
    ISNULL(act.ActDormant,0) AS ActDormant,    
    ISNULL(act.ActExisting,0) AS ActExisting,     
    ISNULL(act.ActProspect,0) AS ActProspect,     
    ISNULL(act.ActDormant,0) + ISNULL(act.ActExisting,0) + ISNULL(act.ActProspect,0) AS TotalAct,    
    ISNULL(tsk.TskDormant,0) AS TskDormant,     
    ISNULL(tsk.TskExisting,0) AS TskExisting,    
    ISNULL(tsk.TskProspect,0) AS TskProspect,    
    ISNULL(tsk.TskDormant,0) + ISNULL(tsk.TskExisting,0) + ISNULL(tsk.TskProspect,0)  AS TotalTsk FROM #tmpAct as act    
    full outer  join #tmpTsk tsk on act.LoginID = tsk.LoginID and act.MonthYear = tsk.MonthYear    
    order by MonthYear DESC, LoginID    
        
    Drop table #tmpAct Drop table #tmpTsk    
END    
    
-- exec sp_itech_CRMStatistics_By_SalePerson '12/01/2017', '12/31/2017' , 'US','ALL','jgilkens'    
-- exec sp_itech_CRMStatistics_By_SalePerson '04/01/2015', '04/30/2015' , 'US','ALL', 'ALL'    
--   exec sp_itech_CRMStatistics_By_SalePerson '06/01/2020', '07/01/2020' , 'US','ALL', 'ALL'
/*
2020/07/10	Sumit
Visit - Dormant Account >> Visit - Lost/Dormant Account
*/
GO
