USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CostAnalysisInterco_NP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukesh >              
-- Create date: <27 Jun 2016>              
-- Description: <Getting top 50 customers for SSRS reports>        
            
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_CostAnalysisInterco_NP] @FromDate datetime, @ToDate datetime,@DateRange int, @ShpWhs Varchar(5), @status char = 'I'            
AS  
Begin            
SET    NOCOUNT ON;              
declare @sqltxt varchar(max)              
declare @execSQLtxt varchar(max)              
declare @FD varchar(10)              
declare @TD varchar(10)              
              
              
CREATE TABLE #tmp (  CustID varchar(15)               
        ,CustName Varchar(65)              
        ,InvDt varchar(15)              
        ,Product   varchar(300)              
        ,NoOfPcs Varchar(10)              
        , Inches    DECIMAL(20, 2)              
        , Weight  DECIMAL(20, 2)              
        , Part   VARCHAR(50)               
        ,InvNo Varchar(50)    
        ,OrdNo Varchar(100)              
        , Date     varchar(65)              
        ,Form  varchar(35)              
        ,Grade  varchar(35)              
        ,Size  varchar(35)              
        ,Finish  varchar(35)              
        ,Measure  DECIMAL(20, 2)              
        ,TotalValue DECIMAL(20, 2)              
        ,GPPct DECIMAL(20, 2)              
        ,NPAmt DECIMAL(20, 2) 
		,NPPct DECIMAL(20, 2)             
        ,PONumber Varchar(75)          
        ,TransportNo Varchar(30)     ,        
        MktCatg varchar(30),        
        ShpWhs varchar(3),    
        Branch varchar(10)    
        ,RefPfx varchar(2)                 
        ,RefNo varchar(10)                
        ,RefItm varchar(10)                
        ,CstNo   varchar(10)                
        ,CstDesc Varchar(25)                
        , Cost    DECIMAL(20, 4)                
        , CostUM  Varchar(3)                
        , BasCryVal   Decimal(20,2)          
                 );               
              
              
 if @ShpWhs ='ALL' 
 BEGIN
  set @ShpWhs = ''
 END
              
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )              
 BEGIN              
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month              
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month              
 End              
else if @DateRange=2 -- Last 7 days (excluding today)              
 BEGIN              
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day              
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day              
 End              
else if @DateRange=3 -- Last 14 days (excluding today)              
 BEGIN              
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day              
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day              
 End              
else if @DateRange=4 --Last 12 months excluding all the days in the current month              
 BEGIN              
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month              
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month              
 End              
else              
 Begin              
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)              
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) 
               End           
    if @status = 'I'            
      BEGIN          
     SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo,OrdNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,CstDesc ,Cost,CostUM,BasCryVal  )              
       SELECT  stn_sld_cus_id as CustID , cus_cus_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, stn_blg_wgt as Weight, frm_Desc25 + '' '' + grd_desc25 as Product,                
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  ivh_inv_due_dt as Date,stn_blg_MSR as Measure ,STN_TOT_VAL ,              
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val   END as ''NPAmt'', 
       (CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE (stn_npft_avg_val/STN_TOT_VAL) * 100 END) as NPPct,	   
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_mpft_avg_val   END as ''GPPct'',              
       stn_blg_pcs,stn_upd_ref, convert(Varchar(10),stn_ord_no) + ''-'' + Convert(varchar(10),stn_ord_itm) + ''-'' + Convert(varchar(10),stn_ord_rls_no), stn_cus_po,stn_transp_pfx + ''-'' + Convert(Varchar(15),stn_transp_no) + ''-'' + Convert(Varchar(5),stn_shpt_itm)  , cuc_desc30, stn_shpg_whs , stn_shpt_brh,    
       csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst, csi_cst_um, csi_bas_cry_val            
       FROM US_sahstn_rec left join US_ivtivh_rec on stn_cmpy_id = ivh_cmpy_id and stn_upd_ref = ivh_upd_ref    
       join US_IVTIVS_rec on ivs_cmpy_id = stn_cmpy_id and ivs_shpt_pfx = stn_shpt_pfx and ivs_shpt_no = stn_shpt_no    
       join US_cttcsi_rec on  csi_cmpy_id = ivs_cmpy_id and  csi_ref_pfx = ivs_shpt_pfx and csi_ref_no = ivs_shpt_no    
       , US_inrfrm_rec,               
      US_inrgrd_rec,US_arrcus_rec    left  join US_arrcuc_rec on cus_cus_cat = cuc_cus_cat        
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm               
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id and stn_Frm <> ''XXXX'''           
       END            
       ELSE            
       BEGIN --for Order information  
        SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo, OrdNo, PONumber,TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,CstDesc ,Cost  
        ,CostUM,BasCryVal  )              
        select  ivs_Sld_cus_id, cus_cus_nm as CustName, ivh_inv_dt , ipd_lgth,  sum(ivd_blg_wgt), LTRIM(ipd_frm) + '' X '' + LTRIM(ipd_grd) + '' X '' + LTRIM(ipd_size) as Product,             
     ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_part,ivh_inv_due_dt, sum(ivd_blg_msr), SUM(tot.chl_chrg_val)  ,             
      (SUM(cht_tot_val) - sum(oit_tot_avg_val))  as NPAmt,             
	  case when sum(cht_tot_val) > 0             
                       then             
                                    (SUM(cht_tot_val) - sum(oit_tot_avg_val))/sum(cht_tot_val) * 100            
                                    else            
                                    0            
 end             
                              as NetPftVal,            
                                          
                              case when sum(cht_tot_val) > 0             
                              then             
                                    (SUM(cht_tot_val) - sum(oit_mtl_avg_val))/sum(cht_tot_val) * 100            
                                    else            
                                    0            
                              end             
                              as MPFTAvgVal,             
     sum(ivd_blg_pcs),            
     ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivs_ord_no, ivd_cus_po,ivs_transp_pfx + ''-'' + Convert(Varchar(15),ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm)             
       , cuc_desc30, ivd_shpg_whs, ipd_brh,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst, csi_cst_um, csi_bas_cry_val         
       from US_ivtivs_rec     
       join US_cttcsi_rec on csi_cmpy_id = ivs_cmpy_id and csi_ref_pfx = ivs_shpt_pfx and csi_ref_no = ivs_shpt_no      
                     
       join US_ivtivh_rec on             
       ivs_cmpy_id = ivh_cmpy_id and ivs_inv_pfx = ivh_inv_pfx and ivs_inv_no = ivh_inv_no            
       join US_ortoit_rec on oit_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = oit_ref_pfx and            
                                oit_ref_itm = 0 and ivs_Shpt_no = oit_ref_no            
                                 join US_ortcht_rec on cht_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = cht_ref_pfx and            
    cht_ref_itm = 0 and cht_tot_typ = ''T''  and ivs_Shpt_no = cht_ref_no            
     join US_tctipd_rec on ivs_cmpy_id = ipd_cmpy_id            
     and ivs_shpt_pfx = ipd_ref_pfx and ivs_shpt_no = ipd_ref_no             
     join US_ivtivd_rec            
     on ivd_cmpy_id = ipd_cmpy_id            
     and ivd_shpt_pfx = ipd_ref_pfx and ivd_shpt_no = ipd_ref_no and ivd_shpt_itm = ipd_ref_itm             
     join US_ortchl_rec tot            
     on ivd_cmpy_id = tot.chl_cmpy_id            
     and ivd_shpt_pfx = tot.chl_ref_pfx and ivd_shpt_no = tot.chl_ref_no and ivd_shpt_itm = tot.chl_ref_itm             
     and tot.chl_chrg_cl = ''E''            
     join US_ortchl_rec mtl            
     on ivd_cmpy_id = mtl.chl_cmpy_id            
     and ivd_shpt_pfx = mtl.chl_ref_pfx and ivd_shpt_no = mtl.chl_ref_no and ivd_shpt_itm = mtl.chl_ref_itm             
     and mtl.chl_chrg_cl = ''E'' and mtl.chl_chrg_no = 1            
     join US_arrcus_rec on cus_cmpy_id = ivs_cmpy_id  and cus_cus_id = ivs_Sld_cus_id           
     left join US_arrcuc_rec on cus_cus_cat = cuc_cus_cat        
      where ivs_sts_actn = ''A''  and  ipd_frm <> ''XXXX''            
      and ivh_inv_dt >= '''+ @FD +''' and ivh_inv_dt <= '''+ @TD +'''            
      group by ivs_Sld_cus_id,cus_cus_nm,ivh_inv_dt, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ivd_cus_po, ipd_part, ivh_inv_due_dt,ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivs_ord_no,ivs_transp_pfx + ''-'' + Convert(Varchar(15)      
      ,ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm), cuc_desc30, ivd_shpg_whs, ipd_brh ,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst, csi_cst_um, csi_bas_cry_val     '            
             
     END                  
     print(@sqltxt);               
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
                 
      
   
      
  SELECT * FROM #tmp where ShpWhs not in ('SFS') AND Branch not in ('SFS')  and (ShpWhs = @ShpWhs OR @ShpWhs = '') 
  and CustID in ('10993','11980','12896','2046','3330','5833') order by CustID           
  DROP TABLE #tmp              
              
END              
              
 -- Exec [sp_itech_CostAnalysisInterco] '2016-05-07', '2016-07-06',1,'ALL','O'
GO
