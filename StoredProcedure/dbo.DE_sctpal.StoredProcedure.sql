USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_sctpal]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  Sumit         
-- Create date: <11/25/2020>        
-- Description: <Description,Propagation Audit Log,>        
-- =============================================        
CREATE PROCEDURE [dbo].[DE_sctpal]         
         
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
SET NOCOUNT ON;        
      
truncate table dbo.DE_sctpal_rec;           
-- Insert statements for procedure here        
      
insert into dbo.DE_sctpal_rec      
SELECT * FROM [LIVEDESTX].[livedestxdb].[informix].[sctpal_rec] where pal_aud_ctl_no not in ('175105','175184','175944', '182573', '182578');        
        
END       
    
/*    
20210322 Sumit    
overflow or mismatch error    
skip 175105    
20210324 Sumit  
overflow or mismatch error  
skip 175184  
20210413  Sumit    
Skip 175944 due to error    
20210716 Sumit
skip  182573, 182578 due to error
*/
GO
