USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Departmental_KPI_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================              
-- Author:  <Mukesh >              
-- Create date: <28 Feb 2017>              
          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_Departmental_KPI_V1] @DBNAME varchar(50),@FromDate datetime, @ToDate datetime --, @IncludeLTA char(1) = '1'             
AS              
BEGIN              
               
 SET NOCOUNT ON;     
     
declare @sqltxt varchar(6000)              
declare @execSQLtxt varchar(7000)              
declare @DB varchar(100)              
declare @FD varchar(10)              
declare @TD varchar(10)               
declare @ExcessFD varchar(10)          
declare @ExcessTD varchar(10)        
declare @FD2 varchar(10)              
declare @TD2 varchar(10)       
declare @ShtgFD varchar(10)       
declare @LTD varchar(10)       
      
    set @DB=  @DBNAME       
           
 set @FD = CONVERT(VARCHAR(10), @FromDate,120)          
 set @TD = CONVERT(VARCHAR(10), @ToDate,120)         
       
  set @FD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, 0, @FromDate), 0),120)   -- current month from date          
  set @TD2 = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate), -1),120)  --current month To date          
          
       set @ExcessFD =  CONVERT(VARCHAR(10),DATEADD(MONTH, -6, @FromDate),120)          
       set @ExcessTD =   @FD           
set @ShtgFD = CONVERT(VARCHAR(10), DateAdd(mm, -12, @FromDate) , 120)   -- For shotage calculation       
set @LTD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-18, -1),120)  -- Lost Account  correct           
          
CREATE TABLE #tmp (                  
          TtlMultMetalInvVal    DECIMAL(20, 0)              
        , TtlMultMetalInvWgt        DECIMAL(20, 0)              
        , ExcessMultMetalInvWgt5Pct    DECIMAL(20, 0)           
        , ShotageMultMetalInvVal Decimal(20,0)             
        , ShotageMultMetalInvWgt    DECIMAL(20, 0)              
        , ObsoleteMultMetalInvWgt   DECIMAL(20, 0)      
         ,TtlMultMetalBuyoutVal           DECIMAL(20, 0)           
        ,TtlMultMetalBuyoutWgt           DECIMAL(20, 0)              
        , BHMMultMetalLBS    DECIMAL(20, 0)              
        , CRPMultMetalLBS    DECIMAL(20, 0)          
        ,EXPMultMetalLBS  DECIMAL(20, 0)            
        ,HIBMultMetalLBS  DECIMAL(20, 0)             
        ,INDMultMetalLBS  DECIMAL(20, 0)           
        ,JACMultMetalLBS  DECIMAL(20, 0)           
        ,LAXMultMetalLBS  DECIMAL(20, 0)             
        ,MTLMultMetalLBS  DECIMAL(20, 0)           
        , ROSMultMetalLBS DECIMAL(20, 0)           
        , SEAMultMetalLBS DECIMAL(20, 0)           
        , SHAMultMetalLBS DECIMAL(20, 0)           
        ,TAIMultMetalLBS DECIMAL(20, 0)           
        ,WDLMultMetalLBS DECIMAL(20, 0)           
        ,TtlSalesShipped DECIMAL(20, 0)           
        ,TtlLBSShipped  DECIMAL(20, 0)           
        ,TtlNetProfitPCt DECIMAL(20, 2)            
        ,TtlOperatingExp DECIMAL(20, 0)          
        , TtlOperatingProfit DECIMAL(20, 0)          
        , TtlReturnVal DECIMAL(20, 0)          
        , TtlTitaniumInvVal DECIMAL(20, 0)         
        , TtlTitaniumInvWgt DECIMAL(20, 0)        
        , ExcessTitaniumInvVal DECIMAL(20, 0)        
        , ExcessTitaniumInvWgt DECIMAL(20, 0)        
        , ShortageTitaniumVal DECIMAL(20, 0)        
        , ShortageTitaniumWgt DECIMAL(20, 0)        
        , ObsoleteTitaniumVal DECIMAL(20, 0)        
        , ObsoleteTitaniumWgt DECIMAL(20, 0)        
        , TtlBuyOutVal DECIMAL(20, 0)        
        , TtlBuyOutWgt DECIMAL(20, 0)        
        , USBranchLBSShipped DECIMAL(20, 0)        
        , USBranchSales DECIMAL(20, 0)        
        , USBranchBooking DECIMAL(20, 0)        
        , USBranchNetProfit DECIMAL(20, 0)        
        , USBranchNetProfitPct DECIMAL(20, 0)        
        , USBranchOperatingExp DECIMAL(20, 0)        
        , USBranchOperatingProfit DECIMAL(20, 0)        
        , USBranchActiveCusBase DECIMAL(20, 0)        
        , USBranchExcessLBS DECIMAL(20, 0)        
        , USBranchMulMtlLBS DECIMAL(20, 0)        
        , USBranchNonUSMtlLBS DECIMAL(20, 0)        
        , WGTShippedLBS DECIMAL(20, 0)        
        , PiecesShipped DECIMAL(20, 0)        
        , BOL DECIMAL(20, 0)        
        , TtlHour DECIMAL(20, 0)        
        , WTPerHr DECIMAL(20, 0)        
        , BOLPerHr DECIMAL(20, 0)        
        , PiecesPerHr DECIMAL(20, 0)      
        ,Branch Varchar(3)      
        , DBName Varchar(3)    
        , ShotageInvWgt DECIMAL(20, 0)     
        ,MarketingSpendVal DECIMAL(20, 0)     
        ,CostQualityCert DECIMAL(20, 0)     
        ,LumberSpendVAl Decimal(20,0)    
        ,WaterjetSpendVAl Decimal(20,0)    
        ,RMSpendVAl Decimal(20,0)    
        ,OtherWhsSpendVAl Decimal(20,0)    
        ,SawBladeSpendVAl Decimal(20,0)    
        ,ForkliftSpendVAl Decimal(20,0)    
        ,JanitorialSpendVAl Decimal(20,0)    
        ,OfficeSpendVAl Decimal(20,0)    
        ,BuildingsSpendVAl Decimal(20,0)    
        ,RefusesSpendVAl Decimal(20,0)    
        ,SafetySpendVAl Decimal(20,0)    
        ,EquipmentSpendVAl Decimal(20,0)    
                   );            
              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(5);              
DECLARE @Name VARCHAR(15);              
DECLARE @CurrenyRate varchar(15);        
    
IF @DBNAME = 'ALL'          
 BEGIN          
       
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_V2      
    OPEN ScopeCursor;      
             
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
     WHILE @@FETCH_STATUS = 0          
       BEGIN          
      SET @DB= @Prefix          
     -- print(@DB)          
        DECLARE @query NVARCHAR(4000);          
         IF (UPPER(@Prefix) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@Prefix) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@Prefix) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@Prefix) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@Prefix) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End  
 Else if(UPPER(@Prefix) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End  
        
              
-- TOTAL Multi-Metals INVENTORY VALUE ($)      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalInvVal)         
 select '''+ @DB +''', stn_shpt_brh, SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'') group by stn_shpt_brh               
'          
 print @query;            
        
 EXECUTE sp_executesql @query;    
       
-- EXCESS Multi-Metals INVENTORY WGHT. (5%)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,ExcessMultMetalInvWgt5Pct)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh , '        
          
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
  Begin        
         
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
Else         
Begin        
      
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
      
SET @query = @query + '        
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
and SUBSTRING(currentPrdData.stn_frm,1,1) not in  (''T'')            
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery  '      
       
print @query;            
        
 EXECUTE sp_executesql @query;      
     
 -- TOTAL Multi-Metals INVENTORY WGHT. (lbs.)       
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalInvWgt)         
 select '''+ @DB +''', stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' SUM(stn_blg_wgt * 2.20462) as LBShipped'                  
           ELSE                  
            SET @query = @query + ' SUM(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'') group by stn_shpt_brh               
'          
 print @query;            
        
 EXECUTE sp_executesql @query;    
     
 -- ShotageInvWgt    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ShotageInvWgt)        
 Select '''+ @DB +''', t.prd_brh,  '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + '     
             case         
    when Sum(SalesHist12M_Avg) > 2000 then (Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then (Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then (Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then (Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then (Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then (Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462 Else 0 END as Shortage'                  
           ELSE                  
            SET @query = @query + ' case         
    when Sum(SalesHist12M_Avg) > 2000 then Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs) Else 0 END as Shortage '                  
                             
           SET @query = @query + '        
 from(      
 select prd_cmpy_id as [Database], prd_brh,       
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,        
       (Select SUM(SAT_BLG_WGT)/12 from '+ @DB +'_sahsat_rec where  SAT_INV_DT >=''' + @ShtgFD + ''' and sat_shpt_brh = prd_brh ) as SalesHist12M_Avg,        
       (select sum(potpod_rec.pod_bal_wgt) from '+ @DB +'_tctipd_rec join '+ @DB +'_potpod_rec as potpod_rec        
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''        
       where IPD_CMPY_ID = prd_cmpy_id and ipd_brh =prd_brh) as IncomingLbs        
       from '+ @DB +'_intprd_rec          
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')         
       group by  prd_cmpy_id ,prd_brh) as t group by t.prd_brh; '      
     print @query;            
        
 EXECUTE sp_executesql @query;      
 -- SHORTAGE Multi-Metals WEIGHT (Lbs.)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,ShotageMultMetalInvWgt)        
 Select '''+ @DB +''', t.prd_brh,  '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + '     
             case         
    when Sum(SalesHist12M_Avg) > 2000 then (Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then (Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then (Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then (Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then (Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462       
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then (Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462 Else 0 END as Shortage'                  
           ELSE                  
            SET @query = @query + ' case         
    when Sum(SalesHist12M_Avg) > 2000 then Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs) Else 0 END as Shortage '                  
                             
           SET @query = @query + '        
 from(      
 select prd_cmpy_id as [Database], prd_brh,       
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,        
       (Select SUM(SAT_BLG_WGT)/12 from '+ @DB +'_sahsat_rec where  SAT_INV_DT >=''' + @ShtgFD + ''' and sat_shpt_brh = prd_brh ) as SalesHist12M_Avg,        
       (select sum(potpod_rec.pod_bal_wgt) from '+ @DB +'_tctipd_rec join '+ @DB +'_potpod_rec as potpod_rec        
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''        
       where IPD_CMPY_ID = prd_cmpy_id and ipd_brh =prd_brh) as IncomingLbs        
       from '+ @DB +'_intprd_rec          
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')   and SUBSTRING(prd_frm,1,1) not in  (''T'')      
       group by  prd_cmpy_id ,prd_brh) as t group by t.prd_brh; '      
     print @query;            
        
 EXECUTE sp_executesql @query;      
     
 -- TtlMultMetalBuyoutVal    
        
SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalBuyoutVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,                 
          SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
     print @query;            
        
 EXECUTE sp_executesql @query;      
    
      
 -- TOTAL Multi-Metals BUYOUTS-WGHT     
      
SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalBuyoutWgt)        
                                     
   select '''+ @DB +''', stn_shpt_brh,'                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;    
                     
-- BHM Multi-metals KPI (Lbs. Shipped)      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BHMMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '         
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                            
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''BHM''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
       
-- CRP Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,CRPMultMetalLBS)        
                                  
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                             
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''CRP''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- EXP Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,EXPMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''EXP''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- HIB Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,HIBMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '     
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''HIB''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
       
-- IND Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,INDMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''IND''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- JAC Multi-metals KPI (Lbs. Shipped)       
SET @query = 'INSERT INTO #tmp(DBName, Branch,JACMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''JAC''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- LAX Multi-metals KPI (Lbs. Shipped)      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LAXMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''LAX''             
     group by  stn_shpt_brh              
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- MTL Multi-metals KPI (Lbs. Shipped)       
SET @query = 'INSERT INTO #tmp(DBName, Branch,MTLMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''MTL''             
      group by  stn_shpt_brh             
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- ROS Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,ROSMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''ROS''             
    group by  stn_shpt_brh                 
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- SEA Multi-metals KPI (Lbs. Shipped)       
SET @query = 'INSERT INTO #tmp(DBName, Branch,SEAMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                             
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''SEA''             
    group by  stn_shpt_brh                 
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- SHA Multi-metals KPI (Lbs. Shipped)      
SET @query = 'INSERT INTO #tmp(DBName, Branch,SHAMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''SHA''             
     group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- TAI Multi-metals KPI (Lbs. Shipped)       
SET @query = 'INSERT INTO #tmp(DBName, Branch,TAIMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''TAI''             
    group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
      
-- WDL Multi-metals KPI (Lbs. Shipped)       
SET @query = 'INSERT INTO #tmp(DBName, Branch,WDLMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''WDL''     
   group by  stn_shpt_brh              
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
       
 -- Total Sales $ Shipped      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlSalesShipped)        
                                     
   select '''+ @DB +''',stn_shpt_brh, SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh             
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
       
 -- Total Lbs. Shipped      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlLBSShipped)        
                                     
   select '''+ @DB +''',stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                  
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;      
       
 -- Total Net Profit %      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlNetProfitPCt)        
                                     
   select '''+ @DB +''',stn_shpt_brh,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;      
       
 -- Total Operating Expenses      
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlOperatingExp)        
                                     
   select '''+ @DB +''',stn_shpt_brh,SUM(ISNULL(stn_tot_repl_val,0) )* '+ @CurrenyRate +'  as openExp                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                 
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;     
     
 -- US Branch Excess Lbs. Sold USBranchExcessLBS    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,USBranchExcessLBS)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh , '        
          
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
  Begin        
         
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
Else         
Begin        
      
        SET @query = @query + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
      
SET @query = @query + '        
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery '      
       
print @query;            
        
 EXECUTE sp_executesql @query;      
     
 -- TOTAL BUYOUTS - VALUE $   (1%)     
      
SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlBuyOutVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
     and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;    
     
 -- TOTAL BUYOUTS - WEIGHT Lbs.  (1%)    
      
SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlBuyOutWgt)        
                                     
   select '''+ @DB +''', stn_shpt_brh,'                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @query = @query + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
    and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;    
     
 -- US Branch Net Profit $     
 SET @query = 'INSERT INTO #tmp( DBName, Branch,USBranchNetProfit)     
 select   '''+ @DB +''' as Databases, replace(stn_shpt_brh, ''SFS'',''LAX'') as stn_shpt_brh, SUM(ISNULL(stn_npft_avg_val,0) * 1) as GP$MTD     
           from '+ @DB +'_sahstn_rec             
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )        
           group by Replace(stn_shpt_brh,''SFS'',''LAX'')     
 '    
 print @query;            
        
 EXECUTE sp_executesql @query;    
     
  -- US booked val            
        SET @query = 'INSERT INTO #tmp( DBName, Branch,USBranchBooking)           
                   
SELECT  '''+ @DB +''',a.mbk_brh,Sum(a.mbk_tot_val* '+ @CurrenyRate +') as mbk_tot_val          
      FROM '+ @DB +'_ortmbk_rec a         
      join '+ @DB +'_ortorh_rec on orh_ord_pfx = a.mbk_ord_pfx and orh_ord_no = a.mbk_ord_no            
      INNER JOIN '+ @DB +'_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id            
      left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
      WHERE a.mbk_ord_pfx=''SO''             
        AND a.mbk_ord_itm<>999             
         AND a.mbk_actvy_dt between '''+@FD+''' And '''+@TD+'''          
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
         group by  a.mbk_brh          
         '           
                   
          print @query;            
        
 EXECUTE sp_executesql @query;    
     
  --US branch MultiMetal LBs         
           
 SET @query = 'INSERT INTO #tmp(DBName, Branch,USBranchMulMtlLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @query = @query + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @query = @query + ' sum(stn_blg_wgt) as LBShipped'                 
                             
           SET @query = @query + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')                
   group by  stn_shpt_brh                 
   '          
    print @query;            
        
 EXECUTE sp_executesql @query;          
           
  -- US Bol            
SET @query = 'INSERT INTO #tmp(DBName, Branch,BOL)         
 SELECT  '''+ @DB +''',whs_mng_brh,COUNT(*) FROM           
'+ @DB +'_mxtarh_Rec           
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no          
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''          
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs          
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1          
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''  group by whs_mng_brh           
'          
   print @query;            
        
 EXECUTE sp_executesql @query;     
     
 -- US Branch Active Customer Base    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,USBranchActiveCusBase)     
 select '''+ @DB +''', replace(CUS_ADMIN_BRH,''SFS'',''LAX'') as CUS_ADMIN_BRH   , count(crd_cus_id)    
              FROM (select  stn_sld_cus_id as CustomerID      
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @TD +'''       
              group by stn_sld_cus_id ) as t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              where t.CustomerID = crd_cus_id      
              Group by CUS_ADMIN_BRH'      
               print @query;        
          
 EXECUTE sp_executesql @query;                  
    
-- Non US Metal Titaniumn LBS Sold    
       
   SET @query = 'INSERT INTO #tmp(DBName, Branch,USBranchNonUSMtlLBS)      
      Select '''+ @DB +''',t.stn_shpt_brh  , sum(wgt) as lbs     
   from (                             
   select Replace(stn_shpt_brh,''SFS'',''LAX'') as stn_shpt_brh , stn_frm as form ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')     
  Begin    
    SET @query = @query + ' sum(stn_blg_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @query = @query + ' sum(stn_blg_wgt) as wgt '     
   End                        
     SET @query = @query + '                           
    from '+ @DB +'_sahstn_rec                                                  
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''         
   and SUBSTRING(stn_frm,1,1) in  (''T'') and stn_fnsh != ''US''           
   group by stn_shpt_brh, stn_frm                
   ) as t        
   group by t.stn_shpt_brh'      
    print @query;        
           
 EXECUTE sp_executesql @query;    
     
 -- WGT SHIPPED: Lbs.    
SET @query = 'INSERT INTO #tmp(DBName, Branch,WGTShippedLBS)      
      Select '''+ @DB +''',Replace(whs_mng_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @query = @query + ' sum(stn_blg_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @query = @query + ' sum(stn_blg_wgt) as wgt '     
   End                        
     SET @query = @query + ' FROM  '+ @DB +'_sahstn_rec        
join  '+ @DB +'_scrwhs_rec on   whs_cmpy_id = stn_cmpy_id  and whs_whs = stn_shpg_whs     
 join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
         --   and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )          
           group by Replace(whs_mng_brh,''SFS'',''LAX'') '      
   print @query;        
    
 EXECUTE sp_executesql @query;     
     
 -- PIECES SHIPPED:      
SET @query = 'INSERT INTO #tmp(DBName, Branch,PiecesShipped)      
      Select '''+ @DB +''', Replace(whs_mng_brh,''SFS'',''LAX'') as Branch , SUM(stn_blg_pcs)     
FROM  '+ @DB +'_sahstn_rec        
join  '+ @DB +'_scrwhs_rec on   whs_cmpy_id = stn_cmpy_id  and whs_whs = stn_shpg_whs     
 join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
        --   and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )           
           group by Replace(whs_mng_brh,''SFS'',''LAX'')'      
   print @query;        
    
 EXECUTE sp_executesql @query;      
     
 -- TOTAL HOURS:    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlHour)      
      Select '''+ @DB +''', Replace(BranchPrefix,''SFS'',''LAX''), sum(KPIHour)/(COUNT(*)*5) as oneDayh from tbl_itech_MonthlyKPI     
      Where KPIHour != 0 and KPIHour is not null and KPIDate between '''+ @FD +''' and '''+ @TD +'''    
       group by Replace(BranchPrefix,''SFS'',''LAX'')'    
     
     print(@query)            
        EXECUTE sp_executesql @query;      
    
-- MarketingSpendVal     
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
  where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
 EXECUTE sp_executesql @query;     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End  
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000''   '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End  
     
 -- ObsoleteMultMetalInvWgt    
SET @query = 'INSERT INTO #tmp(DBName, Branch,ObsoleteMultMetalInvWgt)      
       Select '''+ @DB +''', Replace(prd_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @query = @query + ' Sum(prd_ohd_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @query = @query + ' Sum(prd_ohd_wgt) as wgt '     
   End                        
     SET @query = @query + ' FROM  '+ @DB +'_intprd_rec where  prd_invt_sts = ''S''    
and (    
Select SUM(SAT_BLG_WGT ) from '+ @DB +'_sahsat_rec where  SAT_FRM  = prd_frm and SAT_GRD = prd_grd and  sat_size = prd_size and  sat_fnsh = prd_fnsh     
                 
       and SAT_INV_DT  between '''+ @ShtgFD +''' and '''+ @FD +''') = 0 and SUBSTRING(prd_frm,1,1) not in  (''T'')     
       group by  Replace(prd_brh,''SFS'',''LAX'')        
        '    
  print @query;        
 EXECUTE sp_executesql @query;     
    
-- TtlReturnVal    
SET @query = 'INSERT INTO #tmp(DBName, Branch,TtlReturnVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
     and stn_tot_val < 0            
   group by  stn_shpt_brh                
   '       
     print @query;        
 EXECUTE sp_executesql @query;       
     
-- CostQualityCert     
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
 EXECUTE sp_executesql @query;     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
      from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
 EXECUTE sp_executesql @query;    
 End  
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '      
  print @query;        
 EXECUTE sp_executesql @query;    
 End  
      
-- ExcessTitaniumInvVal    
SET @query = 'INSERT INTO #tmp(DBName, Branch,ExcessTitaniumInvVal)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh ,    
          
  ISNULL(SUM(ISNULL(currentPrdData.stn_tot_val ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(sat_tot_val ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_mat_val ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg     
       
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery '      
       
print @query;        
 EXECUTE sp_executesql @query;    
       
-- ObsoleteTitaniumWgt    
SET @query = 'INSERT INTO #tmp(DBName, Branch,ObsoleteTitaniumWgt)      
       Select '''+ @DB +''', Replace(prd_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @query = @query + ' Sum(prd_ohd_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @query = @query + ' Sum(prd_ohd_wgt) as wgt '     
   End                        
     SET @query = @query + ' FROM  '+ @DB +'_intprd_rec where  prd_invt_sts = ''S''    
and (    
Select SUM(SAT_BLG_WGT ) from '+ @DB +'_sahsat_rec where  SAT_FRM  = prd_frm and SAT_GRD = prd_grd and  sat_size = prd_size and  sat_fnsh = prd_fnsh     
                 
       and SAT_INV_DT  between '''+ @ShtgFD +''' and '''+ @FD +''') = 0 and SUBSTRING(prd_frm,1,1)  in  (''T'')     
       group by  Replace(prd_brh,''SFS'',''LAX'')        
        '    
  print @query;        
 EXECUTE sp_executesql @query;     
    
-- 20170328    
-- LumberSpendVAl    
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
     Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000''    '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
    
-- WaterjetSpendVAl    
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
   union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End     
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000''    '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
    
-- RMSpendVAl    
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
     
-- OtherWhsSpendVAl     
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'''    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
     
-- SawBladeSpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees              
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End  
     
 -- ForkliftSpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End  
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'''    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End  
     
-- JanitorialSpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '       
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'''    
  print @query;       
 EXECUTE sp_executesql @query;     
 End  
     
 -- OfficeSpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
 union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
 EXECUTE sp_executesql @query;     
 End  
     
 -- BuildingsSpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
  union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
     
-- RefusesSpendVAl    
    
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000''  '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
     
-- SafetySpendVAl    
 if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
     
 --   EquipmentSpendVAl    
if(@DB = 'US')    
 begin       
SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @query;        
       
 EXECUTE sp_executesql @query;      
     
     
     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @query = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @query;        
       
 EXECUTE sp_executesql @query;     
 End   
            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;          
       END           
    CLOSE ScopeCursor;          
    DEALLOCATE ScopeCursor;          
  END          
  ELSE          
     BEGIN           
       print 'starting' ;          
      IF (UPPER(@DBNAME) = 'TW')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End          
    Else if (UPPER(@DBNAME) = 'NO')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))          
    End          
    Else if (UPPER(@DBNAME) = 'CA')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))          
    End          
    Else if (UPPER(@DBNAME) = 'CN')          
    begin          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))          
    End          
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')          
    begin          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))          
    End          
    Else if(UPPER(@DBNAME) = 'UK')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))          
    End          
    Else if(UPPER(@DBNAME) = 'TWCN')          
    begin          
       SET @DB ='TW'          
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))          
    End  
 Else if(UPPER(@DBNAME) = 'DE')          
    begin          
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))          
    End   
             
-- TOTAL Multi-Metals INVENTORY VALUE ($)      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalInvVal)         
 select '''+ @DB +''', stn_shpt_brh, SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'') group by stn_shpt_brh               
'          
 print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
       
-- EXCESS Multi-Metals INVENTORY WGHT. (5%)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ExcessMultMetalInvWgt5Pct)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh , '        
          
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
  Begin        
         
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
Else         
Begin        
      
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
      
SET @sqltxt = @sqltxt + '        
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
and SUBSTRING(currentPrdData.stn_frm,1,1) not in  (''T'')            
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery  '      
       
print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
     
 -- TOTAL Multi-Metals INVENTORY WGHT. (lbs.)       
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalInvWgt)         
 select '''+ @DB +''', stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt * 2.20462) as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' SUM(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'') group by stn_shpt_brh               
'          
 print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
 -- ShotageInvWgt    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ShotageInvWgt)        
 Select '''+ @DB +''', t.prd_brh,  '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + '     
             case         
    when Sum(SalesHist12M_Avg) > 2000 then (Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then (Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then (Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then (Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then (Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then (Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462 Else 0 END as Shortage'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' case         
    when Sum(SalesHist12M_Avg) > 2000 then Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs) Else 0 END as Shortage '                  
                             
           SET @sqltxt = @sqltxt + '        
 from(      
 select prd_cmpy_id as [Database], prd_brh,       
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,        
       (Select SUM(SAT_BLG_WGT)/12 from '+ @DB +'_sahsat_rec where  SAT_INV_DT >=''' + @ShtgFD + ''' and sat_shpt_brh = prd_brh ) as SalesHist12M_Avg,        
       (select sum(potpod_rec.pod_bal_wgt) from '+ @DB +'_tctipd_rec join '+ @DB +'_potpod_rec as potpod_rec        
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''        
       where IPD_CMPY_ID = prd_cmpy_id and ipd_brh =prd_brh) as IncomingLbs        
       from '+ @DB +'_intprd_rec          
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')         
       group by  prd_cmpy_id ,prd_brh) as t group by t.prd_brh; '      
     print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
 -- SHORTAGE Multi-Metals WEIGHT (Lbs.)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ShotageMultMetalInvWgt)        
 Select '''+ @DB +''', t.prd_brh,  '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + '     
             case         
    when Sum(SalesHist12M_Avg) > 2000 then (Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then (Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then (Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then (Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then (Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then (Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs))* 2.20462 Else 0 END as Shortage'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' case         
    when Sum(SalesHist12M_Avg) > 2000 then Sum(AvlLbs) - (4*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 1000 AND Sum(SalesHist12M_Avg) <= 2000 then Sum(AvlLbs) - (3*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 750 AND Sum(SalesHist12M_Avg) <= 1000 then Sum(AvlLbs) - (2.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 500 AND Sum(SalesHist12M_Avg) <= 750 then Sum(AvlLbs) - (2*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 350 AND Sum(SalesHist12M_Avg) <= 500 then Sum(AvlLbs) - (1.5*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs)        
    when Sum(SalesHist12M_Avg) >= 150 AND Sum(SalesHist12M_Avg) <= 350 then Sum(AvlLbs) - (1*Sum(SalesHist12M_Avg)) + Sum(IncomingLbs) Else 0 END as Shortage '             
                             
           SET @sqltxt = @sqltxt + '        
 from(      
 select prd_cmpy_id as [Database], prd_brh,       
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,        
       (Select SUM(SAT_BLG_WGT)/12 from '+ @DB +'_sahsat_rec where  SAT_INV_DT >=''' + @ShtgFD + ''' and sat_shpt_brh = prd_brh ) as SalesHist12M_Avg,        
       (select sum(potpod_rec.pod_bal_wgt) from '+ @DB +'_tctipd_rec join '+ @DB +'_potpod_rec as potpod_rec        
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''        
       where IPD_CMPY_ID = prd_cmpy_id and ipd_brh =prd_brh) as IncomingLbs        
       from '+ @DB +'_intprd_rec          
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')   and SUBSTRING(prd_frm,1,1) not in  (''T'')      
       group by  prd_cmpy_id ,prd_brh) as t group by t.prd_brh; '      
     print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
    
-- TtlMultMetalBuyoutVal    
        
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalBuyoutVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,                 
          SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
 -- TOTAL Multi-Metals BUYOUTS-WGHT     
      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlMultMetalBuyoutWgt)        
                                     
   select '''+ @DB +''', stn_shpt_brh,'                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
                     
-- BHM Multi-metals KPI (Lbs. Shipped)      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BHMMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                            
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''BHM''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
       
-- CRP Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CRPMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                             
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''CRP''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- EXP Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EXPMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''EXP''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- HIB Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,HIBMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '     
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''HIB''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
       
-- IND Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,INDMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''IND''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- JAC Multi-metals KPI (Lbs. Shipped)       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JACMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                               
    from '+ @DB +'_sahstn_rec                    
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''JAC''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- LAX Multi-metals KPI (Lbs. Shipped)      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LAXMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''LAX''             
     group by  stn_shpt_brh              
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- MTL Multi-metals KPI (Lbs. Shipped)       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MTLMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''MTL''             
      group by  stn_shpt_brh             
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- ROS Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ROSMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''ROS''             
    group by  stn_shpt_brh                 
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- SEA Multi-metals KPI (Lbs. Shipped)       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SEAMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                             
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''SEA''             
    group by  stn_shpt_brh        
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- SHA Multi-metals KPI (Lbs. Shipped)      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SHAMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''SHA''             
     group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- TAI Multi-metals KPI (Lbs. Shipped)       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TAIMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''TAI''             
    group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
      
-- WDL Multi-metals KPI (Lbs. Shipped)       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WDLMultMetalLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh, '                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')   and stn_shpt_brh = ''WDL''     
   group by  stn_shpt_brh              
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
       
 -- Total Sales $ Shipped      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlSalesShipped)        
                                     
   select '''+ @DB +''',stn_shpt_brh, SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh             
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
       
 -- Total Lbs. Shipped      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlLBSShipped)        
                                     
   select '''+ @DB +''',stn_shpt_brh,'                
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                  
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
       
 -- Total Net Profit %      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlNetProfitPCt)        
                                     
   select '''+ @DB +''',stn_shpt_brh,(Case SUM(ISNULL(stn_tot_val,0)) When 0 then 0 else (SUM(ISNULL(stn_npft_avg_val,0))/SUM(ISNULL(stn_tot_val,0))*100) end ) as GPPct                               
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
       
 -- Total Operating Expenses      
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlOperatingExp)        
                                     
   select '''+ @DB +''',stn_shpt_brh,SUM(ISNULL(stn_tot_repl_val,0) )* '+ @CurrenyRate +'  as openExp                              
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   group by  stn_shpt_brh                 
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
     
 -- US Branch Excess Lbs. Sold USBranchExcessLBS    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,USBranchExcessLBS)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh , '        
          
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
  Begin        
         
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT * 2.20462,0)),0) as periodSelRange,        
        (select ISNULL(SUM(ISNULL(SAT_BLG_WGT * 2.20462,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
Else         
Begin        
      
        SET @sqltxt = @sqltxt + ' ISNULL(SUM(ISNULL(currentPrdData.Stn_BLG_WGT ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(SAT_BLG_WGT ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg '        
end        
      
SET @sqltxt = @sqltxt + '        
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery '      
       
print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);       
     
 -- TOTAL BUYOUTS - VALUE $   (1%)     
      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlBuyOutVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
     and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
 -- TOTAL BUYOUTS - WEIGHT Lbs.  (1%)    
      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlBuyOutWgt)        
                                     
   select '''+ @DB +''', stn_shpt_brh,'                  
           if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
    and stn_sls_cat = ''BO''             
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
 -- US Branch Net Profit $     
 SET @sqltxt = 'INSERT INTO #tmp( DBName, Branch,USBranchNetProfit)     
 select   '''+ @DB +''' as Databases, replace(stn_shpt_brh, ''SFS'',''LAX'') as stn_shpt_brh, SUM(ISNULL(stn_npft_avg_val,0) * 1) as GP$MTD     
           from '+ @DB +'_sahstn_rec             
           join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
           and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )        
           group by Replace(stn_shpt_brh,''SFS'',''LAX'')     
 '    
 print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
  -- US booked val            
        SET @sqltxt = 'INSERT INTO #tmp( DBName, Branch,USBranchBooking)           
                   
SELECT  '''+ @DB +''',a.mbk_brh,Sum(a.mbk_tot_val* '+ @CurrenyRate +') as mbk_tot_val          
      FROM '+ @DB +'_ortmbk_rec a         
      join '+ @DB +'_ortorh_rec on orh_ord_pfx = a.mbk_ord_pfx and orh_ord_no = a.mbk_ord_no            
      INNER JOIN '+ @DB +'_arrcus_rec b ON a.mbk_sld_cus_id=b.cus_cus_id            
      left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat           
      WHERE a.mbk_ord_pfx=''SO''             
        AND a.mbk_ord_itm<>999             
         AND a.mbk_actvy_dt between '''+@FD+''' And '''+@TD+'''          
         and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )         
         group by  a.mbk_brh          
         '           
                   
          print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
  --US branch MultiMetal LBs         
           
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,USBranchMulMtlLBS)        
                                     
   select '''+ @DB +''',stn_shpt_brh,'                
           if (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                     
             SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt* 2.20462)  as LBShipped'                  
           ELSE                  
            SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as LBShipped'                  
                             
           SET @sqltxt = @sqltxt + '                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
   and SUBSTRING(stn_frm,1,1) not in  (''T'')                
   group by  stn_shpt_brh                 
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);           
           
  -- US Bol            
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BOL)         
 SELECT  '''+ @DB +''',whs_mng_brh,COUNT(*) FROM           
'+ @DB +'_mxtarh_Rec           
INNER JOIN '+ @DB +'_trjtph_rec ON substring(SUBSTRING(arh_prim_ref, 3, 8), patindex(''%[^0]%'',SUBSTRING(arh_prim_ref, 3, 8)), LEN(SUBSTRING(arh_prim_ref, 3, 8))) = tph_transp_no          
AND arh_cmpy_id = tph_cmpy_id AND tph_transp_pfx = ''TR''          
join '+ @DB +'_scrwhs_rec on whs_cmpy_id = tph_cmpy_id and whs_whs = tph_trpln_whs          
WHERE arh_bus_Doc_Typ = ''PKL'' AND arh_arch_ver_No =  1          
AND arh_gen_dtts BETWEEN '''+@FD+''' And '''+@TD+'''  group by whs_mng_brh           
'          
   print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);      
     
 -- US Branch Active Customer Base    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,USBranchActiveCusBase)     
 select '''+ @DB +''', replace(CUS_ADMIN_BRH,''SFS'',''LAX'') as CUS_ADMIN_BRH   , count(crd_cus_id)    
              FROM (select  stn_sld_cus_id as CustomerID      
              from '+ @DB +'_sahstn_rec        
              where STN_INV_DT > '''+  @LTD + ''' and STN_INV_DT <= ''' + @TD +'''       
              group by stn_sld_cus_id ) as t , '+ @DB +'_arrcrd_rec join '+ @DB +'_arrcus_rec on crd_cus_id = cus_cus_id         
              where t.CustomerID = crd_cus_id      
              Group by CUS_ADMIN_BRH'      
               print @sqltxt;        
  set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);                   
    
-- Non US Metal Titaniumn LBS Sold    
       
   SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,USBranchNonUSMtlLBS)      
      Select '''+ @DB +''',t.stn_shpt_brh  , sum(wgt) as lbs     
   from (                             
   select Replace(stn_shpt_brh,''SFS'',''LAX'') as stn_shpt_brh , stn_frm as form ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as wgt '     
   End                        
     SET @sqltxt = @sqltxt + '                           
    from '+ @DB +'_sahstn_rec                                                  
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''         
   and SUBSTRING(stn_frm,1,1) in  (''T'') and stn_fnsh != ''US''           
   group by stn_shpt_brh, stn_frm                
   ) as t        
   group by t.stn_shpt_brh'      
    print @sqltxt;        
   set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
 -- WGT SHIPPED: Lbs.    
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WGTShippedLBS)      
      Select '''+ @DB +''',Replace(whs_mng_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @sqltxt = @sqltxt + ' sum(stn_blg_wgt) as wgt '     
   End                        
     SET @sqltxt = @sqltxt + ' FROM  '+ @DB +'_sahstn_rec        
join  '+ @DB +'_scrwhs_rec on   whs_cmpy_id = stn_cmpy_id  and whs_whs = stn_shpg_whs     
 join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
         --  and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )          
           group by Replace(whs_mng_brh,''SFS'',''LAX'') '      
   print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
     
 -- PIECES SHIPPED:      
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,PiecesShipped)      
      Select '''+ @DB +''', Replace(whs_mng_brh,''SFS'',''LAX'') as Branch , SUM(stn_blg_pcs)     
FROM  '+ @DB +'_sahstn_rec        
join  '+ @DB +'_scrwhs_rec on   whs_cmpy_id = stn_cmpy_id  and whs_whs = stn_shpg_whs     
 join '+ @DB +'_arrcus_rec on cus_cmpy_id = stn_cmpy_id and cus_cus_id = stn_sld_cus_id       
           left join '+ @DB +'_arrcuc_rec on cuc_cus_cat = cus_cus_cat                  
           where stn_inv_Dt >= '''+@FD+''' and stn_inv_dt <= '''+@TD+'''             
         --   and (cuc_desc30 <> ''Interco'' or cuc_desc30 is null )           
           group by Replace(whs_mng_brh,''SFS'',''LAX'')'      
   print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);       
     
 -- TOTAL HOURS:    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlHour)      
      Select '''+ @DB +''', Replace(BranchPrefix,''SFS'',''LAX''), sum(KPIHour)/(COUNT(*)*5) as oneDayh from tbl_itech_MonthlyKPI     
      Where KPIHour != 0 and KPIHour is not null and KPIDate between '''+ @FD +''' and '''+ @TD +'''    
       group by Replace(BranchPrefix,''SFS'',''LAX'')'    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
     
 -- MarketingSpendVal     
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,MarketingSpendVal)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6290,6640,6620,6615,6660,6625) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000''    '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
    
-- ObsoleteMultMetalInvWgt    
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ObsoleteMultMetalInvWgt)      
       Select '''+ @DB +''', Replace(prd_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @sqltxt = @sqltxt + ' Sum(prd_ohd_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @sqltxt = @sqltxt + ' Sum(prd_ohd_wgt) as wgt '     
   End                        
     SET @sqltxt = @sqltxt + ' FROM  '+ @DB +'_intprd_rec where  prd_invt_sts = ''S''    
and (    
Select SUM(SAT_BLG_WGT ) from '+ @DB +'_sahsat_rec where  SAT_FRM  = prd_frm and SAT_GRD = prd_grd and  sat_size = prd_size and  sat_fnsh = prd_fnsh     
                 
       and SAT_INV_DT  between '''+ @ShtgFD +''' and '''+ @FD +''') = 0 and SUBSTRING(prd_frm,1,1) not in  (''T'')     
       group by  Replace(prd_brh,''SFS'',''LAX'')        
        '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
     
-- TtlReturnVal    
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,TtlReturnVal)        
                                     
   select '''+ @DB +''', stn_shpt_brh,SUM(ISNULL(stn_tot_val,0))* '+ @CurrenyRate +'                                
    from '+ @DB +'_sahstn_rec                                                      
   where STN_INV_DT  Between  '''+@FD+''' And '''+@TD+'''           
     and stn_tot_val < 0   
   group by  stn_shpt_brh                
   '          
    print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
-- CostQualityCert    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);      
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,CostQualityCert)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6535) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'''    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
-- ExcessTitaniumInvVal    
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ExcessTitaniumInvVal)        
   SELECT '''+ @DB +''', stn_shpt_brh, (case when (periodSelRange) < ((prd_ohd_wg) - (InvoiceMonthTotal)) then (periodSElRange)     
   else ((prd_ohd_wg) - (InvoiceMonthTotal)) end)  as excess     
  FROM       
  (         
  SELECT currentPrdData.stn_sld_cus_id as CustID,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh,       
  currentPrdData.stn_shpt_brh ,    
          
  ISNULL(SUM(ISNULL(currentPrdData.stn_tot_val ,0)),0) as periodSelRange,        
         (select ISNULL(SUM(ISNULL(sat_tot_val ,0)),0)  from ' + @DB + '_sahsat_rec sixMonthWgt where         
sixMonthWgt.sat_frm = currentPrdData.stn_frm and sixMonthWgt.sat_grd = currentPrdData.stn_grd and sixMonthWgt.sat_size = currentPrdData.stn_size        
and sixMonthWgt.sat_fnsh = currentPrdData.stn_fnsh and sixMonthWgt.sat_shpt_brh = currentPrdData.stn_shpt_brh and         
sixMonthWgt.sat_inv_dt between  ''' + @ExcessFD + '''  And ''' + @ExcessTD + ''') as InvoiceMonthTotal,        
        
(select ISNULL(SUM(ISNULL(prd_ohd_mat_val ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = currentPrdData.stn_FRM         
and prd_grd = currentPrdData.stn_GRD         
and prd_size = currentPrdData.stn_size and prd_fnsh = currentPrdData.stn_fnsh and prd_invt_sts = ''S''         
and UpdateDtTm between ''' + @FD2 + '''  And ''' + @TD2 + '''  ) as prd_ohd_wg     
       
FROM '+ @DB +'_sahstn_rec currentPrdData          
WHERE currentPrdData.Stn_INV_DT  between ''' + @FD +''' and ''' +@TD+ '''  and currentPrdData.stn_frm <> ''XXXX''        
group by currentPrdData.stn_sld_cus_id, currentPrdData.stn_shpt_brh ,currentPrdData.Stn_FRM,currentPrdData.Stn_GRD,currentPrdData.stn_size, currentPrdData.stn_fnsh          
             
) as oquery '      
       
print @sqltxt;            
set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
     
-- ObsoleteTitaniumWgt    
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ObsoleteTitaniumWgt)      
       Select '''+ @DB +''', Replace(prd_brh,''SFS'',''LAX'') as Branch ,'    
    if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')      
  Begin    
    SET @sqltxt = @sqltxt + ' Sum(prd_ohd_wgt)* 2.20462 as wgt '      
   end    
   else    
   Begin    
    SET @sqltxt = @sqltxt + ' Sum(prd_ohd_wgt) as wgt '     
   End                        
     SET @sqltxt = @sqltxt + ' FROM  '+ @DB +'_intprd_rec where  prd_invt_sts = ''S''    
and (    
Select SUM(SAT_BLG_WGT ) from '+ @DB +'_sahsat_rec where  SAT_FRM  = prd_frm and SAT_GRD = prd_grd and  sat_size = prd_size and  sat_fnsh = prd_fnsh     
                 
       and SAT_INV_DT  between '''+ @ShtgFD +''' and '''+ @FD +''') = 0 and SUBSTRING(prd_frm,1,1)  in  (''T'')     
       group by  Replace(prd_brh,''SFS'',''LAX'')        
        '    
  print @sqltxt;        
 set @execSQLtxt = @sqltxt;           
 EXEC (@execSQLtxt);     
    
-- 20170328    
-- LumberSpendVAl    
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);   
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,LumberSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6470) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'''    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End  
    
-- WaterjetSpendVAl    
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
     Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);   
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,WaterjetSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6476) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End  
    
-- RMSpendVAl    
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);   
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RMSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6365) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
     
-- OtherWhsSpendVAl     
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);    
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OtherWhsSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6480) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End  
     
-- SawBladeSpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
      Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);  
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SawBladeSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6475) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
     
 -- ForkliftSpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,ForkliftSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6350) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
     
-- JanitorialSpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);   
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,JanitorialSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6325) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End  
     
 -- OfficeSpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
  from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);   
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees        
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,OfficeSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6430) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End  
     
 -- BuildingsSpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);  
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,BuildingsSpendVAL)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6360) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
EXEC (@execSQLtxt);     
 End   
     
-- RefusesSpendVAl    
    
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);    
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,RefusesSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6330) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
     
-- SafetySpendVAl    
 if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);  
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End     
  Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,SafetySpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6478) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
     
 --   EquipmentSpendVAl    
if(@DB = 'US')    
 begin       
SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
      Select '''+ @DB +''', ''CRP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       Union    
       Select '''+ @DB +''', ''EXP'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''700000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''HIB'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''550000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''IND'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''950000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''JAC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''400000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''LAX'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''810000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROS'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''050000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''SEA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''850000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''WDL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''600000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ROC'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from US_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''100000000000000000000000000000''     
       '               
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);  
 End    
 Else if(@DB = 'UK')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''BHM'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from UK_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CA')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
      Select '''+ @DB +''', ''MTL'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                 
       from CA_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'CN')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
     Select '''+ @DB +''', ''SHA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from CN_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'TW')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''TAI'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from TW_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End    
 Else if(@DB = 'DE')    
 begin    
 SET @sqltxt = 'INSERT INTO #tmp(DBName, Branch,EquipmentSpendVAl)      
       Select '''+ @DB +''', ''DEU'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''010000000000000000000000000000''     
       union    
       Select '''+ @DB +''', ''ITA'', (SUM(gld_cr_amt) - SUM(gld_dr_amt)) * '+ @CurrenyRate +' as WarehseFees                
       from DE_glhgld_rec               
       where gld_bsc_gl_acct in (6450) and gld_acctg_per = ' + REPLACE(CONVERT(varchar(7), @FD,126), '-','') + ' and gld_sacct = ''020000000000000000000000000000'' '    
  print @sqltxt;        
set @execSQLtxt = @sqltxt;       
 EXEC (@execSQLtxt);     
 End   
    
       
End    


SElect (case when Branch ='SHA' then 'CN' when Branch = 'IND' then 'UK' when (Branch = 'DEU' or Branch ='ITA') then 'DE' else DBName end) as DBName, Branch, Sum(TtlMultMetalInvVal) as TtlMultMetalInvVal,Sum(TtlMultMetalInvWgt) as TtlMultMetalInvWgt,    
  (select sum(t3.ExcessMultMetalInvWgt5Pct) from #tmp t3  where t3.ExcessMultMetalInvWgt5Pct>0 and t3.Branch = t1.BRANCH )     
   as ExcessMultMetalInvWgt5Pct,      
  SUM(ShotageMultMetalInvWgt) as ShotageMultMetalInvWgt, SUM(TtlMultMetalBuyoutWgt) as TtlMultMetalBuyoutWgt      
  , Sum(BHMMultMetalLBS) as BHMMultMetalLBS , Sum(CRPMultMetalLBS) as CRPMultMetalLBS       
, Sum(EXPMultMetalLBS) as EXPMultMetalLBS , Sum(HIBMultMetalLBS) as HIBMultMetalLBS       
, Sum(INDMultMetalLBS) as INDMultMetalLBS , Sum(JACMultMetalLBS) as JACMultMetalLBS       
, Sum(LAXMultMetalLBS) as LAXMultMetalLBS , Sum(MTLMultMetalLBS) as MTLMultMetalLBS       
, Sum(ROSMultMetalLBS) as ROSMultMetalLBS , Sum(SEAMultMetalLBS) as SEAMultMetalLBS       
, Sum(SHAMultMetalLBS) as SHAMultMetalLBS , Sum(TAIMultMetalLBS) as TAIMultMetalLBS       
, Sum(WDLMultMetalLBS) as WDLMultMetalLBS , SUM(TtlSalesShipped) as TtlSalesShipped      
,SUM(TtlLBSShipped) as TtlLBSShipped, SUM(TtlNetProfitPCt) as TtlNetProfitPCt, SUM(TtlOperatingExp) as TtlOperatingExp,      
SUM(TtlSalesShipped) - Sum(TtlMultMetalInvVal) as TtlTitaniumInvVal, SUM(TtlLBSShipped) - Sum(TtlMultMetalInvWgt) as TtlTitaniumInvWgt,    
   Sum(USBranchBooking) as USBranchBooking,Sum(USBranchMulMtlLBS) as USBranchMulMtlLBS,     
   (select sum(t2.USBranchExcessLBS) from #tmp t2  where t2.USBranchExcessLBS>0 and t2.Branch = t1.BRANCH )  as USBranchExcessLBS,     
   SUM(ShotageInvWgt) - SUM(ShotageMultMetalInvWgt) as ShotageTitaniumInvWgt, SUM(TtlBuyOutVal) as TtlBuyOutVal,    
   SUM(USBranchNetProfit) as USBranchNetProfit,SUM(USBranchActiveCusBase) as USBranchActiveCusBase,    
   SUM(TtlBuyOutWgt) as TtlBuyOutWgt, SUM(USBranchNonUSMtlLBS) as USBranchNonUSMtlLBS, SUM(WGTShippedLBS) as WGTShippedLBS,    
   Sum(PiecesShipped) as PiecesShipped,Sum(BOL) as BOL , SUM(TtlHour) as TtlHour,    
   (case When Sum(TtlHour) > 0 then  SUM(WGTShippedLBS)/Sum(TtlHour) else 0 end) as WTPerHour,    
 (case When Sum(TtlHour) > 0 then  Sum(PiecesShipped)/Sum(TtlHour) else 0 end) as PiecesPerHour ,     
 (case When Sum(TtlHour) > 0 then  Sum(BOL)/Sum(TtlHour) else 0 end) as BOLPerHour,    
 SUM(MarketingSpendVal) as MarketingSpendVal, SUM(ObsoleteMultMetalInvWgt) as ObsoleteMultMetalInvWgt, SUM(TtlMultMetalBuyoutVal) as TtlMultMetalBuyoutVal,    
 SUM(TtlReturnVal) as TtlReturnVal, SUM(CostQualityCert) as CostQualityCert, SUM(ObsoleteTitaniumWgt) as ObsoleteTitaniumWgt,    
 Sum(LumberSpendVAl) as LumberSpendVAl, Sum(WaterjetSpendVAl) as WaterjetSpendVAl, Sum(RMSpendVAl) as RMSpendVAl,    
 SUM(OtherWhsSpendVAl) as OtherWhsSpendVAl, SUM(SawBladeSpendVAl) as SawBladeSpendVAl, SUM(ForkliftSpendVAl) as ForkliftSpendVAl,    
 SUM(JanitorialSpendVAl) as JanitorialSpendVAl, SUM(OfficeSpendVAl) as OfficeSpendVAl, SUM(BuildingsSpendVAl) as BuildingsSpendVAl,    
 SUM(RefusesSpendVAl) as RefusesSpendVAl, sUM(SafetySpendVAl) as SafetySpendVAl, SUM(EquipmentSpendVAl) as EquipmentSpendVAl,    
 (select sum(t4.ExcessTitaniumInvVal) from #tmp t4  where t4.ExcessTitaniumInvVal>0 and t4.Branch = t1.BRANCH )     
   as ExcessTitaniumInvVal    
	into #tempresult1      
  from #tmp as t1 where (DBName!='US' or BRANCH not in ('TAI','SFS','MTL'))      
  -- and (BRANCH =  @Branch  or  @Branch = '')    
  group by (case when Branch ='SHA' then 'CN' when Branch = 'IND' then 'UK' when (Branch = 'DEU' or Branch ='ITA') then 'DE' else DBName end), Branch     
  order by    DBName,Branch

-- insert result into tempresult1 and fetch result based on selected database
IF @DBNAME = 'ALL'
begin
  select * from #tempresult1 order by DBName,Branch

end
else
begin

	select * from #tempresult1 where DBName = ''+ @DBNAME +'' order by DBName,Branch

end

drop table #tempresult1;
  Drop table #tmp;        
          
              
              
 END        
-- exec [sp_itech_Departmental_KPI] 'ALL','2017-02-01', '2017-02-28' exec [sp_itech_Departmental_KPI] 'UK','2017-02-01', '2017-02-28'      
/*  
20210125 Sumit  
add germany database  
20210816 Sumit
Add condition to Set Database on the basis of branches which are exist on multiple databases and insert into #tempresult1, fetch result from #tempresult1 table
Mail: FW: Report Fixes
*/
GO
