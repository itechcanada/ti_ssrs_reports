USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ivjjcl]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Dec 21, 2016,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
Create PROCEDURE [dbo].[TW_ivjjcl]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.TW_ivjjcl_rec', 'U') IS NOT NULL  
  drop table dbo.TW_ivjjcl_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.TW_ivjjcl_rec  
  from [LIVETWSTX].[livetwstxdb].[informix].[ivjjcl_rec] ;   
    
END  
-- select * from TW_ivjjcl_rec 
GO
