USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_scrdtr]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  iTECH     
-- Create date: Jan 13, 2020    
-- Description: <Description,,>    
-- =============================================    
Create PROCEDURE [dbo].[CA_scrdtr]     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
    -- Insert statements for procedure here    
        
        
    IF OBJECT_ID('dbo.CA_scrdtr_rec', 'U') IS NOT NULL    
  drop table dbo.CA_scrdtr_rec;    
        
            
SELECT *    
into  dbo.CA_scrdtr_rec    
FROM [LIVECASTX].[livecastxdb].[informix].[scrdtr_rec];    
    
END    
--- exec CA_scrpyt    
-- select * from CA_scrdtr_rec
GO
