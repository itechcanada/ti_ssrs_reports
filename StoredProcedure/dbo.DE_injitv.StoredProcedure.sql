USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[DE_injitv]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,10/27/2020>  
-- Description: <Germany Inventory Transaction Quantity/Value>  
-- =============================================  
CREATE PROCEDURE [dbo].[DE_injitv]
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
truncate table dbo.DE_injitv_rec;   

-- Insert statements for procedure here    
Insert into dbo.DE_injitv_rec   
SELECT *  
FROM [LIVEDESTX].[livedestxdb].[informix].[injitv_rec]
  
  
END  
GO
