USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetBranchesAddress]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <09 Nov 2017>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetBranchesAddress]  @DBNAME varchar(50)  ,@CustomerID varchar(50)  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
  
CREATE TABLE #tmp (  BranchName varchar(300)   
        ,BranchAddress Varchar(500)  
        )  
  
 
 if @CustomerID ='ALL'          
 BEGIN          
 set @CustomerID = ''          
 END   
   
IF @DBNAME = 'ALL'  
 BEGIN  
  
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(4000);     
       
      SET @query ='INSERT INTO #tmp ( BranchName,BranchAddress)  
      select RTRIM(LTRIM(brh_nm1)) as branchName, LTRIM(RTrim(brh_addr1)) + LTRIM(RTRIM(brh_addr2)) + LTRIM(RTRIM(brh_addr3)) + '', '' + LTRIM(RTRIM(brh_city)) 
      + '', '' + LTRIM(RTRIM(brh_st_prov)) + '' '' + LTRIM(RTRIM(brh_pcd)) + '' '' + LTRIM(RTRIM(brh_cty)) as Address from  '+ @Prefix +'_scrbrh_rec
      where  (brh_brh = (select top 1 cus_admin_brh from '+ @Prefix +'_arrcus_rec where (cus_cus_id = '''+ @CustomerID +''' or ''' + @CustomerID + ''' = '''') ) ) 
      
       '  
        print(@query);  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN      
     SET @sqltxt ='INSERT INTO #tmp ( BranchName,BranchAddress)  
      select RTRIM(LTRIM(brh_nm1)) as branchName, LTRIM(RTrim(brh_addr1)) + LTRIM(RTRIM(brh_addr2)) + LTRIM(RTRIM(brh_addr3)) + '', '' + LTRIM(RTRIM(brh_city)) 
      + '', '' + LTRIM(RTRIM(brh_st_prov)) + '' '' + LTRIM(RTRIM(brh_pcd)) + '' '' + LTRIM(RTRIM(brh_cty)) as Address from  '+ @DBNAME +'_scrbrh_rec 
      where  (brh_brh = (select top 1 cus_admin_brh from '+ @DBNAME +'_arrcus_rec where (cus_cus_id = '''+ @CustomerID +''' or ''' + @CustomerID + ''' = '''') ) ) 
       '  
     print(@sqltxt);   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
      End  
        
     select * from #tmp where BranchName != '';
      drop table  #tmp  
END  
  
-- exec sp_itech_GetBranchesAddress  'UK','ALL'  
  
  /*
  2016-05-19
  Changed by mukesh
  */
  
GO
