USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenPoDetail_V3]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
      
      
          
-- =============================================                            
-- Author:  <Sumit>                            
-- Create date: <14 July 2020>                            
-- Description: <Getting All Open PO Detail>                            
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_OpenPoDetail_V3]  @DBNAME varchar(10), @Branch varchar(5)      
as           
begin      
      
--DECLARE @FMTONLY BIT;        
--IF 1 = 0        
--BEGIN        
-- SET @FMTONLY = 1;        
-- SET FMTONLY OFF;        
--END      
      
SET NOCOUNT ON;            
DECLARE @query1 nvarchar(max)            
DECLARE @query2 nvarchar(max)            
DECLARE @DB varchar(100)                                                             
DECLARE @DatabaseName VARCHAR(35)                                                              
DECLARE @Prefix VARCHAR(35)                                 
DECLARE @Name VARCHAR(15)                  
DECLARE @CurrenyRate varchar(15);      
      
CREATE TABLE #temp1 (               
 DBName varchar(10),         
 poh_cmpy_id varchar(10),        
 poh_po_brh varchar(10),          
 whs_mng_brh varchar(10),          
 poh_ven_id varchar(10),          
 nad_nm1 varchar(150),          
 poh_shp_fm int,          
 pod_po_pfx varchar(5),          
 pod_po_no varchar(10),          
 pod_po_itm int,          
 prd_desc1 varchar(150),          
 prd_desc2 varchar(50),          
 ipd_frm varchar(50),  
 ipd_grd varchar(50),  
 ipd_size varchar(50),  
 ipd_fnsh varchar(50),  
 pod_po_dist int,           
 poh_po_pl_dt date,          
 poh_byr varchar(10),          
 poh_mil varchar(10),          
 por_mil_ord_no varchar(50),          
 por_roll_sch varchar(50),          
 poi_ord_wgt_um varchar(5),          
 poi_ord_qty int,          
 poi_bal_qty int,          
 poi_ord_wgt int,          
 poi_bal_wgt int,          
 poi_pur_cat varchar(5),          
 pod_arr_to_dt date,          
 pod_shp_to_whs varchar(5),          
 bal_pcs int,          
 bal_msr int,          
 bal_wgt int,          
 bal_qty int,          
 pod_lnd_cst numeric(13,2),      
 csi_bas_cry_val int,      
 arr_year int,          
 arr_month int,          
 po_month int,          
 pum varchar(5),          
 quantity_display_um varchar(5),          
 mat_cst_dcml int,          
 prm_pcs_ctl int,          
 frm_invt_ctl varchar(5),          
 frm_non_invt int,          
 por_po_intl_rls_no int,          
 por_due_fm_dt date,          
 latepcs int,          
 latewgt int,          
 lateqty int,          
 currpcs int,          
 currwgt int,        
 currqty int        
 );        
 CREATE TABLE #temp2 (               
 DBName varchar(10),             
 pod_cmpy_id varchar(10),        
 pod_po_pfx varchar(5),          
 pod_po_no varchar(10),          
 pod_po_itm int,          
 janpcs int,          
 janwgt int,          
 janqty int,          
 febpcs int,          
 febwgt int,          
 febqty int,          
 marpcs int,          
 marwgt int,          
 marqty int,          
 aprpcs int,          
 aprwgt int,          
 aprqty int,          
 maypcs int,          
 maywgt int,          
 mayqty int,          
 junepcs int,          
 junewgt int,          
 juneqty int,          
 julypcs int,          
 julywgt int,          
 julyqty int,          
 augpcs int,          
 augwgt int,          
 augqty int,          
 septpcs int,          
 septwgt int,          
 septqty int,          
 octpcs int,          
 octwgt int,          
 octqty int,          
 novpcs int,          
 novwgt int,          
 novqty int,          
 decpcs int,          
 decwgt int,          
 decqty int          
  );          
          
set @DB=  @DBNAME            
      
IF @Branch = 'ALL'                    
BEGIN                    
 set @Branch = ''                    
END      
          
 IF @DBNAME = 'ALL'           
 BEGIN                                                                
  DECLARE ScopeCursor CURSOR FOR                                                    
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                                     
  OPEN ScopeCursor;                                                    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                          
  WHILE @@FETCH_STATUS = 0                                                                
  BEGIN                                                            
   SET @DB= @Prefix          
          
    IF (UPPER(@DB) = 'TW')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
  End                      
    Else if (UPPER(@DB) = 'NO')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
  End                          
    Else if (UPPER(@DB) = 'CA')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                          
  End                          
    Else if (UPPER(@DB) = 'CN')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                          
  End                          
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                          
  begin                          
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
  End                          
 Else if(UPPER(@DB) = 'UK')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                          
  End                          
 Else if(UPPER(@DB) = 'DE')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                          
  End                          
    Else if(UPPER(@DB) = 'TWCN')                          
  begin                          
   SET @DB ='TW'                          
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
  End      
          
 --Print '1';      
 Print @DB +' - '+ @CurrenyRate ;       
 --Print '2'      
  set @query1 = 'insert into #temp1 select ''' + @DB + ''' as dbname, poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,  
  pod_po_no ,pod_po_itm , mat_extd_desc as prd_desc1, prm_shrt_size_desc as prd_desc2, ipd_frm, ipd_grd, ipd_size, ipd_fnsh,       
 pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,        
 poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,(pod_ord_pcs - pod_rcvd_pcs ) as bal_pcs,(pod_ord_msr - pod_rcvd_msr ) bal_msr,        
 (pod_ord_wgt - pod_rcvd_wgt ) as bal_wgt,(pod_ord_qty - pod_rcvd_qty ) as bal_qty ,cast((ISNULL(pod_lnd_cst,0) * ' + @CurrenyRate + ') as numeric(13,2)) as pod_lnd_cst,      
  case when ISNULL(pod_lnd_cst,0) =0 then 0 else sum((round(csi_bas_cry_val,0)  * '+ @CurrenyRate +')) end as csi_bal_cry_val,      
 (YEAR (pod_arr_to_dt )) as arr_year ,(MONTH (pod_arr_to_dt )) as arr_month ,        
 ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) as po_month ,        
 CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_disp_um  ELSE mat_m_qty_disp_um  END as PUM,        
 CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_cst_um  ELSE mat_m_qty_cst_um  END as quantity_display_UM ,        
 mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_pcs else NULL end as LatePcs,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_wgt else NULL end as LateWgt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_qty else NULL end as Lateqty,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_pcs else NULL end as CurrPcs,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_wgt else NULL end as CurrWgt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_qty else NULL end as Currqty        
 --into #temp1        
 from ' + @DB + '_tctnad_rec , ' + @DB + '_potpoh_rec, ' + @DB + '_potpoi_rec , ' + @DB + '_potpod_rec, ' + @DB + '_scrwhs_rec,         
 ' + @DB + '_tctipd_rec, ' + @DB + '_inrprm_rec, ' + @DB + '_inrmat_rec, ' + @DB + '_inrfrm_rec, ' + @DB + '_potpor_rec, '+ @DB +'_cttcsi_rec        
 where         
 nad_cmpy_id = poh_cmpy_id and nad_ref_pfx = poh_po_pfx and nad_ref_no = poh_po_no and nad_ref_itm =0 and nad_addr_typ=''B''         
 and poh_cmpy_id = poi_cmpy_id and poh_po_pfx = poi_po_pfx and poh_po_no = poi_po_no         
 and poi_cmpy_id = pod_cmpy_id and poi_po_pfx = pod_po_pfx and poi_po_no = pod_po_no and poi_po_itm = pod_po_itm and pod_trcomp_sts != ''C''        
 and pod_cmpy_id = whs_cmpy_id and pod_shp_to_whs = whs_whs        
 and poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm        
 and ipd_frm = prm_frm and ipd_grd = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh        
 and prm_frm = mat_frm and prm_grd = mat_grd        
 and ipd_frm = frm_frm        
 and poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm and poi_pur_cat != ''NI'' and pod_po_intl_rls_no = por_po_intl_rls_no       
 and pod_cmpy_id = csi_cmpy_id and pod_po_pfx = csi_ref_pfx and pod_po_no = csi_ref_no and pod_po_itm = csi_ref_itm and pod_po_dist= csi_ref_sbitm       
 group by poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,pod_po_no ,pod_po_itm , mat_extd_desc , prm_shrt_size_desc, ipd_frm, ipd_grd, ipd_size, ipd_fnsh,     
 pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,        
 poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,pod_ord_pcs, pod_rcvd_pcs,pod_ord_msr, pod_rcvd_msr,      
 pod_ord_wgt , pod_bal_pcs , pod_bal_wgt, pod_bal_qty, pod_rcvd_wgt,pod_ord_qty, pod_rcvd_qty, pod_lnd_cst,pod_arr_to_dt,mat_e_qty_disp_um, mat_m_qty_cst_um,      
 prm_bas_msr, mat_m_qty_disp_um,mat_e_qty_cst_um,        
 mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt'       
 --Print '3';      
 Print @query1;        
 EXECUTE sp_executesql @query1;       
 --Print '4';      
  set @query2 =        
   'insert into #temp2 select ''' + @DB + ''' as db_name, pod_cmpy_id,pod_po_pfx, pod_po_no, pod_po_itm,        
 case when MONTH(pod_arr_to_dt) = 1 then pod_bal_pcs else NULL end as JanPcs,          
 case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_wgt as int) else NULL end as JanWgt,          
 case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_qty as int) else NULL end as Janqty,          
 case when MONTH(pod_arr_to_dt) = 2 then pod_bal_pcs else NULL end as FebPcs,          
 case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_wgt as int) else NULL end as FebWgt,          
 case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_qty as int) else NULL end as Febqty,          
 case when MONTH(pod_arr_to_dt) = 3 then pod_bal_pcs else NULL end as MarPcs,          
 case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_wgt as int) else NULL end as MarWgt,          
 case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_qty as int) else NULL end as Marqty,          
 case when MONTH(pod_arr_to_dt) = 4 then pod_bal_pcs else NULL end as AprPcs,          
 case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_wgt as int) else NULL end as AprWgt,          
 case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_qty as int) else NULL end as Aprqty,          
 case when MONTH(pod_arr_to_dt) = 5 then pod_bal_pcs else NULL end as MayPcs,          
 case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_wgt as int) else NULL end as MayWgt,          
 case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_qty as int) else NULL end as Mayqty,          
 case when MONTH(pod_arr_to_dt) = 6 then pod_bal_pcs else NULL end as JunePcs,          
 case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_wgt as int) else NULL end as JuneWgt,          
 case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_qty as int) else NULL end as Juneqty,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_pcs else NULL end as JulyPcs,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_wgt else NULL end as JulyWgt,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_qty else NULL end as Julyqty,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_pcs else NULL end as AugPcs,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_wgt else NULL end as AugWgt,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_qty else NULL end as Augqty,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_pcs else NULL end as SeptPcs,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_wgt else NULL end as SeptWgt,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_qty else NULL end as Septqty,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_pcs else NULL end as OctPcs,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_wgt else NULL end as OctWgt,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_qty else NULL end as Octqty,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_pcs else NULL end as NovPcs,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_wgt else NULL end as NovWgt,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_qty else NULL end as Novqty,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_pcs else NULL end as DecPcs,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_wgt else NULL end as DecWgt,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_qty else NULL end as Decqty        
 --into #temp2        
 from '+ @DB +'_potpod_rec where pod_trcomp_sts !=''C'''          
  print @query2        
  EXECUTE sp_executesql @query2;                                                                 
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                                
  END                                                                 
  CLOSE ScopeCursor;                                                                
  DEALLOCATE ScopeCursor;                                                  
 END            
 else          
 Begin      
 -- for single database      
 IF (UPPER(@DBNAME) = 'TW')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
  End                          
    Else if (UPPER(@DBNAME) = 'NO')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
  End                          
    Else if (UPPER(@DBNAME) = 'CA')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                          
  End                          
    Else if (UPPER(@DBNAME) = 'CN')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                        
  End                          
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')                          
  begin                          
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
  End                          
 Else if(UPPER(@DBNAME) = 'UK')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                          
  End                          
 Else if(UPPER(@DBNAME) = 'DE')                          
  begin                          
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                          
  End                          
    Else if(UPPER(@DBNAME) = 'TWCN')                          
  begin                          
   SET @DB ='TW'                          
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
  End      
          
 print @DB +' - '+ @CurrenyRate ;       
      
  set @query1 = 'insert into #temp1 select ''' + @DB + ''' as dbname, poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,  
  pod_po_no ,pod_po_itm , mat_extd_desc as prd_desc1, prm_shrt_size_desc as prd_desc2, ipd_frm, ipd_grd, ipd_size, ipd_fnsh,   
 pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,        
 poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,(pod_ord_pcs - pod_rcvd_pcs ) as bal_pcs,(pod_ord_msr - pod_rcvd_msr ) bal_msr,        
 (pod_ord_wgt - pod_rcvd_wgt ) as bal_wgt,(pod_ord_qty - pod_rcvd_qty ) as bal_qty ,cast((ISNULL(pod_lnd_cst,0) * ' + @CurrenyRate + ') as numeric(13,2)) as pod_lnd_cst,      
  case when ISNULL(pod_lnd_cst,0) =0 then 0 else sum((round(csi_bas_cry_val,0)  * '+ @CurrenyRate +')) end as csi_bal_cry_val,      
 (YEAR (pod_arr_to_dt )) as arr_year ,(MONTH (pod_arr_to_dt )) as arr_month ,        
 ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) as po_month ,        
 CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_disp_um  ELSE mat_m_qty_disp_um  END as PUM,        
 CASE WHEN (prm_bas_msr = ''E'' )  THEN mat_e_qty_cst_um  ELSE mat_m_qty_cst_um  END as quantity_display_UM ,        
 mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_pcs else NULL end as LatePcs,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_wgt else NULL end as LateWgt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) < 0 then pod_bal_qty else NULL end as Lateqty,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_pcs else NULL end as CurrPcs,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_wgt else NULL end as CurrWgt,        
 case when ((((YEAR (pod_arr_to_dt ) - YEAR (GETDATE() ) ) * 12 ) + MONTH (pod_arr_to_dt ) ) - MONTH (getdate() ) ) = 0 then pod_bal_qty else NULL end as Currqty        
 --into #temp1        
 from ' + @DB + '_tctnad_rec , ' + @DB + '_potpoh_rec, ' + @DB + '_potpoi_rec , ' + @DB + '_potpod_rec, ' + @DB + '_scrwhs_rec,         
 ' + @DB + '_tctipd_rec, ' + @DB + '_inrprm_rec, ' + @DB + '_inrmat_rec, ' + @DB + '_inrfrm_rec, ' + @DB + '_potpor_rec, '+ @DB +'_cttcsi_rec        
 where         
 nad_cmpy_id = poh_cmpy_id and nad_ref_pfx = poh_po_pfx and nad_ref_no = poh_po_no and nad_ref_itm =0 and nad_addr_typ=''B''         
 and poh_cmpy_id = poi_cmpy_id and poh_po_pfx = poi_po_pfx and poh_po_no = poi_po_no         
 and poi_cmpy_id = pod_cmpy_id and poi_po_pfx = pod_po_pfx and poi_po_no = pod_po_no and poi_po_itm = pod_po_itm and pod_trcomp_sts != ''C''        
 and pod_cmpy_id = whs_cmpy_id and pod_shp_to_whs = whs_whs        
 and poi_cmpy_id = ipd_cmpy_id and poi_po_pfx = ipd_ref_pfx and poi_po_no = ipd_ref_no and poi_po_itm = ipd_ref_itm        
 and ipd_frm = prm_frm and ipd_grd = prm_grd and ipd_size = prm_size and ipd_fnsh = prm_fnsh        
 and prm_frm = mat_frm and prm_grd = mat_grd        
 and ipd_frm = frm_frm        
 and poi_cmpy_id = por_cmpy_id and poi_po_pfx = por_po_pfx and poi_po_no = por_po_no and poi_po_itm = por_po_itm and poi_pur_cat != ''NI'' and pod_po_intl_rls_no = por_po_intl_rls_no       
 and pod_cmpy_id = csi_cmpy_id and pod_po_pfx = csi_ref_pfx and pod_po_no = csi_ref_no and pod_po_itm = csi_ref_itm and pod_po_dist= csi_ref_sbitm       
 group by poh_cmpy_id, poh_po_brh ,whs_mng_brh ,poh_ven_id ,nad_nm1 ,poh_shp_fm ,pod_po_pfx ,pod_po_no ,pod_po_itm , mat_extd_desc , prm_shrt_size_desc, ipd_frm, ipd_grd, ipd_size, ipd_fnsh,     
 pod_po_dist ,poh_po_pl_dt ,poh_byr ,poh_mill ,por_mill_ord_no ,por_roll_sch ,poi_ord_wgt_um ,poi_ord_qty ,poi_bal_qty ,        
 poi_ord_wgt ,poi_bal_wgt ,poi_pur_cat ,pod_arr_to_dt ,pod_shp_to_whs ,pod_ord_pcs, pod_rcvd_pcs,pod_ord_msr, pod_rcvd_msr,      
 pod_ord_wgt , pod_bal_pcs , pod_bal_wgt, pod_bal_qty, pod_rcvd_wgt,pod_ord_qty, pod_rcvd_qty, pod_lnd_cst,pod_arr_to_dt,mat_e_qty_disp_um, mat_m_qty_cst_um,      
 prm_bas_msr, mat_m_qty_disp_um,mat_e_qty_cst_um,        
 mat_cst_dcml ,prm_pcs_ctl ,frm_invt_ctl ,frm_non_invt ,por_po_intl_rls_no ,por_due_fm_dt'        
 print @query1;        
 EXECUTE sp_executesql @query1;        
  set @query2 =        
   'insert into #temp2 select ''' + @DB + ''' as dbname, pod_cmpy_id,pod_po_pfx, pod_po_no, pod_po_itm,        
 case when MONTH(pod_arr_to_dt) = 1 then pod_bal_pcs else NULL end as JanPcs,          
 case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_wgt as int) else NULL end as JanWgt,          
 case when MONTH(pod_arr_to_dt) = 1 then Cast(pod_bal_qty as int) else NULL end as Janqty,          
 case when MONTH(pod_arr_to_dt) = 2 then pod_bal_pcs else NULL end as FebPcs,          
 case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_wgt as int) else NULL end as FebWgt,          
 case when MONTH(pod_arr_to_dt) = 2 then Cast(pod_bal_qty as int) else NULL end as Febqty,          
 case when MONTH(pod_arr_to_dt) = 3 then pod_bal_pcs else NULL end as MarPcs,          
 case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_wgt as int) else NULL end as MarWgt,          
 case when MONTH(pod_arr_to_dt) = 3 then Cast(pod_bal_qty as int) else NULL end as Marqty,          
 case when MONTH(pod_arr_to_dt) = 4 then pod_bal_pcs else NULL end as AprPcs,          
 case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_wgt as int) else NULL end as AprWgt,          
 case when MONTH(pod_arr_to_dt) = 4 then Cast(pod_bal_qty as int) else NULL end as Aprqty,          
 case when MONTH(pod_arr_to_dt) = 5 then pod_bal_pcs else NULL end as MayPcs,          
 case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_wgt as int) else NULL end as MayWgt,          
 case when MONTH(pod_arr_to_dt) = 5 then Cast(pod_bal_qty as int) else NULL end as Mayqty,          
 case when MONTH(pod_arr_to_dt) = 6 then pod_bal_pcs else NULL end as JunePcs,          
 case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_wgt as int) else NULL end as JuneWgt,          
 case when MONTH(pod_arr_to_dt) = 6 then Cast(pod_bal_qty as int) else NULL end as Juneqty,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_pcs else NULL end as JulyPcs,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_wgt else NULL end as JulyWgt,        
 case when MONTH(pod_arr_to_dt) = 7 then pod_bal_qty else NULL end as Julyqty,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_pcs else NULL end as AugPcs,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_wgt else NULL end as AugWgt,        
 case when MONTH(pod_arr_to_dt) = 8 then pod_bal_qty else NULL end as Augqty,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_pcs else NULL end as SeptPcs,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_wgt else NULL end as SeptWgt,        
 case when MONTH(pod_arr_to_dt) = 9 then pod_bal_qty else NULL end as Septqty,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_pcs else NULL end as OctPcs,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_wgt else NULL end as OctWgt,        
 case when MONTH(pod_arr_to_dt) = 10 then pod_bal_qty else NULL end as Octqty,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_pcs else NULL end as NovPcs,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_wgt else NULL end as NovWgt,        
 case when MONTH(pod_arr_to_dt) = 11 then pod_bal_qty else NULL end as Novqty,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_pcs else NULL end as DecPcs,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_wgt else NULL end as DecWgt,        
 case when MONTH(pod_arr_to_dt) = 12 then pod_bal_qty else NULL end as Decqty              
 --into #temp2        
 from '+ @DB +'_potpod_rec where pod_trcomp_sts !=''C'''          
  print @query2        
  EXECUTE sp_executesql @query2;          
 end          
          
 set @query1 = 'select t1.DBName,t1.poh_cmpy_id,t1.poh_po_brh,t1.whs_mng_brh,t1.poh_ven_id,t1.nad_nm1,t1.poh_shp_fm,t1.pod_po_pfx,t1.pod_po_no,      
t1.pod_po_itm,t1.prd_desc1,t1.prd_desc2,t1.ipd_frm, t1.ipd_grd, t1.ipd_size, t1.ipd_fnsh,t1.pod_po_dist,t1.poh_po_pl_dt,t1.poh_byr,t1.poh_mil,t1.por_mil_ord_no,t1.por_roll_sch,t1.poi_ord_wgt_um,      
t1.poi_ord_qty,t1.poi_bal_qty,t1.poi_ord_wgt,t1.poi_bal_wgt,t1.poi_pur_cat,t1.pod_arr_to_dt,t1.pod_shp_to_whs,t1.bal_pcs,t1.bal_msr,t1.bal_wgt,      
t1.bal_qty,t1.pod_lnd_cst,t1.csi_bas_cry_val,t1.arr_year,t1.arr_month,t1.po_month,t1.pum,t1.quantity_display_um,t1.mat_cst_dcml,t1.prm_pcs_ctl,      
t1.frm_invt_ctl,t1.frm_non_invt,t1.por_po_intl_rls_no,t1.por_due_fm_dt,t1.latepcs,t1.latewgt,t1.lateqty,t1.currpcs,t1.currwgt,t1.currqty,      
t2.janpcs,t2.janwgt,t2.janqty,t2.febpcs,t2.febwgt,t2.febqty,t2.marpcs,t2.marwgt,t2.marqty,t2.aprpcs,t2.aprwgt,t2.aprqty,t2.maypcs,t2.maywgt,t2.mayqty,      
t2.junepcs,t2.junewgt,t2.juneqty,t2.julypcs,t2.julywgt,t2.julyqty,t2.augpcs,t2.augwgt,t2.augqty,t2.septpcs,t2.septwgt,t2.septqty,      
t2.octpcs,t2.octwgt,t2.octqty,t2.novpcs,t2.novwgt,t2.novqty,t2.decpcs,t2.decwgt,t2.decqty       
from #temp1 t1 left outer join #temp2 t2        
 on t1.DBName = t2.DBName and t1.poh_cmpy_id = t2.pod_cmpy_id and t1.pod_po_pfx = t2.pod_po_pfx and t1.pod_po_no = t2.pod_po_no and       
 t1.pod_po_itm = t2.pod_po_itm where (t1.whs_mng_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')     
 order by t1.DBName, t1.poh_cmpy_id, t1.whs_mng_brh,t1.poh_ven_id, t1.pod_po_no,t1.pod_po_itm'         
        
print @query1        
execute sp_executesql @query1;        
      
 --IF @FMTONLY = 1        
 --BEGIN        
 -- SET FMTONLY ON;        
 --END      
        
drop table #temp1;        
drop table #temp2;        
      
end          
/*          
          
2020/07/14 Sumit          
SP created          
exec [sp_itech_OpenPoDetail] 'ALL','ALL'      
exec [dbo].[sp_itech_OpenPoDetail] 'UK','ALL'      
exec [sp_itech_OpenPoDetail] 'US','DET'   
exec sp_itech_OpenPoDetail_V2 'ALL','ALL' 
20210623	Sumit
exclude Non Inventory PO , add filter poi_pur_cat != ''NI''
JIRA: Titanium - Fwd: FW: Open PO Report
*/   
  
GO
