USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ivjjct]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[US_ivjjct]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_ivjjct_rec', 'U') IS NOT NULL
		drop table dbo.US_ivjjct_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_ivjjct_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[ivjjct_rec] ; 
  
END
-- select * from US_ivjjct_rec 
GO
