USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_plswrw]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,JUN 28, 2015,>  
-- Description: <Description,  Warehouse Rmnt Weekly Data>  
  
-- =============================================  
Create PROCEDURE [dbo].[CN_plswrw]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_plswrw_rec', 'U') IS NOT NULL  
  drop table dbo.CN_plswrw_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_plswrw_rec  
  from [LIVECN_IW].[livecnstxdb_iw].[informix].[plswrw_rec] ;   
    
END  
-- select * from CN_plswrw_rec where wrw_run_dt is desc
GO
