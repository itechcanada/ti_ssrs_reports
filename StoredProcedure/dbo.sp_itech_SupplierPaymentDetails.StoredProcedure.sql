USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SupplierPaymentDetails]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mrinal >    
-- Create date: <19 July 2016>    
-- Description: <Getting Vendor Voucher SSRS reports>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_itech_SupplierPaymentDetails] @DBNAME varchar(50), @FromDate datetime, @ToDate datetime   
As    
Begin    
  
  
  
declare @DB varchar(100)    
declare @sqltxt varchar(6000)    
declare @execSQLtxt varchar(7000)    
declare @FD varchar(10)    
declare @TD varchar(10)    
declare @FDVchEnt varchar(10)    
declare @TDVchEnt varchar(10)    
set @FD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@FromDate)-12,0)) , 120)    
set @TD = CONVERT(VARCHAR(10),DATEADD(MONTH, DATEDIFF(MONTH, -1, @FromDate)-1, -1),120)  
set @FDVchEnt = CONVERT(VARCHAR(10), @FromDate , 120)    
set @TDVchEnt = CONVERT(VARCHAR(10), @ToDate , 120)   
  
DECLARE @ExchangeRate varchar(15)   
    
  
    
CREATE TABLE #tmp (   [Database]   VARCHAR(10)    
        , SupplierName   VARCHAR(100)    
        , StreetAddress    VARCHAR(100)   
        , City    VARCHAR(100)    
        , StateProv     varchar(50)    
        , PostalCode    varchar(50)    
        , Country     varchar(50)    
        , Phone    varchar(50)  
        , AnnualSpend    decimal(20,2)    
        , AnnualPaymentCount  int  
        , PaymentTerm    VARCHAR(50)   
        , PaymentType   VARCHAR(50)   
        , SupplierID    VARCHAR(50)   
        , AnnualInvCount     int    
        , AnnualPoCount   int  
        )    
            
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(35);    
DECLARE @Name VARCHAR(15);    
DECLARE @CurrenyRate varchar(15);  
    
IF @DBNAME = 'ALL'    
 BEGIN    
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName    
    OPEN ScopeCursor;    
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
        DECLARE @query NVARCHAR(max);   
           
        IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End
		Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End
             
       SET @query ='INSERT INTO #tmp ([Database], SupplierName, StreetAddress, City, StateProv, PostalCode, Country, Phone, AnnualSpend,AnnualPaymentCount,PaymentTerm,PaymentType,  
        SupplierID,AnnualInvCount,AnnualPoCount )    
        SELECT ''' +  @Prefix + ''' as [Database], ven_ven_long_nm  
,(select cva_addr1 from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_addr  
,(select cva_city from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_city  
,(select cva_st_prov from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_state_prov  
,(select cva_pcd from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_postal_code  
,(select cva_cty from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_country  
,(select cva_tel_no from ' + @Prefix + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_tel_no  
, SUM(jvc_vchr_amt) as ven_annual_spend  
, COUNT(jvc_vchr_amt) as ven_annual_payment_count   
,(Select pyt_desc30 from ' + @Prefix + '_scrpyt_rec Where pyt_pttrm = (select top 1 vch_pttrm from ' + @Prefix + '_aptvch_rec where vch_ven_id  = jvc_ven_id)) AS ven_payment_term  
, (select mpt_desc15 from ' + @Prefix + '_scrmpt_rec where mpt_mthd_pmt = (Select top 1 shf_mthd_pmt from ' + @Prefix + '_aprshf_rec where shf_ven_id = jvc_ven_id )) AS ven_payment_type  
, jvc_ven_id AS supplier_id  
  
 ,(select COUNT(*) from   
(  
Select COUNT(*) invcnt from ' + @Prefix + '_aptpyh_rec where pyh_ven_id = jvc_ven_id and pyh_ven_inv_dt >= '''+ @FD +''' and pyh_ven_inv_dt <= '''+ @TD +'''  
group by pyh_ven_inv_no ) as oquery ) as oq  
  
, (select COUNT(*) from (  
select count(*) as pocount from ' + @Prefix + '_potpoh_rec where poh_ven_id = jvc_ven_id and poh_po_pl_dt >= '''+ @FD +''' and poh_po_pl_dt <= '''+ @TD +''' group by poh_po_no) as oquery) as ven_annual_po_count  
    
FROM ' + @Prefix + '_apjjvc_rec  
JOIN ' + @Prefix + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id AND ven_ven_id = jvc_ven_id    
where jvc_ent_dt >= '''+ @FDVchEnt +''' and jvc_ent_dt <= '''+ @TDVchEnt +'''  
GROUP BY jvc_ven_id, ven_ven_long_nm    
          ;'           
    print @query;    
        EXECUTE sp_executesql @query;    
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN    
         IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End 
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End
     
        SET @sqltxt ='INSERT INTO #tmp ([Database], SupplierName, StreetAddress, City, StateProv, PostalCode, Country, Phone, AnnualSpend,AnnualPaymentCount,PaymentTerm,PaymentType,  
        SupplierID,AnnualInvCount,AnnualPoCount )    
        SELECT ''' +  @DBNAME + ''' as [Database], ven_ven_long_nm  
,(select cva_addr1 from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_addr  
,(select cva_city from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_city  
,(select cva_st_prov from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_state_prov  
,(select cva_pcd from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_postal_code  
,(select cva_cty from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_country  
,(select cva_tel_no from ' + @DBNAME + '_scrcva_rec where cva_cus_ven_id=jvc_ven_id AND cva_cus_ven_typ=''V'' AND cva_addr_typ=''L'') as ven_tel_no  
, SUM(jvc_vchr_amt) as ven_annual_spend  
, COUNT(jvc_vchr_amt) as ven_annual_payment_count   
,(Select pyt_desc30 from ' + @DBNAME + '_scrpyt_rec Where pyt_pttrm = (select top 1 vch_pttrm from ' + @DBNAME + '_aptvch_rec where vch_ven_id  = jvc_ven_id)) AS ven_payment_term  
, (select mpt_desc15 from ' + @DBNAME + '_scrmpt_rec where mpt_mthd_pmt = (Select top 1 shf_mthd_pmt from ' + @DBNAME + '_aprshf_rec where shf_ven_id = jvc_ven_id )) AS ven_payment_type  
, jvc_ven_id AS supplier_id  
  
 ,(select COUNT(*) from   
(  
Select COUNT(*) invcnt from ' + @DBNAME + '_aptpyh_rec where pyh_ven_id = jvc_ven_id and pyh_ven_inv_dt >= '''+ @FD +''' and pyh_ven_inv_dt <= '''+ @TD +'''  
group by pyh_ven_inv_no ) as oquery ) as oq  
  
, (select COUNT(*) from (  
select count(*) as pocount from ' + @DBNAME + '_potpoh_rec where poh_ven_id = jvc_ven_id and poh_po_pl_dt >= '''+ @FD +''' and poh_po_pl_dt <= '''+ @TD +''' group by poh_po_no) as oquery) as ven_annual_po_count  
    
FROM ' + @DBNAME + '_apjjvc_rec  
JOIN ' + @DBNAME + '_aprven_rec on ven_cmpy_id = jvc_cmpy_id AND ven_ven_id = jvc_ven_id    
where jvc_ent_dt >= '''+ @FDVchEnt +''' and jvc_ent_dt <= '''+ @TDVchEnt +'''  
GROUP BY jvc_ven_id, ven_ven_long_nm  
;'   
   
     print(@sqltxt)     
    set @execSQLtxt = @sqltxt;     
   EXEC (@execSQLtxt);    
   END   
      
   Select *                     
    from #tmp    
    order by [Database]  
   Drop table #tmp    
End     
    
            
-- Exec [sp_itech_SupplierPaymentDetails] 'UK','2016-07-15','2016-07-30'  
/*  
Date: 20160804  
Mail Sub:a report based on vendor payment  
Supplier Analysis is a good name.  
I need to be able to choose a date range.  
20210128	Sumit
add Germany Database
*/    
GO
