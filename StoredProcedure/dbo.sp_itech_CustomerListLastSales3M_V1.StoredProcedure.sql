USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerListLastSales3M_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh>
-- Create date: <03 March 2016>
-- Description:	<Get List of Customer of LAX customer that have no any sales in last 3 month>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_CustomerListLastSales3M_V1]  @DBNAME varchar(50), @BRANCH Varchar(3), @version char = '0'
	-- Add the parameters for the stored procedure here
	
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)              
declare @execSQLtxt varchar(7000)              
declare @DB varchar(100) 

if(@BRANCH = 'ALL')
begin 
set @BRANCH = '';
end

set @DB=  @DBNAME 
CREATE TABLE #tmp (   [Database]   VARCHAR(10)              
        , custID     Varchar(65)              
        , custName     Varchar(65)              
        , branch     Varchar(65)              
        , lastSalesDate    Varchar(10)    
                   
                 );  
                 
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(35);              
DECLARE @Name VARCHAR(15);        
   IF @DBNAME = 'ALL'              
 BEGIN              
 IF @version = '0'      
  BEGIN      
  DECLARE ScopeCursor CURSOR FOR      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName      
    OPEN ScopeCursor;      
  END      
  ELSE      
  BEGIN
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
			OPEN ScopeCursor; 
  END 
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(max);                 
      SET @DB= @Prefix       
                    
           SET @query ='INSERT INTO #tmp ([Database],custID,custName,branch,lastSalesDate)              
                  Select ''' +  @DB + ''', cus_cus_id, cus_cus_nm,cus_admin_brh,convert(Varchar(10),coc_lst_sls_dt,120) as coc_lst_sls_dt from 
                   ' + @DB + '_arbcoc_rec
join ' + @DB + '_arrcus_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id
Where coc_lst_sls_dt < convert(varchar(10),DATEADD(month,-3, GETDATE()),120) and (cus_admin_brh = ''' + @BRANCH + ''' OR ''' + @BRANCH + ''' = '''')
 order by coc_lst_sls_dt desc
       '              
                    
                     
      print @query;              
        EXECUTE sp_executesql @query;              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE              
     BEGIN          
           
         
         
        
                
       SET @sqltxt ='INSERT INTO #tmp ([Database],custID,custName,branch,lastSalesDate)              
                  Select ''' +  @DB + ''', cus_cus_id, cus_cus_nm,cus_admin_brh,convert(Varchar(10),coc_lst_sls_dt,120) as coc_lst_sls_dt from 
                   ' + @DB + '_arbcoc_rec
join ' + @DB + '_arrcus_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id
Where coc_lst_sls_dt < convert(varchar(10),DATEADD(month,-3, GETDATE()),120) and (cus_admin_brh = ''' + @BRANCH + ''' OR ''' + @BRANCH + ''' = '''')
 order by coc_lst_sls_dt desc'              
                
                 
                  
                        
     print( @sqltxt)               
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
   END              
        
                 
   SElect * from #tmp;
   drop table #tmp;

END

-- exec [sp_itech_CustomerListLastSales3M] 'ALL','ALL'
GO
