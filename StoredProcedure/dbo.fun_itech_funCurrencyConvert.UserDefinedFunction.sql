USE [Stratix_US]
GO
/****** Object:  UserDefinedFunction [dbo].[fun_itech_funCurrencyConvert]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Function to retrieve       
CREATE FUNCTION  [dbo].[fun_itech_funCurrencyConvert](@DBNAME Varchar(20), @crx_eqv_cry Varchar(5),@crx_orig_cry varchar(5)) RETURNS varchar(15)      
AS      
BEGIN      
      
DECLARE @ExRate Varchar(15)      
declare @DB varchar(100)      
      
      
--set @DB= UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'      
      
    IF (UPPER(@DBNAME) = 'TW')      
    begin      
    SET @ExRate = (select ISNULL(crx_xexrt, 1)from  [TW_scrcrx_rec] where crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
 End      
 Else if (UPPER(@DBNAME) = 'NO')      
    begin      
     SET @ExRate = (select ISNULL(crx_xexrt, 1) from [NO_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End      
    Else if (UPPER(@DBNAME) = 'CA')      
    begin      
     SET @ExRate = (select ISNULL(crx_xexrt, 1) from [CA_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End      
    Else if (UPPER(@DBNAME) = 'US')      
    begin      
     SET @ExRate = (select ISNULL(crx_xexrt, 1) from [US_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End      
    Else if (UPPER(@DBNAME) = 'CN')      
    begin      
     SET @ExRate = (select ISNULL(crx_xexrt, 1) from [CN_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End      
    Else if(UPPER(@DBNAME) = 'UK')      
    begin      
       SET @ExRate = (select ISNULL(crx_xexrt, 1) from [UK_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End    
 Else if(UPPER(@DBNAME) = 'DE')      
    begin      
       SET @ExRate = (select ISNULL(crx_xexrt, 1) from [DE_scrcrx_rec] where   crx_eqv_cry = @crx_eqv_cry  and crx_orig_cry=@crx_orig_cry)      
    End     
      
       
RETURN @ExRate      
      
END      
/*    
20210119 Sumit    
Added germany database currency condition    
*/    
    
--SELECT dbo.Fun_itech_funCurrencyConvert('NO','USD','NOK') AS TEST      
GO
