USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_ShortageWeight]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mayank >  
-- Create date: <11 Feb 2013>  
-- Description: <Getting top 50 customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_ShortageWeight] @DBNAME varchar(50), @version char = '0'
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
set @DB=  @DBNAME  
set @FD = CONVERT(VARCHAR(10), DateAdd(mm, -12, GetDate()) , 120)  
  
CREATE TABLE #tmp (   [Database]   VARCHAR(10)  
        , Form     Varchar(65)  
        , Grade     Varchar(65)  
        , Size     Varchar(65)  
        , Finish    Varchar(65)  
        , OnHandWeightLBS  DECIMAL(20, 2)  
                    , IncomingLBS     DECIMAL(20, 2)  
                    , AvlLbs     DECIMAL(20, 2)  
        , ReservedWeightLBS   DECIMAL(20, 2)  
        , SalesHist12M_Avg    DECIMAL(20, 2)  
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  IF @version = '0'
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
		END
		ELSE
		BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS 
		  OPEN ScopeCursor;
		END
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(1500);     
      SET @DB= @Prefix    
      SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,OnHandWeightLBS,AvlLbs,ReservedWeightLBS,SalesHist12M_Avg,IncomingLBS)  
                   select prd_cmpy_id as [Database], PRD_FRM as Form, PRD_GRD as Grade,PRD_SIZE as Size, PRD_FNSH as Finish, SUM(prd_ohd_wgt) as OnHandWeightLBS,  
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,  SUM(PRD_ORD_RES_WGT) as ReservedWeightBS,  
       (Select SUM(SAT_BLG_WGT)/12 from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD aND PRD_SIZE = SAT_SIZE and  PRD_FNSH = SAT_FNSH  
       and SAT_INV_DT >='''+ @FD +''' ) as SalesHist12M_Avg,  
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''  
       where IPD_CMPY_ID = prd_cmpy_id and ipd_FRM=PRD_FRM   
       and ipd_GRD = PRD_GRD and ipd_SIZE = PRD_SIZE and ipd_fnsh = PRD_FNSH) as IncomingLbs  
       from ' + @DB + '_intprd_rec    
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')  
       group by  prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH  ;'  
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
     SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,OnHandWeightLBS,AvlLbs,ReservedWeightLBS,SalesHist12M_Avg,IncomingLBS)  
                   select prd_cmpy_id as [Database], PRD_FRM as Form, PRD_GRD as Grade,PRD_SIZE as Size, PRD_FNSH as Finish, SUM(prd_ohd_wgt) as OnHandWeightLBS,  
       SUM(prd_ohd_wgt) - SUM(PRD_ORD_RES_WGT)   As AvlLbs,  SUM(PRD_ORD_RES_WGT) as ReservedWeightBS,  
       (Select SUM(SAT_BLG_WGT)/12 from ' + @DB + '_sahsat_rec where PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD aND PRD_SIZE = SAT_SIZE and  PRD_FNSH = SAT_FNSH  
       and SAT_INV_DT >='''+ @FD +''' ) as SalesHist12M_Avg,  
       (select sum(potpod_rec.pod_bal_wgt) from ' + @DB + '_tctipd_rec join ' + @DB + '_potpod_rec as potpod_rec  
       on pod_cmpy_id =ipd_cmpy_id and pod_po_no = ipd_ref_no and ipd_ref_itm = pod_po_itm and pod_po_pfx = ipd_ref_pfx and pod_trcomp_sts <> ''C''  
       where IPD_CMPY_ID = prd_cmpy_id and ipd_FRM=PRD_FRM   
       and ipd_GRD = PRD_GRD and ipd_SIZE = PRD_SIZE and ipd_fnsh = PRD_FNSH) as IncomingLbs  
       from ' + @DB + '_intprd_rec    
       where (PRD_INVT_STS = ''S'' or PRD_INVT_STS = ''N'')  
       group by  prd_cmpy_id, PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH  ;'  
           --'INSERT INTO #tmp ([Database],Form,Grade,Size,Finish,OnHandWeightLBS,ReservedWeightBS,AvWgt12MonthsLBS)  
           --  select '''+@DB+''', PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH, SUM(prd_ohd_wgt) as StockLbs, SUM(PRD_ORD_RES_WGT) as StockReserved,   
     --  SUM(SAT_BLG_WGT)/12 as AvgPerMonth  from ' + @DB + '_intprd_rec left join  
     --  ' + @DB + '_sahsat_rec on PRD_FRM = SAT_FRM and PRD_GRD = SAT_GRD aND PRD_SIZE = SAT_SIZE and  PRD_FNSH = SAT_FNSH  
     --  where PRD_INVT_STS = ''S''  AND SAT_INV_DT >'''+ @FD +'''  
     --  group by  PRD_FRM, PRD_GRD,PRD_SIZE, PRD_FNSH '  
     print(@sqltxt)   
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     END  
       
 if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')  
           Begin      
    select   
        [Database]    
        , Form      
        , Grade     
        , Size      
        , Finish     
        , (OnHandWeightLBS * 2.20462) as OnHandWeightLBS    
                    , (IncomingLBS * 2.20462) as IncomingLBS     
                    , (AvlLbs + IncomingLBS) * 2.20462 as AvlLbs   
        , (ReservedWeightLBS * 2.20462) as ReservedWeightLBS     
        , SalesHist12M_Avg ,  
    case when SalesHist12M_Avg > 2000 then 'A+'  
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then 'A'  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then 'B'  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then 'C'  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then 'D'  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then 'E' Else 'F' END as Ranking,  
    case   
    when SalesHist12M_Avg > 2000 then 4*SalesHist12M_Avg   
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then 3*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then 2.5*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then 2*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then 1.5*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then 1*SalesHist12M_Avg  Else 0 END as RequiredLbs,  
    case   
    when SalesHist12M_Avg > 2000 then AvlLbs - (4*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then AvlLbs - (3*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then AvlLbs - (2.5*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then AvlLbs - (2*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then AvlLbs - (1.5*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then AvlLbs - (1*SalesHist12M_Avg) + IncomingLbs Else 0 END as Shortage  
    from #tmp order by Form;;  
             END  
             ELSE  
                Begin  
                select   
        [Database]    
        , Form      
        , Grade     
        , Size      
        , Finish     
        , OnHandWeightLBS    
                    , IncomingLBS      
                    , AvlLbs + IncomingLBS as AvlLbs   
        , ReservedWeightLBS     
        , SalesHist12M_Avg ,  
    case when SalesHist12M_Avg > 2000 then 'A+'  
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then 'A'  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then 'B'  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then 'C'  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then 'D'  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then 'E' Else 'F' END as Ranking,  
    case   
    when SalesHist12M_Avg > 2000 then 4*SalesHist12M_Avg   
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then 3*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then 2.5*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then 2*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then 1.5*SalesHist12M_Avg  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then 1*SalesHist12M_Avg  Else 0 END as RequiredLbs,  
    case   
    when SalesHist12M_Avg > 2000 then AvlLbs - (4*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 1000 AND SalesHist12M_Avg <= 2000 then AvlLbs - (3*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 750 AND SalesHist12M_Avg <= 1000 then AvlLbs - (2.5*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 500 AND SalesHist12M_Avg <= 750 then AvlLbs - (2*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 350 AND SalesHist12M_Avg <= 500 then AvlLbs - (1.5*SalesHist12M_Avg) + IncomingLbs  
    when SalesHist12M_Avg >= 150 AND SalesHist12M_Avg <= 350 then AvlLbs - (1*SalesHist12M_Avg) + IncomingLbs Else 0 END as Shortage  
    from #tmp order by Form;;  
               
             END  
                   
   drop table #tmp  
END  
  
-- exec sp_itech_ShortageWeight 'ALL','0'
  
  
  
  
GO
