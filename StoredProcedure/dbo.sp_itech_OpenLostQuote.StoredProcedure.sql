USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_OpenLostQuote]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <26 Feb 2013>
-- Description:	<Getting top 40 customers for SSRS reports>
-- =============================================

CREATE PROCEDURE [dbo].[sp_itech_OpenLostQuote] @DBNAME varchar(50),@Branch varchar(10),@version char = '0'

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD_LastYear varchar(10)
declare @TD_Currentyear varchar(10)

set @DB= @DBNAME 
if month(GetDate()) = 1 
begin
set @FD_LastYear =   convert(varchar(5),year(GetDate())-2) +'-'+ convert(varchar(2),12) +'-01'-- FirstDateOFCurrentYear
end
else
begin
set @FD_LastYear =   convert(varchar(5),year(GetDate())-1) +'-'+ convert(varchar(2),month(GetDate())-1) +'-01'-- FirstDateOFCurrentYear
End 



set @TD_Currentyear = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of Previous Month
--SET @FD_LastYear='2013-03-01';
CREATE TABLE #tmp (  ORH_ORD_PFX  	 Varchar(2)
					,ORH_ORD_NO		 Varchar(15) 
   					, ActDate			 varchar(15)
   					,Years           Varchar(5)
   					,StageName    Varchar(30)
   					,Branch varchar(10) 
   	               );
   	              
DECLARE @DatabaseName VARCHAR(35);
DECLARE @prefix VARCHAR(5);
DECLARE @Name VARCHAR(15);

if @Branch ='ALL'
 BEGIN
 set @Branch = ''
 END

--set @DB= UPPER('['+ @DBNAME +']') +'.' + LOWER('['+ @DBNAME + 'db' +']')+'.' + '[informix]'
set @DB= @DBNAME;
IF @DBNAME = 'ALL'
	BEGIN
	IF @version = '0'  
		  BEGIN  
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
			OPEN ScopeCursor;  
		  END  
		  ELSE  
		  BEGIN  
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
			OPEN ScopeCursor;  
		  END  
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  				SET @DB=  @prefix  
  				--set @DB= UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  			   DECLARE @query NVARCHAR(4000);
		      SET @query = 'INSERT INTO #tmp (ORH_ORD_PFX,ORH_ORD_NO, ActDate,Years,StageName,Branch)
						select tsh_ref_pfx, tsh_ref_no,convert(varchar(7),min(tsh_actvy_dtts), 126) +''-01'' as ActDate,
							YEAR(min(tsh_actvy_dtts)) as Years,''Quotes Created'' as StageName,ORH_ORD_BRH 
							from  ' + @DB + '_tcttsh_rec 
							inner join ' + @DB + '_ortorh_rec on orh_cmpy_id = tsh_cmpy_id and 
                            orh_ord_pfx = tsh_ref_pfx  and orh_ord_no = tsh_ref_no
							where (tsh_ref_pfx = ''QT'') and tsh_sts_actn = ''C'' and tsh_sts_typ = ''T''
							and (ORH_ORD_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by tsh_ref_pfx, tsh_ref_no,ORH_ORD_BRH having MIN(tsh_actvy_dtts) >= '''+ @FD_LastYear +''' and  MIN(tsh_actvy_dtts) <=   '''+ @TD_Currentyear +'''
						UNION
						select tsh_ref_pfx, tsh_ref_no,convert(varchar(7),min(tsh_actvy_dtts), 126) +''-01'' as ActDate,
							YEAR(min(tsh_actvy_dtts)) as Years,''Orders Created'' as StageName,ORH_ORD_BRH 
							from  ' + @DB + '_tcttsh_rec 
							inner join ' + @DB + '_ortorh_rec on orh_cmpy_id = tsh_cmpy_id and 
                            orh_ord_pfx = tsh_ref_pfx  and orh_ord_no = tsh_ref_no
							where (tsh_ref_pfx = ''SO'') and tsh_sts_actn = ''C'' and tsh_sts_typ = ''T''
							and (ORH_ORD_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by tsh_ref_pfx, tsh_ref_no,ORH_ORD_BRH having MIN(tsh_actvy_dtts) >= '''+ @FD_LastYear +''' and  MIN(tsh_actvy_dtts) <=   '''+ @TD_Currentyear +'''
						UNION
						SELECT bka_ord_pfx, bka_ord_no,convert(varchar(7),Max(bka_actvy_dt), 126) +''-01'' as ActDate,
							YEAR(Max(bka_actvy_dt)) as Years,''Quotes Lost'' as StageName,bka_brh
							from ' + @DB + '_ortbka_rec
							where  bka_trs_md =''D'' AND bka_ord_itm  <>999
							and     bka_ord_pfx = ''QT''   
							and (bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by bka_ord_pfx, bka_ord_no,bka_brh
							having Max(bka_actvy_dt) >= '''+ @FD_LastYear +''' and  MAX(bka_actvy_dt) <=   '''+ @TD_Currentyear +'''
						UNION	
						SELECT bka_ord_pfx, bka_ord_no,convert(varchar(7),Max(bka_actvy_dt), 126) +''-01'' as ActDate,
							YEAR(Max(bka_actvy_dt)) as Years,''Orders Lost'' as StageName,bka_brh
							from ' + @DB + '_ortbka_rec
							where  bka_trs_md =''D'' AND bka_ord_itm  <>999
							and     bka_ord_pfx = ''SO''   
							and (bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by bka_ord_pfx, bka_ord_no,bka_brh
							having Max(bka_actvy_dt) >= '''+ @FD_LastYear +''' and  MAX(bka_actvy_dt) <=   '''+ @TD_Currentyear +'''
						'
							
					print(@query)		
  	  			EXECUTE sp_executesql @query;
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
						SET @sqltxt= 'INSERT INTO #tmp (ORH_ORD_PFX,ORH_ORD_NO, ActDate,Years,StageName,Branch)
						select tsh_ref_pfx, tsh_ref_no,convert(varchar(7),min(tsh_actvy_dtts), 126) +''-01'' as ActDate,
							YEAR(min(tsh_actvy_dtts)) as Years,''Quotes Created'' as StageName,ORH_ORD_BRH 
							from  ' + @DB + '_tcttsh_rec 
							inner join ' + @DB + '_ortorh_rec on orh_cmpy_id = tsh_cmpy_id and 
                            orh_ord_pfx = tsh_ref_pfx  and orh_ord_no = tsh_ref_no
							where (tsh_ref_pfx = ''QT'') and tsh_sts_actn = ''C'' and tsh_sts_typ = ''T''
							and (ORH_ORD_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by tsh_ref_pfx, tsh_ref_no,ORH_ORD_BRH having MIN(tsh_actvy_dtts) >= '''+ @FD_LastYear +''' and  MIN(tsh_actvy_dtts) <=   '''+ @TD_Currentyear +'''
						UNION
						select tsh_ref_pfx, tsh_ref_no,convert(varchar(7),min(tsh_actvy_dtts), 126) +''-01'' as ActDate,
							YEAR(min(tsh_actvy_dtts)) as Years,''Orders Created'' as StageName,ORH_ORD_BRH 
							from  ' + @DB + '_tcttsh_rec 
							inner join ' + @DB + '_ortorh_rec on orh_cmpy_id = tsh_cmpy_id and 
                            orh_ord_pfx = tsh_ref_pfx  and orh_ord_no = tsh_ref_no
							where (tsh_ref_pfx = ''SO'') and tsh_sts_actn = ''C'' and tsh_sts_typ = ''T''
							and (ORH_ORD_BRH = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by tsh_ref_pfx, tsh_ref_no,ORH_ORD_BRH having MIN(tsh_actvy_dtts) >= '''+ @FD_LastYear +''' and  MIN(tsh_actvy_dtts) <=   '''+ @TD_Currentyear +'''
						UNION
						SELECT bka_ord_pfx, bka_ord_no,convert(varchar(7),Max(bka_actvy_dt), 126) +''-01'' as ActDate,
							YEAR(Max(bka_actvy_dt)) as Years,''Quotes Lost'' as StageName,bka_brh
							from ' + @DB + '_ortbka_rec
							where  bka_trs_md =''D'' AND bka_ord_itm  <>999
							and     bka_ord_pfx = ''QT'' 
							and (bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')  
							group by bka_ord_pfx, bka_ord_no,bka_brh
							having Max(bka_actvy_dt) >= '''+ @FD_LastYear +''' and  MAX(bka_actvy_dt) <=   '''+ @TD_Currentyear +'''
						UNION	
						SELECT bka_ord_pfx, bka_ord_no,convert(varchar(7),Max(bka_actvy_dt), 126) +''-01'' as ActDate,
							YEAR(Max(bka_actvy_dt)) as Years,''Orders Lost'' as StageName,bka_brh
							from ' + @DB + '_ortbka_rec
							where  bka_trs_md =''D'' AND bka_ord_itm  <>999
							and     bka_ord_pfx = ''SO''   
							and (bka_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')
							group by bka_ord_pfx, bka_ord_no,bka_brh
							having Max(bka_actvy_dt) >= '''+ @FD_LastYear +''' and  MAX(bka_actvy_dt) <=   '''+ @TD_Currentyear +''''
		print(@sqltxt)
	set @execSQLtxt = @sqltxt; 
	EXEC (@execSQLtxt);
     END
   
--DECLARE @cols AS NVARCHAR(MAX),
--@query2  AS NVARCHAR(MAX)
--select @cols = STUFF((SELECT distinct ',' + QUOTENAME(ActDate) 
--                    from #tmp
--            FOR XML PATH(''), TYPE
--            ).value('.', 'NVARCHAR(MAX)') 
--        ,1,1,'')

--set @query2 = ' 
--SELECT  StageName,ORH_ORD_PFX,' + @cols + '  from 
--             (
--                select ORH_ORD_PFX, count,ActDate,StageName
--                from #tmp
--            ) x
--            pivot 
--            (
--                max(count)
--                for ActDate in (' + @cols + ')
--            )p 
--             '
--print(@cols);
--EXEC (@query2);
--select * from #tmp
   
  select ORH_ORD_PFX,COUNT(distinct ORH_ORD_NO) as count,ActDate,Years,StageName,BRANCH from #tmp where Years >= 2012 and BRANCH != 'SFS'--and StageName = 'Orders Lost'
 -- and ActDate >= '2013-04-01' and StageName <> 'Quotes Created' and StageName <> 'Orders Created'
  group by ORH_ORD_PFX,ActDate,Years,StageName ,BRANCH
   
END

-- exec sp_itech_OpenLostQuote 'US','ALL'

/*
 option out PSM and especially SFS when all databases is selected.
 */





GO
