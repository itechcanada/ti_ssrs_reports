USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTop40ProspectCustomer]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <15 oct 2015>  
-- Description: <Getting top 40 Propects customers for SSRS reports>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_itech_GetTop40ProspectCustomer] @DBNAME varchar(50), @Branch varchar(15),@version char = '0'  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
 
 
 
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
  
set @DB=  @DBNAME   
  
CREATE TABLE #tmp1 (    CustID   VARCHAR(10)  
        , CustName     VARCHAR(65)  
        , Market   VARCHAR(65)   
        , Branch   VARCHAR(65)   
        , CreationDate  Varchar(10)  
        , Databases   VARCHAR(15)  
        ,TotalSales  Decimal(20,2)  
        ,ContactAdd  Varchar(100)  
        ,CustEmail Varchar(100) 
        ,CustAddress Varchar(150)         
        ,CustCity Varchar(50)
        ,CustState Varchar(10)
        
                   );   
                   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(5);  
DECLARE @Name VARCHAR(15);  
  
--if @Market ='ALL'  
-- BEGIN  
-- set @Market = ''  
-- END  
  
if @Branch ='ALL'  
 BEGIN  
 set @Branch = ''  
 END  
  
DECLARE @CurrenyRate varchar(15);  
  
  
  
IF @DBNAME = 'ALL'  
 BEGIN  
  IF @version = '0'  
		  BEGIN  
		  DECLARE ScopeCursor CURSOR FOR  
		   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName  
			OPEN ScopeCursor;  
		  END  
		  ELSE  
		  BEGIN   
  DECLARE ScopeCursor CURSOR FOR  
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS   
    OPEN ScopeCursor;  
 End   
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
      SET @DB= @Prefix  
     -- print(@DB)  
       
     IF (UPPER(@DB) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DB) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DB) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DB) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DB) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DB) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DB) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DB) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
       
       
        DECLARE @query NVARCHAR(4000);  
           
        SET @query = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch, CreationDate, Databases,TotalSales,ContactAdd,CustEmail,CustAddress,CustCity,CustState)  
       select cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch, cus_upd_dtts,  
       ''' + @DB + ''',0 as TotalValue,  
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else  
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else  
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else  
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end    
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),  
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = cus_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail,     
               LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)), LTRIM(RTRIM(cva_city)),  LTRIM(RTRIM(cva_st_prov))       
                  
              
            from ' + @DB + '_arrcus_rec   
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
            left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = cus_cus_id and 
            cva_addr_no = 0 and cva_addr_typ = ''L'' 
            where cus_cus_acct_typ = ''P''  AND (CUS_ADMIN_BRH = '''+@Branch +''' OR  '''+@Branch +''' = '' '') '   
           
      
      SET @query= @query +   ' group by  cus_cus_id, cus_cus_long_nm, cuc_desc30,CUS_ADMIN_BRH,cus_upd_dtts,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov))     
                           '  
        
       
         
     print(@query)    
        EXECUTE sp_executesql @query;  
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
     BEGIN   
       print 'starting' ;  
      IF (UPPER(@DBNAME) = 'TW')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
    Else if (UPPER(@DBNAME) = 'NO')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
    End  
    Else if (UPPER(@DBNAME) = 'CA')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
    End  
    Else if (UPPER(@DBNAME) = 'CN')  
    begin  
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
    End  
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
    begin  
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
    End  
    Else if(UPPER(@DBNAME) = 'UK')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
    End  
    Else if(UPPER(@DBNAME) = 'DE')  
    begin  
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
    End  
    Else if(UPPER(@DBNAME) = 'TWCN')  
    begin  
       SET @DB ='TW'  
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
    End  
     print 'Ending else';  
     print @CurrenyRate ;  
                          
    
       
     SET @sqltxt = 'INSERT INTO #tmp1 (CustID, CustName,Market,Branch, CreationDate, Databases,TotalSales,ContactAdd,CustEmail,CustAddress,CustCity,CustState)  
       select cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch, cus_upd_dtts,  
       ''' + @DB + ''',0 as TotalValue,  
            (select top 1 case when cvt_tel_area_cd = '''' and cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + Rtrim(cvt_tel_no)  else  
            case when cvt_tel_ext = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) else  
            case when cvt_tel_area_cd = '''' then rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) else  
            rtrim(cvt_frst_nm) + '' '' + Rtrim(cvt_lst_nm) + '' ('' + Rtrim(cvt_tel_area_cd) + '') '' + Rtrim(cvt_tel_no) + '' - '' + Rtrim(cvt_tel_ext) end   end  end    
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no),  
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_cus_ven_id = cus_cus_id and cvt_cntc_typ = ''PU'' order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail ,     
               LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)), LTRIM(RTRIM(cva_city)),  LTRIM(RTRIM(cva_st_prov)) 
              
            from ' + @DB + '_arrcus_rec   
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
            left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' and cva_cus_ven_id = cus_cus_id and 
            cva_addr_no = 0 and cva_addr_typ = ''L'' 
            where cus_cus_acct_typ = ''P'' AND (CUS_ADMIN_BRH = '''+@Branch +''' OR  '''+@Branch +''' = '' '') '  
            
      
      SET @sqltxt= @sqltxt +   ' group by  cus_cus_id, cus_cus_long_nm, cuc_desc30,CUS_ADMIN_BRH,cus_upd_dtts ,LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),LTRIM(RTRIM(cva_city)),LTRIM(RTRIM(cva_st_prov))   
                           '  
         
  print(@sqltxt)  
  set @execSQLtxt = @sqltxt;   
  EXEC (@execSQLtxt);  
     END  
     
     
    
-- SET @sqltxt=  '   
--  SELECT  CustID, CustName,   
--CustEmail,    
--Market,Branch,SUM(TotalValue) as TotalValue, SUM(TotalNETProfitValue)/nullif(SUM(TotalValue), 0) * 100 as NProfitPercentage,  
--  sUM(TotalMatValue) as TotalMatValue,SUM(TotalMatProfitValue)/nullif(SUM(TotalMatValue), 0) * 100 as GProfitPercentage,Databases,'''' as Cry,ContactAdd FROM #tmp1   
--  group by CustID, CustName,Market,Branch,Databases,ContactAdd,CustEmail  
--  order by TotalValue desc;'  
   
      
--  print(@sqltxt)  
--  set @execSQLtxt = @sqltxt;   
--  EXEC (@execSQLtxt);  
  select * from #tmp1  
   drop table #tmp1  
END  
  
-- exec [sp_itech_GetTop40ProspectCustomer] 'US', 'ALL'  
-- exec [sp_itech_GetTop40ProspectCustomer] 'ALL' ,'ALL' 
/*
Date: 20170411
Sub: STRATIX REPORTS

*/
GO
