USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_cprcpc]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 23, 2014,>  
-- 
  
-- =============================================  
Create PROCEDURE [dbo].[US_cprcpc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_cprcpc_rec', 'U') IS NOT NULL  
  drop table dbo.US_cprcpc_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_cprcpc_rec  
  from [LIVEUSSTX].[liveusstxdb].[informix].[cprcpc_rec] ;   
    
END  
-- select * from US_cprcpc_rec
GO
