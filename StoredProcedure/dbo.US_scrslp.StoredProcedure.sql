USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_scrslp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================    
-- Author:  <Author,Clayton Daigle>    
-- Create date: <Create Date,10/5/2012,>    
-- Description: <Description,Open Orders,>    
-- =============================================    
CREATE PROCEDURE [dbo].[US_scrslp] 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
   
 SET NOCOUNT ON;        
--     IF OBJECT_ID('dbo.US_scrslp_rec', 'U') IS NOT NULL        
--  drop table dbo.US_scrslp_rec;       
---- Insert statements for procedure here    
--SELECT *  into dbo.US_scrslp_rec  
--FROM [LIVEUSSTX].[liveusstxdb].[informix].[scrslp_rec]    

Truncate table dbo.US_scrslp_rec;   
   
Insert into dbo.US_scrslp_rec    
SELECT *  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[scrslp_rec];
    
END    

/*  
  20171031  
  change delete to drop table US_scrslp_rec  
20201014	Sumit
commend drop and select statement, add truncate and insert statement to keep table index
*/  
   
GO
