USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_ccratp]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,June 28, 2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
Create PROCEDURE [dbo].[TW_ccratp]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.TW_ccratp_rec', 'U') IS NOT NULL
		drop table dbo.TW_ccratp_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.TW_ccratp_rec
  from [LIVETWSTX].[livetwstxdb].[informix].[ccratp_rec] ; 
  
END
-- select * from TW_ccratp_rec 
GO
