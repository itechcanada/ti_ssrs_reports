USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Customer_Snap_Shot_OpenOrder]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <11 Nov 2017>            
-- Description: <Getting Open order of customers for SSRS reports>            
         
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_Customer_Snap_Shot_OpenOrder] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@CustomerID varchar(10) = ''            
            
AS            
BEGIN           
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(7000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
declare @NOOfCust varchar(15)            
DECLARE @ExchangeRate varchar(15)   
DECLARE @ExchangeRateCADTOUSD varchar(15)       
DECLARE @ExchangeRateNOKTOUSD varchar(15)      
DECLARE @ExchangeRateEURTOUSD varchar(15)   
          
     if(@FromDate = '' OR @FromDate = null OR @FromDate = '1900-01-01' )      
     begin      
     set @FromDate = '2009-01-01'      
      End      
            
      if (@ToDate = '' OR @ToDate = null OR @ToDate = '1900-01-01')      
      begin      
      set @ToDate = CONVERT(VARCHAR(10), GETDATE() , 120)      
      end      
            
set @DB= @DBNAME        
    
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)            
            
            
CREATE TABLE #tmp (    
  Branch varchar(10)            
        ,CustID   VARCHAR(10)  
        , ChargeValue    DECIMAL(20, 2)    
        , DueDate    varchar(20)     
  --CompanyId varchar(5)             
  --      ,OrderPrefix Varchar(2)            
  --      ,OrderNo varchar(10)            
  --      ,OrderItem   varchar(3)            
        --, CustName     VARCHAR(65)            
        --, CustomerCat   VARCHAR(65)             
        --, BalPcs  DECIMAL(20, 2)            
        --, BalWgt           DECIMAL(20, 2)            
        --, BalMsr    DECIMAL(20, 2)            
        --, StatusAction               varchar(2)            
        --, Salesperson     varchar(65)            
        --, ProductName   VARCHAR(100)             
        --, OrderType     varchar(65)            
        --,Form  varchar(35)            
        --,Grade  varchar(35)            
        --,Size  varchar(35)            
        --,Finish  varchar(35)           
        --,CustPO   VARCHAR(30)         
        --,CustAdd VARCHAR(170)         
        -- ,SalesPersonIs Varchar(10)          
        --,SalesPersonOs Varchar(10)         
        --,NetAmt Decimal(20,2)        
        --,NetPct Decimal(20,2)        
        --,ShipWhs Varchar(3)       
        --,PrdFull varchar(70)        
                 );             
CREATE TABLE #tmp1 (    
  Branch varchar(10)            
        ,CustID   VARCHAR(10)  
        , ChargeValue    DECIMAL(20, 0)    
        , DueDate    varchar(20)     
     )       
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(5);            
DECLARE @Name VARCHAR(15);            
DECLARE @CurrenyRate varchar(15);             
    
             
 if @Branch ='ALL'            
 BEGIN            
 set @Branch = ''            
 END            
  
if @CustomerID ='ALL'            
 BEGIN            
 set @CustomerID = ''            
 END             
            
IF @DBNAME = 'ALL'            
BEGIN            
             
	DECLARE ScopeCursor CURSOR FOR              
    select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
	OPEN ScopeCursor;              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
    WHILE @@FETCH_STATUS = 0            
    BEGIN            
		DECLARE @query NVARCHAR(Max);               
		SET @DB= @Prefix            
		IF (UPPER(@Prefix) = 'TW')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                        End                    
		Else if (UPPER(@Prefix) = 'NO')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
		End                    
		Else if (UPPER(@Prefix) = 'CA')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
		End                    
		Else if (UPPER(@Prefix) = 'CN')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
		End                    
		Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                    
		begin                    
			SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
		End                    
		Else if(UPPER(@Prefix) = 'UK')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
		End               
		Else if(UPPER(@Prefix) = 'DE')                    
		begin                    
			SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
		End               
		
		SET @query =            
       'INSERT INTO #tmp (Branch ,CustID,ChargeValue,DueDate)            
		select ord_ord_brh as Branch, ord_sld_cus_id as CustID, '  
		if(UPPER(@Prefix) = 'TW')   
		begin   
			SET @ExchangeRateCADTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))   
			set @query = @query + ' (case when orh_cry = ''USD'' then chl_chrg_val when orh_cry = ''CAD'' then chl_chrg_val* '+ @ExchangeRateCADTOUSD +'  else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end  
		else if (UPPER(@Prefix) = 'CA')   
        begin   
			set @query = @query + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end  
		else if (UPPER(@Prefix) = 'US')   
        begin   
			SET @ExchangeRateCADTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))   
			SET @ExchangeRateNOKTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))       
			set @query = @query + ' (case when orh_cry = ''CAD'' then chl_chrg_val* '+ @ExchangeRateCADTOUSD +' when orh_cry = ''NOK'' then chl_chrg_val* '+ @ExchangeRateNOKTOUSD +' else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end  
		else if (UPPER(@Prefix) = 'UK')   
        begin   
			SET @ExchangeRateNOKTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
			set @ExchangeRateEURTOUSD = (select ISNULL(crx_xexrt, 1) from [UK_scrcrx_rec] where   crx_eqv_cry = 'USD'  and crx_orig_cry='EUR')  
			set @query = @query + ' (case when orh_cry = ''USD'' then chl_chrg_val when orh_cry = ''EUR'' then chl_chrg_val * '+ @ExchangeRateEURTOUSD +' when orh_cry = ''NOK'' then chl_chrg_val * '+ @ExchangeRateNOKTOUSD +' else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end  
		else if (UPPER(@Prefix) = 'CN')   
		begin   
			set @query = @query + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end
		else if (UPPER(@Prefix) = 'DE')   
		begin   
			set @query = @query + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
		end
		
		set @query = @query + ' orl_due_to_dt as DueDate     
		from ' + @DB + '_ortord_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
		' + @DB + '_ortchl_rec ,' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec  where            
		ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
		and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
		and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
		and cds_cd=orh_ord_typ       AND         
        ord_cmpy_id = cht_cmpy_id   AND         ord_ord_pfx = cht_ref_pfx   AND         ord_ord_no  = cht_ref_no    AND         ord_ord_itm = cht_ref_itm      
        and cht_tot_typ = ''T''              
		and orl_bal_qty >= 0             
		and chl_chrg_cl= ''E''            
		and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
		and orh_sts_actn <> ''C''            
		and ord_ord_pfx <>''QT''             
		and  ( (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
		or        
		(orl_due_to_dt is null and orh_ord_typ =''J''))        
		and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
		and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')      
        and  ord_ord_brh not in (''SFS'')          
		order by ord_ord_no,ord_ord_itm,orl_due_to_dt'            
            
		print(@query);         
        EXECUTE sp_executesql @query;            
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
	END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
END            
ELSE            
BEGIN             
              
	Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
                 
    IF (UPPER(@DB) = 'TW')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DB) = 'NO')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
	End                    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
		SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End              
    Else if(UPPER(@DB) = 'DE')                    
    begin                    
		SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End              
       
       
    SET @sqltxt ='INSERT INTO #tmp (Branch ,CustID,ChargeValue,DueDate)            
    select ord_ord_brh as Branch, ord_sld_cus_id as CustID, '  
    if(UPPER(@DB) = 'TW')   
    begin   
		SET @ExchangeRateCADTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))   
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''USD'' then chl_chrg_val when orh_cry = ''CAD'' then chl_chrg_val* '+ @ExchangeRateCADTOUSD +'  else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
	end  
    else if (UPPER(@DB) = 'CA')   
    begin   
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
	end  
    else if (UPPER(@DB) = 'US')   
    begin   
		SET @ExchangeRateCADTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))   
        SET @ExchangeRateNOKTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))       
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''CAD'' then chl_chrg_val* '+ @ExchangeRateCADTOUSD +' when orh_cry = ''NOK'' then chl_chrg_val* '+ @ExchangeRateNOKTOUSD +' else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
	end  
    else if (UPPER(@DB) = 'UK')   
    begin   
		SET @ExchangeRateNOKTOUSD = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        set @ExchangeRateEURTOUSD = (select ISNULL(crx_xexrt, 1) from [UK_scrcrx_rec] where   crx_eqv_cry = 'USD'  and crx_orig_cry='EUR')  
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''USD'' then chl_chrg_val when orh_cry = ''EUR'' then chl_chrg_val * '+ @ExchangeRateEURTOUSD +' when orh_cry = ''NOK'' then chl_chrg_val * '+ @ExchangeRateNOKTOUSD +' else chl_chrg_val * '+ @CurrenyRate +'
		end ) as ChargeValue, '  
	end  
    else if (UPPER(@DB) = 'CN')   
    begin   
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
	end
	else if (UPPER(@DB) = 'DE')   
    begin   
		set @sqltxt = @sqltxt + ' (case when orh_cry = ''USD'' then chl_chrg_val else chl_chrg_val * '+ @CurrenyRate +' end ) as ChargeValue, '  
	end
    
	set @sqltxt = @sqltxt + ' orl_due_to_dt as DueDate  from ' + @DB + '_ortord_rec,' + @DB + '_ortorh_rec ,' + @DB + '_ortorl_rec,            
    ' + @DB + '_ortchl_rec, ' + @DB + '_rprcds_rec , ' + @DB + '_ortcht_rec  where            
    ord_ord_pfx=orh_ord_pfx and ord_ord_no=orh_ord_no and ord_cmpy_id=orh_cmpy_id            
    and ord_ord_pfx=orl_ord_pfx and ord_ord_itm =orl_ord_itm and ord_cmpy_id=orl_cmpy_id and ord_ord_no =orl_ord_no            
    and orl_ord_pfx = chl_ref_pfx and orl_ord_no = chl_ref_no and orl_ord_itm = chl_ref_itm and  orl_cmpy_id = chl_cmpy_id            
    and cds_cd=orh_ord_typ AND    
    ord_cmpy_id = cht_cmpy_id   AND  ord_ord_pfx = cht_ref_pfx   AND  ord_ord_no  = cht_ref_no    AND  ord_ord_itm = cht_ref_itm      
    and cht_tot_typ = ''T''              
    and orl_bal_qty >= 0             
    and chl_chrg_cl= ''E''            
    and ord_sts_actn=''A'' and cds_data_el_nm=''ORD-TYP'' and cds_lng=''en''            
    and orh_sts_actn <> ''C''            
    and ord_ord_pfx <>''QT''             
    and          
    ( (CONVERT(VARCHAR(10), orl_due_to_dt, 120) Between CONVERT(VARCHAR(10), '''+ @FD +''', 120)  and  CONVERT(VARCHAR(10), '''+ @TD +''', 120)   )        
	or        
    (orl_due_to_dt is null and orh_ord_typ =''J''))        
    and (ord_ord_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')            
    and (ord_sld_cus_id = '''+ @CustomerID +''' or '''+ @CustomerID +'''= '''')      
    and  ord_ord_brh not in (''SFS'')          
    order by ord_ord_no,ord_ord_itm,orl_due_to_dt'          
         
    print('test')  ;        
    print(@sqltxt);             
    set @execSQLtxt = @sqltxt;             
    EXEC (@execSQLtxt);            
END  
       
     --SELECT * from #tmp;  
       
     insert into #tmp1            
   SELECT  * FROM #tmp ; --distinct 20180209  
     
    select 'Total (Past)' as Text, SUM(ChargeValue) as value, '1' as seq  
   from #tmp1 where DueDate < CONVERT(VARCHAR(10), GETDATE(), 120)   
   union  
   select '3 (Past) Month' as Text, SUM(ChargeValue) as value, '2' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE()-90, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE()-60 , 120)   
   union  
    select '2 (Past) Month' as Text, SUM(ChargeValue) as value, '3' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE()-60, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE()-30 , 120)   
   union  
   select '1 (Past) Month' as Text, SUM(ChargeValue) as value, '4' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE()-30, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE() , 120)   
   union  
   select 'Current Month' as Text, SUM(ChargeValue) as value, '5' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE(), 120) and DueDate < CONVERT(VARCHAR(10), GETDATE() + 30 , 120)   
   union  
   select '1 (Future) Month' as Text, SUM(ChargeValue) as value, '6' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE() + 30, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE() + 60 , 120)   
  union  
   select '2 (Future) Month' as Text, SUM(ChargeValue) as value, '7' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE() + 60, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE() + 90 , 120)    
   union  
   select '3 (Future) Month' as Text, SUM(ChargeValue) as value, '8' as seq  
   from #tmp1 where DueDate >= CONVERT(VARCHAR(10), GETDATE() + 90, 120) and DueDate < CONVERT(VARCHAR(10), GETDATE() + 120 , 120)   
   union  
   select 'Total (Future)' as Text, SUM(ChargeValue) as value, '9' as seq  
   from #tmp1 where DueDate > CONVERT(VARCHAR(10), GETDATE() , 120) ;   
     
   drop table #tmp;      
   drop table #tmp1;      
         
END            
            
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@CustomerID varchar(10) = ''            
            
-- exec [sp_itech_Customer_Snap_Shot_OpenOrder] '1/10/2018', '2/09/2018' , 'ALL','ALL','317'          
-- exec [sp_itech_Customer_Snap_Shot_OpenOrder] '05/01/2015', '05/31/2015' , 'ALL','ALL','ALL','ALL',1   
/*
20210526	Sumit
add chl_chrg_val condition for germany (DE)
*/
GO
