USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_sasmsu]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Aug 17, 2016,>    
-- Description: <Description,Open Job history,>    
    
-- =============================================    
create PROCEDURE [dbo].[CA_sasmsu]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.CA_sasmsu_rec', 'U') IS NOT NULL    
  drop table dbo.CA_sasmsu_rec;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.CA_sasmsu_rec    
  from [LIVECA_IW].[livecastxdb_iw].[informix].[sasmsu_rec]  ;     
      
END    
-- select * from CA_sasmsu_rec
GO
