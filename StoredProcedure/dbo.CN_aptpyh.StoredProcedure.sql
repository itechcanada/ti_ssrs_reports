USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_aptpyh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 03, 2016,>  
-- Description: <Description,Open Job history,>  
  
-- =============================================  
create PROCEDURE [dbo].[CN_aptpyh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_aptpyh_rec', 'U') IS NOT NULL  
  drop table dbo.CN_aptpyh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_aptpyh_rec  
  from [LIVECNSTX].[livecnstxdb].[informix].[aptpyh_rec] ;   
    
END  
-- select * from CN_aptpyh_rec
GO
