USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_nctnhh]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,mrinal>  
-- Create date: <Create Date,sep 21, 2015,>  
-- Description: <Description,,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CN_nctnhh]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CN_nctnhh_rec', 'U') IS NOT NULL  
  drop table dbo.CN_nctnhh_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CN_nctnhh_rec  
  from [LIVECNSTX].[livecnstxdb].[informix].[nctnhh_rec];   
    
END  
-- select * from CN_nctnhh_rec
GO
