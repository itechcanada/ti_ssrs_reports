USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_potpoi]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
CREATE  PROCEDURE [dbo].[US_potpoi] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.US_potpoi_rec', 'U') IS NOT NULL
		drop table dbo.US_potpoi_rec;
    
        
SELECT *
into  dbo.US_potpoi_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[potpoi_rec];

END

-- exec US_potpoi
-- select * from US_potpoi_rec
GO
