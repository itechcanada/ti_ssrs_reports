USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_GetTOP20_InvetoryLBS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Updated Date, 02 Jul 2014>  
-- Description: <Description, Add pierce concept>  
-- =============================================  
  
  
CREATE PROCEDURE [dbo].[sp_itech_GetTOP20_InvetoryLBS]  @NoOfRecords int = 20 
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
   
   
declare @sqltxt varchar(6000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
  
set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month  
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)), 120)  --Last Day of current month  
  
--set @FD='2013-03-01'  
--set @TD='2013-03-31'  
  
CREATE TABLE #tmp (  dbName varchar(3),  
     dDate    Date  
        ,Product  varchar(50)  
        ,InventoryWgt DECIMAL(20, 2)          
        );   
  
    -- Insert statements for procedure here  
    IF (@NoOfRecords = 30)
  BEGIN
		  INSERT INTO #tmp(dbName, dDate,Product,InventoryWgt)  
		select 'PS',  
		convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
		SUM(prd_ohd_wgt) as InventoryWgt  from PS_intprd_rec_history  
		where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
		group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
  END
  ELSE
  BEGIN

  INSERT INTO #tmp(dbName, dDate,Product,InventoryWgt)   
select 'US',   
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt) as InventoryWgt  from US_intprd_rec_history   
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
UNION  
select 'UK',  
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt* 2.20462) as InventoryWgt  from UK_intprd_rec_history  
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
UNION  
select 'CA',  
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt) as InventoryWgt  from CA_intprd_rec_history  
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
UNION  
select 'TW',  
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt) as InventoryWgt  from TW_intprd_rec_history  
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
UNION  
select 'NO',  
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt* 2.20462) as InventoryWgt  from NO_intprd_rec_history  
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD))  
UNION  
select 'CN',  
convert(date, UpdateDtTm, 126) as UpdateDt , RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) as Product,  
SUM(prd_ohd_wgt) as InventoryWgt  from CN_intprd_rec_history  
where  UpdateDtTm >=  dateadd(month, datediff(month, 0, getdate()) - 12, 0) and UpdateDtTm <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0))  
group by UpdateDtTm, RTRIM(LTrim(prd_FRM)) + '/' + RTRIM(LTrim(prd_GRD)) 
  END
  
--select * from #tmp;  
  
   
select dDate,sum(InventoryWgt) as InventoryWgt, Product into #tmpMain from #tmp group by dDate, Product ;  
  
SELECT  dDate,InventoryWgt,Product  
   FROM (SELECT  
   ROW_NUMBER() OVER ( PARTITION BY dDate ORDER BY dDate desc, InventoryWgt DESC ) AS 'RowNumber',  
   dDate,InventoryWgt,Product  
   FROM #tmpMain   
   ) dt  
   WHERE RowNumber <= @NoOfRecords 
   order by dDate desc , InventoryWgt desc  
      
     drop table #tmp;  
  drop table #tmpMain;  
  
END  
-- exec [dbo].[sp_itech_GetTOP20_InvetoryLBS] exec [dbo].[sp_itech_GetTOP20_InvetoryLBS] 30
GO
