USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_SCRSIC]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Sumit>  
-- Create date: <30 Sept 2020>  
-- Description: <to get Sub Classification/Market>  
  
-- =============================================  
create PROCEDURE [dbo].[CA_SCRSIC]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CA_scrsic_rec', 'U') IS NOT NULL  
  drop table dbo.CA_scrsic_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.CA_scrsic_rec  
from [LIVECASTX].[livecastxdb].[informix].scrsic_rec;   
  
END
GO
