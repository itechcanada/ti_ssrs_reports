USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_trjtph]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,10/1/2012,>  
-- Description: <Description,Warehouse Shipment On Time Performance,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[TW_trjtph]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.TW_trjtph_rec', 'U') IS NOT NULL        
  drop table dbo.TW_trjtph_rec;        
            
                
SELECT *        
into  dbo.TW_trjtph_rec 
FROM [LIVETWSTX].[livetwstxdb].[informix].[trjtph_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
