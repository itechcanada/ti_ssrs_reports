USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CRMStatistics_By_SalePerson_summaryBranch_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                                
-- Author:  <Mukesh >                                
-- Create date: <27 Oct 2016>                                
-- Modified by: <Mukesh>                               
                              
-- =============================================                                
CREATE PROCEDURE [dbo].[sp_itech_CRMStatistics_By_SalePerson_summaryBranch_V1] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(50),@SalePerson as Varchar(65)                               
                                
AS                                
BEGIN                                
                                 
 SET NOCOUNT ON;                       
                     
    
    
declare @sqltxtAct varchar(8000)                                
declare @execSQLtxtAct varchar(8000)                                
declare @sqltxtTsk varchar(8000)                                
declare @execSQLtxtTsk varchar(8000)                                
declare @DB varchar(100)                                
declare @FD varchar(10)                                
declare @TD varchar(10)                                
                                
set @DB=  @DBNAME                                
                      
IF @Branch = 'ALL'                                
 BEGIN                                
  set @Branch = ''                                
 END                                  
                                 
 IF @SalePerson = 'ALL'                                
 BEGIN                                
  set @SalePerson = ''                                
 END                                
                                 
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                                
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                                
                                
CREATE TABLE #tmpTsk ([Database]   VARCHAR(10)                      
  ,Branch Varchar(10)                              
     , LoginID   VARCHAR(50)                                
   , MonthYear   VARCHAR(7)                                 
        , ActDormant  integer                                
        , ActExisting  integer                                
        , ActProspect  integer                                
        , TskDormant  integer                                
        , TskExisting  integer                                
        , TskProspect  integer                                
		, TskUnqualifiedLead  integer                                
        , ActCallDormant  integer                                
        , ActCallExisting  integer                                
        , ActCallProspect  integer                                
        , TskCallDormant  integer                                
        , TskCallExisting  integer                                
        , TskCallProspect  integer                                
		, TskCallUnqualifiedLead  integer
                 );                                 
                            
DECLARE @DatabaseName VARCHAR(35);                                
DECLARE @Prefix VARCHAR(35);                                
DECLARE @Name VARCHAR(15);                                
                                
IF @DBNAME = 'ALL'                                
 BEGIN                                
                                    
    DECLARE ScopeCursor CURSOR FOR                                  
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                   
   OPEN ScopeCursor;                                  
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                
     WHILE @@FETCH_STATUS = 0                                
       BEGIN                                
        DECLARE @queryTsk NVARCHAR(Max);                                   
      SET @DB= @Prefix                                  
                                        
        SET @queryTsk =     
              'INSERT INTO #tmpTsk ([Database],Branch, LoginID, MonthYear, TskCallDormant, TskCallExisting, TskCallProspect, TskCallUnqualifiedLead, TskDormant, TskExisting, TskProspect, TskUnqualifiedLead)                              
      Select * from (                               
      select ''' +  @Prefix + ''' as [Database], usr_usr_brh as branch , cta_tsk_asgn_to ,          
      Case when (cta_tsk_strt_dtts < cta_crtd_dtts) then CONVERT(VARCHAR(7), cta_crtd_dtts, 120)  else CONVERT(VARCHAR(7), cta_tsk_strt_dtts, 120)  end as MonthDate          
      ,  rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                               
       from ' + @DB + '_cctcta_rec join                               
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1                 
        join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to                 
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                          
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR                      
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                             
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')                        
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                                
       group by  cta_tsk_asgn_to, usr_usr_brh ,  atp_desc30             
       ,cta_tsk_strt_dtts,cta_crtd_dtts           
       ,CONVERT(VARCHAR(7), cta_crtd_dtts, 120) ,CONVERT(VARCHAR(7), cta_tsk_strt_dtts, 120)                    
       ) AS Query1                                
      pivot (SUM(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Phone Call Unqualified Lead],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account],[Visit Unqualified Lead])) As po'                          
     
   
        EXECUTE sp_executesql @queryTsk;                                
        print(@queryTsk);                                
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                
       END                                 
    CLOSE ScopeCursor;                                
    DEALLOCATE ScopeCursor;                                
  END                                
  ELSE                                
     BEGIN                                    
 -- task                                  
                                    
      SET @sqltxtTsk ='INSERT INTO #tmpTsk ([Database],Branch, LoginID, MonthYear, TskCallDormant, TskCallExisting, TskCallProspect, TskCallUnqualifiedLead, TskDormant, TskExisting, TskProspect, TskUnqualifiedLead)                              
      Select * from (                               
      select ''' +  @DB + ''' as [Database], usr_usr_brh as branch,  cta_tsk_asgn_to,           
      Case when (cta_tsk_strt_dtts < cta_crtd_dtts) then CONVERT(VARCHAR(7), cta_crtd_dtts, 120)  else CONVERT(VARCHAR(7), cta_tsk_strt_dtts, 120)  end as MonthDate          
      ,rtrim(atp_desc30) As att_desc30, COUNT(*) as counts                               
       from ' + @DB + '_cctcta_rec join                               
       ' + @DB + '_ccratp_rec on atp_actvy_tsk_purp = cta_crmtsk_purp and cta_crmacct_typ = 1                
         join ' + @DB + '_scrslp_rec on slp_cmpy_id =  cta_cmpy_id and slp_lgn_id =  cta_tsk_asgn_to                 
       join ' + @DB + '_mxrusr_rec on  usr_lgn_id = slp_lgn_id                        
       where ((CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_tsk_strt_dtts , 120) <= '''+ @TD +'''   ) OR                      
       ( CONVERT(VARCHAR(10), cta_crtd_dtts , 120) >= '''+ @FD +''' and CONVERT(VARCHAR(10), cta_crtd_dtts , 120) <= '''+ @TD +'''   ))                   
       And (cta_tsk_asgn_to = ''' + @SalePerson + ''' OR ''' + @SalePerson + ''' = '''')                           
       And (usr_usr_brh = ''' + @Branch + ''' OR ''' + @Branch + ''' = '''')                           
   group by  cta_tsk_asgn_to,usr_usr_brh, atp_desc30            
       ,cta_tsk_strt_dtts,cta_crtd_dtts           
       ,CONVERT(VARCHAR(7), cta_crtd_dtts, 120) ,CONVERT(VARCHAR(7), cta_tsk_strt_dtts, 120)                
       ) AS Query1                                
      pivot (SUM(counts) for att_desc30 in([Phone Call - Lost/Dormant],[Phone Call - Existing Account],[Phone Call - Prospect Account],[Phone Call Unqualified Lead],[Visit - Lost/Dormant Account],[Visit - Existing Account],[Visit - Prospect Account],[Visit Unqualified Lead])) As po'                          
     
   
       
        
                                    
    print(@sqltxtTsk)                                
    set @execSQLtxtTsk = @sqltxtTsk;                                 
   EXEC (@execSQLtxtTsk);                                
     END                                
   -- SELECT * FROM #tmpAct Order by MonthYear desc, LoginID                            
   -- SELECT * FROM #tmpTsk Order by MonthYear desc, LoginID                                
   SELECT   [Database],  Branch,              
   MonthYear,                       
    LTRIM(RTRIM(tsk.LoginID))  AS LoginID,                            
    SUM(ISNULL(tsk.TskCallDormant,0)) AS TskCallDormant,                                 
    Sum(ISNULL(tsk.TskCallExisting,0)) AS TskCallExisting,                                
    Sum(ISNULL(tsk.TskCallProspect,0)) AS TskCallProspect,                               
	Sum(ISNULL(tsk.TskCallUnqualifiedLead,0)) AS TskCallUnqualifiedLead,   
    Sum(ISNULL(tsk.TskDormant,0)) AS TskDormant,                                 
    Sum(ISNULL(tsk.TskExisting,0)) AS TskExisting,                                
    Sum(ISNULL(tsk.TskProspect,0)) AS TskProspect,
	Sum(ISNULL(tsk.TskUnqualifiedLead,0)) AS TskUnqualifiedLead 
    FROM #tmpTsk as tsk group by [Database],  Branch,  LoginID, MonthYear          
    order by  LoginID                                
                                    
    Drop table #tmpTsk ;                           
END                                
                                
-- exec sp_itech_CRMStatistics_By_SalePerson_summaryBranch_V1 '12/01/2019', '01/31/2020' , 'ALL','ALL' ,'ALL'       
-- exec sp_itech_CRMStatistics_By_SalePerson_summaryBranch_V1 '07/01/2020', '07/09/2020' , 'US','ROS' ,'ALL'  
/*                      
20171221                      
Mail Sub: CRM Data Issue                
              
20190904              
Mail Sub:DMS Report - LAX Branch Update Request               
join                              
       ' + @DB + '_arrcus_rec on cus_cmpy_id =  cta_cmpy_id and cta_crmacct_id = cus_cus_id            
20200131          
Mail Sub:CRM Report Mod Request            
  
20200709 Sumit  
Mail : Fwd: FW: CRM report shows no dormant calls for July---something's is up with the report--yesterday it was fine?  
Modify Phone Call - Dormant Account >> Phone Call - Lost/Dormant and Visit - Dormant Account >> Visit - Lost/Dormant Account  

20200818	Sumit
Mail : RE: CRM
add Phone Call Unqualified Lead   ,Visit Unqualified Lead    
*/   
  
  
GO
