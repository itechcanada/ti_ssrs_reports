USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_JobYield]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mukesh >
-- Create date: <11 NOV 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_JobYield]  @ActivityFromDate datetime, @ActivityToDate datetime, @DBNAME varchar(50), @PRS VARCHAR(50)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt1 varchar(8000)
declare @sqltxt2 varchar(8000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)
declare @FD varchar(10)
declare @TD varchar(10)
set @FD = CONVERT(VARCHAR(10), @ActivityFromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ActivityToDate , 120)

set @DB=  @DBNAME
IF @PRS = 'ALL'
BEGIN
SET @PRS = ''
END


CREATE TABLE #tmp ( [Database]		 VARCHAR(10)
					, actvy_whs		 VARCHAR(3)
   					, actvy_dt		 datetime
   					, pwg		     Varchar(3)
   					, pwc			 Varchar(3)
   					, lgn_id		 Varchar(8)
   					, ref_ipno		 int
   					, trh_prs_1 	 Varchar(3)
   					, trh_prs_2		 Varchar(3) 
   					, trt_tcons_wgt  int
   					, trt_tprod_wgt  int
                    , sum_drp_mst  	 int
                    ,YLD_PCT		 DECIMAL(20, 1)
                    ,trjct_wgt		 int
                    ,RJCT_PCT		 DECIMAL(20, 1)
                    ,tscr_wgt		 int
                    ,Sum1			 DECIMAL(20, 1)
                    ,Sum2			 DECIMAL(20, 1)
                    ,SCR_PCT		 DECIMAL(20, 1)
                    ,tuscr_wgt		 DECIMAL(20, 1)
                    ,USCR_WGT_PCT	 DECIMAL(20, 1)
                    ,YLD_LS_PCT		 DECIMAL(20, 1)
                    , ref_itm		 int
                    , ref_sbitm		 int
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query1 NVARCHAR(max);  	
  	  			DECLARE @query2 NVARCHAR(max);  	
  				SET @DB= @Prefix 
  				if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')	
			     Begin
			        SET @query1 ='INSERT INTO #tmp ([Database], actvy_whs, actvy_dt, pwg, pwc, lgn_id, ref_ipno, trh_prs_1, trh_prs_2, trt_tcons_wgt, trt_tprod_wgt
													, sum_drp_mst,YLD_PCT,trjct_wgt,RJCT_PCT,tscr_wgt,Sum1,Sum2,SCR_PCT,tuscr_wgt,USCR_WGT_PCT,YLD_LS_PCT, ref_itm, ref_sbitm)
			                
			                SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
								    ' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0) AS Sum1,
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0) AS Sum2,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) ELSE 0.0 END AS USCR_WGT_PCT,
									CASE WHEN (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) > 0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / 
									(' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) ELSE 0.0 END AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm									

							FROM ' + @DB + '_ipjtrh_rec , ' + @DB + '_ipjtrt_rec , ' + @DB + '_injith_rec , ' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0 
								   
								   UNION ALL 
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0, 
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0, 
									0,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm 
							FROM ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL '
								   
					Set @query2 = ' SELECT 
									''' +  @DB + ''' as [Database],        
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
								    ' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,  
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0,  
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							   ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) AS USCR_WGT_PCT,
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							  ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
						WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0;'
			                
			     End
			     Else
			     Begin
			         SET @query1 ='INSERT INTO #tmp ([Database], actvy_whs, actvy_dt, pwg, pwc, lgn_id, ref_ipno, trh_prs_1, trh_prs_2, trt_tcons_wgt, trt_tprod_wgt
													, sum_drp_mst,YLD_PCT,trjct_wgt,RJCT_PCT,tscr_wgt,Sum1,Sum2,SCR_PCT,tuscr_wgt,USCR_WGT_PCT,YLD_LS_PCT, ref_itm, ref_sbitm)
			                
			                SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
								    ' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0) AS Sum1,
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0) AS Sum2,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) ELSE 0.0 END AS USCR_WGT_PCT,
									CASE WHEN (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) > 0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / 
									(' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) ELSE 0.0 END AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm

							FROM ' + @DB + '_ipjtrh_rec , ' + @DB + '_ipjtrt_rec , ' + @DB + '_injith_rec , ' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0 
								   
								   UNION ALL 
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0, 
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0, 
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0'
								   
				Set @query2 = ' UNION ALL SELECT  
									''' +  @DB + ''' as [Database],       
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
								    ' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,  
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0,  
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							   ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) AS USCR_WGT_PCT,
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							  ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
						WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0;'
			     End

  	  			DECLARE @query3 NVARCHAR(MAX);  	
  				 
  				 Set @query3 = @query1 + @query2;
  				 print @query3;
  	  			EXECUTE sp_executesql @query3;  --@query2
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN
			if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')	
			Begin
			    SET @sqltxt1 ='INSERT INTO #tmp ([Database], actvy_whs, actvy_dt, pwg, pwc, lgn_id, ref_ipno, trh_prs_1, trh_prs_2, trt_tcons_wgt, trt_tprod_wgt
													, sum_drp_mst,YLD_PCT,trjct_wgt,RJCT_PCT,tscr_wgt,Sum1,Sum2,SCR_PCT,tuscr_wgt,USCR_WGT_PCT,YLD_LS_PCT, ref_itm, ref_sbitm)
			                
			                SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
								    ' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0) AS Sum1,
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0) AS Sum2,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) ELSE 0.0 END AS USCR_WGT_PCT,
									CASE WHEN (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) > 0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / 
									(' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) ELSE 0.0 END AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm

							FROM ' + @DB + '_ipjtrh_rec , ' + @DB + '_ipjtrt_rec , ' + @DB + '_injith_rec , ' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0 
								   
								   UNION ALL 
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0, 
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0, 
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL '
								   
				Set @sqltxt2 = ' SELECT
									''' +  @DB + ''' as [Database],         
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
								    ' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,  
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0,  
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							   ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt*2.20462,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) AS USCR_WGT_PCT,
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							  ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
						WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0;'
			End
			Else
			Begin

			     SET @sqltxt1 ='INSERT INTO #tmp ([Database], actvy_whs, actvy_dt, pwg, pwc, lgn_id, ref_ipno, trh_prs_1, trh_prs_2, trt_tcons_wgt, trt_tprod_wgt
													, sum_drp_mst,YLD_PCT,trjct_wgt,RJCT_PCT,tscr_wgt,Sum1,Sum2,SCR_PCT,tuscr_wgt,USCR_WGT_PCT,YLD_LS_PCT, ref_itm, ref_sbitm)
			                
			                SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
								    ' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0) AS Sum1,
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0) AS Sum2,
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) ELSE 0.0 END AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									CASE WHEN ' + @DB + '_ipjtrt_rec.trt_tcons_wgt>0 THEN ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) ELSE 0.0 END AS USCR_WGT_PCT,
									CASE WHEN (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) > 0 THEN ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / 
									(' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) ELSE 0.0 END AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm

							FROM ' + @DB + '_ipjtrh_rec , ' + @DB + '_ipjtrt_rec , ' + @DB + '_injith_rec , ' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0 
								   
								   UNION ALL 
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0) AS sum_drp_mst,
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0, 
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0, 
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL '
								   
				 Set @sqltxt2 = ' SELECT  ''' +  @DB + ''' as [Database],       
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
								    ' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									0,  
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									0,   
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									0,  
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt + ' + @DB + '_ipjtrt_rec.trt_tuscr_wgt) / (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt)) * 100),1) AS YLD_LS_PCT,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							   ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
							WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) <> 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt = 0
								   
								   UNION ALL
								   
							SELECT  ''' +  @DB + ''' as [Database],
									' + @DB + '_ipjtrh_rec.trh_actvy_whs,
									' + @DB + '_ipjtrh_rec.trh_actvy_dt,
									' + @DB + '_ipjtrh_rec.trh_pwg,
									' + @DB + '_ipjtrh_rec.trh_pwc,
									' + @DB + '_injith_rec.ith_upd_lgn_id,
									' + @DB + '_ipjtrh_rec.trh_ref_no,
									' + @DB + '_ipjtrh_rec.trh_prs_1,
									' + @DB + '_ipjtrh_rec.trh_prs_2,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tcons_wgt,0),
									ROUND(' + @DB + '_ipjtrt_rec.trt_tprod_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tprod_wgt + ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt + ' + @DB + '_ipjtrt_rec.trt_tmst_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS YLD_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_trjct_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_trjct_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS RJCT_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tscr_wgt,0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt),0),
									ROUND((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt),0),
									ROUND((((' + @DB + '_ipjtrt_rec.trt_tetrm_wgt + ' + @DB + '_ipjtrt_rec.trt_tecut_wgt + ' + @DB + '_ipjtrt_rec.trt_tkrfls_wgt + ' + @DB + '_ipjtrt_rec.trt_tscr_wgt) / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt) * 100),1) AS SCR_PCT,
									ROUND(' + @DB + '_ipjtrt_rec.trt_tuscr_wgt,0),
									ROUND(((' + @DB + '_ipjtrt_rec.trt_tuscr_wgt / ' + @DB + '_ipjtrt_rec.trt_tcons_wgt ) * 100),1) AS USCR_WGT_PCT,
									0  ,
									' + @DB + '_ipjtrh_rec.trh_ref_itm,
									' + @DB + '_ipjtrh_rec.trh_ref_sbitm
							FROM
							  ' + @DB + '_ipjtrh_rec ,' + @DB + '_ipjtrt_rec ,' + @DB + '_injith_rec ,' + @DB + '_scrwhs_rec
						WHERE ( trh_actvy_dt >= '''+ @FD +''' AND trh_actvy_dt <= '''+ @TD +''' )
								   AND ' + @DB + '_ipjtrh_rec.trh_cmpy_id = ' + @DB + '_injith_rec.ith_cmpy_id
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_pfx = ' + @DB + '_injith_rec.ith_ref_pfx
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_no = ' + @DB + '_injith_rec.ith_ref_no
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_itm = ' + @DB + '_injith_rec.ith_ref_itm
								   AND ' + @DB + '_ipjtrh_rec.trh_ref_sbitm = ' + @DB + '_injith_rec.ith_ref_sbitm
								   AND ' + @DB + '_scrwhs_rec.whs_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_scrwhs_rec.whs_whs = ' + @DB + '_ipjtrh_rec.trh_actvy_whs
								   AND ' + @DB + '_ipjtrt_rec.trt_cmpy_id = ' + @DB + '_ipjtrh_rec.trh_cmpy_id
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_pfx = ' + @DB + '_ipjtrh_rec.trh_ref_pfx
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_no = ' + @DB + '_ipjtrh_rec.trh_ref_no
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_itm = ' + @DB + '_ipjtrh_rec.trh_ref_itm
								   AND ' + @DB + '_ipjtrt_rec.trt_ref_sbitm = ' + @DB + '_ipjtrh_rec.trh_ref_sbitm
								   AND (' + @DB + '_ipjtrt_rec.trt_tcons_wgt - ' + @DB + '_ipjtrt_rec.trt_tmst_wgt - ' + @DB + '_ipjtrt_rec.trt_tdrp_wgt) = 0 
								   AND ' + @DB + '_ipjtrt_rec.trt_tcons_wgt <> 0;'
			End
			       
					print(@sqltxt1)	
					print(@sqltxt2)
				--set @execSQLtxt = @sqltxt; 
			EXEC (@sqltxt1 + @sqltxt2);
   END
   
select distinct [Database],actvy_whs, actvy_dt, pwg, pwc, lgn_id, cast( ref_ipno as varchar) + '-' + cast( ref_itm as varchar) + '-' + cast(  ref_sbitm as varchar) as  ref_no, trh_prs_1, trh_prs_2, trt_tcons_wgt, trt_tprod_wgt
				, sum_drp_mst,YLD_PCT,trjct_wgt,RJCT_PCT,tscr_wgt,Sum1,Sum2,SCR_PCT,tuscr_wgt,USCR_WGT_PCT,YLD_LS_PCT ,
				case [Database]

				when 'US' then ( select SUM(itv_trs_pcs) from US_injitv_rec join US_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'USS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

when 'UK' then ( select SUM(itv_trs_pcs) from UK_injitv_rec join UK_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'UKS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

when 'TW' then ( select SUM(itv_trs_pcs) from TW_injitv_rec join TW_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'TWS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

when 'NO' then ( select SUM(itv_trs_pcs) from NO_injitv_rec join NO_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'NOS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

when 'CN' then ( select SUM(itv_trs_pcs) from CN_injitv_rec join CN_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'CNS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

when 'CA' then ( select SUM(itv_trs_pcs) from CA_injitv_rec join CA_injitd_rec
on itd_cmpy_id = itv_cmpy_id and itd_ref_no = itv_ref_no and itd_ref_pfx = itv_ref_pfx
and itd_ref_itm = itv_ref_itm and itd_ref_sbitm = itv_ref_sbitm and itv_trs_seq_no = itd_trs_seq_no and itv_actvy_dt = itd_actvy_dt
where itv_cmpy_id = 'CAS' and itv_ref_pfx = 'IP'  and itv_ref_no = ref_ipno and itv_ref_itm = ref_itm  and itv_ref_sbitm = ref_sbitm
) 

				Else 0
				End	as TrsPcs,
case [Database]

				when 'US' then (SELECT  sum(tdc_bas_cry_val)
FROM US_injtdc_rec where tdc_cmpy_id = 'USS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)

when 'UK' then ( SELECT  sum(tdc_bas_cry_val)
FROM UK_injtdc_rec where tdc_cmpy_id = 'UKS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)
 

when 'TW' then ( SELECT  sum(tdc_bas_cry_val)
FROM TW_injtdc_rec where tdc_cmpy_id = 'TWS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)
 

when 'NO' then (SELECT  sum(tdc_bas_cry_val)
FROM NO_injtdc_rec where tdc_cmpy_id = 'NOS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)
 

when 'CN' then (SELECT  sum(tdc_bas_cry_val)
FROM CN_injtdc_rec where tdc_cmpy_id = 'CNS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)

when 'CA' then ( SELECT  sum(tdc_bas_cry_val)
FROM CA_injtdc_rec where tdc_cmpy_id = 'CAS' and tdc_ref_pfx = 'IP'  and tdc_ref_no = ref_ipno and tdc_ref_itm = ref_itm  and tdc_ref_sbitm = ref_sbitm)

				Else 0
				End	as CstVal				
													
 from #tmp  
 where (trh_prs_1 = '' + @PRS + '' OR '' + @PRS + '' = '')
 order by  [Database], actvy_whs, actvy_dt , ref_no
   drop table #tmp
END

-- exec [sp_itech_JobYield] '2013-11-01','2013-11-30','US', 'WJT'


--Select top 20 * from US_ipjtrh_rec
--Select top 20 * from US_injith_rec
GO
