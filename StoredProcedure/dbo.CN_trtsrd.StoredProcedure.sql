USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_trtsrd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		iTECH 
-- Create date: April 4, 2013
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[CN_trtsrd] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    
    IF OBJECT_ID('dbo.CN_trtsrd_rec', 'U') IS NOT NULL
		drop table dbo.CN_trtsrd_rec;
    
        
SELECT *
into  dbo.CN_trtsrd_rec
FROM [LIVECNSTX].[livecnstxdb].[informix].[trtsrd_rec];

END
-- select * from CN_trtsrd_rec
-- exec CN_trtsrd 
GO
