USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_PurchaseJournal]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Mukesh >            
-- Create date: <15 Jul 2019>            
-- Description: <Getting top 50 customers for SSRS reports>            
        
         
-- =============================================            
CREATE PROCEDURE [dbo].[sp_itech_PurchaseJournal] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@vendorID varchar(8)  
,@invoiceNO varchar(22)           
            
AS            
BEGIN           
      
             
 SET NOCOUNT ON;            
declare @sqltxt varchar(6000)            
declare @execSQLtxt varchar(7000)            
declare @DB varchar(100)            
declare @FD varchar(10)            
declare @TD varchar(10)            
DECLARE @ExchangeRate varchar(15)        
      
set @DB= @DBNAME       
             
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)            
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)            
            
CREATE TABLE #tmp (  CompanyId varchar(3)             
        ,VoucherPfx Varchar(2)            
        ,VoucherNo varchar(8)            
        ,JournalDate   varchar(10)            
        ,VendorID varchar(8)            
        ,VenInvNo   VARCHAR(22)             
        , VenInvDate     VARCHAR(10)            
        , VoucherBrh   VARCHAR(3)             
        , VoucherDesc    varchar(30)            
        , VoucherDueDate  varchar(10)            
        , VoucherDiscDate           varchar(10)             
        , VoucherAmt    DECIMAL(20, 2)            
        , VoucherCry               varchar(3)            
        , VoucherDscbAmt     DECIMAL(20, 2)             
        , VoucherDiscAmt    DECIMAL(20, 2)            
        , ExchangeRate   DECIMAL(20, 8)              
        , VenLongName     varchar(35)            
        ,BscGlAcct  varchar(8)            
        ,SAcct  varchar(30)            
        ,DRAmt  DECIMAL(20, 2)   
        ,CRAmt  DECIMAL(20, 2)            
        ,TDSAmt   DECIMAL(20, 2)          
        ,TotalExGnLs DECIMAL(20, 2)        
                 );        
            
DECLARE @DatabaseName VARCHAR(35);            
DECLARE @Prefix VARCHAR(5);            
DECLARE @Name VARCHAR(15);            
DECLARE @CurrenyRate varchar(15);             
            
 if @Branch ='ALL'            
 BEGIN            
 set @Branch = ''            
 END            
            
IF @DBNAME = 'ALL'            
 BEGIN            
              
    DECLARE ScopeCursor CURSOR FOR              
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
   OPEN ScopeCursor;              
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
     WHILE @@FETCH_STATUS = 0            
       BEGIN            
        DECLARE @query nvarchar(max);               
      SET @DB= @Prefix          
      IF (UPPER(@Prefix) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@Prefix) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
 SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@Prefix) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End               
    Else if(UPPER(@Prefix) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End               
      SET @query =            
       'INSERT INTO #tmp ( CompanyId,VoucherPfx,VoucherNo,JournalDate ,VendorID ,VenInvNo , VenInvDate , VoucherBrh  , VoucherDesc , VoucherDueDate , VoucherDiscDate , VoucherAmt,  
 VoucherCry  , VoucherDscbAmt , VoucherDiscAmt  , ExchangeRate  , VenLongName ,BscGlAcct,SAcct ,DRAmt ,CRAmt,TDSAmt ,TotalExGnLs  )            
        SELECT  jvc_cmpy_id,  
         jvc_vchr_pfx,  
         jvc_vchr_no,  
         jvc_ent_dt,  
         jvc_ven_id,  
         jvc_ven_inv_no,  
         jvc_ven_inv_dt,  
         jvc_vchr_brh,  
         jvc_desc30,  
         jvc_due_dt,  
         jvc_disc_dt,  
         jvc_vchr_amt,  
         jvc_cry,  
         jvc_dscb_amt,  
         jvc_disc_amt,  
         jvc_exrt,        
         ven_ven_long_nm,  
         glj_bsc_gl_acct,  
         glj_sacct,  
         glj_dr_amt,  
         glj_cr_amt,  
         pjt_tdsamt,  
         pjt_tot_ex_gn_ls  
  
FROM   
' + @DB + '_apjjvc_rec  
join ' + @DB + '_apjglj_rec on jvc_cmpy_id  = glj_cmpy_id  
      AND jvc_vchr_pfx = glj_vchr_pfx  
      AND jvc_vchr_no  = glj_vchr_no  
join ' + @DB + '_apjpjt_rec on  jvc_cmpy_id  = pjt_cmpy_id  
      AND jvc_vchr_pfx = pjt_vchr_pfx  
      AND jvc_vchr_no  = pjt_vchr_no  
join ' + @DB + '_aprven_rec on jvc_cmpy_id  = ven_cmpy_id  
      AND jvc_ven_id   = ven_ven_id       
WHERE jvc_ent_dt  BETWEEN  '''+ @FD +''' and   '''+ @TD +'''  
      AND (jvc_vchr_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
      AND (jvc_ven_id = '''+ @vendorID +''' or '''+ @vendorID +'''= '''')   
     AND (jvc_ven_inv_no = '''+ @invoiceNO +''' or '''+ @invoiceNO +'''= '''') '            
            
     print(@query);         
        EXECUTE sp_executesql @query;          
              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;            
       END             
    CLOSE ScopeCursor;            
    DEALLOCATE ScopeCursor;            
  END            
  ELSE            
     BEGIN             
            
     Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')              
                 
     IF (UPPER(@DB) = 'TW')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                    
    End                    
    Else if (UPPER(@DB) = 'NO')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                    
    End                    
    Else if (UPPER(@DB) = 'CA')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                    
    End                    
    Else if (UPPER(@DB) = 'CN')                    
    begin                    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                    
    End                    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                    
    begin                    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                    
    End                    
    Else if(UPPER(@DB) = 'UK')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                    
    End              
    Else if(UPPER(@DB) = 'DE')                    
    begin                    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                    
    End              
                
     SET @sqltxt ='INSERT INTO #tmp ( CompanyId,VoucherPfx,VoucherNo,JournalDate ,VendorID ,VenInvNo , VenInvDate , VoucherBrh  , VoucherDesc , VoucherDueDate , VoucherDiscDate , VoucherAmt,  
 VoucherCry  , VoucherDscbAmt , VoucherDiscAmt  , ExchangeRate  , VenLongName ,BscGlAcct,SAcct ,DRAmt ,CRAmt,TDSAmt ,TotalExGnLs)       
       SELECT  jvc_cmpy_id,  
         jvc_vchr_pfx,  
         jvc_vchr_no,  
         jvc_ent_dt,  
         jvc_ven_id,  
         jvc_ven_inv_no,  
         jvc_ven_inv_dt,  
         jvc_vchr_brh,  
         jvc_desc30,  
         jvc_due_dt,  
         jvc_disc_dt,  
         jvc_vchr_amt,  
         jvc_cry,  
         jvc_dscb_amt,  
         jvc_disc_amt,  
         jvc_exrt,        
         ven_ven_long_nm,  
         glj_bsc_gl_acct,  
         glj_sacct,  
         glj_dr_amt,  
         glj_cr_amt,  
         pjt_tdsamt,  
         pjt_tot_ex_gn_ls  
  
FROM   
' + @DB + '_apjjvc_rec  
join ' + @DB + '_apjglj_rec on jvc_cmpy_id  = glj_cmpy_id  
      AND jvc_vchr_pfx = glj_vchr_pfx  
      AND jvc_vchr_no  = glj_vchr_no  
join ' + @DB + '_apjpjt_rec on  jvc_cmpy_id  = pjt_cmpy_id  
      AND jvc_vchr_pfx = pjt_vchr_pfx  
      AND jvc_vchr_no  = pjt_vchr_no  
join ' + @DB + '_aprven_rec on jvc_cmpy_id  = ven_cmpy_id  
      AND jvc_ven_id   = ven_ven_id       
WHERE jvc_ent_dt  BETWEEN  '''+ @FD +''' and   '''+ @TD +'''  
      AND (jvc_vchr_brh = '''+ @Branch +''' or '''+ @Branch +'''= '''')    
      AND (jvc_ven_id = '''+ @vendorID +''' or '''+ @vendorID +'''= '''')   
     AND (jvc_ven_inv_no = '''+ @invoiceNO +''' or '''+ @invoiceNO +'''= '''')               
       '            
     print(@sqltxt);             
    set @execSQLtxt = @sqltxt;             
       EXEC (@execSQLtxt);            
     END            
     
   SELECT CompanyId,VoucherPfx + RIGHT('00000'+ CONVERT(VARCHAR,VoucherNo),8) as RefNo,JournalDate ,VendorID ,VenInvNo , VenInvDate , VoucherBrh  ,  
    VoucherDesc , VoucherDueDate , VoucherDiscDate , VoucherAmt,  
 VoucherCry  , VoucherDscbAmt , VoucherDiscAmt  , ExchangeRate  , VenLongName ,BscGlAcct,substring(SAcct,0,3) as SAcct,DRAmt ,CRAmt,TDSAmt ,TotalExGnLs FROM #tmp   ;   
   drop table #tmp;      
END            
            
-- @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(10),@vendorID varchar(8),@invoiceNO varchar(22)   
            
-- exec [sp_itech_PurchaseJournal] '01/01/2019', '01/31/2019' , 'US','ALL','303',''     
-- exec [sp_itech_PurchaseJournal] '05/01/2019', '05/31/2019' , 'ALL','ALL','ALL','ALL'      
        
/*       
      
date : 2019-07-15  
   
*/
GO
