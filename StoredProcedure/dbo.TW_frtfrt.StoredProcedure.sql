USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_frtfrt]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[TW_frtfrt]  
   
AS  
BEGIN  
 -- SET NOOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.TW_frtfrt_rec', 'U') IS NOT NULL        
  drop table dbo.TW_frtfrt_rec;        
            
                
SELECT *        
into  dbo.TW_frtfrt_rec 
FROM [LIVETWSTX].[livetwstxdb].[informix].[frtfrt_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
