USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_iprpwc]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
  
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,12/2/2013,>  
-- Description: <>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_iprpwc]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.US_iprpwc_rec', 'U') IS NOT NULL        
  drop table dbo.US_iprpwc_rec;        
            
                
SELECT *        
into  dbo.US_iprpwc_rec 
FROM [LIVEUSSTX].[liveusstxdb].[informix].[iprpwc_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
