USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_AR_Age_TrialBalance]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Mukesh >              
-- Create date: <15 Jan 2015>              
-- Description: <Getting result of AR Age Trial Balance >              
-- Last changes Date:        
--- Last updated :         
-- =============================================              
CREATE PROCEDURE [dbo].[sp_itech_AR_Age_TrialBalance] @ToDate datetime, @DBNAME varchar(50)        
AS              
BEGIN        
  
        
               
 SET NOCOUNT ON;              
declare @sqltxt varchar(max)              
declare @execSQLtxt varchar(max)              
declare @DB varchar(100)            
declare @FD varchar(10)              
declare @TD varchar(10)              
DECLARE @CurrenyRate varchar(15)              
              
SET @DB=@DBNAME;              
              
CREATE TABLE #tmp (      
   custID   varchar(9)    
   ,customerSName   varchar(15)  
   ,custAdminBrh   varchar(15)   
   ,customerName   varchar(35)    
         ,currency Varchar(3)    
         ,invDate Varchar(10)    
        ,invDueDate  Varchar(10)    
        ,invNumber Varchar(60)      
        ,branch Varchar(3)    
        ,exRt   DECIMAL(20, 2)            
        ,balanceAmt DECIMAL(20, 2)              
        ,baseCryBalance DECIMAL(20, 2)    
        ,currentDue DECIMAL(20, 2)   
        ,pastDue30 DECIMAL(20, 2)   
        ,pastDue60 DECIMAL(20, 2)   
        ,pastDue90 DECIMAL(20, 2)   
        ,pastDue120 DECIMAL(20, 2)    
        ,pastDueAbove120 DECIMAL(20, 2)   
         ,Databases Varchar(10)  
              
                 );               
              
DECLARE @DatabaseName VARCHAR(35);              
DECLARE @Prefix VARCHAR(5);              
DECLARE @Name VARCHAR(15);              
Declare @Value as varchar(500);              
        
  --  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)   --First day of previous month              
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120) --Last Day of previous month    
      
IF @DBNAME = 'ALL'              
 BEGIN              
              
  DECLARE ScopeCursor CURSOR FOR              
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS               
    OPEN ScopeCursor;              
            
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
     WHILE @@FETCH_STATUS = 0              
       BEGIN              
        DECLARE @query NVARCHAR(4000);               
                      
        IF (UPPER(@Prefix) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@Prefix) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@Prefix) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@Prefix) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@Prefix) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End
	Else if(UPPER(@Prefix) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End
                
      SET @query = 'INSERT INTO #tmp (Databases,  custID,customerSName, customerName,custAdminBrh, currency,invDate, invDueDate, invNumber, branch,exRt, balanceAmt, baseCryBalance,currentDue,pastDue30,pastDue60,pastDue90,pastDue120,pastDueAbove120 )      
       Select ''' + @Prefix + ''', cus_cus_id, cus_cus_nm, cus_cus_long_nm, brh_brh_nm, rvh_cry, rvh_inv_dt, rvh_due_dt, rvh_ar_pfx + ''-'' + rvh_upd_ref, rvh_ar_brh, '+ @CurrenyRate +', rvh_balamt ,   (rvh_balamt)* '+ @CurrenyRate +' as baseCryBalance,  
		(case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''')) < 0 then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue,   (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 0 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 30) then rvh_balamt * '+ @CurrenyRate +' else 0 end) as currentDue,
		(case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 31 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 60) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		(case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 61 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 90) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		(case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 91 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 120) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		(case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''')) >= 121 then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue  
          from ' + @Prefix + '_artrvh_rec   join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = rvh_cmpy_id and cus_cus_id = rvh_cr_ctl_cus_id    
          join ' + @Prefix + '_scrbrh_rec on brh_cmpy_id = cus_cmpy_id and brh_brh = cus_admin_brh   
            where rvh_inv_dt < ''' + @TD + ''' and rvh_arch_trs = 0 '     
           
       if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')          
       BEGIN          
       SET @query = @query + '  '          
       END          
       ELSE          
       BEGIN          
       SET @query = @query + '  '          
       END     
                
        SET @query = @query + '  '              
               
                  print(@query);              
        EXECUTE sp_executesql @query;              
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;              
       END               
    CLOSE ScopeCursor;              
    DEALLOCATE ScopeCursor;              
  END              
  ELSE              
     BEGIN               
                    
     IF (UPPER(@DB) = 'TW')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                
    End                
    Else if (UPPER(@DB) = 'NO')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                
    End                
    Else if (UPPER(@DB) = 'CA')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                
    End                
    Else if (UPPER(@DB) = 'CN')                
    begin                
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                
    End                
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                
    begin                
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                
    End                
    Else if(UPPER(@DB) = 'UK')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                
    End
	Else if(UPPER(@DB) = 'DE')                
    begin                
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                
    End
                
              
     SET @sqltxt ='INSERT INTO #tmp (Databases,  custID,customerSName, customerName, custAdminBrh, currency,invDate, invDueDate, invNumber, branch,exRt, balanceAmt, baseCryBalance,currentDue,pastDue30,pastDue60,pastDue90,pastDue120,pastDueAbove120)  
       Select ''' + @DB + ''', cus_cus_id, cus_cus_nm,cus_cus_long_nm,brh_brh_nm, rvh_cry, rvh_inv_dt, rvh_due_dt, rvh_ar_pfx + ''-'' + rvh_upd_ref, rvh_ar_brh,'+ @CurrenyRate +', rvh_balamt,   rvh_balamt* '+ @CurrenyRate +' as baseCryBalance,  
         (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''')) < 0 then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue,   (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 0 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 30) then rvh_balamt * '+ @CurrenyRate +' else 0 end) as currentDue, 
		 (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 31 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 60) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		 (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 61 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 90) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		 (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') >= 91 and DATEDIFF(Day,rvh_due_dt,''' + @TD + ''') <= 120) then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue, 
		 (case When (DATEDIFF(Day,rvh_due_dt,''' + @TD + ''')) >= 121 then rvh_balamt* '+ @CurrenyRate +' else 0 end) as currentDue  
    from ' + @DB + '_artrvh_rec   join ' + @DB + '_arrcus_rec on cus_cmpy_id = rvh_cmpy_id and cus_cus_id = rvh_cr_ctl_cus_id    
    join ' + @DB + '_scrbrh_rec on brh_cmpy_id = cus_cmpy_id and brh_brh = cus_admin_brh   
      where rvh_inv_dt < '''+ @TD +''' and rvh_arch_trs = 0 '          
       if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')          
       BEGIN          
       SET @sqltxt = @sqltxt + '  '          
       END          
       ELSE          
       BEGIN          
       SET @sqltxt = @sqltxt + '  '          
       END          
        SET @sqltxt = @sqltxt + '  '              
               
          --print(@sqltxt);              
     print(@DB);              
     print(@sqltxt);               
    set @execSQLtxt = @sqltxt;               
   EXEC (@execSQLtxt);              
     END              
  SELECT * FROM #tmp;-- where custID = '10993' ;             
  DROP TABLE #tmp;              
              
END              
              
 -- exec sp_itech_AR_Age_TrialBalance '2015-01-09' ,'US'     
/*
20210128	Sumit
Add germany database
*/
GO
