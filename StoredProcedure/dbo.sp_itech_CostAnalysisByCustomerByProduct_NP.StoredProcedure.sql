USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CostAnalysisByCustomerByProduct_NP]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Mayank >                      
-- Create date: <11 Feb 2013>                      
-- Description: <Getting top 50 customers for SSRS reports>                
-- Last changed date : <04 Feb 2015 >                      
-- Last changed Description : <Add new column shping warehouse >             
-- Last change Date: 29 Jun 2015            
-- Last changes By: Mukesh            
-- Last changes Desc: Remove the live connection of database             
-- Last change Date: 20 Jan 2016            
-- Last changes By: Mukesh            
-- Last changes Desc: Add includeInterco filter                     
-- =============================================                      
CREATE PROCEDURE [dbo].[sp_itech_CostAnalysisByCustomerByProduct_NP] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(50)='ALL',@CustomerID  varchar(Max),                
@DateRange int,@CustmomerNoTxt varchar(Max), @version char = '0' , @status char = 'I' , @IncludeInterco char = '0'                    
AS                      
BEGIN               
            
                       
 SET NOCOUNT ON;                      
declare @sqltxt varchar(max)                      
declare @execSQLtxt varchar(max)                      
declare @DB varchar(100)                    
declare @FD varchar(10)                      
declare @TD varchar(10)                      
declare @NOOfCust varchar(15)                      
DECLARE @CurrenyRate varchar(15)                      
DECLARE @IsExcInterco char(1)            
                      
SET @DB=@DBNAME;                      
                      
CREATE TABLE #tmp (  CustID varchar(15)                       
        ,CustName Varchar(65)                      
        ,InvDt varchar(15)                      
        ,Product   varchar(300)                      
        ,NoOfPcs numeric                      
        , Inches    DECIMAL(20, 2)                      
        , Weight  DECIMAL(20, 2)                      
        , Part   VARCHAR(50)                       
        ,InvNo numeric            
        ,OrdNo Varchar(100)                      
        , Date     varchar(65)                      
        ,Form  varchar(35)                      
        ,Grade  varchar(35)                      
        ,Size  varchar(35)                      
        ,Finish  varchar(35)                      
        ,Measure  DECIMAL(20, 2)                      
        ,TotalValue DECIMAL(20, 2)                      
        ,GPPct DECIMAL(20, 2)         
        ,NPAmt DECIMAL(20, 2)                     
        ,NPPct DECIMAL(20, 2)                     
        ,PONumber Varchar(75)                  
        ,TransportNo Varchar(30)     ,                
        MktCatg varchar(30),                
        ShpWhs varchar(3),            
        Branch varchar(10)            
        ,RefPfx varchar(2)                         
        ,RefNo numeric                        
        ,RefItm numeric                        
        ,CstNo   varchar(10)                        
        ,CstDesc Varchar(25)                        
        , Cost    DECIMAL(20, 4)                        
        , CostUM  Varchar(3)                        
        , BasCryVal   Decimal(20,2) 
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)                 
                 );                       
                      
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @Prefix VARCHAR(5);                      
DECLARE @Name VARCHAR(15);                      
DECLARE @CusID varchar(max);                      
Declare @Value as varchar(500);                      
DECLARE @CustIDLength int;   ---SELECT DATALENGTH(yourtextfield)                      
             
if (@IncludeInterco = '0')            
BEGIN            
set @IsExcInterco ='T'            
END            
                      
--SET @CustomerID= '1111111111,1003'                      
if @CustmomerNoTxt <> ''                      
 BEGIN                        set @CustomerID = @CustmomerNoTxt                      
 END                      
                      
SET @CustIDLength= (select DATALENGTH(RTRIM(LTRIM(@CustomerID))));                      
                      
if @CustomerID = ''                      
 BEGIN                      
  set @CustomerID = '0'                      
 END                      
                      
if @DateRange=1 -- Last month  (Last month excluding all the days in the current month )                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , 120)   --First day of previous month                      
  set @TD = CONVERT(VARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) , 120) --Last Day of previous month                      
 End                      
else if @DateRange=2 -- Last 7 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-7, GETDATE()) , 120)   -- Last 7 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End                      
else if @DateRange=3 -- Last 14 days (excluding today)                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), DATEADD(day,-14, GETDATE()) , 120)   -- Last 14 day                      
  set @TD = CONVERT(VARCHAR(10), GETDATE()-1 , 120) -- previous day                      
 End                      
else if @DateRange=4 --Last 12 months excluding all the days in the current month                      
 BEGIN                      
  set @FD = CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                      
  set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)  --Last Day of previous month                      
 End                      
else                      
 Begin                      
  set @FD = CONVERT(VARCHAR(10), @FromDate , 120)                      
  set @TD = CONVERT(VARCHAR(10), @ToDate , 120)                      
 End                   
                      
        --->> Input customer data                      
  DECLARE @pos int,@curruntLocation varchar(max), @input varchar(max)                      
  SELECT @pos=0                      
  SELECT @input =@CustomerID                     
  SELECT @input = @input + ','                      
  CREATE TABLE #tempTable (temp varchar(max) )                      
  WHILE CHARINDEX(',',@input) > 0                      
  BEGIN                      
   SELECT @pos=CHARINDEX(',',@input)                      
   SELECT @curruntLocation = LTRIM(RTRIM(SUBSTRING(@input,1,@pos-1)))                      
   INSERT INTO #tempTable (temp) VALUES (LTRIM(RTRIM(@curruntLocation)))                      
   SELECT @input=SUBSTRING(@input,@pos+1,@CustIDLength)                      
  END                      
  select @input= COALESCE(@input + ',','') + ''''+ temp +'''' from #tempTable                      
  set @CusID =(Select right(@input, len(@input)-1) as AfterRemoveFistCharacter)                      
  DROP TABLE #tempTable      
      
  if @Branch = 'ALL'    
 BEGIN    
 set @Branch = ''    
 END                      
                        
IF @DBNAME = 'ALL'                      
 BEGIN                      
 IF @version = '0'                      
  BEGIN                      
  DECLARE ScopeCursor CURSOR FOR                      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName                      
    OPEN ScopeCursor;                      
  END                      
  ELSE                      
  BEGIN                      
  DECLARE ScopeCursor CURSOR FOR                      
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                       
    OPEN ScopeCursor;                      
  END                      
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
     WHILE @@FETCH_STATUS = 0                      
       BEGIN                      
        DECLARE @query NVARCHAR(max);                       
        DECLARE @query1 NVARCHAR(max);
        IF (UPPER(@Prefix) = 'TW')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                        
    End                        
    Else if (UPPER(@Prefix) = 'NO')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                        
    End                        
    Else if (UPPER(@Prefix) = 'CA')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                        
    End                        
    Else if (UPPER(@Prefix) = 'CN')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                        
    End                        
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')                        
    begin                        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                        
    End                        
    Else if(UPPER(@Prefix) = 'UK')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))               
    End                         
    Else if(UPPER(@Prefix) = 'DE')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))               
    End                         
      if @status = 'I'                    
      BEGIN                    
      SET @query = 'INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo,OrdNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,
		CstDesc ,Cost,CostUM,BasCryVal,SalesPresonIs,SalesPresonOs )
       SELECT  stn_sld_cus_id as CustID , cus_cus_long_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, '
       if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                      
       BEGIN                      
       SET @query = @query + ' stn_blg_wgt * 2.20462 as Weight, '
       END                      
       ELSE                      
       BEGIN                      
       SET @query = @query + ' stn_blg_wgt as Weight, '            
       END                      
        SET @query = @query + ' (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))
        from ' + @Prefix + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product,      
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  ivh_inv_due_dt as Date ,stn_blg_MSR as Measure,        
       STN_TOT_VAL * '+ @CurrenyRate +',
		CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',
        (CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE (stn_npft_avg_val/STN_TOT_VAL)*100 end) as NPPct,        
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_mpft_avg_val  * '+ @CurrenyRate +' END as ''GPPct'',
       stn_blg_pcs,stn_upd_ref, convert(Varchar(10),stn_ord_no) + ''-'' + Convert(varchar(10),stn_ord_itm) + ''-'' + Convert(varchar(10),stn_ord_rls_no), stn_cus_po,stn_transp_pfx + ''-'' + Convert(Varchar(15),stn_transp_no) + ''-'' + Convert(Varchar(5), 
       stn_shpt_itm) , cuc_desc30, stn_shpg_whs, stn_shpt_brh ,
       csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val * '+ @CurrenyRate +',isslp.slp_lgn_id, osslp.slp_lgn_id
       FROM  ' + @Prefix + '_sahstn_rec
       join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id
       left  join ' + @Prefix + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat
       left join ' + @Prefix + '_cttcsi_rec on  csi_cmpy_id = stn_cmpy_id and  csi_ref_pfx = stn_shpt_pfx and csi_ref_no = stn_shpt_no  and csi_ref_itm = stn_shpt_itm 
       left join ' + @Prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = stn_cmpy_id and isslp.slp_slp = stn_is_slp     
       left join ' + @Prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = stn_Cmpy_id and osslp.slp_slp = stn_os_slp  
       left join ' + @Prefix + '_ivtivh_rec on stn_cmpy_id = ivh_cmpy_id and stn_upd_ref = ivh_upd_ref 
       ,  ' + @Prefix + '_inrfrm_rec, ' + @Prefix + '_inrgrd_rec  
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'
                             
     if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @query += ' and  (stn_sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                     
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @query += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                      
             Set @query += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @query += ' and stn_Frm <> ''XXXX'' and (stn_shpt_brh = '''+ @Branch +''' or ''' + @Branch +''' = '''')'                       
   END                    
   ELSE                    
   BEGIN                    
    SET @query ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo,OrdNo,PONumber, TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,CstDesc
    ,Cost,CostUM,BasCryVal,SalesPresonIs,SalesPresonOs )
        select  ivs_Sld_cus_id, cus_cus_long_nm as CustName, ivh_inv_dt , ipd_lgth,'
        -- Changed by mrinal on 19-05              
        print('MR ' + @Prefix)            
        if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')                      
     BEGIN                      
     SET @query = @query + ' sum(ivd_blg_wgt) * 2.20462 , '                      
     END                      
       ELSE                      
     BEGIN                      
     SET @query = @query + ' sum(ivd_blg_wgt),'                      
     END              
                      
        SET @query = @query + ' (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh))
     from ' + @Prefix + '_inrprm_rec where prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size = ipd_size and prm_fnsh = ipd_fnsh) as Product,
     ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_part,ivh_inv_due_dt, sum(ivd_blg_msr), SUM(tot.chl_chrg_val) * '+ @CurrenyRate +' ,        
     (SUM(cht_tot_val) - sum(oit_tot_avg_val)) * '+ @CurrenyRate +'  as NPAmt,
      case when sum(cht_tot_val) > 0
                        then              
                                    (SUM(cht_tot_val) - sum(oit_tot_avg_val))/sum(cht_tot_val) * 100
                                    else
                                    0                 
                              end                 
                              as NetPftVal,
                              case when sum(cht_tot_val) > 0
                              then
                                    (SUM(cht_tot_val) - sum(oit_mtl_avg_val))/sum(cht_tot_val) * 100
                                    else
                                    0                 
                              end                  
                              as MPFTAvgVal,
       sum(ivd_blg_pcs),
     ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivs_ord_no,ivd_cus_po,ivs_transp_pfx + ''-'' + Convert(Varchar(15),ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm)  ,
     cuc_desc30, ivd_shpg_whs , ipd_brh,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val * '+ @CurrenyRate +' ,isslp.slp_lgn_id, osslp.slp_lgn_id
       from ' + @Prefix + '_ivtivs_rec
       join  ' + @Prefix + '_ivtivh_rec on
       ivs_cmpy_id = ivh_cmpy_id and ivs_inv_pfx = ivh_inv_pfx and ivs_inv_no = ivh_inv_no
       join ' + @Prefix + '_ortoit_rec on oit_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = oit_ref_pfx and
                                oit_ref_itm = 0 and ivs_Shpt_no = oit_ref_no
             join ' + @Prefix + '_ortcht_rec on cht_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = cht_ref_pfx and
                                cht_ref_itm = 0 and cht_tot_typ = ''T''  and ivs_Shpt_no = cht_ref_no             
     join ' + @Prefix + '_tctipd_rec on ivs_cmpy_id = ipd_cmpy_id 
     and ivs_shpt_pfx = ipd_ref_pfx and ivs_shpt_no = ipd_ref_no 
     join ' + @Prefix + '_cttcsi_rec on csi_cmpy_id = ipd_cmpy_id and csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm
     join ' + @Prefix + '_ivtivd_rec
     on ivd_cmpy_id = ipd_cmpy_id                 
     and ivd_shpt_pfx = ipd_ref_pfx and ivd_shpt_no = ipd_ref_no and ivd_shpt_itm = ipd_ref_itm
     join ' + @Prefix + '_ortchl_rec tot 
     on ivd_cmpy_id = tot.chl_cmpy_id                 
     and ivd_shpt_pfx = tot.chl_ref_pfx and ivd_shpt_no = tot.chl_ref_no and ivd_shpt_itm = tot.chl_ref_itm
     and tot.chl_chrg_cl = ''E'' 
     join ' + @Prefix + '_ortchl_rec mtl
     on ivd_cmpy_id = mtl.chl_cmpy_id                 
     and ivd_shpt_pfx = mtl.chl_ref_pfx and ivd_shpt_no = mtl.chl_ref_no and ivd_shpt_itm = mtl.chl_ref_itm
     and mtl.chl_chrg_cl = ''E'' and mtl.chl_chrg_no = 1
     join ' + @Prefix + '_arrcus_rec on cus_cmpy_id = ivs_cmpy_id  and cus_cus_id = ivs_Sld_cus_id 
     left join ' + @Prefix + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat
     left join ' + @Prefix + '_scrslp_rec isslp on isslp.slp_cmpy_id = ivd_cmpy_id and isslp.slp_slp = ivd_is_slp
     left join ' + @Prefix + '_scrslp_rec osslp on osslp.slp_cmpy_id = ivd_cmpy_id and osslp.slp_slp = ivd_os_slp 
      where ivs_sts_actn = ''A'' '
  if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @query += ' and  (ivs_Sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @query += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                   Set @query += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @query += ' and ipd_frm <> ''XXXX'' and (ipd_brh = '''+ @Branch +''' or ''' + @Branch +''' = '''')                   
      and ivh_inv_dt >= '''+ @FD +''' and ivh_inv_dt <= '''+ @TD +'''                    
      group by ivs_Sld_cus_id,cus_cus_long_nm,ivh_inv_dt, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ivd_cus_po, ipd_part, ivh_inv_due_dt,ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no),
      ivs_ord_no,ivs_transp_pfx + ''-'' + Convert(Varchar(15)
      ,ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm),cuc_desc30,ivd_shpg_whs, ipd_brh,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val,isslp.slp_lgn_id, osslp.slp_lgn_id '                            
   END                    
                  print(@query);                      
        EXECUTE sp_executesql @query;                      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
       END                       
    CLOSE ScopeCursor;                      
    DEALLOCATE ScopeCursor;                      
  END                      
  ELSE                      
    BEGIN                       
     SET @DatabaseName = (select DatabaseName from tbl_itech_DatabaseName_PS where Prefix =''+ @DB +'')                    
                            
     IF (UPPER(@DB) = 'TW')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                        
    End                        
    Else if (UPPER(@DB) = 'NO')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                        
    End                        
    Else if (UPPER(@DB) = 'CA')                        
    begin                        
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                        
    End                        
    Else if (UPPER(@DB) = 'CN')                        
    begin                     
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                        
    End                        
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                        
    begin                        
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                        
    End                        
    Else if(UPPER(@DB) = 'UK')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                        
    End                        
    Else if(UPPER(@DB) = 'DE')                        
    begin                        
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                        
    End                        
                        
    if @status = 'I'                    
      BEGIN                    
     SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo,OrdNo,PONumber,TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,    
     CstDesc  ,Cost ,CostUM,BasCryVal,SalesPresonIs,SalesPresonOs  )                      
       SELECT  stn_sld_cus_id as CustID , cus_cus_long_nm as CustName, stn_inv_Dt as InvDt, stn_lgth as Inches, '                      
       if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                      
       BEGIN                      
       SET @sqltxt = @sqltxt + ' stn_blg_wgt * 2.20462 as Weight, '                      
       END                      
       ELSE                      
       BEGIN                      
       SET @sqltxt = @sqltxt + ' stn_blg_wgt as Weight, '                      
       END                      
        SET @sqltxt = @sqltxt + ' (select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) 
        from ' + @DB + '_inrprm_rec where prm_frm = stn_frm and prm_grd = stn_grd and prm_size = stn_size and prm_fnsh = stn_fnsh) as Product,                        
       stn_frm as Form, stn_grd as Grade, stn_size  as Size ,stn_fnsh as Finish,stn_part as Part,  ivh_inv_due_dt as Date,stn_blg_MSR as Measure ,STN_TOT_VAL * '+ @CurrenyRate +',                      
        CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_npft_avg_val  * '+ @CurrenyRate +' END as ''NPAmt'',         
        (CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE (stn_npft_avg_val/STN_TOT_VAL)*100 end) as NPPct,                                         
       CASE WHEN (stn_tot_val) = 0 THEN 0 ELSE stn_mpft_avg_val  * '+ @CurrenyRate +' END as ''GPPct'',                      
       stn_blg_pcs,stn_upd_ref, convert(Varchar(10),stn_ord_no) + ''-'' + Convert(varchar(10),stn_ord_itm) + ''-'' + Convert(varchar(10),stn_ord_rls_no), stn_cus_po,stn_transp_pfx + ''-'' + Convert(Varchar(15),stn_transp_no) + ''-'' + Convert(Varchar(5),
		stn_shpt_itm)  , cuc_desc30, stn_shpg_whs , stn_shpt_brh,            
       csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val * '+ @CurrenyRate +',isslp.slp_lgn_id, osslp.slp_lgn_id
       FROM  ' + @DB + '_sahstn_rec 
       join ' + @DB + '_arrcus_rec on cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id
       left  join ' + @DB + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat
       left join ' + @DB + '_cttcsi_rec on  csi_cmpy_id = stn_cmpy_id and  csi_ref_pfx = stn_shpt_pfx and csi_ref_no = stn_shpt_no  and csi_ref_itm = stn_shpt_itm 
       left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = stn_cmpy_id and isslp.slp_slp = stn_is_slp     
       left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = stn_Cmpy_id and osslp.slp_slp = stn_os_slp  
       left join ' + @DB + '_ivtivh_rec on stn_cmpy_id = ivh_cmpy_id and stn_upd_ref = ivh_upd_ref 
       ,  ' + @DB + '_inrfrm_rec, ' + @DB + '_inrgrd_rec               
       where stn_inv_Dt >= '''+ @FD +''' and stn_inv_Dt <= '''+ @TD +''' and grd_grd = stn_grd and frm_frm = stn_frm                       
       and cus_cmpy_id = stn_Cmpy_id and cus_cus_id = stn_sld_cus_id'                      
     if @CusID Like '%''0''%'                      
      BEGIN                      
      --Set @sqltxt += ' and  (stn_sld_cus_id = '''+ @CusID +''' or '''+ @CusID +'''= '''')'                       
      Set @sqltxt += ' and  (stn_sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                      
       Else                      
        BEGIN                      
             Set @sqltxt += ' and  stn_sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @sqltxt += ' and stn_Frm <> ''XXXX'' and (stn_shpt_brh = '''+ @Branch +''' or ''' + @Branch +''' = '''') '                      
       END                    
       ELSE                    
       BEGIN --for Order information                    
        SET @sqltxt ='INSERT INTO #tmp ( CustID,CustName ,InvDt,Inches,Weight ,Product,Form,Grade,Size,Finish,Part,Date,Measure,TotalValue,NPAmt,NPPct,GPPct,NoOfPcs,InvNo, OrdNo, PONumber,TransportNo, MktCatg, ShpWhs, Branch,RefPfx, RefNo ,RefItm,CstNo,
     CstDesc ,Cost ,CostUM,BasCryVal ,SalesPresonIs,SalesPresonOs )                      
        select  ivs_Sld_cus_id, cus_cus_long_nm as CustName, ivh_inv_dt , ipd_lgth, '              
          -- Changed by mrinal on 19-05              
        print('MR :' + @DB)              
        if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')                      
     BEGIN                      
     SET @sqltxt = @sqltxt + ' sum(ivd_blg_wgt) * 2.20462, '                      
     END                      
       ELSE                      
     BEGIN                
                         
     SET @sqltxt = @sqltxt + ' sum(ivd_blg_wgt),'                      
     END              
                      
        SET @sqltxt = @sqltxt + '(select RTRIM(LTRIM(prm_frm)) + ''/'' + RTRIM(LTRIM(prm_grd)) + ''/'' + RTRIM(LTRIM(prm_size_desc)) + ''/'' + RTRIM(LTRIM(prm_fnsh)) 
        from ' + @DB + '_inrprm_rec where prm_frm = ipd_frm and prm_grd = ipd_grd and prm_size = ipd_size and prm_fnsh = ipd_fnsh) as Product,                    
     ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_part,ivh_inv_due_dt, sum(ivd_blg_msr), SUM(tot.chl_chrg_val) * '+ @CurrenyRate +' ,                     
          (SUM(cht_tot_val) - sum(oit_tot_avg_val)) * '+ @CurrenyRate +'  as NPAmt,                     
      case when sum(cht_tot_val) > 0                     
                       then                     
                                    (SUM(cht_tot_val) - sum(oit_tot_avg_val))/sum(cht_tot_val) * 100                    
                                    else                    
                                    0                    
 end                     
                              as NetPftVal,                    
                                                  
                              case when sum(cht_tot_val) > 0                     
                              then                     
                                    (SUM(cht_tot_val) - sum(oit_mtl_avg_val))/sum(cht_tot_val) * 100                    
                                    else                    
                                    0                    
                              end                     
                              as MPFTAvgVal,                     
     sum(ivd_blg_pcs),                    
     ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivs_ord_no, ivd_cus_po,ivs_transp_pfx + ''-'' + Convert(Varchar(15),ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm)                     
       , cuc_desc30, ivd_shpg_whs, ipd_brh,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val * '+ @CurrenyRate +',isslp.slp_lgn_id, osslp.slp_lgn_id
       from ' + @DB + '_ivtivs_rec             
       join  ' + @DB + '_ivtivh_rec on                     
       ivs_cmpy_id = ivh_cmpy_id and ivs_inv_pfx = ivh_inv_pfx and ivs_inv_no = ivh_inv_no                    
       join ' + @DB + '_ortoit_rec on oit_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = oit_ref_pfx and                    
                                oit_ref_itm = 0 and ivs_Shpt_no = oit_ref_no                    
                                 join ' + @DB + '_ortcht_rec on cht_cmpy_id = ivs_cmpy_id and ivs_Shpt_pfx = cht_ref_pfx and                    
    cht_ref_itm = 0 and cht_tot_typ = ''T''  and ivs_Shpt_no = cht_ref_no               
     join ' + @DB + '_tctipd_rec on ivs_cmpy_id = ipd_cmpy_id                    
     and ivs_shpt_pfx = ipd_ref_pfx and ivs_shpt_no = ipd_ref_no   
     join ' + @DB + '_cttcsi_rec on csi_cmpy_id = ipd_cmpy_id and csi_ref_pfx = ipd_ref_pfx and csi_ref_no = ipd_ref_no and csi_ref_itm = ipd_ref_itm                                                        
     join ' + @DB + '_ivtivd_rec                    
     on ivd_cmpy_id = ipd_cmpy_id                    
     and ivd_shpt_pfx = ipd_ref_pfx and ivd_shpt_no = ipd_ref_no and ivd_shpt_itm = ipd_ref_itm                     
     join ' + @DB + '_ortchl_rec tot                    
     on ivd_cmpy_id = tot.chl_cmpy_id                    
     and ivd_shpt_pfx = tot.chl_ref_pfx and ivd_shpt_no = tot.chl_ref_no and ivd_shpt_itm = tot.chl_ref_itm                     
     and tot.chl_chrg_cl = ''E''                    
     join ' + @DB + '_ortchl_rec mtl                    
     on ivd_cmpy_id = mtl.chl_cmpy_id                    
     and ivd_shpt_pfx = mtl.chl_ref_pfx and ivd_shpt_no = mtl.chl_ref_no and ivd_shpt_itm = mtl.chl_ref_itm                     
     and mtl.chl_chrg_cl = ''E'' and mtl.chl_chrg_no = 1                    
     join ' + @DB + '_arrcus_rec on cus_cmpy_id = ivs_cmpy_id  and cus_cus_id = ivs_Sld_cus_id                   
     left join ' + @DB + '_arrcuc_rec on cus_cus_cat = cuc_cus_cat  
     left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = ivd_cmpy_id and isslp.slp_slp = ivd_is_slp     
     left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = ivd_cmpy_id and osslp.slp_slp = ivd_os_slp                   
      where ivs_sts_actn = ''A'' '                    
                          
  if @CusID Like '%''0''%'                      
      BEGIN                      
      Set @sqltxt += ' and  (ivs_Sld_cus_id = '''' or ''''= '''')'                       
      END                      
     Else                      
      BEGIN                      
          if @CusID Like '%''1111111111''%' --- Eaton Group                      
        BEGIN                      
          Set @Value= (select dbo.fun_itech_funSub_ContactorID(@DB,'1111111111'))                      
          Set @CusID= @CusID +','+ @Value                      
          Set @sqltxt += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                       
        END                  
       Else                      
        BEGIN                      
             Set @sqltxt += ' and  ivs_Sld_cus_id IN ('+RTRIM(LTRIM(@CusID ))+')'                      
        END                      
      END                      
      Set @sqltxt += ' and  ipd_frm <> ''XXXX'' and (ipd_brh = '''+ @Branch +''' or ''' + @Branch +''' = '''')                    
      and ivh_inv_dt >= '''+ @FD +''' and ivh_inv_dt <= '''+ @TD +'''                    
      group by ivs_Sld_cus_id,cus_cus_long_nm,ivh_inv_dt, ipd_frm, ipd_grd, ipd_size, ipd_fnsh, ipd_wdth, ipd_lgth, ivd_cus_po, ipd_part, ivh_inv_due_dt,ivs_Shpt_pfx + ''-'' + Convert(Varchar(15),ivs_Shpt_no), ivs_ord_no,      
      ivs_transp_pfx + ''-'' + Convert(Varchar(15)              
      ,ivs_transp_no) + ''-'' + Convert(Varchar(5),ivs_opn_itm), cuc_desc30, ivd_shpg_whs, ipd_brh ,csi_ref_pfx, csi_ref_No, csi_ref_Itm, csi_cst_no, csi_cst_desc20, csi_cst* '+ @CurrenyRate +', csi_cst_um, csi_bas_cry_val ,isslp.slp_lgn_id, osslp.slp_lgn_id    '                    
                     
       END                     
                             
     print(@CustIDLength)                      
     print( @CusID)                      
     print(@sqltxt);                       
    set @execSQLtxt = @sqltxt;                       
   EXEC (@execSQLtxt);                      
     END                      
              
  if @IsExcInterco ='T'            
  BEGIN            
              
  SELECT * FROM #tmp where MktCatg != 'Interco' and ShpWhs not in ('SFS') AND Branch not in ('SFS') --and Form = 'TIRD' and Grade = '64'   
  --and size in ('5','2.5','.625') and Finish = 'US'  
   order by CustID                   
  END            
  Else            
  BEGIN            
  SELECT * FROM #tmp where  ShpWhs not in ('SFS') AND Branch not in ('SFS') --and RefNo = '204609' --and Form = 'TIRD' and Grade = '64'   
  order by CustID   -- MktCatg = 'Interco' and            
  END            
  DROP TABLE #tmp                      
                      
END                      
--@DBNAME varchar(50),@Branch varchar(50)='ALL',@CustomerID  varchar(Max),                
--@DateRange int,@CustmomerNoTxt varchar(Max), @version char = '0' , @status char = 'I' , @IncludeInterco char = '0'                        
 -- exec sp_itech_CostAnalysisByCustomerByProduct_NP '08/01/2017', '08/31/2017' ,'ALL','ALL','0','5','','0','I' ,'1'              
--  exec sp_itech_CostAnalysisByCustomerByProduct_NP '08/01/2017', '08/31/2017' ,'CA','ALL','0','5','','0','O'              
 -- exec sp_itech_CostAnalysisByCustomerByProduct_NP '01/04/2018', '03/05/2018' ,'ALL','ALL','0','5','','0','I'                      
--11930,12681,141,614,654,855,5567,6010,9309,12217,12535,12645,13219                      
--'11930','12681','141','614','654','855','5567','6010','9309','12217','12535','12645','13219'                  
--select * US_ortcht_rec                
                
--[sp_itech_CostAnalysisByCustomerByProduct_NP] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Branch varchar(50)='ALL',@CustomerID  varchar(Max),                
-- @DateRange int,@CustmomerNoTxt varchar(Max), @version char = '0' , @status char = 'I' , @IncludeInterco char = '0'                       
/*      
20170419    
Sub: please make this report Branch specific      
CHANGES:-        
20160525 :-        
 option out PSM and especially SFS when all databases is selected        
         
 SOLUTION:-         
 NitBranch not in ('SFS') and NitWhs not in ('SFS')        
     
 20170531    
 Mail sub: why is this report coming up blank when interco is set to true     
 
 20180305
 Mail sub:Report Modification Request
 
*/ 
GO
