USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_arbcub]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,Sumit>  
-- Create date: <Create Date,18/6/2021>  
-- Description: <Description,CustomerReport-PaymentTerm>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_arbcub]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
drop table dbo.US_arbcub_rec ;   
   
-- Insert statements for procedure here  
SELECT cub_cmpy_id,cub_cus_id,cub_apd_1,cub_apd_2,cub_apd_3,cub_apd_4,cub_apd_5,cub_apd_6,cub_apd_7,cub_apd_8,cub_apd_9,cub_apd_10,cub_apd_11,cub_apd_12 into US_arbcub_rec   
FROM [LIVEUSSTX].[liveusstxdb].[informix].[arbcub_rec];

END  
  
GO
