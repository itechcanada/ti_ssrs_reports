USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_glbacb]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,24/11/2015,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
create PROCEDURE [dbo].[UK_glbacb]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_glbacb_rec', 'U') IS NOT NULL  
  drop table dbo.UK_glbacb_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_glbacb_rec  
  from [LIVEUKGL].[liveukgldb].[informix].[glbacb_rec] ;  
    
END  
  
-- Select * from UK_glbacb_rec  
GO
