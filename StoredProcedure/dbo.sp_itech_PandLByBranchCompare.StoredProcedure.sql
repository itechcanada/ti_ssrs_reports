USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_PandLByBranchCompare]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Bablu >        
-- Create date: <11 NOV 2013>        
-- Description: <Getting top 50 customers for SSRS reports>        
-- Last changed date <18 Feb 2015>      
-- Last changed description : add the joining to get cus_adm_brh and apply filter     
-- Last change Date: 29 Jun 2015  
-- Last changes By: Bablu  
-- Last changes Desc: Remove the live connection of database   
-- Last change Date: 20 Jan 2016  
-- Last changes By: Bablu  
-- Last changes Desc: Include interco option in filter   
-- =============================================        
CREATE PROCEDURE [dbo].[sp_itech_PandLByBranchCompare]  @DBNAME varchar(50), @AcctPeriod varchar(6), @BranchName Varchar(30)        
        
AS        
BEGIN        
  
  
 SET NOCOUNT ON;        
declare @sqltxt1 varchar(8000)        
declare @execSQLtxt varchar(7000)        
declare @DB varchar(100)        
DECLARE @IsExcInterco char(1)  
DECLARE @StartAcctgPer varchar(6)
set @DB=  @DBNAME        

set  @StartAcctgPer = @AcctPeriod;
print @AcctPeriod;
print @AcctPeriod -1;

CREATE TABLE #tmp1 ( 
	tmpTblKey varchar(200),
    DBName   VARCHAR(20)   
  , Ctr   int  
  , Branch  varchar(50)  
  , GldAcctNo   VARCHAR(10)                                  
  , AcctTypeDesc   VARCHAR(50)     
  , AmountAcctPer1 Decimal(20,2)        
   , AmountAcctPer2 Decimal(20,2)
    , AmountAcctPer3 Decimal(20,2)  
	 , AmountAcctPer4 Decimal(20,2)  
	  , AmountAcctPer5 Decimal(20,2)  
	   , AmountAcctPer6 Decimal(20,2) 
  ); 


  --while loop six times

  insert into  #tmp1  (tmpTblKey)
  select 'US1'+ @BranchName + pl_cat as tmpTblKey
 from tbl_itech_chartofaccountlist where pl_cat='Revenue' 
  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey
 from tbl_itech_chartofaccountlist where  pl_cat='Eaton Subcontractor surcharges & tariffs' 
  
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey from tbl_itech_chartofaccountlist where  pl_cat='Cost of Sales' 

  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey
  from  
   tbl_itech_chartofaccountlist where  acct_cls='EXP' and pl_cat <> 'Depreciation'
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey  from  
   tbl_itech_chartofaccountlist where acct_cls='EXP' and pl_cat = 'Depreciation'

  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey
  from  
   tbl_itech_chartofaccountlist where acct_cls='OEXP' 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey from  
   tbl_itech_chartofaccountlist where acct_cls='OINC' ;


 
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer1 = TIAmountAcctPer1

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;



  -- Period  2
 set @AcctPeriod = @AcctPeriod -1;
  
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer2= TIAmountAcctPerAmout

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPerAmout,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;

  
  -- Period  3
 set @AcctPeriod = @AcctPeriod -1;
  
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer3= TIAmountAcctPerAmout

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPerAmout,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;

   -- Period  4
 set @AcctPeriod = @AcctPeriod -1;
  
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer4= TIAmountAcctPerAmout

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPerAmout,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;

   -- Period  5
 set @AcctPeriod = @AcctPeriod -1;
  
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer5= TIAmountAcctPerAmout

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPerAmout,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;

   -- Period  6
 set @AcctPeriod = @AcctPeriod -1;
  
  Update  #tmp1 
   set 
   #tmp1.tmpTblKey = TItmpTblKey,
   #tmp1.DBName=TIDBName,
   #tmp1.Ctr = TICtr,
   #tmp1.Branch = TIBranch,
   #tmp1.GldAcctNo = TIGldAcctNo,
   #tmp1.AcctTypeDesc  = TIAcctTypeDesc,
   #tmp1.AmountAcctPer6= TIAmountAcctPerAmout

   from(
   select 'US1'+ @BranchName + pl_cat as TItmpTblKey,  'US' as TIDBName, 1 as TICtr, @BranchName as TIBranch,'' as TIGldAcctNo, pl_cat as TIAcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as TIAmountAcctPerAmout,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Revenue' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct

  UNION  
  select 'US2'+ @BranchName + pl_cat as tmpTblKey,  'US', 2 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Eaton Subcontractor surcharges & tariffs' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
  select 'US3'+ @BranchName + pl_cat as tmpTblKey,  'US', 3 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
   from  [Stratix_US].[dbo].[US_glhgld_rec]
  join tbl_itech_chartofaccountlist on bsc_gl_acct = gld_bsc_gl_acct and pl_cat='Cost of Sales' 
  join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct
  UNION 
 select 'US4'+ @BranchName + pl_cat as tmpTblKey,  'US', 4 as Ctr, @BranchName as Branch,gld_bsc_gl_acct as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 
  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat <> 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_bsc_gl_acct 
UNION
 select 'US5'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 5 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6  from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='EXP' and pl_cat = 'Depreciation'
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
  UNION
  select 'US6'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 6 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt )  *-1 as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OEXP' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct 
UNION
    select 'US7'+ @BranchName + ''+ pl_cat as tmpTblKey,  'US', 7 as Ctr, @BranchName as Branch,'' as GldAcctNo, pl_cat as AcctTypeDesc , 
  sum(gld_cr_amt - gld_dr_amt ) as AmountAcctPer1,0 as AmountAcctPer2, 0 as AmountAcctPer3,0 as AmountAcctPer4,0 as AmountAcctPer5,0 as AmountAcctPer6 from  
   tbl_itech_chartofaccountlist left join [Stratix_US].[dbo].[US_glhgld_rec]
   on bsc_gl_acct = gld_bsc_gl_acct and acct_cls='OINC' 
  left join tbl_itech_US_SubAccounts on LEFT(gld_sacct,2) = subAccount    
  where gld_acctg_per = @AcctPeriod and subAccountDesc = @BranchName  
  group by  pl_cat, subAccountDesc, gld_sacct ) as TITables
  
  where  #tmp1.tmpTblKey = TITables.TItmpTblKey;


  --select Ctr, GldAcctNo, AcctTypeDesc, isnull(AmountAcctPer1,0), AmountAcctPer2,isnull(AmountAcctPer3,0),AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (1,2)
  --UNION
 -- select 1.0 as Ctr, 'GL AccountNo', 'Account Type', @StartAcctgPer, @StartAcctgPer-1,@StartAcctgPer-2, @StartAcctgPer-3, @StartAcctgPer-4, @StartAcctgPer-5
--  UNION
  select 1.1 as Ctr, '' as GldAcctNo, 'Revenue' as AcctTypeDesc, (select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer1, (select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer2,(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer3,(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer4,(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer5,(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) as AmountAcctPer6
  UNION
  select 1.2, '', '', 0,0,0,0,0,0 
  UNION
   select Ctr, GldAcctNo,'Total Cost of Sales' as AcctTypeDesc, AmountAcctPer1, AmountAcctPer2,AmountAcctPer3,AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (3)
  UNION
  select 3.1, '', '', Null,Null,Null,Null,Null,Null 
  UNION
  select 3.5,'', 'Gross Margin', (select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3)) , 
  (select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3)),
  (select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3)),
  (select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3)),
  (select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3)),
  (select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3))
   UNION
  select 3.6,'', 'Gross Margin %', (((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 , 
  (((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 ,
  (((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 ,
  (((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 ,
  (((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 ,
  (((select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3)))/(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100 
    UNION
    select 3.7, '', '', Null,Null,Null,Null,Null,Null
   UNION    
   select Ctr, GldAcctNo, AcctTypeDesc, AmountAcctPer1, AmountAcctPer2,AmountAcctPer3,AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (4)
   union
   select Ctr, GldAcctNo, AcctTypeDesc, AmountAcctPer1, AmountAcctPer2,AmountAcctPer3,AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (5)
   union
   select Ctr, GldAcctNo, AcctTypeDesc, AmountAcctPer1, AmountAcctPer2,AmountAcctPer3,AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (5)
   union
    select 5.1,'', 'Total Operating Expenses', (select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (4,5))  , 
  (select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (4,5)) ,
  (select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (4,5)) ,
  (select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (4,5)) ,
  (select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (4,5)) ,
  (select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (4,5)) 
  UNION
    select 5.2, '', '', Null,Null,Null,Null,Null,Null
   union 
     select 5.2,'', 'Operating Income', (((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (4,5))) , 
  (((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (4,5))) ,
  (((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (4,5))) ,
  (((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (4,5))) ,
  (((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (4,5))) ,
  (((select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3)))- (select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (4,5))) 
  union
   select 5.3,'', 'Operating Profit Margin %', ((((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) , 
  ((((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) ,
  ((((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) ,
  ((((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) ,
  ((((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) ,
  ((((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3))-(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (4,5)))/(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100) 
   UNION
    select 5.4, '', '', Null,Null,Null,Null,Null,Null
  union
   select Ctr, GldAcctNo, AcctTypeDesc, isnull(AmountAcctPer1,0), AmountAcctPer2,isnull(AmountAcctPer3,0),AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (6)
   UNION
    select 6.1, '', '', Null,Null,Null,Null,Null,Null
   union 
   select Ctr, GldAcctNo, 'Total Non Operating Income', isnull(AmountAcctPer1,0), AmountAcctPer2,isnull(AmountAcctPer3,0),AmountAcctPer4,AmountAcctPer5,AmountAcctPer6 from #tmp1 where Ctr in (7)
    UNION
    select 7.1, '', '', Null,Null,Null,Null,Null,Null
   union
    select 7.2,'', 'Net Income', ((((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (6))+(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (7))), 
  ((((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (6))+(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (7))),
  ((((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (6)) + (select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (7))),
  ((((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (6)) + (select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (7))),
  ((((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (6))+(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (7))),
  ((((select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (4,5)))-(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (6))+(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (7))) 
  union
    select 7.3,'', 'Net Income %', ((((((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer1) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer1)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100), 
  ((((((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer2) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer2)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100),
  ((((((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer3) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer3)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100),
  ((((((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer4) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer4)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100),
  ((((((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer5) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer5)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100),
  ((((((select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)) - (select  sum(AmountAcctPer6) from #tmp1 where #tmp1.Ctr in (3)))-(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (4,5)))-((select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (6,7))))/(select sum(t1.AmountAcctPer6)  from #tmp1 as t1 where t1.Ctr in (1,2)))*100)
     
  order by Ctr
    
END        
        
-- exec [sp_itech_PandLByBranchCompare]'US' , '202109','LAX'
-- select distinct gld_acctg_per from US_glhgld_rec order by gld_acctg_per desc
-- select subAccountDesc from tbl_itech_US_SubAccounts

GO
