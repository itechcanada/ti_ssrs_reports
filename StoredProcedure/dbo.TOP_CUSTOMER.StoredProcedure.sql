USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TOP_CUSTOMER]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <11/20/2012>
-- Description:	<Create Top customer by either dollars or weights>
-- =============================================
CREATE PROCEDURE [dbo].[TOP_CUSTOMER]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT a.cus_cus_long_nm, 'USA' as 'Country', a.cus_admin_brh, b.sat_inv_dt,b.sat_tot_val, b.sat_tot_val*1 as Conversion, b.sat_blg_wgt
FROM [US_arrcus_rec] a 
	INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE
a.cus_actv=1 AND
b.sat_inv_pfx='SE' AND
a.cus_admin_brh NOT IN ('CRP','TAI','ROC')
UNION
SELECT a.cus_cus_long_nm, 'UK' as 'Country', a.cus_admin_brh, b.sat_inv_dt,b.sat_tot_val, 
b.sat_tot_val*1.6  as Conversion,b.sat_blg_wgt
FROM [UK_arrcus_rec] a 
	INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE
a.cus_actv=1 AND
b.sat_inv_pfx='SE'
UNION
SELECT a.cus_cus_long_nm, 'CANADA' as 'Country', a.cus_admin_brh, b.sat_inv_dt,b.sat_tot_val,
b.sat_tot_val*1 as Conversion,b.sat_blg_wgt
FROM [CA_arrcus_rec] a 
	INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE
a.cus_actv=1 AND
b.sat_inv_pfx='SE'
UNION
SELECT a.cus_cus_long_nm, 'TAIWAN' as 'Country', a.cus_admin_brh, b.sat_inv_dt,b.sat_tot_val, 
b.sat_tot_val/33 as Conversion,b.sat_blg_wgt
FROM [TW_arrcus_rec] a 
	INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE
a.cus_actv=1 AND
b.sat_inv_pfx='SE'
UNION
SELECT a.cus_cus_long_nm, 'TAIWAN' as 'Country', a.cus_admin_brh, b.sat_inv_dt,b.sat_tot_val, 
b.sat_tot_val/33 as Conversion,b.sat_blg_wgt
FROM [NO_arrcus_rec] a 
	INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
WHERE
a.cus_actv=1 AND
b.sat_inv_pfx='SE'
END



GO
