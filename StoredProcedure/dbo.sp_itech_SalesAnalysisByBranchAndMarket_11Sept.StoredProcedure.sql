USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_SalesAnalysisByBranchAndMarket_11Sept]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Mayank >
-- Create date: <11 Feb 2013>
-- Description:	<Getting top 50 customers for SSRS reports>
-- Last updated Date 13 Apr 2015
-- Last updated decreption : Change the column name crx_xex_rt to crx_xexrt
-- Last updated by : Mukesh 
-- =============================================
CREATE PROCEDURE [dbo].[sp_itech_SalesAnalysisByBranchAndMarket_11Sept] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50),@Market as varchar(65)

AS
BEGIN
	
	SET NOCOUNT ON;
declare @sqltxt varchar(6000)
declare @execSQLtxt varchar(7000)
declare @DB varchar(100)
declare @FD varchar(10)
declare @TD varchar(10)
declare @NOOfCust varchar(15)
DECLARE @ExchangeRate varchar(15)

set @DB=  @DBNAME
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)

CREATE TABLE #tmp (   CustID		 VARCHAR(10)
   					, CustName 			 VARCHAR(65)
   					, Market	  VARCHAR(65) 
   					, Branch		 VARCHAR(65) 
   					, Form           Varchar(65)
   					, Grade           Varchar(65)
   					, TotWeight		DECIMAL(20, 2)
   					, WeightUM           Varchar(10)
                    , TotalValue  	 DECIMAL(20, 2)
   					, NetGP               DECIMAL(20, 2)
   					, TotalMatValue			 DECIMAL(20, 2)
   					, MatGP			 DECIMAL(20, 2)
   					, Databases	  VARCHAR(15) 
   					, MarketID	  integer
   					,stn_cry   varchar(5)
   	             );	

DECLARE @DatabaseName VARCHAR(35);
DECLARE @Prefix VARCHAR(35);
DECLARE @Name VARCHAR(15);
DECLARE @CurrenyRate varchar(15);

if @Market ='ALL'
 BEGIN
 set @Market = ''
 END

IF @DBNAME = 'ALL'
	BEGIN
		DECLARE ScopeCursor CURSOR FOR
			select DatabaseName,Name,Prefix from tbl_itech_DatabaseName
		  OPEN ScopeCursor;
				FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			WHILE @@FETCH_STATUS = 0
  			  BEGIN
  	  			DECLARE @query NVARCHAR(max);  	
  				SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'
  				
  				 IF (UPPER(@Prefix) = 'TW')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
								End
								Else if (UPPER(@Prefix) = 'NO')
								begin
									SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
								End
								Else if(UPPER(@Prefix) = 'UK')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
								End
								Else if(UPPER(@Prefix) = 'DE')
								begin
								   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
								End
  				
  				
  				if  (UPPER(@Prefix) = 'TW' OR UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
                BEGIN
							  SET @query ='INSERT INTO #tmp (CustID, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,
								STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,'
								if  (UPPER(@Prefix)= 'NO' OR  UPPER(@Prefix)= 'UK')			
							      SET @query = @query + '	SUM(stn_blg_wgt * 2.20462) as TotWeight,'
							      ELSE
							        SET @query = @query + '	SUM(stn_blg_wgt) as TotWeight,'
								
								SET @query = @query + ' stn_ord_wgt_um as WeightUM,
								Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, 
								Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as NetGP,
								 Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val * '+ @CurrenyRate +') else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
								 Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val * '+ @CurrenyRate +') else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
				END
						 Else
						BEGIN
								SET @query ='INSERT INTO #tmp (CustID, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,
								STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, 
								stn_ord_wgt_um as WeightUM,
								Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, 
								Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as NetGP,
								 Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
								 Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
  	  			EXECUTE sp_executesql @query;
  	  			
  	  			
  	  			print(@query)
  	  			FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;
  			  END 
		  CLOSE ScopeCursor;
		  DEALLOCATE ScopeCursor;
  END
  ELSE
     BEGIN 
			  Set @Name=(select Name from tbl_itech_DatabaseName where Prefix=''+ @DBNAME + '')
			
			 IF (UPPER(@DBNAME) = 'TW')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))
			End
			Else if (UPPER(@DBNAME) = 'NO')
			begin
				SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))
			End
			Else if(UPPER(@DBNAME) = 'UK')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))
			End
			Else if(UPPER(@DBNAME) = 'DE')
			begin
			   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))
			End
			  
			if  (UPPER(@DBNAME) = 'TW' OR UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
                BEGIN
							  SET @sqltxt ='INSERT INTO #tmp (CustID, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,
								STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,'
								if  (UPPER(@DBNAME)= 'NO' OR  UPPER(@DBNAME)= 'UK')			
							      SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt * 2.20462) as TotWeight,'
							      ELSE
							        SET @sqltxt = @sqltxt + '	SUM(stn_blg_wgt) as TotWeight,' 
								
							SET @sqltxt = @sqltxt + '	stn_ord_wgt_um as WeightUM,
								Case when (stn_cry = ''USD'') then SUM(stn_tot_val * '+ @CurrenyRate +') else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, 
								Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val * '+ @CurrenyRate +') else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as NetGP,
								 Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val * '+ @CurrenyRate +') else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
								 Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val * '+ @CurrenyRate +') else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
				END
						 Else
						BEGIN
								SET @sqltxt ='INSERT INTO #tmp (CustID, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry) 				
  								select stn_sld_cus_id as CustID, cus_cus_nm as CustName, cuc_desc30 as Market,
								STN_SHPT_BRH as Branch, stn_frm as Form, stn_grd as Grade,
								SUM(stn_blg_wgt) as TotWeight, stn_ord_wgt_um as WeightUM,
								Case when (stn_cry = ''USD'') then SUM(stn_tot_val) else SUM(stn_tot_val * ISNULL(crx_xexrt, 1)) end  as TotalValue, 
								Case when (stn_cry = ''USD'') then SUM(stn_npft_avg_val) else SUM(stn_npft_avg_val * ISNULL(crx_xexrt, 1)) end  as NetGP,
								 Case when (stn_cry = ''USD'') then SUM(stn_tot_mtl_val) else  SUM(stn_tot_mtl_val * ISNULL(crx_xexrt, 1)) end as TotalMatValue , 
								 Case when (stn_cry = ''USD'') then SUM(stn_mpft_avg_val) else  SUM(stn_mpft_avg_val * ISNULL(crx_xexrt, 1)) end as MatGP,
								  cuc_cus_cat as MarketID, '''+ @Name +''' as databases,stn_cry
								 from ' + @DB + '_sahstn_rec 
								 join ' + @DB + '_arrcus_rec on cus_cus_id = stn_sld_cus_id 
								 left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
								 Left Join  '+ @DB +'_scrcrx_rec on stn_cry= crx_orig_cry and crx_eqv_cry = ''USD'' 
								 where stn_inv_Dt >='''+ @FD +''' and stn_inv_dt <='''+ @TD +'''
								  and stn_frm <> ''XXXX''  and (cuc_desc30 = '''+ @Market +''' or '''+ @Market +'''= '''')
								  group by  stn_sld_cus_id, cus_cus_nm, cuc_desc30, STN_SHPT_BRH, stn_frm, stn_grd,
								 stn_ord_wgt_um ,cuc_cus_cat,stn_cry,crx_xexrt
								 order by stn_sld_cus_id '
						End
						print(@sqltxt)
				set @execSQLtxt = @sqltxt; 
			EXEC (@execSQLtxt);
     END
     
   SELECT *,  SUM(NetGP)/nullif(SUM(TotalValue), 0) * 100 as 'GP%' FROM #tmp 
   group by CustID, CustName,Market,Branch,Form,Grade,TotWeight,WeightUM,TotalValue,NetGP,TotalMatValue,MatGP,MarketID,Databases,stn_cry
   order by CustID 
   
END

-- exec sp_itech_SalesAnalysisByBranchAndMarket_11Sept '01/01/2013', '8/28/2013' , 'TW','ALL'
-- exec sp_itech_SalesAnalysisByBranchAndMarket '12/14/2012', '2/13/2013' , 'US','Aerospace '




GO
