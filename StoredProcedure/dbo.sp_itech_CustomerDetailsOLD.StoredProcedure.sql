USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerDetailsOLD]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <14 Dec 2016>    
-- =============================================    
  
CREATE PROCEDURE [dbo].[sp_itech_CustomerDetailsOLD] @DBNAME varchar(50), @Branch varchar(15),
 @version char = '0' , @IncludeInterco char = '0' ,  @IncludeShipTo char = '0'   
    
AS    
BEGIN    


     
 SET NOCOUNT ON;    
declare @sqltxt varchar(max)    
declare @execSQLtxt varchar(max)    
declare @DB varchar(100)   
declare @LTD varchar(10)   
declare @D3MFD varchar(10)    
declare @D3MTD varchar(10)   
    
set @DB=  @DBNAME     
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-18,0)), 120)  -- Lost Account  
-- set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-4,0)) , 120)   -- Dormant 3 Month    
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)   -- Last date of previous month 
 
    
CREATE TABLE #tmp (    CustID   VARCHAR(100)    
        , CustName     VARCHAR(100)    
        , Market   VARCHAR(65)     
        , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,ContactFirstNM  Varchar(100)    
        ,ContactLastNM  Varchar(100)    
        ,CustEmail Varchar(100)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)   
        ,LastSaleDT varchar(10)   
        ,SlsPrsLngOS varchar(35) 
        ,CusType Varchar(1)         
        ,LostCustomer Varchar(1) 
        ,ShipTo		Varchar(5)
        ,Active		varchar (5)
                   );     
  
  
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
DECLARE @Name VARCHAR(15);    
    
--if @Market ='ALL'    
-- BEGIN    
-- set @Market = ''    
-- END    
    
if @Branch ='ALL'    
 BEGIN    
 set @Branch = ''    
 END    
    
DECLARE @CurrenyRate varchar(15);    
    
    
    
IF @DBNAME = 'ALL'    
 BEGIN    
  IF @version = '0'      
   BEGIN      
   DECLARE ScopeCursor CURSOR FOR    
    select DatabaseName, company,prefix from tbl_itech_DatabaseName     
     OPEN ScopeCursor;     
   END      
   ELSE      
   BEGIN     
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
    OPEN ScopeCursor;    
  End    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
      SET @DB= @Prefix    
     -- print(@DB)    
         
     IF (UPPER(@DB) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DB) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DB) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DB) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DB) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DB) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DB) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
         
         
        DECLARE @query NVARCHAR(max);    
             
        SET @query = 'INSERT INTO #tmp (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,ShipTo,Active)  
      select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail 
                 , shp_is_slp, shp_os_slp ,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
                 ,(select b.usr_nm from ' + @DB + '_scrslp_rec a join ' + @DB + '_mxrusr_rec b on a.slp_lgn_id = b.usr_lgn_id and a.slp_slp = shp_os_slp)  
                 , cus_cus_acct_typ, 
                 (case when (select count(*) from ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id and  coc_lst_sls_dt <= '''+@LTD+''' ) > 0 then ''L'' else '''' end ) as lostcustomer, 
                 shp_shp_to,cus_actv
            from ' + @DB + '_arrcus_rec     
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
            left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id  and shp_cus_id = cus_cus_id    
 '    
           
     print(@query)      
        EXECUTE sp_executesql @query;  
 
         
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
       print 'starting' ;    
      IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
     print 'Ending else';    
     print @CurrenyRate ;    
                            
      
         
     SET @sqltxt = 'INSERT INTO #tmp (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer,ShipTo,Active)  
     select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail 
                 , shp_is_slp, shp_os_slp ,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
                 ,(select b.usr_nm from ' + @DB + '_scrslp_rec a join ' + @DB + '_mxrusr_rec b on a.slp_lgn_id = b.usr_lgn_id and a.slp_slp = shp_os_slp)  
                 , cus_cus_acct_typ, 
                 (case when (select count(*) from ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id and  coc_lst_sls_dt <= '''+@LTD+''' ) > 0 then ''L'' else '''' end ) as lostcustomer, 
                 shp_shp_to,cus_actv
            from ' + @DB + '_arrcus_rec     
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
            left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id  and shp_cus_id = cus_cus_id    
'  
           
  print(@sqltxt)    
  set @execSQLtxt = @sqltxt;     
  EXEC (@execSQLtxt);    
    
  
  
     END    
     
if @IncludeShipTo = '1'
begin     
 
 if @IncludeInterco = '1'  
 begin  
  Select Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType ,
  LostCustomer,ShipTo, (Case  when Active = '1' then 'A' else 'I' end) as Active
   from #tmp where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ') order by CusType;
  End  
  Else  
  Begin  
  Select Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType,
  LostCustomer,ShipTo,(Case  when Active = '1' then 'A' else 'I' end) as Active from #tmp where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ')  
  and Market  <> 'Interco'  order by CusType;
  End  
End
Else
Begin
if @IncludeInterco = '1'  
 begin  
  Select Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType ,
  LostCustomer,ShipTo, (Case  when Active = '1' then 'A' else 'I' end) as Active
   from #tmp where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ') and (ShipTo = '0' or ShipTo = null) order by CusType;
  End  
  Else  
  Begin  
  Select Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,
  LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType,
  LostCustomer,ShipTo,
  (Case  when Active = '1' then 'A' else 'I' end) as Active from #tmp where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ')  
  and (isnull(Market,'')  <> 'Interco' )  and (ShipTo = '0' or ShipTo = null) 
  order by Market;
  End  
End    
    
   drop table #tmp ;  
END    
    
-- exec [sp_itech_CustomerDetails] 'ALL', 'ALL'   
-- exec [sp_itech_CustomerDetailsOLD] 'US', 'ALL','1','0', '0'
GO
