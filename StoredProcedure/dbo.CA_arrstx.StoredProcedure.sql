USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_arrstx]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,July 10, 2014,>
-- Description:	<Description,Sales Territory,>

-- =============================================
Create PROCEDURE [dbo].[CA_arrstx]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.CA_arrstx_rec', 'U') IS NOT NULL
		drop table dbo.CA_arrstx_rec ;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.CA_arrstx_rec
  from [LIVECASTX].[livecastxdb].[informix].[arrstx_rec] ;
  
END
GO
