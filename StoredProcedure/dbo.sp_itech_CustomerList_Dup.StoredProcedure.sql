USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_CustomerList_Dup]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh >    
-- Create date: <19 Apr 2017>    
-- Description: <Getting top 40 Propects customers for SSRS reports>    
-- =============================================    
  
CREATE PROCEDURE [dbo].[sp_itech_CustomerList_Dup] @DBNAME varchar(50), @Branch varchar(15), @version char = '0' , @IncludeInterco char = '0'    
    
AS    
BEGIN    


     
 SET NOCOUNT ON;    
declare @sqltxt varchar(max)    
declare @execSQLtxt varchar(max)    
declare @DB varchar(100)   
declare @LTD varchar(10)   
declare @D3MFD varchar(10)    
declare @D3MTD varchar(10)   
    
set @DB=  @DBNAME     
set @LTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-18,0)), 120)  -- Lost Account  
-- set @D3MFD = CONVERT(VARCHAR(10), DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,GETDATE())-4,0)) , 120)   -- Dormant 3 Month    
set @D3MTD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)   -- Last date of previous month 
 
 CREATE TABLE #tmp (    CustID   VARCHAR(100)    
        , CustName     VARCHAR(100)    
        , Market   VARCHAR(65)     
        , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,ContactFirstNM  Varchar(100)    
        ,ContactLastNM  Varchar(100)    
        ,CustEmail Varchar(100)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)   
        ,LastSaleDT varchar(10)   
        ,SlsPrsLngOS varchar(35) 
        ,CusType Varchar(1)         
        ,LostCustomer Varchar(1)  
        ,cusAddress Varchar(150)
        ,cusPCD varchar(10)
        ,cusCity Varchar(35)
        ,CusState Varchar(3)
        ,CusPhone Varchar(30)
                   );   
    
CREATE TABLE #tmp1 (    CustID   VARCHAR(100)    
        , CustName     VARCHAR(100)    
        , Market   VARCHAR(65)     
        , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,ContactFirstNM  Varchar(100)    
        ,ContactLastNM  Varchar(100)    
        ,CustEmail Varchar(100)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)   
        ,LastSaleDT varchar(10)   
        ,SlsPrsLngOS varchar(35) 
        ,CusType Varchar(1)         
        ,LostCustomer Varchar(1)  
        ,cusAddress Varchar(150)
        ,cusPCD varchar(10)
        ,cusCity Varchar(35)
        ,CusState Varchar(3)
        ,CusPhone Varchar(30)
                   );     
  CREATE TABLE #tmp2 (    CustID   VARCHAR(100)    
        , CustName     VARCHAR(100)    
        , Market   VARCHAR(65)     
        , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,ContactFirstNM  Varchar(100)    
        ,ContactLastNM  Varchar(100)    
        ,CustEmail Varchar(100)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)   
        ,LastSaleDT varchar(10)   
        ,SlsPrsLngOS varchar(35)  
        ,CusType Varchar(1) 
        ,LostCustomer Varchar(1) 
         ,cusAddress Varchar(150)
        ,cusPCD varchar(10)
        ,cusCity Varchar(35)
        ,CusState Varchar(3)   
        ,CusPhone Varchar(30)          
                   );     
CREATE TABLE #tmp3 (    CustID   VARCHAR(100)    
        , CustName     VARCHAR(100)    
        , Market   VARCHAR(65)     
        , Branch   VARCHAR(3)     
        , Databases   VARCHAR(2)    
        ,ContactFirstNM  Varchar(100)    
        ,ContactLastNM  Varchar(100)    
        ,CustEmail Varchar(100)   
        ,SalesPresonIs varchar(10)  
        ,SalesPresonOs varchar(10)   
        ,LastSaleDT varchar(10)   
        ,SlsPrsLngOS varchar(35) 
        ,CusType Varchar(1)  
        ,LostCustomer Varchar(1)  
         ,cusAddress Varchar(150)
        ,cusPCD varchar(10)
        ,cusCity Varchar(35)
        ,CusState Varchar(3)  
        ,CusPhone Varchar(30)          
                   );     
   
  
DECLARE @DatabaseName VARCHAR(35);    
DECLARE @Prefix VARCHAR(5);    
DECLARE @Name VARCHAR(15);    
    
--if @Market ='ALL'    
-- BEGIN    
-- set @Market = ''    
-- END    
    
if @Branch ='ALL'    
 BEGIN    
 set @Branch = ''    
 END    
    
DECLARE @CurrenyRate varchar(15);    
    
    
    
IF @DBNAME = 'ALL'    
 BEGIN    
  IF @version = '0'      
   BEGIN      
   DECLARE ScopeCursor CURSOR FOR    
    select DatabaseName, company,prefix from tbl_itech_DatabaseName     
     OPEN ScopeCursor;     
   END      
   ELSE      
   BEGIN     
  DECLARE ScopeCursor CURSOR FOR    
   select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_ALL     
    OPEN ScopeCursor;    
  End    
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
     WHILE @@FETCH_STATUS = 0    
       BEGIN    
      SET @DB= @Prefix    
     -- print(@DB)    
         
     IF (UPPER(@DB) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DB) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DB) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DB) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DB) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DB) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DB) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
         
         
        DECLARE @query NVARCHAR(max);    
             
        SET @query = 'INSERT INTO #tmp2 (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone )  
     select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail 
                ,isslp.slp_lgn_id, osslp.slp_lgn_id,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
                 ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )  
                 , cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
                 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
            from ' + @DB + '_arrcus_rec 
            left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''     
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
            left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id 
            left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
			left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp                 
            where cus_cus_acct_typ = ''P''  and cus_actv = 1 
 union             
select ''' + @DB + ''',  cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail ,isslp.slp_lgn_id, osslp.slp_lgn_id ,  
 ( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
 ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
 ,cus_cus_acct_typ  , '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
from ' + @DB + '_arrcus_rec    
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L'' 
join ' + @DB + '_scrcvt_rec on cus_cmpy_id = cvt_cmpy_id and cus_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = cus_cus_acct_typ and (cvt_email Is not null and LTrim(cvt_email) <> '''')  
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id 
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp               
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH,isslp.slp_lgn_id, osslp.slp_lgn_id,cus_cus_acct_typ , LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')'  
print(@query)      
        EXECUTE sp_executesql @query;   
  
SET @query = 'INSERT INTO #tmp3 (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone )
select ''' + @DB + ''', cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,
(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)
as firstNM,
(select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
as LastNM,
IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
order by cvt_ref_pfx desc, cvt_cntc_no), '''') as CustEmail ,isslp.slp_lgn_id, osslp.slp_lgn_id ,
( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )
,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )
,cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
from ' + @DB + '_arrcus_rec
join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_cus_ven_id = cus_cus_id and cva_cus_ven_typ = cus_cus_acct_typ
and (cva_email Is not null and LTrim(cva_email) <> '''')
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
Union
SELECT  ''' + @DB + ''', CUS_CUS_ID as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,
(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' order by cvt_ref_pfx  desc, cvt_cntc_no)
as firstNM,
(select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
as LastNM,
IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  order by cvt_ref_pfx
desc, cvt_cntc_no), '''') as CustEmail
,isslp.slp_lgn_id, osslp.slp_lgn_id , coc_lst_sls_dt ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
,cus_cus_acct_typ, ''L'' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
FROM ' + @DB + '_arbcoc_rec
INNER JOIN ' + @DB + '_arrcus_rec ON coc_cus_id = CUS_CUS_ID
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
and cva_addr_no = 0 and cva_addr_typ = ''L''
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = coc_cmpy_id and shp_shp_to = 0 and shp_cus_id = coc_cus_id
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp
where coc_lst_sls_dt <= '''+@LTD+'''
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt ,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
Union  
SELECT  ''' + @DB + ''', CUS_CUS_ID as CustID,  CUS_CUS_LONG_NM as CustName,cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,     
                 (select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  order by cvt_ref_pfx
  desc, cvt_cntc_no), '''') as CustEmail   
                ,isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
                ,cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
                REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
               FROM ' + @DB + '_sahstn_rec    
           INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID  
         left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''   
           Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
           left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id    
           left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id 
           left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp   
           where  STN_INV_DT >   '''+ @LTD +'''  and STN_INV_DT <= ''' + @D3MTD +''' 
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
           REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')  '    
           
     print(@query)      
        EXECUTE sp_executesql @query;  
          
insert into #tmp1           
select * from #tmp2  
Union   
select * from #tmp3;  
  
Truncate table #tmp2;  
Truncate table #tmp3;    
         
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;    
       END     
    CLOSE ScopeCursor;    
    DEALLOCATE ScopeCursor;    
  END    
  ELSE    
     BEGIN     
       print 'starting' ;    
      IF (UPPER(@DBNAME) = 'TW')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
    Else if (UPPER(@DBNAME) = 'NO')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))    
    End    
    Else if (UPPER(@DBNAME) = 'CA')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))    
    End    
    Else if (UPPER(@DBNAME) = 'CN')    
    begin    
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))    
    End    
    Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')    
    begin    
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))    
    End    
    Else if(UPPER(@DBNAME) = 'UK')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))    
    End    
    Else if(UPPER(@DBNAME) = 'DE')    
    begin    
       SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))    
    End    
    Else if(UPPER(@DBNAME) = 'TWCN')    
    begin    
       SET @DB ='TW'    
       SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))    
    End    
     print 'Ending else';    
     print @CurrenyRate ;    
                            
      
         
     SET @sqltxt = 'INSERT INTO #tmp2 (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone )  
     select  ''' + @DB + ''',cus_cus_id as CustID, cus_cus_long_nm as CustName, cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail 
                ,isslp.slp_lgn_id, osslp.slp_lgn_id ,( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
                ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )   
                 , cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
                 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
            from ' + @DB + '_arrcus_rec 
          left  join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''     
            left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
            left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id 
            left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp   
            where cus_cus_acct_typ = ''P''  and cus_actv = 1 
 union             
select ''' + @DB + ''',  cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  
                 order by cvt_ref_pfx  desc, cvt_cntc_no), '''') as CustEmail,isslp.slp_lgn_id, osslp.slp_lgn_id ,  
 ( select coc_lst_sls_dt from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
 ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
 ,cus_cus_acct_typ  , '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
from ' + @DB + '_arrcus_rec  
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''   
join ' + @DB + '_scrcvt_rec on cus_cmpy_id = cvt_cmpy_id and cus_cus_id = cvt_cus_ven_id and cvt_cus_ven_typ = cus_cus_acct_typ and (cvt_email Is not null and LTrim(cvt_email) <> '''')  
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat  
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id  
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH,isslp.slp_lgn_id, osslp.slp_lgn_id,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''') '  
print(@sqltxt)    
  set @execSQLtxt = @sqltxt;     
  EXEC (@execSQLtxt);  
SET @sqltxt = 'INSERT INTO #tmp3 (Databases,CustID, CustName,Market,Branch, ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,CusType,LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone )
select ''' + @DB + ''', cus_cus_id as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,
(select top 1 ISNULL(rtrim(cvt_frst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)
as firstNM,(select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')
from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)
as LastNM,IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  order by cvt_ref_pfx
desc, cvt_cntc_no), '''') as CustEmail  ,isslp.slp_lgn_id, osslp.slp_lgn_id ,( select ISNULL(coc_lst_sls_dt,'' '') from  ' + @DB + '_arbcoc_rec where coc_cus_id = cus_cus_id )  
,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id )
,cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
from ' + @DB + '_arrcus_rec
join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and cva_cus_ven_id = cus_cus_id and cva_cus_ven_typ = cus_cus_acct_typ
and (cva_email Is not null and LTrim(cva_email) <> '''')
left join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = cus_cmpy_id and shp_shp_to = 0 and shp_cus_id = cus_cus_id 
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp 
group by  cus_cus_id,cus_cus_long_nm,cuc_desc30,CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
Union
SELECT  ''' + @DB + ''', CUS_CUS_ID as CustID,cus_cus_long_nm as CustName, cuc_desc30 as Market, CUS_ADMIN_BRH as Branch,
 (select top 1 ISNULL(rtrim(cvt_frst_nm),'''')
 from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' order by cvt_ref_pfx  desc, cvt_cntc_no)
 as firstNM,
 (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')
 from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)
 as LastNM,
 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  order by cvt_ref_pfx
 desc, cvt_cntc_no), '''') as CustEmail
 ,isslp.slp_lgn_id, osslp.slp_lgn_id, coc_lst_sls_dt ,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
 ,cus_cus_acct_typ, ''L'' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
 REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
FROM ' + @DB + '_arbcoc_rec 
INNER JOIN ' + @DB + '_arrcus_rec ON coc_cus_id = CUS_CUS_ID  
left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''
Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat 
left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = coc_cmpy_id and shp_shp_to = 0 and shp_cus_id = coc_cus_id 
left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp  
 where coc_lst_sls_dt <= '''+@LTD+''' 
group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt ,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)) ,
REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
Union  
SELECT  ''' + @DB + ''', CUS_CUS_ID as CustID,  CUS_CUS_LONG_NM as CustName,cuc_desc30 as Market,CUS_ADMIN_BRH as Branch,     
                 (select top 1 ISNULL(rtrim(cvt_frst_nm),'''')     
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> ''''  order by cvt_ref_pfx  desc, cvt_cntc_no)   
            as firstNM,  
            (select top 1 ISNULL(Rtrim(cvt_lst_nm),'''')      
            from ' + @DB + '_scrcvt_rec where  cvt_cus_ven_id = cus_cus_id and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' order by cvt_ref_pfx  desc, cvt_cntc_no)  
            as LastNM,    
                 IsNull((Select TOP 1 cvt_email from ' + @DB + '_scrcvt_rec where cvt_email <> '''' and cvt_lst_nm is not null and cvt_frst_nm is not null and cvt_lst_nm <> '''' and cvt_frst_nm <> '''' and cvt_cus_ven_id = cus_cus_id  order by cvt_ref_pfx
  desc, cvt_cntc_no), '''') as CustEmail   
                ,isslp.slp_lgn_id, osslp.slp_lgn_id ,coc_lst_sls_dt,(select b.usr_nm from ' + @DB + '_mxrusr_rec b where osslp.slp_lgn_id = b.usr_lgn_id ) 
                ,cus_cus_acct_typ, '''' as lostcustomer, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
                REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')
               FROM ' + @DB + '_sahstn_rec    
           INNER JOIN ' + @DB + '_arrcus_rec ON STN_SLD_CUS_ID = CUS_CUS_ID 
           left join ' + @DB + '_scrcva_rec on cva_cmpy_id = cus_cmpy_id and  cva_cus_ven_id = cus_cus_id and cva_ref_pfx = ''CU'' and cva_cus_ven_typ = ''C'' 
           and cva_addr_no = 0 and cva_addr_typ = ''L''   
           Left Join ' + @DB + '_arrcuc_rec on cuc_cus_cat = cus_cus_cat    
           left join ' + @DB + '_arbcoc_rec on coc_cmpy_id = cus_cmpy_id and coc_cus_id = cus_cus_id    
           left JOIN ' + @DB + '_arrshp_rec  on shp_cmpy_id = stn_cmpy_id and shp_shp_to = 0 and shp_cus_id = stn_sld_cus_id 
           left join ' + @DB + '_scrslp_rec isslp on isslp.slp_cmpy_id = shp_cmpy_id and isslp.slp_slp = shp_is_slp 
left join ' + @DB + '_scrslp_rec osslp on osslp.slp_cmpy_id = shp_cmpy_id and osslp.slp_slp = shp_os_slp   
           where  STN_INV_DT >   '''+ @LTD +'''  and STN_INV_DT <= ''' + @D3MTD +'''    
           group by CUS_CUS_ID,  CUS_CUS_LONG_NM,cuc_desc30, CUS_ADMIN_BRH ,isslp.slp_lgn_id, osslp.slp_lgn_id,coc_lst_sls_dt,cus_cus_acct_typ, LTRIM(RTRIM(cva_addr1)) + '' '' + LTRIM(RTRIM(cva_addr2)) + '' '' + LTRIM(RTRIM(cva_addr3)),cva_pcd,Replace(LTRIM(RTRIM(cva_city)),''-'',''''),LTRIM(RTRIM(cva_st_prov)),
           REPLACE(REPLACE(REPLACE(REPLACE(Rtrim(ltrim(cva_tel_area_cd)) + Rtrim(ltrim(cva_tel_no)),''-'',''''),'','',''''),''.'',''''),'' '','''')   
'  
           
  print(@sqltxt)    
  set @execSQLtxt = @sqltxt;     
  EXEC (@execSQLtxt);    
    
  insert into #tmp1           
select * from #tmp2  
Union   
select * from #tmp3;  
  
Truncate table #tmp2;  
Truncate table #tmp3;   
     END    
     
     
 
 if @IncludeInterco = '1'  
 begin  
 insert into #tmp
  Select CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType ,
  LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone 
   from #tmp1 
   
   where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ') ;-- order by CusType;
  End  
  Else  
  Begin  
  insert into #tmp
  Select CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,SlsPrsLngOS,
  (CAse when LostCustomer = 'L' then 'L'  when CusType = 'P' then 'P' else 'C'  end ) as CusType,
  LostCustomer, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp1 where (Branch = ''+@Branch +'' OR  ''+@Branch +'' = ' ')  
  and Market  <> 'Interco'  ;-- order by CusType;
  End  
  
  --select Count(*) from #tmp;
  
  select custDetail.* into #tmpfinalresult from 
  (
    SElect count(CusPhone) as countPhone ,CusPhone  from #tmp where   CusPhone != '' group by CusPhone having count(CusPhone) >1 -- cusCity !='' and cusAddress !='' and cusPCD !='' and
  ) as duplicatphone 
  join (
  
   SElect  CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,
   SlsPrsLngOS,CusType, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp 
  ) as custDetail on custDetail.CusPhone = duplicatphone.CusPhone 
  order by custDetail.CusPhone;
  
 insert into #tmpfinalresult
 select custDetail.*   from 
  (
    SElect count(CustName) as countCustName ,CustName  from #tmp where   CustName !=''  group by CustName having count(CustName) >1 
  ) as duplicatCustName
  join (
  
   SElect  CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,
   SlsPrsLngOS,CusType, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp 
   where CustID not in (select CustID from  #tmpfinalresult  ) 
  ) as custDetail on custDetail.CustName = duplicatCustName.CustName order by custDetail.CustName;
  
  
  --insert into #tmpfinalresult
  --select custDetail.*   from 
  --(
  --  SElect count(cusPCD) as countPCD ,cusPCD  from #tmp where   cusPCD !=''  group by cusPCD having count(cusPCD) >1 -- cusCity !='' and cusAddress !='' and
  --) as duplicatzipcode 
  --join (
  
  -- SElect  CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,
  -- SlsPrsLngOS,CusType, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp 
  -- where CustID not in (select CustID from  #tmpfinalresult  ) 
  --) as custDetail on custDetail.cusPCD = duplicatzipcode.cusPCD order by custDetail.cusPCD;
  
  
   insert into #tmpfinalresult
  select custDetail.*  from 
  (
    SElect count(cusAddress) as countAddress ,cusAddress  from #tmp where  cusAddress not in ('','~')  group by cusAddress having count(cusAddress) >1
  ) as duplicatAddress 
  join (
  
   SElect  CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,
   SlsPrsLngOS,CusType, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp 
     where CustID not in (select CustID from  #tmpfinalresult  )
  ) as custDetail on custDetail.cusAddress = duplicatAddress.cusAddress  order by custDetail.cusAddress;
  
  select * from #tmpfinalresult;
  
  drop table #tmpfinalresult;
   drop table #tmp; 
   drop table #tmp1 ;  
   drop table #tmp2 ;  
   drop table #tmp3 ;   
END    
    
-- exec [sp_itech_CustomerList_Dup] 'ALL', 'ALL'   -- 271  
-- exec [sp_itech_CustomerList_Dup] 'US', 'ALL','0','1' -- 6242 --  

/*
insert into #tmpfinalresult
  select custDetail.* from 
  (
    SElect count(cusCity) as countCity ,cusCity  from #tmp where  cusCity !=''  group by cusCity having count(cusCity) >1 -- and cusPCD is not null and cusAddress !='' and cusPCD !=''
  ) as duplicatCity 
  join (
  
   SElect  CustID, CustName,Market,Branch, Databases,ContactFirstNM, ContactLastNM,CustEmail,SalesPresonIs,SalesPresonOs,LastSaleDT,
   SlsPrsLngOS,CusType, cusAddress ,cusPCD,cusCity ,CusState, CusPhone  from #tmp 
    where CustID not in (select CustID from  #tmpfinalresult  ) 
  ) as custDetail on custDetail.cusCity = duplicatCity.cusCity order by custDetail.cusCity;
*/ 

/*
Date 20171102
Mail sub:Active Account Base Report Alignment


*/
GO
