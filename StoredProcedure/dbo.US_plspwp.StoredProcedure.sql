USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_plspwp]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Sep 24, 2014,>  
-- Description: <Description,  >  
  
-- =============================================  
Create PROCEDURE [dbo].[US_plspwp]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.US_plspwp_rec', 'U') IS NOT NULL  
  drop table dbo.US_plspwp_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.US_plspwp_rec  
  from [LIVEUS_IW].[liveusstxdb_iw].[informix].[plspwp_rec] ;   
    
END  
-- select * from US_plspwp_rec
GO
