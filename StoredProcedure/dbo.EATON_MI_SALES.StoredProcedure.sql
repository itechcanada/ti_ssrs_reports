USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[EATON_MI_SALES]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Wendy Wang>
-- Create date: <Create Date,,>
-- Description:	<Description,,once receive the Excel file, import to table EATON_MI_PRICING, and run this script.>
-- =============================================
CREATE PROCEDURE [dbo].[EATON_MI_SALES]
	-- Add the parameters for the stored procedure here
	@FromDate datetime,
	@ToDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT b.sat_sld_cus_id, b.sat_nm1, b.sat_upd_ref, b.sat_inv_pfx,b.sat_inv_dt,b.sat_part, b.sat_blg_wgt,
b.sat_frm as form, b.sat_grd as grade, b.sat_size as size, b.sat_fnsh as finish, null as subs, null as eaton
FROM  [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b 	
INNER JOIN [LIVEUSSTX].[liveusstxdb].[informix].[injitd_rec] a ON b.sat_sprc_pfx=a.itd_ref_pfx AND b.sat_sprc_no=a.itd_ref_no
WHERE
b.sat_sld_cus_id ='11930'
AND CAST(b.sat_inv_dt AS datetime) BETWEEN @FromDate AND @ToDate
AND b.sat_frm LIKE 'EA%'
UNION    
SELECT b.sat_sld_cus_id, b.sat_nm1, b.sat_upd_ref, b.sat_inv_pfx,b.sat_inv_dt,b.sat_part, b.sat_blg_wgt,
	 c.form, c.grade, c.size, c.finish, c.subs, c.eaton
FROM  [LIVEUS_IW].[liveusstxdb_iw].[informix].[sahsat_rec] b 
	INNER JOIN dbo.EATON_MI_PRICING c ON b.sat_frm=c.form AND b.sat_grd=c.grade AND b.sat_size=c.size AND  b.sat_fnsh=c.finish
WHERE
b.sat_sld_cus_id in ('6010', '614', '855', '10120', '654', '141', '9309', '6302', '12088', '5724', '12217', '9890', '13211', '12533')
AND CAST(b.sat_inv_dt AS datetime) BETWEEN @FromDate AND @ToDate 


END

GO
