USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_iptjso]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,Mukesh>
-- Create date: <Create Date,11/3/2015,>
-- Description:	<Description,Open Orders,>

-- =============================================
create PROCEDURE [dbo].[US_iptjso]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 IF OBJECT_ID('dbo.US_iptjso_rec', 'U') IS NOT NULL
		drop table dbo.US_iptjso_rec;
    
        
SELECT *
into  dbo.US_iptjso_rec
FROM [LIVEUSSTX].[liveusstxdb].[informix].[iptjso_rec];
	
	

END

-- Select * from US_iptjso_rec













GO
