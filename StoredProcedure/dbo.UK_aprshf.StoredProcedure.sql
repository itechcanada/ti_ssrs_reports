USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_aprshf]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
      
-- =============================================      
-- Author:  <Author,Mukesh>      
-- Create date: <Create Date,Apr 28, 2016,>      
-- Description: <Description,Open Job history,>      
      
-- =============================================      
CREATE PROCEDURE [dbo].[UK_aprshf]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
     IF OBJECT_ID('dbo.UK_aprshf_rec', 'U') IS NOT NULL      
  drop table dbo.UK_aprshf_rec;       
      
       
    -- Insert statements for procedure here      
SELECT shf_cmpy_id,
shf_ven_id,
shf_shp_fm,
-- shf_shp_fm_nm,
shf_shp_fm_long_nm,
shf_admin_brh,
shf_mill,
shf_vat_id,
shf_iso_cert,
shf_qlty_cert,
shf_lng,
shf_pttrm,
shf_pmt_disc_trm,
shf_pur_pttrm,
shf_pur_disc_trm,
shf_mthd_pmt,
shf_shp_itm_tgth,
shf_alw_prtl_shp,
shf_ovrshp_pct,
shf_undshp_pct,
shf_rcvg_tag_pfx,
shf_ack_reqd,
shf_upd_dtts,
shf_upd_dtms,
shf_actv

into  dbo.UK_aprshf_rec      
  from [LIVEUKSTX].[liveukstxdb].[informix].[aprshf_rec]  ;       -- where shf_ven_id <> '528'
        
END      
-- select * from UK_aprshf_rec
GO
