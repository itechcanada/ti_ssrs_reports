USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CN_tctipd]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,03 Sep 2018,>    
-- Description: <>    
    
-- =============================================    
CREATE PROCEDURE [dbo].[CN_tctipd]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
IF OBJECT_ID('dbo.CN_tctipd_rec', 'U') IS NOT NULL          
  drop table dbo.CN_tctipd_rec;          
              
                  
SELECT *          
into  dbo.CN_tctipd_rec   
FROM [LIVECNSTX].[livecnstxdb].[informix].[tctipd_rec]     
    
END    
    
GO
