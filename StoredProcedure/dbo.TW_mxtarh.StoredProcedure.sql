USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_mxtarh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  iTECH   
-- Create date: April 4, 2013  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[TW_mxtarh]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
delete from dbo.TW_mxtarh_rec;     

insert into  dbo.TW_mxtarh_rec (arh_cmpy_id,arh_arch_ver_No,arh_bus_Doc_Typ,arh_gen_dtts,arh_prim_ref)          
SELECT arh_cmpy_id,arh_arch_ver_No,arh_bus_Doc_Typ,arh_gen_dtts,arh_prim_ref

FROM [LIVETWSTX].[livetwstxdb].[informix].[mxtarh_rec];  
  
END  
  
--- exec TW_mxtarh  
-- select * from TW_mxtarh_rec
/*
Date:20170412
Modified by mukesh
Modified SP due to it is taking time
*/
GO
