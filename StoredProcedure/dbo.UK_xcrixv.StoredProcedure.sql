USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_xcrixv]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Mukesh       
-- Create date: Feb 05, 2016      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE [dbo].[UK_xcrixv]       
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
    -- Insert statements for procedure here      
          
          
    IF OBJECT_ID('dbo.UK_xcrixv_rec', 'U') IS NOT NULL      
  drop table dbo.UK_xcrixv_rec;      
          
              
SELECT *      
into  dbo.UK_xcrixv_rec      
FROM [LIVEUKSTX].[liveukstxdb].[informix].[xcrixv_rec];      
      
END      
--  exec  UK_xcrixv      
-- select * from UK_xcrixv_rec
GO
