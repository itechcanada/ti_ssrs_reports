USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[GRAPHS_PRODUCTS]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,CLayton Daigle>
-- Create date: <Create Date,11/6/2012>
-- Description:	<Description,Top ship and stock products>
-- =============================================
CREATE PROCEDURE [dbo].[GRAPHS_PRODUCTS]
	-- Add the parameters for the stored procedure here
	@EndDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   -- Insert statements for procedure here
CREATE TABLE #TOP
(
product varchar(50),
period varchar(15),
wgt float
)


INSERT INTO #TOP
SELECT 
b.sat_frm+b.sat_grd,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS period
,SUM(b.sat_blg_wgt)
FROM [US_arrcus_rec] a 
INNER JOIN [US_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [US_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 5
GROUP BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE)
ORDER BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) ASC

INSERT INTO #TOP
SELECT 
b.sat_frm+b.sat_grd,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS period
,SUM(b.sat_blg_wgt)*(2.2)
FROM [UK_arrcus_rec] a 
INNER JOIN [UK_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [UK_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 5
GROUP BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE)
ORDER BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) ASC

INSERT INTO #TOP
SELECT 
b.sat_frm+b.sat_grd as product,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS period
,SUM(b.sat_blg_wgt)
FROM [CA_arrcus_rec] a 
INNER JOIN [CA_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [CA_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 5
GROUP BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE)
ORDER BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) ASC

INSERT INTO #TOP
SELECT 
b.sat_frm+b.sat_grd,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS period
,SUM(b.sat_blg_wgt)
FROM [TW_arrcus_rec] a 
INNER JOIN [TW_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [TW_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 5
GROUP BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE)
ORDER BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) ASC

INSERT INTO #TOP
SELECT 
b.sat_frm+b.sat_grd,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) AS period
,SUM(b.sat_blg_wgt)*(2.2)
FROM [NO_arrcus_rec] a 
INNER JOIN [NO_sahsat_rec] b ON a.cus_cus_id=b.sat_sld_cus_id	
INNER JOIN [NO_arrcuc_rec]	c ON a.cus_cus_cat=c.cuc_cus_cat
WHERE 
a.cus_actv=1 
AND b.sat_inv_pfx='SE' 
AND b.sat_frm <> 'XXXX' 
AND c.cuc_desc30<>'Interco'
AND DATEDIFF(MONTH,CAST(b.sat_inv_dt AS datetime),@EndDate) BETWEEN 0 AND 5
GROUP BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE)
ORDER BY b.sat_frm+b.sat_grd ,CAST(DATENAME(MONTH,b.sat_inv_dt) + ' ' + DATENAME(YEAR,b.sat_inv_dt) AS DATE) ASC


CREATE TABLE #TOP20
(
product varchar(50),
wgt float
)
INSERT INTO #TOP20
SELECT TOP 20 WITH TIES
product, SUM(wgt)
FROM #TOP
GROUP BY product
ORDER BY SUM(wgt) DESC


SELECT product, period, SUM(wgt) as wgt 
FROM #TOP 
WHERE 
product IN (SELECT product FROM #TOP20)
GROUP BY product,period
ORDER BY product, period




DROP TABLE #TOP
DROP TABLE #TOP20

END



GO
