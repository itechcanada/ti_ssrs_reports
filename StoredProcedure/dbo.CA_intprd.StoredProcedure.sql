USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_intprd]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[CA_intprd]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.CA_intprd_rec', 'U') IS NOT NULL  
  drop table dbo.CA_intprd_rec ;   
  
   
    -- Insert statements for procedure here  
SELECT * 
into  dbo.CA_intprd_rec  
  from [LIVECASTX].[livecastxdb].[informix].[intprd_rec] ;  
    
END
GO
