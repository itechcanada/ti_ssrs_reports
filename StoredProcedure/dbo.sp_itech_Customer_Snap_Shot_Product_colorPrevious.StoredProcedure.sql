USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_Customer_Snap_Shot_Product_colorPrevious]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Mukesh>    
-- Create date: <31 Oct 2017>    
-- Description: <sp_itech_Customer_Snap_Shot>   
-- =============================================    
CREATE  PROCEDURE [dbo].[sp_itech_Customer_Snap_Shot_Product_colorPrevious]  @DBNAME varchar(3), @CustomerID Varchar(10), @Year Varchar(4)
AS    
BEGIN    
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
declare @DB varchar(100);    
declare @sqltxt varchar(6000);    
declare @execSQLtxt varchar(7000);    
DECLARE @CountryName VARCHAR(25);       
DECLARE @prefix VARCHAR(15);       
DECLARE @DatabaseName VARCHAR(35);        
DECLARE @CurrenyRate varchar(15);      
DECLARE @StartDate VARCHAR(10);      
DECLARE @EndDate varchar(10); 
declare @iYear Varchar(4);   
    
CREATE TABLE #temp ( Dbname   VARCHAR(10)    
     ,CusID   VARCHAR(10)    
     , Product Varchar(300)
     ,Wgt   decimal(20,2)     
     );    
  
  CREATE TABLE #temp1 ( Dbname   VARCHAR(10)    
     ,CusID   VARCHAR(10)    
     , Product Varchar(300)
     ,Wgt   decimal(20,2)     
     );   
    
IF(YEAR(@Year) = YEAR(getdate())-1)
Begin
set @iYear = YEAR(getdate()) ;
insert into #temp1
EXEC [sp_itech_Customer_Snap_Shot_Product] @DBNAME ,@CustomerID, @iYear ;
END
Else
BEGIN
set @iYear = YEAR(getdate()) -1;
insert into #temp1
EXEC [sp_itech_Customer_Snap_Shot_Product] @DBNAME ,@CustomerID,@iYear ;
END     

insert into #temp 
EXEC [sp_itech_Customer_Snap_Shot_Product] @DBNAME ,@CustomerID,@Year ;

 SELECT top 20 Dbname,CusID, Product,Wgt, Case When Product in (select #temp1.Product from #temp1) then 'G' else 'O' end as ColorCode
  FROM #temp order by Wgt desc;    
 DROP TABLE  #temp ;    
 DROP TABLE  #temp1 ;  
END    
    
--EXEC [sp_itech_Customer_Snap_Shot_Product_color] 'US' ,'11930','2016'   
--EXEC [sp_itech_Customer_Snap_Shot_Product] 'UK' ,'1738'
--EXEC [sp_itech_Customer_Snap_Shot_Product] 'ALL','1738'    
  
  
/*  
sp_itech_GetTop40CustomerNoGrouping_WithEmail  sp_itech_GetTop40CustomerCurrentYTD_WithEmail sp_itech_GetTop40CustomerPreviousYTD_WithEmail
*/
GO
