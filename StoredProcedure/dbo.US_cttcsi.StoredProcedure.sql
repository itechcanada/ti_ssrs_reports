USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_cttcsi]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh   
-- Create date: Dec 12, 2015  
-- Description: <Description,,>  
-- =============================================  
Create PROCEDURE [dbo].[US_cttcsi]   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
    -- Insert statements for procedure here  
      
      
    IF OBJECT_ID('dbo.US_cttcsi_rec', 'U') IS NOT NULL  
  drop table dbo.US_cttcsi_rec;  
      
          
SELECT *  
into  dbo.US_cttcsi_rec  
FROM [LIVEUSSTX].[liveusstxdb].[informix].[cttcsi_rec];  
  
END  
--  exec  US_cttcsi  
-- select * from US_cttcsi_rec
GO
