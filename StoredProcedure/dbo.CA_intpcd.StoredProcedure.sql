USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[CA_intpcd]    Script Date: 03-11-2021 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,OCT 30, 2015,>    
-- Description: <Description,Open Orders,>    
    
-- =============================================    
create PROCEDURE [dbo].[CA_intpcd]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.CA_intpcd_rec', 'U') IS NOT NULL    
  drop table dbo.CA_intpcd_rec ;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.CA_intpcd_rec    
  from [LIVECASTX].[livecastxdb].[informix].[intpcd_rec] ;     
      
END    
-- select * from CA_intpcd_rec 
GO
