USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[WAREHOUSE_SHIPMENT_OTP]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Clayton Daigle>
-- Create date: <Create Date,10/3/2012,>
-- Description:	<Description,Warehouse Shipment On Time Performance,>
-- Change: <Clayton Daigle 9/24/2012 address problem with dates in Norway>
-- =============================================
CREATE PROCEDURE [dbo].[WAREHOUSE_SHIPMENT_OTP]
	-- Add the parameters for the stored procedure here
	
	@FromDate datetime,
	@ToDate   datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 'USA' as Country, a.tph_sch_dtts, a.tph_transp_pfx, a.tph_transp_no, b.tud_sprc_pfx, b.tud_sprc_no, b.tud_trpln_whs, b.tud_comp_pcs, b.tud_comp_wgt, c.orl_due_to_dt,c.orl_shpg_whs
FROM [US_trjtph_rec] a
INNER JOIN [US_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
INNER JOIN [US_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN @FromDate AND @ToDate 
AND b.tud_sprc_pfx<>'IT' 
AND b.tud_prnt_no<>0
UNION
SELECT 'UK' as Country, a.tph_sch_dtts, a.tph_transp_pfx, a.tph_transp_no, b.tud_sprc_pfx, b.tud_sprc_no, b.tud_trpln_whs, b.tud_comp_pcs, b.tud_comp_wgt, c.orl_due_to_dt,c.orl_shpg_whs
FROM [UK_trjtph_rec] a
INNER JOIN [UK_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
INNER JOIN [UK_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN @FromDate AND @ToDate 
AND b.tud_sprc_pfx<>'IT' 
AND b.tud_prnt_no<>0
UNION
SELECT 'CANADA' as Country, a.tph_sch_dtts, a.tph_transp_pfx, a.tph_transp_no, b.tud_sprc_pfx, b.tud_sprc_no, b.tud_trpln_whs, b.tud_comp_pcs, b.tud_comp_wgt, c.orl_due_to_dt,c.orl_shpg_whs
FROM [CA_trjtph_rec] a
INNER JOIN [CA_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
INNER JOIN [CA_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN @FromDate AND @ToDate 
AND b.tud_sprc_pfx<>'IT' 
AND b.tud_prnt_no<>0
UNION
SELECT 'TAIWAN' as Country, a.tph_sch_dtts, a.tph_transp_pfx, a.tph_transp_no, b.tud_sprc_pfx, b.tud_sprc_no, b.tud_trpln_whs, b.tud_comp_pcs, b.tud_comp_wgt, c.orl_due_to_dt,c.orl_shpg_whs
FROM [TW_trjtph_rec] a
INNER JOIN [TW_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
INNER JOIN [TW_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN @FromDate AND @ToDate 
AND b.tud_sprc_pfx<>'IT' 
AND b.tud_prnt_no<>0
UNION
SELECT 'NORWAY' as Country, a.tph_sch_dtts, a.tph_transp_pfx, a.tph_transp_no, b.tud_sprc_pfx, b.tud_sprc_no, b.tud_trpln_whs, b.tud_comp_pcs, b.tud_comp_wgt, c.orl_due_to_dt,c.orl_shpg_whs
FROM [NO_trjtph_rec] a
INNER JOIN [NO_trjtud_rec] b ON a.tph_transp_pfx=b.tud_transp_pfx AND a.tph_transp_no=b.tud_transp_no
INNER JOIN [NO_ortorl_rec] c ON b.tud_prnt_pfx=c.orl_ord_pfx AND b.tud_prnt_no=c.orl_ord_no AND b.tud_prnt_itm=c.orl_ord_itm
WHERE CAST(a.tph_sch_dtts AS datetime) BETWEEN @FromDate AND @ToDate 
AND b.tud_sprc_pfx<>'IT' 
AND b.tud_prnt_no<>0
END













GO
