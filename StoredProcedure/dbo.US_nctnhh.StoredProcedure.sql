USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_nctnhh]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,mrinal>
-- Create date: <Create Date,Aug 6, 2015,>
-- Description:	<Description,gl account,>

-- =============================================
CREATE PROCEDURE [dbo].[US_nctnhh]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	    IF OBJECT_ID('dbo.US_nctnhh_rec', 'U') IS NOT NULL
		drop table dbo.US_nctnhh_rec;	

	
    -- Insert statements for procedure here
SELECT *
into  dbo.US_nctnhh_rec
  from [LIVEUSSTX].[liveusstxdb].[informix].[nctnhh_rec]; 
  
END
-- select * from US_nctnhh_rec
GO
