USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_iptrps]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Mukesh>  
-- Create date: <Create Date,Aug 6, 2015,>  
-- Description: <Description,gl account,>  
  
-- =============================================  
create PROCEDURE [dbo].[UK_iptrps]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
     IF OBJECT_ID('dbo.UK_iptrps_rec', 'U') IS NOT NULL  
  drop table dbo.UK_iptrps_rec;   
  
   
    -- Insert statements for procedure here  
SELECT *  
into  dbo.UK_iptrps_rec  
  from [LIVEUKSTX].[liveukstxdb].[informix].[iptrps_rec];   
    
END  
-- select * from UK_iptrps_rec
GO
