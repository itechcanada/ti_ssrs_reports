USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_TIPurchaseJournal]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Mukesh >  
-- Create date: <21 Dec 2015>  
  
-- =============================================  
-- Description: <Cost Reconciliation Distribution for SSRS reports>  
  
CREATE PROCEDURE [dbo].[sp_itech_TIPurchaseJournal] @FromDate datetime, @ToDate datetime, @DBNAME varchar(50)  
  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
declare @sqltxt varchar(7000)  
declare @execSQLtxt varchar(7000)  
declare @DB varchar(100)  
declare @FD varchar(10)  
declare @TD varchar(10)  
declare @NOOfCust varchar(15)  
DECLARE @ExchangeRate varchar(15)  
  
DECLARE @FromYear varchar(10)  
DECLARE @ToYear varchar(10)  
  
set @DB=  @DBNAME  
set @FD = CONVERT(VARCHAR(10), @FromDate , 120)  
set @TD = CONVERT(VARCHAR(10), @ToDate , 120)  
  
  
  
  
CREATE TABLE #tmp ( Databases   VARCHAR(2)  
     ,  VchPfx   VARCHAR(2)  
        , VchNo    VARCHAR(10)  
        , SSNID    VARCHAR(10)   
        , EntryDate   Varchar(10)  
        , LoginID   Varchar(10)  
        , VendorID          Varchar(10)  
                    , VendorInvNo    Varchar(30)  
        , VendorInvDate     Varchar(10)  
        , POPfx    Varchar(2)  
        , PONo    Varchar(10)  
        , VchAmt   Decimal(20,2)   
        , VchDiscountableAmt  Decimal(20,2)   
        ,PaymentTrm   VARCHAR(5)   
        ,DiscountTrm  VARCHAR(5)    
        ,DueDate   VARCHAR(10)   
        ,DiscountDate   VARCHAR(10)   
        ,DiscountAmt  Decimal(20,2)  
        ,VchCategory  varchar(5)  
        ,UpdateDate   VARCHAR(10)   
                 );   
  
DECLARE @DatabaseName VARCHAR(35);  
DECLARE @Prefix VARCHAR(35);  
DECLARE @Name VARCHAR(15);  
DECLARE @CurrenyRate varchar(15);  
  
  
IF @DBNAME = 'ALL'  
 BEGIN  
    DECLARE ScopeCursor CURSOR FOR    
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS     
   OPEN ScopeCursor;    
     
    FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
     WHILE @@FETCH_STATUS = 0  
       BEGIN  
        DECLARE @query NVARCHAR(max);    
      SET @DB= @Prefix  --UPPER('['+ @DatabaseName +']') +'.' + LOWER('['+ @DatabaseName + 'db' +']')+'.' + '[informix]'  
       IF (UPPER(@Prefix) = 'TW')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
        End  
    Else if (UPPER(@Prefix) = 'NO')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
        End  
    Else if (UPPER(@Prefix) = 'CA')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
        End  
    Else if (UPPER(@Prefix) = 'CN')  
        begin  
         SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
        End  
    Else if (UPPER(@Prefix) = 'US' OR UPPER(@Prefix) = 'PS')  
        begin  
         SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
        End  
    Else if(UPPER(@Prefix) = 'UK')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
        End 
		Else if(UPPER(@Prefix) = 'DE')  
        begin  
           SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
        End
      
       
      SET @query =  'INSERT INTO #tmp (Databases, VchPfx,  VchNo, SSNID, EntryDate, LoginID, VendorID, VendorInvNo, VendorInvDate, POPfx , PONo, VchAmt,  
      VchDiscountableAmt,PaymentTrm,DiscountTrm,DueDate,DiscountDate,DiscountAmt,VchCategory,UpdateDate)  
        
      select  ''' + @Prefix + ''', vch_vchr_pfx,vch_vchr_no, vch_ssn_id,vch_ent_dt,vch_lgn_id, vch_ven_id,vch_ven_inv_no,vch_ven_inv_dt,vch_po_pfx,vch_po_no,vch_vchr_amt,  
      vch_dscb_amt,vch_pttrm,vch_disc_trm, vch_due_dt,vch_disc_dt, vch_disc_amt, vch_vchr_cat,vch_recu_gnrtn_dt  from ' + @Prefix + '_aptvch_rec  
      where vch_ent_dt >='''+ @FD +''' and vch_ent_dt <='''+ @TD +'''  
         
       '  
         
         
        EXECUTE sp_executesql @query;  
        print(@query)   
          
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;  
       END   
         
    CLOSE ScopeCursor;  
    DEALLOCATE ScopeCursor;  
  END  
  ELSE  
   BEGIN   
         
    Set @Name=(select Name from tbl_itech_DatabaseName_PS where Prefix=''+ @DBNAME + '')    
    print('2:' + @Name);  
    print('3:' + @DBNAME);  
     
    IF (UPPER(@DBNAME) = 'TW')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))  
   End  
   Else if (UPPER(@DBNAME) = 'NO')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))  
   End  
   Else if (UPPER(@DBNAME) = 'CA')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))  
   End  
   Else if (UPPER(@DBNAME) = 'CN')  
   begin  
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))  
   End  
   Else if (UPPER(@DBNAME) = 'US' OR UPPER(@DBNAME) = 'PS')  
   begin  
    SET @CurrenyRate = (SELECT ISNULL(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))  
   End  
   Else if(UPPER(@DBNAME) = 'UK')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))  
   End
   Else if(UPPER(@DBNAME) = 'DE')  
   begin  
      SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))  
   End
       
     SET @sqltxt ='INSERT INTO #tmp (Databases, VchPfx,  VchNo, SSNID, EntryDate, LoginID, VendorID, VendorInvNo, VendorInvDate, POPfx , PONo, VchAmt,  
      VchDiscountableAmt,PaymentTrm,DiscountTrm,DueDate,DiscountDate,DiscountAmt,VchCategory,UpdateDate)  
      select  ''' + @DBNAME + ''', vch_vchr_pfx,vch_vchr_no, vch_ssn_id,vch_ent_dt,vch_lgn_id, vch_ven_id,vch_ven_inv_no,vch_ven_inv_dt,vch_po_pfx,vch_po_no,vch_vchr_amt,  
      vch_dscb_amt,vch_pttrm,vch_disc_trm, vch_due_dt,vch_disc_dt, vch_disc_amt, vch_vchr_cat,vch_recu_gnrtn_dt  from ' + @DBNAME + '_aptvch_rec  
      where vch_ent_dt >='''+ @FD +''' and vch_ent_dt <='''+ @TD +'''   
       '  
    print(@sqltxt)  ;  
    set @execSQLtxt = @sqltxt;   
   EXEC (@execSQLtxt);  
     
     END  
      
      
         
       
     select * FROM #tmp;  
       
     drop table #tmp  
       
END  
  
-- exec [sp_itech_TIPurchaseJournal] '07/01/2015', '07/31/2015','ALL' ;  
--aptvch
/*
20210128	Sumit
Add Germany Database
*/
GO
