USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_JournalReceiving_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
    
    
-- =============================================      
-- Author:  <Sumit >      
-- Create date: <01 July 2020>      
-- Description: <Journal Receiving>      
-- =============================================      
    
CREATE PROCEDURE [dbo].[sp_itech_JournalReceiving_V1]     
(    
 @DBNAME varchar(50),    
 @FromDate datetime,     
 @ToDate datetime,    
 @venId varchar(10)    
)    
AS                                                        
BEGIN                                                        
SET NOCOUNT ON;    
DECLARE @query NVARCHAR(max);    
DECLARE @DB varchar(100)                                                     
DECLARE @DatabaseName VARCHAR(35);                                                        
DECLARE @Prefix VARCHAR(35);                          
DECLARE @Name VARCHAR(15);                                                  
DECLARE @CurrenyRate varchar(15)    
    
set @DB=  @DBNAME                                                 
                               
CREATE TABLE #temp (       
 [Database] varchar(10),     
 Cus_Ven_Id varchar(10),     
 Vendor_Name varchar(150),     
 Pep_Pep varchar(50),     
 Pep_Prd_Divn varchar(5),     
 Pep_Prd_Ln varchar(5),     
 Pep_Prd_Grp varchar(5),     
 Pep_Prd_Cat varchar(5),     
 Pep_Desc25 varchar(50),     
 Pep_Extd_Desc varchar(150),     
 Itv_Trs_Pcs int,     
 Itv_Trs_Wgt decimal(20,2),     
 Itv_Trs_Qty decimal(20,2),     
 Itv_Trs_Mat_Cst decimal(20,2),     
 Itv_Trs_Mat_Val decimal(20,2),    
 CurrecyRate decimal(13,2),
  );                                                       
    
IF @DBNAME = 'ALL'                                                        
 BEGIN                                                        
  DECLARE ScopeCursor CURSOR FOR                                            
  select DatabaseName,Name,Prefix from tbl_itech_DatabaseName_PS                                             
  OPEN ScopeCursor;                                            
  FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                  
  WHILE @@FETCH_STATUS = 0                                                        
  BEGIN                                                        
                                                              
   SET @DB= @Prefix                                                 
-- #region commented code for currency value    
   IF (UPPER(@DB) = 'TW')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
  End                        
    Else if (UPPER(@DB) = 'NO')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
  End                            
    Else if (UPPER(@DB) = 'CA')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
  End                            
    Else if (UPPER(@DB) = 'CN')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
  End                            
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                            
  begin                            
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
  End                            
 Else if(UPPER(@DB) = 'UK')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
  End                            
 Else if(UPPER(@DB) = 'DE')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
  End                            
    Else if(UPPER(@DB) = 'TWCN')                            
  begin                            
   SET @DB ='TW'                            
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
  End   
  print @DB +' - '+ @CurrenyRate ;   
    
-- #endregion commented code for currency value    
   set @query ='insert into #temp([Database], Cus_Ven_Id, Vendor_Name, Pep_Pep, Pep_Prd_Divn, Pep_Prd_Ln, Pep_Prd_Grp, Pep_Prd_Cat, Pep_Desc25, Pep_Extd_Desc,     
   Itv_Trs_Pcs, Itv_Trs_Wgt, Itv_Trs_Qty, Itv_Trs_Mat_Cst, Itv_Trs_Mat_Val, CurrecyRate)     
   Select ''' + @DB + ''', irh.irh_cus_ven_id, ven.ven_ven_long_nm, pep.pep_pep, pep.pep_prd_divn, pep.pep_prd_ln, pep.pep_prd_grp, pep.pep_prd_cat, pep.pep_desc25, pep.pep_extd_desc,     
   sum(itv.itv_trs_pcs) as itv_trs_pcs, sum(itv.itv_trs_wgt) as itv_trs_wgt, sum(itv.itv_trs_qty) as itv_trs_qty,   
   (ISNULL(sum(itv.itv_trs_mat_cst),0) * ' + @CurrenyRate + ') as itv_trs_mat_cst, (ISNULL(sum(itv.itv_trs_mat_val),0) * ' + @CurrenyRate + ') as itv_trs_mat_val, ' + @CurrenyRate + ' as CurrencyRate     
   from ' + @DB + '_injitd_rec itd     
   inner join ' + @DB + '_injith_rec ith on itd.itd_cmpy_id = ith.ith_cmpy_id and itd.itd_ref_pfx = ith.ith_ref_pfx    
   and itd.itd_ref_no = ith.ith_ref_no and itd.itd_ref_itm = ith.ith_ref_itm and itd.itd_actvy_dt = ith.ith_actvy_dt     
   inner join ' + @DB + '_injitv_rec itv on itd.itd_cmpy_id = itv.itv_cmpy_id and itd.itd_ref_pfx = itv.itv_ref_pfx and itd.itd_ref_no = itv.itv_ref_no and itd.itd_ref_itm = itv.itv_ref_itm     
   and itd.itd_ref_sbitm = itv.itv_ref_sbitm and itd.itd_actvy_dt = itv.itv_actvy_dt and itd.itd_trs_seq_no = itv.itv_trs_seq_no    
   inner join ' + @DB + '_inrprm_rec prm on itd.itd_frm = prm.prm_frm and itd.itd_grd = prm.prm_grd and itd.itd_size = prm.prm_size and itd.itd_fnsh = prm.prm_fnsh    
   inner join ' + @DB + '_inrpep_rec pep on prm.prm_pep = pep.pep_pep     
   inner join ' + @DB + '_trjirh_rec irh on itd.itd_cmpy_id = irh.irh_cmpy_id and itd.itd_ref_pfx = irh.irh_ref_pfx and itd.itd_ref_no = irh.irh_ref_no and itd.itd_actvy_dt = irh.irh_actvy_dt    
   inner join ' + @DB + '_scrwhs_rec whs on itd.itd_cmpy_id = whs.whs_cmpy_id and itd.itd_whs = whs.whs_whs    
   inner join ' + @DB + '_aprven_rec ven on itd.itd_cmpy_id = ven.ven_cmpy_id and irh.irh_cus_ven_id = ven.ven_ven_id    
   where itd.itd_cst_qty_indc = 1 and itd.itd_ref_pfx=''RC'' and ith.ith_upd_comp = ''1'' and irh.irh_rcpt_typ =''P'' and irh.irh_cus_ven_typ=''V''     
   and (CONVERT(varchar(10),itd.itd_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and     
   (CONVERT(varchar(10),itd.itd_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')    
   and (irh.irh_cus_ven_id ='''+ @venId +''' or '''+ @venId +'''= '''')    
   group by irh.irh_cus_ven_id, ven.ven_ven_long_nm, pep.pep_pep, pep.pep_prd_divn, pep.pep_prd_ln, pep.pep_prd_grp, pep.pep_prd_cat, pep.pep_desc25,pep.pep_extd_desc    
   order by irh.irh_cus_ven_id, pep.pep_desc25'    
   print @query;    
   EXECUTE sp_executesql @query;                                                        
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                                                        
  END                                                         
  CLOSE ScopeCursor;                                                        
  DEALLOCATE ScopeCursor;                                          
 END                  
ELSE                                                        
 BEGIN    
--#region commented code for currency value    
   IF (UPPER(@DB) = 'TW')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
  End                            
    Else if (UPPER(@DB) = 'NO')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                            
  End                            
    Else if (UPPER(@DB) = 'CA')                            
  begin           
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                            
  End                            
    Else if (UPPER(@DB) = 'CN')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                            
  End                            
    Else if (UPPER(@DB) = 'US' OR UPPER(@DB) = 'PS')                            
  begin                            
   SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                            
  End                            
 Else if(UPPER(@DB) = 'UK')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                            
  End                            
 Else if(UPPER(@DB) = 'DE')                            
  begin                            
   SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                            
  End                            
    Else if(UPPER(@DB) = 'TWCN')                            
  begin                            
   SET @DB ='TW'                            
   SET @CurrenyRate =(SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                            
  End        
  print @DB +' - '+ @CurrenyRate ;   
  
--#endregion commented code for currency value    
  set @query ='insert into #temp([Database], Cus_Ven_Id, Vendor_Name, Pep_Pep, Pep_Prd_Divn, Pep_Prd_Ln, Pep_Prd_Grp, Pep_Prd_Cat, Pep_Desc25, Pep_Extd_Desc,     
  Itv_Trs_Pcs, Itv_Trs_Wgt, Itv_Trs_Qty, Itv_Trs_Mat_Cst, Itv_Trs_Mat_Val, CurrecyRate)     
  Select ''' + @DB + ''', irh.irh_cus_ven_id, ven.ven_ven_long_nm, pep.pep_pep, pep.pep_prd_divn, pep.pep_prd_ln, pep.pep_prd_grp, pep.pep_prd_cat, pep.pep_desc25, pep.pep_extd_desc,     
  sum(itv.itv_trs_pcs) as itv_trs_pcs,sum(itv.itv_trs_wgt) as itv_trs_wgt, sum(itv.itv_trs_qty) as itv_trs_qty,   
  (ISNULL(sum(itv.itv_trs_mat_cst),0) * ' + @CurrenyRate + ') as itv_trs_mat_cst, (ISNULL(sum(itv.itv_trs_mat_val),0) * ' + @CurrenyRate + ') as itv_trs_mat_val, ' + @CurrenyRate + ' as CurrencyRate     
  from ' + @DB + '_injitd_rec itd     
  inner join ' + @DB + '_injith_rec ith on itd.itd_cmpy_id = ith.ith_cmpy_id and itd.itd_ref_pfx = ith.ith_ref_pfx    
  and itd.itd_ref_no = ith.ith_ref_no and itd.itd_ref_itm = ith.ith_ref_itm and itd.itd_actvy_dt = ith.ith_actvy_dt     
  inner join ' + @DB + '_injitv_rec itv on itd.itd_cmpy_id = itv.itv_cmpy_id and itd.itd_ref_pfx = itv.itv_ref_pfx and itd.itd_ref_no = itv.itv_ref_no and itd.itd_ref_itm = itv.itv_ref_itm     
  and itd.itd_ref_sbitm = itv.itv_ref_sbitm and itd.itd_actvy_dt = itv.itv_actvy_dt and itd.itd_trs_seq_no = itv.itv_trs_seq_no    
  inner join ' + @DB + '_inrprm_rec prm on itd.itd_frm = prm.prm_frm and itd.itd_grd = prm.prm_grd and itd.itd_size = prm.prm_size and itd.itd_fnsh = prm.prm_fnsh    
  inner join ' + @DB + '_inrpep_rec pep on prm.prm_pep = pep.pep_pep     
  inner join ' + @DB + '_trjirh_rec irh on itd.itd_cmpy_id = irh.irh_cmpy_id and itd.itd_ref_pfx = irh.irh_ref_pfx and itd.itd_ref_no = irh.irh_ref_no and itd.itd_actvy_dt = irh.irh_actvy_dt    
  inner join ' + @DB + '_scrwhs_rec whs on itd.itd_cmpy_id = whs.whs_cmpy_id and itd.itd_whs = whs.whs_whs    
  inner join ' + @DB + '_aprven_rec ven on itd.itd_cmpy_id = ven.ven_cmpy_id and irh.irh_cus_ven_id = ven.ven_ven_id    
  where itd.itd_cst_qty_indc = 1 and itd.itd_ref_pfx=''RC'' and ith.ith_upd_comp = ''1'' and irh.irh_rcpt_typ =''P'' and irh.irh_cus_ven_typ=''V''     
  and (CONVERT(varchar(10),itd.itd_actvy_dt,111) >= '''+ CONVERT(varchar(10),@FromDate,111) +''' or '''+ CONVERT(varchar(10),@FromDate,111) +''' ='''' ) and     
  (CONVERT(varchar(10),itd.itd_actvy_dt,111) <= '''+ CONVERT(varchar(10),@ToDate,111) +''' or '''+ CONVERT(varchar(10),@ToDate,111) +''' ='''')    
  and (irh.irh_cus_ven_id ='''+ @venId +''' or '''+ @venId +'''= '''')    
  group by irh.irh_cus_ven_id, ven.ven_ven_long_nm, pep.pep_pep, pep.pep_prd_divn, pep.pep_prd_ln, pep.pep_prd_grp, pep.pep_prd_cat, pep.pep_desc25,pep.pep_extd_desc    
  order by irh.irh_cus_ven_id, pep.pep_desc25'    
  print @query;    
  EXECUTE sp_executesql @query;    
 END                                                        
     
select * from #temp;    
drop table #temp  ;    
    
END     
/*    
Comments    
20200910 Sumit  
Add currency conversion process for itv_trs_mat_cst and Itv_Tsr_Mat_Val  
Mail: RE: Receiving Report  
20200915	Sumit
Add currency rate column
Mail: RE: Journal Receiving Detail.xlsx
*/    
--exec sp_itech_JournalReceiving_V1 'UK','09-01-2020','09-10-2020',''    
GO
