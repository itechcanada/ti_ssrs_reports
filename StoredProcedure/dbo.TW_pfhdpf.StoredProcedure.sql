USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[TW_pfhdpf]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
      
      
      
      
      
      
      
      
      
-- =============================================      
-- Author:  <Author,Clayton Daigle>      
-- Create date: <Create Date,11/6/2012,>      
-- Description: <Description,Open Orders,>     
-- Last changed by mukesh Date Jun 30, 2015     
      
-- =============================================      
CREATE PROCEDURE [dbo].[TW_pfhdpf]      
       
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON; 
      
   
Delete from dbo.TW_pfhdpf_rec ;       
       
Insert into dbo.TW_pfhdpf_rec       
      
    -- Insert statements for procedure here      
 (dpf_cmpy_id,dpf_sprc_pfx,dpf_sprc_no,dpf_ord_pfx,dpf_ord_no,dpf_ord_itm,dpf_ord_rls_no,dpf_ord_brh,dpf_sld_cus_id,dpf_shp_to,dpf_cus_po,dpf_sls_cat,dpf_dlvy_mthd,      
dpf_crr_nm,dpf_rls_due_fm_dt,dpf_rls_due_fm_hr,dpf_rls_due_fm_mt,dpf_rls_due_to_dt,dpf_rls_due_to_hr,dpf_rls_due_to_mt,dpf_shp_pcs,dpf_shp_msr,dpf_shp_wgt,dpf_shp_qty,      
dpf_bal_pcs,dpf_bal_pcs_typ,dpf_bal_msr,dpf_bal_msr_typ,dpf_bal_wgt,dpf_bal_wgt_typ,dpf_bal_qty,dpf_bal_qty_typ,dpf_rdy_by_dt,dpf_rdy_by_hr,dpf_rdy_by_mt,dpf_part_cus_id,dpf_part,dpf_part_revno,      
dpf_ord_wgt_um,dpf_frm,dpf_grd,dpf_size,dpf_fnsh,dpf_ef_evar,dpf_wdth,dpf_lgth,dpf_dim_dsgn,dpf_idia,dpf_odia,dpf_ga_size,dpf_ga_typ,dpf_transp_pfx,dpf_transp_no,dpf_shpg_whs,dpf_shp_to_city,      
dpf_shpg_dtts,dpf_shpg_dtms,dpf_lte_shpt,dpf_rsn_typ,dpf_rsn,dpf_rsn_rmk,dpf_lgn_id,dpf_crtd_dtts,dpf_crtd_dtms,dpf_smry_upd_flg)      
select dpf_cmpy_id,dpf_sprc_pfx,dpf_sprc_no,dpf_ord_pfx,dpf_ord_no,dpf_ord_itm,dpf_ord_rls_no,dpf_ord_brh,dpf_sld_cus_id,dpf_shp_to,dpf_cus_po,dpf_sls_cat,dpf_dlvy_mthd,      
dpf_crr_nm,dpf_rls_due_fm_dt,dpf_rls_due_fm_hr,dpf_rls_due_fm_mt,dpf_rls_due_to_dt,dpf_rls_due_to_hr,dpf_rls_due_to_mt,dpf_shp_pcs,dpf_shp_msr,dpf_shp_wgt,dpf_shp_qty,      
dpf_bal_pcs,dpf_bal_pcs_typ,dpf_bal_msr,dpf_bal_msr_typ,dpf_bal_wgt,dpf_bal_wgt_typ,dpf_bal_qty,dpf_bal_qty_typ,dpf_rdy_by_dt,dpf_rdy_by_hr,dpf_rdy_by_mt,dpf_part_cus_id,'-',dpf_part_revno,      
dpf_ord_wgt_um,dpf_frm,dpf_grd,dpf_size,dpf_fnsh,dpf_ef_evar,dpf_wdth,dpf_lgth,dpf_dim_dsgn,dpf_idia,dpf_odia,dpf_ga_size,dpf_ga_typ,dpf_transp_pfx,dpf_transp_no,dpf_shpg_whs,dpf_shp_to_city,      
dpf_shpg_dtts,dpf_shpg_dtms,dpf_lte_shpt,dpf_rsn_typ,dpf_rsn,dpf_rsn_rmk,dpf_lgn_id,dpf_crtd_dtts,dpf_crtd_dtms,dpf_smry_upd_flg      
    
FROM [LIVETWSTX].[livetwstxdb].[informix].[pfhdpf_rec]      
      
      
END      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
GO
