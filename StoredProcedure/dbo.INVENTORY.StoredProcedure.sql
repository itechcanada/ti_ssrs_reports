USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[INVENTORY]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,Clayton Daigle>
-- Create date: <Create Date,11/8/2012>
-- Description:	<Description,Monthly Inventory, 2/16/2012 - add form and size>
-- =============================================
CREATE PROCEDURE [dbo].[INVENTORY]
	-- Add the parameters for the stored procedure here
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
CREATE TABLE #PRODUCT
(
country varchar(10),
form varchar(25),
size varchar(25),
product varchar(50),
frmgrd varchar(25),
[3m_lbs] float,
[6m_lbs] float,
[12m_lbs] float,
po_wgt float,
so_wgt float,
stock_lbs float,
stock_cost float,
stock_value float,
stock_mo_supply float,
po_mo_supply float
)

INSERT INTO #PRODUCT
SELECT 'USA' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM [US_inrprm_rec]
WHERE prm_frm<>'XXXX' and prm_grd<>'S82D'
UNION
SELECT 'UK' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL  FROM [UK_inrprm_rec]
WHERE prm_frm<>'XXXX'
UNION
SELECT 'CANADA' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product,prm_frm+prm_grd, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL  FROM [CA_inrprm_rec]
WHERE prm_frm<>'XXXX'
UNION
SELECT 'TAIWAN' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL  FROM [TW_inrprm_rec]
WHERE prm_frm<>'XXXX'
UNION
SELECT 'NORWAY' as country,prm_frm,prm_size, prm_frm+prm_grd+prm_size+prm_fnsh as product, prm_frm+prm_grd,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL  FROM [NO_inrprm_rec]
WHERE prm_frm<>'XXXX'

	
/*Shipment*/
CREATE TABLE #SHIPMENT
(
country varchar(10),
product varchar(50),
[3m_lbs] float,
[6m_lbs] float,
[12m_lbs] float
)

INSERT INTO #SHIPMENT
SELECT 
'USA' as country,
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
'6m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
'12m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
FROM [US_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND 
sat_sls_cat<> 'CO'
AND sat_frm+sat_grd NOT IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'UK' as country,
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
'6m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
'12m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
FROM [UK_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO'
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'CANADA' as country,
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
'6m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
'12m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
FROM [CA_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO'
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'TAIWAN' as country,
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
'6m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
'12m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
FROM [TW_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO'
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'NORWAY' as country,
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_wgt END),
'6m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_wgt END),
'12m_lbs'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_wgt END)
FROM [NO_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO'
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh

/*Fittings and Pipe*/
INSERT INTO #SHIPMENT
SELECT 
'USA',
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_pcs END),
'6m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_pcs END),
'12m'=SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_pcs END)
FROM [US_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' and
sat_sls_cat<> 'CO' AND
sat_frm+sat_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh
UNION
SELECT 
'USA',
sat_frm+sat_grd+sat_size+sat_fnsh AS product,
'3m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 2 THEN sat_blg_msr END)/12,0),
'6m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 5 THEN sat_blg_msr END)/12,0),
'12m'=ROUND(SUM(CASE WHEN DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11 THEN sat_blg_msr END)/12,0)
FROM [US_sahsat_rec] 
WHERE 
sat_inv_pfx='SE' AND
sat_frm <> 'XXXX' AND
sat_sls_cat<> 'CO' AND
sat_frm+sat_grd IN ('TISP  2','TIWP  2')
AND DATEDIFF(M,CAST(sat_inv_dt AS datetime), @EndDate) BETWEEN 0 AND 11
GROUP BY sat_frm+sat_grd+sat_size+sat_fnsh


/*Open PO*/
CREATE TABLE #OPEN_PO
(
country varchar(10),
product varchar(50),
po_wgt float
)
INSERT INTO #OPEN_PO
SELECT 'USA' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O'
AND b.ipd_frm+b.ipd_grd NOT IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh
UNION
SELECT 'UK' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
FROM [UK_potpod_rec] a
INNER JOIN [UK_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O'
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh
UNION
SELECT 'CANADA' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
FROM [CA_potpod_rec] a
INNER JOIN [CA_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O'
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh
UNION
SELECT 'TAIWAN' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
FROM [TW_potpod_rec] a
INNER JOIN [TW_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O'
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh
UNION
SELECT 'NORWAY' as country, b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_wgt) as po_wgt 
FROM [NO_potpod_rec] a
INNER JOIN [NO_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O'
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh

/*Fitting and Pipe*/
INSERT INTO #OPEN_PO
SELECT 'USA', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, SUM(a.pod_bal_pcs)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh

INSERT INTO #OPEN_PO
SELECT 'USA', b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh as product, ROUND(SUM(a.pod_bal_msr)/12,0)
FROM [US_potpod_rec] a
INNER JOIN [US_tctipd_rec] b ON a.pod_po_pfx=b.ipd_ref_pfx AND a.pod_po_no=b.ipd_ref_no AND a.pod_po_itm=b.ipd_ref_itm
WHERE a.pod_po_pfx='PO' AND
a.pod_bal_wgt>0 AND
a.POD_TRCOMP_STS='O' AND
b.ipd_frm+b.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY b.ipd_frm+b.ipd_grd+b.ipd_size+b.ipd_fnsh



/*OPEN SO*/
CREATE TABLE #OPEN_SO
(
country varchar(10),
product varchar(50),
so_wgt float
)
INSERT INTO #OPEN_SO
SELECT 'USA' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
FROM [US_ortchl_rec] a
INNER JOIN [US_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A'
AND c.ipd_frm+c.ipd_grd NOT IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO
SELECT 'UK' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
FROM [UK_ortchl_rec] a
INNER JOIN [UK_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [UK_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A'
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO 
SELECT 'CANADA' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
FROM [CA_ortchl_rec] a
INNER JOIN [CA_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [CA_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A'
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO
SELECT 'TAIWAN' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
FROM [TW_ortchl_rec] a
INNER JOIN [TW_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [TW_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A'
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO
SELECT 'NORWAY' as country, c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_wgt) as so_wgt
FROM [NO_ortchl_rec] a
INNER JOIN [NO_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [NO_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A'
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

/*Fitting and Pipe*/
INSERT INTO #OPEN_SO
SELECT 'USA', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product,SUM(b.ord_bal_pcs) 
FROM [US_ortchl_rec] a
INNER JOIN [US_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A' AND
c.ipd_frm+c.ipd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh

INSERT INTO #OPEN_SO
SELECT 'USA', c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh as product, ROUND(SUM(b.ord_bal_msr)/12,0)
FROM [US_ortchl_rec] a
INNER JOIN [US_ortord_rec] b ON a.chl_ref_pfx=b.ord_ord_pfx AND a.chl_ref_no=b.ord_ord_no AND a.chl_ref_itm=b.ord_ord_itm
INNER JOIN [US_tctipd_rec] c ON b.ord_ord_pfx=c.ipd_ref_pfx AND b.ord_ord_no=c.ipd_ref_no AND b.ord_ord_itm=c.ipd_ref_itm
WHERE 
a.chl_chrg_no = 1 AND
a.chl_chrg_cl='E' AND
b.ord_ord_pfx= 'SO' AND
b.ord_sts_actn='A' AND
c.ipd_frm+c.ipd_grd IN ('TISP  2','TIWP  2')
GROUP BY c.ipd_frm+c.ipd_grd+c.ipd_size+c.ipd_fnsh




/*STOCK STATUS*/
CREATE TABLE #STOCK_STATUS1
(
country varchar(10),
product varchar(50),
stock_lbs float,
stock_cost float,
stock_value float
)
INSERT INTO #STOCK_STATUS1
SELECT 'USA' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_wgt, NULL, NULL
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S'
AND b.prd_frm+b.prd_grd NOT IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')

INSERT INTO #STOCK_STATUS1
SELECT 'UK',b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_wgt, NULL, NULL
FROM [UK_intprd_rec] b 
WHERE
b.prd_invt_sts = 'S'

INSERT INTO #STOCK_STATUS1
SELECT 'CANADA' ,b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_wgt, NULL, NULL
FROM [CA_intprd_rec] b 
WHERE
b.prd_invt_sts = 'S'

INSERT INTO #STOCK_STATUS1
SELECT 'TAIWAN' ,b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh,b.prd_ohd_wgt, NULL, NULL
FROM [TW_intprd_rec] b 
WHERE
b.prd_invt_sts = 'S'

INSERT INTO #STOCK_STATUS1
SELECT 'NORWAY' ,b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh,b.prd_ohd_wgt, NULL, NULL
FROM [NO_intprd_rec]  b 
WHERE
b.prd_invt_sts = 'S'

/*Fittings and Pipe*/
INSERT INTO #STOCK_STATUS1
SELECT 'USA' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_pcs, NULL, NULL
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')

INSERT INTO #STOCK_STATUS1
SELECT 'USA' , b.prd_frm+b.prd_grd+b.prd_size+b.prd_fnsh, b.prd_ohd_msr, NULL, NULL
FROM [US_intprd_rec] b                                                                    
WHERE
b.prd_invt_sts = 'S' AND
b.prd_frm+b.prd_grd IN ('TISP  2','TIWP  2')

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [US_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.country='USA'

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [UK_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.country='UK'

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [CA_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.country='CANADA'

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [TW_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.country='TAIWAN'

UPDATE #STOCK_STATUS1
SET stock_cost=a.acp_tot_mat_cst
FROM #STOCK_STATUS1 b, [NO_intacp_rec] a
WHERE b.product=a.acp_frm+a.acp_grd+a.acp_size+a.acp_fnsh
AND b.country='NORWAY'



CREATE TABLE #STOCK_STATUS2
(
country varchar(10),
product varchar(50),
stock_lbs float,
stock_cost float,
stock_value float
)
INSERT INTO #STOCK_STATUS2
SELECT country, product, SUM(stock_lbs), AVG(stock_cost), AVG(stock_cost)*SUM(stock_lbs)
FROM #STOCK_STATUS1
WHERE LEFT(product,7) NOT IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2','TISP  2','TIWP  2')
AND country='USA'
GROUP BY country, product

INSERT INTO #STOCK_STATUS2
SELECT country, product, SUM(stock_lbs), AVG(stock_cost), AVG(stock_cost)*SUM(stock_lbs)
FROM #STOCK_STATUS1
WHERE 
country<>'USA'
GROUP BY country, product

/*Fitting and Pipe*/
INSERT INTO #STOCK_STATUS2
SELECT country, product, SUM(stock_lbs), AVG(stock_cost), AVG(stock_cost)*SUM(stock_lbs)
FROM #STOCK_STATUS1
WHERE LEFT(product,7)  IN ('TIER  2','TICR  2','TIEL  2','TIFL  2','TISE  2','TITE  2','TIFT  2')
AND country='USA'
GROUP BY country, product

INSERT INTO #STOCK_STATUS2
SELECT country, product, ROUND(SUM(stock_lbs)/12,0), AVG(stock_cost), AVG(stock_cost)/12*SUM(stock_lbs)
FROM #STOCK_STATUS1
WHERE LEFT(product,7)  IN ('TISP  2','TIWP  2')
AND country='USA'
GROUP BY country, product


UPDATE #PRODUCT
SET [3m_lbs]=b.[3m_lbs],
    [6m_lbs]=b.[6m_lbs],
    [12m_lbs]=b.[12m_lbs]
FROM #PRODUCT a, #SHIPMENT b
WHERE a.country=b.country AND a.product=b.product
AND (b.[3m_lbs] IS NOT NULL OR b.[6m_lbs] IS NOT NULL OR b.[12m_lbs] IS NOT NULL)


UPDATE #PRODUCT
SET po_wgt=b.po_wgt
FROM #PRODUCT a ,#OPEN_PO b
WHERE a.country=b.country AND a.product=b.product
AND b.po_wgt IS NOT NULL

UPDATE #PRODUCT
SET so_wgt=b.so_wgt
FROM #PRODUCT a , #OPEN_SO b
WHERE a.country=b.country AND a.product=b.product
AND b.so_wgt IS NOT NULL

UPDATE #PRODUCT
SET stock_lbs=b.stock_lbs,
    stock_cost=b.stock_cost,
    stock_value=b.stock_value
FROM #PRODUCT a,#STOCK_STATUS2 b
WHERE a.country=b.country AND a.product=b.product
AND (b.stock_lbs IS NOT NULL OR b.stock_cost IS NOT NULL OR b.stock_value IS NOT NULL)

UPDATE #PRODUCT
SET stock_mo_supply=stock_lbs/([3m_lbs]/3),
    po_mo_supply=po_wgt/([3m_lbs]/3)
FROM #PRODUCT
WHERE [3m_lbs] <>0

/*
CREATE TABLE [dbo].[HPM_DETAIL_TEST](
	[country] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[ABC] [nvarchar](255) NULL,
	[Stock_Wgt] [float] NULL,
	[Stock_Value] [nvarchar](255) NULL,
	[Stock_Cost] [nvarchar](255) NULL,
	[3M_wgt] [float] NULL,
	[6M_wgt] [float] NULL,
	[12M_wgt] [float] NULL,
	[Stock_Mo_Supply] [nvarchar](255) NULL,
	[SO_Wgt] [nvarchar](255) NULL,
	[PO_Wgt] [nvarchar](255) NULL,
	[PO_Mo_Supply] [nvarchar](255) NULL
) ON [PRIMARY]
*/
DELETE FROM HPM_DETAIL
INSERT INTO HPM_DETAIL
SELECT country, product, 
CASE WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<250 THEN ''
	 WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<750  THEN 'C' 
	 WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]<1250 THEN 'B' 
	 WHEN form='TIRD' AND size<='.5625' AND [6m_lbs]>1249 THEN 'A'
	 WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<1000 THEN ''
	 WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<3000 THEN 'C' 
	 WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]<5000 THEN 'B' 
	 WHEN form='TIRD' AND size<='3.500' AND [6m_lbs]>4999 THEN 'A' 
	 WHEN form='TIRD' AND [6m_lbs]<1500 THEN ' '
	 WHEN form='TIRD' AND [6m_lbs]<3000 THEN 'C' 
	 WHEN form='TIRD' AND [6m_lbs]<5000 THEN 'B' 
	 WHEN form='TIRD' AND [6m_lbs]>4999 THEN 'A'	
	 WHEN form='TIPL' AND [6m_lbs]<1000 THEN ' '
	 WHEN form='TIPL' AND [6m_lbs]<2000 THEN 'C' 
	 WHEN form='TIPL' AND [6m_lbs]<4000 THEN 'B' 
	 WHEN form='TIPL' AND [6m_lbs]>3999 THEN 'A' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<250 THEN ''
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<750  THEN 'C' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]<1250 THEN 'B' 
     WHEN form='NIRD' AND size<='.5625' AND [6m_lbs]>1249 THEN 'A'
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<1000 THEN ''
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<3000 THEN 'C' 
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]<5000 THEN 'B' 
     WHEN form='NIRD' AND size<='3.500' AND [6m_lbs]>4999 THEN 'A'
     WHEN form='NIRD' AND [6m_lbs]<1500 THEN ''
     WHEN form='NIRD' AND [6m_lbs]<3000 THEN 'C' 
     WHEN form='NIRD' AND [6m_lbs]<5000 THEN 'B' 
     WHEN form='NIRD' AND [6m_lbs]>4999 THEN 'A'
	 WHEN form='TISH' AND [6m_lbs]<499 THEN ''
	 WHEN form='TISH' AND [6m_lbs]<1499  THEN 'C' 
	 WHEN form='TISH' AND [6m_lbs]<2000 THEN 'B' 
	 WHEN form='TISH' AND [6m_lbs]>1999 THEN 'A' END
,stock_lbs,stock_value,stock_cost,[3m_lbs],[6m_lbs],[12m_lbs],stock_mo_supply,so_wgt,po_wgt,po_mo_supply
FROM #PRODUCT



SELECT * FROM #PRODUCT
WHERE [3m_lbs] IS NOT NULL OR
[6m_lbs] IS NOT NULL OR
[12m_lbs]  IS NOT NULL OR
po_wgt  IS NOT NULL OR
so_wgt  IS NOT NULL OR
stock_lbs  IS NOT NULL OR
stock_cost  IS NOT NULL OR
stock_value  IS NOT NULL OR
stock_mo_supply  IS NOT NULL OR
po_mo_supply  IS NOT NULL 



DROP TABLE #PRODUCT
DROP TABLE #SHIPMENT
DROP TABLE #OPEN_PO
DROP TABLE #OPEN_SO
DROP TABLE #STOCK_STATUS1
DROP TABLE #STOCK_STATUS2

END



GO
