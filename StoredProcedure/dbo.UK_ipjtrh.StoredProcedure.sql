USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_ipjtrh]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,11/6/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[UK_ipjtrh]  
   
AS  
BEGIN  
 -- SET UKCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON; 
 IF OBJECT_ID('dbo.UK_ipjtrh_rec', 'U') IS NOT NULL          
  drop table dbo.UK_ipjtrh_rec;          
              
                  
SELECT 
trh_cmpy_id,
trh_ref_pfx,
trh_ref_no,
trh_ref_itm,
trh_ref_sbitm,
trh_actvy_dt,
trh_jbs_pfx,
trh_jbs_no,
trh_job_seq_no,
trh_cus_ownd,
trh_cus_id,
--trh_cus_nm,  -- commented due to sign mismatch issue 20180123
trh_plng_whs,
trh_ent_msr,
trh_actvy_whs,
trh_prs_cl,
trh_prs_1,
trh_prs_2,
trh_pwc,
trh_sch_pwc,
trh_pwg,
trh_sch_strt_dtts,
trh_sch_strt_dtms,
trh_sch_strt_ltts,
trh_sch_strt_ltms,
trh_expct_dtts,
trh_expct_dtms,
trh_strt_dtts,
trh_strt_dtms,
trh_job_cl,
trh_match_prod,
trh_est_stup_tm,
trh_est_run_tm,
trh_est_trdn_tm,
trh_prs_dy,
trh_desc30,
trh_tact_stup_tm,
trh_tact_run_tm,
trh_tact_trdn_tm,
trh_chrgb_dwntm,
trh_nchrgb_dwntm,
trh_rec_comp_dtts,
trh_rec_comp_dtms,
trh_lgn_id
          
into  dbo.UK_ipjtrh_rec   

FROM [LIVEUKSTX].[liveukstxdb].[informix].[ipjtrh_rec]  
  
  
END  
  
GO
