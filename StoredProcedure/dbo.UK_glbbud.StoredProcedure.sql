USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[UK_glbbud]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Author,Mukesh>    
-- Create date: <Create Date,Aug 6, 2015,>    
-- Description: <Description,gl account,>    
    
-- =============================================    
create PROCEDURE [dbo].[UK_glbbud]    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     IF OBJECT_ID('dbo.UK_glbbud_rec', 'U') IS NOT NULL    
  drop table dbo.UK_glbbud_rec;     
    
     
    -- Insert statements for procedure here    
SELECT *    
into  dbo.UK_glbbud_rec    
  from [LIVEUKGL].[liveukgldb].[informix].[glbbud_rec] ;     
      
END    
-- select * from UK_glbbud_rec
GO
