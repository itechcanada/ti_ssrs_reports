USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[sp_itech_MonthlyInventoryReportSUMMARYHistory_V1]    Script Date: 03-11-2021 16:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Mukesh >                      
-- Create date: <13 Jan 2017>                      
                   
                    
-- =============================================                      
CREATE PROCEDURE [dbo].[sp_itech_MonthlyInventoryReportSUMMARYHistory_V1] @DBNAME varchar(50), @InventoryStatus varchar(10),  @IncludeSizeFinish Char = '1',  @CurrencyInUS Char = '1'                     
                      
AS                      
BEGIN                      
               
                       
 SET NOCOUNT ON;       
   
  
  DECLARE @ExchangeRate varchar(15)                   
 DECLARE @CurrenyRate varchar(15)     
 declare @sqltxt Varchar(8000)       
declare @execSQLtxt Varchar(8000)     
declare @12FD varchar(10)                        
declare @3FD varchar(10)                        
declare @6FD varchar(10)                        
declare @TD varchar(10)                        
    
set @3FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 3, 0) , 120)   --First day of previous 3 month                        
set @6FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 6, 0) , 120)   --First day of previous 6 month                        
set @12FD =  CONVERT(VARCHAR(10), dateadd(month, datediff(month, 0, getdate()) - 12, 0) , 120)   --First day of previous 12 month                        
set @TD = CONVERT(VARCHAR(10), DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)), 120)    ---Last day of previous month       
        
     
 CREATE TABLE #tmp (   [Database]   VARCHAR(10)                      
        , Form     Varchar(65)                      
        , Grade     Varchar(65)                      
        , Size     Varchar(65)       
        , SizeDesc     Varchar(65)                    
        , Finish    Varchar(65)            
        , Whs Varchar(10)                    
        , Months3InvoicedWeight   DECIMAL(20, 2)                      
        , Months6InvoicedWeight   DECIMAL(20, 2)                      
        , Months12InvoicedWeight   DECIMAL(20, 2)                      
        , OpenSOWgt DECIMAL(20, 2)                      
        , OpenPOWgt DECIMAL(20, 2)                      
         , Avg3Month     DECIMAL(20, 2)                      
		 ,AvlWgt DECIMAL(20, 2)
         ,OhdStock DECIMAL(20, 2)                      
         ,OhdStockCost DECIMAL(20, 2)                      
         ,ReplCost DECIMAL(20, 2)                        
         ,CustID Varchar(max)                     
         ,MktSeg Varchar(max)         
         , Months3Sales   DECIMAL(20, 2)            
         , COGS   DECIMAL(20, 2)         
         , ReservedWeight Decimal (20,2)        
         ,TotalPOCost  Decimal (20,2)     
         , Months3InventoryWeight   DECIMAL(20, 2)                      
        , Months6InventoryWeight   DECIMAL(20, 2)                      
        , Months12InventoryWeight   DECIMAL(20, 2)                 
                 );    
                      
DECLARE @DatabaseName VARCHAR(35);                      
DECLARE @Prefix VARCHAR(35);                      
DECLARE @Name VARCHAR(15);       
declare @DB varchar(100)          
set @DB=  @DBNAME                                   
                    
if @DBNAME = 'ALL'      
Begin      
    
DECLARE ScopeCursor CURSOR FOR          
     select DatabaseName,Name,Prefix from tbl_itech_DatabaseName          
   OPEN ScopeCursor;     
   FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
     WHILE @@FETCH_STATUS = 0                      
       BEGIN                      
        DECLARE @query NVARCHAR(max);                         
      SET @DB= @Prefix      
          
  IF (@CurrencyInUS = '0')          
     Begin          
     set @CurrenyRate = 1;          
     End          
     Else          
     Begin                  
   IF (UPPER(@DB) = 'TW')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
    End                          
    Else if (UPPER(@DB) = 'NO')                          
    begin     
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
    End                          
    Else if (UPPER(@DB) = 'CA')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                          
    End                          
    Else if (UPPER(@DB) = 'CN')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                          
    End                          
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                          
    begin                          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
    End                          
    Else if(UPPER(@DB) = 'UK')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                          
    End                
    Else if(UPPER(@DB) = 'DE')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                          
    End                
     End       
         
    SET @query ='INSERT INTO #tmp ([Database],Form,Grade,Size,SizeDesc,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,    
    OpenSOWgt,Avg3Month,AvlWgt, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,ReservedWeight,TotalPOCost, Months3InventoryWeight, Months6InventoryWeight, Months12InventoryWeight )      
       select [Database],Ltrim(Form) , Ltrim(Grade) , Size,SizeDesc, Finish, Whs, Months3InvoicedWeight, Months6InvoicedWeight, Months12InvoicedWeight,                     
 ReplCost* '+ @CurrenyRate +',OpenPOWgt, OpenSOWgt,Avg3Month,AvlWgt,OhdStock,OhdStockCost* '+ @CurrenyRate +',CustID , MktSeg, Months3Sales  , COGS* '+ @CurrenyRate +',    
 ReservedWeight ,TotalPOCost* '+ @CurrenyRate +' , '    
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')     
  BEGIN    
  set @query = @query + '    
  (select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @3FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @6FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @12FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg '    
END    
else    
BEGIN    
 set @query = @query + '    
  (select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @3FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @6FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @12FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg '    
END    
    
  set @query = @query + '  from tbl_itech_MonthlyInventoryReportSUMMARYS hst  Where  [Database] = ''' + @DB + ''' '                  
print @query;                      
        EXECUTE sp_executesql @query;                      
        FETCH NEXT FROM ScopeCursor INTO @DatabaseName,@Name,@Prefix;                      
       END                       
    CLOSE ScopeCursor;                      
    DEALLOCATE ScopeCursor;      
             
End      
Else    
Begin      
    
  IF (@CurrencyInUS = '0')          
     Begin          
     set @CurrenyRate = 1;          
     End          
     Else          
     Begin                  
   IF (UPPER(@DB) = 'TW')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('TW','USD','TWD'))                          
    End                          
    Else if (UPPER(@DB) = 'NO')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('NO','USD','NOK'))                          
    End                          
    Else if (UPPER(@DB) = 'CA')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CA','USD','CAD'))                          
    End                          
    Else if (UPPER(@DB) = 'CN')                          
    begin                          
     SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('CN','USD','RMB'))                          
    End                          
    Else if (UPPER(@DB) = 'US' or UPPER(@DB) = 'PS' )                          
    begin                          
     SET @CurrenyRate = (SELECT ISnull(dbo.fun_itech_funCurrencyConvert('US','USD','USD'),1))                          
    End                          
    Else if(UPPER(@DB) = 'UK')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('UK','USD','GBP'))                          
    End                
    Else if(UPPER(@DB) = 'DE')                          
    begin                          
    SET @CurrenyRate = (SELECT dbo.fun_itech_funCurrencyConvert('DE','USD','EUR'))                          
    End                
     End       
         
     SET @sqltxt ='INSERT INTO #tmp ([Database],Form,Grade,Size,SizeDesc,Finish,Whs,Months3InvoicedWeight,Months6InvoicedWeight,Months12InvoicedWeight,ReplCost,OpenPOWgt,    
    OpenSOWgt,Avg3Month,AvlWgt, OhdStock, OhdStockCost, CustID,MktSeg ,Months3Sales,COGS,ReservedWeight,TotalPOCost, Months3InventoryWeight, Months6InventoryWeight, Months12InventoryWeight )      
       select [Database],Ltrim(Form) , Ltrim(Grade) , Size, SizeDesc,Finish, Whs, Months3InvoicedWeight, Months6InvoicedWeight, Months12InvoicedWeight,                     
 ReplCost* '+ @CurrenyRate +',OpenPOWgt, OpenSOWgt,Avg3Month,AvlWgt,OhdStock,OhdStockCost* '+ @CurrenyRate +',CustID , MktSeg, Months3Sales  , COGS* '+ @CurrenyRate +',    
 ReservedWeight ,TotalPOCost* '+ @CurrenyRate +' ,'    
  if  (UPPER(@DB)= 'NO' OR  UPPER(@DB)= 'UK')     
  BEGIN    
  set @sqltxt = @sqltxt + '    
  (select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @3FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @6FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt * 2.20462,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @12FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg '    
END    
else    
BEGIN    
 set @sqltxt = @sqltxt + '    
  (select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @3FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @6FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg ,    
(select ISNULL(SUM(ISNULL(prd_ohd_wgt ,0)),0) FROM ' + @DB + '_intprd_rec_history where prd_frm = hst.Form     
and prd_grd = hst.Grade and prd_size = hst.Size and prd_fnsh = hst.Finish and prd_invt_sts = ''S''     
and UpdateDtTm between ''' + @12FD + '''  And ''' + @TD + '''  ) as prd_ohd_wg '    
END    
    
  set @sqltxt = @sqltxt + ' from tbl_itech_MonthlyInventoryReportSUMMARYS hst Where  [Database] = ''' + @DB + ''' '     
                
       print( @sqltxt)                       
    set @execSQLtxt = @sqltxt;                       
   EXEC (@execSQLtxt);                 
                                       
   END      
       
       
   if(@IncludeSizeFinish = '0')        
Begin        
Select  [Database], Product, 0 as Size, 0 as  Finish, Whs, MatGroup, SUM(ISNULL( Months3InvoicedWeight,0)) as Months3InvoicedWeight,        
  SUM(ISNULL(Months6InvoicedWeight,0)) as Months6InvoicedWeight, SUM(ISNULL(Months12InvoicedWeight,0)) as Months12InvoicedWeight,        
  SUM(ISNULL(ReplCost,0)) as ReplCost,  SUM(ISNULL(OpenPOWgt,0)) as OpenPOWgt,  SUM(ISNULL(OpenSOWgt,0)) as OpenSOWgt,        
   SUM(ISNULL(Avg3Month,0)) as Avg3Month, SUM(ISNULL(POMOSupply,0)) as POMOSupply,SUM(ISNULL(StockMOSupply,0)) as StockMOSupply,        
    SUM(ISNULL(AvlWgt,0)) as AvlWgt, SUM(ISNULL(OhdStock,0)) as OhdStock,SUM(ISNULL(OhdStockCost,0)) as OhdStockCost , SUM(ISNULL(InStockCostWgt,0)) as InStockCostWgt, '' as CustID,                   
    '' as MktSeg, SUM(ISNULL(Excess,0)) as Excess,SUM(ISNULL(Excess1500,0)) as Excess1500,SUM(ISNULL(Excess2500,0)) as Excess2500,        
    SUM(ISNULL(ExcessValue,0)) as ExcessValue ,Product as  ProductWithSize, SUM(ISNULL(Months3Sales,0)) as Months3Sales, SUM(ISNULL(COGS,0)) as COGS ,        
    SUM(ISNULL(ReservedWeight,0)) as ReservedWeight, SUM(ISNULL(TotalPOCost,0)) as TotalPOCost,    
    SUM(ISNULL(Months3InventoryWeight,0)) as Months3InventoryWeight, SUM(ISNULL(Months6InventoryWeight,0)) as Months6InventoryWeight,     
    SUM(ISNULL(Months12InventoryWeight,0)) as Months12InventoryWeight  from (        
                         
select [Database],RTrim(Ltrim(Form)) + '/'+ RTRIM(Ltrim(Grade)) + '/' + RTRIM(LTRIM(SizeDesc)) + '/' + RTRIM(LTRIM(finish)) as 'Product', Size,  Finish, Whs,                    
--select [Database],Ltrim(Form) + '/'+ Ltrim(Grade)   as 'Product', Size, Finish,                     
CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,                      
 Months3InvoicedWeight,                      
 Months6InvoicedWeight,                      
 Months12InvoicedWeight,                     
 ReplCost,                     
 OpenPOWgt,                      
 OpenSOWgt,                      
 Avg3Month,                       
 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',                      
 cast(OhdStock/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply',
 AvlWgt,
 OhdStock,                       
 OhdStockCost,                       
 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt' ,                    
 CustID ,                    
 MktSeg,                  
 (OhdStock - ISNULL(Months6InvoicedWeight, 0)) AS Excess,                  
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 1500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess1500,         
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 2500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess2500,        
 Ltrim(Form) + '/'+ Ltrim(Grade) + ' ' + Size   as ProductWithSize,        
 Months3Sales  , COGS,        
 ReservedWeight,  TotalPOCost,     
  Months3InventoryWeight, Months6InventoryWeight, Months12InventoryWeight,     
CONVERT (DECIMAL(10,2), ((CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END)  * (OhdStock - ISNULL(Months6InvoicedWeight, 0)))) AS ExcessValue         
          
 from #tmp                      
 WHERE  (                      
(@InventoryStatus = 'False')OR                
(@InventoryStatus = 'True'  and OhdStock>0)                      
)                     
--and Form = 'ALPL' and Grade = 'AL6061'                      
   ) as t group by [Database], Product, Whs, MatGroup    order by Product  ;      
         
End        
                      
                    
  ELSE                      
     BEGIN                  
             
                         
select [Database],RTrim(Ltrim(Form)) + '/'+ RTRIM(Ltrim(Grade)) + '/' + RTRIM(LTRIM(SizeDesc)) + '/' + RTRIM(LTRIM(finish)) as 'Product', Size, Finish, Whs,                    
CASE WHEN SUBSTRING(Form,1,1) = 'T' OR SUBSTRING(Form,1,1) = 'Z' THEN 'TITANIUM' ELSE 'HPM' END as MatGroup,                      
 Months3InvoicedWeight,                      
 Months6InvoicedWeight,                      
 Months12InvoicedWeight,                     
 ReplCost,                     
 OpenPOWgt,                      
 OpenSOWgt,                      
 Avg3Month,                       
 cast(OpenPOWgt/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'POMOSupply',                      
 cast(OhdStock/NULLIF(Avg3Month, 0) as decimal(20,2)) as 'StockMOSupply', 
 AvlWgt,
 OhdStock,                       
 OhdStockCost,                       
 cast(NULLIF(OhdStockCost, 0)/NULLIF(OhdStock,0) as decimal(20,2)) as 'InStockCostWgt' ,                    
 CustID ,                    
 MktSeg,                  
 (OhdStock - ISNULL(Months6InvoicedWeight, 0)) AS Excess,                  
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 1500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess1500,                      
 CASE WHEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) > 2500 THEN (OhdStock - ISNULL(Months6InvoicedWeight, 0)) ELSE '0' END as Excess2500,                  
 Ltrim(Form) + '/'+ Ltrim(Grade) + ' ' + Size   as ProductWithSize,        
         
CONVERT (DECIMAL(10,2), ((CASE WHEN OhdStock > 0 THEN ROUND(OhdStockCost/OhdStock,2) ELSE 0 END)  * (OhdStock - ISNULL(Months6InvoicedWeight, 0)))) AS ExcessValue          
  ,Months3Sales  , COGS,ReservedWeight ,TotalPOCost ,Months3InventoryWeight, Months6InventoryWeight, Months12InventoryWeight       
 from #tmp                      
 WHERE  (                      
(@InventoryStatus = 'False')OR                      
(@InventoryStatus = 'True'  )                      
)                   
--and Form = 'TIRC' and Grade = '64'  and Size = '2X3'                    
 order by Product  ;     
      
END                      
 End         
/*
--exec [sp_itech_MonthlyInventoryReportSUMMARYHistory] 'CA',  'False'  ,'0','1' -- 3911,3927        
--exec [sp_itech_MonthlyInventoryReportSUMMARYHistory] 'ALL',  'True'                  

20210609	Sumit
add AvlWgt column
Mail: RE: FW: Inventory report 5
*/
GO
