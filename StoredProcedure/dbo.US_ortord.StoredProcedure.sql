USE [Stratix_US]
GO
/****** Object:  StoredProcedure [dbo].[US_ortord]    Script Date: 03-11-2021 16:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
-- =============================================  
-- Author:  <Author,Clayton Daigle>  
-- Create date: <Create Date,10/5/2012,>  
-- Description: <Description,Open Orders,>  
  
-- =============================================  
CREATE PROCEDURE [dbo].[US_ortord]  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
IF OBJECT_ID('dbo.US_ortord_rec', 'U') IS NOT NULL            
  drop table dbo.US_ortord_rec;     
   
  
    -- Insert statements for procedure here  
SELECT *  
into dbo.US_ortord_rec 
FROM [LIVEUSSTX].[liveusstxdb].[informix].[ortord_rec]  
  
  
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
