USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_ccrtkt_rec_PRE_April10]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_ccrtkt_rec_PRE_April10](
	[tkt_crm_tsk_typ] [char](3) NOT NULL,
	[tkt_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[tkt_desc30] [char](30) NOT NULL,
	[tkt_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
