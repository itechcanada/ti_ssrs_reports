USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_trhray_rec_copy]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_trhray_rec_copy](
	[ray_cmpy_id] [char](3) NOT NULL,
	[ray_ref_pfx] [char](2) NOT NULL,
	[ray_ref_no] [numeric](8, 0) NOT NULL,
	[ray_ref_itm] [numeric](3, 0) NOT NULL,
	[ray_ref_ln_no] [numeric](2, 0) NOT NULL,
	[ray_cst_no] [numeric](3, 0) NOT NULL,
	[ray_cst_cl] [char](1) NOT NULL,
	[ray_cst_qty] [numeric](16, 4) NOT NULL,
	[ray_cst_qty_um] [char](3) NULL,
	[ray_bas_cry_val] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
