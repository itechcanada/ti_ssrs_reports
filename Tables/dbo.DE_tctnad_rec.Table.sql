USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_tctnad_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_tctnad_rec](
	[nad_cmpy_id] [char](3) NOT NULL,
	[nad_Ref_pfx] [char](2) NOT NULL,
	[nad_Ref_no] [numeric](8, 0) NOT NULL,
	[nad_Ref_itm] [numeric](3, 0) NOT NULL,
	[nad_addr_typ] [char](1) NOT NULL,
	[nad_pcd] [char](10) NOT NULL,
	[nad_nm1] [char](35) NOT NULL
) ON [PRIMARY]
GO
