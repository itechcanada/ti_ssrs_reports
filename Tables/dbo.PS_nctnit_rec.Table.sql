USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_nctnit_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_nctnit_rec](
	[nit_cmpy_id] [char](3) NOT NULL,
	[nit_ncr_pfx] [char](2) NOT NULL,
	[nit_ncr_no] [numeric](8, 0) NOT NULL,
	[nit_ncr_itm] [numeric](3, 0) NOT NULL,
	[nit_claim_itm_typ] [char](1) NOT NULL,
	[nit_rcpt_pfx] [char](2) NOT NULL,
	[nit_rcpt_no] [numeric](8, 0) NOT NULL,
	[nit_rcpt_itm] [numeric](3, 0) NOT NULL,
	[nit_brh] [char](3) NULL,
	[nit_whs] [char](3) NULL,
	[nit_desc60] [char](60) NOT NULL,
	[nit_pmw_ent_mthd] [char](1) NOT NULL,
	[nit_claim_pcs] [numeric](7, 0) NOT NULL,
	[nit_claim_msr] [numeric](16, 4) NOT NULL,
	[nit_claim_wgt] [numeric](10, 2) NOT NULL,
	[nit_claim_qty] [numeric](16, 4) NOT NULL,
	[nit_claim_val] [numeric](12, 2) NOT NULL,
	[nit_ord_wgt_um] [char](3) NOT NULL,
	[nit_apvd_pcs] [numeric](7, 0) NOT NULL,
	[nit_apvd_msr] [numeric](16, 4) NOT NULL,
	[nit_apvd_wgt] [numeric](10, 2) NOT NULL,
	[nit_apvd_qty] [numeric](16, 4) NOT NULL,
	[nit_apcr_pcs] [numeric](7, 0) NOT NULL,
	[nit_apcr_msr] [numeric](16, 4) NOT NULL,
	[nit_apcr_wgt] [numeric](10, 2) NOT NULL,
	[nit_apcr_qty] [numeric](16, 4) NOT NULL,
	[nit_apcr_val] [numeric](12, 2) NOT NULL,
	[nit_rtnd_pcs] [numeric](7, 0) NOT NULL,
	[nit_rtnd_msr] [numeric](16, 4) NOT NULL,
	[nit_rtnd_wgt] [numeric](10, 2) NOT NULL,
	[nit_rtnd_qty] [numeric](16, 4) NOT NULL,
	[nit_pcs_typ] [char](1) NOT NULL,
	[nit_msr_typ] [char](1) NOT NULL,
	[nit_wgt_typ] [char](1) NOT NULL,
	[nit_qty_typ] [char](1) NOT NULL,
	[nit_rma_sts] [char](3) NOT NULL,
	[nit_cr_auth_sts] [char](3) NOT NULL,
	[nit_ncr_sts_nts] [char](60) NOT NULL,
	[nit_rsln_typ] [char](3) NOT NULL,
	[nit_mat_dispn] [char](1) NOT NULL,
	[nit_rsn_typ] [char](3) NOT NULL,
	[nit_rsn] [char](3) NULL,
	[nit_ncr_actn_cd] [char](3) NULL,
	[nit_use_wgt_mult] [numeric](1, 0) NOT NULL,
	[nit_chrg_qty_typ] [char](1) NOT NULL
) ON [PRIMARY]
GO
