USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_ipjsoh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_ipjsoh_rec](
	[soh_cmpy_id] [char](3) NOT NULL,
	[soh_jbs_pfx] [char](2) NOT NULL,
	[soh_jbs_no] [numeric](8, 0) NOT NULL,
	[soh_ord_pfx] [char](2) NOT NULL,
	[soh_ord_no] [numeric](8, 0) NOT NULL,
	[soh_ord_itm] [numeric](3, 0) NOT NULL,
	[soh_ord_sitm] [numeric](2, 0) NOT NULL,
	[soh_ord_typ] [char](1) NOT NULL,
	[soh_dflt_shpg_whs] [char](3) NOT NULL,
	[soh_prod_pcs] [numeric](7, 0) NOT NULL,
	[soh_prod_msr] [numeric](16, 4) NOT NULL,
	[soh_prod_wgt] [numeric](10, 2) NOT NULL,
	[soh_prod_qty] [numeric](16, 4) NOT NULL,
	[soh_expct_pcs] [numeric](7, 0) NOT NULL,
	[soh_expct_msr] [numeric](16, 4) NOT NULL,
	[soh_expct_wgt] [numeric](10, 2) NOT NULL,
	[soh_expct_qty] [numeric](16, 4) NOT NULL,
	[soh_lst_job_pfx] [char](2) NOT NULL,
	[soh_lst_job_no] [numeric](8, 0) NOT NULL,
	[soh_lst_job_whs] [char](3) NOT NULL,
	[soh_lst_job_prs] [char](3) NOT NULL
) ON [PRIMARY]
GO
