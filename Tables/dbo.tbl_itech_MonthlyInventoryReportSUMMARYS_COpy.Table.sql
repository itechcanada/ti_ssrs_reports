USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_MonthlyInventoryReportSUMMARYS_COpy]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_MonthlyInventoryReportSUMMARYS_COpy](
	[UpdDtTm] [varchar](25) NULL,
	[Database] [varchar](10) NULL,
	[Form] [varchar](65) NULL,
	[Grade] [varchar](65) NULL,
	[Size] [varchar](65) NULL,
	[Finish] [varchar](65) NULL,
	[Whs] [varchar](10) NULL,
	[Months3InvoicedWeight] [decimal](20, 2) NULL,
	[Months6InvoicedWeight] [decimal](20, 2) NULL,
	[Months12InvoicedWeight] [decimal](20, 2) NULL,
	[ReplCost] [decimal](20, 2) NULL,
	[OpenPOWgt] [decimal](20, 2) NULL,
	[OpenSOWgt] [decimal](20, 2) NULL,
	[Avg3Month] [decimal](20, 2) NULL,
	[OhdStock] [decimal](20, 2) NULL,
	[OhdStockCost] [decimal](20, 2) NULL,
	[CustID] [varchar](max) NULL,
	[MktSeg] [varchar](max) NULL,
	[Months3Sales] [decimal](20, 2) NULL,
	[COGS] [decimal](20, 2) NULL,
	[ReservedWeight] [decimal](20, 2) NULL,
	[TotalPOCost] [decimal](20, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
