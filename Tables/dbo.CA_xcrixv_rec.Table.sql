USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_xcrixv_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_xcrixv_rec](
	[ixv_cmpy_id] [char](3) NOT NULL,
	[ixv_ven_id] [char](8) NOT NULL,
	[ixv_sply_cmpy_id] [char](3) NOT NULL,
	[ixv_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
