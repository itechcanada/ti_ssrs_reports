USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_perplv_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_perplv_rec](
	[plv_cmpy_id] [char](3) NOT NULL,
	[plv_prc_bk_ctl_no] [numeric](10, 0) NOT NULL,
	[plv_prc_lvl] [numeric](2, 0) NOT NULL,
	[plv_prc_mkup_typ] [char](1) NOT NULL,
	[plv_prc_mkup_rt] [numeric](13, 4) NOT NULL,
	[plv_prc_mkup_pct] [numeric](5, 2) NOT NULL,
	[plv_prc_qty_brk] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
