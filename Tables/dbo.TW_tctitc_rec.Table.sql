USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_tctitc_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_tctitc_rec](
	[itc_cmpy_id] [char](3) NOT NULL,
	[itc_ref_pfx] [char](2) NOT NULL,
	[itc_ref_no] [numeric](8, 0) NOT NULL,
	[itc_ref_itm] [numeric](3, 0) NOT NULL,
	[itc_cert_typ] [char](3) NOT NULL,
	[itc_sdo] [char](6) NOT NULL,
	[itc_cpy_shpt] [numeric](1, 0) NOT NULL,
	[itc_cpy_ml] [numeric](1, 0) NOT NULL,
	[itc_extl_sys] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
