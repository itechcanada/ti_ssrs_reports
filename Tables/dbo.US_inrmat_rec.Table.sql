USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_inrmat_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_inrmat_rec](
	[mat_frm] [char](6) NOT NULL,
	[mat_grd] [char](8) NOT NULL,
	[mat_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[mat_extd_desc] [char](50) NOT NULL,
	[mat_fnsh_reqd] [numeric](1, 0) NOT NULL,
	[mat_e_dns] [numeric](8, 6) NOT NULL,
	[mat_m_dns] [numeric](8, 6) NOT NULL,
	[mat_dflt_pep] [char](10) NULL,
	[mat_tst_tmpl] [char](3) NULL,
	[mat_clr] [char](3) NULL,
	[mat_wgt_dcml] [numeric](1, 0) NOT NULL,
	[mat_e_qty_cst_um] [char](3) NOT NULL,
	[mat_e_qty_disp_um] [char](3) NOT NULL,
	[mat_e_dflt_cstg_um] [char](3) NULL,
	[mat_e_dflt_chrg_um] [char](3) NULL,
	[mat_m_qty_cst_um] [char](3) NOT NULL,
	[mat_m_qty_disp_um] [char](3) NOT NULL,
	[mat_m_dflt_cstg_um] [char](3) NULL,
	[mat_m_dflt_chrg_um] [char](3) NULL,
	[mat_cst_dcml] [numeric](1, 0) NOT NULL,
	[mat_prc_dcml] [numeric](1, 0) NOT NULL,
	[mat_po_aly_schg] [char](3) NULL,
	[mat_sls_aly_schg] [char](3) NULL,
	[mat_dflt_so_ga_typ] [char](1) NOT NULL,
	[mat_dflt_po_ga_typ] [char](1) NOT NULL,
	[mat_dflt_pcs_ctl] [numeric](1, 0) NOT NULL,
	[mat_jmy_alwd] [numeric](1, 0) NOT NULL,
	[mat_mktg_grp] [char](5) NULL,
	[mat_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
