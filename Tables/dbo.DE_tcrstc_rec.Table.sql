USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_tcrstc_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_tcrstc_rec](
	[stc_sts_typ] [char](1) NOT NULL,
	[stc_aplc_lvl] [char](1) NOT NULL,
	[stc_sts_actn] [char](1) NOT NULL,
	[stc_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[stc_desc30] [char](30) NOT NULL
) ON [PRIMARY]
GO
