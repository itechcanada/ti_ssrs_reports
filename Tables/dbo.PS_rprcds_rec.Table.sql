USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_rprcds_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_rprcds_rec](
	[cds_data_el_nm] [char](14) NOT NULL,
	[cds_lng] [char](2) NOT NULL,
	[cds_cd] [char](3) NOT NULL,
	[cds_strt_rcs_id] [numeric](10, 0) NOT NULL,
	[cds_desc_lgth] [numeric](2, 0) NOT NULL,
	[cds_desc] [char](30) NOT NULL,
	[cds_repos_sts] [char](1) NOT NULL,
	[cds_trnsd_sts] [char](1) NOT NULL,
	[cds_chg_lgn_id] [char](8) NOT NULL,
	[cds_chg_dt] [date] NOT NULL
) ON [PRIMARY]
GO
