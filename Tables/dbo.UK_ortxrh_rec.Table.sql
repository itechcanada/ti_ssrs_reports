USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_ortxrh_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_ortxrh_rec](
	[xrh_cmpy_id] [char](3) NOT NULL,
	[xrh_ord_pfx] [char](2) NOT NULL,
	[xrh_ord_no] [numeric](8, 0) NOT NULL,
	[xrh_ord_itm] [numeric](3, 0) NOT NULL,
	[xrh_ord_rls_no] [numeric](2, 0) NOT NULL,
	[xrh_sld_to_long_nm] [char](35) NOT NULL,
	[xrh_shp_to_long_nm] [char](35) NOT NULL
) ON [PRIMARY]
GO
