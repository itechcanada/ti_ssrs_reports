USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_accountCategory]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_accountCategory](
	[idAccountCategory] [int] NOT NULL,
	[accountCategoryShortName] [varchar](50) NULL,
	[accountCategoryLongName] [varchar](100) NULL,
	[companyID] [varchar](3) NULL,
 CONSTRAINT [PK_tbl_itech_accountCategory] PRIMARY KEY CLUSTERED 
(
	[idAccountCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
