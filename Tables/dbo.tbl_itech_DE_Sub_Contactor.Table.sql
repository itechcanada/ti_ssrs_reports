USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_DE_Sub_Contactor]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_DE_Sub_Contactor](
	[id] [int] NOT NULL,
	[contractor_cust_id] [text] NULL,
	[contractor_cust_nm] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
