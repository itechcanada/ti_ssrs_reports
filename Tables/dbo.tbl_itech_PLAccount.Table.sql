USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_PLAccount]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_PLAccount](
	[idPLAccount] [int] NOT NULL,
	[plBasicGLAccount] [varchar](50) NULL,
	[plAccountDescription] [varchar](150) NULL,
	[plAccountType] [varchar](50) NULL,
	[plAccountClass] [varchar](50) NULL,
	[plNormalDebitCredit] [varchar](50) NULL,
	[plSubAcctDistComb] [varchar](50) NULL,
	[plBR] [varchar](50) NULL,
	[plCustomerGroup] [varchar](150) NULL,
	[plTrend] [varchar](150) NULL,
	[plAccountCategoryID] [int] NOT NULL,
	[plCompanyID] [varchar](3) NULL,
	[orderInPlace] [int] NULL,
	[MultiplicationFactor] [varchar](5) NULL,
 CONSTRAINT [PK_tbl_itech_PLAccount] PRIMARY KEY CLUSTERED 
(
	[idPLAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
