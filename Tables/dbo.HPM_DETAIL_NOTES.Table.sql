USE [Stratix_US]
GO
/****** Object:  Table [dbo].[HPM_DETAIL_NOTES]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HPM_DETAIL_NOTES](
	[country] [nvarchar](50) NULL,
	[product] [nvarchar](255) NULL,
	[notes] [nvarchar](255) NULL
) ON [PRIMARY]
GO
