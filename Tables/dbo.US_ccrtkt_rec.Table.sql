USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_ccrtkt_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_ccrtkt_rec](
	[tkt_crm_tsk_typ] [char](3) NOT NULL,
	[tkt_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[tkt_desc30] [char](30) NOT NULL,
	[tkt_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
