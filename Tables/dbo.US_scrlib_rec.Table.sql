USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_scrlib_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_scrlib_rec](
	[lib_lib_id] [char](10) NOT NULL,
	[lib_lng] [char](2) NOT NULL,
	[lib_desc50] [char](50) NOT NULL,
	[lib_txt] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
