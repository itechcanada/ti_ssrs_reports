USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_iprpwc_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_iprpwc_rec](
	[pwc_cmpy_id] [char](3) NOT NULL,
	[pwc_whs] [char](3) NOT NULL,
	[pwc_pwc] [char](3) NOT NULL,
	[pwc_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[pwc_desc15] [char](15) NOT NULL,
	[pwc_pwg] [char](3) NOT NULL,
	[pwc_tm_ent_reqd] [numeric](1, 0) NOT NULL,
	[pwc_cons_loc] [char](7) NULL,
	[pwc_cons_min_idia] [numeric](9, 5) NOT NULL,
	[pwc_cons_max_idia] [numeric](9, 5) NOT NULL,
	[pwc_cons_min_odia] [numeric](9, 5) NOT NULL,
	[pwc_cons_max_odia] [numeric](9, 5) NOT NULL,
	[pwc_cons_min_ga] [numeric](8, 5) NOT NULL,
	[pwc_cons_max_ga] [numeric](8, 5) NOT NULL,
	[pwc_cons_max_wdth] [numeric](9, 4) NOT NULL,
	[pwc_cons_max_lgth] [numeric](9, 4) NOT NULL,
	[pwc_cons_min_id] [numeric](8, 4) NOT NULL,
	[pwc_cons_max_id] [numeric](8, 4) NOT NULL,
	[pwc_cons_max_od] [numeric](8, 4) NOT NULL,
	[pwc_cons_max_wgt] [numeric](10, 2) NOT NULL,
	[pwc_cons_max_hgt] [numeric](11, 4) NOT NULL,
	[pwc_prod_max_od] [numeric](8, 4) NOT NULL,
	[pwc_prod_max_hgt] [numeric](11, 4) NOT NULL,
	[pwc_max_arb_wdth] [numeric](11, 4) NOT NULL,
	[pwc_krf_dim] [numeric](9, 4) NOT NULL,
	[pwc_dflt_trim] [numeric](9, 4) NOT NULL,
	[pwc_drop_loc] [char](7) NULL,
	[pwc_fnsh_loc] [char](7) NULL,
	[pwc_scr_loc] [char](7) NULL,
	[pwc_std_scr_pct] [numeric](5, 2) NOT NULL,
	[pwc_stup_cst] [numeric](13, 4) NOT NULL,
	[pwc_stup_cst_um] [char](3) NULL,
	[pwc_run_cst] [numeric](13, 4) NOT NULL,
	[pwc_run_cst_um] [char](3) NULL,
	[pwc_trdn_cst] [numeric](13, 4) NOT NULL,
	[pwc_trdn_cst_um] [char](3) NULL,
	[pwc_dwntm_cst] [numeric](13, 4) NOT NULL,
	[pwc_dwntm_cst_um] [char](3) NULL,
	[pwc_osprs] [numeric](1, 0) NOT NULL,
	[pwc_incl_sttm_capy] [numeric](1, 0) NOT NULL,
	[pwc_consb_reqd] [numeric](1, 0) NOT NULL,
	[pwc_tot_mt_1] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_2] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_3] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_4] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_5] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_6] [numeric](5, 0) NOT NULL,
	[pwc_tot_mt_7] [numeric](5, 0) NOT NULL,
	[pwc_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
