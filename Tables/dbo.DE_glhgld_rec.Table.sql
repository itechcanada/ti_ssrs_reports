USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_glhgld_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_glhgld_rec](
	[gld_lgr_id] [char](3) NOT NULL,
	[gld_acctg_per] [numeric](6, 0) NOT NULL,
	[gld_ref_pfx] [char](2) NOT NULL,
	[gld_ref_no] [numeric](8, 0) NOT NULL,
	[gld_ref_itm] [numeric](3, 0) NOT NULL,
	[gld_ref_sbitm] [numeric](4, 0) NOT NULL,
	[gld_actvy_dt] [date] NOT NULL,
	[gld_dist_ln_no] [numeric](6, 0) NOT NULL,
	[gld_bsc_gl_acct] [char](8) NOT NULL,
	[gld_sacct] [char](30) NOT NULL,
	[gld_dr_amt] [numeric](12, 2) NOT NULL,
	[gld_cr_amt] [numeric](12, 2) NOT NULL,
	[gld_dist_rmk] [char](50) NOT NULL
) ON [PRIMARY]
GO
