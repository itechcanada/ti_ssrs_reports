USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_scrspv_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_scrspv_rec](
	[spv_cty] [char](3) NOT NULL,
	[spv_st_prov] [char](2) NOT NULL,
	[spv_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[spv_desc30] [char](30) NOT NULL,
	[spv_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
