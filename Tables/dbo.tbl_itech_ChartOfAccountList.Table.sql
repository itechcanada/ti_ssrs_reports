USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_ChartOfAccountList]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_ChartOfAccountList](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bsc_gl_acct] [nvarchar](10) NULL,
	[description] [nvarchar](50) NULL,
	[acct_type] [nvarchar](10) NULL,
	[acct_cls] [nvarchar](10) NULL,
	[normal_dr_cr] [nvarchar](10) NULL,
	[vldt_sacct_dist_comb] [nvarchar](10) NULL,
	[br_desc] [nvarchar](10) NULL,
	[pl_cat] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_itech_ChartOfAccountList] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
