USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_iptjso_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_iptjso_rec](
	[jso_cmpy_id] [char](3) NOT NULL,
	[jso_jbs_pfx] [char](2) NOT NULL,
	[jso_jbs_no] [numeric](8, 0) NOT NULL,
	[jso_ord_pfx] [char](2) NOT NULL,
	[jso_ord_no] [numeric](8, 0) NOT NULL,
	[jso_ord_itm] [numeric](3, 0) NOT NULL,
	[jso_ord_rls_no] [numeric](2, 0) NOT NULL,
	[jso_jso_seq_no] [numeric](8, 0) NOT NULL,
	[jso_ord_typ] [char](1) NOT NULL,
	[jso_rdy_by_ltts] [datetime2](7) NULL,
	[jso_rdy_by_ltms] [numeric](3, 0) NULL,
	[jso_dflt_shpg_whs] [char](3) NOT NULL,
	[jso_prod_pcs] [numeric](7, 0) NOT NULL,
	[jso_prod_msr] [numeric](16, 4) NOT NULL,
	[jso_prod_wgt] [numeric](10, 2) NOT NULL,
	[jso_prod_qty] [numeric](16, 4) NOT NULL,
	[jso_comp_pcs] [numeric](7, 0) NOT NULL,
	[jso_comp_msr] [numeric](16, 4) NOT NULL,
	[jso_comp_wgt] [numeric](10, 2) NOT NULL,
	[jso_comp_qty] [numeric](16, 4) NOT NULL,
	[jso_expct_pcs] [numeric](7, 0) NOT NULL,
	[jso_expct_msr] [numeric](16, 4) NOT NULL,
	[jso_expct_wgt] [numeric](10, 2) NOT NULL,
	[jso_expct_qty] [numeric](16, 4) NOT NULL,
	[jso_lst_job_pfx] [char](2) NOT NULL,
	[jso_lst_job_no] [numeric](8, 0) NOT NULL,
	[jso_lst_job_whs] [char](3) NOT NULL,
	[jso_lst_job_prs] [char](3) NOT NULL
) ON [PRIMARY]
GO
