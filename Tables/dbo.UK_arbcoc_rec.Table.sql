USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_arbcoc_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_arbcoc_rec](
	[coc_cmpy_id] [char](3) NOT NULL,
	[coc_cus_id] [char](8) NOT NULL,
	[coc_ar_bal] [numeric](15, 2) NOT NULL,
	[coc_uackd_bal] [numeric](15, 2) NOT NULL,
	[coc_frst_sls_dt] [date] NULL,
	[coc_lst_sls_dt] [date] NULL,
	[coc_dspt_bal] [numeric](15, 2) NOT NULL,
	[coc_lte_chrg_bal] [numeric](15, 2) NOT NULL,
	[coc_ovd_amt_dy] [numeric](18, 2) NOT NULL,
	[coc_most_ovd_dy] [numeric](4, 0) NOT NULL,
	[coc_csh_ip_amt] [numeric](12, 2) NOT NULL,
	[coc_lst_pmt_amt] [numeric](15, 2) NOT NULL,
	[coc_cry] [char](3) NULL,
	[coc_lst_pmt_dt] [date] NULL,
	[coc_hi_balamt] [numeric](13, 0) NOT NULL,
	[coc_hi_balamt_dt] [date] NULL
) ON [PRIMARY]
GO
