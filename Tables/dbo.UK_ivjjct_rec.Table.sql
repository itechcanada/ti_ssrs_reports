USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_ivjjct_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_ivjjct_rec](
	[jct_cmpy_id] [char](3) NOT NULL,
	[jct_ref_pfx] [char](2) NOT NULL,
	[jct_ref_no] [numeric](8, 0) NOT NULL,
	[jct_ref_itm] [numeric](3, 0) NOT NULL,
	[jct_upd_dt] [date] NOT NULL,
	[jct_tot_typ] [char](1) NOT NULL,
	[jct_tot_mtl_val] [numeric](15, 2) NOT NULL,
	[jct_tot_val] [numeric](15, 2) NOT NULL
) ON [PRIMARY]
GO
