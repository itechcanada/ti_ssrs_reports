USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_GetBooked_Shipment_GP_LBShipped_history]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_GetBooked_Shipment_GP_LBShipped_history](
	[UpdateDtTm] [datetime] NOT NULL,
	[item] [varchar](50) NULL,
	[Category] [varchar](150) NULL,
	[actvy_dt] [varchar](15) NULL,
	[Value] [decimal](20, 2) NULL,
	[Databases] [varchar](50) NULL
) ON [PRIMARY]
GO
