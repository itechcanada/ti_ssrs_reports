USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_cprchp_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_cprchp_rec](
	[chp_cmpy_id] [char](3) NOT NULL,
	[chp_part_sts] [char](1) NOT NULL,
	[chp_part_ctl_no] [numeric](10, 0) NOT NULL,
	[chp_chrg_ln_no] [numeric](2, 0) NOT NULL,
	[chp_chrg_no] [numeric](3, 0) NOT NULL,
	[chp_cc_seq_no] [numeric](2, 0) NOT NULL,
	[chp_chrg_desc20] [char](20) NOT NULL,
	[chp_chrg_cl] [char](1) NOT NULL,
	[chp_chrg] [numeric](13, 4) NOT NULL,
	[chp_chrg_um] [char](3) NOT NULL,
	[chp_disc] [numeric](13, 4) NOT NULL,
	[chp_disc_um] [char](3) NULL,
	[chp_chrg_orig] [char](1) NOT NULL
) ON [PRIMARY]
GO
