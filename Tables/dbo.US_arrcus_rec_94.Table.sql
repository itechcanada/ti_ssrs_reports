USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_arrcus_rec_94]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_arrcus_rec_94](
	[cus_cmpy_id] [char](3) NOT NULL,
	[cus_cus_id] [char](8) NOT NULL,
	[cus_cus_nm] [char](15) NOT NULL,
	[cus_cus_long_nm] [char](35) NOT NULL,
	[cus_cus_acct_typ] [char](1) NOT NULL,
	[cus_cry] [char](3) NOT NULL,
	[cus_admin_brh] [char](3) NOT NULL,
	[cus_sic] [char](8) NULL,
	[cus_web_site] [char](50) NOT NULL,
	[cus_lng] [char](2) NOT NULL,
	[cus_cus_cat] [char](2) NULL,
	[cus_prnt_org] [char](6) NULL,
	[cus_rltnshp] [char](1) NULL,
	[cus_sls_area] [char](3) NULL,
	[cus_rqst_pttrm] [numeric](3, 0) NULL,
	[cus_rqst_disc_trm] [numeric](2, 0) NULL,
	[cus_upd_dtts] [datetime2](7) NULL,
	[cus_upd_dtms] [numeric](3, 0) NULL,
	[cus_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
