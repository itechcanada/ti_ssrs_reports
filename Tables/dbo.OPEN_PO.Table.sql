USE [Stratix_US]
GO
/****** Object:  Table [dbo].[OPEN_PO]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OPEN_PO](
	[country] [varchar](10) NULL,
	[type] [varchar](10) NULL,
	[product] [varchar](50) NULL,
	[po] [float] NULL
) ON [PRIMARY]
GO
