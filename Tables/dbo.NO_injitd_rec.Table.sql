USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_injitd_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_injitd_rec](
	[itd_cmpy_id] [char](3) NOT NULL,
	[itd_ref_pfx] [char](2) NOT NULL,
	[itd_ref_no] [numeric](8, 0) NOT NULL,
	[itd_ref_itm] [numeric](3, 0) NOT NULL,
	[itd_ref_sbitm] [numeric](4, 0) NOT NULL,
	[itd_actvy_dt] [date] NOT NULL,
	[itd_trs_seq_no] [numeric](8, 0) NOT NULL,
	[itd_prnt_pfx] [char](2) NOT NULL,
	[itd_prnt_no] [numeric](8, 0) NOT NULL,
	[itd_prnt_itm] [numeric](3, 0) NOT NULL,
	[itd_prnt_sitm] [numeric](2, 0) NOT NULL,
	[itd_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[itd_brh] [char](3) NOT NULL,
	[itd_frm] [char](6) NOT NULL,
	[itd_grd] [char](8) NOT NULL,
	[itd_size] [char](15) NOT NULL,
	[itd_fnsh] [char](8) NOT NULL,
	[itd_ef_svar] [char](30) NOT NULL,
	[itd_idia] [numeric](9, 5) NOT NULL,
	[itd_odia] [numeric](9, 5) NOT NULL,
	[itd_ga_size] [numeric](8, 5) NOT NULL,
	[itd_ga_typ] [char](1) NOT NULL,
	[itd_wdth] [numeric](9, 4) NOT NULL,
	[itd_lgth] [numeric](9, 4) NOT NULL,
	[itd_dim_dsgn] [char](1) NOT NULL,
	[itd_rdm_dim_1] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_2] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_3] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_4] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_5] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_6] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_7] [numeric](9, 4) NOT NULL,
	[itd_rdm_dim_8] [numeric](9, 4) NOT NULL,
	[itd_tag_no] [char](12) NOT NULL,
	[itd_lbl_idfr] [char](13) NOT NULL,
	[itd_whs] [char](3) NOT NULL,
	[itd_loc] [char](7) NULL,
	[itd_mill] [char](3) NULL,
	[itd_heat] [char](15) NULL,
	[itd_qds_ctl_no] [numeric](10, 0) NOT NULL,
	[itd_ownr] [char](1) NOT NULL,
	[itd_ownr_ref_id] [char](8) NOT NULL,
	[itd_ownr_tag_no] [char](15) NOT NULL,
	[itd_bgt_for] [char](1) NOT NULL,
	[itd_bgt_for_id] [char](38) NOT NULL,
	[itd_invt_typ] [char](1) NOT NULL,
	[itd_invt_sts] [char](1) NOT NULL,
	[itd_invt_qlty] [char](1) NULL,
	[itd_invt_cat] [char](1) NOT NULL,
	[itd_orig_zn] [char](3) NULL,
	[itd_ord_ffm] [char](15) NOT NULL,
	[itd_upd_dtts] [datetime2](7) NULL,
	[itd_upd_dtms] [numeric](3, 0) NULL,
	[itd_prod_for] [numeric](1, 0) NOT NULL,
	[itd_part_cus_id] [char](8) NULL,
	[itd_part] [char](30) NOT NULL,
	[itd_part_rev_no] [char](2) NOT NULL,
	[itd_part_acs] [char](1) NOT NULL,
	[itd_cst_qty_indc] [numeric](1, 0) NOT NULL,
	[itd_prod_indc] [numeric](1, 0) NOT NULL,
	[itd_prs_parm_indc] [numeric](1, 0) NOT NULL,
	[itd_instr_indc] [numeric](1, 0) NOT NULL,
	[itd_hld_indc] [numeric](1, 0) NOT NULL,
	[itd_cond_indc] [numeric](1, 0) NOT NULL,
	[itd_atchmt_indc] [numeric](1, 0) NOT NULL,
	[itd_src_tag_indc] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
