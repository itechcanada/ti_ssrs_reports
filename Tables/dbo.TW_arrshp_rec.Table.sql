USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_arrshp_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_arrshp_rec](
	[shp_cmpy_id] [char](3) NOT NULL,
	[shp_cus_id] [char](8) NOT NULL,
	[shp_shp_to] [numeric](3, 0) NOT NULL,
	[shp_shp_to_nm] [char](15) NOT NULL,
	[shp_shp_to_long_nm] [char](35) NOT NULL,
	[shp_admin_brh] [char](3) NOT NULL,
	[shp_ter] [char](4) NULL,
	[shp_is_slp] [char](4) NOT NULL,
	[shp_os_slp] [char](4) NOT NULL,
	[shp_lng] [char](2) NOT NULL,
	[shp_dlvy_fm_hr] [numeric](2, 0) NOT NULL,
	[shp_dlvy_fm_mt] [numeric](2, 0) NOT NULL,
	[shp_dlvy_to_hr] [numeric](2, 0) NOT NULL,
	[shp_dlvy_to_mt] [numeric](2, 0) NOT NULL,
	[shp_blnd_shpt_mthd] [char](1) NOT NULL,
	[shp_shp_itm_tgth] [numeric](1, 0) NOT NULL,
	[shp_alw_prtl_shp] [numeric](1, 0) NOT NULL,
	[shp_ovrshp_pct] [numeric](5, 2) NOT NULL,
	[shp_undshp_pct] [numeric](5, 2) NOT NULL,
	[shp_shpg_tag_pfx] [char](2) NOT NULL,
	[shp_tr_wgt_reqd] [numeric](1, 0) NOT NULL,
	[shp_shpg_wgt_mthd] [char](1) NOT NULL,
	[shp_aiag_suplr] [char](11) NOT NULL,
	[shp_chrg_qty_typ] [char](1) NOT NULL,
	[shp_wgt_mult] [char](1) NOT NULL,
	[shp_wgt_mult_pct] [numeric](6, 4) NOT NULL,
	[shp_transp_dlvy] [char](3) NULL,
	[shp_mult_cus_po] [numeric](1, 0) NOT NULL,
	[shp_prt_pcs_lbl] [numeric](1, 0) NOT NULL,
	[shp_prt_blg_lbl] [numeric](1, 0) NOT NULL,
	[shp_prt_wgt_lbl] [numeric](1, 0) NOT NULL,
	[shp_prt_pk_list] [numeric](1, 0) NOT NULL,
	[shp_max_lift_wgt] [numeric](10, 2) NOT NULL,
	[shp_bill_skd_wgt] [numeric](1, 0) NOT NULL,
	[shp_upd_dtts] [datetime2](7) NULL,
	[shp_upd_dtms] [numeric](3, 0) NULL,
	[shp_prt_stmt_orig] [numeric](1, 0) NOT NULL,
	[shp_ent_msr] [char](1) NOT NULL,
	[shp_aplc_bns] [numeric](1, 0) NOT NULL,
	[shp_misc_shp_to] [numeric](1, 0) NOT NULL,
	[shp_dun_no] [numeric](9, 0) NOT NULL,
	[shp_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
