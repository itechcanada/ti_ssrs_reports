USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_page_content]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_page_content](
	[ContentID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
	[ContentPlaceHolderID] [int] NOT NULL,
	[ContentDescEn] [text] NULL,
	[ContentDescFr] [text] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_page_content] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
