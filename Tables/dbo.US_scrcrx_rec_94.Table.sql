USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_scrcrx_rec_94]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_scrcrx_rec_94](
	[crx_cmpy_id] [char](3) NOT NULL,
	[crx_orig_cry] [char](3) NOT NULL,
	[crx_eqv_cry] [char](3) NOT NULL,
	[crx_xexrt] [numeric](13, 8) NOT NULL
) ON [PRIMARY]
GO
