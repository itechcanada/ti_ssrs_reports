USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_management_account_20211001]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_management_account_20211001](
	[id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NULL,
	[order] [int] NOT NULL,
	[DataBaseName] [varchar](10) NULL
) ON [PRIMARY]
GO
