USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_AM_HISTORY_PREVIOUS]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_AM_HISTORY_PREVIOUS](
	[UpdDtTm] [varchar](23) NOT NULL,
	[Databases] [varchar](15) NULL,
	[Branch] [varchar](20) NULL,
	[ShipmentsAvgDaily] [decimal](38, 6) NULL,
	[PrvMonthShipmentsAvgDaily] [decimal](38, 6) NULL,
	[PrvMonthShipmentsMTD] [decimal](38, 1) NOT NULL,
	[ShipmentsMTD] [decimal](38, 1) NOT NULL,
	[Budget] [decimal](38, 1) NOT NULL,
	[Forecast] [decimal](38, 1) NOT NULL,
	[PrvMonthGP$MTD] [decimal](38, 1) NOT NULL,
	[GP$MTD] [decimal](38, 1) NOT NULL,
	[GPBudget] [decimal](38, 1) NOT NULL,
	[GPForecast] [decimal](38, 1) NOT NULL,
	[GPPctMTD] [decimal](38, 6) NULL,
	[GPPctBdgYear] [decimal](38, 6) NULL,
	[GPPctForecast] [decimal](38, 1) NOT NULL,
	[PrvMonthBookingAvgDaily] [decimal](38, 1) NOT NULL,
	[PrvMonthBookingMTD] [decimal](38, 1) NOT NULL,
	[BookingAvgDaily] [decimal](38, 1) NOT NULL,
	[BookingMTD] [decimal](38, 1) NOT NULL,
	[TotalOpenOrders] [decimal](38, 1) NOT NULL,
	[PrvMonthTotalOpenOrders] [decimal](38, 1) NOT NULL,
	[PrvMonthOpenOrdersMTD] [decimal](38, 1) NOT NULL,
	[PrvMonthMTDOrdersCount] [decimal](38, 1) NOT NULL,
	[OpenOrdersMTD] [decimal](38, 1) NOT NULL,
	[MTDOrdersCount] [decimal](38, 1) NOT NULL,
	[PrvMonthLBShippedMTD] [decimal](38, 1) NOT NULL,
	[PrvMonthLBSShippedPlan] [decimal](38, 1) NOT NULL,
	[LBShippedMTD] [decimal](38, 1) NOT NULL,
	[LBSShippedPlan] [decimal](38, 1) NOT NULL,
	[PctShippedPlan] [decimal](38, 6) NULL,
	[Inventory] [decimal](38, 1) NOT NULL,
	[PrvMonthInventory] [decimal](38, 1) NOT NULL,
	[PrvMonthWarehseFees] [decimal](38, 1) NOT NULL,
	[WarehseFees] [decimal](38, 1) NOT NULL,
	[GPPctFeeMTD] [decimal](38, 6) NULL
) ON [PRIMARY]
GO
