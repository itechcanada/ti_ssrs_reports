USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_perppb_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_perppb_rec](
	[ppb_cmpy_id] [char](3) NOT NULL,
	[ppb_brh] [char](3) NOT NULL,
	[ppb_frm] [char](6) NOT NULL,
	[ppb_grd] [char](8) NOT NULL,
	[ppb_size] [char](15) NOT NULL,
	[ppb_fnsh] [char](8) NOT NULL,
	[ppb_wdth_fm] [numeric](9, 4) NOT NULL,
	[ppb_wdth_to] [numeric](9, 4) NOT NULL,
	[ppb_lgth_fm] [numeric](9, 4) NOT NULL,
	[ppb_lgth_to] [numeric](9, 4) NOT NULL,
	[ppb_repl_cst] [numeric](13, 4) NOT NULL,
	[ppb_repl_cst_um] [char](3) NOT NULL,
	[ppb_rct_expy_dt] [date] NULL,
	[ppb_frt_in_cst] [numeric](13, 4) NOT NULL,
	[ppb_frt_in_cst_um] [char](3) NOT NULL,
	[ppb_shw_avg_cst] [numeric](1, 0) NOT NULL,
	[ppb_next_repl_cst] [numeric](13, 4) NOT NULL,
	[ppb_nrct_expy_dt] [date] NULL,
	[ppb_bas_prc] [numeric](13, 4) NOT NULL,
	[ppb_bas_prc_um] [char](3) NOT NULL,
	[ppb_bpr_expy_dt] [date] NULL,
	[ppb_next_bas_prc] [numeric](13, 4) NOT NULL,
	[ppb_nbpr_expy_dt] [date] NULL,
	[ppb_lib_id] [char](10) NULL,
	[ppb_lng] [char](2) NOT NULL
) ON [PRIMARY]
GO
