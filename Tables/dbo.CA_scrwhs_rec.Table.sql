USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_scrwhs_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_scrwhs_rec](
	[whs_cmpy_id] [char](3) NOT NULL,
	[whs_whs] [char](3) NOT NULL,
	[whs_whs_nm] [char](15) NOT NULL,
	[whs_mng_brh] [char](3) NOT NULL,
	[whs_nm1] [char](35) NOT NULL,
	[whs_nm2] [char](35) NOT NULL,
	[whs_addr1] [char](35) NOT NULL,
	[whs_addr2] [char](35) NOT NULL,
	[whs_addr3] [char](35) NOT NULL,
	[whs_city] [char](35) NOT NULL,
	[whs_pcd] [char](10) NOT NULL,
	[whs_latd] [numeric](11, 8) NOT NULL,
	[whs_lngtd] [numeric](11, 8) NOT NULL,
	[whs_cty] [char](3) NOT NULL,
	[whs_st_prov] [char](2) NULL,
	[whs_transp_zn] [char](6) NOT NULL,
	[whs_tel_area_cd] [char](7) NOT NULL,
	[whs_tel_no] [char](15) NOT NULL,
	[whs_tel_ext] [char](5) NOT NULL,
	[whs_fax_area_cd] [char](7) NOT NULL,
	[whs_fax_no] [char](15) NOT NULL,
	[whs_fax_ext] [char](5) NOT NULL,
	[whs_fob] [char](30) NOT NULL,
	[whs_whs_typ] [char](1) NOT NULL,
	[whs_ownr] [char](1) NOT NULL,
	[whs_lng] [char](2) NOT NULL,
	[whs_tmzn] [char](5) NOT NULL,
	[whs_trnst_whs_indc] [numeric](1, 0) NOT NULL,
	[whs_dun_no] [numeric](9, 0) NOT NULL,
	[whs_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
