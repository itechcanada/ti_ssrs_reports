USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_ivjjcl_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_ivjjcl_rec](
	[jcl_cmpy_id] [char](3) NOT NULL,
	[jcl_ref_pfx] [char](2) NOT NULL,
	[jcl_ref_no] [numeric](8, 0) NOT NULL,
	[jcl_ref_itm] [numeric](3, 0) NOT NULL,
	[jcl_upd_dt] [date] NOT NULL,
	[jcl_ref_ln_no] [numeric](2, 0) NOT NULL,
	[jcl_chrg_no] [numeric](3, 0) NOT NULL,
	[jcl_chrg_desc20] [char](20) NOT NULL,
	[jcl_cc_seq_no] [numeric](2, 0) NOT NULL,
	[jcl_cc_typ] [numeric](1, 0) NOT NULL,
	[jcl_chrg_cl] [char](1) NOT NULL,
	[jcl_prs_chrg] [numeric](1, 0) NOT NULL,
	[jcl_chrg_orig] [char](1) NOT NULL,
	[jcl_chrg_qty] [numeric](16, 4) NOT NULL,
	[jcl_chrg_qty_um] [char](3) NULL,
	[jcl_chrg] [numeric](13, 4) NOT NULL,
	[jcl_chrg_um] [char](3) NULL,
	[jcl_chrg_val] [numeric](12, 2) NOT NULL,
	[jcl_disc] [numeric](13, 4) NOT NULL,
	[jcl_disc_um] [char](3) NULL,
	[jcl_disc_val] [numeric](12, 2) NOT NULL,
	[jcl_net_chrg_val] [numeric](12, 2) NOT NULL,
	[jcl_ovrd_chrg] [numeric](1, 0) NOT NULL,
	[jcl_cst_ref_ln_no] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
