USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_SalesPerson]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_SalesPerson](
	[idSalesPerson] [int] IDENTITY(1,1) NOT NULL,
	[salesPerson_ShortName] [varchar](4) NOT NULL,
	[salesPerson_LoginID] [varchar](8) NULL,
	[salesPerson_Branch] [varchar](3) NULL,
	[isActive] [char](1) NOT NULL
) ON [PRIMARY]
GO
