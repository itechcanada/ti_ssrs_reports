USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_scrdtr_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_scrdtr_rec](
	[dtr_disc_trm] [numeric](2, 0) NOT NULL,
	[dtr_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[dtr_desc30] [char](30) NOT NULL,
	[dtr_disc_trm_typ] [char](3) NOT NULL,
	[dtr_disc_nbr_mth] [numeric](2, 0) NOT NULL,
	[dtr_disc_nbr_dy] [numeric](3, 0) NOT NULL,
	[dtr_disc_cut_dy] [numeric](2, 0) NOT NULL,
	[dtr_disc_dy_1] [numeric](2, 0) NOT NULL,
	[dtr_disc_dy_2] [numeric](2, 0) NOT NULL,
	[dtr_disc_rt] [numeric](5, 2) NOT NULL,
	[dtr_actvy_per] [char](3) NOT NULL,
	[dtr_actvy_dy] [numeric](2, 0) NOT NULL,
	[dtr_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
