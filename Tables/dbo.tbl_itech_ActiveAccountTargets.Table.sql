USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_ActiveAccountTargets]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_ActiveAccountTargets](
	[idActiveAccountTargets] [int] IDENTITY(1,1) NOT NULL,
	[databaseName] [varchar](2) NULL,
	[branchName] [varchar](3) NOT NULL,
	[targetYear] [int] NOT NULL,
	[janTarget] [int] NOT NULL,
	[febTarget] [int] NOT NULL,
	[marTarget] [int] NOT NULL,
	[aprTarget] [int] NOT NULL,
	[mayTarget] [int] NOT NULL,
	[junTarget] [int] NOT NULL,
	[julTarget] [int] NOT NULL,
	[augTarget] [int] NOT NULL,
	[sepTarget] [int] NOT NULL,
	[octTarget] [int] NOT NULL,
	[novTarget] [int] NOT NULL,
	[decTarget] [int] NOT NULL
) ON [PRIMARY]
GO
