USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_cttcsi_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_cttcsi_rec](
	[csi_cmpy_id] [char](3) NOT NULL,
	[csi_ref_pfx] [char](2) NOT NULL,
	[csi_ref_no] [numeric](8, 0) NOT NULL,
	[csi_ref_itm] [numeric](3, 0) NOT NULL,
	[csi_ref_sbitm] [numeric](4, 0) NOT NULL,
	[csi_aplc_pfx] [char](2) NOT NULL,
	[csi_aplc_no] [numeric](8, 0) NOT NULL,
	[csi_aplc_itm] [numeric](3, 0) NOT NULL,
	[csi_aplc_sbitm] [numeric](4, 0) NOT NULL,
	[csi_ref_ln_no] [numeric](2, 0) NOT NULL,
	[csi_cst_no] [numeric](3, 0) NOT NULL,
	[csi_cst_desc20] [char](20) NOT NULL,
	[csi_cst_src_pfx] [char](2) NOT NULL,
	[csi_cst_cl] [char](1) NOT NULL,
	[csi_cc_typ] [numeric](1, 0) NOT NULL,
	[csi_cst_orig] [char](1) NOT NULL,
	[csi_cst] [numeric](13, 4) NOT NULL,
	[csi_cst_um] [char](3) NULL,
	[csi_cst_qty] [numeric](16, 4) NOT NULL,
	[csi_cst_qty_um] [char](3) NULL,
	[csi_cst_qty_typ] [char](1) NOT NULL,
	[csi_cry] [char](3) NOT NULL,
	[csi_exrt] [numeric](13, 8) NOT NULL,
	[csi_ex_rt_typ] [char](1) NOT NULL,
	[csi_comp_cst_val] [numeric](12, 2) NOT NULL,
	[csi_bas_cry_val] [numeric](12, 2) NOT NULL,
	[csi_ven_cry_val] [numeric](12, 2) NOT NULL,
	[csi_ovrd_cst] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
