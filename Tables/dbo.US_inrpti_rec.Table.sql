USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_inrpti_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_inrpti_rec](
	[pti_frm] [char](6) NOT NULL,
	[pti_grd] [char](8) NOT NULL,
	[pti_size] [char](15) NOT NULL,
	[pti_fnsh] [char](8) NOT NULL,
	[pti_apln] [char](2) NOT NULL,
	[pti_rmk_cl] [char](1) NOT NULL,
	[pti_rmk_typ] [char](2) NOT NULL,
	[pti_lng] [char](2) NOT NULL,
	[pti_lib_id] [char](10) NULL,
	[pti_txt] [text] NULL,
	[pti_rmk_expy_dt] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
