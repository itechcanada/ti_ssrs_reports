USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_trjiri_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_trjiri_rec](
	[iri_cmpy_id] [char](3) NOT NULL,
	[iri_ref_pfx] [char](2) NOT NULL,
	[iri_ref_no] [numeric](8, 0) NOT NULL,
	[iri_ref_itm] [numeric](3, 0) NOT NULL,
	[iri_actvy_dt] [date] NOT NULL,
	[iri_pur_cat] [char](2) NULL,
	[iri_sls_cat] [char](2) NULL,
	[iri_prnt_pfx] [char](2) NOT NULL,
	[iri_prnt_no] [numeric](8, 0) NOT NULL,
	[iri_prnt_itm] [numeric](3, 0) NOT NULL,
	[iri_prnt_sitm] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
