USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_potpoh_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_potpoh_rec](
	[poh_cmpy_id] [char](3) NOT NULL,
	[poh_po_pfx] [char](2) NOT NULL,
	[poh_po_no] [numeric](8, 0) NOT NULL,
	[poh_po_brh] [char](3) NOT NULL,
	[poh_ven_id] [char](8) NOT NULL,
	[poh_shp_fm] [numeric](3, 0) NOT NULL,
	[poh_mill] [char](3) NULL,
	[poh_po_typ] [char](1) NOT NULL,
	[poh_inv_ml_to_brh] [char](3) NOT NULL,
	[poh_pur_pttrm] [numeric](3, 0) NOT NULL,
	[poh_pur_disc_trm] [numeric](2, 0) NULL,
	[poh_byr] [char](4) NOT NULL,
	[poh_expd] [char](4) NOT NULL,
	[poh_cry] [char](3) NOT NULL,
	[poh_exrt] [numeric](13, 8) NOT NULL,
	[poh_ex_rt_typ] [char](1) NOT NULL,
	[poh_shp_itm_tgth] [numeric](1, 0) NOT NULL,
	[poh_ack_reqd] [numeric](1, 0) NOT NULL,
	[poh_po_pl_dt] [date] NOT NULL,
	[poh_tot_opn_wgt] [numeric](12, 2) NOT NULL,
	[poh_tot_cls_wgt] [numeric](12, 2) NOT NULL,
	[poh_tot_rcvd_wgt] [numeric](12, 2) NOT NULL,
	[poh_apvd_val] [numeric](15, 2) NOT NULL,
	[poh_opn_itm] [numeric](3, 0) NOT NULL,
	[poh_cls_itm] [numeric](3, 0) NOT NULL,
	[poh_crtd_lgn_id] [char](8) NOT NULL
) ON [PRIMARY]
GO
