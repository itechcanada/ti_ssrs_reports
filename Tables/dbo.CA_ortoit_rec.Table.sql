USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_ortoit_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_ortoit_rec](
	[oit_cmpy_id] [char](3) NOT NULL,
	[oit_ref_pfx] [char](2) NOT NULL,
	[oit_ref_no] [numeric](8, 0) NOT NULL,
	[oit_ref_itm] [numeric](3, 0) NOT NULL,
	[oit_mtl_avg_val] [numeric](15, 2) NOT NULL,
	[oit_mtl_repl_val] [numeric](15, 2) NOT NULL,
	[oit_tot_avg_val] [numeric](15, 2) NOT NULL,
	[oit_tot_repl_val] [numeric](15, 2) NOT NULL,
	[oit_mpft_avg_val] [numeric](15, 2) NOT NULL,
	[oit_mpft_repl_val] [numeric](15, 2) NOT NULL,
	[oit_npft_avg_val] [numeric](15, 2) NOT NULL,
	[oit_npft_repl_val] [numeric](15, 2) NOT NULL,
	[oit_mpft_avg_pct] [numeric](5, 2) NOT NULL,
	[oit_mpft_repl_pct] [numeric](5, 2) NOT NULL,
	[oit_npft_avg_pct] [numeric](5, 2) NOT NULL,
	[oit_npft_repl_pct] [numeric](5, 2) NOT NULL,
	[oit_tot_tx_val] [numeric](15, 2) NOT NULL,
	[oit_trm_disc_dt] [date] NULL,
	[oit_trm_disc_val] [numeric](15, 2) NOT NULL,
	[oit_orig_ord_val] [numeric](15, 2) NOT NULL,
	[oit_orig_ord_wgt] [numeric](12, 2) NOT NULL,
	[oit_shp_val] [numeric](15, 2) NOT NULL
) ON [PRIMARY]
GO
