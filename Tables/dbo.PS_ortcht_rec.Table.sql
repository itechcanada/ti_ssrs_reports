USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_ortcht_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_ortcht_rec](
	[cht_cmpy_id] [char](3) NOT NULL,
	[cht_ref_pfx] [char](2) NOT NULL,
	[cht_ref_no] [numeric](8, 0) NOT NULL,
	[cht_ref_itm] [numeric](3, 0) NOT NULL,
	[cht_tot_typ] [char](1) NOT NULL,
	[cht_tot_mtl_val] [numeric](15, 2) NOT NULL,
	[cht_tot_val] [numeric](15, 2) NOT NULL
) ON [PRIMARY]
GO
