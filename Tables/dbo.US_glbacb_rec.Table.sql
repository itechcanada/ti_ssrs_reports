USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_glbacb_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_glbacb_rec](
	[acb_lgr_id] [char](3) NOT NULL,
	[acb_bsc_gl_acct] [char](8) NOT NULL,
	[acb_sacct] [char](30) NOT NULL,
	[acb_fis_yr] [numeric](4, 0) NOT NULL,
	[acb_bgn_dr_amt] [numeric](12, 2) NOT NULL,
	[acb_bgn_cr_amt] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_1] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_2] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_3] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_4] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_5] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_6] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_7] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_8] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_9] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_10] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_11] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_12] [numeric](12, 2) NOT NULL,
	[acb_dr_amt_13] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_1] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_2] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_3] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_4] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_5] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_6] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_7] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_8] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_9] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_10] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_11] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_12] [numeric](12, 2) NOT NULL,
	[acb_cr_amt_13] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
