USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Admin]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Admin](
	[id] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[EmailID] [varchar](100) NULL,
	[Password] [varchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
