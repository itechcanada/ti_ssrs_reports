USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_intpcr_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_intpcr_rec](
	[pcr_cmpy_id] [char](3) NOT NULL,
	[pcr_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[pcr_mill] [char](3) NULL,
	[pcr_heat] [char](15) NULL,
	[pcr_orig_zn] [char](3) NULL,
	[pcr_crtd_ref_pfx] [char](2) NOT NULL,
	[pcr_crtd_ref_no] [numeric](8, 0) NOT NULL,
	[pcr_crtd_ref_itm] [numeric](3, 0) NOT NULL,
	[pcr_crtd_ref_sbitm] [numeric](4, 0) NOT NULL,
	[pcr_crtd_dtts] [datetime2](7) NULL,
	[pcr_crtd_dtms] [numeric](3, 0) NULL,
	[pcr_agng_dtts] [datetime2](7) NULL,
	[pcr_agng_dtms] [numeric](3, 0) NULL,
	[pcr_crtd_invt_qty] [numeric](16, 4) NOT NULL,
	[pcr_crtd_shpnt_qty] [numeric](16, 4) NOT NULL,
	[pcr_crtd_lnd_cst] [numeric](13, 4) NOT NULL,
	[pcr_mill_id] [char](15) NOT NULL,
	[pcr_bgt_as_msr] [char](1) NOT NULL,
	[pcr_ven_id] [char](8) NULL,
	[pcr_ven_ref] [char](22) NOT NULL,
	[pcr_po_pfx] [char](2) NOT NULL,
	[pcr_po_no] [numeric](8, 0) NOT NULL,
	[pcr_po_itm] [numeric](3, 0) NOT NULL,
	[pcr_po_sitm] [numeric](2, 0) NOT NULL,
	[pcr_cum_scr_wgt] [numeric](10, 2) NOT NULL,
	[pcr_blg_wdth] [numeric](9, 4) NOT NULL,
	[pcr_rcvd_tr_wgt] [numeric](5, 0) NOT NULL,
	[pcr_mill_ord_no] [char](25) NOT NULL,
	[pcr_qds_ctl_no] [numeric](10, 0) NOT NULL,
	[pcr_qds_ref_pfx] [char](2) NOT NULL,
	[pcr_qds_ref_no] [numeric](8, 0) NOT NULL,
	[pcr_qds_ref_itm] [numeric](3, 0) NOT NULL,
	[pcr_qds_ref_sbitm] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO
