USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_injith_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_injith_rec](
	[ith_cmpy_id] [char](3) NOT NULL,
	[ith_ref_pfx] [char](2) NOT NULL,
	[ith_ref_no] [numeric](8, 0) NOT NULL,
	[ith_ref_itm] [numeric](3, 0) NOT NULL,
	[ith_ref_sbitm] [numeric](4, 0) NOT NULL,
	[ith_actvy_dt] [date] NOT NULL,
	[ith_actvy_whs] [char](3) NOT NULL,
	[ith_actvy_lgn_id] [char](8) NOT NULL,
	[ith_upd_dtts] [datetime2](7) NULL,
	[ith_upd_dtms] [numeric](3, 0) NULL,
	[ith_upd_lgn_id] [char](8) NOT NULL,
	[ith_upd_comp] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
