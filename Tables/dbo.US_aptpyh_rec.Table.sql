USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_aptpyh_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_aptpyh_rec](
	[pyh_cmpy_id] [char](3) NOT NULL,
	[pyh_ap_pfx] [char](2) NOT NULL,
	[pyh_ap_no] [numeric](8, 0) NOT NULL,
	[pyh_ven_id] [char](8) NOT NULL,
	[pyh_ven_inv_no] [char](22) NOT NULL,
	[pyh_ven_inv_dt] [date] NOT NULL,
	[pyh_extl_ref] [char](22) NOT NULL,
	[pyh_desc30] [char](30) NOT NULL,
	[pyh_po_pfx] [char](2) NULL,
	[pyh_po_no] [numeric](8, 0) NOT NULL,
	[pyh_po_itm] [numeric](3, 0) NOT NULL,
	[pyh_cry] [char](3) NOT NULL,
	[pyh_exrt] [numeric](13, 8) NOT NULL,
	[pyh_ex_rt_typ] [char](1) NOT NULL,
	[pyh_pttrm] [numeric](3, 0) NOT NULL,
	[pyh_disc_trm] [numeric](2, 0) NULL,
	[pyh_due_nbr_dy] [numeric](3, 0) NOT NULL,
	[pyh_disc_nbr_dy] [numeric](3, 0) NOT NULL,
	[pyh_disc_rt] [numeric](5, 2) NOT NULL,
	[pyh_disc_dt] [date] NULL,
	[pyh_due_dt] [date] NOT NULL,
	[pyh_disc_amt] [numeric](12, 2) NOT NULL,
	[pyh_opa_pfx] [char](2) NOT NULL,
	[pyh_opa_no] [numeric](8, 0) NOT NULL,
	[pyh_ap_brh] [char](3) NOT NULL,
	[pyh_ent_dt] [date] NULL,
	[pyh_chk_itm_rmk] [char](50) NOT NULL,
	[pyh_pmt_typ] [char](1) NOT NULL,
	[pyh_vchr_xref_pfx] [char](2) NULL,
	[pyh_vchr_xref_no] [numeric](8, 0) NOT NULL,
	[pyh_orig_amt] [numeric](12, 2) NOT NULL,
	[pyh_ip_amt] [numeric](12, 2) NOT NULL,
	[pyh_balamt] [numeric](12, 2) NOT NULL,
	[pyh_arch_trs] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
