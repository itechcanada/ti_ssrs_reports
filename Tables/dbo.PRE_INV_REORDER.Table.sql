USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PRE_INV_REORDER]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRE_INV_REORDER](
	[type] [varchar](10) NULL,
	[title] [varchar](50) NULL,
	[product] [varchar](50) NULL,
	[rating] [varchar](20) NULL,
	[needed] [varchar](1) NULL,
	[stock_qty] [int] NULL,
	[reorder_point] [int] NULL,
	[input] [varchar](30) NULL,
	[vendor] [varchar](50) NULL,
	[comment] [nvarchar](255) NULL
) ON [PRIMARY]
GO
