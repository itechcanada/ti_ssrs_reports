USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_intercoCustomers]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_intercoCustomers](
	[idIntercoCustomer] [int] NOT NULL,
	[companyID] [varchar](50) NOT NULL,
	[customerID] [varchar](50) NOT NULL,
	[description] [varchar](100) NULL,
	[isActive] [char](1) NULL,
 CONSTRAINT [PK_tbl_itech_intercoCustomers] PRIMARY KEY CLUSTERED 
(
	[idIntercoCustomer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
