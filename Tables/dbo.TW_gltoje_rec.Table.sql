USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_gltoje_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_gltoje_rec](
	[oje_oje_pfx] [char](2) NOT NULL,
	[oje_oje_no] [numeric](8, 0) NOT NULL,
	[oje_jrnl_ent_typ] [char](1) NOT NULL,
	[oje_actvy_dt] [date] NOT NULL,
	[oje_lgr_id] [char](3) NOT NULL,
	[oje_src_jrnl] [char](3) NOT NULL,
	[oje_ssn_id] [numeric](6, 0) NOT NULL,
	[oje_lgn_id] [char](8) NOT NULL,
	[oje_nar_desc] [char](50) NOT NULL,
	[oje_trs_src] [char](1) NOT NULL,
	[oje_glc_id] [char](3) NOT NULL,
	[oje_transl_sts] [numeric](1, 0) NOT NULL,
	[oje_fm_acctg_per] [numeric](6, 0) NOT NULL,
	[oje_to_acctg_per] [numeric](6, 0) NOT NULL,
	[oje_lst_per_pstd] [numeric](6, 0) NOT NULL,
	[oje_orig_ref_pfx] [char](2) NOT NULL,
	[oje_orig_ref_no] [numeric](8, 0) NOT NULL,
	[oje_orig_ref_itm] [numeric](3, 0) NOT NULL,
	[oje_orig_ref_sbitm] [numeric](4, 0) NOT NULL,
	[oje_orig_cry] [char](3) NOT NULL,
	[oje_orig_exrt] [numeric](13, 8) NOT NULL,
	[oje_gl_extl_id1] [char](10) NOT NULL,
	[oje_gl_extl_id2] [char](10) NOT NULL,
	[oje_gl_extl1] [char](22) NOT NULL,
	[oje_gl_extl2] [char](22) NOT NULL,
	[oje_gl_extl_desc] [char](35) NOT NULL
) ON [PRIMARY]
GO
