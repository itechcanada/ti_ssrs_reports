USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_US_arbcoc_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_US_arbcoc_rec](
	[coc_cmpy_id] [char](3) NOT NULL,
	[coc_cus_id] [char](8) NOT NULL,
	[coc_frst_sls_dt] [date] NULL,
	[coc_lst_sls_dt] [date] NULL
) ON [PRIMARY]
GO
