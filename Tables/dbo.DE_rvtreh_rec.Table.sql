USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_rvtreh_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_rvtreh_rec](
	[reh_cmpy_id] [char](3) NOT NULL,
	[reh_res_hst_ctl_no] [numeric](10, 0) NOT NULL,
	[reh_res_ctl_no] [numeric](10, 0) NOT NULL,
	[reh_hst_lvl1] [numeric](3, 0) NOT NULL,
	[reh_hst_lvl2] [numeric](3, 0) NOT NULL,
	[reh_hst_lvl3] [numeric](3, 0) NOT NULL,
	[reh_hst_lvl4] [numeric](3, 0) NOT NULL,
	[reh_hst_lvl5] [numeric](3, 0) NOT NULL,
	[reh_ref_pfx] [char](2) NOT NULL,
	[reh_ref_no] [numeric](8, 0) NOT NULL,
	[reh_ref_itm] [numeric](3, 0) NOT NULL,
	[reh_ref_sbitm] [numeric](4, 0) NOT NULL,
	[reh_res_ln] [numeric](4, 0) NOT NULL,
	[reh_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[reh_crtd_ref_pfx] [char](2) NOT NULL,
	[reh_crtd_ref_no] [numeric](8, 0) NOT NULL,
	[reh_crtd_ref_itm] [numeric](3, 0) NOT NULL,
	[reh_crtd_ref_sbitm] [numeric](4, 0) NOT NULL,
	[reh_res_typ] [char](1) NOT NULL,
	[reh_res_pcs] [numeric](7, 0) NOT NULL,
	[reh_res_msr] [numeric](16, 4) NOT NULL,
	[reh_res_wgt] [numeric](10, 2) NOT NULL,
	[reh_res_qty] [numeric](16, 4) NOT NULL,
	[reh_res_blg_pcs] [numeric](7, 0) NOT NULL,
	[reh_res_blg_msr] [numeric](16, 4) NOT NULL,
	[reh_res_blg_wgt] [numeric](10, 2) NOT NULL,
	[reh_res_blg_qty] [numeric](16, 4) NOT NULL,
	[reh_chrg_qty_typ] [char](1) NOT NULL,
	[reh_frz_qty_cd] [numeric](1, 0) NOT NULL,
	[reh_res_shp_rdy] [numeric](1, 0) NOT NULL,
	[reh_res_mkd] [numeric](1, 0) NOT NULL,
	[reh_res_mkd_pfx] [char](2) NOT NULL,
	[reh_res_mkd_no] [numeric](8, 0) NOT NULL,
	[reh_res_mkd_itm] [numeric](3, 0) NOT NULL,
	[reh_res_mkd_sitm] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
