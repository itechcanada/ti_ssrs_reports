USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_arbcub_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_arbcub_rec](
	[cub_cmpy_id] [char](3) NOT NULL,
	[cub_cus_id] [char](8) NOT NULL,
	[cub_apd_1] [numeric](3, 0) NOT NULL,
	[cub_apd_2] [numeric](3, 0) NOT NULL,
	[cub_apd_3] [numeric](3, 0) NOT NULL,
	[cub_apd_4] [numeric](3, 0) NOT NULL,
	[cub_apd_5] [numeric](3, 0) NOT NULL,
	[cub_apd_6] [numeric](3, 0) NOT NULL,
	[cub_apd_7] [numeric](3, 0) NOT NULL,
	[cub_apd_8] [numeric](3, 0) NOT NULL,
	[cub_apd_9] [numeric](3, 0) NOT NULL,
	[cub_apd_10] [numeric](3, 0) NOT NULL,
	[cub_apd_11] [numeric](3, 0) NOT NULL,
	[cub_apd_12] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
GO
