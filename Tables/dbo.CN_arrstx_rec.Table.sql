USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_arrstx_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_arrstx_rec](
	[stx_cmpy_id] [char](3) NOT NULL,
	[stx_cus_id] [char](8) NOT NULL,
	[stx_shp_to] [numeric](3, 0) NOT NULL,
	[stx_tx_typ] [char](1) NOT NULL,
	[stx_tx_rgn] [char](6) NOT NULL,
	[stx_tx_id1] [char](20) NOT NULL,
	[stx_tx_id2] [char](10) NOT NULL,
	[stx_tx_sts] [char](1) NOT NULL,
	[stx_exmpt_rsn] [char](3) NULL,
	[stx_rsn_typ] [char](3) NOT NULL,
	[stx_tx_rvw_dt] [date] NULL,
	[stx_exmpt_expy_dt] [date] NULL,
	[stx_exmpt_typ] [char](1) NOT NULL,
	[stx_exmpt_amt] [numeric](12, 2) NOT NULL,
	[stx_upd_dtts] [datetime2](7) NULL,
	[stx_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
