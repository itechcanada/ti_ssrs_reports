USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_intpcd_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_intpcd_rec](
	[pcd_cmpy_id] [char](3) NOT NULL,
	[pcd_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[pcd_ln_no] [numeric](2, 0) NOT NULL,
	[pcd_cond] [char](3) NOT NULL,
	[pcd_cond_orig] [char](3) NULL
) ON [PRIMARY]
GO
