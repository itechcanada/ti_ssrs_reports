USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_frtfrt_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_frtfrt_rec](
	[frt_cmpy_id] [char](3) NOT NULL,
	[frt_ref_pfx] [char](2) NOT NULL,
	[frt_ref_no] [numeric](8, 0) NOT NULL,
	[frt_ref_itm] [numeric](3, 0) NOT NULL,
	[frt_trm_trd] [char](3) NOT NULL,
	[frt_dlvy_mthd] [char](2) NOT NULL,
	[frt_frt_ven_id] [char](8) NULL,
	[frt_crr_nm] [char](35) NOT NULL,
	[frt_port] [char](3) NOT NULL,
	[frt_dest] [char](30) NOT NULL,
	[frt_trnst_whs] [char](3) NULL,
	[frt_dsgd_whs] [char](3) NULL,
	[frt_cry] [char](3) NULL,
	[frt_exrt] [numeric](13, 8) NOT NULL,
	[frt_ex_rt_typ] [char](1) NOT NULL,
	[frt_ven_ref] [char](22) NOT NULL,
	[frt_trrte] [char](6) NULL,
	[frt_cst] [numeric](13, 4) NOT NULL,
	[frt_cst_um] [char](3) NULL,
	[frt_comp_cst] [numeric](13, 4) NOT NULL,
	[frt_fl_cst] [numeric](13, 4) NOT NULL,
	[frt_fl_cst_um] [char](3) NULL,
	[frt_chrg] [numeric](13, 4) NOT NULL,
	[frt_chrg_um] [char](3) NULL,
	[frt_comp_chrg] [numeric](13, 4) NOT NULL,
	[frt_fl_chrg] [numeric](13, 4) NOT NULL,
	[frt_fl_chrg_um] [char](3) NULL,
	[frt_frt_na] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
