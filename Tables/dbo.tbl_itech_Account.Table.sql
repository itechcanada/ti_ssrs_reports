USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Account]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Account](
	[ID] [int] NULL,
	[Name] [varchar](50) NULL
) ON [PRIMARY]
GO
