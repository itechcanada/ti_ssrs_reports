USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_nctncf_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_nctncf_rec](
	[ncf_cmpy_id] [char](3) NOT NULL,
	[ncf_ncr_pfx] [char](2) NOT NULL,
	[ncf_ncr_no] [numeric](8, 0) NOT NULL,
	[ncf_ncr_itm] [numeric](3, 0) NOT NULL,
	[ncf_ncr_ln_no] [numeric](2, 0) NOT NULL,
	[ncf_rsn_typ] [char](3) NOT NULL,
	[ncf_rsn] [char](3) NOT NULL,
	[ncf_flt] [char](3) NOT NULL,
	[ncf_rsn_ref] [char](10) NOT NULL
) ON [PRIMARY]
GO
