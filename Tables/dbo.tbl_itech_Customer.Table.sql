USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Customer]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Customer](
	[Cust_id] [int] NOT NULL,
	[Cust_Name] [varchar](100) NULL,
	[Cust_EmailID] [varchar](100) NULL,
	[Cust_password] [varchar](100) NULL,
	[Cust_Stratix_ID] [varchar](50) NULL,
	[Cust_Active] [bit] NOT NULL,
	[Cust_Contact_Name] [varchar](50) NULL,
	[Cust_ShipTo] [varchar](10) NULL,
	[FName] [varchar](50) NULL,
	[LName] [varchar](50) NULL,
	[DatabaseName] [varchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Cust_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
