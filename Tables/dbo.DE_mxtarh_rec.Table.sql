USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_mxtarh_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_mxtarh_rec](
	[arh_cmpy_id] [char](3) NOT NULL,
	[arh_arch_ver_No] [numeric](3, 0) NOT NULL,
	[arh_bus_Doc_Typ] [char](3) NOT NULL,
	[arh_gen_dtts] [datetime2](7) NULL,
	[arh_prim_ref] [char](30) NOT NULL
) ON [PRIMARY]
GO
