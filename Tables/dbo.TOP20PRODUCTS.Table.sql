USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TOP20PRODUCTS]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TOP20PRODUCTS](
	[PRODUCT] [nvarchar](255) NULL,
	[PERIOD] [datetime] NULL,
	[amount] [float] NULL
) ON [PRIMARY]
GO
