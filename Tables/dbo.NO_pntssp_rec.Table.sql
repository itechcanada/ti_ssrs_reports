USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_pntssp_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_pntssp_rec](
	[ssp_cmpy_id] [char](3) NOT NULL,
	[ssp_ref_pfx] [char](2) NOT NULL,
	[ssp_ref_no] [numeric](8, 0) NOT NULL,
	[ssp_ref_itm] [numeric](3, 0) NOT NULL,
	[ssp_ref_sitm] [numeric](2, 0) NOT NULL,
	[ssp_std_spec_no] [char](10) NOT NULL,
	[ssp_mss_ctl_no] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
GO
