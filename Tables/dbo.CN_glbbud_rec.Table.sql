USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_glbbud_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_glbbud_rec](
	[bud_lgr_id] [char](3) NOT NULL,
	[bud_bsc_gl_acct] [char](8) NOT NULL,
	[bud_sacct] [char](30) NOT NULL,
	[bud_fis_yr] [numeric](4, 0) NOT NULL,
	[bud_dr_amt_1] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_2] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_3] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_4] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_5] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_6] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_7] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_8] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_9] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_10] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_11] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_12] [numeric](12, 2) NOT NULL,
	[bud_dr_amt_13] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_1] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_2] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_3] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_4] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_5] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_6] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_7] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_8] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_9] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_10] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_11] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_12] [numeric](12, 2) NOT NULL,
	[bud_cr_amt_13] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
