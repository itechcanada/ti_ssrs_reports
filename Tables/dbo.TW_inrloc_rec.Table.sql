USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_inrloc_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_inrloc_rec](
	[loc_cmpy_id] [char](3) NOT NULL,
	[loc_whs] [char](3) NOT NULL,
	[loc_loc] [char](7) NOT NULL,
	[loc_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[loc_desc15] [char](15) NOT NULL,
	[loc_capy_wgt] [numeric](12, 2) NOT NULL,
	[loc_capy_wgt_um] [char](3) NOT NULL,
	[loc_auto_stckr] [numeric](1, 0) NOT NULL,
	[loc_whs_zn] [char](3) NOT NULL,
	[loc_lck_loc] [numeric](1, 0) NOT NULL,
	[loc_stg_loc] [numeric](1, 0) NOT NULL,
	[loc_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
