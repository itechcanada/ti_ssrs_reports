USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Management_Account]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Management_Account](
	[id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NULL,
	[order] [int] NOT NULL,
	[DataBaseName] [varchar](10) NULL,
 CONSTRAINT [PK_tbl_itech_Management_Account] PRIMARY KEY CLUSTERED 
(
	[order] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
