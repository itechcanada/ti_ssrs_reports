USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_ipjtrh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_ipjtrh_rec](
	[trh_cmpy_id] [char](3) NOT NULL,
	[trh_ref_pfx] [char](2) NOT NULL,
	[trh_ref_no] [numeric](8, 0) NOT NULL,
	[trh_ref_itm] [numeric](3, 0) NOT NULL,
	[trh_ref_sbitm] [numeric](4, 0) NOT NULL,
	[trh_actvy_dt] [date] NOT NULL,
	[trh_jbs_pfx] [char](2) NOT NULL,
	[trh_jbs_no] [numeric](8, 0) NOT NULL,
	[trh_job_seq_no] [numeric](8, 0) NOT NULL,
	[trh_cus_ownd] [numeric](1, 0) NOT NULL,
	[trh_cus_id] [char](8) NOT NULL,
	[trh_cus_nm] [char](15) NOT NULL,
	[trh_plng_whs] [char](3) NOT NULL,
	[trh_ent_msr] [char](1) NOT NULL,
	[trh_actvy_whs] [char](3) NOT NULL,
	[trh_prs_cl] [char](2) NOT NULL,
	[trh_prs_1] [char](3) NOT NULL,
	[trh_prs_2] [char](3) NOT NULL,
	[trh_pwc] [char](3) NOT NULL,
	[trh_sch_pwc] [char](3) NOT NULL,
	[trh_pwg] [char](3) NOT NULL,
	[trh_sch_strt_dtts] [datetime2](7) NULL,
	[trh_sch_strt_dtms] [numeric](3, 0) NULL,
	[trh_sch_strt_ltts] [datetime2](7) NULL,
	[trh_sch_strt_ltms] [numeric](3, 0) NULL,
	[trh_expct_dtts] [datetime2](7) NULL,
	[trh_expct_dtms] [numeric](3, 0) NULL,
	[trh_strt_dtts] [datetime2](7) NULL,
	[trh_strt_dtms] [numeric](3, 0) NULL,
	[trh_job_cl] [char](1) NOT NULL,
	[trh_match_prod] [char](1) NOT NULL,
	[trh_est_stup_tm] [numeric](5, 0) NOT NULL,
	[trh_est_run_tm] [numeric](5, 0) NOT NULL,
	[trh_est_trdn_tm] [numeric](5, 0) NOT NULL,
	[trh_prs_dy] [numeric](3, 0) NOT NULL,
	[trh_desc30] [char](30) NOT NULL,
	[trh_tact_stup_tm] [numeric](7, 0) NOT NULL,
	[trh_tact_run_tm] [numeric](7, 0) NOT NULL,
	[trh_tact_trdn_tm] [numeric](7, 0) NOT NULL,
	[trh_chrgb_dwntm] [numeric](7, 0) NOT NULL,
	[trh_nchrgb_dwntm] [numeric](7, 0) NOT NULL,
	[trh_rec_comp_dtts] [datetime2](7) NULL,
	[trh_rec_comp_dtms] [numeric](3, 0) NULL,
	[trh_lgn_id] [char](8) NOT NULL
) ON [PRIMARY]
GO
