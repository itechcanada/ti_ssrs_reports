USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_apjjci_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_apjjci_rec](
	[jci_cmpy_id] [char](3) NOT NULL,
	[jci_vchr_pfx] [char](2) NOT NULL,
	[jci_vchr_no] [numeric](8, 0) NOT NULL,
	[jci_vchr_itm] [numeric](3, 0) NOT NULL,
	[jci_crcn_pfx] [char](2) NOT NULL,
	[jci_crcn_no] [numeric](8, 0) NOT NULL,
	[jci_crcn_brh] [char](3) NOT NULL,
	[jci_trs_pfx] [char](2) NOT NULL,
	[jci_trs_no] [numeric](8, 0) NOT NULL,
	[jci_trs_itm] [numeric](3, 0) NOT NULL,
	[jci_trs_sbitm] [numeric](4, 0) NOT NULL,
	[jci_extl_ref] [char](22) NOT NULL,
	[jci_cc_no] [numeric](3, 0) NOT NULL,
	[jci_dsqty] [numeric](16, 4) NOT NULL,
	[jci_dsqty_um] [char](3) NULL,
	[jci_dsamt] [numeric](12, 2) NOT NULL,
	[jci_cry] [char](3) NOT NULL,
	[jci_ex_rt] [numeric](12, 8) NOT NULL
) ON [PRIMARY]
GO
