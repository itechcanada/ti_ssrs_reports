USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_aprshf_rec_copy]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_aprshf_rec_copy](
	[shf_cmpy_id] [char](3) NOT NULL,
	[shf_ven_id] [char](8) NOT NULL,
	[shf_shp_fm] [numeric](3, 0) NOT NULL,
	[shf_shp_fm_nm] [char](15) NOT NULL,
	[shf_shp_fm_long_nm] [char](35) NOT NULL,
	[shf_admin_brh] [char](3) NOT NULL,
	[shf_mill] [char](3) NULL,
	[shf_vat_id] [char](20) NOT NULL,
	[shf_iso_cert] [numeric](1, 0) NOT NULL,
	[shf_qlty_cert] [char](1) NOT NULL,
	[shf_lng] [char](2) NOT NULL,
	[shf_pttrm] [numeric](3, 0) NOT NULL,
	[shf_pmt_disc_trm] [numeric](2, 0) NULL,
	[shf_pur_pttrm] [numeric](3, 0) NOT NULL,
	[shf_pur_disc_trm] [numeric](2, 0) NULL,
	[shf_mthd_pmt] [char](2) NOT NULL,
	[shf_shp_itm_tgth] [numeric](1, 0) NOT NULL,
	[shf_alw_prtl_shp] [numeric](1, 0) NOT NULL,
	[shf_ovrshp_pct] [numeric](5, 2) NOT NULL,
	[shf_undshp_pct] [numeric](5, 2) NOT NULL,
	[shf_rcvg_tag_pfx] [char](2) NOT NULL,
	[shf_ack_reqd] [numeric](1, 0) NOT NULL,
	[shf_upd_dtts] [datetime2](7) NULL,
	[shf_upd_dtms] [numeric](3, 0) NULL,
	[shf_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
