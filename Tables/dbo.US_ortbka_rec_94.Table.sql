USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_ortbka_rec_94]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_ortbka_rec_94](
	[bka_cmpy_id] [char](3) NOT NULL,
	[bka_ord_pfx] [char](2) NOT NULL,
	[bka_ord_no] [numeric](8, 0) NOT NULL,
	[bka_ord_itm] [numeric](3, 0) NOT NULL,
	[bka_actvy_dt] [date] NOT NULL,
	[bka_ln_no] [numeric](2, 0) NOT NULL,
	[bka_brh] [char](3) NOT NULL,
	[bka_trs_md] [char](1) NOT NULL,
	[bka_actvy_tm] [numeric](6, 0) NOT NULL,
	[bka_lgn_id] [char](8) NOT NULL,
	[bka_ord_typ] [char](1) NOT NULL,
	[bka_sld_cus_id] [char](8) NOT NULL,
	[bka_shp_to] [numeric](3, 0) NOT NULL,
	[bka_is_slp] [char](4) NOT NULL,
	[bka_os_slp] [char](4) NOT NULL,
	[bka_tkn_slp] [char](4) NOT NULL,
	[bka_sls_cat] [char](2) NOT NULL,
	[bka_frm] [char](6) NOT NULL,
	[bka_grd] [char](8) NOT NULL,
	[bka_num_size1] [numeric](9, 5) NOT NULL,
	[bka_num_size2] [numeric](9, 5) NOT NULL,
	[bka_num_size3] [numeric](9, 5) NOT NULL,
	[bka_num_size4] [numeric](9, 5) NOT NULL,
	[bka_num_size5] [numeric](9, 5) NOT NULL,
	[bka_size] [char](15) NOT NULL,
	[bka_fnsh] [char](8) NOT NULL,
	[bka_ef_evar] [char](30) NOT NULL,
	[bka_wdth] [numeric](9, 4) NOT NULL,
	[bka_lgth] [numeric](9, 4) NOT NULL,
	[bka_idia] [numeric](9, 5) NOT NULL,
	[bka_odia] [numeric](9, 5) NOT NULL,
	[bka_ga_size] [numeric](8, 5) NOT NULL,
	[bka_ga_typ] [char](1) NOT NULL,
	[bka_pcs] [numeric](7, 0) NOT NULL,
	[bka_msr] [numeric](16, 4) NOT NULL,
	[bka_wgt] [numeric](10, 2) NOT NULL,
	[bka_qty] [numeric](16, 4) NOT NULL,
	[bka_mtl_chrg] [numeric](13, 4) NOT NULL,
	[bka_mtl_chrg_um] [char](3) NOT NULL,
	[bka_tot_chrg] [numeric](13, 4) NOT NULL,
	[bka_cry] [char](3) NOT NULL,
	[bka_exrt] [numeric](13, 8) NOT NULL,
	[bka_ex_rt_typ] [char](1) NOT NULL,
	[bka_tot_mtl_val] [numeric](15, 2) NOT NULL,
	[bka_tot_val] [numeric](15, 2) NOT NULL,
	[bka_mtl_avg_val] [numeric](15, 2) NOT NULL,
	[bka_mtl_repl_val] [numeric](15, 2) NOT NULL,
	[bka_tot_avg_val] [numeric](15, 2) NOT NULL,
	[bka_tot_repl_val] [numeric](15, 2) NOT NULL,
	[bka_cls_rsn] [char](3) NOT NULL,
	[bka_cls_rmk] [char](50) NOT NULL,
	[bka_actvy_dtts] [datetime2](7) NULL,
	[bka_actvy_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
