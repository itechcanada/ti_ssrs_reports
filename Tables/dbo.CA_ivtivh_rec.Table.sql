USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_ivtivh_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_ivtivh_rec](
	[ivh_cmpy_id] [char](3) NOT NULL,
	[ivh_inv_pfx] [char](2) NOT NULL,
	[ivh_inv_no] [numeric](8, 0) NOT NULL,
	[ivh_inv_brh] [char](3) NOT NULL,
	[ivh_inv_typ] [char](1) NOT NULL,
	[ivh_inv_mthd] [char](1) NOT NULL,
	[ivh_sld_cus_id] [char](8) NOT NULL,
	[ivh_cr_ctl_cus_id] [char](8) NULL,
	[ivh_job_id] [char](8) NULL,
	[ivh_rsn_typ] [char](3) NULL,
	[ivh_rsn] [char](3) NULL,
	[ivh_rsn_ref] [char](10) NOT NULL,
	[ivh_cry] [char](3) NOT NULL,
	[ivh_exrt] [numeric](13, 8) NOT NULL,
	[ivh_ex_rt_typ] [char](1) NOT NULL,
	[ivh_pttrm] [numeric](3, 0) NOT NULL,
	[ivh_disc_trm] [numeric](2, 0) NULL,
	[ivh_mthd_pmt] [char](2) NOT NULL,
	[ivh_upd_ref_no] [numeric](10, 0) NOT NULL,
	[ivh_upd_ref] [char](22) NOT NULL,
	[ivh_ar_val] [numeric](15, 2) NOT NULL,
	[ivh_inv_dt] [date] NULL,
	[ivh_ar_disc_val] [numeric](15, 2) NOT NULL,
	[ivh_disc_dt] [date] NULL,
	[ivh_inv_due_dt] [date] NULL,
	[ivh_sls_cat] [char](2) NOT NULL,
	[ivh_tot_opn_wgt] [numeric](12, 2) NOT NULL,
	[ivh_tot_cls_wgt] [numeric](12, 2) NOT NULL,
	[ivh_opn_itm] [numeric](3, 0) NOT NULL,
	[ivh_cls_itm] [numeric](3, 0) NOT NULL,
	[ivh_flt] [char](3) NULL,
	[ivh_inv_lnk] [numeric](1, 0) NOT NULL,
	[ivh_src_upd_ref] [char](22) NOT NULL,
	[ivh_crtd_dtts] [datetime2](7) NULL,
	[ivh_crtd_dtms] [numeric](3, 0) NULL,
	[ivh_ssn_log_ctl_no] [numeric](10, 0) NOT NULL,
	[ivh_sts_actn] [char](1) NOT NULL
) ON [PRIMARY]
GO
