USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_ivtivs_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_ivtivs_rec](
	[ivs_cmpy_id] [char](3) NOT NULL,
	[ivs_shpt_pfx] [char](2) NOT NULL,
	[ivs_shpt_no] [numeric](8, 0) NOT NULL,
	[ivs_shpt_brh] [char](3) NOT NULL,
	[ivs_sld_cus_id] [char](8) NOT NULL,
	[ivs_shp_to] [numeric](3, 0) NOT NULL,
	[ivs_cus_po] [char](30) NOT NULL,
	[ivs_transp_pfx] [char](2) NULL,
	[ivs_transp_no] [numeric](8, 0) NULL,
	[ivs_pk_list_no] [numeric](3, 0) NOT NULL,
	[ivs_ord_pfx] [char](2) NOT NULL,
	[ivs_ord_no] [numeric](8, 0) NOT NULL,
	[ivs_shpg_whs] [char](3) NULL,
	[ivs_shp_dt] [date] NULL,
	[ivs_inv_pfx] [char](2) NOT NULL,
	[ivs_inv_no] [numeric](8, 0) NOT NULL,
	[ivs_inv_upd] [numeric](1, 0) NOT NULL,
	[ivs_tot_opn_wgt] [numeric](12, 2) NOT NULL,
	[ivs_tot_cls_wgt] [numeric](12, 2) NOT NULL,
	[ivs_opn_itm] [numeric](3, 0) NOT NULL,
	[ivs_cls_itm] [numeric](3, 0) NOT NULL,
	[ivs_sts_actn] [char](1) NOT NULL
) ON [PRIMARY]
GO
