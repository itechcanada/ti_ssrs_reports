USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_gltojd_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_gltojd_rec](
	[ojd_oje_pfx] [char](2) NOT NULL,
	[ojd_oje_no] [numeric](8, 0) NOT NULL,
	[ojd_dist_ln_no] [numeric](6, 0) NOT NULL,
	[ojd_bsc_gl_acct] [char](8) NOT NULL,
	[ojd_lgr_id] [char](3) NOT NULL,
	[ojd_sacct] [char](30) NULL,
	[ojd_dr_amt] [numeric](12, 2) NOT NULL,
	[ojd_cr_amt] [numeric](12, 2) NOT NULL,
	[ojd_dist_rmk] [char](50) NOT NULL
) ON [PRIMARY]
GO
