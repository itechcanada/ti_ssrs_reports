USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_costCharge]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_costCharge](
	[idCostCharge] [int] NOT NULL,
	[costChargeNumber] [int] NOT NULL,
	[description] [varchar](500) NULL,
	[cc_M] [char](1) NULL,
	[cc_Grp] [varchar](3) NULL,
	[cc_Sg] [varchar](3) NULL,
	[PrcCstDesc] [varchar](500) NULL
) ON [PRIMARY]
GO
