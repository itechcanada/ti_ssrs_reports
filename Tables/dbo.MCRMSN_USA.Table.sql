USE [Stratix_US]
GO
/****** Object:  Table [dbo].[MCRMSN_USA]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MCRMSN_USA](
	[msn_mss_ctl_no] [numeric](10, 0) NOT NULL,
	[msn_frm] [char](6) NOT NULL,
	[msn_grd] [char](8) NOT NULL,
	[msn_all_grd] [numeric](1, 0) NOT NULL,
	[msn_fnsh] [char](8) NOT NULL,
	[msn_all_fnsh] [numeric](1, 0) NOT NULL,
	[msn_dflt_std] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
