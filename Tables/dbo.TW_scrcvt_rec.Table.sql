USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_scrcvt_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_scrcvt_rec](
	[cvt_cmpy_id] [char](3) NOT NULL,
	[cvt_ref_pfx] [char](2) NOT NULL,
	[cvt_cus_ven_typ] [char](1) NOT NULL,
	[cvt_cus_ven_id] [char](8) NOT NULL,
	[cvt_addr_typ] [char](1) NOT NULL,
	[cvt_addr_no] [numeric](3, 0) NOT NULL,
	[cvt_cntc_typ] [char](2) NOT NULL,
	[cvt_cntc_no] [numeric](1, 0) NOT NULL,
	[cvt_sltn_cd] [char](3) NULL,
	[cvt_frst_nm] [char](20) NOT NULL,
	[cvt_lst_nm] [char](30) NOT NULL,
	[cvt_ttl] [char](30) NOT NULL,
	[cvt_tel_area_cd] [char](7) NOT NULL,
	[cvt_tel_no] [char](15) NOT NULL,
	[cvt_tel_ext] [char](5) NOT NULL,
	[cvt_mobl_area_cd] [char](7) NOT NULL,
	[cvt_mobl_no] [char](15) NOT NULL,
	[cvt_fax_area_cd] [char](7) NOT NULL,
	[cvt_fax_no] [char](15) NOT NULL,
	[cvt_fax_ext] [char](5) NOT NULL,
	[cvt_email] [char](50) NOT NULL,
	[cvt_rmk] [char](50) NOT NULL,
	[cvt_job_fct] [char](3) NULL,
	[cvt_ml_1] [char](3) NULL,
	[cvt_ml_2] [char](3) NULL,
	[cvt_ml_3] [char](3) NULL,
	[cvt_ml_4] [char](3) NULL,
	[cvt_ml_5] [char](3) NULL,
	[cvt_ml_6] [char](3) NULL,
	[cvt_ml_7] [char](3) NULL,
	[cvt_ml_8] [char](3) NULL,
	[cvt_ml_9] [char](3) NULL,
	[cvt_ml_10] [char](3) NULL,
	[cvt_upd_dtts] [datetime2](7) NULL,
	[cvt_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
