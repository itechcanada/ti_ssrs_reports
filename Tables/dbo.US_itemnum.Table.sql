USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_itemnum]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_itemnum](
	[item_num] [char](50) NOT NULL
) ON [PRIMARY]
GO
