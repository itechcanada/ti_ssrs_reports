USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tblDailySynchronizationStatus]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDailySynchronizationStatus](
	[Prefix] [varchar](150) NULL,
	[tablename] [varchar](150) NULL,
	[localcount] [decimal](20, 0) NULL,
	[livecount] [decimal](20, 0) NULL,
	[pct] [decimal](20, 2) NULL,
	[CreatedOn] [datetime] NOT NULL
) ON [PRIMARY]
GO
