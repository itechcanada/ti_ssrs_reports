USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_iTECHTipProduct]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_iTECHTipProduct](
	[FormGrade] [nchar](35) NULL,
	[Size] [nchar](20) NULL,
	[Finish] [nchar](20) NULL,
	[DBname] [nchar](2) NULL
) ON [PRIMARY]
GO
