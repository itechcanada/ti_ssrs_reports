USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_arrcuc_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_arrcuc_rec](
	[cuc_cus_cat] [char](2) NOT NULL,
	[cuc_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[cuc_desc30] [char](30) NOT NULL,
	[cuc_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
