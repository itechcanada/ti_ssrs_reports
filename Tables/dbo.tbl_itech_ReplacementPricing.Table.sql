USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_ReplacementPricing]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_ReplacementPricing](
	[otherQTY] [numeric](38, 2) NULL,
	[Database] [varchar](10) NULL,
	[ProductShort] [varchar](213) NULL,
	[Product] [varchar](263) NULL,
	[Form] [varchar](65) NULL,
	[Grade] [varchar](65) NULL,
	[Size] [char](15) NULL,
	[Finish] [varchar](65) NULL,
	[ReplCost] [decimal](20, 2) NULL,
	[LstReplCostUpdateDT] [varchar](10) NULL,
	[ReplCostUpdateDT] [varchar](10) NULL,
	[OhdStock] [decimal](20, 0) NULL,
	[Months6InvoicedWeight] [decimal](20, 0) NULL,
	[Months6InvoicedSales] [decimal](20, 0) NULL,
	[AvgDaysInventory] [decimal](20, 2) NULL,
	[ExcessInventory] [decimal](20, 0) NULL,
	[OpenPOWgt] [decimal](20, 0) NULL,
	[AvailableWeight] [decimal](20, 0) NULL,
	[Months6InvoicedNP] [decimal](20, 0) NULL,
	[Months6TotalInvoiced] [numeric](18, 0) NULL,
	[avgCost] [decimal](20, 2) NULL,
	[BHMQTY] [numeric](38, 2) NULL,
	[DETQTY] [numeric](38, 2) NULL,
	[HIBQTY] [numeric](38, 2) NULL,
	[JACQTY] [numeric](38, 2) NULL,
	[LAXQTY] [numeric](38, 2) NULL,
	[MTLQTY] [numeric](38, 2) NULL,
	[ROSQTY] [numeric](38, 2) NULL,
	[SEAQTY] [numeric](38, 2) NULL,
	[SHAQTY] [numeric](38, 2) NULL,
	[TAIQTY] [numeric](38, 2) NULL,
	[WDLQTY] [numeric](38, 2) NULL,
	[BONQTY] [numeric](38, 2) NULL
) ON [PRIMARY]
GO
