USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_scrbrh_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_scrbrh_rec](
	[brh_cmpy_id] [char](3) NOT NULL,
	[brh_brh] [char](3) NOT NULL,
	[brh_brh_nm] [char](15) NOT NULL,
	[brh_divn] [char](3) NOT NULL,
	[brh_rgn] [char](3) NOT NULL,
	[brh_nm1] [char](35) NOT NULL,
	[brh_nm2] [char](35) NOT NULL,
	[brh_addr1] [char](35) NOT NULL,
	[brh_addr2] [char](35) NOT NULL,
	[brh_addr3] [char](35) NOT NULL,
	[brh_city] [char](35) NOT NULL,
	[brh_pcd] [char](10) NOT NULL,
	[brh_latd] [numeric](11, 8) NOT NULL,
	[brh_lngtd] [numeric](11, 8) NOT NULL,
	[brh_cty] [char](3) NOT NULL,
	[brh_st_prov] [char](2) NULL,
	[brh_tel_area_cd] [char](7) NOT NULL,
	[brh_tel_no] [char](15) NOT NULL,
	[brh_tel_ext] [char](5) NOT NULL,
	[brh_fax_area_cd] [char](7) NOT NULL,
	[brh_fax_no] [char](15) NOT NULL,
	[brh_fax_ext] [char](5) NOT NULL,
	[brh_rcvb_rmt_to] [char](1) NOT NULL,
	[brh_rmt_to_addr] [char](4) NULL,
	[brh_pinv_rmt_to] [char](1) NOT NULL,
	[brh_pinv_rmt_addr] [char](4) NULL,
	[brh_use_mult] [numeric](1, 0) NOT NULL,
	[brh_lng] [char](2) NOT NULL,
	[brh_tmzn] [char](5) NOT NULL,
	[brh_dflt_stkg_whs] [char](3) NOT NULL,
	[brh_dun_no] [numeric](9, 0) NOT NULL,
	[brh_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
