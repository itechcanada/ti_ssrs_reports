USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_short_ti_part]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_short_ti_part](
	[idShortTIPartNo] [int] IDENTITY(1,1) NOT NULL,
	[shortTIPartNo] [varchar](40) NOT NULL,
 CONSTRAINT [PK_tbl_itech_short_ti_part] PRIMARY KEY CLUSTERED 
(
	[idShortTIPartNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
