USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_ortipr_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_ortipr_rec](
	[ipr_cmpy_id] [char](3) NOT NULL,
	[ipr_ref_pfx] [char](2) NOT NULL,
	[ipr_ref_no] [numeric](8, 0) NOT NULL,
	[ipr_ref_itm] [numeric](3, 0) NOT NULL,
	[ipr_ref_sitm] [numeric](2, 0) NOT NULL,
	[ipr_ref_ln_no] [numeric](2, 0) NOT NULL,
	[ipr_chrg_ln_no] [numeric](2, 0) NOT NULL,
	[ipr_prs_seq_no] [numeric](6, 0) NOT NULL,
	[ipr_actvy_whs] [char](3) NULL,
	[ipr_pwg] [char](3) NULL,
	[ipr_pwc] [char](3) NULL,
	[ipr_prs] [char](3) NOT NULL,
	[ipr_prnt_ln_no] [numeric](2, 0) NOT NULL,
	[ipr_ovrd_prsrt] [numeric](1, 0) NOT NULL,
	[ipr_sch_strt_dtts] [datetime2](7) NULL,
	[ipr_sch_strt_dtms] [numeric](3, 0) NULL,
	[ipr_sch_strt_ltts] [datetime2](7) NULL,
	[ipr_sch_strt_ltms] [numeric](3, 0) NULL,
	[ipr_tfr_trnst_tm] [numeric](7, 0) NOT NULL,
	[ipr_tot_stup_tm] [numeric](7, 0) NOT NULL,
	[ipr_tot_run_tm] [numeric](7, 0) NOT NULL,
	[ipr_prs_dy] [numeric](3, 0) NOT NULL,
	[ipr_sch_dt_ovrd] [char](1) NOT NULL
) ON [PRIMARY]
GO
