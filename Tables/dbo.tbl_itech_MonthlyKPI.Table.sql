USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_MonthlyKPI]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_MonthlyKPI](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DatabasePrefix] [varchar](50) NULL,
	[BranchPrefix] [varchar](50) NULL,
	[KPIDate] [datetime] NULL,
	[KPIHour] [int] NULL,
	[CreatedDateTime] [datetime] NULL
) ON [PRIMARY]
GO
