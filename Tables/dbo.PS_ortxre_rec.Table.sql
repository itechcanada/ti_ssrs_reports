USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_ortxre_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_ortxre_rec](
	[xre_cmpy_id] [char](3) NOT NULL,
	[xre_ord_pfx] [char](2) NOT NULL,
	[xre_ord_no] [numeric](8, 0) NOT NULL,
	[xre_sld_cus_id] [char](8) NOT NULL,
	[xre_shp_to] [numeric](3, 0) NOT NULL,
	[xre_ord_typ] [char](1) NOT NULL,
	[xre_city] [char](35) NOT NULL,
	[xre_cry] [char](3) NOT NULL,
	[xre_crtd_dtts] [datetime2](7) NULL,
	[xre_crtd_dtms] [numeric](3, 0) NULL,
	[xre_expy_dt] [date] NULL,
	[xre_multp_po] [numeric](1, 0) NOT NULL,
	[xre_multp_part] [numeric](1, 0) NOT NULL,
	[xre_multp_ddt] [numeric](1, 0) NOT NULL,
	[xre_sts_actn] [char](1) NOT NULL
) ON [PRIMARY]
GO
