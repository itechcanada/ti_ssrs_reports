USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_ortorh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_ortorh_rec](
	[orh_cmpy_id] [char](3) NOT NULL,
	[orh_ord_pfx] [char](2) NOT NULL,
	[orh_ord_no] [numeric](8, 0) NOT NULL,
	[orh_ord_brh] [char](3) NOT NULL,
	[orh_sld_cus_id] [char](8) NOT NULL,
	[orh_shp_to] [numeric](3, 0) NOT NULL,
	[orh_is_slp] [char](4) NOT NULL,
	[orh_os_slp] [char](4) NOT NULL,
	[orh_tkn_slp] [char](4) NOT NULL,
	[orh_slp_flwp_dt] [date] NULL,
	[orh_expy_dt] [date] NULL,
	[orh_fut_ord_cr_dt] [date] NULL,
	[orh_cr_dt_ovrd] [numeric](1, 0) NOT NULL,
	[orh_shp_itm_tgth] [numeric](1, 0) NOT NULL,
	[orh_blnd_shpt_mthd] [char](1) NOT NULL,
	[orh_shpg_whs] [char](3) NOT NULL,
	[orh_ord_typ] [char](1) NOT NULL,
	[orh_cry] [char](3) NOT NULL,
	[orh_ex_rt] [numeric](12, 8) NOT NULL,
	[orh_ex_rt_typ] [char](1) NOT NULL,
	[orh_pmt_trm] [numeric](2, 0) NULL,
	[orh_disc_trm] [numeric](2, 0) NULL,
	[orh_mthd_pmt] [char](2) NULL,
	[orh_tot_opn_wgt] [numeric](12, 2) NOT NULL,
	[orh_tot_cls_wgt] [numeric](12, 2) NOT NULL,
	[orh_tot_shp_wgt] [numeric](12, 2) NOT NULL,
	[orh_tot_jbdt_wgt] [numeric](12, 2) NOT NULL,
	[orh_opn_itm] [numeric](3, 0) NOT NULL,
	[orh_cls_itm] [numeric](3, 0) NOT NULL,
	[orh_prc_expy_dt] [date] NULL,
	[orh_prc_expy_rmk] [char](30) NOT NULL,
	[orh_job_id] [char](8) NULL,
	[orh_sls_cat] [char](2) NOT NULL,
	[orh_fab_itm_cd] [numeric](1, 0) NOT NULL,
	[orh_prcg_mthd] [char](1) NOT NULL,
	[orh_cntr_sls] [numeric](1, 0) NOT NULL,
	[orh_multp_rls] [numeric](1, 0) NOT NULL,
	[orh_prj_id] [char](8) NOT NULL,
	[orh_sts_actn] [char](1) NOT NULL,
	[orh_lgn_id] [char](8) NOT NULL
) ON [PRIMARY]
GO
