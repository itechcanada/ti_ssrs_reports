USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_scrrsn_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_scrrsn_rec](
	[rsn_rsn_typ] [char](3) NOT NULL,
	[rsn_rsn] [char](3) NOT NULL,
	[rsn_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[rsn_desc30] [char](30) NOT NULL,
	[rsn_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
