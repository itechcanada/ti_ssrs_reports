USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_potpor_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_potpor_rec](
	[por_cmpy_id] [char](3) NOT NULL,
	[por_po_pfx] [char](2) NOT NULL,
	[por_po_no] [numeric](8, 0) NOT NULL,
	[por_po_itm] [numeric](3, 0) NOT NULL,
	[por_po_intl_rls_no] [numeric](2, 0) NOT NULL,
	[por_po_rls_no] [char](10) NOT NULL,
	[por_ord_pcs] [numeric](7, 0) NOT NULL,
	[por_ord_msr] [numeric](16, 4) NOT NULL,
	[por_ord_wgt] [numeric](10, 2) NOT NULL,
	[por_ord_qty] [numeric](16, 4) NOT NULL,
	[por_rcvd_pcs] [numeric](7, 0) NOT NULL,
	[por_rcvd_msr] [numeric](16, 4) NOT NULL,
	[por_rcvd_wgt] [numeric](10, 2) NOT NULL,
	[por_rcvd_qty] [numeric](16, 4) NOT NULL,
	[por_bal_pcs] [numeric](7, 0) NOT NULL,
	[por_bal_msr] [numeric](16, 4) NOT NULL,
	[por_bal_wgt] [numeric](10, 2) NOT NULL,
	[por_bal_qty] [numeric](16, 4) NOT NULL,
	[por_orig_rdy_pcs] [numeric](7, 0) NOT NULL,
	[por_orig_rdy_msr] [numeric](16, 4) NOT NULL,
	[por_orig_rdy_wgt] [numeric](10, 2) NOT NULL,
	[por_orig_rdy_qty] [numeric](16, 4) NOT NULL,
	[por_rdy_pcs] [numeric](7, 0) NOT NULL,
	[por_rdy_msr] [numeric](16, 4) NOT NULL,
	[por_rdy_wgt] [numeric](10, 2) NOT NULL,
	[por_rdy_qty] [numeric](16, 4) NOT NULL,
	[por_rdy_dt] [date] NULL,
	[por_due_dt_qlf] [char](1) NULL,
	[por_due_dt_fmt] [char](2) NOT NULL,
	[por_due_dt_ent] [numeric](8, 0) NOT NULL,
	[por_due_fm_dt] [date] NULL,
	[por_due_to_dt] [date] NULL,
	[por_due_dt_wk_no] [numeric](2, 0) NOT NULL,
	[por_due_dt_wk_cy] [numeric](4, 0) NOT NULL,
	[por_roll_sch] [char](20) NOT NULL,
	[por_ack_rcvd] [numeric](1, 0) NOT NULL,
	[por_ack_dt] [date] NULL,
	[por_mill_ord_no] [char](25) NOT NULL,
	[por_opn_dist] [numeric](2, 0) NOT NULL,
	[por_cls_dist] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
