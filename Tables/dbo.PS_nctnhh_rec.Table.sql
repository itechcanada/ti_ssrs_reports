USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_nctnhh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_nctnhh_rec](
	[nhh_cmpy_id] [char](3) NOT NULL,
	[nhh_ncr_pfx] [char](2) NOT NULL,
	[nhh_ncr_no] [numeric](8, 0) NOT NULL,
	[nhh_ncr_rptd_src] [char](1) NOT NULL,
	[nhh_ncr_claim_typ] [char](1) NOT NULL,
	[nhh_ncr_ent_mthd] [char](1) NOT NULL,
	[nhh_cus_ven_id] [char](8) NOT NULL,
	[nhh_cus_ven_shp] [numeric](3, 0) NOT NULL,
	[nhh_oth_ref] [char](50) NOT NULL,
	[nhh_desc60] [char](60) NOT NULL,
	[nhh_brh] [char](3) NULL,
	[nhh_whs] [char](3) NULL,
	[nhh_cry] [char](3) NULL,
	[nhh_exrt] [numeric](13, 8) NOT NULL,
	[nhh_ex_rt_typ] [char](1) NOT NULL,
	[nhh_ncr_admin] [char](3) NOT NULL,
	[nhh_srns] [char](1) NOT NULL,
	[nhh_crtd_dtts] [datetime2](7) NULL,
	[nhh_crtd_dtms] [numeric](3, 0) NULL,
	[nhh_flwp_dt] [date] NULL,
	[nhh_ncr_sts_nts] [char](60) NOT NULL,
	[nhh_rsln_typ] [char](3) NOT NULL,
	[nhh_rsn_typ] [char](3) NOT NULL,
	[nhh_rsn] [char](3) NULL,
	[nhh_ncr_sts_actn] [char](1) NOT NULL,
	[nhh_ncr_actn_cd] [char](3) NULL
) ON [PRIMARY]
GO
