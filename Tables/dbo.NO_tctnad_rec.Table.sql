USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_tctnad_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_tctnad_rec](
	[nad_cmpy_id] [char](3) NOT NULL,
	[nad_ref_pfx] [char](2) NOT NULL,
	[nad_ref_no] [numeric](8, 0) NOT NULL,
	[nad_ref_itm] [numeric](3, 0) NOT NULL,
	[nad_addr_typ] [char](1) NOT NULL,
	[nad_nm1] [char](35) NOT NULL,
	[nad_nm2] [char](35) NOT NULL,
	[nad_addr1] [char](35) NOT NULL,
	[nad_addr2] [char](35) NOT NULL,
	[nad_addr3] [char](35) NOT NULL,
	[nad_city] [char](35) NOT NULL,
	[nad_pcd] [char](10) NOT NULL,
	[nad_latd] [numeric](11, 8) NOT NULL,
	[nad_lngtd] [numeric](11, 8) NOT NULL,
	[nad_cty] [char](3) NOT NULL,
	[nad_st_prov] [char](2) NULL,
	[nad_transp_zn] [char](6) NOT NULL,
	[nad_tel_area_cd] [char](7) NOT NULL,
	[nad_tel_no] [char](15) NOT NULL,
	[nad_tel_ext] [char](5) NOT NULL,
	[nad_fax_area_cd] [char](7) NOT NULL,
	[nad_fax_no] [char](15) NOT NULL,
	[nad_fax_ext] [char](5) NOT NULL,
	[nad_email] [char](50) NOT NULL,
	[nad_tmzn] [char](5) NULL
) ON [PRIMARY]
GO
