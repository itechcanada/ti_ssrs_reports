USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_tcttsa_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_tcttsa_rec](
	[tsa_cmpy_id] [char](3) NOT NULL,
	[tsa_ref_pfx] [char](2) NOT NULL,
	[tsa_ref_no] [numeric](8, 0) NOT NULL,
	[tsa_ref_itm] [numeric](3, 0) NOT NULL,
	[tsa_ref_sbitm] [numeric](4, 0) NOT NULL,
	[tsa_sts_typ] [char](1) NOT NULL,
	[tsa_sts_actn] [char](1) NOT NULL,
	[tsa_rsn_typ] [char](3) NULL,
	[tsa_rsn] [char](3) NULL,
	[tsa_sts] [char](3) NULL,
	[tsa_rmk] [char](50) NOT NULL,
	[tsa_crtd_dtts] [datetime2](7) NULL,
	[tsa_crtd_dtms] [numeric](3, 0) NULL,
	[tsa_lst_upd_dtts] [datetime2](7) NULL,
	[tsa_lst_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
