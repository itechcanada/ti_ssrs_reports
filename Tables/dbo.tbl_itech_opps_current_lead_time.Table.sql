USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_opps_current_lead_time]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_opps_current_lead_time](
	[category] [varchar](100) NOT NULL,
	[ROC] [varchar](50) NULL,
	[LAX] [varchar](50) NULL,
	[CHC] [varchar](50) NULL,
	[TEX] [varchar](50) NULL,
	[JAX] [varchar](50) NULL,
	[SEA] [varchar](50) NULL,
	[DET] [varchar](50) NULL,
	[MTL] [varchar](50) NULL,
	[UK] [varchar](50) NULL,
	[SHI] [varchar](50) NULL,
	[TAI] [varchar](50) NULL,
	[seq] [int] NOT NULL,
	[WDL] [varchar](50) NULL,
	[HIB] [varchar](50) NULL,
	[JAC] [varchar](50) NULL,
	[BHM] [varchar](50) NULL,
	[SHA] [varchar](50) NULL,
	[categoryDay] [varchar](50) NULL,
	[categoryDesc] [nvarchar](100) NULL
) ON [PRIMARY]
GO
