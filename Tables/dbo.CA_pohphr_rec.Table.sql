USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_pohphr_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_pohphr_rec](
	[phr_cmpy_id] [char](3) NOT NULL,
	[phr_po_pfx] [char](2) NOT NULL,
	[phr_po_no] [numeric](8, 0) NOT NULL,
	[phr_po_itm] [numeric](3, 0) NOT NULL,
	[phr_po_crtd_dt] [date] NOT NULL,
	[phr_brh] [char](3) NOT NULL,
	[phr_po_typ] [char](1) NOT NULL,
	[phr_po_pl_dt] [date] NOT NULL,
	[phr_prc_in_eff] [numeric](1, 0) NOT NULL,
	[phr_ord_pcs] [numeric](7, 0) NOT NULL,
	[phr_ord_msr] [numeric](16, 4) NOT NULL,
	[phr_ord_wgt] [numeric](10, 2) NOT NULL,
	[phr_ord_qty] [numeric](16, 4) NOT NULL,
	[phr_ord_wgt_um] [char](3) NULL,
	[phr_ven_id] [char](8) NOT NULL,
	[phr_shp_fm] [numeric](3, 0) NOT NULL,
	[phr_ven_long_nm] [char](35) NOT NULL,
	[phr_frm] [char](6) NOT NULL,
	[phr_grd] [char](8) NOT NULL,
	[phr_num_size1] [numeric](9, 5) NOT NULL,
	[phr_num_size2] [numeric](9, 5) NOT NULL,
	[phr_num_size3] [numeric](9, 5) NOT NULL,
	[phr_num_size4] [numeric](9, 5) NOT NULL,
	[phr_num_size5] [numeric](9, 5) NOT NULL,
	[phr_size] [char](15) NOT NULL,
	[phr_fnsh] [char](8) NOT NULL,
	[phr_ef_evar] [char](30) NOT NULL,
	[phr_wdth] [numeric](9, 4) NOT NULL,
	[phr_lgth] [numeric](9, 4) NOT NULL,
	[phr_dim_dsgn] [char](1) NOT NULL,
	[phr_idia] [numeric](9, 5) NOT NULL,
	[phr_odia] [numeric](9, 5) NOT NULL,
	[phr_ga_size] [numeric](8, 5) NOT NULL,
	[phr_ga_typ] [char](1) NOT NULL,
	[phr_rdm_dim_1] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_2] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_3] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_4] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_5] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_6] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_7] [numeric](9, 4) NOT NULL,
	[phr_rdm_dim_8] [numeric](9, 4) NOT NULL,
	[phr_rdm_area] [numeric](16, 4) NOT NULL,
	[phr_ent_msr] [char](1) NOT NULL,
	[phr_frc_fmt] [numeric](1, 0) NOT NULL,
	[phr_ord_lgth_typ] [char](2) NULL,
	[phr_prd_desc50a] [char](50) NOT NULL,
	[phr_prd_desc50b] [char](50) NOT NULL,
	[phr_prd_desc50c] [char](50) NOT NULL,
	[phr_mtl_cst] [numeric](13, 4) NOT NULL,
	[phr_mtl_cst_um] [char](3) NULL,
	[phr_tot_cst] [numeric](13, 4) NOT NULL,
	[phr_qty_cst_um] [char](3) NULL,
	[phr_cry] [char](3) NOT NULL,
	[phr_exrt] [numeric](13, 8) NOT NULL,
	[phr_ex_rt_typ] [char](1) NOT NULL,
	[phr_pur_cat] [char](2) NOT NULL,
	[phr_dflt_dist_typ] [char](1) NOT NULL,
	[phr_src] [char](2) NOT NULL,
	[phr_aly_schg_apln] [char](1) NOT NULL,
	[phr_consg] [numeric](1, 0) NOT NULL,
	[phr_invt_qlty] [char](1) NULL,
	[phr_trm_trd] [char](3) NOT NULL,
	[phr_byr] [char](4) NOT NULL,
	[phr_crtd_lgn_id] [char](8) NOT NULL,
	[phr_po_ref_key] [char](13) NOT NULL
) ON [PRIMARY]
GO
