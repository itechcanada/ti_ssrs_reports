USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_STRATIXDocListUK]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_STRATIXDocListUK](
	[DocType] [varchar](19) NULL,
	[DocNumber] [varchar](22) NULL,
	[DocGenDateTime] [datetime2](7) NULL,
	[DocTotalPages] [numeric](6, 0) NULL,
	[DocFileSizeBytes] [numeric](10, 0) NULL,
	[DocArhDocPfx] [char](1) NULL,
	[DocArhArchCtlNo] [numeric](10, 0) NULL,
	[DocArhArchCtlItm] [numeric](3, 0) NULL,
	[DocArhArchVerNo] [numeric](6, 0) NULL,
	[DocCustID] [char](8) NULL,
	[DocRecUpdated] [datetime] NOT NULL
) ON [PRIMARY]
GO
