USE [Stratix_US]
GO
/****** Object:  Table [dbo].[HPM_DETAIL_TEST]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HPM_DETAIL_TEST](
	[country] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[ABC] [nvarchar](255) NULL,
	[Stock_Wgt] [float] NULL,
	[Stock_Value] [nvarchar](255) NULL,
	[Stock_Cost] [nvarchar](255) NULL,
	[3M_wgt] [float] NULL,
	[6M_wgt] [float] NULL,
	[12M_wgt] [float] NULL,
	[Stock_Mo_Supply] [nvarchar](255) NULL,
	[SO_Wgt] [nvarchar](255) NULL,
	[PO_Wgt] [nvarchar](255) NULL,
	[PO_Mo_Supply] [nvarchar](255) NULL
) ON [PRIMARY]
GO
