USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Forecast_AM_20210201]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Forecast_AM_20210201](
	[Budget] [int] NULL,
	[Forecast] [int] NULL,
	[GPBudget] [int] NULL,
	[LBSShippedPlan] [int] NULL,
	[WarehseFees] [int] NULL,
	[Branch] [varchar](50) NOT NULL,
	[Databases] [varchar](50) NOT NULL,
	[YearMonth] [varchar](10) NULL,
	[Date] [date] NOT NULL,
	[InvtValBrh] [varchar](10) NULL,
	[GPForecast] [int] NULL,
	[GPForecastPct] [decimal](20, 2) NULL
) ON [PRIMARY]
GO
