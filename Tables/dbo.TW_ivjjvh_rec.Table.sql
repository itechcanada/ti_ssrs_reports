USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_ivjjvh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_ivjjvh_rec](
	[jvh_cmpy_id] [char](3) NOT NULL,
	[jvh_upd_ref_no] [numeric](10, 0) NOT NULL,
	[jvh_upd_dt] [date] NOT NULL,
	[jvh_inv_brh] [char](3) NOT NULL,
	[jvh_inv_pfx] [char](2) NOT NULL,
	[jvh_inv_no] [numeric](8, 0) NOT NULL,
	[jvh_inv_typ] [char](1) NOT NULL,
	[jvh_inv_mthd] [char](1) NOT NULL,
	[jvh_sld_cus_id] [char](8) NOT NULL,
	[jvh_cr_ctl_cus_id] [char](8) NULL,
	[jvh_job_id] [char](8) NULL,
	[jvh_rsn_typ] [char](3) NULL,
	[jvh_rsn] [char](3) NULL,
	[jvh_rsn_ref] [char](10) NOT NULL,
	[jvh_cry] [char](3) NOT NULL,
	[jvh_ex_rt] [numeric](12, 8) NOT NULL,
	[jvh_ex_rt_typ] [char](1) NOT NULL,
	[jvh_pmt_trm] [numeric](2, 0) NOT NULL,
	[jvh_disc_trm] [numeric](2, 0) NULL,
	[jvh_mthd_pmt] [char](2) NOT NULL,
	[jvh_upd_ref] [char](22) NOT NULL,
	[jvh_ar_val] [numeric](15, 2) NOT NULL,
	[jvh_inv_upd_dt] [date] NOT NULL,
	[jvh_inv_dt] [date] NOT NULL,
	[jvh_ar_disc_val] [numeric](15, 2) NOT NULL,
	[jvh_disc_dt] [date] NULL,
	[jvh_inv_due_dt] [date] NOT NULL,
	[jvh_sls_cat] [char](2) NOT NULL,
	[jvh_lgn_id] [char](8) NOT NULL,
	[jvh_not_comp_itm] [numeric](3, 0) NOT NULL,
	[jvh_flt] [char](3) NULL
) ON [PRIMARY]
GO
