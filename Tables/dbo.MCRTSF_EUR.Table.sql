USE [Stratix_US]
GO
/****** Object:  Table [dbo].[MCRTSF_EUR]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MCRTSF_EUR](
	[tsf_mss_ctl_no] [numeric](10, 0) NOT NULL,
	[tsf_tst_abbr] [char](5) NOT NULL,
	[tsf_thck_to] [numeric](9, 4) NOT NULL,
	[tsf_tst_seq_no] [numeric](2, 0) NOT NULL,
	[tsf_min_tst_val] [numeric](8, 2) NOT NULL,
	[tsf_max_tst_val] [numeric](8, 2) NOT NULL,
	[tsf_um] [char](3) NOT NULL
) ON [PRIMARY]
GO
