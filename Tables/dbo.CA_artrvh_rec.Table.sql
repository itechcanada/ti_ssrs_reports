USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_artrvh_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_artrvh_rec](
	[rvh_cmpy_id] [char](3) NOT NULL,
	[rvh_ar_pfx] [char](2) NOT NULL,
	[rvh_ar_no] [numeric](8, 0) NOT NULL,
	[rvh_cr_ctl_cus_id] [char](8) NOT NULL,
	[rvh_sld_cus_id] [char](8) NOT NULL,
	[rvh_extl_ref] [char](22) NOT NULL,
	[rvh_desc30] [char](30) NOT NULL,
	[rvh_rsn_typ] [char](3) NULL,
	[rvh_rsn] [char](3) NULL,
	[rvh_rsn_ref] [char](10) NOT NULL,
	[rvh_job_id] [char](8) NULL,
	[rvh_cry] [char](3) NOT NULL,
	[rvh_exrt] [numeric](13, 8) NOT NULL,
	[rvh_ex_rt_typ] [char](1) NOT NULL,
	[rvh_pttrm] [numeric](3, 0) NOT NULL,
	[rvh_disc_trm] [numeric](2, 0) NULL,
	[rvh_due_nbr_dy] [numeric](3, 0) NOT NULL,
	[rvh_disc_nbr_dy] [numeric](3, 0) NOT NULL,
	[rvh_disc_rt] [numeric](5, 2) NOT NULL,
	[rvh_due_dt] [date] NOT NULL,
	[rvh_disc_dt] [date] NULL,
	[rvh_disc_amt] [numeric](12, 2) NOT NULL,
	[rvh_ar_brh] [char](3) NOT NULL,
	[rvh_upd_ref_no] [numeric](10, 0) NOT NULL,
	[rvh_upd_dt] [date] NULL,
	[rvh_upd_ref] [char](22) NOT NULL,
	[rvh_inv_dt] [date] NOT NULL,
	[rvh_flwp_dt] [date] NULL,
	[rvh_orig_amt] [numeric](12, 2) NOT NULL,
	[rvh_ip_amt] [numeric](12, 2) NOT NULL,
	[rvh_balamt] [numeric](12, 2) NOT NULL,
	[rvh_arch_trs] [numeric](1, 0) NOT NULL,
	[rvh_arch_dt] [date] NOT NULL,
	[rvh_sls_qlf] [char](1) NOT NULL,
	[rvh_ord_pfx] [char](2) NOT NULL,
	[rvh_ord_no] [numeric](8, 0) NOT NULL
) ON [PRIMARY]
GO
