USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_permpb_rec_Copy]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_permpb_rec_Copy](
	[mpb_cmpy_id] [char](3) NOT NULL,
	[mpb_prc_bk_ctl_no] [numeric](10, 0) NOT NULL,
	[mpb_bk_typ] [char](1) NOT NULL,
	[mpb_prc_cat] [char](3) NOT NULL,
	[mpb_gnr_prc_cat] [numeric](1, 0) NOT NULL,
	[mpb_mpb_prd_cnfg] [char](1) NOT NULL,
	[mpb_cus_id] [char](8) NOT NULL,
	[mpb_frm] [char](6) NOT NULL,
	[mpb_grd] [char](8) NOT NULL,
	[mpb_size] [char](15) NOT NULL,
	[mpb_fnsh] [char](8) NOT NULL,
	[mpb_fm_wdth] [numeric](9, 4) NOT NULL,
	[mpb_to_wdth] [numeric](9, 4) NOT NULL,
	[mpb_fm_lgth] [numeric](9, 4) NOT NULL,
	[mpb_to_lgth] [numeric](9, 4) NOT NULL,
	[mpb_part] [char](30) NOT NULL,
	[mpb_part_rev_no] [char](2) NOT NULL,
	[mpb_ctrct_prc_bk] [numeric](1, 0) NOT NULL,
	[mpb_lib_id] [char](10) NULL,
	[mpb_lng] [char](2) NOT NULL,
	[mpb_rvw_dt] [date] NULL,
	[mpb_ctrct] [char](15) NOT NULL,
	[mpb_ctrct_prc] [numeric](13, 4) NOT NULL,
	[mpb_ctrct_prc_um] [char](3) NULL,
	[mpb_cry] [char](3) NOT NULL,
	[mpb_cpr_expy_dt] [date] NULL,
	[mpb_ncpr] [numeric](13, 4) NOT NULL,
	[mpb_ncpr_expy_dt] [date] NULL,
	[mpb_cmdty] [char](2) NOT NULL,
	[mpb_qty_brk_um] [char](3) NOT NULL,
	[mpb_alt_qty_brk_um] [char](3) NULL,
	[mpb_qty_mkup_typ] [char](1) NOT NULL,
	[mpb_qty_disc_typ] [char](1) NOT NULL
) ON [PRIMARY]
GO
