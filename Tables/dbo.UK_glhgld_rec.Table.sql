USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_glhgld_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_glhgld_rec](
	[gld_cr_amt] [numeric](12, 2) NOT NULL,
	[gld_dr_amt] [numeric](12, 2) NOT NULL,
	[gld_bsc_gl_acct] [char](8) NOT NULL,
	[gld_actvy_Dt] [date] NOT NULL,
	[gld_sacct] [char](30) NOT NULL,
	[gld_acctg_per] [numeric](6, 0) NOT NULL
) ON [PRIMARY]
GO
