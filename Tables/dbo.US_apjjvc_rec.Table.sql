USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_apjjvc_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_apjjvc_rec](
	[jvc_cmpy_id] [char](3) NOT NULL,
	[jvc_vchr_pfx] [char](2) NOT NULL,
	[jvc_vchr_no] [numeric](8, 0) NOT NULL,
	[jvc_ssn_id] [numeric](6, 0) NOT NULL,
	[jvc_ent_dt] [date] NOT NULL,
	[jvc_lgn_id] [char](8) NOT NULL,
	[jvc_ven_id] [char](8) NOT NULL,
	[jvc_ven_inv_no] [char](22) NOT NULL,
	[jvc_extl_ref] [char](22) NOT NULL,
	[jvc_ven_inv_dt] [date] NOT NULL,
	[jvc_po_pfx] [char](2) NULL,
	[jvc_po_no] [numeric](8, 0) NOT NULL,
	[jvc_po_itm] [numeric](3, 0) NOT NULL,
	[jvc_mt_pfx] [char](2) NOT NULL,
	[jvc_mt_no] [numeric](8, 0) NOT NULL,
	[jvc_mt_itm] [numeric](3, 0) NOT NULL,
	[jvc_voyg_no] [char](15) NOT NULL,
	[jvc_vchr_brh] [char](3) NOT NULL,
	[jvc_ptx_vchr_amt] [numeric](12, 2) NOT NULL,
	[jvc_vchr_amt] [numeric](12, 2) NOT NULL,
	[jvc_dscb_amt] [numeric](12, 2) NOT NULL,
	[jvc_desc30] [char](30) NOT NULL,
	[jvc_cry] [char](3) NOT NULL,
	[jvc_exrt] [numeric](13, 8) NOT NULL,
	[jvc_ex_rt_typ] [char](1) NOT NULL,
	[jvc_pttrm] [numeric](3, 0) NOT NULL,
	[jvc_disc_trm] [numeric](2, 0) NULL,
	[jvc_due_dt] [date] NOT NULL,
	[jvc_disc_dt] [date] NULL,
	[jvc_disc_amt] [numeric](12, 2) NOT NULL,
	[jvc_auth_ref] [char](22) NOT NULL,
	[jvc_vchr_cat] [char](4) NOT NULL,
	[jvc_svc_ffm_dt] [date] NULL,
	[jvc_upd_dt] [date] NOT NULL
) ON [PRIMARY]
GO
