USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_scrslp_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_scrslp_rec](
	[slp_cmpy_id] [char](3) NOT NULL,
	[slp_slp] [char](4) NOT NULL,
	[slp_lgn_id] [char](8) NOT NULL,
	[slp_prc_var_pct] [numeric](5, 2) NOT NULL,
	[slp_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
