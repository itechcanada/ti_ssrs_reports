USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_plrppp_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_plrppp_rec](
	[ppp_cmpy_id] [char](3) NOT NULL,
	[ppp_purch_pt] [char](5) NOT NULL,
	[ppp_rpd_ctl_no] [numeric](10, 0) NOT NULL,
	[ppp_purch_pt_tmpl] [char](4) NOT NULL,
	[ppp_min_pur_qty] [numeric](16, 4) NOT NULL,
	[ppp_eo_qty] [numeric](16, 4) NOT NULL,
	[ppp_sls_swks_flg] [numeric](1, 0) NOT NULL,
	[ppp_plng_rmk] [char](50) NOT NULL,
	[ppp_rptg_per_fmt] [char](2) NOT NULL,
	[ppp_actst_dt] [date] NOT NULL
) ON [PRIMARY]
GO
