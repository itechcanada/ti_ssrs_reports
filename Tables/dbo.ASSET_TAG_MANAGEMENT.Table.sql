USE [Stratix_US]
GO
/****** Object:  Table [dbo].[ASSET_TAG_MANAGEMENT]    Script Date: 03-11-2021 16:26:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ASSET_TAG_MANAGEMENT](
	[asset_tag] [varchar](20) NULL,
	[location] [varchar](15) NOT NULL,
	[manufacture] [varchar](50) NULL,
	[model_nbr] [varchar](50) NULL,
	[serial_nbr] [varchar](50) NULL,
	[service_tag] [varchar](50) NULL,
	[express_service_code] [varchar](50) NULL,
	[purchased_dt] [varchar](25) NULL,
	[price] [float] NULL
) ON [PRIMARY]
GO
