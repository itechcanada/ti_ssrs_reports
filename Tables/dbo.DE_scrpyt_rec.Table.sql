USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_scrpyt_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_scrpyt_rec](
	[pyt_pttrm] [numeric](3, 0) NOT NULL,
	[pyt_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[pyt_desc30] [char](30) NOT NULL,
	[pyt_pmt_trm_typ] [char](3) NOT NULL,
	[pyt_due_nbr_mth] [numeric](2, 0) NOT NULL,
	[pyt_due_nbr_dy] [numeric](3, 0) NOT NULL,
	[pyt_cut_off_dy] [numeric](2, 0) NOT NULL,
	[pyt_due_dy_1] [numeric](2, 0) NOT NULL,
	[pyt_due_dy_2] [numeric](2, 0) NOT NULL,
	[pyt_intpr_due] [char](3) NOT NULL,
	[pyt_actvy_per] [char](3) NOT NULL,
	[pyt_actvy_dy] [numeric](2, 0) NOT NULL,
	[pyt_instlm_pmt] [numeric](1, 0) NOT NULL,
	[pyt_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
