USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_cprclg_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_cprclg_rec](
	[clg_cmpy_id] [char](3) NOT NULL,
	[clg_part_sts] [char](1) NOT NULL,
	[clg_part_ctl_no] [numeric](10, 0) NOT NULL,
	[clg_part_ln_no] [numeric](3, 0) NOT NULL,
	[clg_cus_ven_typ] [char](1) NOT NULL,
	[clg_cus_ven_id] [char](8) NOT NULL,
	[clg_part] [char](30) NOT NULL,
	[clg_part_rev_no] [char](2) NOT NULL,
	[clg_prt_extl_doc] [char](1) NOT NULL,
	[clg_low_part] [char](30) NOT NULL,
	[clg_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
