USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_ivjjch_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_ivjjch_rec](
	[jch_cmpy_id] [char](3) NOT NULL,
	[jch_upd_ref_no] [numeric](10, 0) NOT NULL,
	[jch_upd_itm] [numeric](3, 0) NOT NULL,
	[jch_upd_dt] [date] NOT NULL,
	[jch_upd_ln_no] [numeric](2, 0) NOT NULL,
	[jch_chrg_no] [numeric](3, 0) NOT NULL,
	[jch_chrg_desc20] [char](20) NOT NULL,
	[jch_cc_seq_no] [numeric](2, 0) NOT NULL,
	[jch_cc_typ] [numeric](1, 0) NOT NULL,
	[jch_chrg_cl] [char](1) NOT NULL,
	[jch_prs_chrg] [numeric](1, 0) NOT NULL,
	[jch_chrg_orig] [char](1) NOT NULL,
	[jch_chrg_qty] [numeric](16, 4) NOT NULL,
	[jch_chrg_qty_um] [char](3) NULL,
	[jch_chrg] [numeric](13, 4) NOT NULL,
	[jch_chrg_um] [char](3) NULL,
	[jch_chrg_val] [numeric](12, 2) NOT NULL,
	[jch_disc] [numeric](13, 4) NOT NULL,
	[jch_disc_um] [char](3) NULL,
	[jch_disc_val] [numeric](12, 2) NOT NULL,
	[jch_net_chrg_val] [numeric](12, 2) NOT NULL,
	[jch_ovrd_chrg] [numeric](1, 0) NOT NULL,
	[jch_cst_ref_ln_no] [numeric](2, 0) NOT NULL
) ON [PRIMARY]
GO
