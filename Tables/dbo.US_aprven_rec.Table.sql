USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_aprven_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_aprven_rec](
	[ven_cmpy_id] [char](3) NOT NULL,
	[ven_ven_id] [char](8) NOT NULL,
	[ven_ven_nm] [char](15) NOT NULL,
	[ven_ven_long_nm] [char](35) NOT NULL,
	[ven_ven_acct_typ] [char](1) NOT NULL,
	[ven_cmn_ven_id] [char](8) NULL,
	[ven_cry] [char](3) NOT NULL,
	[ven_aly_schg_apln] [char](1) NOT NULL,
	[ven_admin_brh] [char](3) NOT NULL,
	[ven_sic] [char](8) NULL,
	[ven_web_site] [char](50) NOT NULL,
	[ven_lng] [char](2) NOT NULL,
	[ven_fis_id] [char](16) NOT NULL,
	[ven_1099_cat] [char](4) NULL,
	[ven_1099_tx_id] [char](15) NOT NULL,
	[ven_ven_cat] [char](2) NULL,
	[ven_ref_id1] [char](20) NOT NULL,
	[ven_ref_id2] [char](20) NOT NULL,
	[ven_mkt_typ] [char](3) NULL,
	[ven_rltd_cus_id] [char](8) NULL,
	[ven_ven_cr_lim] [numeric](15, 2) NOT NULL,
	[ven_pmt_typ] [char](1) NOT NULL,
	[ven_pmt_hld] [numeric](1, 0) NOT NULL,
	[ven_prnt_org] [char](6) NULL,
	[ven_rltnshp] [char](1) NULL,
	[ven_bnk_nm] [char](35) NOT NULL,
	[ven_ins_expy_dt] [date] NULL,
	[ven_dun_no] [numeric](9, 0) NOT NULL,
	[ven_upd_dtts] [datetime2](7) NULL,
	[ven_upd_dtms] [numeric](3, 0) NULL,
	[ven_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
