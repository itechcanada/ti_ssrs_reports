USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_pohrhr_rec_copy]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_pohrhr_rec_copy](
	[rhr_cmpy_id] [char](3) NOT NULL,
	[rhr_po_pfx] [char](2) NOT NULL,
	[rhr_po_no] [numeric](8, 0) NOT NULL,
	[rhr_po_itm] [numeric](3, 0) NOT NULL,
	[rhr_po_crtd_dt] [date] NOT NULL,
	[rhr_ref_pfx] [char](2) NOT NULL,
	[rhr_ref_no] [numeric](8, 0) NOT NULL,
	[rhr_ref_itm] [numeric](3, 0) NOT NULL,
	[rhr_rcvg_whs] [char](3) NOT NULL,
	[rhr_rcpt_actvy_dt] [date] NULL,
	[rhr_rcpt_upd_dt] [date] NULL,
	[rhr_mtl_cst] [numeric](13, 4) NOT NULL,
	[rhr_mtl_cst_um] [char](3) NULL,
	[rhr_tot_cst] [numeric](13, 4) NOT NULL,
	[rhr_qty_cst_um] [char](3) NULL,
	[rhr_cry] [char](3) NOT NULL,
	[rhr_ex_rt] [numeric](12, 8) NOT NULL,
	[rhr_ex_rt_typ] [char](1) NOT NULL,
	[rhr_rcvd_pcs] [numeric](7, 0) NOT NULL,
	[rhr_rcvd_msr] [numeric](16, 4) NOT NULL,
	[rhr_rcvd_wgt] [numeric](10, 2) NOT NULL,
	[rhr_rcvd_qty] [numeric](16, 4) NOT NULL,
	[rhr_trm_trd] [char](3) NOT NULL,
	[rhr_diff_prd] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
