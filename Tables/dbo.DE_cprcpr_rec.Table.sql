USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_cprcpr_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_cprcpr_rec](
	[cpr_cmpy_id] [char](3) NOT NULL,
	[cpr_part_sts] [char](1) NOT NULL,
	[cpr_part_ctl_no] [numeric](10, 0) NOT NULL,
	[cpr_part_desc50] [char](50) NOT NULL,
	[cpr_prs_rvw] [numeric](1, 0) NOT NULL,
	[cpr_prd_prtct] [char](1) NOT NULL,
	[cpr_cons_prd_prtct] [char](1) NOT NULL,
	[cpr_instr_prtct] [char](1) NOT NULL,
	[cpr_spec_prtct] [char](1) NOT NULL,
	[cpr_prs_rtng_prtct] [char](1) NOT NULL,
	[cpr_tst_cert_prtct] [char](1) NOT NULL,
	[cpr_crtd_lgn_id] [char](8) NOT NULL,
	[cpr_crtd_dtts] [datetime2](7) NULL,
	[cpr_crtd_dtms] [numeric](3, 0) NULL,
	[cpr_part_apvd] [numeric](1, 0) NOT NULL,
	[cpr_apvl_lgn_id] [char](8) NULL,
	[cpr_apvd_dt] [date] NULL,
	[cpr_lst_chg_dtts] [datetime2](7) NULL,
	[cpr_lst_chg_dtms] [numeric](3, 0) NULL,
	[cpr_lst_usd_dtts] [datetime2](7) NULL,
	[cpr_lst_usd_dtms] [numeric](3, 0) NULL,
	[cpr_sls_cat] [char](2) NOT NULL,
	[cpr_std_prs_rte] [char](3) NOT NULL,
	[cpr_src] [char](2) NOT NULL,
	[cpr_end_use] [char](3) NULL,
	[cpr_end_use_desc] [char](50) NOT NULL,
	[cpr_min_stk_qty] [numeric](16, 4) NOT NULL,
	[cpr_wgt_per_pcs] [numeric](10, 4) NOT NULL,
	[cpr_min_fnsh_qty] [numeric](16, 4) NOT NULL,
	[cpr_prd_cyc_tm] [numeric](2, 0) NOT NULL,
	[cpr_rvw_dt] [date] NULL,
	[cpr_rev_dt] [date] NOT NULL,
	[cpr_vld_sts] [char](1) NOT NULL,
	[cpr_chrg_qty_typ] [char](1) NOT NULL,
	[cpr_actv] [numeric](1, 0) NOT NULL,
	[cpr_apl_reb] [numeric](1, 0) NOT NULL,
	[cpr_admin_brh] [char](3) NULL,
	[cpr_invt_typ] [char](1) NOT NULL,
	[cpr_prt_pkg_cmpt] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
