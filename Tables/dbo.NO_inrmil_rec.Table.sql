USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_inrmil_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_inrmil_rec](
	[mil_mill] [char](3) NOT NULL,
	[mil_nm] [char](35) NOT NULL,
	[mil_orig_cty] [char](3) NULL,
	[mil_unknw_orig] [numeric](1, 0) NOT NULL,
	[mil_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
