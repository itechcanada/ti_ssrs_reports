USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_intacp_rec_94]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_intacp_rec_94](
	[acp_cmpy_id] [char](3) NOT NULL,
	[acp_avg_cst_pool] [numeric](10, 0) NOT NULL,
	[acp_frm] [char](6) NOT NULL,
	[acp_grd] [char](8) NOT NULL,
	[acp_size] [char](15) NOT NULL,
	[acp_fnsh] [char](8) NOT NULL,
	[acp_ef_svar] [char](30) NOT NULL,
	[acp_cus_id] [char](8) NULL,
	[acp_avg_cst_dim_id] [numeric](10, 0) NULL,
	[acp_csp_ctl_no] [numeric](10, 0) NULL,
	[acp_brh] [char](3) NOT NULL,
	[acp_whs] [char](3) NOT NULL,
	[acp_invt_typ_grp] [char](3) NULL,
	[acp_invt_typ] [char](1) NULL,
	[acp_invt_qlty] [char](1) NOT NULL,
	[acp_invt_cat] [char](1) NULL,
	[acp_orig_zn] [char](3) NULL,
	[acp_tot_wgt] [numeric](12, 2) NOT NULL,
	[acp_tot_qty] [numeric](18, 4) NOT NULL,
	[acp_tot_mat_cst] [numeric](13, 4) NOT NULL,
	[acp_tot_mat_val] [numeric](15, 2) NOT NULL,
	[acp_tot_bk_wgt] [numeric](12, 2) NOT NULL,
	[acp_tot_bk_qty] [numeric](18, 4) NOT NULL,
	[acp_tot_bk_mat_cst] [numeric](13, 4) NOT NULL,
	[acp_tot_bk_mat_val] [numeric](15, 2) NOT NULL,
	[acp_avg_cst_sts] [char](1) NOT NULL,
	[acp_std_prd_sts] [char](1) NOT NULL
) ON [PRIMARY]
GO
