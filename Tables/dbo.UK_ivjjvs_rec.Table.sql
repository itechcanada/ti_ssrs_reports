USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_ivjjvs_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_ivjjvs_rec](
	[jvs_cmpy_id] [char](3) NOT NULL,
	[jvs_ref_pfx] [char](2) NOT NULL,
	[jvs_ref_no] [numeric](8, 0) NOT NULL,
	[jvs_upd_dt] [date] NOT NULL,
	[jvs_shpt_brh] [char](3) NOT NULL,
	[jvs_sld_cus_id] [char](8) NOT NULL,
	[jvs_shp_to] [numeric](3, 0) NOT NULL,
	[jvs_cus_po] [char](30) NULL,
	[jvs_transp_pfx] [char](2) NULL,
	[jvs_transp_no] [numeric](8, 0) NULL,
	[jvs_pk_list_no] [numeric](3, 0) NOT NULL,
	[jvs_ord_pfx] [char](2) NOT NULL,
	[jvs_ord_no] [numeric](8, 0) NOT NULL,
	[jvs_shpg_whs] [char](3) NULL,
	[jvs_shp_dt] [date] NULL,
	[jvs_upd_ref_no] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
GO
