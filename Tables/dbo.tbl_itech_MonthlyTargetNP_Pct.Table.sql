USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_MonthlyTargetNP_Pct]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_MonthlyTargetNP_Pct](
	[idMonthlyTarget] [int] NOT NULL,
	[branchName] [varchar](50) NOT NULL,
	[valueOfType] [varchar](50) NOT NULL,
	[janTarget] [decimal](20, 1) NOT NULL,
	[febTarget] [decimal](20, 1) NOT NULL,
	[marTarget] [decimal](20, 1) NOT NULL,
	[aprTarget] [decimal](20, 1) NOT NULL,
	[mayTarget] [decimal](20, 1) NOT NULL,
	[junTarget] [decimal](20, 1) NOT NULL,
	[julTarget] [decimal](20, 1) NOT NULL,
	[augTarget] [decimal](20, 1) NOT NULL,
	[sepTarget] [decimal](20, 1) NOT NULL,
	[octTarget] [decimal](20, 1) NOT NULL,
	[novTarget] [decimal](20, 1) NOT NULL,
	[decTarget] [decimal](20, 1) NOT NULL
) ON [PRIMARY]
GO
