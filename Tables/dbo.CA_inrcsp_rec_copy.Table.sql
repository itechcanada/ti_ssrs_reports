USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_inrcsp_rec_copy]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_inrcsp_rec_copy](
	[csp_cmpy_id] [char](3) NOT NULL,
	[csp_std_prd_sts] [char](1) NOT NULL,
	[csp_csp_ctl_no] [numeric](10, 0) NOT NULL,
	[csp_bgt_for_cus_id] [char](8) NULL,
	[csp_frm] [char](6) NOT NULL,
	[csp_grd] [char](8) NOT NULL,
	[csp_size] [char](15) NOT NULL,
	[csp_fnsh] [char](8) NOT NULL,
	[csp_std_wdth] [numeric](9, 4) NOT NULL,
	[csp_std_lgth] [numeric](9, 4) NOT NULL,
	[csp_std_idia] [numeric](9, 5) NOT NULL,
	[csp_std_odia] [numeric](9, 5) NOT NULL,
	[csp_std_ga_size] [numeric](8, 5) NOT NULL,
	[csp_wdth_fm] [numeric](9, 4) NOT NULL,
	[csp_wdth_to] [numeric](9, 4) NOT NULL,
	[csp_lgth_fm] [numeric](9, 4) NOT NULL,
	[csp_lgth_to] [numeric](9, 4) NOT NULL,
	[csp_idia_fm] [numeric](9, 5) NOT NULL,
	[csp_idia_to] [numeric](9, 5) NOT NULL,
	[csp_odia_fm] [numeric](9, 5) NOT NULL,
	[csp_odia_to] [numeric](9, 5) NOT NULL,
	[csp_ga_size_fm] [numeric](8, 5) NOT NULL,
	[csp_ga_size_to] [numeric](8, 5) NOT NULL,
	[csp_gnrt_std_prd] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
