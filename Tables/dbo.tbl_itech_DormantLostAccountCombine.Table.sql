USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_DormantLostAccountCombine]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_DormantLostAccountCombine](
	[ExecutionDate] [date] NOT NULL,
	[id] [int] NULL,
	[AccountType] [varchar](100) NULL,
	[CustomerID] [varchar](50) NULL,
	[Category] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[AccountDate] [varchar](50) NULL,
	[Months] [varchar](20) NULL,
	[Branch] [varchar](50) NULL,
	[SalePersonName] [varchar](50) NULL,
	[Database] [varchar](50) NULL
) ON [PRIMARY]
GO
