USE [Stratix_US]
GO
/****** Object:  Table [dbo].[EATON_MI_PRICING]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EATON_MI_PRICING](
	[SPEC] [nvarchar](255) NULL,
	[DIAMETER] [float] NULL,
	[SUBS] [money] NULL,
	[EATON] [float] NULL,
	[FGSF] [nvarchar](255) NULL,
	[Form] [nvarchar](255) NULL,
	[Grade] [nvarchar](255) NULL,
	[Size] [nvarchar](255) NULL,
	[Finish] [nvarchar](255) NULL
) ON [PRIMARY]
GO
