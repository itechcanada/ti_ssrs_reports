USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_itemnum3]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_itemnum3](
	[prm_frm] [char](6) NULL,
	[prm_grd] [char](8) NULL,
	[prm_size] [char](15) NULL,
	[prm_fnsh] [char](8) NULL,
	[item_num] [char](50) NULL
) ON [PRIMARY]
GO
