USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_cctatv_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_cctatv_rec](
	[atv_cmpy_id] [char](3) NOT NULL,
	[atv_crm_actvy_pfx] [char](2) NOT NULL,
	[atv_crm_actvy_no] [numeric](8, 0) NOT NULL,
	[atv_crm_actvy_typ] [char](3) NOT NULL,
	[atv_actvy_tsk_purp] [char](3) NOT NULL,
	[atv_actvy_strt_dt] [date] NULL,
	[atv_actvy_end_dt] [date] NULL,
	[atv_actvy_strt_tm] [numeric](4, 0) NULL,
	[atv_actvy_dur] [char](10) NOT NULL,
	[atv_actvy_loc] [char](35) NOT NULL,
	[atv_actvy_subj] [char](128) NOT NULL,
	[atv_crtd_lgn_id] [char](8) NOT NULL,
	[atv_crtd_dtts] [datetime2](7) NULL,
	[atv_crtd_dtms] [numeric](3, 0) NULL,
	[atv_upd_lgn_id] [char](8) NOT NULL,
	[atv_upd_dtts] [datetime2](7) NULL,
	[atv_upd_dtms] [numeric](3, 0) NULL,
	[atv_del_dtts] [datetime2](7) NULL,
	[atv_del_dtms] [numeric](3, 0) NULL,
	[atv_prnt_crm_pfx] [char](2) NOT NULL,
	[atv_prnt_crm_no] [numeric](8, 0) NOT NULL,
	[atv_cus_ven_typ] [char](1) NOT NULL,
	[atv_cus_ven_id] [char](8) NOT NULL,
	[atv_cmptr_cus_id] [char](8) NULL,
	[atv_brh] [char](3) NOT NULL,
	[atv_actvy_txt] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
