USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_potpoi_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_potpoi_rec](
	[poi_cmpy_id] [char](3) NOT NULL,
	[poi_po_pfx] [char](2) NOT NULL,
	[poi_po_no] [numeric](8, 0) NOT NULL,
	[poi_po_itm] [numeric](3, 0) NOT NULL,
	[poi_po_brh] [char](3) NOT NULL,
	[poi_ord_pcs] [numeric](7, 0) NOT NULL,
	[poi_ord_msr] [numeric](16, 4) NOT NULL,
	[poi_ord_wgt] [numeric](10, 2) NOT NULL,
	[poi_ord_qty] [numeric](16, 4) NOT NULL,
	[poi_ord_wgt_um] [char](3) NULL,
	[poi_rcvd_pcs] [numeric](7, 0) NOT NULL,
	[poi_rcvd_msr] [numeric](16, 4) NOT NULL,
	[poi_rcvd_wgt] [numeric](10, 2) NOT NULL,
	[poi_rcvd_qty] [numeric](16, 4) NOT NULL,
	[poi_bal_pcs] [numeric](7, 0) NOT NULL,
	[poi_bal_msr] [numeric](16, 4) NOT NULL,
	[poi_bal_wgt] [numeric](10, 2) NOT NULL,
	[poi_bal_qty] [numeric](16, 4) NOT NULL,
	[poi_bkt_pcs] [numeric](7, 0) NOT NULL,
	[poi_bkt_msr] [numeric](16, 4) NOT NULL,
	[poi_bkt_wgt] [numeric](10, 2) NOT NULL,
	[poi_bkt_qty] [numeric](16, 4) NOT NULL,
	[poi_bkt_wgt_um] [char](3) NULL,
	[poi_consg] [numeric](1, 0) NOT NULL,
	[poi_pur_cat] [char](2) NOT NULL,
	[poi_src] [char](2) NOT NULL,
	[poi_alw_prtl_shp] [numeric](1, 0) NOT NULL,
	[poi_ovrshp_pct] [numeric](5, 2) NOT NULL,
	[poi_undshp_pct] [numeric](5, 2) NOT NULL,
	[poi_invt_qlty] [char](1) NULL,
	[poi_aly_schg_apln] [char](1) NOT NULL,
	[poi_tot_cst] [numeric](13, 4) NOT NULL,
	[poi_qty_cst_um] [char](3) NULL,
	[poi_dsgd_shp_whs] [char](3) NULL,
	[poi_ven_prc_src] [char](1) NOT NULL,
	[poi_prc_bk_ctl_no] [numeric](10, 0) NULL,
	[poi_prc_in_eff] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
