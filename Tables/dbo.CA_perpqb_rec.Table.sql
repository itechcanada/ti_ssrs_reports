USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_perpqb_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_perpqb_rec](
	[pqb_cmpy_id] [char](3) NOT NULL,
	[pqb_prc_bk_ctl_no] [numeric](10, 0) NOT NULL,
	[pqb_qty_brkt] [numeric](16, 4) NOT NULL,
	[pqb_qty_mkup_rt] [numeric](13, 4) NOT NULL,
	[pqb_qty_mkup_pct] [numeric](5, 2) NOT NULL,
	[pqb_qty_disc_rt] [numeric](13, 4) NOT NULL,
	[pqb_qty_disc_pct] [numeric](5, 2) NOT NULL
) ON [PRIMARY]
GO
