USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_iptjbs_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_iptjbs_rec](
	[jbs_cmpy_id] [char](3) NOT NULL,
	[jbs_jbs_pfx] [char](2) NOT NULL,
	[jbs_jbs_no] [numeric](8, 0) NOT NULL,
	[jbs_cus_ownd] [numeric](1, 0) NOT NULL,
	[jbs_cus_id] [char](8) NULL,
	[jbs_plng_whs] [char](3) NOT NULL,
	[jbs_ent_msr] [char](1) NOT NULL,
	[jbs_nbr_plnd_ord] [numeric](3, 0) NOT NULL,
	[jbs_jbs_cl] [char](1) NOT NULL,
	[jbs_nbr_strm] [numeric](2, 0) NOT NULL,
	[jbs_ord_pfx] [char](2) NOT NULL,
	[jbs_ord_no] [numeric](8, 0) NOT NULL,
	[jbs_prod_cpost] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
