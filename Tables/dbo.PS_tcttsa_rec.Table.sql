USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_tcttsa_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_tcttsa_rec](
	[TSA_CMPY_ID] [char](3) NOT NULL,
	[TSA_REF_PFX] [char](2) NOT NULL,
	[TSA_REF_NO] [decimal](8, 0) NOT NULL,
	[TSA_REF_ITM] [decimal](3, 0) NOT NULL,
	[TSA_REF_SBITM] [decimal](4, 0) NOT NULL,
	[TSA_STS_TYP] [char](1) NOT NULL,
	[TSA_STS_ACTN] [char](1) NOT NULL,
	[TSA_RSN_TYP] [char](3) NULL,
	[TSA_RSN] [char](3) NULL,
	[TSA_STS] [char](3) NULL,
	[TSA_RMK] [char](50) NOT NULL,
	[TSA_CRTD_DTTS] [datetime] NULL,
	[TSA_CRTD_DTMS] [decimal](3, 0) NULL,
	[TSA_LST_UPD_DTTS] [datetime] NULL,
	[TSA_LST_UPD_DTMS] [decimal](3, 0) NULL
) ON [PRIMARY]
GO
