USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_scrsic_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_scrsic_rec](
	[sic_sic] [char](8) NOT NULL,
	[sic_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[sic_desc30] [char](30) NOT NULL,
	[sic_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
