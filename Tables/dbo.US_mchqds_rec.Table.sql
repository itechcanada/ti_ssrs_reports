USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_mchqds_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_mchqds_rec](
	[qds_cmpy_id] [char](3) NOT NULL,
	[qds_qds_ctl_no] [numeric](10, 0) NOT NULL,
	[qds_alt_ctl_no] [numeric](10, 0) NOT NULL,
	[qds_whs] [char](3) NOT NULL,
	[qds_mill] [char](3) NOT NULL,
	[qds_heat] [char](15) NOT NULL,
	[qds_qds_sts] [char](3) NULL,
	[qds_qds_typ] [char](1) NOT NULL,
	[qds_apvd_flg] [numeric](1, 0) NOT NULL,
	[qds_nbr_intl_cert] [numeric](3, 0) NOT NULL,
	[qds_cert_rcvd] [numeric](1, 0) NOT NULL,
	[qds_orig_pfx] [char](2) NOT NULL,
	[qds_orig_no] [numeric](8, 0) NOT NULL,
	[qds_orig_itm] [numeric](3, 0) NOT NULL,
	[qds_orig_sbitm] [numeric](4, 0) NOT NULL,
	[qds_crtd_dt] [date] NOT NULL,
	[qds_crtd_pfx] [char](2) NOT NULL,
	[qds_crtd_no] [numeric](8, 0) NOT NULL,
	[qds_crtd_itm] [numeric](3, 0) NOT NULL,
	[qds_crtd_sbitm] [numeric](4, 0) NOT NULL,
	[qds_orig_po_pfx] [char](2) NOT NULL,
	[qds_orig_po_no] [numeric](8, 0) NOT NULL,
	[qds_orig_po_itm] [numeric](3, 0) NOT NULL,
	[qds_orig_po_dist] [numeric](2, 0) NOT NULL,
	[qds_mill_ord_no] [char](25) NOT NULL,
	[qds_ven_id] [char](8) NOT NULL,
	[qds_apvd_lgn_id] [char](8) NULL,
	[qds_apvd_upd_dtts] [datetime2](7) NULL,
	[qds_apvd_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
