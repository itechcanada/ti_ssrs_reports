USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Last_search]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Last_search](
	[Search_id] [int] NOT NULL,
	[User_ID] [varchar](70) NULL,
	[Formsearch] [varchar](10) NULL,
	[SearchType] [varchar](10) NULL,
	[SearchString] [varchar](100) NULL,
	[SearchDateFrom] [datetime] NULL,
	[SearchDateTo] [datetime] NULL,
	[DocType] [varchar](25) NULL,
	[SearchedDateTime] [datetime] NULL,
 CONSTRAINT [PK_Last_search] PRIMARY KEY CLUSTERED 
(
	[Search_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
