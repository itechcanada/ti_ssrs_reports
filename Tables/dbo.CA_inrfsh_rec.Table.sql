USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_inrfsh_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_inrfsh_rec](
	[fsh_fnsh] [char](8) NOT NULL,
	[fsh_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[fsh_desc15] [char](15) NOT NULL,
	[fsh_extl_desc] [char](50) NOT NULL,
	[fsh_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
