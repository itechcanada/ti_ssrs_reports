USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_ccratp_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_ccratp_rec](
	[atp_actvy_tsk_purp] [char](3) NOT NULL,
	[atp_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[atp_desc30] [char](30) NOT NULL,
	[atp_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
