USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UOM]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UOM](
	[Product Number] [varchar](50) NULL,
	[Part Number] [varchar](50) NULL,
	[UOM] [varchar](50) NULL,
	[LBS] [varchar](50) NULL
) ON [PRIMARY]
GO
