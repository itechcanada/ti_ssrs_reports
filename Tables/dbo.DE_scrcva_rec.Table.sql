USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_scrcva_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_scrcva_rec](
	[cva_cmpy_id] [char](3) NOT NULL,
	[cva_ref_pfx] [char](2) NOT NULL,
	[cva_cus_ven_typ] [char](1) NOT NULL,
	[cva_cus_ven_id] [char](8) NOT NULL,
	[cva_addr_no] [numeric](3, 0) NOT NULL,
	[cva_addr_typ] [char](1) NOT NULL,
	[cva_nm1] [char](35) NOT NULL,
	[cva_nm2] [char](35) NOT NULL,
	[cva_addr1] [char](35) NOT NULL,
	[cva_addr2] [char](35) NOT NULL,
	[cva_addr3] [char](35) NOT NULL,
	[cva_city] [char](35) NOT NULL,
	[cva_pcd] [char](10) NOT NULL,
	[cva_latd] [numeric](11, 8) NOT NULL,
	[cva_lngtd] [numeric](11, 8) NOT NULL,
	[cva_cty] [char](3) NOT NULL,
	[cva_st_prov] [char](2) NULL,
	[cva_transp_zn] [char](6) NOT NULL,
	[cva_tel_area_cd] [char](7) NOT NULL,
	[cva_tel_no] [char](15) NOT NULL,
	[cva_tel_ext] [char](5) NOT NULL,
	[cva_fax_area_cd] [char](7) NOT NULL,
	[cva_fax_no] [char](15) NOT NULL,
	[cva_fax_ext] [char](5) NOT NULL,
	[cva_email] [char](50) NOT NULL,
	[cva_tmzn] [char](5) NULL,
	[cva_upd_dtts] [datetime2](7) NULL,
	[cva_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
