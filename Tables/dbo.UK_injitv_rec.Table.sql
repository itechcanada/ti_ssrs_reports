USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_injitv_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_injitv_rec](
	[itv_cmpy_id] [char](3) NOT NULL,
	[itv_ref_pfx] [char](2) NOT NULL,
	[itv_ref_no] [numeric](8, 0) NOT NULL,
	[itv_ref_itm] [numeric](3, 0) NOT NULL,
	[itv_ref_sbitm] [numeric](4, 0) NOT NULL,
	[itv_actvy_dt] [date] NOT NULL,
	[itv_trs_seq_no] [numeric](8, 0) NOT NULL,
	[itv_trs_pcs] [numeric](7, 0) NOT NULL,
	[itv_trs_pcs_typ] [char](1) NOT NULL,
	[itv_trs_msr] [numeric](16, 4) NOT NULL,
	[itv_trs_msr_typ] [char](1) NOT NULL,
	[itv_trs_wgt] [numeric](10, 2) NOT NULL,
	[itv_trs_wgt_typ] [char](1) NOT NULL,
	[itv_trs_qty] [numeric](16, 4) NOT NULL,
	[itv_trs_qty_typ] [char](1) NOT NULL,
	[itv_bal_pcs] [numeric](7, 0) NOT NULL,
	[itv_bal_pcs_typ] [char](1) NOT NULL,
	[itv_bal_msr] [numeric](16, 4) NOT NULL,
	[itv_bal_msr_typ] [char](1) NOT NULL,
	[itv_bal_wgt] [numeric](10, 2) NOT NULL,
	[itv_bal_wgt_typ] [char](1) NOT NULL,
	[itv_bal_qty] [numeric](16, 4) NOT NULL,
	[itv_bal_qty_typ] [char](1) NOT NULL,
	[itv_trs_mat_cst] [numeric](13, 4) NOT NULL,
	[itv_trs_mat_val] [numeric](12, 2) NOT NULL,
	[itv_bal_mat_cst] [numeric](13, 4) NOT NULL,
	[itv_bal_mat_val] [numeric](15, 2) NOT NULL,
	[itv_rltd_pcs] [numeric](7, 0) NOT NULL,
	[itv_rltd_msr] [numeric](16, 4) NOT NULL,
	[itv_rltd_wgt] [numeric](10, 2) NOT NULL,
	[itv_rltd_qty] [numeric](16, 4) NOT NULL
) ON [PRIMARY]
GO
