USE [Stratix_US]
GO
/****** Object:  Table [dbo].[FmGrdSizefnsh]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FmGrdSizefnsh](
	[prm_frm] [char](6) NOT NULL,
	[prm_grd] [char](8) NOT NULL,
	[prm_size] [char](15) NOT NULL,
	[prm_fnsh] [nchar](8) NOT NULL
) ON [PRIMARY]
GO
