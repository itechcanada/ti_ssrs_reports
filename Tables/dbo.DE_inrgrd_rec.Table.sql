USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_inrgrd_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_inrgrd_rec](
	[grd_grd] [char](8) NOT NULL,
	[grd_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[grd_desc15] [char](15) NOT NULL,
	[grd_desc25] [char](25) NOT NULL,
	[grd_ceq_aplc] [numeric](1, 0) NOT NULL,
	[grd_di_aplc] [numeric](1, 0) NOT NULL,
	[grd_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
