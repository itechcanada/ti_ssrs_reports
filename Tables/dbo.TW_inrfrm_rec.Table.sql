USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_inrfrm_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_inrfrm_rec](
	[frm_frm] [char](6) NOT NULL,
	[frm_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[frm_desc15] [char](15) NOT NULL,
	[frm_desc25] [char](25) NOT NULL,
	[frm_untzd_part] [char](1) NOT NULL,
	[frm_kit_list] [numeric](1, 0) NOT NULL,
	[frm_sha] [char](3) NOT NULL,
	[frm_invt_ctl] [char](1) NOT NULL,
	[frm_invt_qty_typ] [char](1) NOT NULL,
	[frm_size_typ] [char](2) NOT NULL,
	[frm_disp_seq] [char](2) NOT NULL,
	[frm_dflt_dim_seg] [char](3) NOT NULL,
	[frm_dflt_sk_mthd] [char](3) NOT NULL,
	[frm_dflt_lgth_disp] [numeric](1, 0) NOT NULL,
	[frm_purch_dim_dsgn] [char](1) NOT NULL,
	[frm_sllg_dim_dsgn] [char](1) NOT NULL,
	[frm_consb] [numeric](1, 0) NOT NULL,
	[frm_non_invt] [numeric](1, 0) NOT NULL,
	[frm_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
