USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_ctrccr_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_ctrccr_rec](
	[ccr_cc_no] [numeric](3, 0) NOT NULL,
	[ccr_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[ccr_desc20] [char](20) NOT NULL,
	[ccr_cc_typ] [numeric](1, 0) NOT NULL,
	[ccr_cc_grp] [char](3) NOT NULL,
	[ccr_cc_sub_grp] [char](3) NOT NULL,
	[ccr_prs_chrg] [numeric](1, 0) NOT NULL,
	[ccr_cc_disc] [numeric](1, 0) NOT NULL,
	[ccr_aly_schg] [numeric](1, 0) NOT NULL,
	[ccr_prc_cst_src] [char](1) NOT NULL,
	[ccr_inv_chrg] [numeric](1, 0) NOT NULL,
	[ccr_cmp_chrg_val] [numeric](1, 0) NOT NULL,
	[ccr_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
