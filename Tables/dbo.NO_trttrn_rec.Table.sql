USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_trttrn_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_trttrn_rec](
	[trn_cmpy_id] [char](3) NOT NULL,
	[trn_transp_pfx] [char](2) NOT NULL,
	[trn_transp_no] [numeric](8, 0) NOT NULL,
	[trn_trpln_whs] [char](3) NOT NULL,
	[trn_sch_dtts] [datetime2](7) NULL,
	[trn_sch_dtms] [numeric](3, 0) NULL,
	[trn_trrte] [char](6) NOT NULL,
	[trn_rte_clr] [char](3) NULL,
	[trn_dlvy_mthd] [char](2) NOT NULL,
	[trn_frt_ven_id] [char](8) NULL,
	[trn_crr_nm] [char](35) NOT NULL,
	[trn_frt_cst] [numeric](13, 4) NOT NULL,
	[trn_frt_cst_um] [char](3) NULL,
	[trn_crr_ref_no] [char](22) NOT NULL,
	[trn_fl_cst] [numeric](13, 4) NOT NULL,
	[trn_fl_cst_um] [char](3) NULL,
	[trn_stp_chrg] [numeric](13, 4) NOT NULL,
	[trn_ovrd_frt] [numeric](1, 0) NOT NULL,
	[trn_frt_cry] [char](3) NULL,
	[trn_frt_exrt] [numeric](13, 8) NOT NULL,
	[trn_frt_ex_rt_typ] [char](1) NOT NULL,
	[trn_vcl_no] [char](10) NULL,
	[trn_drvr_1] [char](35) NOT NULL,
	[trn_drvr_2] [char](35) NOT NULL,
	[trn_trctr] [numeric](1, 0) NOT NULL,
	[trn_trlr_no] [char](10) NULL,
	[trn_trlr_typ] [char](2) NOT NULL,
	[trn_max_stp] [numeric](3, 0) NOT NULL,
	[trn_max_wgt] [numeric](10, 2) NOT NULL,
	[trn_max_wdth] [numeric](9, 4) NOT NULL,
	[trn_max_lgth] [numeric](9, 4) NOT NULL,
	[trn_opn_frt_val] [numeric](14, 4) NOT NULL,
	[trn_cls_frt_val] [numeric](14, 4) NOT NULL,
	[trn_opn_fl_val] [numeric](14, 4) NOT NULL,
	[trn_cls_fl_val] [numeric](14, 4) NOT NULL,
	[trn_opn_frt_wgt] [numeric](12, 2) NOT NULL,
	[trn_cls_frt_wgt] [numeric](12, 2) NOT NULL,
	[trn_upd_in_prgs] [char](1) NOT NULL,
	[trn_trcomp_sts] [char](1) NOT NULL
) ON [PRIMARY]
GO
