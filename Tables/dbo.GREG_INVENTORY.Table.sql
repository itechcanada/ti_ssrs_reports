USE [Stratix_US]
GO
/****** Object:  Table [dbo].[GREG_INVENTORY]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GREG_INVENTORY](
	[form] [varchar](50) NULL,
	[grade] [varchar](50) NULL,
	[size] [varchar](50) NULL,
	[finish] [varchar](50) NULL,
	[qty] [varchar](50) NULL,
	[cost] [varchar](50) NULL,
	[location] [varchar](50) NULL
) ON [PRIMARY]
GO
