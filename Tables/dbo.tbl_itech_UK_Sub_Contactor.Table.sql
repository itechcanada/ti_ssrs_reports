USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_UK_Sub_Contactor]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_UK_Sub_Contactor](
	[id] [int] NOT NULL,
	[contractor_cust_id] [text] NULL,
	[contractor_cust_nm] [varchar](100) NULL,
 CONSTRAINT [PK_tbl_itech_UK_Sub_Contactor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
