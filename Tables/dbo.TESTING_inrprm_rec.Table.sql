USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TESTING_inrprm_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TESTING_inrprm_rec](
	[PRM_FRM] [nchar](6) NOT NULL,
	[PRM_GRD] [nchar](8) NOT NULL,
	[PRM_SIZE] [nchar](15) NOT NULL,
	[PRM_FNSH] [nchar](8) NOT NULL,
	[PRM_REF_CTL_NO] [decimal](10, 0) NOT NULL,
	[PRM_SIZE_DESC] [nchar](25) NOT NULL,
	[PRM_SHRT_SIZE_DESC] [nchar](16) NOT NULL,
	[PRM_BAS_MSR] [nchar](1) NOT NULL,
	[PRM_E_NUM_SIZE1] [decimal](9, 5) NOT NULL,
	[PRM_E_NUM_SIZE2] [decimal](9, 5) NOT NULL,
	[PRM_E_NUM_SIZE3] [decimal](9, 5) NOT NULL,
	[PRM_E_NUM_SIZE4] [decimal](9, 5) NOT NULL,
	[PRM_E_NUM_SIZE5] [decimal](9, 5) NOT NULL,
	[PRM_M_NUM_SIZE1] [decimal](9, 5) NOT NULL,
	[PRM_M_NUM_SIZE2] [decimal](9, 5) NOT NULL,
	[PRM_M_NUM_SIZE3] [decimal](9, 5) NOT NULL,
	[PRM_M_NUM_SIZE4] [decimal](9, 5) NOT NULL,
	[PRM_M_NUM_SIZE5] [decimal](9, 5) NOT NULL,
	[PRM_PCS_CTL] [decimal](1, 0) NOT NULL,
	[PRM_DIM_SEG] [nchar](3) NOT NULL,
	[PRM_SK_MTHD] [nchar](3) NOT NULL,
	[PRM_COIL_FRM] [decimal](1, 0) NOT NULL,
	[PRM_LGTH_BAS_COIL] [decimal](1, 0) NOT NULL,
	[PRM_NON_STK] [decimal](1, 0) NOT NULL,
	[PRM_E_TWF] [decimal](12, 6) NOT NULL,
	[PRM_E_THEO_WGT_UM] [nchar](3) NULL,
	[PRM_M_TWF] [decimal](12, 6) NOT NULL,
	[PRM_M_THEO_WGT_UM] [nchar](3) NULL,
	[PRM_LGTH_DISP_FMT] [decimal](1, 0) NOT NULL,
	[PRM_PEP] [nchar](10) NOT NULL,
	[PRM_PRD_MGMT_SEQ] [decimal](6, 0) NOT NULL,
	[PRM_CLR] [nchar](3) NULL,
	[PRM_ORD_WGT_MULT] [decimal](5, 3) NOT NULL,
	[PRM_CST_OPT] [nchar](1) NOT NULL,
	[PRM_STAT_CD] [decimal](8, 0) NOT NULL,
	[PRM_E_XSECT_AREA] [decimal](16, 4) NOT NULL,
	[PRM_M_XSECT_AREA] [decimal](16, 4) NOT NULL,
	[PRM_SLS_CLSFCN] [nchar](2) NOT NULL,
	[PRM_E_MAX_WDTH] [decimal](9, 4) NOT NULL,
	[PRM_M_MAX_WDTH] [decimal](9, 4) NOT NULL,
	[PRM_E_MAX_LGTH] [decimal](9, 4) NOT NULL,
	[PRM_M_MAX_LGTH] [decimal](9, 4) NOT NULL,
	[PRM_STP_BUY] [decimal](1, 0) NOT NULL,
	[PRM_EF_APLC] [decimal](1, 0) NOT NULL,
	[PRM_ITAR] [decimal](1, 0) NOT NULL,
	[PRM_HRMNZ_TRF_CD] [decimal](8, 0) NOT NULL,
	[PRM_ACTV] [decimal](1, 0) NOT NULL,
 CONSTRAINT [inrprm_pk] PRIMARY KEY CLUSTERED 
(
	[PRM_FRM] ASC,
	[PRM_GRD] ASC,
	[PRM_SIZE] ASC,
	[PRM_FNSH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
