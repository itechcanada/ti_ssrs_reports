USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_mxtarh_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_mxtarh_rec](
	[arh_cmpy_id] [char](3) NOT NULL,
	[arh_doc_pfx] [char](1) NOT NULL,
	[arh_arch_ctl_no] [numeric](10, 0) NOT NULL,
	[arh_arch_ver_no] [numeric](3, 0) NOT NULL,
	[arh_arch_ctl_itm] [numeric](3, 0) NOT NULL,
	[arh_bus_doc_typ] [char](3) NOT NULL,
	[arh_gen_dtts] [datetime2](7) NULL,
	[arh_gen_dtms] [numeric](3, 0) NULL,
	[arh_prim_ref] [char](30) NOT NULL,
	[arh_oth_ref] [char](50) NOT NULL,
	[arh_rpt_pgm_nm] [char](14) NOT NULL,
	[arh_lgn_id] [char](8) NOT NULL,
	[arh_tot_pg] [numeric](6, 0) NOT NULL,
	[arh_file_size] [numeric](10, 0) NOT NULL,
	[arh_doc_img_trgt] [numeric](1, 0) NOT NULL,
	[arh_blb] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
