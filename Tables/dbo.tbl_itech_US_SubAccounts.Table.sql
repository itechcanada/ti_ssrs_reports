USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_US_SubAccounts]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_US_SubAccounts](
	[subAccount] [nchar](3) NOT NULL,
	[subAccountDesc] [nchar](55) NOT NULL,
	[isActive] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_itech_US_SubAccounts] ADD  CONSTRAINT [DF_tbl_itech_US_SubAccounts_isActive]  DEFAULT ((1)) FOR [isActive]
GO
