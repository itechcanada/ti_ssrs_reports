USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_mxtwmq_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_mxtwmq_rec](
	[wmq_lgn_id] [char](8) NOT NULL,
	[wmq_clnt_dtts] [datetime2](7) NOT NULL,
	[wmq_clnt_dtms] [numeric](3, 0) NOT NULL,
	[wmq_msg_dtts] [datetime2](7) NOT NULL,
	[wmq_msg_dtms] [numeric](3, 0) NOT NULL,
	[wmq_cmpy_id] [char](3) NOT NULL,
	[wmq_err_msg_typ] [char](1) NOT NULL,
	[wmq_msg_var] [varchar](255) NOT NULL,
	[wmq_ref_pfx] [char](2) NULL,
	[wmq_ref_no] [numeric](8, 0) NOT NULL,
	[wmq_ref_itm] [numeric](3, 0) NOT NULL,
	[wmq_ref_sbitm] [numeric](4, 0) NOT NULL,
	[wmq_pgm_nm] [char](14) NOT NULL,
	[wmq_bus_cond] [char](15) NOT NULL,
	[wmq_bus_cond_desc] [char](50) NOT NULL,
	[wmq_seq_no] [numeric](10, 0) NOT NULL,
	[wmq_rtng_lgn_id] [char](8) NULL,
	[wmq_rd_sts] [numeric](1, 0) NOT NULL,
	[wmq_pge_sts] [numeric](1, 0) NOT NULL,
	[wmq_extl_msg] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
