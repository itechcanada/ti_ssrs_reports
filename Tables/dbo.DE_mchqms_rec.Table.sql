USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_mchqms_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_mchqms_rec](
	[qms_cmpy_id] [char](3) NOT NULL,
	[qms_qds_ctl_no] [numeric](10, 0) NOT NULL,
	[qms_mss_ctl_no] [numeric](10, 0) NOT NULL,
	[qms_mtchg_chm] [char](2) NOT NULL,
	[qms_mtchg_tst] [char](2) NOT NULL,
	[qms_mtchg_jmy] [char](2) NOT NULL
) ON [PRIMARY]
GO
