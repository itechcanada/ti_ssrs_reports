USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_job_yield]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_job_yield](
	[Database] [varchar](10) NULL,
	[actvy_whs] [varchar](3) NULL,
	[actvy_dt] [datetime] NULL,
	[pwg] [varchar](3) NULL,
	[pwc] [varchar](3) NULL,
	[lgn_id] [varchar](8) NULL,
	[ref_no] [varchar](92) NULL,
	[trh_prs_1] [varchar](3) NULL,
	[trh_prs_2] [varchar](3) NULL,
	[trt_tcons_wgt] [int] NULL,
	[trt_tprod_wgt] [int] NULL,
	[sum_drp_mst] [int] NULL,
	[YLD_PCT] [decimal](20, 1) NULL,
	[trjct_wgt] [int] NULL,
	[RJCT_PCT] [decimal](20, 1) NULL,
	[tscr_wgt] [int] NULL,
	[Sum1] [decimal](20, 1) NULL,
	[Sum2] [decimal](20, 1) NULL,
	[SCR_PCT] [decimal](20, 1) NULL,
	[tuscr_wgt] [decimal](20, 1) NULL,
	[USCR_WGT_PCT] [decimal](20, 1) NULL,
	[YLD_LS_PCT] [decimal](20, 1) NULL,
	[itd_heat] [varchar](50) NULL,
	[branch] [varchar](3) NULL,
	[TrsPcs] [numeric](38, 0) NULL,
	[CstVal] [numeric](38, 2) NULL
) ON [PRIMARY]
GO
