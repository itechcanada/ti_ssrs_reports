USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_STRATIXOrderListUS]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_STRATIXOrderListUS](
	[CustNm] [nchar](65) NOT NULL,
	[ord_sld_cus_id] [char](8) NOT NULL,
	[OrderNo] [numeric](10, 0) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[YourPO] [nchar](35) NOT NULL,
	[PODate] [datetime] NOT NULL
) ON [PRIMARY]
GO
