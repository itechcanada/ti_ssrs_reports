USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_tctcnt_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_tctcnt_rec](
	[cnt_cmpy_id] [char](3) NOT NULL,
	[cnt_ref_pfx] [char](2) NOT NULL,
	[cnt_ref_no] [numeric](8, 0) NOT NULL,
	[cnt_ref_itm] [numeric](3, 0) NOT NULL,
	[cnt_cntc_typ] [char](2) NOT NULL,
	[cnt_sltn_cd] [char](3) NULL,
	[cnt_frst_nm] [char](20) NOT NULL,
	[cnt_lst_nm] [char](30) NOT NULL,
	[cnt_ttl] [char](30) NOT NULL,
	[cnt_tel_area_cd] [char](7) NOT NULL,
	[cnt_tel_no] [char](15) NOT NULL,
	[cnt_tel_ext] [char](5) NOT NULL,
	[cnt_mobl_area_cd] [char](7) NOT NULL,
	[cnt_mobl_no] [char](15) NOT NULL,
	[cnt_fax_area_cd] [char](7) NOT NULL,
	[cnt_fax_no] [char](15) NOT NULL,
	[cnt_fax_ext] [char](5) NOT NULL,
	[cnt_email] [char](50) NOT NULL,
	[cnt_rmk] [char](50) NOT NULL
) ON [PRIMARY]
GO
