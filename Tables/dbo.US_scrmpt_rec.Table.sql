USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_scrmpt_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_scrmpt_rec](
	[mpt_mthd_pmt] [char](2) NOT NULL,
	[mpt_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[mpt_desc15] [char](15) NOT NULL,
	[mpt_long_desc50] [char](50) NOT NULL,
	[mpt_auto_cr_apv] [numeric](1, 0) NOT NULL,
	[mpt_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
