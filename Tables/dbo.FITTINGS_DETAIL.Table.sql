USE [Stratix_US]
GO
/****** Object:  Table [dbo].[FITTINGS_DETAIL]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FITTINGS_DETAIL](
	[type] [varchar](10) NULL,
	[product] [varchar](50) NULL,
	[frmgrd] [varchar](255) NULL,
	[size] [varchar](255) NULL,
	[po] [float] NULL,
	[stock_so] [float] NULL,
	[cust_so] [float] NULL,
	[stock] [float] NULL
) ON [PRIMARY]
GO
