USE [Stratix_US]
GO
/****** Object:  Table [dbo].[glbacb_branch]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[glbacb_branch](
	[Ledger_ID] [nchar](2) NULL,
	[Component_ID] [nchar](2) NULL,
	[Cmpt_Value] [nchar](2) NOT NULL,
	[Description] [nvarchar](45) NULL,
	[acb_branch] [nvarchar](45) NULL,
	[acb_branch_active] [bit] NULL,
 CONSTRAINT [PK_glbacb_branch] PRIMARY KEY CLUSTERED 
(
	[Cmpt_Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
