USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_apjglj_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_apjglj_rec](
	[glj_cmpy_id] [char](3) NOT NULL,
	[glj_vchr_pfx] [char](2) NOT NULL,
	[glj_vchr_no] [numeric](8, 0) NOT NULL,
	[glj_vchr_itm] [numeric](3, 0) NOT NULL,
	[glj_bsc_gl_acct] [char](8) NOT NULL,
	[glj_sacct] [char](30) NOT NULL,
	[glj_dr_amt] [numeric](12, 2) NOT NULL,
	[glj_cr_amt] [numeric](12, 2) NOT NULL,
	[glj_dist_rmk] [char](50) NOT NULL
) ON [PRIMARY]
GO
