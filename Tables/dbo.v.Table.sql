USE [Stratix_US]
GO
/****** Object:  Table [dbo].[v]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[v](
	[stn_sld_cus_id] [char](8) NOT NULL,
	[FirstInvOfMonth] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
