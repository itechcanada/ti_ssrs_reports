USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_cctcta_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_cctcta_rec](
	[cta_cmpy_id] [char](3) NOT NULL,
	[cta_crmtsk_pfx] [char](2) NOT NULL,
	[cta_crmtsk_no] [numeric](8, 0) NOT NULL,
	[cta_crtd_lgn_id] [char](8) NOT NULL,
	[cta_crtd_dtts] [datetime2](7) NULL,
	[cta_crtd_dtms] [numeric](3, 0) NULL,
	[cta_tsk_desc60] [char](60) NOT NULL,
	[cta_tsk_asgn_to] [char](8) NOT NULL,
	[cta_crmtsk_typ] [char](3) NOT NULL,
	[cta_crmtsk_prty] [char](1) NOT NULL,
	[cta_crmtsk_sts] [char](3) NOT NULL,
	[cta_crmtsk_purp] [char](3) NULL,
	[cta_prnt_tsk_pfx] [char](2) NOT NULL,
	[cta_prnt_tsk_no] [numeric](8, 0) NOT NULL,
	[cta_ref_pfx] [char](2) NOT NULL,
	[cta_ref_no] [numeric](8, 0) NOT NULL,
	[cta_ref_itm] [numeric](3, 0) NOT NULL,
	[cta_inv_ref_no] [char](22) NOT NULL,
	[cta_extl_ref] [char](22) NOT NULL,
	[cta_tsk_loc] [char](60) NOT NULL,
	[cta_crmacct_typ] [char](1) NOT NULL,
	[cta_crmacct_id] [char](8) NOT NULL,
	[cta_tsk_strt_dtts] [datetime2](7) NULL,
	[cta_tsk_strt_dtms] [numeric](3, 0) NULL,
	[cta_tsk_end_dtts] [datetime2](7) NULL,
	[cta_tsk_end_dtms] [numeric](3, 0) NULL,
	[cta_due_dt] [date] NULL,
	[cta_tsk_rmdr_cd] [char](1) NOT NULL,
	[cta_tsk_rmdr_dtts] [datetime2](7) NULL,
	[cta_tsk_rmdr_dtms] [numeric](3, 0) NULL,
	[cta_recu_ser_no] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
GO
