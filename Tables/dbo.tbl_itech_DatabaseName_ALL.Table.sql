USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_DatabaseName_ALL]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_DatabaseName_ALL](
	[id] [int] NULL,
	[Name] [varchar](50) NULL,
	[DatabaseName] [varchar](50) NULL,
	[Prefix] [varchar](50) NULL,
	[Company] [varchar](50) NULL,
	[Views] [varchar](50) NULL,
	[Tables] [varchar](50) NULL,
	[Order_tables] [varchar](50) NULL
) ON [PRIMARY]
GO
