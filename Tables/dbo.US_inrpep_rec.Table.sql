USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_inrpep_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_inrpep_rec](
	[pep_pep] [char](10) NOT NULL,
	[pep_prd_divn] [char](2) NOT NULL,
	[pep_prd_ln] [char](2) NOT NULL,
	[pep_prd_grp] [char](2) NOT NULL,
	[pep_prd_cat] [char](2) NOT NULL,
	[pep_prd_typ] [char](2) NOT NULL,
	[pep_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[pep_desc25] [char](25) NOT NULL,
	[pep_extd_desc] [char](50) NOT NULL,
	[pep_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
