USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_intpic_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_intpic_rec](
	[pic_cmpy_id] [char](3) NOT NULL,
	[pic_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[pic_brh] [char](3) NOT NULL,
	[pic_extd_prd] [char](67) NOT NULL,
	[pic_whs] [char](3) NOT NULL,
	[pic_sk_ctl] [char](37) NOT NULL,
	[pic_tag_no] [char](12) NOT NULL
) ON [PRIMARY]
GO
