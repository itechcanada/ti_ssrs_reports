USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_mcrmss_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_mcrmss_rec](
	[mss_mss_ctl_no] [numeric](10, 0) NOT NULL,
	[mss_sdo] [char](6) NOT NULL,
	[mss_std_id] [char](20) NOT NULL,
	[mss_addnl_id] [char](20) NOT NULL,
	[mss_supp_sdo1] [char](6) NULL,
	[mss_supp_sdo2] [char](6) NULL,
	[mss_bas_msr] [char](1) NOT NULL,
	[mss_isd_dt] [date] NULL,
	[mss_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
