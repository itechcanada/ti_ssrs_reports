USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_stxGetVendorTrackingURL]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_stxGetVendorTrackingURL](
	[stxVendorID] [nchar](20) NOT NULL,
	[stxVendorActive] [tinyint] NOT NULL,
	[stxVendorURL] [nchar](300) NULL
) ON [PRIMARY]
GO
