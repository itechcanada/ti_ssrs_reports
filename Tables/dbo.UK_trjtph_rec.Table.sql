USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_trjtph_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_trjtph_rec](
	[tph_cmpy_id] [char](3) NOT NULL,
	[tph_transp_pfx] [char](2) NOT NULL,
	[tph_transp_no] [numeric](8, 0) NOT NULL,
	[tph_trpln_whs] [char](3) NOT NULL,
	[tph_mtr_plng_whs] [char](3) NOT NULL,
	[tph_trrte] [char](6) NOT NULL,
	[tph_rte_clr] [char](3) NULL,
	[tph_dlvy_mthd] [char](2) NOT NULL,
	[tph_frt_ven_id] [char](8) NULL,
	[tph_crr_nm] [char](35) NOT NULL,
	[tph_frt_cst] [numeric](13, 4) NOT NULL,
	[tph_frt_cst_um] [char](3) NULL,
	[tph_crr_ref_no] [char](22) NOT NULL,
	[tph_fl_cst] [numeric](13, 4) NOT NULL,
	[tph_fl_cst_um] [char](3) NULL,
	[tph_stp_chrg] [numeric](13, 4) NOT NULL,
	[tph_ovrd_frt] [numeric](1, 0) NOT NULL,
	[tph_frt_cst_typ] [char](1) NOT NULL,
	[tph_sch_resp] [char](1) NOT NULL,
	[tph_frt_cry] [char](3) NULL,
	[tph_frt_exrt] [numeric](13, 8) NOT NULL,
	[tph_frt_ex_rt_typ] [char](1) NOT NULL,
	[tph_vcl_no] [char](10) NULL,
	[tph_sch_rmk] [char](100) NOT NULL,
	[tph_lic_pl] [char](15) NOT NULL,
	[tph_drvr_1] [char](35) NOT NULL,
	[tph_drvr_2] [char](35) NOT NULL,
	[tph_trctr] [numeric](1, 0) NOT NULL,
	[tph_trlr_no] [char](10) NULL,
	[tph_trlr_typ] [char](2) NOT NULL,
	[tph_max_stp] [numeric](3, 0) NOT NULL,
	[tph_max_wgt] [numeric](10, 2) NOT NULL,
	[tph_max_wdth] [numeric](9, 4) NOT NULL,
	[tph_max_lgth] [numeric](9, 4) NOT NULL,
	[tph_appt_no] [char](10) NOT NULL,
	[tph_gt_dck] [char](10) NOT NULL,
	[tph_sch_dtts] [datetime2](7) NULL,
	[tph_sch_dtms] [numeric](3, 0) NULL,
	[tph_opn_frt_val] [numeric](14, 4) NOT NULL,
	[tph_cls_frt_val] [numeric](14, 4) NOT NULL,
	[tph_opn_fl_val] [numeric](14, 4) NOT NULL,
	[tph_cls_fl_val] [numeric](14, 4) NOT NULL,
	[tph_opn_frt_wgt] [numeric](12, 2) NOT NULL,
	[tph_cls_frt_wgt] [numeric](12, 2) NOT NULL,
	[tph_nbr_stp] [numeric](3, 0) NOT NULL,
	[tph_jrnl_comp] [numeric](1, 0) NOT NULL,
	[tph_mt_pfx] [char](2) NOT NULL,
	[tph_mt_no] [numeric](8, 0) NOT NULL
) ON [PRIMARY]
GO
