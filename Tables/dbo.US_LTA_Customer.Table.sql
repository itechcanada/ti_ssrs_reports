USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_LTA_Customer]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_LTA_Customer](
	[idlta_customer] [int] NOT NULL,
	[branch] [varchar](3) NOT NULL,
	[customer_id] [varchar](50) NOT NULL,
	[customer_name] [varchar](75) NOT NULL,
	[market] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
