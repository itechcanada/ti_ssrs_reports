USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_xcrixc_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_xcrixc_rec](
	[ixc_cmpy_id] [char](3) NOT NULL,
	[ixc_cus_id] [char](8) NOT NULL,
	[ixc_sllg_cmpy_id] [char](3) NOT NULL,
	[ixc_lgn_id] [char](8) NOT NULL,
	[ixc_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
