USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_nctnic_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_nctnic_rec](
	[nic_cmpy_id] [char](3) NOT NULL,
	[nic_ncr_pfx] [char](2) NOT NULL,
	[nic_ncr_no] [numeric](8, 0) NOT NULL,
	[nic_ncr_itm] [numeric](3, 0) NOT NULL,
	[nic_sprc_pfx] [char](2) NOT NULL,
	[nic_sprc_no] [numeric](8, 0) NOT NULL,
	[nic_shpt_pfx] [char](2) NOT NULL,
	[nic_shpt_no] [numeric](8, 0) NOT NULL,
	[nic_shpt_itm] [numeric](3, 0) NOT NULL,
	[nic_ord_pfx] [char](2) NOT NULL,
	[nic_ord_no] [numeric](8, 0) NOT NULL,
	[nic_ord_itm] [numeric](3, 0) NOT NULL,
	[nic_ord_rls_no] [numeric](2, 0) NOT NULL,
	[nic_cus_po] [char](30) NOT NULL,
	[nic_ven_id] [char](8) NULL,
	[nic_is_slp] [char](4) NULL,
	[nic_sls_cat] [char](2) NOT NULL
) ON [PRIMARY]
GO
