USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_mxrusr_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_mxrusr_rec](
	[usr_lgn_id] [char](8) NOT NULL,
	[usr_nm] [char](35) NOT NULL,
	[usr_tel_area_cd] [char](7) NOT NULL,
	[usr_tel_no] [char](15) NOT NULL,
	[usr_tel_ext] [char](5) NOT NULL,
	[usr_fax_area_cd] [char](7) NOT NULL,
	[usr_fax_no] [char](15) NOT NULL,
	[usr_fax_ext] [char](5) NOT NULL,
	[usr_email] [char](50) NOT NULL,
	[usr_mobl_area_cd] [char](7) NOT NULL,
	[usr_mobl_no] [char](15) NOT NULL,
	[usr_ttl] [char](30) NOT NULL,
	[usr_lng] [char](2) NOT NULL,
	[usr_cty] [char](3) NOT NULL,
	[usr_usr_cmpy_id] [char](3) NOT NULL,
	[usr_usr_brh] [char](3) NOT NULL,
	[usr_brh_acs] [char](1) NOT NULL,
	[usr_rinvtobrh_acs] [char](1) NOT NULL,
	[usr_vinvtobrh_acs] [char](1) NOT NULL,
	[usr_usr_whs] [char](3) NOT NULL,
	[usr_whs_acs] [char](1) NOT NULL,
	[usr_rinvtwhs_acs] [char](1) NOT NULL,
	[usr_vinvtwhs_acs] [char](1) NOT NULL,
	[usr_own_ord_only] [numeric](1, 0) NOT NULL,
	[usr_brh_acs_oth] [char](1) NOT NULL,
	[usr_rinvtobrh_oth] [char](1) NOT NULL,
	[usr_vinvtobrh_oth] [char](1) NOT NULL,
	[usr_whs_acs_oth] [char](1) NOT NULL,
	[usr_rinvtwhs_oth] [char](1) NOT NULL,
	[usr_vinvtwhs_oth] [char](1) NOT NULL,
	[usr_lvl] [char](1) NOT NULL,
	[usr_chksum] [char](38) NOT NULL,
	[usr_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
