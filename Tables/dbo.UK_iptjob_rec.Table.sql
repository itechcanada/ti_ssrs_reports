USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_iptjob_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_iptjob_rec](
	[job_cmpy_id] [char](3) NOT NULL,
	[job_job_pfx] [char](2) NOT NULL,
	[job_job_no] [numeric](8, 0) NOT NULL,
	[job_jbs_pfx] [char](2) NOT NULL,
	[job_jbs_no] [numeric](8, 0) NOT NULL,
	[job_strm_no] [numeric](2, 0) NOT NULL,
	[job_job_seq_no] [numeric](8, 0) NOT NULL,
	[job_actvy_whs] [char](3) NOT NULL,
	[job_prs_cl] [char](2) NOT NULL,
	[job_pwc] [char](3) NOT NULL,
	[job_pwg] [char](3) NOT NULL,
	[job_sch_strt_dtts] [datetime2](7) NULL,
	[job_sch_strt_dtms] [numeric](3, 0) NULL,
	[job_sch_strt_ltts] [datetime2](7) NULL,
	[job_sch_strt_ltms] [numeric](3, 0) NULL,
	[job_expct_dtts] [datetime2](7) NULL,
	[job_expct_dtms] [numeric](3, 0) NULL,
	[job_eord_rby_ltts] [datetime2](7) NULL,
	[job_eord_rby_ltms] [numeric](3, 0) NULL,
	[job_eord_pfx] [char](2) NOT NULL,
	[job_eord_no] [numeric](8, 0) NOT NULL,
	[job_eord_itm] [numeric](3, 0) NOT NULL,
	[job_eord_rls_no] [numeric](2, 0) NOT NULL,
	[job_job_cl] [char](1) NOT NULL,
	[job_match_prod] [char](1) NOT NULL,
	[job_est_stup_tm] [numeric](5, 0) NOT NULL,
	[job_est_run_tm] [numeric](5, 0) NOT NULL,
	[job_est_trdn_tm] [numeric](5, 0) NOT NULL,
	[job_prs_dy] [numeric](3, 0) NOT NULL,
	[job_desc30] [char](30) NOT NULL,
	[job_prs_gnrtn_sts] [char](1) NOT NULL,
	[job_job_sts] [char](1) NOT NULL,
	[job_strt_dtts] [datetime2](7) NULL,
	[job_strt_dtms] [numeric](3, 0) NULL,
	[job_yld_ls_pct] [numeric](6, 3) NOT NULL,
	[job_part_cus_id] [char](8) NULL,
	[job_part] [char](30) NOT NULL,
	[job_part_revno] [char](8) NOT NULL,
	[job_alt_job_exst] [numeric](1, 0) NOT NULL,
	[job_byp] [numeric](1, 0) NOT NULL,
	[job_lyt_exst] [numeric](1, 0) NOT NULL,
	[job_cus_id] [char](8) NULL,
	[job_otzr_id] [char](3) NOT NULL,
	[job_otzr_job_nm] [char](50) NOT NULL,
	[job_rapd_rec] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
