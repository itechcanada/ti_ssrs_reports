USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_ivjjit_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_ivjjit_rec](
	[jit_cmpy_id] [char](3) NOT NULL,
	[jit_ref_pfx] [char](2) NOT NULL,
	[jit_ref_no] [numeric](8, 0) NOT NULL,
	[jit_ref_itm] [numeric](3, 0) NOT NULL,
	[jit_upd_dt] [date] NOT NULL,
	[jit_mtl_avg_val] [numeric](15, 2) NOT NULL,
	[jit_mtl_repl_val] [numeric](15, 2) NOT NULL,
	[jit_tot_avg_val] [numeric](15, 2) NOT NULL,
	[jit_tot_repl_val] [numeric](15, 2) NOT NULL,
	[jit_mpft_avg_val] [numeric](15, 2) NOT NULL,
	[jit_mpft_repl_val] [numeric](15, 2) NOT NULL,
	[jit_npft_avg_val] [numeric](15, 2) NOT NULL,
	[jit_npft_repl_val] [numeric](15, 2) NOT NULL,
	[jit_mpft_avg_pct] [numeric](5, 2) NOT NULL,
	[jit_mpft_repl_pct] [numeric](5, 2) NOT NULL,
	[jit_npft_avg_pct] [numeric](5, 2) NOT NULL,
	[jit_npft_repl_pct] [numeric](5, 2) NOT NULL,
	[jit_tot_tx_val] [numeric](15, 2) NOT NULL,
	[jit_trm_disc_dt] [date] NULL,
	[jit_trm_disc_val] [numeric](15, 2) NOT NULL
) ON [PRIMARY]
GO
