USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_apjjca_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_apjjca_rec](
	[jca_cmpy_id] [char](3) NOT NULL,
	[jca_vchr_pfx] [char](2) NOT NULL,
	[jca_vchr_no] [numeric](8, 0) NOT NULL,
	[jca_vchr_itm] [numeric](3, 0) NOT NULL,
	[jca_extl_ref] [char](22) NOT NULL,
	[jca_cc_no] [numeric](3, 0) NOT NULL,
	[jca_desc30] [char](30) NOT NULL,
	[jca_po_pfx] [char](2) NOT NULL,
	[jca_po_no] [numeric](8, 0) NOT NULL,
	[jca_po_itm] [numeric](3, 0) NOT NULL,
	[jca_adv_pfx] [char](2) NOT NULL,
	[jca_adv_amt] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
