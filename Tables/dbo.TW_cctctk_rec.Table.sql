USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_cctctk_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_cctctk_rec](
	[ctk_cmpy_id] [char](3) NOT NULL,
	[ctk_crm_tsk_pfx] [char](2) NOT NULL,
	[ctk_crm_tsk_no] [numeric](8, 0) NOT NULL,
	[ctk_tsk_asgn_to] [char](8) NOT NULL,
	[ctk_crm_tsk_typ] [char](3) NOT NULL,
	[ctk_actvy_tsk_purp] [char](3) NOT NULL,
	[ctk_tsk_due_dt] [date] NULL,
	[ctk_crm_tsk_prty] [char](3) NOT NULL,
	[ctk_crm_tsk_sts] [char](3) NOT NULL,
	[ctk_crm_tsk_comp] [char](3) NOT NULL,
	[ctk_tsk_subj] [char](128) NOT NULL,
	[ctk_crtd_lgn_id] [char](8) NOT NULL,
	[ctk_crtd_dtts] [datetime2](7) NULL,
	[ctk_crtd_dtms] [numeric](3, 0) NULL,
	[ctk_upd_lgn_id] [char](8) NOT NULL,
	[ctk_upd_dtts] [datetime2](7) NULL,
	[ctk_upd_dtms] [numeric](3, 0) NULL,
	[ctk_del_dtts] [datetime2](7) NULL,
	[ctk_del_dtms] [numeric](3, 0) NULL,
	[ctk_prnt_crm_pfx] [char](2) NOT NULL,
	[ctk_prnt_crm_no] [numeric](8, 0) NOT NULL,
	[ctk_cus_ven_typ] [char](1) NOT NULL,
	[ctk_cus_ven_id] [char](8) NOT NULL,
	[ctk_cmptr_cus_id] [char](8) NULL,
	[ctk_brh] [char](3) NOT NULL,
	[ctk_tsk_txt] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
