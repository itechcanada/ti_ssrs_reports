USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_injitd_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_injitd_rec](
	[itd_cmpy_id] [char](3) NOT NULL,
	[itd_ref_pfx] [char](2) NOT NULL,
	[itd_ref_no] [numeric](8, 0) NOT NULL,
	[itd_ref_itm] [numeric](3, 0) NOT NULL,
	[itd_ref_sbitm] [numeric](4, 0) NOT NULL,
	[itd_actvy_dt] [date] NOT NULL,
	[itd_trs_seq_no] [numeric](8, 0) NOT NULL,
	[itd_prnt_pfx] [char](2) NOT NULL,
	[itd_prnt_no] [numeric](8, 0) NOT NULL,
	[itd_prnt_itm] [numeric](3, 0) NOT NULL,
	[itd_prnt_sitm] [numeric](2, 0) NOT NULL,
	[itd_itm_ctl_no] [numeric](10, 0) NOT NULL,
	[itd_brh] [char](3) NOT NULL,
	[itd_frm] [char](6) NOT NULL,
	[itd_grd] [char](8) NOT NULL,
	[itd_size] [char](15) NOT NULL,
	[itd_fnsh] [char](8) NOT NULL,
	[itd_ef_svar] [char](30) NOT NULL,
	[itd_whs] [char](3) NOT NULL,
	[itd_mill] [char](3) NULL,
	[itd_heat] [char](15) NULL,
	[itd_ownr] [char](1) NOT NULL,
	[itd_bgt_for] [char](1) NOT NULL,
	[itd_invt_cat] [char](1) NOT NULL,
	[itd_upd_dtts] [datetime2](7) NULL,
	[itd_cst_qty_indc] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
