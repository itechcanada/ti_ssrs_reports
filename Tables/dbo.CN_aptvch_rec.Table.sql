USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_aptvch_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_aptvch_rec](
	[vch_cmpy_id] [char](3) NOT NULL,
	[vch_vchr_pfx] [char](2) NOT NULL,
	[vch_vchr_no] [numeric](8, 0) NOT NULL,
	[vch_ssn_id] [numeric](6, 0) NOT NULL,
	[vch_ent_dt] [date] NOT NULL,
	[vch_lgn_id] [char](8) NOT NULL,
	[vch_ven_id] [char](8) NOT NULL,
	[vch_ven_inv_no] [char](22) NOT NULL,
	[vch_extl_ref] [char](22) NOT NULL,
	[vch_ven_inv_dt] [date] NOT NULL,
	[vch_po_pfx] [char](2) NULL,
	[vch_po_no] [numeric](8, 0) NOT NULL,
	[vch_po_itm] [numeric](3, 0) NOT NULL,
	[vch_mt_pfx] [char](2) NOT NULL,
	[vch_mt_no] [numeric](8, 0) NOT NULL,
	[vch_mt_itm] [numeric](3, 0) NOT NULL,
	[vch_voyg_no] [char](15) NOT NULL,
	[vch_vchr_brh] [char](3) NOT NULL,
	[vch_ptx_vchr_amt] [numeric](12, 2) NOT NULL,
	[vch_vchr_amt] [numeric](12, 2) NOT NULL,
	[vch_dscb_amt] [numeric](12, 2) NOT NULL,
	[vch_desc30] [char](30) NOT NULL,
	[vch_cry] [char](3) NOT NULL,
	[vch_exrt] [numeric](13, 8) NOT NULL,
	[vch_ex_rt_typ] [char](1) NOT NULL,
	[vch_pttrm] [numeric](3, 0) NOT NULL,
	[vch_disc_trm] [numeric](2, 0) NULL,
	[vch_due_nbr_dy] [numeric](3, 0) NOT NULL,
	[vch_disc_nbr_dy] [numeric](3, 0) NOT NULL,
	[vch_disc_rt] [numeric](5, 2) NOT NULL,
	[vch_due_dt] [date] NOT NULL,
	[vch_disc_dt] [date] NULL,
	[vch_disc_amt] [numeric](12, 2) NOT NULL,
	[vch_chk_itm_rmk] [char](50) NOT NULL,
	[vch_pmt_typ] [char](1) NOT NULL,
	[vch_vchr_xref] [numeric](8, 0) NOT NULL,
	[vch_auth_ref] [char](22) NOT NULL,
	[vch_vchr_cat] [char](4) NOT NULL,
	[vch_svc_ffm_dt] [date] NULL,
	[vch_ppmt_elgbl] [numeric](1, 0) NOT NULL,
	[vch_recu_flg] [numeric](1, 0) NOT NULL,
	[vch_recu_freq] [char](1) NULL,
	[vch_recu_exp_dt] [date] NULL,
	[vch_recu_gnrtn_dt] [date] NULL,
	[vch_ven_inv_sfx] [numeric](4, 0) NOT NULL
) ON [PRIMARY]
GO
