USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_ipjtrt_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_ipjtrt_rec](
	[trt_cmpy_id] [char](3) NOT NULL,
	[trt_ref_pfx] [char](2) NOT NULL,
	[trt_ref_no] [numeric](8, 0) NOT NULL,
	[trt_ref_itm] [numeric](3, 0) NOT NULL,
	[trt_ref_sbitm] [numeric](4, 0) NOT NULL,
	[trt_tprod_wgt] [numeric](12, 2) NOT NULL,
	[trt_tcons_wgt] [numeric](12, 2) NOT NULL,
	[trt_tdrp_wgt] [numeric](12, 2) NOT NULL,
	[trt_tmst_wgt] [numeric](12, 2) NOT NULL,
	[trt_trjct_wgt] [numeric](12, 2) NOT NULL,
	[trt_tscr_wgt] [numeric](12, 2) NOT NULL,
	[trt_tetrm_wgt] [numeric](12, 2) NOT NULL,
	[trt_tecut_wgt] [numeric](12, 2) NOT NULL,
	[trt_tkrfls_wgt] [numeric](12, 2) NOT NULL,
	[trt_tuscr_wgt] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
