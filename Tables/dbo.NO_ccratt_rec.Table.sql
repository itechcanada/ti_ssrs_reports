USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_ccratt_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_ccratt_rec](
	[att_crm_actvy_typ] [char](3) NOT NULL,
	[att_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[att_desc30] [char](30) NOT NULL,
	[att_actvy_cl] [char](1) NOT NULL,
	[att_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
