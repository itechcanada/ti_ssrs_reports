USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_sctpal_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_sctpal_rec](
	[pal_aud_ctl_no] [numeric](10, 0) NOT NULL,
	[pal_aud_seq_no] [numeric](3, 0) NOT NULL,
	[pal_tbl_nm] [char](6) NOT NULL,
	[pal_fct] [char](3) NOT NULL,
	[pal_lgn_id] [char](8) NOT NULL,
	[pal_upd_dtts] [datetime2](7) NULL,
	[pal_upd_dtms] [numeric](3, 0) NULL,
	[pal_cmpy_id] [char](3) NOT NULL,
	[pal_rec_txt] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
