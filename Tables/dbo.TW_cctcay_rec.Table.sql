USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_cctcay_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_cctcay_rec](
	[cay_cmpy_id] [char](3) NOT NULL,
	[cay_crmact_pfx] [char](2) NOT NULL,
	[cay_crmact_no] [numeric](8, 0) NOT NULL,
	[cay_prfd_by] [char](8) NOT NULL,
	[cay_crtd_dtts] [datetime2](7) NULL,
	[cay_crtd_dtms] [numeric](3, 0) NULL,
	[cay_actvy_dtts] [datetime2](7) NULL,
	[cay_actvy_dtms] [numeric](3, 0) NULL,
	[cay_crmact_typ] [char](3) NOT NULL,
	[cay_crmact_purp] [char](3) NULL,
	[cay_crmacct_typ] [char](1) NOT NULL,
	[cay_crmacct_id] [char](8) NOT NULL,
	[cay_actvy_desc60] [char](60) NOT NULL,
	[cay_actvy_dur_tm] [numeric](7, 0) NOT NULL,
	[cay_crmtsk_pfx] [char](2) NOT NULL,
	[cay_crmtsk_no] [numeric](8, 0) NOT NULL,
	[cay_ref_pfx] [char](2) NOT NULL,
	[cay_ref_no] [numeric](8, 0) NOT NULL,
	[cay_ref_itm] [numeric](3, 0) NOT NULL,
	[cay_inv_ref_no] [char](22) NOT NULL,
	[cay_extl_ref] [char](22) NOT NULL,
	[cay_email_dirn] [char](3) NOT NULL,
	[cay_sys_actvy] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
