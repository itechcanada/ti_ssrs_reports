USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_Networkdays]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_Networkdays](
	[idNetworkdays] [int] NOT NULL,
	[branchName] [varchar](50) NOT NULL,
	[workDaysPerYear] [int] NOT NULL,
	[janWorkDays] [int] NOT NULL,
	[febWorkDays] [int] NOT NULL,
	[marWorkDays] [int] NOT NULL,
	[aprWorkDays] [int] NOT NULL,
	[mayWorkDays] [int] NOT NULL,
	[junWorkDays] [int] NOT NULL,
	[julWorkDays] [int] NOT NULL,
	[augWorkDays] [int] NOT NULL,
	[sepWorkDays] [int] NOT NULL,
	[octWorkDays] [int] NOT NULL,
	[novWorkDays] [int] NOT NULL,
	[decWorkDays] [int] NOT NULL
) ON [PRIMARY]
GO
