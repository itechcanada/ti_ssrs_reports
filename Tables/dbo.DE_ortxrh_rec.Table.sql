USE [Stratix_US]
GO
/****** Object:  Table [dbo].[DE_ortxrh_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DE_ortxrh_rec](
	[xrh_cmpy_id] [char](3) NOT NULL,
	[xrh_ord_pfx] [char](2) NOT NULL,
	[xrh_ord_no] [numeric](8, 0) NOT NULL,
	[xrh_ord_itm] [numeric](3, 0) NOT NULL,
	[xrh_ord_rls_no] [numeric](2, 0) NOT NULL,
	[xrh_ord_brh] [char](3) NOT NULL,
	[xrh_sld_cus_id] [char](8) NOT NULL,
	[xrh_cus_po] [char](30) NOT NULL,
	[xrh_frm] [char](6) NOT NULL,
	[xrh_grd] [char](8) NOT NULL,
	[xrh_num_size1] [numeric](9, 5) NOT NULL,
	[xrh_num_size2] [numeric](9, 5) NOT NULL,
	[xrh_num_size3] [numeric](9, 5) NOT NULL,
	[xrh_num_size4] [numeric](9, 5) NOT NULL,
	[xrh_num_size5] [numeric](9, 5) NOT NULL,
	[xrh_size] [char](15) NOT NULL,
	[xrh_fnsh] [char](8) NOT NULL,
	[xrh_ef_evar] [char](30) NOT NULL,
	[xrh_idia] [numeric](9, 5) NOT NULL,
	[xrh_odia] [numeric](9, 5) NOT NULL,
	[xrh_ga_size] [numeric](8, 5) NOT NULL,
	[xrh_wdth] [numeric](9, 4) NOT NULL,
	[xrh_lgth] [numeric](9, 4) NOT NULL,
	[xrh_part_cus_id] [char](8) NULL,
	[xrh_part] [char](30) NOT NULL,
	[xrh_part_revno] [char](8) NOT NULL,
	[xrh_sld_to_long_nm] [char](35) NOT NULL,
	[xrh_shp_to_long_nm] [char](35) NOT NULL,
	[xrh_prd_desc50a] [char](50) NOT NULL,
	[xrh_prd_desc50b] [char](50) NOT NULL,
	[xrh_prd_desc50c] [char](50) NOT NULL,
	[xrh_wgt_um] [char](3) NOT NULL,
	[xrh_lgth_disp_fmt] [numeric](1, 0) NOT NULL,
	[xrh_rls_pcs] [numeric](7, 0) NOT NULL,
	[xrh_rls_pcs_typ] [char](1) NOT NULL,
	[xrh_rls_msr] [numeric](16, 4) NOT NULL,
	[xrh_rls_msr_typ] [char](1) NOT NULL,
	[xrh_rls_wgt] [numeric](10, 2) NOT NULL,
	[xrh_rls_wgt_typ] [char](1) NOT NULL,
	[xrh_upd_dtts] [datetime2](7) NULL,
	[xrh_upd_dtms] [numeric](3, 0) NULL,
	[xrh_cls_dt] [date] NULL,
	[xrh_is_slp] [char](4) NOT NULL,
	[xrh_os_slp] [char](4) NOT NULL,
	[xrh_tkn_slp] [char](4) NOT NULL,
	[xrh_job_id] [char](8) NOT NULL,
	[xrh_grp_ord_itm] [numeric](3, 0) NOT NULL,
	[xrh_sts_actn] [char](1) NOT NULL
) ON [PRIMARY]
GO
