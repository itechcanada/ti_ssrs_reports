USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_tcttsh_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_tcttsh_rec](
	[tsh_cmpy_id] [char](3) NOT NULL,
	[tsh_ref_pfx] [char](2) NOT NULL,
	[tsh_ref_no] [numeric](8, 0) NOT NULL,
	[tsh_ref_itm] [numeric](3, 0) NOT NULL,
	[tsh_ref_sbitm] [numeric](4, 0) NOT NULL,
	[tsh_sts_typ] [char](1) NOT NULL,
	[tsh_actvy_dtts] [datetime2](7) NOT NULL,
	[tsh_actvy_dtms] [numeric](3, 0) NOT NULL,
	[tsh_sts_actn] [char](1) NOT NULL,
	[tsh_rsn_typ] [char](3) NULL,
	[tsh_rsn] [char](3) NULL,
	[tsh_sts] [char](3) NULL,
	[tsh_rmk] [char](50) NOT NULL,
	[tsh_actvy_lgn_id] [char](8) NOT NULL
) ON [PRIMARY]
GO
