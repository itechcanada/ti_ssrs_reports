USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_IRM_Part_Prd_Input]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_IRM_Part_Prd_Input](
	[partNo] [varchar](50) NOT NULL,
	[product] [varchar](100) NOT NULL
) ON [PRIMARY]
GO
