USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_glrbga_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_glrbga_rec](
	[bga_bsc_gl_acct] [char](8) NOT NULL,
	[bga_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[bga_desc30] [char](30) NOT NULL,
	[bga_acct_typ] [char](1) NOT NULL,
	[bga_transl_rt_typ] [char](1) NOT NULL,
	[bga_acct_cl] [char](4) NOT NULL,
	[bga_nml_dr_cr] [char](1) NOT NULL,
	[bga_vld_sacct_comb] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_1] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_2] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_3] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_4] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_5] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_6] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_7] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_8] [numeric](1, 0) NOT NULL,
	[bga_cmpt_aplc_9] [numeric](1, 0) NOT NULL,
	[bga_sacct_aplc] [numeric](1, 0) NOT NULL,
	[bga_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
