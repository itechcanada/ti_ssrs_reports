USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_sahsax_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_sahsax_rec](
	[sax_cmpy_id] [char](3) NOT NULL,
	[sax_sls_qlf] [char](1) NOT NULL,
	[sax_upd_ref_no] [numeric](10, 0) NOT NULL,
	[sax_upd_ref_itm] [numeric](6, 0) NOT NULL,
	[sax_upd_dt] [date] NOT NULL,
	[sax_job_pfx] [char](2) NOT NULL,
	[sax_job_no] [numeric](8, 0) NOT NULL,
	[sax_jbs_pfx] [char](2) NOT NULL,
	[sax_jbs_no] [numeric](8, 0) NOT NULL,
	[sax_actvy_whs] [char](3) NOT NULL,
	[sax_prs] [char](3) NOT NULL,
	[sax_pwc] [char](3) NOT NULL
) ON [PRIMARY]
GO
