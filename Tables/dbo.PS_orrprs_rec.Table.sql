USE [Stratix_US]
GO
/****** Object:  Table [dbo].[PS_orrprs_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS_orrprs_rec](
	[prs_cmpy_id] [char](3) NOT NULL,
	[prs_prs] [char](3) NOT NULL,
	[prs_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[prs_desc15] [char](15) NOT NULL,
	[prs_prs_cl] [char](2) NOT NULL,
	[prs_cc_no] [numeric](3, 0) NULL,
	[prs_cst_pror_bss] [char](1) NOT NULL,
	[prs_prs_cst_bss] [char](1) NOT NULL,
	[prs_tm_lag] [numeric](5, 0) NOT NULL,
	[prs_cert_prs_typ] [char](3) NOT NULL,
	[prs_chg_frm] [numeric](1, 0) NOT NULL,
	[prs_chg_grd] [numeric](1, 0) NOT NULL,
	[prs_chg_fnsh] [numeric](1, 0) NOT NULL,
	[prs_frm_edt] [numeric](1, 0) NOT NULL,
	[prs_grd_edt] [numeric](1, 0) NOT NULL,
	[prs_size_edt] [numeric](1, 0) NOT NULL,
	[prs_fnsh_edt] [numeric](1, 0) NOT NULL,
	[prs_wdth_edt] [numeric](1, 0) NOT NULL,
	[prs_lgth_edt] [numeric](1, 0) NOT NULL,
	[prs_ga_edt] [numeric](1, 0) NOT NULL,
	[prs_dia_edt] [numeric](1, 0) NOT NULL,
	[prs_sch_lvl_typ] [char](1) NOT NULL,
	[prs_alwd_scr_pct] [numeric](5, 2) NOT NULL,
	[prs_match_prod] [char](1) NOT NULL,
	[prs_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
