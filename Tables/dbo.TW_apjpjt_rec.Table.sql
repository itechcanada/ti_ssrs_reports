USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TW_apjpjt_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TW_apjpjt_rec](
	[pjt_cmpy_id] [char](3) NOT NULL,
	[pjt_vchr_pfx] [char](2) NOT NULL,
	[pjt_vchr_no] [numeric](8, 0) NOT NULL,
	[pjt_tdsamt] [numeric](12, 2) NOT NULL,
	[pjt_tot_ex_gn_ls] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
GO
