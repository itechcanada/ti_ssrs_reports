USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_xctava_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_xctava_rec](
	[ava_tbl_nm] [char](6) NOT NULL,
	[ava_key_val] [char](245) NOT NULL,
	[ava_atmpl] [char](8) NOT NULL,
	[ava_attr] [char](6) NOT NULL,
	[ava_atmpl_typ] [char](3) NOT NULL,
	[ava_attr_val_var] [varchar](30) NOT NULL,
	[ava_attr_mval_var] [varchar](15) NOT NULL,
	[ava_attr_xval_var] [varchar](15) NOT NULL,
	[ava_key_fld01_var] [varchar](30) NULL,
	[ava_key_fld02_var] [varchar](30) NULL,
	[ava_key_fld03_var] [varchar](30) NULL,
	[ava_key_fld04_var] [varchar](30) NULL,
	[ava_key_fld05_var] [varchar](30) NULL,
	[ava_key_fld06_var] [varchar](30) NULL,
	[ava_key_fld07_var] [varchar](30) NULL,
	[ava_key_fld08_var] [varchar](30) NULL,
	[ava_key_fld09_var] [varchar](30) NULL,
	[ava_key_fld10_var] [varchar](30) NULL,
	[ava_key_fld11_var] [varchar](30) NULL,
	[ava_key_fld12_var] [varchar](30) NULL,
	[ava_key_fld13_var] [varchar](30) NULL,
	[ava_key_fld14_var] [varchar](30) NOT NULL
) ON [PRIMARY]
GO
