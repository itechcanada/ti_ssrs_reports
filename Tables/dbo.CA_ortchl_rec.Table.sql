USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_ortchl_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_ortchl_rec](
	[chl_cmpy_id] [char](3) NOT NULL,
	[chl_ref_pfx] [char](2) NOT NULL,
	[chl_ref_no] [numeric](8, 0) NOT NULL,
	[chl_ref_itm] [numeric](3, 0) NOT NULL,
	[chl_ref_ln_no] [numeric](2, 0) NOT NULL,
	[chl_chrg_no] [numeric](3, 0) NOT NULL,
	[chl_chrg_desc20] [char](20) NOT NULL,
	[chl_cc_seq_no] [numeric](2, 0) NOT NULL,
	[chl_cc_typ] [numeric](1, 0) NOT NULL,
	[chl_chrg_cl] [char](1) NOT NULL,
	[chl_prs_chrg] [numeric](1, 0) NOT NULL,
	[chl_chrg_orig] [char](1) NOT NULL,
	[chl_chrg_qty] [numeric](16, 4) NOT NULL,
	[chl_chrg_qty_um] [char](3) NULL,
	[chl_chrg] [numeric](13, 4) NOT NULL,
	[chl_chrg_um] [char](3) NULL,
	[chl_chrg_val] [numeric](12, 2) NOT NULL,
	[chl_disc] [numeric](13, 4) NOT NULL,
	[chl_disc_um] [char](3) NULL,
	[chl_disc_val] [numeric](12, 2) NOT NULL,
	[chl_net_chrg_val] [numeric](12, 2) NOT NULL,
	[chl_ovrd_chrg] [numeric](1, 0) NOT NULL,
	[chl_cst_ref_ln_no] [numeric](2, 0) NOT NULL,
	[chl_no_chrg] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
