USE [Stratix_US]
GO
/****** Object:  Table [dbo].[tbl_itech_potpod_history]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_itech_potpod_history](
	[pod_cmpy_id] [char](3) NOT NULL,
	[pod_po_pfx] [char](2) NOT NULL,
	[pod_po_no] [numeric](18, 0) NOT NULL,
	[pod_po_itm] [numeric](18, 0) NOT NULL,
	[pod_po_dist] [numeric](18, 0) NOT NULL,
	[pod_arr_dt_ent] [date] NULL,
	[pod_arr_fm_dt] [date] NULL,
	[pod_arr_to_dt] [date] NULL,
	[recordCreatedDate] [date] NOT NULL
) ON [PRIMARY]
GO
