USE [Stratix_US]
GO
/****** Object:  Table [dbo].[INV_REORDER_BACKUP]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INV_REORDER_BACKUP](
	[title] [varchar](50) NULL,
	[product] [varchar](50) NULL,
	[rating] [varchar](20) NULL,
	[needed] [varchar](1) NULL,
	[stock_qty] [int] NULL,
	[reorder_point] [int] NULL,
	[input] [varchar](30) NULL,
	[vendor] [varchar](50) NULL,
	[comment] [nvarchar](225) NULL,
	[type] [varchar](10) NULL
) ON [PRIMARY]
GO
