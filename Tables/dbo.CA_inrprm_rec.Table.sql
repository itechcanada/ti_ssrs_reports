USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CA_inrprm_rec]    Script Date: 03-11-2021 16:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CA_inrprm_rec](
	[prm_frm] [char](6) NOT NULL,
	[prm_grd] [char](8) NOT NULL,
	[prm_size] [char](15) NOT NULL,
	[prm_fnsh] [char](8) NOT NULL,
	[prm_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[prm_size_desc] [char](25) NOT NULL,
	[prm_shrt_size_desc] [char](16) NOT NULL,
	[prm_bas_msr] [char](1) NOT NULL,
	[prm_e_num_size1] [numeric](9, 5) NOT NULL,
	[prm_e_num_size2] [numeric](9, 5) NOT NULL,
	[prm_e_num_size3] [numeric](9, 5) NOT NULL,
	[prm_e_num_size4] [numeric](9, 5) NOT NULL,
	[prm_e_num_size5] [numeric](9, 5) NOT NULL,
	[prm_m_num_size1] [numeric](9, 5) NOT NULL,
	[prm_m_num_size2] [numeric](9, 5) NOT NULL,
	[prm_m_num_size3] [numeric](9, 5) NOT NULL,
	[prm_m_num_size4] [numeric](9, 5) NOT NULL,
	[prm_m_num_size5] [numeric](9, 5) NOT NULL,
	[prm_pcs_ctl] [numeric](1, 0) NOT NULL,
	[prm_dim_seg] [char](3) NOT NULL,
	[prm_sk_mthd] [char](3) NOT NULL,
	[prm_coil_frm] [numeric](1, 0) NOT NULL,
	[prm_lgth_bas_coil] [numeric](1, 0) NOT NULL,
	[prm_non_stk] [numeric](1, 0) NOT NULL,
	[prm_e_twf] [numeric](12, 6) NOT NULL,
	[prm_e_theo_wgt_um] [char](3) NULL,
	[prm_m_twf] [numeric](12, 6) NOT NULL,
	[prm_m_theo_wgt_um] [char](3) NULL,
	[prm_lgth_disp_fmt] [numeric](1, 0) NOT NULL,
	[prm_pep] [char](10) NOT NULL,
	[prm_prd_mgmt_seq] [numeric](6, 0) NOT NULL,
	[prm_clr] [char](3) NULL,
	[prm_ord_wgt_mult] [numeric](5, 3) NOT NULL,
	[prm_cst_opt] [char](1) NOT NULL,
	[prm_stat_cd] [numeric](8, 0) NOT NULL,
	[prm_e_xsect_area] [numeric](16, 4) NOT NULL,
	[prm_m_xsect_area] [numeric](16, 4) NOT NULL,
	[prm_sls_clsfcn] [char](2) NOT NULL,
	[prm_e_max_wdth] [numeric](9, 4) NOT NULL,
	[prm_m_max_wdth] [numeric](9, 4) NOT NULL,
	[prm_e_max_lgth] [numeric](9, 4) NOT NULL,
	[prm_m_max_lgth] [numeric](9, 4) NOT NULL,
	[prm_stp_buy] [numeric](1, 0) NOT NULL,
	[prm_ef_aplc] [numeric](1, 0) NOT NULL,
	[prm_itar] [numeric](1, 0) NOT NULL,
	[prm_hrmnz_trf_cd] [numeric](8, 0) NOT NULL,
	[prm_misc_prm] [numeric](1, 0) NOT NULL,
	[prm_actv] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
