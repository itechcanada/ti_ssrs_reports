USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_trhrad_rec_copy]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_trhrad_rec_copy](
	[rad_cmpy_id] [char](3) NOT NULL,
	[rad_ref_pfx] [char](2) NOT NULL,
	[rad_ref_no] [numeric](8, 0) NOT NULL,
	[rad_ref_itm] [numeric](3, 0) NOT NULL,
	[rad_ref_upd_dt] [date] NOT NULL,
	[rad_src_ref_pfx] [char](2) NOT NULL,
	[rad_src_ref_no] [numeric](8, 0) NOT NULL,
	[rad_src_ref_itm] [numeric](3, 0) NOT NULL,
	[rad_src_ref_upd_dt] [date] NULL,
	[rad_rcpt_typ] [char](1) NOT NULL,
	[rad_rcpt_brh] [char](3) NOT NULL,
	[rad_cus_ven_typ] [char](1) NOT NULL,
	[rad_cus_ven_id] [char](8) NOT NULL,
	[rad_cus_ven_shp] [numeric](3, 0) NOT NULL,
	[rad_rcvg_whs] [char](3) NOT NULL,
	[rad_rcpt_actvy_dt] [date] NOT NULL,
	[rad_rcvd_pcs] [numeric](7, 0) NOT NULL,
	[rad_rcvd_msr] [numeric](16, 4) NOT NULL,
	[rad_rcvd_wgt] [numeric](10, 2) NOT NULL,
	[rad_rcvd_qty] [numeric](16, 4) NOT NULL,
	[rad_frm] [char](6) NOT NULL,
	[rad_grd] [char](8) NOT NULL,
	[rad_num_size1] [numeric](9, 5) NOT NULL,
	[rad_num_size2] [numeric](9, 5) NOT NULL,
	[rad_num_size3] [numeric](9, 5) NOT NULL,
	[rad_num_size4] [numeric](9, 5) NOT NULL,
	[rad_num_size5] [numeric](9, 5) NOT NULL,
	[rad_size] [char](15) NOT NULL,
	[rad_fnsh] [char](8) NOT NULL,
	[rad_ef_evar] [char](30) NOT NULL,
	[rad_wdth] [numeric](9, 4) NOT NULL,
	[rad_lgth] [numeric](9, 4) NOT NULL,
	[rad_dim_dsgn] [char](1) NOT NULL,
	[rad_idia] [numeric](9, 5) NOT NULL,
	[rad_odia] [numeric](9, 5) NOT NULL,
	[rad_ga_size] [numeric](8, 5) NOT NULL,
	[rad_ga_typ] [char](1) NOT NULL,
	[rad_rdm_dim_1] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_2] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_3] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_4] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_5] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_6] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_7] [numeric](9, 4) NOT NULL,
	[rad_rdm_dim_8] [numeric](9, 4) NOT NULL,
	[rad_rdm_area] [numeric](16, 4) NOT NULL,
	[rad_ent_msr] [char](1) NOT NULL,
	[rad_ord_lgth_typ] [char](2) NULL,
	[rad_mtl_cst] [numeric](13, 4) NOT NULL,
	[rad_mtl_cst_um] [char](3) NULL,
	[rad_ven_mtl_val] [numeric](12, 2) NOT NULL,
	[rad_bas_mtl_val] [numeric](12, 2) NOT NULL,
	[rad_tot_cst] [numeric](13, 4) NOT NULL,
	[rad_qty_cst_um] [char](3) NULL,
	[rad_bas_tot_val] [numeric](15, 2) NOT NULL,
	[rad_cry] [char](3) NOT NULL,
	[rad_ex_rt] [numeric](12, 8) NOT NULL,
	[rad_ex_rt_typ] [char](1) NOT NULL,
	[rad_po_brh] [char](3) NOT NULL,
	[rad_po_pfx] [char](2) NOT NULL,
	[rad_po_no] [numeric](8, 0) NOT NULL,
	[rad_po_itm] [numeric](3, 0) NOT NULL,
	[rad_po_dist] [numeric](2, 0) NOT NULL,
	[rad_po_rls_no] [char](10) NOT NULL,
	[rad_po_intl_rls_no] [numeric](2, 0) NOT NULL,
	[rad_po_typ] [char](1) NOT NULL,
	[rad_pur_cat] [char](2) NOT NULL,
	[rad_byr] [char](4) NOT NULL,
	[rad_src] [char](2) NOT NULL,
	[rad_aly_schg_apln] [char](1) NOT NULL,
	[rad_consg] [numeric](1, 0) NOT NULL,
	[rad_invt_qlty] [char](1) NULL,
	[rad_dist_typ] [char](1) NOT NULL,
	[rad_bgt_for] [char](1) NOT NULL,
	[rad_bgt_for_cus_id] [char](8) NULL,
	[rad_bgt_for_part] [char](30) NOT NULL,
	[rad_bgt_for_slp] [char](4) NULL,
	[rad_ord_pfx] [char](2) NOT NULL,
	[rad_ord_no] [numeric](8, 0) NOT NULL,
	[rad_ord_itm] [numeric](3, 0) NOT NULL,
	[rad_so_brh] [char](3) NOT NULL,
	[rad_so_pfx] [char](2) NOT NULL,
	[rad_so_no] [numeric](8, 0) NOT NULL,
	[rad_so_itm] [numeric](3, 0) NOT NULL,
	[rad_so_sitm] [numeric](2, 0) NOT NULL,
	[rad_sld_cus_id] [char](8) NOT NULL,
	[rad_src_whs] [char](3) NOT NULL,
	[rad_transp_pfx] [char](2) NOT NULL,
	[rad_transp_no] [numeric](8, 0) NOT NULL,
	[rad_sprc_pfx] [char](2) NOT NULL,
	[rad_sprc_no] [numeric](8, 0) NOT NULL,
	[rad_actvy_rsn] [char](3) NOT NULL,
	[rad_trm_trd] [char](3) NOT NULL,
	[rad_prs_sts] [numeric](1, 0) NOT NULL
) ON [PRIMARY]
GO
