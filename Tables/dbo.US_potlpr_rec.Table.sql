USE [Stratix_US]
GO
/****** Object:  Table [dbo].[US_potlpr_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[US_potlpr_rec](
	[lpr_cmpy_id] [char](3) NOT NULL,
	[lpr_po_pfx] [char](2) NOT NULL,
	[lpr_po_no] [numeric](8, 0) NOT NULL,
	[lpr_po_itm] [numeric](3, 0) NOT NULL,
	[lpr_chg_ref_no] [numeric](8, 0) NOT NULL,
	[lpr_po_intl_rls_no] [numeric](2, 0) NOT NULL,
	[lpr_lst_chg_dtts] [datetime2](7) NULL,
	[lpr_lst_chg_dtms] [numeric](3, 0) NULL,
	[lpr_chg_fct] [char](1) NOT NULL,
	[lpr_due_dt_qlf] [char](1) NULL,
	[lpr_due_dt_fmt] [char](2) NOT NULL,
	[lpr_due_dt_ent] [numeric](8, 0) NOT NULL,
	[lpr_due_fm_dt] [date] NULL,
	[lpr_due_to_dt] [date] NULL,
	[lpr_due_dt_wk_no] [numeric](2, 0) NOT NULL,
	[lpr_due_dt_wk_cy] [numeric](4, 0) NOT NULL,
	[lpr_due_dt_rsn] [char](3) NOT NULL,
	[lpr_due_dt_rmk30] [char](30) NOT NULL
) ON [PRIMARY]
GO
