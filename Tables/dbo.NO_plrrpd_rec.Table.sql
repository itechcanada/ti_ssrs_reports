USE [Stratix_US]
GO
/****** Object:  Table [dbo].[NO_plrrpd_rec]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NO_plrrpd_rec](
	[rpd_cmpy_id] [char](3) NOT NULL,
	[rpd_rpd_ctl_no] [numeric](10, 0) NOT NULL,
	[rpd_ref_ctl_no] [numeric](10, 0) NOT NULL,
	[rpd_desc30] [char](30) NOT NULL,
	[rpd_frm] [char](6) NOT NULL,
	[rpd_grd] [char](8) NOT NULL,
	[rpd_size] [char](15) NOT NULL,
	[rpd_fnsh] [char](8) NOT NULL,
	[rpd_wdth] [numeric](9, 4) NOT NULL,
	[rpd_lgth] [numeric](9, 4) NOT NULL,
	[rpd_dim_dsgn] [char](1) NOT NULL,
	[rpd_idia] [numeric](9, 5) NOT NULL,
	[rpd_odia] [numeric](9, 5) NOT NULL,
	[rpd_ga_size] [numeric](8, 5) NOT NULL,
	[rpd_bgt_for_cus_id] [char](8) NULL,
	[rpd_wdth_fm] [numeric](9, 4) NOT NULL,
	[rpd_wdth_to] [numeric](9, 4) NOT NULL,
	[rpd_lgth_fm] [numeric](9, 4) NOT NULL,
	[rpd_lgth_to] [numeric](9, 4) NOT NULL,
	[rpd_idia_fm] [numeric](9, 5) NOT NULL,
	[rpd_odia_fm] [numeric](9, 5) NOT NULL,
	[rpd_ga_size_fm] [numeric](8, 5) NOT NULL,
	[rpd_idia_to] [numeric](9, 5) NOT NULL,
	[rpd_odia_to] [numeric](9, 5) NOT NULL,
	[rpd_ga_size_to] [numeric](8, 5) NOT NULL,
	[rpd_prd_dfft] [char](10) NOT NULL,
	[rpd_dflt_purch_pt] [char](5) NULL,
	[rpd_dflt_rmnt_whs] [char](3) NULL,
	[rpd_vrs_pct] [numeric](6, 3) NOT NULL,
	[rpd_yld_ls_pct] [numeric](6, 3) NOT NULL,
	[rpd_abc_rnk] [char](1) NOT NULL,
	[rpd_rnk_sts] [char](1) NOT NULL,
	[rpd_whs_prd_arm] [char](1) NOT NULL,
	[rpd_invt_trn] [numeric](3, 1) NOT NULL,
	[rpd_invt_trn_sts] [char](3) NOT NULL,
	[rpd_actst_dt] [date] NOT NULL,
	[rpd_trn_obj] [numeric](3, 1) NOT NULL,
	[rpd_tearn_obj] [numeric](4, 1) NOT NULL,
	[rpd_wfl_sgnt_dsbl] [numeric](1, 0) NOT NULL,
	[rpd_mohd_qty] [numeric](16, 4) NOT NULL,
	[rpd_mwks_sls] [numeric](3, 0) NOT NULL,
	[rpd_xohd_qty] [numeric](16, 4) NOT NULL,
	[rpd_upd_dtts] [datetime2](7) NULL,
	[rpd_upd_dtms] [numeric](3, 0) NULL
) ON [PRIMARY]
GO
