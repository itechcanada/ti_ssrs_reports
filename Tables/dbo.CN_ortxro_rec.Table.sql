USE [Stratix_US]
GO
/****** Object:  Table [dbo].[CN_ortxro_rec]    Script Date: 03-11-2021 16:27:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CN_ortxro_rec](
	[xro_cmpy_id] [char](3) NOT NULL,
	[xro_ord_ctl_no] [numeric](10, 0) NOT NULL,
	[xro_src_pfx] [char](2) NOT NULL,
	[xro_src_no] [numeric](8, 0) NOT NULL,
	[xro_src_itm] [numeric](3, 0) NOT NULL,
	[xro_src_rls_no] [numeric](2, 0) NOT NULL,
	[xro_ord_pfx] [char](2) NOT NULL,
	[xro_ord_no] [numeric](8, 0) NOT NULL,
	[xro_ord_itm] [numeric](3, 0) NOT NULL,
	[xro_ord_rls_no] [numeric](2, 0) NOT NULL,
	[xro_ord_src] [char](1) NOT NULL
) ON [PRIMARY]
GO
