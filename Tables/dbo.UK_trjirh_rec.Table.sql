USE [Stratix_US]
GO
/****** Object:  Table [dbo].[UK_trjirh_rec]    Script Date: 03-11-2021 16:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UK_trjirh_rec](
	[irh_cmpy_id] [char](3) NOT NULL,
	[irh_ref_pfx] [char](2) NOT NULL,
	[irh_ref_no] [numeric](8, 0) NOT NULL,
	[irh_actvy_dt] [date] NOT NULL,
	[irh_cus_ven_typ] [char](1) NOT NULL,
	[irh_cus_ven_id] [char](8) NOT NULL,
	[irh_rcvg_whs] [char](3) NOT NULL,
	[irh_rcpt_typ] [char](1) NOT NULL
) ON [PRIMARY]
GO
