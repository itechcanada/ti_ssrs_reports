USE [Stratix_US]
GO
/****** Object:  Table [dbo].[TOP_INVENTORY]    Script Date: 03-11-2021 16:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TOP_INVENTORY](
	[product] [varchar](25) NULL,
	[stock_lbs] [float] NULL,
	[period] [datetime] NULL
) ON [PRIMARY]
GO
